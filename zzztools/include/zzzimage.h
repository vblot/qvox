/** -*- mode: c++ -*-
 * @file   zzzimage.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:16:35 2005
 *
 * @brief  ZZZImage volume image template.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _ZZZIMAGE_H_
#define _ZZZIMAGE_H_

#include <exception>
#include <typeinfo>

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <math.h>
#include <cstring>
#include <iomanip>
#include <globals.h>
#include <config.h>
#include <limits>
#include <vector>

#include "Pair.h"
#include "Triple.h"
#include "Triple.h"
#include "RefTriple.h"

#include "panfile.h"
#include "dimension.h"
#include "point.h"
#include "ByteInput.h"
#include "ByteOutput.h"
#include "image_visitor.h"

/**
 * Gathers all properties of an AbstractImage.
 */
struct ImageProperties {
  SSize bands;			/**< Number of bands */
  SSize cols;		   	/**< Number of columns */
  SSize rows;			   /**< Number of rows */
  SSize depth;			/**< Number of planes */
  ColorSpace colorspace;	/**< Color space */
  UInt32 maxLabel;		/**< Number of labels in a region map */
  Int32 size;		      /**< Number of nodes (in a graph) */

  enum DataArrayLayout { NormalArrayLayout, ZeroFramedArrayLayout };
  DataArrayLayout arrayLayout;

  /**
   * Creates a new <code>AbstractImageProperty</code> with the specified
   * attribute values.
   * @param nb	the number of bands.
   * @param nc	the number of columns (width).
   * @param nr	the number of rows (height).
   * @param nd	the number of planes (depth).
    * @param c	the color space.
    * @param l	the number of labels (for a region map).
    * @param s	the number of nodes (for a graph).
    */
  ImageProperties( SSize nb, SSize nc, SSize nr, SSize nd,
                   ColorSpace c,
                   UInt32 l, Int32 s,
                   DataArrayLayout arrayLayout = ZeroFramedArrayLayout ):
    bands( nb ), cols( nc ), rows( nr ), depth( nd ),
    colorspace( c ), maxLabel( l ), size( s ),
    arrayLayout( arrayLayout ) { }

  bool operator==( const ImageProperties & other );
  bool operator!=( const ImageProperties & other );
  friend std::ostream & operator<<( std::ostream & out,
                                    const ImageProperties & props );
};

std::ostream & operator<<( std::ostream & out, const ImageProperties & props );


template<typename T> class ZZZImage;

class AbstractImage {

public:

  static AbstractImage * create( int valueType,
                                 SSize width = 0,
                                 SSize height = 1,
                                 SSize depth = 1,
                                 SSize bands = 1,
                                 ImageProperties::DataArrayLayout arrayLayout
                                 = ImageProperties::ZeroFramedArrayLayout );

  static AbstractImage * create( int valueType,
                                 SSize bands,
                                 const Dimension3d & dim,
                                 ImageProperties::DataArrayLayout  arrayLayout
                                 = ImageProperties::ZeroFramedArrayLayout  );

  static AbstractImage * create( Typobj poType,
                                 SSize width = 0,
                                 SSize height = 1,
                                 SSize depth = 1,
                                 SSize bands = 1,
                                 ImageProperties::DataArrayLayout arrayLayout
                                 = ImageProperties::ZeroFramedArrayLayout );

  static AbstractImage * create( Typobj poType,
                                 SSize bands,
                                 const Dimension3d & dim,
                                 ImageProperties::DataArrayLayout  arrayLayout
                                 = ImageProperties::ZeroFramedArrayLayout  );

  static AbstractImage * createFromFile( ByteInput & input );

  static AbstractImage * createFromFile( const char * fileName  );

public:

  AbstractImage() { }
  virtual ~AbstractImage() { };

  /**
    * Creates and returns a distinct copy of this object.
    */
  virtual AbstractImage * clone() const = 0;
  virtual AbstractImage * clone( int valueType ) const = 0;

  /**
   * Returns the identifier of the object.
   */
  virtual Typobj poType() const = 0;
  virtual Typobj poFileType() const = 0;
  virtual bool poFileType( Typobj ) = 0;
  virtual std::string name() const = 0;

  virtual SSize width() const = 0;
  virtual SSize height() const = 0;
  virtual SSize depth() const = 0;
  virtual SSize bands() const = 0;
  virtual Dimension3d size() const = 0;
  virtual ImageProperties props() const = 0;
  virtual void getDimension( SSize & width, SSize & height, SSize & depth,
                             SSize & bands ) const = 0;

  virtual void boundingBox( SSize & xMin, SSize & yMin, SSize & zMin,
                            SSize & xMax, SSize & yMax, SSize & zMax ) const = 0;

  virtual bool isEmpty() const = 0;

  virtual const char * valueString( SSize x, SSize y, SSize z ) const = 0;
  virtual const char * valueString( const Triple<SSize> & voxel ) const = 0;
  virtual bool nonZero( SSize x, SSize y, SSize z ) const = 0;

  virtual void accept( AbstractImageVisitor & visitor ) = 0;

  virtual void free() = 0;
  virtual bool load( ByteInput & in ) = 0;
  virtual bool save( ByteOutput & out, const char * format ) const = 0;
  virtual bool load( const char * filename, bool multiplexed ) = 0;
  virtual bool save( const char * filename ) const = 0;

  virtual void trim( int axis, SSize n ) = 0;
  virtual void mirror( int axis ) = 0;
  virtual void rotate( int axis ) = 0;
  virtual void fitBoundingBox( SSize margin = 0 ) = 0;
  virtual Size vectorSize() const = 0;
  virtual size_t realBandVectorSize() const = 0;

  virtual bool merge( AbstractImage & other, int axis ) = 0;

  virtual void binarize( UChar value = 255 ) = 0;
  virtual void binarize( UChar red, UChar green, UChar blue ) = 0;
  virtual void clear() = 0;
  virtual void negative() = 0;
  virtual void grayShadeFromHue() = 0;
  virtual void grayShadeFromSaturation() = 0;
  virtual void bitPlane( UChar bit, SSize band ) = 0;
  virtual void bitPlaneGrayCode( UChar bit, SSize band ) = 0;
  virtual void zeroPlane( SSize x, SSize y, SSize z ) = 0;

  virtual AbstractImage & mask( const ZZZImage< UChar > & mask ) = 0;

  virtual void getFramedBitMask( ZZZImage<bool> & ) const = 0;
  virtual void applyFramedBitMask( const ZZZImage<bool> & ) = 0;

  virtual void swapEndianness() = 0;
  virtual void quantize( UInt32 cardinality ) = 0;
  virtual void toGray() = 0;
  virtual void toColor() = 0;
  virtual void hollowOut( int adjacency = ADJ26 ) = 0;
  virtual void seedFill( SSize x, SSize y, SSize z, UChar value = 255 ) = 0;
  virtual void seedFillXY( SSize x, SSize y, SSize z, UChar value = 255 ) = 0;
  virtual void seedFillXZ( SSize x, SSize y, SSize z, UChar value = 255 ) = 0;
  virtual void seedFillYZ( SSize x, SSize y, SSize z, UChar value = 255 ) = 0;

  virtual void surfaceFill( UChar value = 255 ) = 0;
  virtual void fillForHoleClosing() = 0;
  virtual void addBoundaries( UChar value, UChar thickness ) = 0;
  virtual void subSample( UChar minCount = 4 ) = 0;
  virtual void scale( UChar direction, UChar factor = 2 ) = 0;
  virtual void extrude( SSize depth ) = 0;
  virtual void spongify() = 0;

  virtual bool setValue( Triple<SSize> voxel, UChar red, UChar green, UChar blue ) = 0;

  virtual void clamp( const Triple<Int32> & , const Triple<Int32> & ) = 0;
  virtual void clamp( const Triple<UInt32> & , const Triple<UInt32> & ) = 0;
  virtual void clamp( const Triple<Float> & , const Triple<Float> & ) = 0;

  virtual bool boundary( SSize x ) const = 0;
  virtual bool boundary( SSize x, SSize y ) const = 0;
  virtual bool boundary( SSize x, SSize y, SSize z ) const = 0;
  virtual bool boundary( const Pair<SSize> & pixel ) const = 0;
  virtual bool boundary( const Triple<SSize> & voxel ) const = 0;


  virtual Config config( SSize x, SSize y, SSize z ) const = 0;
  virtual Config config( const Triple<SSize> & voxel ) const = 0;


  virtual AbstractImage * levelMap( SSize depth ) = 0;

  /**
   *
   *
   * @param x The x coordinate of the plane to extract, or -1.
   * @param y The y coordinate of the plane to extract, or -1.
   * @param z The z coordinate of the plane to extract, or -1.
   *
   * @return A 2D image (with depth = 1)
   */
  virtual AbstractImage * slice( SSize x, SSize y, SSize z ) = 0;

  virtual AbstractImage * sub( SSize x, SSize y, SSize z,
                               SSize width, SSize height, SSize depth ) = 0;

  virtual void insert( SSize x, SSize y, SSize z,
                       AbstractImage * image ) = 0;

  virtual void insertSlice( SSize x, SSize y, SSize z,
                            AbstractImage * image ) = 0;


  enum ColorMode { CastColorMode, EqualizeColorMode };

  virtual void getColor( SSize x, SSize y, SSize z, UChar & red, UChar & green, UChar & blue) = 0;
  virtual void getColor( const Triple<SSize> & , UChar & red, UChar & green, UChar & blue) = 0;

  virtual void getGrayLevel( SSize x, SSize y, SSize z, UChar & gray ) = 0;
  virtual void getGrayLevel( const Triple<SSize> & , UChar & gray ) = 0;

  virtual void setColorMode( ColorMode ) = 0;

  virtual Triple<UChar> getColorFromLUT( const Triple<SSize> & voxel,
                                         const std::vector< Triple<UChar> > & lut ) const = 0;

protected:
  virtual bool loadDataRLE( ByteInput & in ) = 0;
  virtual bool loadCharData( ByteInput & in ) = 0;
  virtual bool loadDataASC( ByteInput & in ) = 0;
  virtual bool loadData( ByteInput & in, bool multiplexed ) = 0;
  virtual bool loadPandoreData( ByteInput & in, bool region, bool swapBytes ) = 0;
  virtual void reverseDataBytes() = 0;
};

template< typename T >
class ZZZImage : public AbstractImage {

public:
  typedef T value_type;         /**< The type of the data. */

  ZZZImage( SSize width = 0,
            SSize height = 1,
            SSize depth = 1,
            SSize bands = 1,
            ImageProperties::DataArrayLayout arrayLayout
            = ImageProperties::ZeroFramedArrayLayout )
    : AbstractImage(), _bands_array( 0 ),
      _bands( 0 ), _cols( 0 ), _rows( 0 ), _depth( 0 ),
      _arrayLayout( arrayLayout ),
      _colorSpace( RGB ),
      _colorMode(CastColorMode) {
    TRACE << "++ZZZImage<" << TypeName<T>::name() << ">()\n";
    if ( !width || !height || !depth || !bands ) {
      width = height = depth = bands = 0;
    }
    this->_poFileType = (Typobj) Po_type< ZZZImage<T> >::type;
    alloc( width, height, depth, bands, arrayLayout );
  }

  ZZZImage( const ZZZImage & other )
    : AbstractImage(), _bands_array( 0 ), _bands( 0 ),
      _cols( 0 ), _rows( 0 ), _depth( 0 ),
      _arrayLayout( ImageProperties::ZeroFramedArrayLayout ),
      _colorSpace( RGB ),
      _colorMode(CastColorMode) {
    TRACE << "rrZZZImage<" << TypeName<T>::name() << ">()\n";
    this->_poFileType = (Typobj) Po_type< ZZZImage<T> >::type;
    alloc( other._cols, other._rows, other._depth,
           other._bands, other._arrayLayout );
    (*this) = other;
  }

  /**
   * Creates a new image with the specified dimension.
   * Allocates therefrom the related data.
   * @param bands the number of bands of the image.
   * @param dim	the dimension of the image.
   */
  ZZZImage( SSize bands,
            const Dimension3d & dim,
            ImageProperties::DataArrayLayout arrayLayout
            = ImageProperties::ZeroFramedArrayLayout  )
    : AbstractImage(), _bands_array( 0 ),
      _bands( 0 ), _cols( 0 ), _rows( 0 ), _depth( 0 ),
      _arrayLayout( arrayLayout ),
      _colorSpace( RGB ),
      _colorMode(CastColorMode) {
    TRACE << "++ZZZImage<" << TypeName<T>::name() << ">()\n";
    this->_poFileType = (Typobj) Po_type< ZZZImage<T> >::type;
    alloc( dim, bands, arrayLayout );
  }

  /**
    * Creates a new image with the specified properties.
    * Allocates therefrom the related data.
    * @param props	the properties.
    */
  ZZZImage( const ImageProperties & props )
    : AbstractImage(), _bands_array( 0 ),
      _bands( 0 ), _cols( 0 ), _rows( 0 ), _depth( 0 ),
      _arrayLayout( props.arrayLayout ),
      _colorSpace( RGB ),
      _colorMode(CastColorMode) {
    TRACE << "++ZZZImage<" << TypeName<T>::name() << ">()\n";
    this->_poFileType = (Typobj) Po_type< ZZZImage<T> >::type;
    alloc( props.cols, props.rows, props.depth,
           props.bands, props.arrayLayout );
  }

  ~ZZZImage() {
    TRACE << "--ZZZImage<" << TypeName<T>::name() << ">()\n";
    free();
  }

  void insert( const ZZZImage<T> & other, SSize x = 0, SSize y = 0, SSize z = 0 );

  void free();

  void accept( AbstractImageVisitor & visitor );

  static const ZZZImage<T> null; /**< An empty image. */

  /**
   * Allocates the image data from the specified width, height,
   * depth, and number of bands.
   * @param width	the width of the image.
   * @param height	the height of the image.
   * @param depth	the depth of the image.
   * @param bands	the number of bands of the image.
   */
  bool alloc( SSize width = 0,
              SSize height = 1,
              SSize depth = 1,
              SSize bands = 1,
              ImageProperties::DataArrayLayout arrayLayout
              = ImageProperties::ZeroFramedArrayLayout );

  /**
    * Allocates the image data from the specified properties.
    * @param props	the properties of the image.
    */
  bool alloc( const ImageProperties & props ) {
    return alloc( props.cols, props.rows, props.depth, props.arrayLayout );
  }

  /**
   * Allocates the image data from the specified bands number and dimension.
   * @param bands	the number of bands of the image.
   * @param dim	the dimension of the image.
   */
  bool alloc( const Dimension3d & dim,
              SSize bands = 1,
              ImageProperties::DataArrayLayout arrayLayout
              = ImageProperties::ZeroFramedArrayLayout ) {
    return alloc( dim.w, dim.h, dim.d, bands, arrayLayout );
  }

  /**
   * Allocates the (2D) image data from the specified bands number
   * and dimension.
   * @param dim the dimension of the image.
   * @param bands the number of bands of the image.
   */
  bool alloc( const Dimension2d & dim,
              const SSize bands = 1,
              ImageProperties::DataArrayLayout arrayLayout
              = ImageProperties::ZeroFramedArrayLayout ) {
    return alloc( dim.w, dim.h, 1, bands, arrayLayout );
  }

  /**
   * Allocates the (1D) image data from the specified bands number
   * and dimension.
   * @param dim the dimension of the image.
   * @param bands the number of bands of the image.
   */
  bool alloc( const Dimension1d & dim,
              const SSize bands = 1,
              ImageProperties::DataArrayLayout arrayLayout
              = ImageProperties::ZeroFramedArrayLayout ) {
    return alloc( dim.w, 1, 1, bands, arrayLayout );
  }

  /**
   * Returns the specified band of the image data as a unique vector
   * @param band	the band number.
   */
  value_type * dataVector( SSize band ) const {
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
      return &_bands_array[ band ][ -1 ][ -1 ][ -1 ];
    else
      return &_bands_array[ band ][ 0 ][ 0 ][ 0 ];
  }

  /**
   * Returns a pointer to the actual data array of
   * a given band (default is band 0).
   * If the band is out of range, the first band's matrix is returned.
   * (Therefore, gray levels images may be seen has 3 equal bands images.)
   *
   * @param band The number of the wanted band.
   * @return A pointer to the 3D data array.
   */
  value_type *** data( SSize band = 0 ) const {
    if ( !_bands ) return 0;
    if ( band >= _bands ) return _bands_array[ 0 ];
    return _bands_array[ band ];
  }

  /**
   * User defined conversion from an image
   * to a boolean.
   *
   * @return true if the image has a real size (not zero).
   */
  bool isEmpty() const;

  /**
   * Returns the specified band (a 3D matrix) of the image.
   * If the band is out of range, the first band's matrix is returned.
   * (Therefore, gray levels images may be seen has 3 equal bands images.)
   *
   * @param band the band number.
   */
  T *** operator[] ( SSize band ) {
    if ( band >= _bands ) return _bands_array[ 0 ];
    return _bands_array[ band ];
  }

  /**
    * Returns the specified band (a 3D matrix) of the image.
    * If the band is out of range, the first band's matrix is returned.
    * (Therefore, gray levels images may be seen has 3 equal bands images.)
    *
    * @param band the band number.
    */
  T *** operator[] ( SSize band ) const {
    if ( band >= _bands )
      return _bands_array[ 0 ];
    return _bands_array[ band ];
  }

  /**
   * Creates and returns a distinct copy of this object.
   */
  ZZZImage<T> * clone() const;

  AbstractImage * clone( int valueType ) const;

  /**
   * Swaps data with another volume.
   *
   * @param other A volume from which data must be swapped.
   */
  void swapData( ZZZImage<T> & other ) {
    swapValues( _bands_array, other._bands_array );
    swapValues( _bands, other._bands );
    swapValues( _cols, other._cols );
    swapValues( _rows, other._rows );
    swapValues( _depth, other._depth );
    swapValues( _arrayLayout, other._arrayLayout );
    swapValues( _colorSpace, other._colorSpace );
    swapValues( _colorMode, other._colorMode );
    swapValues( _aspect, other._aspect );
    swapValues( _poFileType, other._poFileType );
  };

  /**
   * Sets all pixels with the given value.
   * @param val	the given value.
   * @return the image.
   */
  ZZZImage<T> & operator=( const T val );


  /**
   * Sets the pixels value with the pixels value of the given image.
   * This supposes that images have the same dimensions.
   * @param src the given image.
   * @return the image.
   */
  template< typename U >
  ZZZImage<T> & operator=( const ZZZImage< U > & src ) {
    if ( size() != src.size() || _bands != src.bands() ) {
      alloc( src.size(), src.bands() );
    }
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
      for ( SSize band = 0; band < _bands; ++band ) {
        for ( SSize z = 0; z < _depth; ++z )
          for ( SSize y = 0; y < _rows; ++y )
            for ( SSize x = 0; x < _cols; ++x ) {
              (*this)(x,y,z,band) = src(x,y,z,band);
            }
      }
    else
      for ( SSize band = 0; band < _bands; ++band ) {
        SSize index = 0;
        const SSize limit = _rows * _cols * _depth;
        T * p = _bands_array[band][0][0];
        while ( index < limit ) {
          *p++ = static_cast<T>( src.nth_value( index++ ) );
        }
      }
    return *this;
  }

  /**
   * Sets the pixels value with the pixels value of the given image.
   * This supposes that images have the same dimensions.
   * @param src	the given image.
   * @return the image.
   */
  ZZZImage<T> & operator=( const ZZZImage<T> & src );

  ZZZImage<T> & operator+=( const T &  );

  /**
   * Returns by references the dimensions.
   *
   * @param width
   * @param height
   * @param depth
   * @param bands
   */
  void getDimension( SSize & width, SSize & height, SSize & depth,
                     SSize & bands ) const {
    width = _cols;
    height = _rows;
    depth = _depth;
    bands = _bands;
  }

  bool holds( SSize x ) const {
    return ( x >= 0 ) && ( x < _cols );
  }

  bool holds( SSize x, SSize y ) const {
    return ( x >= 0 ) && ( x < _cols )
        && ( y >= 0 ) && ( y < _rows );
  }

  bool holds( SSize x, SSize y, SSize z ) const {
    return ( x >= 0 ) && ( x < _cols )
        && ( y >= 0 ) && ( y < _rows )
        && ( z >= 0 ) && ( z < _depth );
  }

  bool holds( SSize x, SSize y, SSize z, SSize band ) const {
    return ( x >= 0 ) && ( x < _cols )
        && ( y >= 0 ) && ( y < _rows )
        && ( z >= 0 ) && ( z < _depth )
        && ( band >= 0 ) && ( band < _bands );
  }

  bool holds( const Triple<SSize> & voxel ) const {
    return ( voxel.first >= 0 ) && ( voxel.first < _cols )
        && ( voxel.second >= 0 ) && ( voxel.second < _rows )
        && ( voxel.third >= 0 ) && ( voxel.third < _depth );
  }

  bool holds( const Pair<SSize> & pixel ) const {
    return ( pixel.first >= 0 ) && ( pixel.first < _cols )
        && ( pixel.second >= 0 ) && ( pixel.second < _rows );
  }

  void clamp( const Triple<Int32> & , const Triple<Int32> & );
  void clamp( const Triple<UInt32> & , const Triple<UInt32> & );
  void clamp( const Triple<Float> & , const Triple<Float> & );

  bool boundary( SSize x ) const {
    return ( x == 0 || x == _cols - 1 );
  };

  bool boundary( SSize x, SSize y ) const {
    return ( x == 0 || y == 0 ||
             x == _cols - 1 || y == _rows - 1  );
  };

  bool boundary( SSize x, SSize y, SSize z ) const {
    return ( x == 0 || y == 0 || z == 0
             || x == _cols - 1
             || y == _rows - 1
             || z == _depth - 1 );
  };

  bool boundary( const Pair<SSize> & pixel ) const {
    return ( pixel.first == 0 ||
             pixel.second == 0  ||
             pixel.first == _cols - 1 ||
             pixel.second == _rows - 1  );
  };

  bool boundary( const Triple<SSize> & voxel ) const {
    return ( voxel.first == 0 || voxel.second == 0 || voxel.third == 0
             || voxel.first == _cols - 1
             || voxel.second == _rows - 1
             || voxel.third == _depth - 1 );
  };

  /**
   * Provides access via reference to a voxel, provided its coordinates.
   *
   * @param x The voxel's column.
   * @param y The voxel's row.
   * @param z The voxel's depth.
   * @param band The voxel's band.
   * @return A reference to a voxel value in the data array.
   */
  T & operator()( SSize x, SSize y = 0, SSize z = 0, SSize band = 0 ) {
    return data( band )[z][y][x];
  }

  /**
   * Provides access via reference to a voxel, provided its coordinates.
   *
   * @param x The voxel's column.
   * @param y The voxel's row.
   * @param z The voxel's depth.
   * @param band The voxel's band.
   * @return A reference to a voxel value in the data array.
   */
  const T & operator()( SSize x, SSize y = 0, SSize z = 0, SSize band = 0 ) const {
    return data( band )[z][y][x];
  }

  T nth_value( const SSize index, const SSize band = 0 ) const;

  const char * valueString( SSize x, SSize y, SSize z ) const;

  const char * valueString( const Triple<SSize> & voxel ) const;

  bool nonZero( SSize x, SSize y, SSize z ) const;

  /**
   * Given a pair of integer, returns a repeference
   * to the voxel (in the first plane) with coordinates (first, second, 0)
   *
   * @param p
   *
   * @return
   */

  T & operator() ( const Pair<SSize> & p ) {
    return data( 0 )[ 0 ][ p.second ][ p.first ];
  }

  const T & operator() ( const Pair<SSize> & p ) const {
    return data( 0 )[ 0 ][ p.second ][ p.first ];
  }

  T & operator() ( const Pair<SSize> & p, const SSize band ) {
    return data( band )[ 0 ][ p.second ][ p.first ];
  }

  const T & operator() ( const Pair<SSize> & p, const SSize band ) const {
    return data( band )[ 0 ][ p.second ][ p.first ];
  }


  T & operator() ( const Triple<SSize> & t ) {
    return data( 0 )[ t.third ][ t.second ][ t.first ];
  }

  const T & operator() ( const Triple<SSize> & t ) const {
    return data( 0 )[ t.third ][ t.second ][ t.first ];
  }

  T & operator() ( const Triple<SSize> & t, const SSize band ) {
    return data( band )[ t.third ][ t.second ][ t.first ];
  }

  const T & operator() ( const Triple<SSize> & t, const SSize band ) const {
    return data( band )[ t.third ][ t.second ][ t.first ];
  }

  T gray( const Triple<SSize> & t );

  T gray( const Pair<SSize> & t );

  T gray( SSize x, SSize y = 0, SSize z = 0 );


  Triple<T> triple( SSize x, SSize y = 0, SSize z = 0 ) const {
    switch ( _bands ) {
    case 1:
      return Triple<T>( _bands_array[0][z][y][x],
          _bands_array[0][z][y][x],
          _bands_array[0][z][y][x] );
      break;
    case 3:
      return Triple<T>( _bands_array[0][z][y][x],
          _bands_array[1][z][y][x],
          _bands_array[2][z][y][x] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(x,y,z): Wrong number of bands ("
                << _bands << ")\n";
      return Triple<T>();
    }
  }

  RefTriple<T> triple( SSize x, SSize y = 0, SSize z = 0 ) {
    switch ( _bands ) {
    case 1:
      return RefTriple<T>( _bands_array[0][z][y][x],
          _bands_array[0][z][y][x],
          _bands_array[0][z][y][x] );
      break;
    case 3:
      return RefTriple<T>( _bands_array[0][z][y][x],
          _bands_array[1][z][y][x],
          _bands_array[2][z][y][x] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(x,y,z): Wrong number of bands ("
                << _bands << ")\n";
      return RefTriple<T>();
    }
  }

  Triple<T> triple( const Pair<SSize> & p ) const {
    switch ( _bands ) {
    case 1:
      return Triple<T>( _bands_array[0][0][p.second][p.first],
          _bands_array[0][0][p.second][p.first],
          _bands_array[0][0][p.second][p.first] );
      break;
    case 3:
      return Triple<T>( _bands_array[0][0][p.second][p.first],
          _bands_array[1][0][p.second][p.first],
          _bands_array[2][0][p.second][p.first] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(Pair): Wrong number of bands ("
                << _bands << ")\n";
      return Triple<T>();
    }
  }

  RefTriple<T> triple( const Pair<SSize> & p ) {
    switch ( _bands ) {
    case 1:
      return RefTriple<T>( _bands_array[0][0][p.second][p.first],
          _bands_array[0][0][p.second][p.first],
          _bands_array[0][0][p.second][p.first] );
      break;
    case 3:
      return RefTriple<T>( _bands_array[0][0][p.second][p.first],
          _bands_array[1][0][p.second][p.first],
          _bands_array[2][0][p.second][p.first] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(Pair): Wrong number of bands ("
                << _bands << ")\n";
      return RefTriple<T>();
    }
  }

  Triple<T> triple( const Triple<SSize> & t ) const {
    switch ( _bands ) {
    case 1:
      return Triple<T>( _bands_array[0][t.third][t.second][t.first],
          _bands_array[0][t.third][t.second][t.first],
          _bands_array[0][t.third][t.second][t.first] );
      break;
    case 3:
      return Triple<T>( _bands_array[0][t.third][t.second][t.first],
          _bands_array[1][t.third][t.second][t.first],
          _bands_array[2][t.third][t.second][t.first] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(): Wrong number of bands ("
                << _bands << ")\n";
      return Triple<T>();
    }
  }

  RefTriple<T> triple( const Triple<SSize> & t ) {
    switch ( _bands ) {
    case 1:
      return RefTriple<T>( _bands_array[0][t.third][t.second][t.first],
          _bands_array[0][t.third][t.second][t.first],
          _bands_array[0][t.third][t.second][t.first] );
      break;
    case 3:
      return RefTriple<T>( _bands_array[0][t.third][t.second][t.first],
          _bands_array[1][t.third][t.second][t.first],
          _bands_array[2][t.third][t.second][t.first] );
      break;
    default:
      std::cerr << "ZZZImage<>::triple(): Wrong number of bands ("
                << _bands << ")\n";
      return RefTriple<T>();
    }
  }


  /**
   * Zeros the data vector of the image.
   */
  void clear();

  /**
   * Returns the dimension of the image.
   * @return The dimension (3d) of the image.
   */
  Dimension3d dimension();

  bool zeroFramed() const {
    return _arrayLayout == ImageProperties::ZeroFramedArrayLayout;
  }

  ImageProperties::DataArrayLayout arrayLayout() const { return _arrayLayout; }

  /**
   * Returns the identifier of the object.
   */
  Typobj poType() const { return (Typobj) Po_type< ZZZImage<T> >::type; }

  /**
   * Returns the identifier of the object as stored in a file.
   */
  Typobj poFileType() const { return _poFileType; }

  /**
   * Set the file's Typeobj (if compatible with the current value type).
   *
   * @param Typeobj
   *
   * @return true if the operation was successfull (compatible types).
   */
  bool poFileType( Typobj );

  /**
   * Returns the colorspace of the volume.
   *
   */
  ColorSpace colorSpace() { return _colorSpace; }

  /**
   * Returns the type name.
   */
  std::string name() const { return TypeName< ZZZImage<T> >::name(); }

  /**
   * Returns the number of columns.
   */
  SSize width() const { return _cols; }

  /**
   * Returns the number of rows.
   */
  SSize height() const { return _rows; }

  /**
   * Returns the number of planes.
   */
  SSize depth() const { return _depth; }

  /**
   * Returns the number of bands.
   */
  SSize bands() const { return _bands; }

  /**
   * Returns the dimension of the image.
   */
  Dimension3d size() const { return Dimension3d( _depth, _rows, _cols ); }

  /**
   * Returns the number of pixels in the image.
   */
  Size vectorSize() const {
    return  static_cast<Size>( _depth ) * _rows * _cols;
  }

  /**
   * Returns the actual size of each band vector.
   * @return The size of a band underlying vector.
   */
  size_t realBandVectorSize() const {
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
      return size_t( _depth + 2 ) *
          size_t( _rows + 2 ) * size_t( _cols + 2 );
    else
      return size_t( _depth ) * size_t( _rows ) * size_t( _cols );
  }

  /**
   * Returns the related vector of properties.
   */
  ImageProperties props() const {
    return ImageProperties( _bands, _cols, _rows, _depth,
                            ( ColorSpace ) 0,
                            0,
                            _depth * _cols * _rows,
                            _arrayLayout );
  }

  Triple<float> gravityCenter();

  Bool zero( const SSize x, const SSize y, const SSize z );

  Bool zero( const Triple<SSize> & t ) {
    return zero( t.first, t.second, t.third );
  }


  bool load( const char * filename, bool multiplexed );
  bool load( ByteInput & in  );
  bool load( std::istream & in  );

  bool save( const char * filename ) const;
  bool save( ByteOutput & out, const char * format ) const;

  bool loadFileBuffer( const unsigned char * data, size_t size );

  bool loadASC( ByteInput & input );
  bool load3DZ( ByteInput & input );
  bool loadVFF( ByteInput & input );
  bool loadPAN( ByteInput & input );
  bool loadVOL( ByteInput & input );
  bool loadRAW( ByteInput & input, bool multiplexed );
  bool loadRAW( std::istream & in, bool multiplexed );

  bool save3DZ( ByteOutput & out ) const;
  bool saveVFF( ByteOutput & out ) const;
  bool savePAN( ByteOutput & out ) const;
  bool saveVOL( ByteOutput & out ) const;
  bool saveNPZ( ByteOutput & out ) const;
  bool saveRAW( ByteOutput & out ) const;
  bool saveASC( ByteOutput & out ) const;
  bool saveCSV( ByteOutput & out ) const;
  bool saveJBM( ByteOutput & out ) const;
  bool saveBMP( ByteOutput & out ) const;

  bool loadRAW( std::istream & file, const Dimension3d & dimension, bool multiplexed );
  bool loadCSV( std::istream & in );
  bool loadXBM( std::istream & file );
  bool loadBMP( std::istream & file );

  void mirror( int axis );
  void rotate( int axis );
  void binarize( UChar value = 255 );
  void binarize( UChar red, UChar green, UChar blue );
  void negative();
  void grayShadeFromHue();
  void grayShadeFromSaturation();
  void toGray();
  void toColor();
  void bitPlane( UChar bit, SSize band );
  void bitPlaneGrayCode( UChar bit, SSize band );
  void quantize( UInt32 cardinality );
  void swapEndianness();

  void hollowOut( int adjacency = ADJ26 );

  void seedFill( SSize x, SSize y, SSize z, UChar value = 255 );
  void seedFillXY( SSize x, SSize y, SSize z, UChar value = 255 );
  void seedFillXZ( SSize x, SSize y, SSize z, UChar value = 255 );
  void seedFillYZ( SSize x, SSize y, SSize z, UChar value = 255 );


  void extract( ZZZImage<T> & dst,
                SSize x, SSize y, SSize z,
                SSize width, SSize height, SSize depth );


  ZZZImage<T> * sub( SSize x, SSize y, SSize z,
                     SSize width, SSize height, SSize depth );

  ZZZImage<T> * slice( SSize x, SSize y, SSize z );

  void insert( SSize x, SSize y, SSize z, AbstractImage * image );

  void insertSlice( SSize x, SSize y, SSize z,
                    AbstractImage * image );

  ZZZImage<T> * levelMap( SSize depth );


  T max( SSize band = -1 );
  T min( SSize band = -1 );
  Pair<T> range( SSize band = -1 );

  /**
   * Number of nonzero voxels.
   *
   * @return
   */
  Size nonZeroVoxels();

  /**
   * Fill the inside of a surface (works fine only if the surface is closed).
   *
   * @param value
   */
  void surfaceFill(UChar value = 255);
  void fillForHoleClosing();
  void addBoundaries( UChar value, UChar thickness );


  /*   void threshold( Triple<T> min, Triple<T> max,  */
  /* 		  std::vector< Triple<T> > & removedValues, */
  /* 		  ZZZImage<UChar> & result ); */

  /**
   * Mask the image by zeroing the voxel whose value is 0
   * in the given (other) image.
   * Warning: no size compatibility check.
   *
   * @param other
   *
   * @return
   */
  ZZZImage< T > & operator&=( const ZZZImage< T > & other );

  ZZZImage< T > & operator|=( const ZZZImage< T > & other );

  ZZZImage< T > operator|( const ZZZImage< T > & other );

  ZZZImage< T > & operator-=( const ZZZImage< T > & other );

  ZZZImage< T > & mask( const ZZZImage< UChar > & mask );

  bool isBorder( SSize x, SSize y, SSize z, char adjacency ) const;

  void getFramedBitMask( ZZZImage<bool> & ) const;
  void applyFramedBitMask( const ZZZImage<bool> & );

  bool setValue( Triple<SSize> voxel,
                 UChar red, UChar green, UChar blue );

  UChar orientation( SSize x, SSize y, SSize z ) const;
  UChar orientation( const Triple<SSize> & voxel ) const;
  Config config( SSize x, SSize y, SSize z ) const;
  Config config( const Triple<SSize> & voxel ) const;
  Config config2D( SSize x, SSize y, SSize z ) const;
  void configExtended( Config & conf, Config & p,
                       SSize x, SSize y, SSize z ) const;
  void configExtended( Config & conf, Config & p,
                       const Triple<SSize> & voxel ) const;

  virtual void getColor( SSize x, SSize y, SSize z, UChar & red, UChar & green, UChar & blue);
  virtual void getColor( const Triple<SSize> & , UChar & red, UChar & green, UChar & blue);
  virtual void getGrayLevel( SSize x, SSize y, SSize z, UChar & gray );
  virtual void getGrayLevel( const Triple<SSize> & , UChar & gray );

  void setColorMode( ColorMode );

  virtual Triple<UChar> getColorFromLUT( const Triple<SSize> & voxel,
                                         const std::vector< Triple<UChar> > & lut ) const;


  void mark6border( UChar value );
  void mark6Pborder( UChar value );
  void mark18border( UChar value );
  void mark26border( UChar value );

  void boundingBox( SSize & xMin, SSize & yMin, SSize & zMin,
                    SSize & xMax, SSize & yMax, SSize & zMax ) const;

  void fitBoundingBox( SSize margin = 0 );

  void zeroPlane( SSize x, SSize y, SSize z );

  void subSample( UChar minCount = 4 );
  void scale( UChar direction, UChar factor = 2 );
  void extrude( SSize depth );
  void spongify();

  bool merge( ZZZImage<T> & other, int axis );
  bool merge( AbstractImage & other, int axis );

  /**
   * Fills an histogram from he voxel values.
   *
   * @param h The histogram with given size to be filled.
   * @param minxVal
   * @param maxVal
   * @param band
   */
  void histogram( std::vector<Size> & h,
                  T & minxVal, T & maxVal, SSize band = 0 );


  /**
   *
   *
   * @param axis The orthogonal axis (to the plane). 0 is Ox, 1 is Oy, 2 is Oz
   * @param n The plane coordinate among the given axis
   */
  void trim( int axis, SSize n );

protected:


  bool loadData( ByteInput & input, bool multiplexed );

  bool loadCharData( ByteInput & input );

  /**
   * Load RLE compressed data from an input stream.
   *
   * @param file
   *
   * @return
   */
  bool loadDataRLE( ByteInput & input );


  bool loadDataASC( ByteInput & input );


  bool loadPandoreData( ByteInput & input, bool region, bool swapBytes );

  /**
   * Revers the byte order of all the values in the array.
   *
   */
  void reverseDataBytes();

  /**
   * Writes a pandore header for the image in a file.
   *
   * @param out
   * @param type
   */
  bool savePandoreHeader( ByteOutput & out, Typobj type ) const;

  /**
   * Loads attribute values from the given file.
   * Allocates therefrom the related data.
   * @param file the file where to read attributes values.
   * @param inversionMode Whether or note bytes should be swapped.
   */
  std::istream & loadPandoreAttributes( std::istream  &file, bool inversionMode );

  /**
   * Saves the current attribute values.
   * @param file the file.
   */
  ByteOutput & savePandoreAttributes( ByteOutput & out ) const;

  /**
   * Saves data in the given file.
   * @param file the file where to save data.
   */
  ByteOutput & savePandoreData( ByteOutput & out ) const;

  /**
   * Saves the image data as an image of regions.
   * The value type actually saved depends on the
   * maximal value found in the volume or image.
   *
   * @param file the file where to save data.
   * @param maximalValue The maximal label value.
   */
  ByteOutput & saveRegionData( ByteOutput & out,
                               UInt32 maximalValue ) const;

  /**
   * Load UInt32 data using the minimum of 1, 2 or 4 bytes according
   * to a maximum value specified.
   *
   * @param file
   * @param swapBytes Whether or not bytes should be swapped (but not
   *                  the data buffer).
   * @param maximalValue The maximal value of the labels in a region image.
   *
   * @return The provided input stream.
   */
  bool loadRegionData( ByteInput & input,
                       bool swapBytes,
                       UInt32 maximalValue );

  Triple<float> _aspect;
  value_type ****_bands_array;
  SSize _bands;			/**< Number of bands. */
  SSize _cols;			/**< Number of columns. */
  SSize _rows;			/**< Number of rows. */
  SSize _depth;			/**< Number of planes. */
  ImageProperties::DataArrayLayout _arrayLayout; /**< volume surronded 0s ?  */
  Typobj _poFileType;
  ColorSpace _colorSpace;
  ColorMode _colorMode;
};

int volFileValueType( const char * filename );

/**
 * Returns the value type of a pandore object file.
 *
 * @param filename
 *
 * @return
 */
int pobjectFileValueType( const char * filename );


template<>
struct Po_type< ZZZImage<UChar> > { enum { type = Po_Imx3duc }; };

template<>
struct TypeName< ZZZImage<UChar> > {
  static std::string name() { return "Imx3duc"; }
};

template<>
struct Po_type< ZZZImage<Short> > { enum { type = Po_Imx3dss }; };

template<>
struct TypeName< ZZZImage<Short> > {
  static std::string name() { return "Imx3dss"; }
};

template<>
struct Po_type< ZZZImage<UShort> > { enum { type = Po_Imx3dus }; };

template<>
struct TypeName< ZZZImage<UShort> > {
  static std::string name() { return "Imx3dus"; }
};

template<>
struct Po_type< ZZZImage<Int32> > { enum { type = Po_Imx3dsl }; };

template<>
struct TypeName< ZZZImage<Int32> > {
  static std::string name() { return "Imx3dsl"; }
};

template<>
struct Po_type< ZZZImage<UInt32> > { enum { type = Po4_Imx3dul }; };

template<>
struct TypeName< ZZZImage<UInt32> > {
  static std::string name() { return "Imx3dul"; }
};

template<>
struct Po_type< ZZZImage<Float> > { enum { type = Po_Imx3dsf }; };

template<>
struct TypeName< ZZZImage<Float> > {
  static std::string name() { return "Imx3dsf"; }
};

template<>
struct Po_type< ZZZImage<Double> > { enum { type = Po_Imx3dsd }; };

template<>
struct TypeName< ZZZImage<Double> > {
  static std::string name() { return "Imx3dsd"; }
};

#endif // _ZZZIMAGE_H_




