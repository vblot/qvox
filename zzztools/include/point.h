/* -*- mode: c++ -*-
 * @(#)point.h			PANDORE		2001-04-03
 *
 * PANTHEON Project
 *
 * GREYC IMAGE
 * 6 Boulevard Mar�chal Juin
 * F-14050 Caen Cedex France
 *
 * This file is free software. You can use it, distribute it
 * and/or modify it. However, the entire risk to the quality
 * and performance of this program is with you.
 *
 * Please email any bugs, comments, and/or additions to this file to:
 * Regis.Clouard@greyc.ensicaen.fr
 * http://www.greyc.ensicaen.fr/~regis
 */

/*
 * (C)R�gis Clouard - 1999-10-08
 * (C)Francois Angot - 1999-10-08
 * (R)Alexandre Duret-Lutz. 1999-10-08
 * (R)R�gis Clouard - 2001-04-10 (version 3.00)
 * (R)R�gis Clouard - 2004-06-30 (Set as a Pobject, Add operators *=, /=...)
 * (R)S�bastien Fourey - 2005 ( Flight-weigthted version )
 */

/*
 *                       -- IMPORTANT NOTE --
 *
 *  This file is a modified version of the pandore original files.
 *  It is a lightenned version, specially rewritten for the QVox
 *  visualization tool.
 *  (C)S�bastien Fourey - GREYC ENSICAEN - France
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _POINT_H_
#define _POINT_H_

#include <iostream>
#include <fstream>
#include <dimension.h>

class Dimension1d;
class Dimension2d;
class Dimension3d;
class Point1d;
class Point2d;
class Point3d;


template<>
struct TypeName<Point1d> {
  static std::string name() { return "Point1d"; }
};

template<>
struct TypeName<Point2d> {
  static std::string name() { return "Point2d";}
};

template<>
struct TypeName<Point3d> {
  static std::string name() { return "Point3d";}
};

/**
 * A point representing a location in a given coordinate space,
 * specified in integer precision.
 */
class Point {

 public:

  Point() { }
  virtual ~Point() { };
  /**
   * Returns the identifier of the object.
   */
  Typobj type() const { return Po_Point2d; }

  virtual Point * clone() const  = 0;

  /**
   * Returns the type name.
   */
  std::string name() const { return TypeName< Point2d >::name();}

  std::istream & loadAttributes( std::istream & in ) { return in; }
  std::ostream & saveAttributes( std::ostream & out ) const { return out; }
};

/**
 * A point representing a 1D location in (x) coordinate space,
 * specified in integer precision.
 */
class Point1d : public Point {
 public:
  Int32 x;			/**< The x coordinate. */

  /**
   * Constructs and initializes a point at the origin (0)
   * of the coordinate space.
   */
  Point1d():x( 0 ) { }

  /**
   * Constructs and initializes a point at the specified (x)
   * location in the coordinate space.
   * @param i	the x coordinate
   */
  Point1d( Int32 i ):x( i ) { }

  /**
   * Constructs and initializes a point with the same location
   * as the specified Point object.
   * @param p	a point
   */
  Point1d( const Point1d & p ):Point(),x( p.x ) { }

  /**
   * Constructs and initializes a point with the same location
   * as the specified Dimension object.
   * @param d	a dimension.
   */
  Point1d( const Dimension1d & d ):x( d.w ) { }

  /**
   * Sets the coordinates with the specifiate coordinates.
   * @param pt	the specified coordinates.
   */
  const Point1d & operator=( const Point1d & p ) {
    x = p.x;
    return ( p );
  }

  /**
   * Checks if the coordinates are equals to the specified coordinates.
   * @param pt	the reference coordinates.
   * @return true if coordinates are equals.
   */
  bool operator==( const Point1d & p ) const {
    return p.x == x;
  }

  /**
   * Checks if the coordinates are differents to the specified coordinates.
   * @param pt	the reference coordinates.
   * @return true if coordinates are differents.
   */
  bool operator!=( const Point1d & p ) const {
    return !( p == *this );
  }

  /**
   * Sets the coordinates to the addition with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d & operator+=( const Point1d & p ) {
    x += p.x;
    return *this;
  }

  /**
   * Sets the coordinates to the substraction with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */

  Point1d & operator-=( const Point1d & p ) {
    x -= p.x;
    return *this;
  }

  /**
   * Sets the coordinates to the multiplication with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d & operator*=( const Point1d & p ) {
    x *= p.x;
    return *this;
  }

  /**
   * Sets the coordinates to the division with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d & operator/=( const Point1d & p ) {
    x /= p.x;
    return *this;
  }

  /**
   * Adds the current cordinates with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d operator+( const Point1d & p ) const {
    return Point1d( x + p.x );
  }

  /**
   * Substracts the current cordinates with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d operator-( const Point1d & p ) const {
    return Point1d( x - p.x );
  }

  /**
   * Multiplies the current cordinates with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d operator*( const Point1d & p ) const {
    return Point1d( x * p.x );
  }

  /**
   * Divides the current cordinates with the specified coordinates.
   * @param pt	the specified coordinates.
   * @return the new coordinates.
   */
  Point1d operator/( const Point1d & p ) const {
    return Point1d( x / p.x );
  }

  /**
   * Creates and returns a distinct copy of this object.
   */
  Point * clone() const {
    return new Point1d( x );
  }


  /**
   * Loads data from the given file.
   * @param file the file where to read data.
   */
  std::istream & loadData( std::istream & file, ProgressFunction = 0 ) {
    return file.read( reinterpret_cast<char*>( &x ), sizeof( x ) );
  }

  /**
   * Saves data in the given file.
   * @param file the file where to save data.
   */
  std::ostream & saveData( std::ostream & file, ProgressFunction = 0 ) const {
    return file.write( reinterpret_cast<const char*>( &x ), sizeof( x ) );
  }
};

/**
 * A point representing a 2D location in (x, y) coordinate space,
 * specified in integer precision.
 */
class Point2d : public Point {
 public:
  Int32 x;			/**< The x coordinate. */
  Int32 y;			/**< The y coordinate. */

  /**
   * Constructs and initializes a point at the origin (0,0)
   * of the coordinate space.
   */
  Point2d():x( 0 ), y( 0 ) { }

  /**
   * Constructs and initializes a point at the specified (i,i)
   * location in the coordinate space.
   * @param i	the x and y coordinate
   */
  Point2d( Int32 i ):x( i ), y( i ) { }

  /**
   * Constructs and initializes a point at the specified (i,j)
   * location in the coordinate space.
   * @param i	the x coordinate
   * @param j	the y coordinate
   */
  Point2d( Int32 i, Int32 j ):x( j ), y( i ) { }

  /**
   * Constructs and initializes a point with the same location
   * as the specified Point object.
   * @param p	a point.
   */
  Point2d( const Point2d & p ):Point(),x( p.x ), y( p.y ) { }

  /**
   * Constructs and initializes a point with the same location
   * as the specified Dimension object.
   * @param p	a dimension.
   */
  Point2d( const Dimension2d & p ):x( p.w ), y( p.h ) { }

  /**
   * Sets the coordinates with the specifiate coordinates.
   * @param pt	the specified coordinates.
   */
  const Point2d & operator=( const Point2d & p ) {
    x = p.x;
    y = p.y;
    return ( *this );
  }

  /**
   * Checks if the coordinates are equals to the specified coordinates.
   * @param p the reference coordinates.
   * @return true if coordinates are equals.
   */
  bool operator==( const Point2d & p ) const {
    return p.x == x && p.y == y;
  }

  /**
   * Checks if the coordinates are differents to the specified coordinates.
   * @param p	the reference coordinates.
   * @return true if coordinates are differents.
   */
  bool operator!=( const Point2d & p ) const {
    return !( p == *this );
  }

  /**
   * Sets the coordinates to the addition with the specified coordinates.
   * @param p	the specified coordinates.
   * @return the new coordinates.
   */
  Point2d & operator+=( const Point2d & p ) {
    x += p.x; y += p.y;
    return *this;
  }

  /**
    * Sets the coordinates to the substraction with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
  Point2d & operator-=( const Point2d & p ) {
    x -= p.x; y -= p.y;
    return *this;
  }

  /**
   * Sets the coordinates to the division with the specified coordinates.
   * @param p	the specified coordinates.
   * @returns the new coordinates.
   */
   Point2d & operator/=( const Point2d & p ) {
      x /= p.x; y /= p.y;
      return *this;
   }

   /**
    * Sets the coordinates to the multiplication with the specified
    * coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point2d & operator*=( const Point2d & p ) {
     x *= p.x; y *= p.y;
     return *this;
   }

   /**
    * Adds the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point2d operator+( const Point2d & p ) const {
     return Point2d( y + p.y, x + p.x );
   }

   /**
    * Substracts the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point2d operator-( const Point2d & p ) const {
     return Point2d( y - p.y, x - p.x );
   }

   /**
    * Multiplies the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point2d operator*( const Point2d & p ) const {
     return Point2d( y * p.y, x * p.x );
   }

   /**
    * Divides the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point2d operator/( const Point2d & p ) const {
     return Point2d( y / p.y, x / p.x );
   }

   /**
    * Creates and returns a distinct copy of this object.
    */
   Point *clone() const {
     return new Point2d( y, x );
   }

   /**
    * Loads data from the given file.
    * @param file the file where to read data.
    */
   std::istream & loadData( std::istream & file, ProgressFunction = 0 ) {
     if ( ! file.read( (char*) &y, sizeof( y ) ) ) return file;
     if ( ! file.read( (char*) &x, sizeof( y ) ) ) return file;
     return file;
   }

   /**
    * Saves data in the given file.
    * @param file the file where to save data.
    */
   std::ostream & saveData( std::ostream & file, ProgressFunction = 0 ) const {
     if ( ! file.write( (const char*) &y, sizeof( y ) ) )  return file;
     if ( ! file.write( (const char*) &x, sizeof( y ) ) )  return file;
     return file;
   }
};

/**
 * A point representing a 3D location in (x, y, z) coordinate space,
 * specified in integer precision.
 */
class Point3d : public Point {
 public:
   Int32 z;			/**< The z coordinate */
   Int32 y;			/**< The y coordinate */
   Int32 x;			/**< The x coordinate */

   /**
    * Constructs and initializes a point at the origin (0,0,0)
    * of the coordinate space.
    */
   Point3d():z( 0 ), y( 0 ), x( 0 ) { }

   /**
    * Constructs and initializes a point at the specified (i,i,i)
    * location in the coordinate space.
    * @param i	the x, y and z coordinate
    */
   Point3d( Int32 i ):z( i ), y( i ), x( i ) { }

   /**
    * Constructs and initializes a point at the specified (z,y,x)
    * location in the coordinate space.
    * @param x	the x coordinate
    * @param y	the y coordinate
    * @param z	the z coordinate
    */
   Point3d( Int32 z, Int32 y, Int32 x):z( z ), y( y ), x( x ) { }

   /**
    * Constructs and initializes a point with the same location
    * as the specified Dimenson object.
    * @param p	a dimension.
    */
   Point3d( const Dimension3d & d ):z( d.d ), y( d.h ), x( d.w ) { }

   /**
    * Constructs and initializes a point with the same location
    * as the specified Point object.
    * @param p a point.
    */
   Point3d( const Point3d & p ):Point(),z( p.z ), y( p.y ), x( p.x ) { }

   /**
    * Sets the coordinates with the specifiate coordinates.
    * @param p the specified coordinates.
    */
   Point3d operator=( Point3d p ) {
      x = p.x; y = p.y; z = p.z;
      return p;
   }

   /**
    * Checks if the coordinates are equal to the specified coordinates.
    * @param p	the reference coordinates.
    * @return true if coordinates are equal.
    */
   bool operator==( const Point3d & p ) const {
     return p.x == x && p.y == y && p.z == z;
   }

   /**
    * Checks if the coordinates are different to the specified coordinates.
    * @param p the reference coordinates.
    * @return true if coordinates are different.
    */
   bool operator!=( const Point3d & p ) const {
      return !( p == *this );
   }

   /**
    * Sets the coordinates to the addition with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d & operator+=( const Point3d & p ) {
     x += p.x; y += p.y; z += p.z;
     return *this;
   }

   /**
    * Sets the coordinates to the substraction with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d & operator-=( const Point3d & p ) {
     x -= p.x; y -= p.y; x -= p.x;
     return *this;
   }

   /**
    * Sets the coordinates to the multiplication with the
    * specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d & operator*=( const Point3d & p ) {
      x *= p.x; y *= p.y; x *= p.x;
      return *this;
   }

   /**
    * Sets the coordinates to the division with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d & operator/=( const Point3d & p ) {
     x /= p.x; y /= p.y; x /= p.x;
     return *this;
   }

   /**
    * Adds the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d operator+( const Point3d & p ) const {
      return Point3d( z + p.z, y + p.y, x + p.x );
   }

   /**
    * Substracts the current cordinates with the specified coordinates.
    * @param p the specified coordinates.
    * @return the new coordinates.
    */
   Point3d operator-( const Point3d & p ) const {
      return Point3d( z - p.z, y - p.y, x - p.x );
   }

   /**
    * Multiplies the current cordinates with the specified coordinates.
    * @param p the specified coordinates.
    * @return the new coordinates.
    */
   Point3d operator*( const Point3d & p ) const {
      return Point3d( z * p.z, y * p.y, x * p.x );
   }

   /**
    * Divides the current cordinates with the specified coordinates.
    * @param p	the specified coordinates.
    * @return the new coordinates.
    */
   Point3d operator/( const Point3d & p ) const {
      return Point3d( z / p.z, y / p.y, x / p.x );
   }

   /**
    * Creates and returns a distinct copy of this object.
    */
   Point *clone() const {
     return new Point3d( z, y, x );
   }

   /**
    * Loads data from the given file.
    * @param file the file where to read data.
    */
  std::istream & loadData( std::istream & file, ProgressFunction = 0 )
  {
    if ( ! file.read( (char*) &z, sizeof( z ) ) ) return file;
    if ( ! file.read( (char*) &y, sizeof( y ) ) ) return file;
    if ( ! file.read( (char*) &x, sizeof( x ) ) ) return file;
    return file;
  }

   /**
    * Saves data in the given file.
    * @param file the file where to save data.
    */
   std::ostream & saveData( std::ostream & file, ProgressFunction = 0 ) const
     {
       if ( file.write( (const char*) &z, sizeof( z ) ) ) return file;
       if ( file.write( (const char*) &y, sizeof( y ) ) ) return file;
       if ( file.write( (const char*) &x, sizeof( x ) ) ) return file;
       return file;
     }
};

inline std::istream &
operator>>( std::istream & in, Point3d & p )
{ return p.loadData( in ); }

inline std::istream &
operator>>( std::istream & in, Point2d & p )
{ return p.loadData( in ); }

inline std::istream &
operator>>( std::istream & in, Point1d & p )
{ return p.loadData( in ); }

inline std::ostream &
operator<<( std::ostream & out, Point3d & p )
{ return p.saveData( out ); }

inline std::ostream &
operator<<( std::ostream & out, Point2d & p )
{ return p.saveData( out ); }

inline std::ostream &
operator<<( std::ostream & out, Point1d & p )
{ return p.saveData( out ); }


#endif  // _POINT_H_
