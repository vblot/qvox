/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ImageHeader.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 *
 * @brief  Declaration of the class ImageHeader
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _IMAGEHEADER_H_
#define _IMAGEHEADER_H_

#include <iostream>
#include "ByteInput.h"
#include "tools.h"
#include "panfile.h"
#include "Triple.h"

/**
 *  The ImageHeader class.
 */
class ImageHeader {

public:

   enum FileType { Type3DZ=0, TypePAN, TypeVFF, TypeVOL, TypeASC, TypeNPZ,
       UNKNOWN };

  /**
   * Constructor with no arguments.
   */
   ImageHeader( ByteInput & in );
   FileType fileType() const;
   Endianness endian() const;

   std::ostream & flush( std::ostream & out ) const;

   SSize width() { return _width; }
   SSize height() { return _height; }
   SSize depth() { return _depth; }
   SSize bands() { return _bands; }
   Triple<float> aspect() { return _aspect; }
   FileType fileType() { return _fileType; }
   Typobj poType() { return _poType; }
   ColorSpace colorSpace() { return _colorSpace; }
   Endianness endian() { return _endian; }
   void endian(Endianness e) { _endian = e; }

   bool isOk() { return _isOk; }

 protected:

   bool read();
   bool readVFF(char* header, size_t & header_size);
   bool read3DZ(char* header, size_t & header_size);
   bool readPAN(char* header, size_t & header_size);
   bool readVOL(char* header, size_t & header_size);
   bool readASC(char* header, size_t & header_size);
   bool readNPZ(char* header, size_t & header_size);

 private:

   ByteInput & _in;
   FileType _fileType;
   Typobj _poType;
   ColorSpace _colorSpace;
   Endianness _endian;
   SSize _width;
   SSize _height;
   SSize _depth;
   SSize _bands;
   Triple<float> _aspect;
   bool _isOk;
   char _magic[20];
};

std::ostream & operator<<( std::ostream & out, const ImageHeader & header );

#endif // _IMAGEHEADER_H_
