/** -*- mode: c++ -*-
 * @file   tools3d.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:41 2005
 *
 * @brief  ZZZImage tools.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _TOOLS3D_H_
#define _TOOLS3D_H_

#include <string.h>

#include <cmath>
#include <algorithm>
#include <limits>

#include "zzzimage.h"
#include "zzzimage_bool.h"
#include "config.h"

/**
 *
 *
 * @param image
 * @param a
 * @param b
 * @param c
 * @param t
 * @param value
 */
template< typename T >
void zzzDrawPlane( ZZZImage<T> & image, float a, float b, float c,
                   float t, T value );
/**
 *
 *
 * @param result
 * @param image
 * @param axis
 * @param quater
 */
template < typename T >
void zzzRotate( ZZZImage<T> & result, const ZZZImage<T> & image,
                int axis, int quater);

/**
 *
 *
 * @param result
 * @param image
 * @param rho
 * @param theta
 * @param phi
 */
template < typename T >
void zzzRotate( ZZZImage<T> & result, const ZZZImage<T> & image,
                float rho, float theta, float phi );


/**
 *
 *
 * @param result
 * @param image
 * @param axis
 * @param rho
 * @param xcenter
 * @param ycenter
 * @param zcenter
 */
template < typename T >
void zzzRotate( ZZZImage<T> & result, const ZZZImage<T> & image,
                int axis, float rho,
                SSize xcenter, SSize ycenter, SSize zcenter);


/**
 *
 *
 * @param result
 * @param image
 *
 * @return
 */
template < typename T >
bool zzzUnion( ZZZImage<T> & result, const ZZZImage<T> & image );


/**
 *
 *
 * @param result
 * @param image
 * @param dx
 * @param dy
 * @param dz
 *
 * @return
 */
template < typename T >
bool zzzInsert( ZZZImage<T> & result, const ZZZImage<T> & image,
                SSize dx, SSize dy, SSize dz );


/**
 *
 *
 * @param image
 * @param min
 * @param max
 *
 */
template < typename T >
void zzzRange( ZZZImage<T> & image, T & min, T & max );

/**
 *
 *
 * @param image
 * @param before
 * @param after
 *
 * @return
 */
template < typename T >
Size zzzReplace( ZZZImage<T> & image, T before, T after );

/**
 *
 *
 * @param image
 * @param x_min
 * @param y_min
 * @param z_min
 * @param x_max
 * @param y_max
 * @param z_max
 *
 * @return
 */
template < typename T >
void zzzBoundingBox( ZZZImage<T> & image,
                     SSize & x_min, SSize & y_min, SSize & z_min,
                     SSize & x_max, SSize & y_max, SSize & z_max);

/**
 *
 *
 * @param image
 * @param min
 * @param max
 * @param value
 *
 * @return
 */
template < typename T >
Size zzzTreshold( ZZZImage<T> & image, T min, T max, T value );

/**
 * Spongify the volume.
 *
 * @param image
 *
 * @return
 */
template < typename T >
ZZZImage<T> zzzSponge( const ZZZImage<T> & image );

/**
 *
 *
 * @param image
 *
 * @return
 */
template < typename T >
void zzzDilate6( ZZZImage<T> & image );

/**
 *
 *
 * @param image
 *
 * @return
 */
template < typename T >
void zzzDilate18( ZZZImage<T> & image );

/**
 *
 *
 * @param image
 *
 * @return
 */
template < typename T >
void zzzDilate26( ZZZImage<T> & image );

/**
 *
 *
 * @param image
 *
 * @return
 */
template < typename T >
void zzzClearBorder( ZZZImage<T> & image );


template < typename T >
void zzzAddNoise6( ZZZImage<T> & image, float ratio );

template < typename T >
void zzzAddNoise18( ZZZImage<T> & image, float ratio );

template< typename T >
void threshold( ZZZImage<UChar> & dst,
                ZZZImage<T> & src,
                Triple<T> min,
                Triple<T> max,
                std::vector< Triple<T> > & removedValues );


ZZZImage<Int32>
filterCascade( const ZZZImage<UChar> & image, int finalSize );

template < typename T >
void fillTriangle( ZZZImage<T> & image,
                   const Triple<SSize> & p1, const Triple<SSize> & p2, const Triple<SSize> & p3,
                   Triple<UChar> color );

#endif // _TOOLS3D_H_

