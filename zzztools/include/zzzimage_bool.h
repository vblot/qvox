/** -*- mode: c++ -*-
 * @file   zzzimage_bool.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:16:35 2005
 *
 * @brief  ZZZImage<Bool> volume image of booleans.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _ZZZIMAGE_BOOL_H_
#define _ZZZIMAGE_BOOL_H_

#include <exception>
#include <typeinfo>

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <math.h>
#include <cstring>
#include <iomanip>
#include <globals.h>
#include <config.h>
#include <limits>
#include <vector>

#include "Pair.h"
#include "Triple.h"
#include "RefTriple.h"
#include "BitRef.h"

#include "panfile.h"
#include "dimension.h"
#include "point.h"
#include "ByteInput.h"
#include "ByteOutput.h"

#include "zzzimage.h"

template<>
struct Po_type< ZZZImage<Bool> > { enum { type = Po_Imx3db }; };

template<>
struct TypeName< ZZZImage<Bool> > {
   static std::string name() { return "Imx3db"; }
};

template<>
class ZZZImage<Bool> : public AbstractImage {

public:
   typedef Bool value_type;         /**< The type of the data. */

   ZZZImage( SSize width = 0, SSize height = 1, SSize depth = 1,
             SSize bands = 1 )
      : AbstractImage(), _bands_array( 0 ), _bands( 0 ),
        _cols( 0 ), _rows( 0 ), _depth( 0 ),
        _colorMode( CastColorMode ) {
      TRACE << "++ZZZImage<" << TypeName<Bool>::name() << ">()\n";
      if ( !width || !height || !depth || !bands ) {
         width = height = depth = bands = 0;
      }
      this->_poFileType = (Typobj) Po_type< ZZZImage<Bool> >::type;
      alloc( width, height, depth, bands, true );
   }

   ZZZImage( const ZZZImage & other )
      : AbstractImage(), _bands_array( 0 ), _bands( 0 ),
        _cols( 0 ), _rows( 0 ), _depth( 0 ),
        _colorMode( CastColorMode ) {
      TRACE << "rrZZZImage<" << TypeName<Bool>::name() << ">()\n";
      this->_poFileType = (Typobj) Po_type< ZZZImage<Bool> >::type;
      alloc( other._cols, other._rows, other._depth, other._bands, false );
      (*this) = other;
   }

   /**
   * Creates a new image with the specified dimension.
   * Allocates therefrom the related data.
   * @param bands the number of bands of the image.
   * @param dim	the dimension of the image.
   */
   ZZZImage( SSize bands,
             const Dimension3d & dim )
      : AbstractImage(), _bands_array( 0 ),
        _bands( 0 ), _cols( 0 ), _rows( 0 ), _depth( 0 ),
        _colorMode( CastColorMode ) {
      TRACE << "++ZZZImage<" << TypeName<Bool>::name() << ">()\n";
      this->_poFileType = (Typobj) Po_type< ZZZImage<Bool> >::type;
      alloc( dim, bands, true );
   }

   /**
    * Creates a new image with the specified properties.
    * Allocates therefrom the related data.
    * @param props	the properties.
    */
   ZZZImage( const ImageProperties & props )
      : AbstractImage(), _bands_array( 0 ),
        _bands( 0 ), _cols( 0 ), _rows( 0 ), _depth( 0 ),
        _colorMode( CastColorMode ) {
      TRACE << "++ZZZImage<" << TypeName<Bool>::name() << ">()\n";
      this->_poFileType = (Typobj) Po_type< ZZZImage<Bool> >::type;
      alloc( props.cols, props.rows, props.depth, props.bands, true );
   }

   ~ZZZImage() {
      TRACE << "--ZZZImage<" << TypeName<Bool>::name() << ">()\n";
      free();
   }

   void insert( const ZZZImage<Bool> & other, SSize x = 0, SSize y = 0, SSize z = 0 );

   void free();

   void accept( AbstractImageVisitor & visitor );

   static const ZZZImage<Bool> null; /**< An empty image. */

   /**
   * Allocates the image data from the specified width, height,
   * depth, and number of bands.
   * @param width	the width of the image.
   * @param height	the height of the image.
   * @param depth	the depth of the image.
   * @param bands	the number of bands of the image.
   */
   bool alloc( const SSize width, const SSize height, const SSize depth,
               const SSize bands,
               const bool clear = true );

   /**
   * Allocates the image data from the specified properties.
   * @param props	the properties of the image.
   */
   bool alloc( const ImageProperties & props, const bool clear  ) {
      return alloc( props.cols, props.rows, props.depth, props.bands, clear );
   }

   /**
   * Allocates the image data from the specified bands number and dimension.
   * @param bands	the number of bands of the image.
   * @param dim	the dimension of the image.
   */
   bool alloc( const Dimension3d & dim, const SSize bands, const bool clear ) {
      return alloc( dim.w, dim.h, dim.d, bands, clear );
   }

   /**
   * Allocates the (2D) image data from the specified bands number
   * and dimension.
   * @param dim the dimension of the image.
   * @param bands the number of bands of the image.
   */
   bool alloc( const Dimension2d & dim,
               const SSize bands,
               const bool clear ) {
      return alloc( dim.w, dim.h, 1, bands, clear );
   }

   /**
   * Allocates the (1D) image data from the specified bands number
   * and dimension.
   * @param dim the dimension of the image.
   * @param bands the number of bands of the image.
   */
   bool alloc( const Dimension1d & dim, const SSize bands, const bool clear ) {
      return alloc( dim.w, 1, 1, bands, clear );
   }

   /**
   * Returns the specified band of the image data as a unique vector
   * @param band	the band number.
   */
   UInt * dataVector( SSize band ) const {
      std::cerr << "ZZZImage<Bool>::dataVector(): Should never be called!\n";
      throw -1;
      return _bands_array[ band ];
   }

   /**
   * Returns a pointer to the actual data array of
   * a given band (default is band 0).
   * If the band is out of range, the first band's matrix is returned.
   * (Therefore, gray levels images may be seen has 3 equal bands images.)
   *
   * @param band The number of the wanted band.
   * @return A pointer to the 3D data array.
   */
   UInt * data( SSize band = 0 ) const {
      if ( !_bands ) return 0;
      if ( band >= _bands ) return _bands_array[ 0 ];
      return _bands_array[ band ];
   }

   /**
   * User defined conversion from an image to a boolean.
   *
   * @return true if the image has a real size (not zero).
   */
   bool isEmpty() const;

   /**
   * Returns the specified band (a 3D matrix) of the image.
   * If the band is out of range, the first band's matrix is returned.
   * (Therefore, gray levels images may be seen has 3 equal bands images.)
   *
   * @param band the band number.
   */
   UInt * operator[] ( SSize band ) {
      if ( band >= _bands )
         return _bands_array[ 0 ];
      return _bands_array[ band ];
   }

   /**
    * Returns the specified band (a 3D matrix) of the image.
    * If the band is out of range, the first band's matrix is returned.
    * (Therefore, gray levels images may be seen has 3 equal bands images.)
    *
    * @param band the band number.
    */
   const UInt * operator[] ( SSize band ) const {
      if ( band >= _bands )
         return _bands_array[ 0 ];
      return _bands_array[ band ];
   }

   /**
   * Creates and returns a copy of this object.
   */
   ZZZImage<Bool> * clone() const;

   /**
   * Creates and returns a copy of this object with given value type.
   */
   AbstractImage * clone( int valueType ) const;

   /**
   * Swaps data with another volume.
   *
   * @param other A volume from which data must be swapped.
   */
   void swapData( ZZZImage<Bool> & other ) {
      swapValues( _bands_array, other._bands_array );
      swapValues( _bands, other._bands );
      swapValues( _cols, other._cols );
      swapValues( _rows, other._rows );
      swapValues( _depth, other._depth );
      swapValues( _aspect, other._aspect );
   };

   /**
   * Sets all pixels with the given value.
   * @param val	the given value.
   * @return the image.
   */
   ZZZImage<Bool> & operator=( const Bool val );

   UInt & word( const SSize & x, const SSize  & y, const SSize & z, const SSize & band,
                UInt & mask ) {
      const SSize offset = (z*_cols*_rows + y*_cols + x);
      mask = 1ul << (offset & 0x1Ful);
      return _bands_array[ band ][ offset>>5 ];
   }

   UInt & word( const Triple<SSize> & t, const SSize & band, UInt & mask ) {
      const SSize offset = (t.third*_cols*_rows + t.second*_cols + t.first);
      mask = 1ul << (offset & 0x1Ful);
      return _bands_array[ band ][ offset>>5 ];
   }

   UInt & word( const SSize & index, const SSize & band, UInt & mask ) {
      mask = 1ul << (index & 0x1Ful);
      return _bands_array[ band ][ index>>5 ];
   }

   Bool bit( const SSize & x, const SSize & y = 0, const SSize & z = 0, const SSize & band = 0 ) const {
      const SSize offset = (z*_cols*_rows + y*_cols + x);
      return _bands_array[ band ][ offset>>5 ] & ( 1ul << (offset & 0x1Ful) );
   }

   Bool bit( const Triple<SSize> & t, const SSize & band = 0 ) const {
      const SSize offset = (t.third*_cols*_rows + t.second*_cols + t.first);
      return _bands_array[ band ][ offset>>5 ] & ( 1ul << (offset & 0x1Ful) );
   }

   Bool nth_value( const SSize index, const SSize band = 0 ) const {
      return _bands_array[ band ][ index>>5 ] & ( 1ul << (index & 0x1Ful ) );
   }

   Bool zero( const SSize x, const SSize y, const SSize z ) const {
      const SSize offset = (z*_cols*_rows + y*_cols + x) >> 5;
      const UInt mask =  1ul << ( offset & 0x1Ful );
      for ( SSize b = 0; b < _bands; ++b )
         if ( _bands_array[ b ][ offset ] & mask )
            return false;
      return true;
   }

   /**
   * Sets the pixels value with the pixels value of the given image.
   * This supposes that images have the same dimensions.
   * @param src	the given image.
   * @return the image.
   */
   ZZZImage<Bool> & operator=( const ZZZImage<Bool> & src );

   /**
   * Sets the pixels value with the pixels value of the given image.
   * This supposes that images have the same dimensions.
   * @param src the given image.
   * @return the image.
   */
   template< typename U >
   ZZZImage<Bool> & operator=( const ZZZImage< U > & src );



   ZZZImage<Bool> & operator+=( const Bool &  );

   /**
   * Returns by references the dimensions.
   *
   * @param width
   * @param height
   * @param depth
   * @param bands
   */
   void getDimension( SSize & width, SSize & height, SSize & depth,
                      SSize & bands ) const {
      width = _cols;
      height = _rows;
      depth = _depth;
      bands = _bands;
   }

   bool holds( SSize x ) const {
      return ( x >= 0 ) && ( x < _cols );
   }

   bool holds( SSize x, SSize y ) const {
      return ( x >= 0 ) && ( x < _cols )
            && ( y >= 0 ) && ( y < _rows );
   }

   bool holds( SSize x, SSize y, SSize z ) const {
      return ( x >= 0 ) && ( x < _cols )
            && ( y >= 0 ) && ( y < _rows )
            && ( z >= 0 ) && ( z < _depth );
   }

   bool holds( SSize x, SSize y, SSize z, SSize band ) const {
      return ( x >= 0 ) && ( x < _cols )
            && ( y >= 0 ) && ( y < _rows )
            && ( z >= 0 ) && ( z < _depth )
            && ( band >= 0 ) && ( band < _bands );
   }

   bool holds( const Triple<SSize> & voxel ) const {
      return ( voxel.first >= 0 ) && ( voxel.first < _cols )
            && ( voxel.second >= 0 ) && ( voxel.second < _rows )
            && ( voxel.third >= 0 ) && ( voxel.third < _depth );
   }

   bool holds( const Pair<SSize> & pixel ) const {
      return ( pixel.first >= 0 ) && ( pixel.first < _cols )
            && ( pixel.second >= 0 ) && ( pixel.second < _rows );
   }

   bool boundary( SSize x ) const {
      return ( x == 0 || x == _cols - 1 );
   };

   bool boundary( SSize x, SSize y ) const {
      return ( x == 0 || y == 0 ||
               x == _cols - 1 || y == _rows - 1  );
   };

   bool boundary( SSize x, SSize y, SSize z ) const {
      return ( x == 0 || y == 0 || z == 0
               || x == _cols - 1
               || y == _rows - 1
               || z == _depth - 1 );
   };

   bool boundary( const Pair<SSize> & pixel ) const {
      return ( pixel.first == 0 ||
               pixel.second == 0  ||
               pixel.first == _cols - 1 ||
               pixel.second == _rows - 1  );
   };

   bool boundary( const Triple<SSize> & voxel ) const {
      return ( voxel.first == 0 || voxel.second == 0 || voxel.third == 0
               || voxel.first == _cols - 1
               || voxel.second == _rows - 1
               || voxel.third == _depth - 1 );
   };

   /**
   * Provides access via reference to a voxel, provided its coordinates.
   *
   * @param x The voxel's column.
   * @param y The voxel's row.
   * @param z The voxel's depth.
   * @param band The voxel's band.
   * @return A reference to a voxel value in the data array.
   */
   BitRef operator()( SSize x, SSize y = 0, SSize z = 0, SSize band = 0 ) {
      const SSize plane_size = _cols * _rows;
      const SSize index = x + y*_cols + z*plane_size;
      return BitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }

   /**
   * Provides access via reference to a voxel, provided its coordinates.
   *
   * @param x The voxel's column.
   * @param y The voxel's row.
   * @param z The voxel's depth.
   * @param band The voxel's band.
   * @return A reference to a voxel value in the data array.
   */
   ConstBitRef operator()( SSize x, SSize y = 0, SSize z = 0, SSize band = 0 ) const {
      const SSize plane_size = _cols * _rows;
      const SSize index = x + y*_cols + z*plane_size;
      return ConstBitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }

   /**
   * Provides access via reference to a voxel, provided its coordinates.
   *
   * @param x The voxel's column.
   * @param y The voxel's row.
   * @param z The voxel's depth.
   * @param band The voxel's band.
   * @return A reference to a voxel value in the data array.
   */
   BitRef operator()( const Triple<SSize> & voxel, const SSize band = 0 ) {
      const SSize plane_size = _cols * _rows;
      const SSize index = voxel.first + voxel.second*_cols + voxel.third*plane_size;
      return BitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }

   ConstBitRef operator()( const Triple<SSize> & voxel, const SSize band = 0 ) const {
      const SSize plane_size = _cols * _rows;
      const SSize index = voxel.first + voxel.second*_cols + voxel.third*plane_size;
      return ConstBitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }


   const char * valueString( SSize x, SSize y, SSize z ) const;

   const char * valueString( const Triple<SSize> & voxel ) const;

   bool nonZero( SSize x, SSize y, SSize z ) const;

   /**
   * Given a pair of integer, returns a repeference
   * to the voxel (in the first plane) with coordinates (first, second, 0)
   *
   * @param p
   *
   * @return
   */

   BitRef operator() ( const Pair<SSize> & p ) {
      const SSize index = p.first + p.second*_cols;
      return BitRef( _bands_array[0][ index>>5 ], index & 0x1F );
   }

   ConstBitRef operator() ( const Pair<SSize> & p ) const {
      const SSize index = p.first + p.second*_cols;
      return ConstBitRef( _bands_array[0][ index>>5 ], index & 0x1F );
   }

   BitRef operator() ( const Pair<SSize> & p, const SSize band ) {
      const SSize index = p.first + p.second*_cols;
      return BitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }

   ConstBitRef operator() ( const Pair<SSize> & p, const SSize band ) const {
      const SSize index = p.first + p.second*_cols;
      return BitRef( _bands_array[band][ index>>5 ], index & 0x1F );
   }

   Bool gray( const Triple<SSize> & t );

   Bool gray( const Pair<SSize> & t );

   Bool gray( SSize x, SSize y = 0, SSize z = 0 );

   Triple< BitRef > triple( SSize x, SSize y = 0, SSize z = 0 ) {
      switch ( _bands ) {
      case 1:
         return Triple<BitRef>( (*this)(x,y,z,0), (*this)(x,y,z,0), (*this)(x,y,z,0) );
         break;
      case 3:
         return Triple<BitRef>( (*this)(x,y,z,0), (*this)(x,y,z,1), (*this)(x,y,z,2) );
         break;
      default:
         std::cerr << "ZZZImage<Bool>::triple(x,y,z): Wrong number of bands (" << _bands << ")\n";
         return Triple<BitRef>();
      }
   }

   Triple< ConstBitRef > triple( SSize x, SSize y = 0, SSize z = 0 ) const {
      switch ( _bands ) {
      case 1:
         return Triple<ConstBitRef>( (*this)(x,y,z,0), (*this)(x,y,z,0), (*this)(x,y,z,0) );
         break;
      case 3:
         return Triple<ConstBitRef>( (*this)(x,y,z,0), (*this)(x,y,z,1), (*this)(x,y,z,2) );
         break;
      default:
         std::cerr << "ZZZImage<Bool>::triple(x,y,z): Wrong number of bands (" << _bands << ")\n";
         return Triple<ConstBitRef>();
      }
   }

   Triple< ConstBitRef> triple( const Pair<SSize> & p ) const {
      switch ( _bands ) {
      case 1:
         return Triple<ConstBitRef>( (*this)(p.first,p.second,0,0),
                                     (*this)(p.first,p.second,0,0),
                                     (*this)(p.first,p.second,0,0) );
         break;
      case 3:
         return Triple<ConstBitRef>( (*this)(p.first,p.second,0,0),
                                     (*this)(p.first,p.second,0,1),
                                     (*this)(p.first,p.second,0,2) );
         break;
      default:
         std::cerr << "ZZZImage<>::triple(Pair): Wrong number of bands (" << _bands << ")\n";
         return Triple<ConstBitRef>();
      }
   }

   Triple< BitRef> triple( const Pair<SSize> & p ) {
      switch ( _bands ) {
      case 1:
         return Triple<BitRef>( (*this)(p.first,p.second,0,0),
                                (*this)(p.first,p.second,0,0),
                                (*this)(p.first,p.second,0,0) );
         break;
      case 3:
         return Triple<BitRef>( (*this)(p.first,p.second,0,0),
                                (*this)(p.first,p.second,0,1),
                                (*this)(p.first,p.second,0,2) );
         break;
      default:
         std::cerr << "ZZZImage<>::triple(Pair): Wrong number of bands (" << _bands << ")\n";
         return Triple<BitRef>();
      }
   }

   Triple< BitRef> triple( const Triple<SSize> & t ) {
      switch ( _bands ) {
      case 1:
         return Triple<BitRef>( (*this)(t.first,t.second,t.third,0),
                                (*this)(t.first,t.second,t.third,0),
                                (*this)(t.first,t.second,t.third,0) );
         break;
      case 3:
         return Triple<BitRef>( (*this)(t.first,t.second,t.third,0),
                                (*this)(t.first,t.second,t.third,1),
                                (*this)(t.first,t.second,t.third,2) );
         break;
      default:
         std::cerr << "ZZZImage<>::triple(Triple): Wrong number of bands (" << _bands << ")\n";
         return Triple<BitRef>();
      }
   }

   Triple< ConstBitRef> triple( const Triple<SSize> & t ) const {
      switch ( _bands ) {
      case 1:
         return Triple<ConstBitRef>( (*this)(t.first,t.second,t.third,0),
                                     (*this)(t.first,t.second,t.third,0),
                                     (*this)(t.first,t.second,t.third,0) );
         break;
      case 3:
         return Triple<ConstBitRef>( (*this)(t.first,t.second,t.third,0),
                                     (*this)(t.first,t.second,t.third,1),
                                     (*this)(t.first,t.second,t.third,2) );
         break;
      default:
         std::cerr << "ZZZImage<>::triple(Triple): Wrong number of bands (" << _bands << ")\n";
         return Triple<ConstBitRef>();
      }
   }

   /**
   * Zeros the data vector of the image.
   */
   void clear();

   /**
   * Returns the dimension of the image.
   * @return The dimension (3d) of the image.
   */
   Dimension3d dimension();

   bool zeroFramed() const {
      return false;
   }

   ImageProperties::DataArrayLayout arrayLayout() const { return ImageProperties::NormalArrayLayout; }

   /**
   * Returns the identifier of the object.
   */
   Typobj poType() const { return (Typobj) Po_type< ZZZImage<Bool> >::type; }

   /**
   * Returns the identifier of the object as stored in a file.
   */
   Typobj poFileType() const { return _poFileType; }

   /**
   * Set the file's Typeobj (if compatible with the current value type).
   *
   * @param Typeobj
   *
   * @return true if the operation was successfull (compatible types).
   */
   bool poFileType( Typobj );

   /**
   * Returns the colorspace of the volume.
   *
   */
   ColorSpace colorSpace() { return _colorSpace; }

   /**
   * Returns the type name.
   */
   std::string name() const { return TypeName< ZZZImage<Bool> >::name(); }

   /**
   * Returns the number of columns.
   */
   SSize width() const { return _cols; }

   /**
   * Returns the number of rows.
   */
   SSize height() const { return _rows; }

   /**
   * Returns the number of planes.
   */
   SSize depth() const { return _depth; }

   /**
   * Returns the number of bands.
   */
   SSize bands() const { return _bands; }

   /**
   * Returns the dimension of the image.
   */
   Dimension3d size() const { return Dimension3d( _depth, _rows, _cols ); }

   /**
   * Returns the number of pixels in the image.
   */
   Size vectorSize() const {
      return static_cast<Size>( _depth ) * _rows * _cols;
   }

   /**
   * Returns the actual size of each band vector.
   * @return The size of a band underlying vector.
   */
   size_t realBandVectorSize() const {
      return _band_size;
   }

   /**
   * Returns the related vector of properties.
   */
   ImageProperties props() const {
      return ImageProperties( _bands, _cols, _rows, _depth,
                              ( ColorSpace ) 0,
                              0,
                              _depth * _cols * _rows,
                              ImageProperties::NormalArrayLayout );
   }

   Triple<float> gravityCenter();


   /**
   * Number of nonzero voxels.
   *
   * @return
   */
   Size nonZeroVoxels();

   Bool max( SSize band = -1 );
   Bool min( SSize band = -1 );
   Pair<Bool> range( SSize band = -1 );


   ZZZImage<Bool> * levelMap( SSize depth );

   void extract( ZZZImage<Bool> & dst,
                 SSize x, SSize y, SSize z,
                 SSize width, SSize height, SSize depth );

   ZZZImage<Bool> * sub( SSize x, SSize y, SSize z,
                         SSize width, SSize height, SSize depth );

   ZZZImage<Bool> * slice( SSize x, SSize y, SSize z );

   void insert( SSize x, SSize y, SSize z, AbstractImage * image );

   void insertSlice( SSize x, SSize y, SSize z, AbstractImage * image );


   bool load( const char * filename, bool multiplexed );
   bool load( ByteInput & in  );
   bool load( std::istream & in  );

   bool save( const char * filename ) const;
   bool save( ByteOutput & out, const char * format ) const;

   bool loadFileBuffer( const unsigned char * data, size_t size );

   bool loadASC( ByteInput & input );
   bool load3DZ( ByteInput & input );
   bool loadVFF( ByteInput & input );
   bool loadPAN( ByteInput & input );
   bool loadVOL( ByteInput & input );
   bool loadRAW( ByteInput & input, bool multiplexed );
   bool loadRAW( std::istream & in, bool multiplexed );

   bool save3DZ( ByteOutput & out ) const;
   bool saveVFF( ByteOutput & out ) const;
   bool savePAN( ByteOutput & out ) const;
   bool saveVOL( ByteOutput & out ) const;
   bool saveNPZ( ByteOutput & out ) const;
   bool saveRAW( ByteOutput & out ) const;
   bool saveASC( ByteOutput & out ) const;
   bool saveJBM( ByteOutput & out ) const;
   bool saveBMP( ByteOutput & out ) const;

   bool loadRAW( std::istream & file,
                 const Dimension3d & dimension,
                 bool multiplexed );

   bool loadXBM( std::istream & file );

   bool loadBMP( std::istream & file );

   void mirror( int axis );
   void rotate( int axis );
   void binarize( UChar value = 255 );
   void binarize( UChar red, UChar green, UChar blue );
   void negative();
   void grayShadeFromHue();
   void grayShadeFromSaturation();
   void toGray();
   void toColor();
   void bitPlane( UChar bit, SSize band );
   void bitPlaneGrayCode( UChar bit, SSize band );
   void quantize( UInt32 cardinality );
   void swapEndianness();

   void hollowOut( int adjacency = ADJ26 );

   void seedFill( SSize x, SSize y, SSize z, UChar value = 255 );
   void seedFillXY( SSize x, SSize y, SSize z, UChar value = 255 );
   void seedFillXZ( SSize x, SSize y, SSize z, UChar value = 255 );
   void seedFillYZ( SSize x, SSize y, SSize z, UChar value = 255 );

   /**
   * Fill the inside of a surface (works fine only if the surface is closed).
   *
   * @param value
   */
   void surfaceFill( UChar value = 255 );

   void fillForHoleClosing() { }

   void addBoundaries( UChar value, UChar thickness );


   /*   void threshold( Triple<T> min, Triple<T> max,  */
   /* 		  std::vector< Triple<T> > & removedValues, */
   /* 		  ZZZImage<UChar> & result ); */

   /**
   * Mask the image by zeroing the voxel whose value is 0
   * in the given (other) image.
   * Warning: no size compatibility check.
   *
   * @param other
   *
   * @return
   */
   ZZZImage<Bool> & operator&=( const ZZZImage<Bool> & other );

   ZZZImage<Bool> & operator|=( const ZZZImage<Bool> & other );

   ZZZImage<Bool> operator|( const ZZZImage<Bool> & other );

   ZZZImage<Bool> & operator-=( const ZZZImage<Bool> & other );

   ZZZImage<Bool> & mask( const ZZZImage< UChar > & mask );

   void getFramedBitMask( ZZZImage<Bool> & ) const;
   void applyFramedBitMask( const ZZZImage<Bool> & );

   bool isBorder( SSize x, SSize y, SSize z, char adjacency );

   bool setValue( Triple<SSize> voxel,
                  UChar red, UChar green, UChar blue );

   UChar orientation( SSize x, SSize y, SSize z );
   UChar orientation( const Triple<SSize> & voxel );
   Config config( SSize x, SSize y, SSize z ) const;
   Config config( const Triple<SSize> & voxel ) const;
   Config config2D( SSize x, SSize y, SSize z );

   void configExtended( Config & conf, Config & p,
                        const ZZZImage<Bool> & imP,
                        SSize x, SSize y, SSize z );

   void configExtended( Config & conf, Config & p,
                        const ZZZImage<Bool> & imP,
                        const Triple<SSize> & voxel );

   void getColor( SSize x, SSize y, SSize z, UChar & red, UChar & green, UChar & blue );
   void getColor( const Triple<SSize> & , UChar & red, UChar & green, UChar & blue );
   void getGrayLevel( SSize x, SSize y, SSize z, UChar & gray );
   void getGrayLevel( const Triple<SSize> & , UChar & gray );
   Triple<UChar> getColorFromLUT( const Triple<SSize> & voxel,
                                  const std::vector< Triple<UChar> > & lut ) const;
   void setColorMode( ColorMode );

   void mark6border( ZZZImage<Bool> & mask );
   void mark6Pborder( ZZZImage<Bool> & mask );
   void mark18border( ZZZImage<Bool> & mask );
   void mark26border( ZZZImage<Bool> & mask );

   void boundingBox( SSize & xMin, SSize & yMin, SSize & zMin,
                     SSize & xMax, SSize & yMax, SSize & zMax ) const;

   void fitBoundingBox( SSize margin = 0 );

   void subSample( UChar minCount = 4 );
   void scale( UChar direction, UChar factor = 2 );
   void extrude( SSize depth );
   void spongify();

   void clamp( const Triple<Int32> & min, const Triple<Int32> & max);
   void clamp( const Triple<UInt32> & min, const Triple<UInt32> & max);
   void clamp( const Triple<Float> & min, const Triple<Float> & max);


   bool merge( ZZZImage<Bool> & other, int axis );
   bool merge( AbstractImage & other, int axis );

   void zeroPlane( SSize x, SSize y, SSize z );

   /**
   * Fills an histogram from he voxel values.
   *
   * @param h The histogram with given size to be filled.
   * @param minxVal
   * @param maxVal
   * @param band
   */
   void histogram( std::vector<Size> & h,  Bool & minxVal, Bool & maxVal, int band = 0 );

   /**
   *
   *
   * @param axis The orthogonal axis (to the plane). 0 is Ox, 1 is Oy, 2 is Oz
   * @param n The plane coordinate among the given axis
   */
   void trim( int axis, SSize n );


protected:


   bool loadData( ByteInput & input, bool multiplexed );

   bool loadCharData( ByteInput & input );

   /**
   * Load RLE compressed data from an input stream.
   *
   * @param file
   *
   * @return
   */
   bool loadDataRLE( ByteInput & input );


   bool loadDataASC( ByteInput & input );


   bool loadPandoreData( ByteInput & input, bool region, bool swapBytes );

   /**
   * Revers the byte order of all the values in the array.
   *
   */
   void reverseDataBytes();

   /**
   * Writes a pandore header for the image in a file.
   *
   * @param out
   * @param type
   */
   bool savePandoreHeader( ByteOutput & out, Typobj type ) const;

   /**
   * Loads attribute values from the given file.
   * Allocates therefrom the related data.
   * @param file the file where to read attributes values.
   * @param inversionMode Whether or note bytes should be swapped.
   */
   std::istream & loadPandoreAttributes( std::istream  &file, bool inversionMode );

   /**
   * Saves the current attribute values.
   * @param file the file.
   */
   ByteOutput & savePandoreAttributes( ByteOutput & out ) const;

   /**
   * Saves data in the given file.
   * @param file the file where to save data.
   */
   ByteOutput & savePandoreData( ByteOutput & out ) const;

   /**
   * Saves the image data as an image of regions.
   * The value type actually saved depends on the
   * maximal value found in the volume or image.
   *
   * @param file the file where to save data.
   * @param maximalValue The maximal label value.
   */
   ByteOutput & saveRegionData( ByteOutput & out,
                                UInt32 maximalValue ) const;

   /**
   * Load UInt32 data using the minimum of 1, 2 or 4 bytes according
   * to a maximum value specified.
   *
   * @param file
   * @param swapBytes Whether or not bytes should be swapped (but not the data buffer).
   * @param maximalValue The maximal value of the labels in a region image.
   *
   * @return The provided input stream.
   */
   bool loadRegionData( ByteInput & input,
                        bool swapBytes,
                        UInt32 maximalValue );

   Triple<float> _aspect;
   UInt **_bands_array;
   SSize _bands;			/**< Number of bands. */
   SSize _cols;			/**< Number of columns. */
   SSize _rows;			/**< Number of rows. */
   SSize _depth;			/**< Number of planes. */
   SSize _band_size;
   Typobj _poFileType;
   ColorSpace _colorSpace;
   ColorMode _colorMode;
};



#endif // _ZZZIMAGE_BOOL_H_




