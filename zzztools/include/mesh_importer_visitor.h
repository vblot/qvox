/** -*- mode: c++ -*-
 * @file   draw_line_visitor.h
 * @author Sebastien Fourey (GREYC)
 * @date   Fri Nov 11 20:59:35 2005
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _MESH_IMPORTER_VISITOR_H_
#define _MESH_IMPORTER_VISITOR_H_

#include "image_visitor.h"
#include "globals.h"
#include "Triple.h"
#include "tools.h"
#include "tools3d.h"
#include "zzzimage.h"
#include <vector>
#include <fstream>
#include <cstring>
#include <sstream>

class MeshOFFImporterVisitor : public AbstractImageVisitor {
public:
  MeshOFFImporterVisitor( const char * filename, bool filled = false );

  template <typename T>  void visitAny( ZZZImage<T> & );
  void visit( ZZZImage<UChar> &  );
  void visit( ZZZImage<Short> &  );
  void visit( ZZZImage<UShort> &  );
  void visit( ZZZImage<Int32> &  );
  void visit( ZZZImage<UInt32> &  );
  void visit( ZZZImage<Float> &  );
  void visit( ZZZImage<Double> &  );
  void visit( ZZZImage<Bool> &  );

private:
  const char * _filename;
  bool _filled;
};

class MeshOBJImporterVisitor : public AbstractImageVisitor {
public:
  MeshOBJImporterVisitor( const char * filename, bool filled = false );

  template <typename T>  void visitAny( ZZZImage<T> & );
  void visit( ZZZImage<UChar> &  );
  void visit( ZZZImage<Short> &  );
  void visit( ZZZImage<UShort> &  );
  void visit( ZZZImage<Int32> &  );
  void visit( ZZZImage<UInt32> &  );
  void visit( ZZZImage<Float> &  );
  void visit( ZZZImage<Double> &  );
  void visit( ZZZImage<Bool> &  );

private:
  const char * _filename;
  bool _filled;
};

/**
 * Mesh Importer using the method described by E. Remy (University of Provence - France)
 * =====================================================================================
 *
 * ---- README file shipped with several volumes at the TC'18 --------------------------
 * ---- Beginning of the quotation -----------------------------------------------------
 * The converter was made in 2002 by Thierry Baud and Djemel Guizani when they
 * were in their 3rd year of student at the ES2I/ESIL (Ecole Superieure
 * d'Ingenieurs en Informatique at the Ecole Superieure d'Ingenieurs de Luminy) at
 * the Universite de la Mediterrannee in Marseille France. The converter gives a
 * set of points forming the discrete surface of the set of triangles. The
 * algorithm used is rather simple and stupid:
 * Lets consider a triangle (a,b,c).
 *  - first, an edge of the triangle (say [ab]) is drawn using Bresenham's
 *    algorithm.
 *  - for each of the obtained points x forming this discrete edge [ab],
 *    Bresenham's algorithm is used to draw discrete lines [xc]. This way the
 *    triangle is filled.
 *
 * This discrete "crust" (made of 1-colored points) was then filled using the
 * following scheme:
 *  - fill the exterior of the crust with a different color (say 2).
 *  - change 1-points to background (0-points).
 *  - negate this volume.
 * It is much more simpler to fill the volume this way (by filling its exterior)
 * than filling its interior since the latter gives of lots of small holes in the
 * object.
 *
 * This discretisation process was used from the LWO object to each of the 3D
 * discrete image.
 *
 * May the 24th, 2003
 * E. Remy
 * ---- End of the quotation -----------------------------------------------------------
 */

template<typename T>
void
MeshOFFImporterVisitor::visitAny( ZZZImage<T> & image )
{
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();

  char line[4096];
  std::ifstream file( _filename);
  if ( !file ) {
    ERROR << "importMeshOFF(): Cannot open file." << std::endl;
    return;
  }

  file.getline( line, 4096 );
  if ( strstr( line, "OFF" )  != line ) {
    ERROR << "importMeshOFF(): not an OFF file." << std::endl;
    return;
  }

  Int32 nbVertices, nbEdges, nbFaces;
  std::vector< Triple<float> > vertices;
  file >> nbVertices >>  nbFaces >> nbEdges;

  TSHOW( nbVertices );
  TSHOW( nbEdges );
  TSHOW( nbFaces );

  /* Load vertices */
  Int32 count = nbVertices;
  Triple<float> vertex;
  while ( count-- ) {
    file >> vertex;
    vertices.push_back( vertex );
  }

  /* Get the bounding box */
  Triple<float> pMin,pMax;
  pMin = std::numeric_limits<float>::max();
  pMax = -std::numeric_limits<float>::max();

  std::vector< Triple<float> >::iterator pv = vertices.begin();
  std::vector< Triple<float> >::iterator pvend = vertices.end();
  while ( pv != pvend ) {
    maximize( pMax.first, pv->first );
    maximize( pMax.second, pv->second );
    maximize( pMax.third, pv->third );
    minimize( pMin.first, pv->first );
    minimize( pMin.second, pv->second );
    minimize( pMin.third, pv->third );
    ++pv;
  }

  Triple<float> size = pMax - pMin;
  float scale = ::min( (cols-2)/size.first, ::min( (rows-2)/size.second, (depth-2)/size.third ) );
  Triple<float> center = 0.5f * (pMax + pMin);
  Triple<SSize> volumeCenter( cols>>1, rows>>1, depth>>1 );
  Triple<SSize> p1,p2,p3,p4;
  std::istringstream iss;

  /* Load and "draw" faces on the fly */
  int n,v;
  float red,green,blue;
  Triple<UChar> color;
  int dummy;
  bool warning = false;
  do
    file.getline( line, 1024 );
  while ( file && line[0]=='\0' );
  iss.str( line );
  while ( file.good() ) {
    if ( iss.good() ) {
      iss >> n;
      switch ( n ) {
      case 3:
        iss >> v;
        p1 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();
        iss >> v;
        p2 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();
        iss >> v;
        p3 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();

        color = 127;
        if ( iss.good() &&
             sscanf( iss.str().c_str(), "%d %d %d %d %f %f %f",
                     &dummy, &dummy, &dummy, &dummy, &red, &green, &blue ) == 7 ) {
          if ( image.bands() != 3 )
            color = static_cast<UChar>( 255*(red+green+blue)/3.0f );
          else
            color.set( static_cast<UChar>(red*255),
                       static_cast<UChar>(green*255),
                       static_cast<UChar>(blue*255) );
          if ( color.first + color.second + color.third == 0 )
            color = 127;
        }
        fillTriangle( image, p1, p2, p3, color );
        break;
      case 4:
        iss >> v;
        p1 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();
        iss >> v;
        p2 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();
        iss >> v;
        p3 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();
        iss >> v;
        p4 = volumeCenter + ((vertices[v]-center)*scale).round().to<SSize>();

        color = 127;
        if ( iss.good() &&
             sscanf( iss.str().c_str(), "%d %d %d %d %d %f %f %f",
                     &dummy, &dummy, &dummy, &dummy, &dummy, &red, &green, &blue ) == 8 ) {
          if ( image.bands() != 3 )
            color = static_cast<UChar>( 255*(red+green+blue)/3.0f );
          else
            color.set( static_cast<UChar>(red*255),
                       static_cast<UChar>(green*255),
                       static_cast<UChar>(blue*255) );
          if ( color.first + color.second + color.third == 0 )
            color = 127;
        }

        fillTriangle( image, p1, p2, p3, color );
        fillTriangle( image, p1, p3, p4, color );
        break;
      default:
        if ( !warning ) {
          WARNING << "importMeshOFF(): Cannot handle faces with " << n << " vertices. Ignored.";
          warning = true;
        }
      }
    }
    file.getline( line, 1024 );
    iss.str( line );
    iss.clear();
  }

  if ( _filled )
    image.surfaceFill( 127 );
}

template<typename T>
void
MeshOBJImporterVisitor::visitAny( ZZZImage<T> & image )
{
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  char line[4096];
  std::ifstream file( _filename );
  if ( !file ) {
    ERROR << "importMeshOBJ(): Cannot open file." << std::endl;
    return;
  }

  std::vector< Triple<float> > vertices;
  Triple<float> vertex;
  std::istringstream iss;

  /* Load vertices */
  while ( file.getline( line, 4096 ) ) {
    if ( line[0] == 'v' && line[1] == ' ' ) {
      iss.str( line+2 );
      iss >> vertex;
      vertices.push_back( vertex );
    }
  }

  TRACE << "Read " << vertices.size() << " vertices.\n";

  /* Get the bounding box */
  Triple<float> pMin,pMax;
  pMin = std::numeric_limits<float>::max();
  pMax = -std::numeric_limits<float>::max();

  std::vector< Triple<float> >::iterator pv = vertices.begin();
  std::vector< Triple<float> >::iterator pvend = vertices.end();
  while ( pv != pvend ) {
    maximize( pMax.first, pv->first );
    maximize( pMax.second, pv->second );
    maximize( pMax.third, pv->third );
    minimize( pMin.first, pv->first );
    minimize( pMin.second, pv->second );
    minimize( pMin.third, pv->third );
    ++pv;
  }

  Triple<float> size = pMax - pMin;
  float scale = ::min( (cols-2)/size.first, ::min( (rows-2)/size.second, (depth-2)/size.third ) );
  Triple<float> center = 0.5f * (pMax + pMin);
  Triple<SSize> volumeCenter( cols>>1, rows>>1, depth>>1 );
  Triple<SSize> p1,p2,p3,p4;

  file.close();
  file.open( _filename );

  /* Load and "draw" faces on the fly */
  UInt32 nbFaces=0;
  Triple<UChar> color;
  bool warning = false;
  UInt32 vertexIndices[4];
  int vertexCount;
  std::istringstream iss2;

  while ( file.getline( line, 4096 ) ) {
    if ( line[0] == 'f' && line[1] == ' ' ) {
      std::istringstream iss( line+2 );
      std::string str;
      vertexCount = 0;
      while ( iss >> str ) {
        std::istringstream iss2( str );
        iss2 >> vertexIndices[ vertexCount++ ];
      }

      switch ( vertexCount ) {
      case 3:
        ++nbFaces;
        p1 = volumeCenter
            + ((vertices[vertexIndices[0]-1]-center)*scale).round().to<SSize>();
        p2 = volumeCenter
            + ((vertices[vertexIndices[1]-1]-center)*scale).round().to<SSize>();
        p3 = volumeCenter
            + ((vertices[vertexIndices[2]-1]-center)*scale).round().to<SSize>();
        color = 127;
        fillTriangle( image, p1, p2, p3, color );
        break;
      case 4:
        ++nbFaces;
        p1 = volumeCenter
            + ((vertices[vertexIndices[0]-1]-center)*scale).round().to<SSize>();
        p2 = volumeCenter
            + ((vertices[vertexIndices[1]-1]-center)*scale).round().to<SSize>();
        p3 = volumeCenter
            + ((vertices[vertexIndices[2]-1]-center)*scale).round().to<SSize>();
        p4 = volumeCenter
            + ((vertices[vertexIndices[3]-1]-center)*scale).round().to<SSize>();
        color = 127;
        fillTriangle( image, p1, p2, p3, color );
        fillTriangle( image, p1, p3, p4, color );
        break;
      default:
        if ( !warning ) {
          WARNING << "importMeshOBJ(): Cannot handle faces with " << vertexCount << " vertices. Ignored.";
          warning = true;
        }
      }
    }
  }

  TRACE << "Read " << nbFaces << " faces.\n";

  if ( _filled )
    image.surfaceFill( 127 );
}

#endif // _MESH_IMPORTER_VISITOR_H_

