/* -*- mode: c++; c-basic-offset: 3 -*-
 * @(#)dimension.h			PANDORE		2001-04-03
 *
 * PANTHEON Project
 *
 * GREYC IMAGE
 * 6 Boulevard Mar�chal Juin
 * F-14050 Caen Cedex France
 *
 * This file is free software. You can use it, distribute it
 * and/or modify it. However, the entire risk to the quality
 * and performance of this program is with you.
 *
 * Please email any bugs, comments, and/or additions to this file to:
 * Regis.Clouard@greyc.ensicaen.fr
 * http://www.greyc.ensicaen.fr/~regis
 */

/*
 * (C)R�gis Clouard - 1999-10-08
 * (C)Francois Angot - 1999-10-08
 * (R)Alexandre Duret-Lutz. 1999-10-08
 * (R)R�gis Clouard - 2001-04-10 (version 3.00)
 * (R)R�gis Clouard - 2004-06-30 (Set as a Pobject, Add operators *=, /=...)
 * (R)S�bastien Fourey - 2005 ( Flight-weigthted version )
 */

/*
 *                       -- IMPORTANT NOTE --
 *
 *  This file is a modified version of the pandore original files.
 *  It is a lightenned version, specially rewritten for the QVox
 *  visualization tool.
 *  (C)S�bastien Fourey - GREYC ENSICAEN - France
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIMENSION_H_
#define _DIMENSION_H_

/**
 * The <code>Dimension1d</code> class encapsulates the width
 * of a component (in integer precision) in a single object.
 */
class Dimension1d {
public:
   Int32 w;		/**< The width of the dimension */

   /**
    * Creates a Dimension with a zero width.
    */
   Dimension1d():w( 0 ) {
   }
   /**
    * Creates a Dimension with the specified width.
    * @param width The width.
    */ Dimension1d( Int32 width ):w( width ) {
   }

   /**
    * Creates a Dimension whose width and height are the same
    * as the specified one.
    * @param d	the specified dimension for the width.
    */
   Dimension1d( const Dimension1d & d ):w( d.w ) {
   }

   /**
    * Sets the dimension with the specified dimension.
    * @param d	the reference dimension.
    * @return the reference dimension.
    */
   const Dimension1d & operator=( const Dimension1d & d ) {
      w = d.w;
      return ( d );
   }

   /**
    * Checks if the dimension is equal to the specified dimension.
    * @param d	the reference dimension.
    * @return true if dimensions are equals.
    */
   bool operator==( const Dimension1d & d ) const {
      return d.w == w;
   }

   /**
    * Checks if the dimension is different from the specified dimension.
    * @param d	the reference dimension.
    * @return true if dimensions are different.
    */
   bool operator!=( const Dimension1d & d ) const {
      return !( d == *this );
   }

   /**
    * Multiplies each member of the dimension by the specified value.
    * @param x	an integer.
    */
   Dimension1d operator*( int x ) {
      return Dimension1d( w * x );
   }

   /**
    * Divides each member of the dimension by the specified value
    * @param x	an integer.
    */
   Dimension1d operator/( int x ) {
      return Dimension1d( w / x );
   }

   /**
    * Return whether dimension is not zero
    */
   operator bool() {
      return w != 0;
   }

   /**
    *
    * @brief clear
    */
   void clear() {
      w = 0;
   }

};


/**
 * Creates a new dimension that results from the multiplication
 * of the specified dimension by x.
 * @param d	the dimension.
 * @param x	the multiplier.
 * @return the new dimension.
 */
inline Dimension1d
operator*( int x, const Dimension1d & d )
{ return Dimension1d( d.w * x ); }

/**
 * The <code>Dimension2d</code> class encapsulates the width and the height
 * of a component (in integer precision) in a single object.
 */
class Dimension2d {
public:
   Int32 w;			/**< The width. */
   Int32 h;			/**< Tge height. */

   /**
    * Creates an instance of Dimension with a width of zero
    * and a height of zero.
    */
   Dimension2d():w( 0 ), h( 0 ) { }

   /**
    * Creates a Dimension with the specified 1D dimension.
    * @param d A 1d dimension
    */
   Dimension2d( const Dimension1d & d ):w(d.w),h(1) { }

   /**
    * Creates a Dimension with the specified width.
    * @param height	the width.
    * @param width	the height.
    */
   Dimension2d( Int32 height, Int32 width ):w( width ), h( height ) { }

   /**
    * Creates a Dimension whose width and height are the same
    * as for the specified dimension.
    * @param d	the specified dimension for the width and height values.
    */
   Dimension2d( const Dimension2d & d ):w( d.w ), h( d.h ) { }

   /**
    * Sets the dimension with the specified dimension.
    * @param d	the reference dimension.
    * @return the reference dimension.
    */
   Dimension2d & operator=( const Dimension2d & d ) {
      w = d.w; h = d.h;
      return *this;
   }

   /**
    * Implicit cast to Dimension1d.
    * @return The width attribute as a Dimension1d.
    */
   operator Dimension1d () { return Dimension1d( w ); }

   /**
    * Checks if the dimension is equal to the specified dimension.
    * @param d	the reference dimension.
    * @return true if dimensions are equals.
    */
   bool operator==( const Dimension2d & d ) const {
      return d.w == w && d.h == h;
   }

   /**
    * Checks if the dimension is different from the specified dimension.
    * @param d	the reference dimension.
    * @return true if dimensions are different.
    */
   bool operator!=( const Dimension2d & d ) const {
      return !( d == *this );
   }

   /**
    * Multiplies each member of the dimension by the specified value.
    * @param x	an integer.
    */
   Dimension2d operator*( int x ) {
      return Dimension2d( h * x, w * x );
   }

   /**
    * Divides each member of the dimension by the specified value
    * @param x	an integer.
    */
   Dimension2d operator/( int x ) {
      return Dimension2d( h / x, w / x );
   }

   /**
    * Return whether dimension is not zero
    */
   operator bool() {
      return w != 0 && h != 0;
   }

   /**
    *
    * @brief clear
    */
   void clear() {
      w = h = 0;
   }
};

/**
 * Creates a new dimension that results from the multiplication
 * of the specified dimension by x.
 * @param d	the dimension.
 * @param x	the multiplier.
 * @return the new dimension.
 */
inline const Dimension2d
operator*( int x, const Dimension2d & d )
{ return Dimension2d( d.h * x, d.w * x ); }

/**
 * The <code>Dimension3d</code> class encapsulates the width,
 * the height and the depth of a component (in integer precision)
 * in a single object.
 */
class Dimension3d {
public:
   Int32 d;			/**< The depth. */
   Int32 h;			/**< The height. */
   Int32 w;			/**< The width. */

   /**
    * Creates an instance of Dimension with a width of zero,
    * a height of zero and a depth of zero.
    */
   Dimension3d():d( 0 ), h( 0 ), w( 0 ) { }

   /**
    * Creates a Dimension with the specified 2d dimension.
    * @param d A 2D dimension.
    */
   Dimension3d( const Dimension2d & d )
      :d(1),h(d.h),w(d.w) { }

   /**
    * Creates a Dimension with the specified width, height and depth.
    * @param depth	the depth.
    * @param height	the height.
    * @param width	the width.
    */
   Dimension3d( Int32 depth, Int32 height, Int32 width )
      :d( depth ), h( height ), w( width ) { }

   /**
    * Creates a Dimension whose width, height and depth are the same
    * as for the specified dimension.
    * @param d	the specified dimension for the width, height and depth values.
    */
   Dimension3d( const Dimension3d & d ):d( d.d ), h( d.h ), w( d.w ) { }

   /**
    * Sets the dimension with the specified dimension.
    * @param x	the reference dimension.
    * @return the reference dimension.
    */
   Dimension3d & operator=( const Dimension3d & x ) {
      w = x.w; h = x.h; d = x.d;
      return *this;
   }

   /**
    * Implicit cast to Dimension1d.
    * @return The width attribute as a Dimension1d.
    */
   operator Dimension1d () { return Dimension1d( w ); }

   /**
    * Implicit cast to Dimension2d.
    * @return The width and height attributes as a Dimension2d.
    */
   operator Dimension2d () { return Dimension2d( h, w ); }

   /**
    * Checks if the dimension is equal to the specified dimension.
    * @param x	the reference dimension.
    * @return true if dimensions are equals.
    */
   bool operator==( const Dimension3d & x ) const {
      return x.w == w && x.h == h && x.d == d;
   }
   /**
    * Checks if the dimension is different from the specified dimension.
    * @param x	the reference dimension.
    * @return true if dimensions are different.
    */
   bool operator!=( const Dimension3d & x ) const {
      return !( x == *this );
   }

   /**
    * Multiplies each member of the dimension by the specified value.
    * @param x	an integer.
    */
   Dimension3d operator*( int x ) {
      return Dimension3d( d * x, h * x, w * x );
   }

   /**
    * Divides each member of the dimension by the specified value
    * @param x	an integer.
    */
   Dimension3d operator/( int x ) {
      return Dimension3d( d / x, h / x, w / x );
   }

   /**
    * Return whether dimension is not zero
    */
   operator bool() {
      return w != 0 && h != 0 && d != 0;
   }

   /**
    *
    * @brief clear
    */
   void clear() {
      w = h = d = 0;
   }

};

/**
 * Creates a new dimension that results from the multiplication
 * of the specified dimension by x.
 * @param d	the dimension.
 * @param x	the multiplier.
 * @return the new dimension.
 */
inline const Dimension3d
operator*( int x, const Dimension3d & d )
{ return Dimension3d( d.d * x, d.h * x, d.w * x ); }

#endif  // _PDIMENSION_H_
