/** -*- mode: c++ -*-
 * @file   skel.h
 * @author Sebastien Fourey (GREYC)
 * @date   Fri Nov 11 20:59:35 2005
 * 
 * @brief  
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SKEL_H_
#define _SKEL_H_

#include <vector>
#include "zzzimage.h"
#include "config.h"
#include "tools3d.h"
#include "image_visitor.h"

/*  Marquage de l'objet lors de la squelettisation :
    bit 0  (1) : point de l'objet
    bit 1  (2) : point du bord de l'objet
    bit 3  (4) : point terminal
    Les points marques sont AJOUTES a la liste bord donnee en
    argument.

*/

class SkelSequentialVisitor : public AbstractImageVisitor {
public:
   SkelSequentialVisitor( int iterations,
                          const char * directionSequence,
                          int adjacency,
                          TerminalPointTest terminalTest,
                          bool binarize );
   void visit( ZZZImage<UChar> &  );
   void visit( ZZZImage<Short> &  );
   void visit( ZZZImage<UShort> &  );
   void visit( ZZZImage<Int32> &  );
   void visit( ZZZImage<UInt32> &  );
   void visit( ZZZImage<Float> &  );
   void visit( ZZZImage<Double> &  );
   void visit( ZZZImage<Bool> &  );
private:
   int _iterations;
   const char * _directionSequence;
   int _adjacency;
   TerminalPointTest _terminalTest;
   bool _binarize;
   Size _removedCount;
};

class SkelParallelVisitor : public AbstractImageVisitor {
public:
   SkelParallelVisitor( int iterations,
                        int adjacency,
                        TerminalPointTest terminalTest,
                        bool binarize );
   void visit( ZZZImage<UChar> &  );
   void visit( ZZZImage<Short> &  );
   void visit( ZZZImage<UShort> &  );
   void visit( ZZZImage<Int32> &  );
   void visit( ZZZImage<UInt32> &  );
   void visit( ZZZImage<Float> &  );
   void visit( ZZZImage<Double> &  );
   void visit( ZZZImage<Bool> &  );
private:
   int _iterations;
   int _adjacency;
   TerminalPointTest _terminalTest;
   bool _binarize;
   Size _removedCount;
};

void
markBorder( ZZZImage<UChar> & image, 
            std::vector< Triple<SSize> > & border,
            int adjacency,
            bool binarize,
            int direction = -1 );


Size
skelSequential( ZZZImage<UChar> & image,
                int iterations,
                const char * directionSequence,
                int adjacency,
                TerminalPointTest terminalTest,
                bool binarize );

Size
skelParallel( ZZZImage<UChar> & image,
              int iterations,
              int adjacency,
              TerminalPointTest terminalTest,
              bool binarize );

Size
skelParallelDirectional( ZZZImage<UChar> & image,
                         int iterations,
                         const char * directionSequence,
                         int adjacency,
                         TerminalPointTest terminalTest,
                         bool binarize );

Size
markTerminalPoints( ZZZImage<UChar> & image,
                    std::vector< Triple<SSize> > & border,
                    std::vector< Triple<SSize> > & surfacePoints,
                    TerminalPointTest terminalTest );

#endif // _SKEL_H_

