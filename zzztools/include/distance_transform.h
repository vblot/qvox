/** -*- mode: c++ -*-
 * @file   distance_transform.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:41 2005
 *
 * @brief  Linear time Euclidean distance transform.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DISTANCE_TRANSFORM_H_
#define _DISTANCE_TRANSFORM_H_
#include <cmath>
#include <limits>
#include <zzzimage.h>
#include <config.h>

#include <limits>

/*
 * This file (together with distance_transform.cpp) is an almost
 * verbatim copy of the code written by David Coeurjolly, put on
 * the Code repository of the IAPR TC18's website.
 * http://www.cb.uu.se/~tc18/code_data_set/code.html
 *
 * The Euclidean distance tranform is computed in linear time by the function
 * below. This is thanks to the results of the following papers:
 *
 *  [ST94] T. Saito and J. I. Toriwaki. New algorithms for Euclidean distance
 *  transformations of an /n/-dimensional digitized picture with applications.
 *  Pattern Recognition, 27:1551-1565, 1994.
 *
 *  [MRH00] A. Meijster, J.B.T.M. Roerdink, and W. H. Hesselink.
 *  A general algorithm for computing distance transforms in linear time.
 *  In Mathematical Morphology and its Applications to Image and Signal Processing,
 *  pages 331-340. Kluwer, 2000.
 *
 *  [Hir96] T. Hirata. A unified linear-time algorithm for computing distance maps.
 *  /Information Processing Letters/, 58(3):129-133, May 1996.
 *
 */


template<typename T>
void distanceTransform( const ZZZImage<T> & image, ZZZImage<Int32> & distMap );

template<typename T>
void distanceTransformNoBorder( const ZZZImage<T> & image, ZZZImage<Int32> & distMap );



#endif // _DISTANCE_TRANSFORM_H_
