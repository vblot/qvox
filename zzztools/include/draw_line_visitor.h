/** -*- mode: c++ -*-
 * @file   draw_line_visitor.h
 * @author Sebastien Fourey (GREYC)
 * @date   Fri Nov 11 20:59:35 2005
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DRAW_LINE_VISITOR_H_
#define _DRAW_LINE_VISITOR_H_

#include "image_visitor.h"
#include "globals.h"
#include "Triple.h"
#include "zzzimage.h"
#include <cstdlib>

class DrawLineVisitor : public AbstractImageVisitor {

public:

  DrawLineVisitor( SSize x1, SSize y1, SSize z1,
                   SSize x2, SSize y2, SSize z2,
                   UChar red, UChar green, UChar blue );
  DrawLineVisitor( Triple<SSize> pixel1, Triple<SSize> pixel2, Triple<UChar> color );

  template <typename T>  void visitAny( ZZZImage<T> & );

  void visit( ZZZImage<UChar> &  );
  void visit( ZZZImage<Short> &  );
  void visit( ZZZImage<UShort> &  );
  void visit( ZZZImage<Int32> &  );
  void visit( ZZZImage<UInt32> &  );
  void visit( ZZZImage<Float> &  );
  void visit( ZZZImage<Double> &  );
  void visit( ZZZImage<Bool> &  );

private:
  SSize _x1,_y1,_z1;
  SSize _x2,_y2,_z2;
  UChar _red,_green,_blue;
};

template <typename T>
void
DrawLineVisitor::visitAny( ZZZImage<T> & image )
{
  Triple<SSize> pixel(_x1,_y1,_z1);
  Triple<SSize> delta( _x2-_x1, _y2-_y1, _z2-_z1);
  Triple<UChar> color(_red,_green,_blue);
  SSize x_inc, y_inc, z_inc;
  x_inc = (delta.first < 0) ? -1 : 1;
  y_inc = (delta.second < 0) ? -1 : 1;
  z_inc = (delta.third < 0) ? -1 : 1;

  SSize width, height, depth;
  width = labs( delta.first );
  height = labs( delta.second );
  depth = labs( delta.third );
  Triple<SSize> delta2( width<<1, height<<1, depth<<1 );

  SSize err_1, err_2;
  SSize i;
  if ( image.bands() != 3 ) {
    if ( (width >= height) && (width >= depth) ) {
      err_1 = delta2.second - width;
      err_2 = delta2.third - width;
      for ( i = 0; i < width; ++i ) {
        image( pixel ) = color.first;
        if ( err_1 > 0 ) {
          pixel.second += y_inc;
          err_1 -= delta2.first;
        }
        if ( err_2 > 0 ) {
          pixel.third += z_inc;
          err_2 -= delta2.first;
        }
        err_1 += delta2.second;
        err_2 += delta2.third;
        pixel.first += x_inc;
      }
    } else if ( (height >= width) && (height >= depth) ) {
      err_1 = delta2.first - height;
      err_2 = delta2.third - height;
      for (i = 0; i < height; ++i ) {
        image( pixel ) = color.first;
        if ( err_1 > 0 ) {
          pixel.first += x_inc;
          err_1 -= delta2.second;
        }
        if ( err_2 > 0 ) {
          pixel.third += z_inc;
          err_2 -= delta2.second;
        }
        err_1 += delta2.first;
        err_2 += delta2.third;
        pixel.second += y_inc;
      }
    } else {
      err_1 = delta2.second - depth;
      err_2 = delta2.first - depth;
      for ( i = 0; i < depth; ++i ) {
        image( pixel ) = color.first;
        if ( err_1 > 0 ) {
          pixel.second += y_inc;
          err_1 -= delta2.third;
        }
        if ( err_2 > 0 ) {
          pixel.first += x_inc;
          err_2 -= delta2.third;
        }
        err_1 += delta2.second;
        err_2 += delta2.first;
        pixel.third += z_inc;
      }
    }
    image( pixel ) = color.first;

  } else {

    // Color image (3 bands)
    if ( (width >= height) && (width >= depth) ) {
      err_1 = delta2.second - width;
      err_2 = delta2.third - width;
      for ( i = 0; i < width; ++i ) {
        image( pixel, 0 ) = color.first;
        image( pixel, 1 ) = color.second;
        image( pixel, 2 ) = color.third;
        if ( err_1 > 0 ) {
          pixel.second += y_inc;
          err_1 -= delta2.first;
        }
        if ( err_2 > 0 ) {
          pixel.third += z_inc;
          err_2 -= delta2.first;
        }
        err_1 += delta2.second;
        err_2 += delta2.third;
        pixel.first += x_inc;
      }
    } else if ( (height >= width) && (height >= depth) ) {
      err_1 = delta2.first - height;
      err_2 = delta2.third - height;
      for (i = 0; i < height; ++i ) {
        image( pixel, 0 ) = color.first;
        image( pixel, 1 ) = color.second;
        image( pixel, 2 ) = color.third;
        if ( err_1 > 0 ) {
          pixel.first += x_inc;
          err_1 -= delta2.second;
        }
        if ( err_2 > 0 ) {
          pixel.third += z_inc;
          err_2 -= delta2.second;
        }
        err_1 += delta2.first;
        err_2 += delta2.third;
        pixel.second += y_inc;
      }
    } else {
      err_1 = delta2.second - depth;
      err_2 = delta2.first - depth;
      for ( i = 0; i < depth; ++i ) {
        image( pixel, 0 ) = color.first;
        image( pixel, 1 ) = color.second;
        image( pixel, 2 ) = color.third;
        if ( err_1 > 0 ) {
          pixel.second += y_inc;
          err_1 -= delta2.third;
        }
        if ( err_2 > 0 ) {
          pixel.first += x_inc;
          err_2 -= delta2.third;
        }
        err_1 += delta2.second;
        err_2 += delta2.first;
        pixel.third += z_inc;
      }
    }
    image( pixel, 0 ) = color.first;
    image( pixel, 1 ) = color.second;
    image( pixel, 2 ) = color.third;
  }
}



#endif // _DRAW_LINE_VISITOR_H_

