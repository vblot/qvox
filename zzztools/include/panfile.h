/* -*- mode: c++ -*-
 * @(#)panfile.h		PANDORE		2001-04-03
 *
 * PANTHEON Project
 *
 * GREYC IMAGE
 * 6 Boulevard Mar�chal Juin
 * F-14050 Caen Cedex France
 *
 * This file is free software. You can use it, distribute it
 * and/or modify it. However, the entire risk to the quality
 * and performance of this program is with you.
 *
 * Please email any bugs, comments, and/or additions to this file to:
 * Regis.Clouard@greyc.ensicaen.fr
 * http://www.greyc.ensicaen.fr/~regis
 */

/*
 * (C)R�gis Clouard - 2001-09-24 (Version 3,0)
 * (R)R�gis Clouard - 2002-12-05 (Version 4.0)
 * (R)S�bastien Fourey - 2005 ( Flight-weigthted version )
 */

/*
 *                       -- IMPORTANT NOTE --
 *
 *  This file is a modified version of the pandore original files.
 *  It is a lightenned version, specially rewritten for the QVox
 *  visualization tool.
 *  (C)S�bastien Fourey - GREYC ENSICAEN - France
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _PANFILE_H_
#define _PANFILE_H_
#include <globals.h>
#include <string>
#include <Pair.h>

extern const char * POtypeName[];
extern const char * POvalueTypeName[];
extern const char * POShortValueTypeName[];
extern const char * ColorSpaceNames[ 15 ];

typedef enum {
  Po_ValUnknown,
  Po_ValUC, Po_ValSL, Po_ValSF, Po_ValUL, Po_ValSS, Po_ValSD,
  Po_ValB, Po_ValUS
} ValueType;

const int ValueTypeSize[] = {
  0, 1, 4, 4, 4, 2, 8, 125, 2
};

typedef enum {
   Po_Object,
   Po_Collection,
   Po_Img1duc, Po_Img1dsl, Po_Img1dsf,
   Po_Img2duc, Po_Img2dsl, Po_Img2dsf,
   Po_Img3duc, Po_Img3dsl, Po_Img3dsf,
   Po_Reg1d, Po_Reg2d, Po_Reg3d,
   Po_Graph2d, Po_Graph3d,
   Po_Imc2duc, Po_Imc2dsl, Po_Imc2dsf,
   Po_Imc3duc, Po_Imc3dsl, Po_Imc3dsf,
   Po_Imx1duc, Po_Imx1dsl, Po4_Imx1dul, Po_Imx1dsf,
   Po_Imx2duc, Po_Imx2dsl, Po4_Imx2dul, Po_Imx2dsf,
   Po_Imx3duc, Po_Imx3dsl, Po4_Imx3dul, Po_Imx3dsf,
   Po_Point1d, Po_Point2d, Po_Point3d,

   Po_Img1dss, Po_Img2dss, Po_Img3dss,
   Po_Imc1dss, Po_Imc2dss, Po_Imc3dss,
   Po_Imx1dss, Po_Imx2dss, Po_Imx3dss,

   Po4_Img1dul, Po4_Img2dul, Po4_Img3dul,

   Po4_Imc2dul,
   Po4_Imc3dul,

   Po_Imx1dsd, Po_Imx2dsd, Po_Imx3dsd,
   Po_Imx1db, Po_Imx2db, Po_Imx3db,
   Po_Imx1dus, Po_Imx2dus, Po_Imx3dus
} Typobj;

/*
 * Po_type is an helper class that defines the
 * type identifier of each known object type.
 */
template< class T >
struct Po_type {
 enum { type = Po_Object };
};

template< class T >
struct TypeName {
  static std::string name() { return "unknown"; }
};

template<> struct TypeName <Char> {
   static std::string name() { return "Char"; }
};
template<> struct TypeName <UChar> {
   static std::string name() { return "UChar"; }
};
template<> struct TypeName <Short> {
   static std::string name() { return "Short"; }
};
template<> struct TypeName <UShort> {
   static std::string name() { return "UShort"; }
};
template<> struct TypeName <Int32> {
   static std::string name() { return "Long"; }
};
template<> struct TypeName <UInt32> {
   static std::string name() { return "ULong"; }
};
template<> struct TypeName <Float> {
   static std::string name() { return "Float"; }
};
template<> struct TypeName <Double> {
   static std::string name() { return "Double"; }
};
template<> struct TypeName <Bool> {
   static std::string name() { return "Bool"; }
};

template < class T, int dimension >
struct ImxFileType {  static const Typobj type = Po_Object; };

template<>
struct ImxFileType< UChar, 1 > {  static const Typobj type = Po_Imx1duc; };
template<>
struct ImxFileType< UChar, 2 > {  static const Typobj type = Po_Imx2duc; };
template<>
struct ImxFileType< UChar, 3 > {  static const Typobj type = Po_Imx3duc; };

template<>
struct ImxFileType< Short, 1 > {  static const Typobj type = Po_Imx1dss; };
template<>
struct ImxFileType< Short, 2 > {  static const Typobj type = Po_Imx2dss; };
template<>
struct ImxFileType< Short, 3 > {  static const Typobj type = Po_Imx3dss; };

template<>
struct ImxFileType< UInt32, 1 > {  static const Typobj type = Po4_Imx1dul; };
template<>
struct ImxFileType< UInt32, 2 > {  static const Typobj type = Po4_Imx2dul; };
template<>
struct ImxFileType< UInt32, 3 > {  static const Typobj type = Po4_Imx3dul; };

template<>
struct ImxFileType< Int32, 1 > {  static const Typobj type = Po_Imx1dsl; };
template<>
struct ImxFileType< Int32, 2 > {  static const Typobj type = Po_Imx2dsl; };
template<>
struct ImxFileType< Int32, 3 > {  static const Typobj type = Po_Imx3dsl; };

template<>
struct ImxFileType< Float, 1 > {  static const Typobj type = Po_Imx1dsf; };
template<>
struct ImxFileType< Float, 2 > {  static const Typobj type = Po_Imx2dsf; };
template<>
struct ImxFileType< Float, 3 > {  static const Typobj type = Po_Imx3dsf; };

template<>
struct ImxFileType< Double, 1 > {  static const Typobj type = Po_Imx1dsd; };
template<>
struct ImxFileType< Double, 2 > {  static const Typobj type = Po_Imx2dsd; };
template<>
struct ImxFileType< Double, 3 > {  static const Typobj type = Po_Imx3dsd; };

template<>
struct ImxFileType< Bool, 1 > {  static const Typobj type = Po_Imx1db; };
template<>
struct ImxFileType< Bool, 2 > {  static const Typobj type = Po_Imx2db; };
template<>
struct ImxFileType< Bool, 3 > {  static const Typobj type = Po_Imx3db; };

template<>
struct ImxFileType< UShort, 1 > {  static const Typobj type = Po_Imx1dus; };
template<>
struct ImxFileType< UShort, 2 > {  static const Typobj type = Po_Imx2dus; };
template<>
struct ImxFileType< UShort, 3 > {  static const Typobj type = Po_Imx3dus; };

const int POdimension[] = {
  /* Object */ 0,
  /* Po_Collection */ 0,
  /* Po_Img1duc */ 1, /* Po_Img1dsl */ 1, /* Po_Img1dsf */ 1,
  /* Po_Img2duc */ 2, /* Po_Img2dsl */ 2, /* Po_Img2dsf */ 2,
  /* Po_Img3duc */ 3, /* Po_Img3dsl */ 3, /* Po_Img3dsf */ 3,
  /* Po_Reg1d */ -1, /* Po_Reg2d */ -2, /* Po_Reg3d */ -3 ,
  /* Po_Graph2d */ 0, /* Po_Graph3d */ 0 ,
  /* Po_Imc2duc */ 2, /* Po_Imc2dsl */ 2 , /* Po_Imc2dsf */ 2,
  /* Po_Imc3duc */ 3, /* Po_Imc3dsl */ 3, /* Po_Imc3dsf */ 3,
  /* Po_Imx1duc */ 1, /* Po_Imx1dsl */ 1,
  /* Po4_Imx1dul */ 1, /* Po_Imx1dsf */ 1 ,
  /* Po_Imx2duc */ 2, /* Po_Imx2dsl */ 2,
  /* Po4_Imx2dul */ 2, /* Po_Imx2dsf */ 2 ,
  /* Po_Imx3duc */ 3, /* Po_Imx3dsl */ 3,
  /* Po4_Imx3dul */ 3, /* Po_Imx3dsf */ 3,
  /* Po_Point1d */ 0, /* Po_Point2d */ 0, /* Po_Point3d */ 0,

  /* Po_Img1dss */ 1, /* Po_Img2dss */ 2, /* Po_Img3dss */ 3,
  /* Po_Imc1dss */ 1, /* Po_Imc2dss */ 2, /* Po_Imc3dss */ 3,
  /* Po_Imx1dss */ 1, /* Po_Imx2dss */ 2, /* Po_Imx3dss */ 3,

  /* Po4_Img1dul */ 1,   /* Po4_Img2dul */ 2,   /* Po4_Img3dul */ 3,
  /* Po4_Imc2dul */ 2,  /* Po4_Imc3dul */ 3 ,

  /* Po_Imx1dsd */ 1, /* Po_Imx2dsd */ 2, /* Po_Imx3dsd */ 3,

  /* Po_Imx1db */ 1, /* Po_Imx2db */ 2, /* Po_Imx3db */ 3,
  /* Po_Imx1dus */ 1, /* Po_Imx2dus */ 2, /* Po_Imx3dus */ 3
};

enum ColorSpace { RGB, XYZ, LUV, LAB, HSL, AST, I1I2I3, LCH,
      WRY, RNGNBN, YCBCR, YCH1CH2, YIQ,
      YUV, UNKNOWN };

const bool POColor[] = {
  /* Object */ false,
  /* Po_Collection */ false,
  /* Po_Img1duc */ false, /* Po_Img1dsl */ false, /* Po_Img1dsf */ false,
  /* Po_Img2duc */ false, /* Po_Img2dsl */ false, /* Po_Img2dsf */ false,
  /* Po_Img3duc */ false, /* Po_Img3dsl */ false, /* Po_Img3dsf */ false,
   /* Po_Reg1d */ false, /* Po_Reg2d */ false, /* Po_Reg3d */ false ,
  /* Po_Graph2d */ false, /* Po_Graph3d */ false,
  /* Po_Imc2duc */ true, /* Po_Imc2dsl */ true , /* Po_Imc2dsf */ true,
  /* Po_Imc3duc */ true, /* Po_Imc3dsl */ true, /* Po_Imc3dsf */ true,
  /* Po_Imx1duc */ false, /* Po_Imx1dsl */ false,
  /* Po4_Imx1dul */ false, /* Po_Imx1dsf */ false ,
  /* Po_Imx2duc */ false, /* Po_Imx2dsl */ false,
  /* Po4_Imx2dul */ false, /* Po_Imx2dsf */ false ,
  /* Po_Imx3duc */ false, /* Po_Imx3dsl */ false,
  /* Po4_Imx3dul */ false, /* Po_Imx3dsf */ false,
  /* Po_Point1d */ false, /* Po_Point2d */ false, /* Po_Point3d */ false,

  /* Po_Img1dss */ false, /* Po_Img2dss */ false, /* Po_Img3dss */ false,
  /* Po_Imc1dss */ true, /* Po_Imc2dss */ true, /* Po_Imc3dss */ true,
  /* Po_Imx1dss */ false, /* Po_Imx2dss */ false, /* Po_Imx3dss */ false,

  /* Po4_Img1dul */ false,   /* Po4_Img2dul */ false,   /* Po4_Img3dul */ false,
  /* Po4_Imc2dul */ true,  /* Po4_Imc3dul */ true ,

  /* Po_Imx1dsd */ false, /* Po_Imx2dsd */ false, /* Po_Imx3dsd */ false,

  /* Po_Imx1db */ false, /* Po_Imx2db */ false, /* Po_Imx3db */ false,
  /* Po_Imx1dus */ false, /* Po_Imx2dus */ false, /* Po_Imx3dus */ false
};

const bool POHasBands[] = {
  /* Object */ false,
  /* Po_Collection */ false,
  /* Po_Img1duc */ false, /* Po_Img1dsl */ false, /* Po_Img1dsf */ false,
  /* Po_Img2duc */ false, /* Po_Img2dsl */ false, /* Po_Img2dsf */ false,
  /* Po_Img3duc */ false, /* Po_Img3dsl */ false, /* Po_Img3dsf */ false,
   /* Po_Reg1d */ false, /* Po_Reg2d */ false, /* Po_Reg3d */ false ,
  /* Po_Graph2d */ false, /* Po_Graph3d */ false,
  /* Po_Imc2duc */ true, /* Po_Imc2dsl */ true , /* Po_Imc2dsf */ true,
  /* Po_Imc3duc */ true, /* Po_Imc3dsl */ true, /* Po_Imc3dsf */ true,
  /* Po_Imx1duc */ true, /* Po_Imx1dsl */ true,
  /* Po4_Imx1dul */ true, /* Po_Imx1dsf */ true ,
  /* Po_Imx2duc */ true, /* Po_Imx2dsl */ true,
  /* Po4_Imx2dul */ true, /* Po_Imx2dsf */ true ,
  /* Po_Imx3duc */ true, /* Po_Imx3dsl */ true,
  /* Po4_Imx3dul */ true, /* Po_Imx3dsf */ true,
  /* Po_Point1d */ false, /* Po_Point2d */ false, /* Po_Point3d */ false,

  /* Po_Img1dss */ false, /* Po_Img2dss */ false, /* Po_Img3dss */ false,
  /* Po_Imc1dss */ true, /* Po_Imc2dss */ true, /* Po_Imc3dss */ true,
  /* Po_Imx1dss */ true, /* Po_Imx2dss */ true, /* Po_Imx3dss */ true,

  /* Po4_Img1dul */ false,   /* Po4_Img2dul */ false,   /* Po4_Img3dul */ false,
  /* Po4_Imc2dul */ true,  /* Po4_Imc3dul */ true ,

  /* Po_Imx1dsd */ true, /* Po_Imx2dsd */ true, /* Po_Imx3dsd */ true,

  /* Po_Imx1db */ true, /* Po_Imx2db */ true, /* Po_Imx3db */ true,
  /* Po_Imx1dus */ true, /* Po_Imx2dus */ true, /* Po_Imx3dus */ true
};


const ValueType POvalueType[] = {
  /* Object */ Po_ValUnknown,
  /* Po_Collection */ Po_ValUnknown ,
  /* Po_Img1duc */ Po_ValUC, /* Po_Img1dsl */ Po_ValSL,
  /* Po_Img1dsf */ Po_ValSF,
  /* Po_Img2duc */ Po_ValUC, /* Po_Img2dsl */ Po_ValSL,
  /* Po_Img2dsf */ Po_ValSF,
  /* Po_Img3duc */ Po_ValUC, /* Po_Img3dsl */ Po_ValSL,
  /* Po_Img3dsf */ Po_ValSF,
  /* Po_Reg1d */ Po_ValUnknown,
  /* Po_Reg2d */ Po_ValUnknown,
  /* Po_Reg3d */ Po_ValUnknown ,
  /* Po_Graph2d */ Po_ValUnknown, /* Po_Graph3d */ Po_ValUnknown ,
  /* Po_Imc2duc */ Po_ValUC, /* Po_Imc2dsl */ Po_ValSL,
  /* Po_Imc2dsf */ Po_ValSF,
  /* Po_Imc3duc */ Po_ValUC, /* Po_Imc3dsl */ Po_ValSL,
  /* Po_Imc3dsf */ Po_ValSF,
  /* Po_Imx1duc */ Po_ValUC, /* Po_Imx1dsl */ Po_ValSL,
  /* Po4_Imx1dul */ Po_ValUL, /* Po_Imx1dsf */ Po_ValSF ,
  /* Po_Imx2duc */ Po_ValUC, /* Po_Imx2dsl */ Po_ValSL,
  /* Po4_Imx2dul */ Po_ValUL, /* Po_Imx2dsf */ Po_ValSF ,
  /* Po_Imx3duc */ Po_ValUC, /* Po_Imx3dsl */ Po_ValSL,
  /* Po4_Imx3dul */ Po_ValUL, /* Po_Imx3dsf */ Po_ValSF,
  /* Po_Point1d */ Po_ValUnknown,
  /* Po_Point2d */ Po_ValUnknown,
  /* Po_Point3d */ Po_ValUnknown,

  /* Po_Img1dss */ Po_ValSS, /* Po_Img2dss */ Po_ValSS, /* Po_Img3dss */ Po_ValSS,
  /* Po_Imc1dss */ Po_ValSS, /* Po_Imc2dss */ Po_ValSS, /* Po_Imc3dss */ Po_ValSS,
  /* Po_Imx1dss */ Po_ValSS, /* Po_Imx2dss */ Po_ValSS, /* Po_Imx3dss */ Po_ValSS,

  /* Po4_Img1dul */ Po_ValUL,   /* Po4_Img2dul */ Po_ValUL,   /* Po4_Img3dul */ Po_ValUL,
  /* Po4_Imc2dul */ Po_ValUL,  /* Po4_Imc3dul */ Po_ValUL ,

  /* Po_Imx1dsd */ Po_ValSD, /* Po_Imx2dsd */ Po_ValSD, /* Po_Imx3dsd */ Po_ValSD,

  /* Po_Imx1db */ Po_ValB, /* Po_Imx2db */ Po_ValB, /* Po_Imx3db */ Po_ValB,
  /* Po_Imx1dus */ Po_ValUS, /* Po_Imx2dus */ Po_ValUS, /* Po_Imx3dus */ Po_ValUS
};


#define	PO_MAGIC "PANDORE05"

struct Po_header {
  char magic[12];    /**< The magic number (12 bytes) PO_MAGIC. */
  Typobj potype;    /**< The object type (2 bytes on a 32 bit proc.). */
  char ident[9];     /**< The autor name (9 bytes + 1 complement). */
  char date[10];     /**< The creation date (10 bytes). */
  char dummy[1];
};

#endif // _PPANFILE_H_
