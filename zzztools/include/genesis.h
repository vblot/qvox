/** -*- mode: c++ -*-
 * @file   genesis.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2005
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _GENESIS_H_
#define _GENESIS_H_

#include <iostream>
#include <cmath>
using std::string;
using std::istream;
using std::ostream;
#include "globals.h"
#include "Matrix.h"
#include "Triple.h"
#include "zzzimage.h"

struct Rotation {
  Triple<double> center;
  double rho, theta, phi;
  Matrix mRotation;
  Matrix mInverseRotation;
  bool valid;
  double sine_rho,sine_theta,sine_phi;
  double cosine_rho,cosine_theta,cosine_phi;

  Rotation();
  Rotation( Triple<double> center, double rho, double theta, double phi );
  void set( Triple<double> center, double rho, double theta, double phi );
  Triple<double> apply( Triple<double> point );
  Triple<double> reverse( Triple<double> point );
  void clear();
};

/**
 *
 *
 * @param image
 * @param center
 * @param radius
 * @param value
 * @param translation
 * @param scale
 */
template< typename T >
void genesisSphere( ZZZImage< T > & image,
                    Triple<SSize> center,
                    SSize radius, T value,
                    Triple<double> scale );


template< typename T >
void genesisEllipsoid( ZZZImage< T > & image,
                       Triple<SSize> center, Triple<SSize> radii,
                       T value,
                       Rotation rotation, Triple<double> scale );


/**
 * (x/a)^2 - (y/b)^2 - z = 0
 *
 * @param image
 * @param center
 * @param a
 * @param b
 * @param value
 * @param rotation
 * @param scale
 */
template< typename T >
void genesisHyperbolicParaboloid( ZZZImage<T> & image,
                                  Triple<SSize> center,
                                  double a, double b,
                                  T value,
                                  Rotation rotation,
                                  Triple<double> scale );

/**
 * (x/a)^2 + (y/b)^2 - z = 0
 *
 * @param image
 * @param center
 * @param a
 * @param b
 * @param value
 * @param rotation
 * @param scale
 */
template< typename T >
void genesisParaboloid( ZZZImage<T> & image,
                        Triple<SSize> center,
                        double a, double b,
                        T value,
                        Rotation rotation,
                        Triple<double> scale );

/**
 * ax + by + cz <= 0
 *
 * @param image
 * @param center The position of the origin (0,0,0).
 * @param normal The vector (a,b,c).
 * @param Value
 * @param rotation
 */
template< typename T >
void genesisPlane( ZZZImage<T> & image,
                   Triple<SSize> center,
                   Triple<double> normal,
                   T value,
                   Rotation rotation );

/**
 * Generates a Torus.
 *
 * @param image
 * @param center
 * @param largeRadius
 * @param smallRadius
 * @param value
 * @param rotation
 * @param scale
 */
template< typename T >
void genesisTorus( ZZZImage<T> & image,
                   Triple<SSize> center,
                   SSize largeRadius,
                   SSize smallRadius,
                   T value,
                   Rotation rotation,
                   Triple<double> scale );

/**
 * Generates a box.
 *
 * @param image
 * @param p1
 * @param p2
 * @param value
 * @param rotation
 * @param scale
 * @param translation
 */
template< typename T >
void genesisBox( ZZZImage< T > & image,
                 Triple<SSize> p1, Triple<SSize> p2,
                 T value,
                 Rotation rotation,
                 Triple<double> scale,
                 Triple<SSize> translation );

/**
 * Generates a cylinder.
 *
 * @param image
 * @param center
 * @param radius
 * @param h
 * @param value
 * @param rotation
 * @param scale
 * @param translation
 */
template< typename T >
void genesisCylinder( ZZZImage< T > & image,
                      Triple<SSize> center,
                      SSize radius,
                      SSize h,
                      T value,
                      Rotation rotation,
                      Triple<double> scale,
                      Triple<SSize> translation);


/**
 * Generates an axis.
 *
 * @param image
 * @param center
 * @param value
 */
template< typename T >
void genesisAxis( ZZZImage< T > & image,
                  Triple<SSize> center,
                  T value );


/**
 * Generates a conical volume.
 *
 * @param image
 * @param center
 * @param axis
 * @param angle
 * @param value
 * @param rotation
 */
template< typename T >
void genesisConical( ZZZImage< T > & image,
                     Triple<SSize> center,
                     Triple<Double> axis,
                     double angle,
                     SSize h,
                     T value,
                     Rotation rotation,
                     Triple<double> scale );


template< typename T >
void genesisPyramid( ZZZImage< T > & image,
                     Triple<Double> baseCenter,
                     Triple<Double> topDirection,
                     double height,
                     Triple<Double> baseSmallSideDirection,
                     double baseWidth,
                     double baseHeight,
                     T value );

#endif // _GENESIS_H_
