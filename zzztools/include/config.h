/** -*- mode: c++ -*-
 * @file   config.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2005
 * 
 * @brief Declaration of a 3x3x3 configuration type (i.e., an
 * unsigned long bit mask) and several related functions.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <iostream>
#include <cmath>

#include <sstream>

#include "globals.h"
#include "Triple.h"

/*
 * The config type
 */
typedef unsigned long Config;

typedef bool (*SimplePointTest)(Config);
typedef bool (*PSimplePointTest)(Config , Config);
typedef UChar (*TerminalPointTest)(Config);

extern SimplePointTest SkelSimpleTests[4];
extern PSimplePointTest SkelPSimpleTests[4];
extern TerminalPointTest SkelTerminalTests[10][5];

#define ADJ6  0
#define ADJ6P 1
#define ADJ18 2
#define ADJ26 3

#define CENTER_INDEX 13
#define FIN 50

#define MASK_CONFIG 0x07FFFFFF
#define MASK_CENTER 0x00002000
#define MASK_26_NEIGHBORS 0x07FFDFFF
#define MASK_18_NEIGHBORS 0x2EBDEBA
#define MASK_6_NEIGHBORS  0x415410
#define MASK_6P_NEIGHBORS 0x415410

#define MASK_4  0x10
#define MASK_10 0x400
#define MASK_12 0x1000
#define MASK_14 0x4000
#define MASK_16 0x10000
#define MASK_22 0x400000

#define MASK_0 0x1
#define MASK_1 0x2
#define MASK_2 0x4
#define MASK_3 0x8

#define MASK_5 0x20
#define MASK_6 0x40
#define MASK_7 0x80
#define MASK_8 0x100
#define MASK_9 0x200

#define MASK_11 0x800
#define MASK_13 0x2000
#define MASK_15 0x8000

#define MASK_17 0x20000
#define MASK_18 0x40000
#define MASK_19 0x80000
#define MASK_20 0x100000
#define MASK_21 0x200000

#define MASK_23 0x800000
#define MASK_24 0x1000000
#define MASK_25 0x2000000
#define MASK_26 0x4000000

#define MASK_CUBE0 0x361B
#define MASK_CUBE1 0x6C36
#define MASK_CUBE2 0x1B0D8
#define MASK_CUBE3 0x361B0
#define MASK_CUBE4 0x6C3600 
#define MASK_CUBE5 0xD86C00
#define MASK_CUBE6 0x361B000
#define MASK_CUBE7 0x6C36000

/*
 * Exported variables
 */

// extern const Config TAB_MASK_VOISINS[4];
// extern const char ORDRE_USUEL[4];
// extern const char LCHEMIN[4];
// extern const char ORDRE_USUEL_TESTS[4];

extern const char AdjacencyNames[4][3];	/**< "6", "6P", "18", "26" */
extern const char ComplementaryAdjacency[4];
extern const char DeltaToIndex[3][3][3];

extern const int IndexToDeltaX[27];
extern const int IndexToDeltaY[27];
extern const int IndexToDeltaZ[27];
extern const int IndexToDelta[27][3];
extern const UInt32 IndexToAdjacencyMask[4][32];

extern const Triple< SSize > ConfigShifts[27];
extern const Triple< SSize > NeighborsShifts26[26];
extern const Triple< SSize > NeighborsShifts18[18];
extern const Triple< SSize > NeighborsShifts6[6];


extern const bool Simple8[256];
extern const bool Simple4[256];

#define NEIGHBOR(ARRAY,X,Y,Z,DIRECTION) ARRAY\
      [ (X) + IndexToDeltaX[DIRECTION] ]\
      [ (Y) + IndexToDeltaY[DIRECTION] ]\
      [ (Z) + IndexToDeltaZ[DIRECTION] ]

/** 
 * Checks whether two points, given by there indices in the 
 * 3x3x3 cube, are adjacent.
 * 
 * @param p1 Index of a first point.
 * @param p2 Index of a second point.
 * @param adjacency The adjacency to be considered.
 * 
 * @return 
 */
bool adjacentPoints(UChar p1, UChar p2, int adjacency);

/** 
 * Complementary of a configuration.
 * 
 * @param c A 3x3x3 configuration.
 * @return The complementary configuration.
 */
Config cnfComplementary(Config c); 

/** 
 * Random configuration
 *
 * @return A randomly generated configuration.
 */
Config cnfRandom();

/** 
 * Build a 0/1 string from a config.
 * 
 * @param c A 3x3x3 configuration.
 * 
 * @return A string made of 0 or 1 according to the
 * points in the configuration, from 0 to 26. 
 */
std::string cnfToString( Config c );

/** 
 * Converts a configuration into a string of 0 and 1s.
 * 
 * @param c A 3x3x3 configuration
 */
std::string cnfTo3DString( Config c );

/** 
 * Write a configuration into a stream.
 * 
 * @param c A 3x3x3 configuration
 */
std::ostream & cnfWriteInStream( std::ostream & stream, Config c );

/** 
 * Read a configuration from a stream.
 * 
 * @param stream Stream from which the config should be read.
 * @return The read configuration.
 */
Config cnfReadFromStream( std::istream & stream );


/** 
 * Check whether a configuration is a "bold point".
 * 
 * @param c A 3x3x3 configuration.
 * 
 * @return true if the point is bold, false otherwise.
 */
bool cnfBoldPoint(Config c);


/** 
 * Number of 1s in a configuration.
 * 
 * @param c A 3x3x3 configuration.
 * 
 * @return The number of 1s.
 */
int cnfCount(Config c);


float cnfCurvature(Config c);
float cnfCurvatureObject(Config c);
float cnfCurvatureBackground(Config c);


/** 
 * Builts and returns the (6,26)-geodesic neighborhood
 * (rank 2) from a given configuration.
 * 
 * @param cnf A 3x3x3 configuration.
 * 
 * @return The 6 geodesic neighborhood.
 */
Config cnfGeodesic_6Neighborhood( Config cnf );

/** 
 * Builts and returns the (6,18)-geodesic neighborhood
 * (rank 2) from a given configuration.
 * 
 * @param cnf A 3x3x3 configuration.
 * 
 * @return The (6,18) geodesic neighborhood.
 */
Config cnfGeodesic_6PNeighborhood( Config cnf );

/** 
 * Builts and returns the (18,6)-geodesic neighborhood
 * (rank 2) from a given configuration.
 * 
 * @param cnf A 3x3x3 configuration.
 * 
 * @return The (18,6) geodesic neighborhood.
 */
Config cnfGeodesic_18Neighborhood( Config cnf );

/** 
 * Builts and returns the (26,6)-geodesic neighborhood
 * (rank 2) from a given configuration.
 * 
 * @param cnf A 3x3x3 configuration.
 * 
 * @return The (26,6) geodesic neighborhood.
 */
Config cnfGeodesic_26Neighborhood( Config cnf );


/** 
 * Checks whether a config has a single 6-connected component.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 6-connected component, false otherwise.
 */
bool cnfSingle6CC( const Config c );

/** 
 * Checks whether a config has a single 18-connected component.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 18-connected component, false otherwise.
 */
bool cnfSingle18CC( const Config c );

/** 
 * Checks whether a config has a single 26-connected component.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 26-connected component, false otherwise.
 */
bool cnfSingle26CC( const Config c );

/** 
 * Checks whether a config has a single 6-connected component
 * using a queue.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 6-connected component, false otherwise.
 */
bool cnfSingle6CC_Queue(const Config c);

/** 
 * Checks whether a config has a single 18-connected component
 * using a queue.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 6-connected component, false otherwise.
 */
bool cnfSingle18CC_Queue(const Config c);

/** 
 * Checks whether a config has a single 26-connected component
 * using a queue.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single 6-connected component, false otherwise.
 */
bool cnfSingle26CC_Queue(const Config c);


/** 
 * Checks whether a config has a single connected component
 * using a queue, provided a set of neighbors lists.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single connected component, false otherwise.
 */
bool cnfSingleCC_Queue( const Config c, const int neighborsLists[][32] );


/** 
 * Checks whether a config has two connected component
 * using a queue, provided a set of neighbors lists.
 * 
 * @param c A 3x3x3 configuration.
 * @return true if c has a single connected component, false otherwise.
 */
bool cnfTwoCC_Queue( const Config c, const int neighborsLists[][32] );

/** 
 * Checks wheter a 3x3x3 configuration is that of a 6-simple voxel.
 * 
 * @param c A configuration.
 * @return True if the central voxel of c is 6-simple, false otherwise.
 */
bool cnfSimple6(Config c);

/** 
 * Checks wheter a 3x3x3 configuration is that of a (6+)-simple voxel.
 * 
 * @param c A configuration.
 * @return True if the central voxel of c is (6+)-simple, false otherwise.
 */
bool cnfSimple6P(Config c);

/** 
 * Checks wheter a 3x3x3 configuration is that of a 18-simple voxel.
 * 
 * @param c A configuration.
 * @return True if the central voxel of c is 18-simple, false otherwise.
 */
bool cnfSimple18(Config c);

/** 
 * Checks wheter a 3x3x3 configuration is that of a 26-simple voxel.
 * 
 * @param c A configuration.
 * @return True if the central voxel of c is 26-simple, false otherwise.
 */
bool cnfSimple26(Config c);


/** 
 * Checks wheter a 3x3x3 configuration is that of a 6-Psimple voxel.
 * 
 * @param c A configuration.
 * @param mask_p The bits of c that belong to the set P.
 * @return True if the central voxel of c is 6-Psimple, false otherwise.
 */
bool cnfPSimple6(Config c, Config mask_p);

/** 
 * Checks wheter a 3x3x3 configuration is that of a (6+)-Psimple voxel.
 * 
 * @param c A configuration.
 * @param mask_p The bits of c that belong to the set P.
 * @return True if the central voxel of c is (6+)-Psimple, false otherwise.
 */
bool cnfPSimple6P(Config c, Config mask_p);

/** 
 * Checks wheter a 3x3x3 configuration is that of a 18-Psimple voxel.
 * 
 * @param c A configuration.
 * @param mask_p The bits of c that belong to the set P.
 * @return True if the central voxel of c is 18-Psimple, false otherwise.
 */
bool cnfPSimple18(Config c, Config mask_p);

/** 
 * Checks wheter a 3x3x3 configuration is that of a 26-Psimple voxel.
 * 
 * @param c A configuration.
 * @param mask_p The bits of c that belong to the set P.
 * @return True if the central voxel of c is 26-Psimple, false otherwise.
 */
bool cnfPSimple26(Config c, Config mask_p);


UChar cnfExtremity6(Config c);
UChar cnfExtremity6P(Config c);
UChar cnfExtremity18(Config c);
UChar cnfExtremity26(Config c);


/* FONCTIONS DE TEST DES CARACTERISATIONS LOCALES DE SURFACES DISCRETES
   
   Les fonction de test renvoient un entier 
   bit 0 : point satisfaisant la proprietes
   bit 1 : point satisfaisant partielement la propriete 
*/

/** 
 * Checks whether a 3x3x3 configuration is that of a basic surface voxel.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if the configuration is that of a basic surface, otherwise 0.  
 */
UChar cnfSurface6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a basic surface voxel.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if the configuration is that of a basic surface, otherwise 0.  
 */
UChar cnfSurface6P(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a basic surface voxel.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if the configuration is that of a basic surface, otherwise 0.  
 */
UChar cnfSurface18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a basic surface voxel.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if the configuration is that of a basic surface, otherwise 0.  
 */
UChar cnfSurface26(Config c);


/** 
 * Checks whether a 3x3x3 configuration is that of an isthme.
 * 
 * @param c A 3x3x3 configuration.
 * @return True if c is the configuration of an isthme.
 */
UChar cnfIsthmus6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of an extremity voxels.
 * 
 * @param c A 3x3x3 configuration.
 * @return True if c is the configuration of an isthme.
 */
UChar cnfIsthmus6P(Config c);

/** 
 * Checks whether a  3x3x3 configuration is that of an extremity voxels.
 * 
 * @param c A 3x3x3 configuration.
 * @return True if c is the configuration of an isthme.
 */
UChar cnfIsthmus18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of an extremity voxels.
 * 
 * @param c A 3x3x3 configuration.
 * @return True if c is the configuration of an isthme.
 */
UChar cnfIsthmus26(Config c);


/** 
 * Checks whether a 3x3x3 configuration is that of a 6-border voxel.
 * A 6-border voxel is a voxel 26-adjacent to the background.
 * 
 * @param c A configuration
 * 
 * @return True if c is the configuration of a 6-border voxel.
 */
bool cnfBorder6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 6P-border voxel.
 * A 6-border voxel is a voxel 18-adjacent to the background.
 * 
 * @param c A configuration
 * 
 * @return True if c is the configuration of a 6P-border voxel.
 */
bool cnfBorder6P(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 18-border voxel.
 * A 6-border voxel is a voxel 6-adjacent to the background.
 * 
 * @param c A configuration
 * 
 * @return True if c is the configuration of a 18-border voxel.
 */
bool cnfBorder18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 26-border voxel.
 * A 6-border voxel is a voxel 6-adjacent to the background.
 * 
 * @param c A configuration
 * 
 * @return True if c is the configuration of a 26-border voxel.
 */
bool cnfBorder26(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 6-surface junction.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is the configuration of a 6-surface junction, otherwise 0.
 */
UChar cnfSurfaceJunction6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a (6+)-surface junction.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is the configuration of a (6+)-surface junction, otherwise 0.
 */
UChar cnfSurfaceJunction6P(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 18-surface junction.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is the configuration of a 18-surface junction, otherwise 0.
 */
UChar cnfSurfaceJunction18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 26-surface junction.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is the configuration of a 26-surface junction, otherwise 0.
 */
UChar cnfSurfaceJunction26(Config c);


/** 
 * Checks whether a 3x3x3 configuration is that of a 6-surface according
 * to the charaterizations given by G. Bertrand.
 * 
 * @param c A 3x3x3 configuration.
 * @return 1 if c is the configuration of a 6-surface junction, otherwise 0.
 */
UChar cnfSurfaceBertrand6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a (6+)-surface according
 * to the charaterizations given by G. Bertrand.
 * 
 * @param c A 3x3x3 configuration.
 * @return 1 if c is the configuration of a (6+)-surface junction,
 *         otherwise 0.
 */
UChar cnfSurfaceBertrand6P(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 18-surface according
 * to the charaterizations given by G. Bertrand.
 * 
 * @param c A 3x3x3 configuration.
 * @return 1 if c is the configuration of a 18-surface junction, 
 *         otherwise 0.
 */
UChar cnfSurfaceBertrand18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a 26-surface according
 * to the charaterizations given by G. Bertrand.
 * 
 * @param c A 3x3x3 configuration.
 * @return 1 if c is the configuration of a 26-surface junction, otherwise 0.
 */
UChar cnfSurfaceBertrand26(Config c);


/** 
 * Checks whether a 3x3x3 configuration is that of a Bertrand's 6-surface point
 * or an isthme.
 * 
 * @param c A 3x3x3 configuration.
 * @return Bit 1 if c is an isthme, bit 2 if Bertrand 6-surface, 0 otherwise.
 */
UChar cnfSurfaceBertrandOrIsthmus6(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a Bertrand's 
 * (6+)-surface point or an isthme.
 * 
 * @param c A 3x3x3 configuration.
 * @return Bit 1 if c is an isthme, bit 2 if Bertrand (6+)-surface,
 * 0 otherwise.
 */
UChar cnfSurfaceBertrandOrIsthmus6P(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a Bertrand's
 * 18-surface point or an isthme.
 * 
 * @param c A 3x3x3 configuration.
 * @return Bit 1 if c is an isthme, bit 2 if Bertrand 18-surface, 0 otherwise.
 */
UChar cnfSurfaceBertrandOrIsthmus18(Config c);

/** 
 * Checks whether a 3x3x3 configuration is that of a Bertrand's
 * 26-surface point or an isthme.
 * 
 * @param c A 3x3x3 configuration.
 * @return Bit 1 if c is an isthme, bit 2 if Bertrand 26-surface, 0 otherwise.
 */
UChar cnfSurfaceBertrandOrIsthmus26(Config c);


/*  Retourne dans a et b les composantes 6-connexes du complementaire
    de cnf qui sont 6-adjacentes au point central, 
    si elles sont deux exactement, retourne VRAI (1)
    sinon retourne FAUX (0) */

/** 
 * Extract from a 3x3x3 configuration the two 6-connected components that 
 * are adjacent to the center, if any. Returns true or false depending on
 * the fact that there are exactly two such components.
 * @param cnf A 3x3x3 configuration.
 * @param axx Reference to a configuration.
 * @param bxx Reference to a configuration.
 * 
 * @return True if there are exactly two 6-connected components that are
 * adjacent to the center voxel, false otherwise.
 */
bool cnfLabel6(Config cnf, Config & axx, Config & bxx);

/** 
 * Checks whether a configuration is that of a 26 strong surface.
 * 
 * @param c A 3x3x3 configuration.
 * @return True if the configuration is that of a strong 26-surface, 
 *         otherwise false.
 */
UChar cnfStrong26Surface(Config c);

/** 
 * Checks whether a configuration is that of a 26 strong surface
 * or a 6-extremity.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is a strong surface point, 1 if a 6-extremity, 
 *         zero otherwise.
 */
UChar cnfStrong26SurfaceOrExtremity6(Config c);

/** 
 * Checks whether a configuration is that of a 26 strong surface
 * or an 18-extremity.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is a strong 26-surface point, 1 if an 18 extremity, 
 *         zero otherwise.
 */
UChar cnfStrong26SurfaceOrExtremity18(Config c);

/** 
 * Checks whether a configuration is that of a 26 strong surface
 * or a 26-extremity.
 * 
 * @param c A 3x3x3 configuration.
 * @return 3 if c is a strong 26-surface point, 1 if a 26-extremity, 
 *         zero otherwise.
 */
UChar cnfStrong26SurfaceOrExtremity26(Config c);

inline bool simple4( Config c ) {
  return Simple4[ c ];
}

inline bool simple8( Config c ) {
  return Simple8[ c ];
}

/*
 * Masks providing neighbors of a given point
 * into a 3x3x3 configuration.
 */
extern const Config The6Neighbors[27];
extern const Config The18Neighbors[27];
extern const Config The26Neighbors[27];
extern const int The6NeighborsLists[27][32]; 
extern const int The18NeighborsLists[27][32];
extern const int The26NeighborsLists[27][32];

// extern const char taille_voisins[4];

#endif // _CONFIG_H_
