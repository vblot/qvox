/**
 * @file   panfile.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:42:32 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

const char * POtypeName[] =  { 
  "Object",
  "Collection",
  "Img1duc", "Img1dsl", "Img1dsf",
  "Img2duc", "Img2dsl", "Img2dsf",
  "Img3duc", "Img3dsl", "Img3dsf",
  "Reg1d", "Reg2d", "Reg3d",
  "Graph2d", "Graph3d",
  "Imc2duc", "Imc2dsl", "Imc2dsf",
  "Imc3duc", "Imc3dsl", "Imc3dsf",
  "Imx1duc", "Imx1dsl", "Po4_Imx1dul", "Imx1dsf",
  "Imx2duc", "Imx2dsl", "Po4_Imx2dul", "Imx2dsf",
  "Imx3duc", "Imx3dsl", "Po4_Imx3dul", "Imx3dsf",
  "Point1d", "Point2d", "Point3d",

  "Img1dss", "Img2dss", "Img3dss",
  "Imc1dss", "Imc2dss", "Imc3dss",
  "Imx1dss", "Imx2dss", "Imx3dss",
  
  "Img1dul", "Img2dul", "Img3dul",
  "Imc2dul", "Imc3dul",

  "Imx1dsd", "Imx2dsd", "Imx3dsd",

  "Po_Imx1db", "Po_Imx2db", "Po_Imx3db",
  "Po_Imx1dus", "Po_Imx2dus", "Po_Imx3dus"
};

const char * POvalueTypeName[] =  { 
  "unknown", "unsigned char", "signed long", "float",
  "unsigned long", "short", "double", "bool", "unsigned short"
};

const char * POShortValueTypeName[] = {
  "???", "UChar", "Long", "Float",
  "ULong", "Short", "Double", "Bool", "UShort"
};

const char * ColorSpaceNames[ 15 ] = {
   "RGB", "XYZ", "LUV", "LAB", "HSL", "AST", "I1I2I3", "LCH",
   "WRY", "RNGNBN", "YCBCR", "YCH1CH2", "YIQ",
   "YUV", "UNKNOWN"
};

