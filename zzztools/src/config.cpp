/**
 * @file   config.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:35 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "config.h"
using std::string;
using std::istream;
using std::ostream;

#include <Triple.h>
#include <cstdlib>


/*
 * Positions of the points according to there indices.
 *
 *
 *           24 --------- 25 --------- 26
 *      21           22           23
 * 18 -------- 19 --------- 20
 *
 *           15 --------- 16 --------- 17
 *      12           13           14
 * 9 ---------- 10 ----------- 11
 *                                          Z  Y
 *          6 ---------  7 ---------- 8     | /
 *      3            4            5         |/
 * 0 ---------  1 -----------  2            +---- X
*/

const bool Simple8[256] = {
  false,true,true,true,true,true,true,true,true,false,false,false,true,
  true,true,true,true,false,false,false,true,true,true,true,true,false,
  false,false,true,true,true,true,true,false,false,false,false,false,
  false,false,false,false,false,false,false,false,false,false,true,false,
  false,false,true,true,true,true,true,false,false,false,true,true,true,
  true,true,true,false,true,false,true,false,true,false,false,false,false,
  false,true,false,true,true,true,false,true,true,false,true,false,true,
  true,false,true,true,false,true,false,true,true,false,true,false,true,
  false,true,false,false,false,false,false,true,false,true,true,true,false,
  true,true,false,true,false,true,true,false,true,true,false,true,false,
  true,true,false,true,false,true,false,true,false,false,false,false,false,
  true,false,true,false,false,false,false,false,true,false,true,false,false,
  false,false,false,true,false,true,false,false,false,false,false,false,false,
  false,false,false,false,false,false,false,false,false,false,false,false,
  false,false,true,false,true,false,false,false,false,false,true,false,true,
  true,true,false,true,false,true,false,true,false,false,false,false,false,
  true,false,true,true,true,false,true,true,false,true,false,true,true,false,
  true,true,false,true,false,true,true,false,true,false,true,false,true,
  false,false,false,false,false,true,false,true,true,true,false,true,true,
  false,true,false,true,true,false,true,true,false,true,false};

const bool Simple4[256] = {
  false,true,false,true,true,false,true,true,false,true,false,true,true,false,
  true,true,true,false,true,false,false,false,false,false,true,false,true,
  false,true,false,true,true,false,true,false,true,true,false,true,true,false,
  true,false,true,true,false,true,true,true,false,true,false,false,false,
  false,false,true,false,true,false,true,false,true,true,true,false,true,
  false,false,false,false,false,true,false,true,false,false,false,false,false,
  false,false,false,false,false,false,false,false,false,false,false,false,
  false,false,false,false,true,false,true,false,false,false,false,false,true,
  false,true,false,false,false,false,false,true,false,true,false,false,false,
  false,false,true,false,true,false,true,false,true,true,false,true,false,
  true,true,false,true,true,false,true,false,true,true,false,true,true,true,
  false,true,false,false,false,false,false,true,false,true,false,true,false,
  true,true,false,true,false,true,true,false,true,true,false,true,false,true,
  true,false,true,true,true,false,true,false,false,false,false,false,true,
  false,true,false,true,false,true,true,true,true,true,true,false,false,
  false,true,true,true,true,true,false,false,false,true,false,false,false,
  false,false,false,false,false,false,false,false,false,false,false,false,
  true,true,true,true,true,false,false,false,true,true,true,true,true,false,
  false,false,true,true,true,true,true,false,false,false,true,true,true,true,
  true,true,true,true,false};

const char AdjacencyNames[4][3] = { "6", "6+", "18", "26" };
const char ComplementatyAdjacency[4] = { ADJ26, ADJ18, ADJ6P, ADJ6 };
const char IndicesArray[3][3][3] = {
  {{0, 9, 18}, {3, 12, 21}, {6, 15, 24}},
  {{1, 10, 19}, {4, 13, 22}, {7, 16, 25}},
  {{2, 11, 20}, {5, 14, 23}, {8, 17, 26}}
};

const int IndexToDeltaX[27] = { -1, -1, -1, -1, -1, -1, -1, -1, -1,
                                0, 0, 0, 0, 0, 0, 0, 0, 0,
                                1, 1, 1, 1, 1, 1, 1, 1, 1 };

const int IndexToDeltaY[27] = { -1, -1, -1, 0, 0, 0, 1, 1, 1,
                                -1, -1, -1, 0, 0, 0, 1, 1, 1,
                                -1, -1, -1, 0, 0, 0, 1, 1, 1 };

const int IndexToDeltaZ[27] = { -1, 0, 1, -1, 0, 1, -1, 0, 1,
                                -1, 0, 1, -1, 0, 1, -1, 0, 1,
                                -1, 0, 1, -1, 0, 1, -1, 0, 1};

const int IndexToDelta[27][3] = {
  {-1, -1, -1}, {-1, -1, 0}, {-1, -1, 1},
  {-1, 0, -1},  {-1, 0, 0}, {-1, 0, 1},
  {-1, 1, -1}, {-1, 1, 0}, {-1, 1, 1},
  {0, -1, -1}, {0, -1, 0}, {0, -1, 1},
  {0, 0, -1}, {0, 0, 0}, {0, 0, 1},
  {0, 1, -1}, {0, 1, 0}, {0, 1, 1},
  {1, -1, -1}, {1, -1, 0}, {1, -1, 1},
  {1, 0, -1}, {1, 0, 0}, {1, 0, 1},
  {1, 1, -1}, {1, 1, 0}, {1, 1, 1}
};

const Triple< SSize > ConfigShifts[27] = {
  Triple<SSize>(-1,-1,-1),Triple<SSize>(-1,-1,0),Triple<SSize>(-1,-1,1),
  Triple<SSize>(-1,0,-1), Triple<SSize>(-1,0,0),Triple<SSize>(-1,0,1),
  Triple<SSize>(-1,1,-1),Triple<SSize>(-1,1,0),Triple<SSize>(-1,1,1),
  Triple<SSize>(0,-1,-1),Triple<SSize>(0,-1,0),Triple<SSize>(0,-1,1),
  Triple<SSize>(0,0,-1),Triple<SSize>(0,0,0),Triple<SSize>(0,0,1),
  Triple<SSize>(0,1,-1),Triple<SSize>(0,1,0),Triple<SSize>(0,1,1),
  Triple<SSize>(1,-1,-1),Triple<SSize>(1,-1,0),Triple<SSize>(1,-1,1),
  Triple<SSize>(1,0,-1),Triple<SSize>(1,0,0),Triple<SSize>(1,0,1),
  Triple<SSize>(1,1,-1),Triple<SSize>(1,1,0),Triple<SSize>(1,1,1)
};

const Triple< SSize > NeighborsShifts26[26] = {
  Triple<SSize>(-1,-1,-1),Triple<SSize>(-1,-1,0),Triple<SSize>(-1,-1,1),
  Triple<SSize>(-1,0,-1), Triple<SSize>(-1,0,0),Triple<SSize>(-1,0,1),
  Triple<SSize>(-1,1,-1),Triple<SSize>(-1,1,0),Triple<SSize>(-1,1,1),
  Triple<SSize>(0,-1,-1),Triple<SSize>(0,-1,0),Triple<SSize>(0,-1,1),
  Triple<SSize>(0,0,-1),                      Triple<SSize>(0,0,1),
  Triple<SSize>(0,1,-1),Triple<SSize>(0,1,0),Triple<SSize>(0,1,1),
  Triple<SSize>(1,-1,-1),Triple<SSize>(1,-1,0),Triple<SSize>(1,-1,1),
  Triple<SSize>(1,0,-1),Triple<SSize>(1,0,0),Triple<SSize>(1,0,1),
  Triple<SSize>(1,1,-1),Triple<SSize>(1,1,0),Triple<SSize>(1,1,1)
};

const Triple< SSize > NeighborsShifts18[18] = {
  Triple<SSize>(-1,-1,0),
  Triple<SSize>(-1,0,-1), Triple<SSize>(-1,0,0), Triple<SSize>(-1,0,1),
  Triple<SSize>(-1,1,0),
  Triple<SSize>(0,-1,-1),Triple<SSize>(0,-1,0),Triple<SSize>(0,-1,1),
  Triple<SSize>(0,0,-1),                      Triple<SSize>(0,0,1),
  Triple<SSize>(0,1,-1),Triple<SSize>(0,1,0),Triple<SSize>(0,1,1),
  Triple<SSize>(1,-1,0),
  Triple<SSize>(1,0,-1),Triple<SSize>(1,0,0),Triple<SSize>(1,0,1),
  Triple<SSize>(1,1,0)
};

const Triple<SSize> NeighborsShifts6[6] = {
  Triple<SSize>(1,0,0),
  Triple<SSize>(-1,0,0),
  Triple<SSize>(0,1,0),
  Triple<SSize>(0,-1,0),
  Triple<SSize>(0,0,1),
  Triple<SSize>(0,0,-1)
};

const UInt32 IndexToAdjacencyMask[4][32] = {
  { 0x20a, 0x415, 0x822, 0x1051, 0x20aa, 0x4114, 0x8088, 0x10150, 0x200a0,
    0x41401, 0x82a02, 0x104404, 0x20a208, 0x415410, 0x822820, 0x1011040,
    0x202a080, 0x4014100, 0x280200, 0x540400, 0x880800, 0x1441000, 0x2a82000,
    0x4504000, 0x2208000, 0x5410000, 0x2820000},

  { 0x20a, 0x415, 0x822, 0x1051, 0x20aa, 0x4114, 0x8088, 0x10150, 0x200a0,
    0x41401, 0x82a02, 0x104404, 0x20a208, 0x415410, 0x822820, 0x1011040,
    0x202a080, 0x4014100, 0x280200, 0x540400, 0x880800, 0x1441000, 0x2a82000,
    0x4504000, 0x2208000, 0x5410000, 0x2820000 },

  { 0x161a, 0x2e3d, 0x4c32, 0xb2d3, 0x175ef, 0x26996, 0x19098, 0x3a178,
    0x340b0, 0x2c340b, 0x5c7a17, 0x986426, 0x165a659, 0x2ebdeba, 0x4d32d34,
    0x32130c8, 0x742f1d0, 0x68161a0, 0x681600, 0xf42e00, 0xc84c00, 0x34cb200,
    0x7bd7400, 0x65a6800, 0x2619000, 0x5e3a000, 0x2c34000 },

  { 0x361a, 0x7e3d, 0x6c32, 0x1b6d3, 0x3ffef, 0x36d96, 0x1b098, 0x3f178,
    0x360b0, 0x6c341b, 0xfc7a3f, 0xd86436, 0x36da6db, 0x7ffdfff, 0x6db2db6,
    0x36130d8, 0x7e2f1f8, 0x6c161b0, 0x683600, 0xf47e00, 0xc86c00, 0x34db600,
    0x7bffe00, 0x65b6c00, 0x261b000, 0x5e3f000, 0x2c36000 }
};

const Config The6Neighbors[ 27 ] = {
  0x20a, 0x415, 0x822, 0x1051, 0xaa, 0x4114, 0x8088, 0x10150, 0x200a0, 0x41401,
  0x80a02, 0x104404, 0x208208, 0x415410, 0x820820, 0x1011040, 0x2028080,
  0x4014100, 0x280200, 0x540400, 0x880800, 0x1441000, 0x2a80000, 0x4504000,
  0x2208000, 0x5410000, 0x2820000
};

const Config The18Neighbors[ 27 ] = {
  0x161a, 0xe3d, 0x4c32, 0x92d3, 0x155ef, 0x24996, 0x19098, 0x38178,
  0x340b0, 0x2c140b, 0x5c5a17, 0x984426, 0x1658659, 0x2ebdeba, 0x4d30d34,
  0x32110c8, 0x742d1d0, 0x68141a0, 0x681600, 0xf40e00, 0xc84c00,
  0x34c9200, 0x7bd5400, 0x65a4800, 0x2619000, 0x5e38000, 0x2c34000
};

const Config The26Neighbors[ 27 ] = {
  0x161a, 0x5e3d, 0x4c32, 0x196d3, 0x3dfef, 0x34d96, 0x19098, 0x3d178,
  0x340b0, 0x6c141b, 0xfc5a3f, 0xd84436, 0x36d86db, 0x7ffdfff, 0x6db0db6,
  0x36110d8, 0x7e2d1f8, 0x6c141b0, 0x681600, 0xf45e00, 0xc84c00,
  0x34d9600, 0x7bfde00, 0x65b4c00, 0x2619000, 0x5e3d000, 0x2c34000
};

/*
 *  Lists of the 6 neighbors for each index.
 *  The value 127 stands for "end of the list".
 */
const int The6NeighborsLists[27][32] = {
  {1, 3, 9, 127},
  {0, 2, 4, 10, 127},
  {1, 5, 11, 127},
  {0, 4, 6, 12, 127},
  {1, 3, 5, 7, 127},
  {2, 4, 8, 14, 127},
  {3, 7, 15, 127},
  {4, 6, 8, 16, 127},
  {5, 7, 17, 127},
  {0, 10, 12, 18, 127},
  {1, 9, 11, 19, 127},
  {2, 10, 14, 20, 127},
  {3, 9, 15, 21, 127},
  {4, 10, 12, 14, 16, 22, 127},
  {5, 11, 17, 23, 127},
  {6, 12, 16, 24, 127},
  {7, 15, 17, 25, 127},
  {8, 14, 16, 26, 127},
  {9, 19, 21, 127},
  {10, 18, 20, 22, 127},
  {11, 19, 23, 127},
  {12, 18, 22, 24, 127},
  {19, 21, 23, 25, 127},
  {14, 20, 22, 26, 127},
  {15, 21, 25, 127},
  {16, 22, 24, 26, 127},
  {17, 23, 25, 127}
};


const int The18NeighborsLists[27][32] = {
  {1, 3, 4, 9, 10, 12, 127},
  {0, 2, 3, 4, 5, 9, 10, 11, 127},
  {1, 4, 5, 10, 11, 14, 127},
  {0, 1, 4, 6, 7, 9, 12, 15, 127},
  {0, 1, 2, 3, 5, 6, 7, 8, 10, 12, 14, 16, 127},
  {1, 2, 4, 7, 8, 11, 14, 17, 127},
  {3, 4, 7, 12, 15, 16, 127},
  {3, 4, 5, 6, 8, 15, 16, 17, 127},
  {4, 5, 7, 14, 16, 17, 127},
  {0, 1, 3, 10, 12, 18, 19, 21, 127},
  {0, 1, 2, 4, 9, 11, 12, 14, 18, 19, 20, 22, 127},
  {1, 2, 5, 10, 14, 19, 20, 23, 127},
  {0, 3, 4, 6, 9, 10, 15, 16, 18, 21, 22, 24, 127},
  {1, 3, 4, 5, 7, 9, 10, 11, 12, 14, 15, 16, 17, 19, 21, 22, 23, 25, 127},
  {2, 4, 5, 8, 10, 11, 16, 17, 20, 22, 23, 26, 127},
  {3, 6, 7, 12, 16, 21, 24, 25, 127},
  {4, 6, 7, 8, 12, 14, 15, 17, 22, 24, 25, 26, 127},
  {5, 7, 8, 14, 16, 23, 25, 26, 127},
  {9, 10, 12, 19, 21, 22, 127},
  {9, 10, 11, 18, 20, 21, 22, 23, 127},
  {10, 11, 14, 19, 22, 23, 127},
  {9, 12, 15, 18, 19, 22, 24, 25, 127},
  {10, 12, 14, 16, 18, 19, 20, 21, 23, 24, 25, 26, 127},
  {11, 14, 17, 19, 20, 22, 25, 26, 127},
  {12, 15, 16, 21, 22, 25, 127},
  {15, 16, 17, 21, 22, 23, 24, 26, 127},
  {14, 16, 17, 22, 23, 25, 127}
};

const int The26NeighborsLists[27][32] = {
  {1, 3, 4, 9, 10, 12, 127},
  {0, 2, 3, 4, 5, 9, 10, 11, 12, 14, 127},
  {1, 4, 5, 10, 11, 14, 127},
  {0, 1, 4, 6, 7, 9, 10, 12, 15, 16, 127},
  {0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 127},
  {1, 2, 4, 7, 8, 10, 11, 14, 16, 17, 127},
  {3, 4, 7, 12, 15, 16, 127},
  {3, 4, 5, 6, 8, 12, 14, 15, 16, 17, 127},
  {4, 5, 7, 14, 16, 17, 127},
  {0, 1, 3, 4, 10, 12, 18, 19, 21, 22, 127},
  {0, 1, 2, 3, 4, 5, 9, 11, 12, 14, 18, 19, 20, 21, 22, 23, 127},
  {1, 2, 4, 5, 10, 14, 19, 20, 22, 23, 127},
  {0, 1, 3, 4, 6, 7, 9, 10, 15, 16, 18, 19, 21, 22, 24, 25, 127},
  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18,
   19, 20, 21, 22, 23, 24, 25, 26, 127},
  {1, 2, 4, 5, 7, 8, 10, 11, 16, 17, 19, 20, 22, 23, 25, 26, 127},
  {3, 4, 6, 7, 12, 16, 21, 22, 24, 25, 127},
  {3, 4, 5, 6, 7, 8, 12, 14, 15, 17, 21, 22, 23, 24, 25, 26, 127},
  {4, 5, 7, 8, 14, 16, 22, 23, 25, 26, 127},
  {9, 10, 12, 19, 21, 22, 127},
  {9, 10, 11, 12, 14, 18, 20, 21, 22, 23, 127},
  {10, 11, 14, 19, 22, 23, 127},
  {9, 10, 12, 15, 16, 18, 19, 22, 24, 25, 127},
  {9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 25, 26, 127},
  {10, 11, 14, 16, 17, 19, 20, 22, 25, 26, 127},
  {12, 15, 16, 21, 22, 25, 127},
  {12, 14, 15, 16, 17, 21, 22, 23, 24, 26, 127},
  {14, 16, 17, 22, 23, 25, 127}
};


SimplePointTest SkelSimpleTests[4] = { cnfSimple6,
                                       cnfSimple6P,
                                       cnfSimple18,
                                       cnfSimple26
                                     };

PSimplePointTest SkelPSimpleTests[4] = { cnfPSimple6,
                                         cnfPSimple6P,
                                         cnfPSimple18,
                                         cnfPSimple26
                                       };

TerminalPointTest SkelTerminalTests[10][5] = {
  {0, 0, 0, 0},
  {cnfSurface6, cnfSurface6P, cnfSurface18, cnfSurface26},
  {cnfIsthmus6, cnfIsthmus6P, cnfIsthmus18, cnfIsthmus26},
  {cnfExtremity6, cnfExtremity6P, cnfExtremity18, cnfExtremity26},
  {cnfSurfaceJunction6, cnfSurfaceJunction6P,
   cnfSurfaceJunction18, cnfSurfaceJunction26},
  {cnfSurfaceBertrand6, cnfSurfaceBertrand6P,
   cnfSurfaceBertrand18, cnfSurfaceBertrand26},
  {cnfSurfaceBertrandOrIsthmus6, cnfSurfaceBertrandOrIsthmus6P,
   cnfSurfaceBertrandOrIsthmus18, cnfSurfaceBertrandOrIsthmus26},
  {0, 0, 0, cnfStrong26Surface},
  {cnfStrong26SurfaceOrExtremity6, cnfStrong26SurfaceOrExtremity6,
   cnfStrong26SurfaceOrExtremity18, cnfStrong26SurfaceOrExtremity26}
};


/*
 *  Functions
 */

#define X 0
#define Y 1
#define Z 2

#define VECT_SUM(V,A,B) V[X]=A[X]+B[X]; V[Y]=A[Y]+B[Y]; V[Z]=A[Z]+B[Z]
#define VECT_DIFF(V,A,B) V[X]=A[X]-B[X]; V[Y]=A[Y]-B[Y]; V[Z]=A[Z]-B[Z]
#define VECT_EQUAL(A,B) A[X]==B[X] && A[Y]==B[Y] && A[Z]==B[Z]
#define VECT_DIV(A,B,D) A[X]=B[X]/D; A[Y]=B[Y]/D; A[Z]=B[Z]/D

namespace {
  inline int bit_count( unsigned long x ) {
    x = x - ((x >> 1) & 0x55555555);
    x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
    x = (x + (x >> 4)) & 0x0F0F0F0F;
    x = x + (x >> 8);
    x = x + (x >> 16);
    return x & 0x0000003F;
  }
}

float
cnfCurvatureObject( Config c )
{
  int direction;
  Config config;
  Config vg_o26;
  Config vg_c6;

  float f[3] = { .0, .0, .0 };
  int nf = 0;

  float agn[3] = { .0, .0, .0 };
  int nagn = 0;

  float agnb[3] = { .0, .0, .0 };
  int nagnb = 0;

  float w[3] = { .0, .0, .0 };

  float scalar, module;

  /* Courbure tres basique */
  {
    return ( 13 - cnfCount( c ) ) / 26.0f;
  }

  config = c & MASK_26_NEIGHBORS;
  vg_o26 = config | MASK_CENTER;
  vg_c6 = cnfGeodesic_6PNeighborhood( ( ~config ) & MASK_26_NEIGHBORS );

  // F(x) = \{ (y+yb)/2 / y \in G_n(x,X) et yb \in G_nb(x,Xb) \cap N_6(y) \}
  for ( direction = 0; direction < 27; direction++ ) {

    if ( vg_c6 & ( 1 << direction ) ) {
      VECT_SUM( agnb, agnb, IndexToDelta[direction] );
      nagnb++;
    }

    if ( vg_o26 & ( 1 << direction ) ) {
      const int *voisin = The6NeighborsLists[direction];
      int v;

      VECT_SUM( agn, agn, IndexToDelta[direction] );
      nagn++;

      while ( ( v = *voisin ) != 127 ) {
        if ( vg_c6 & ( 1 << v ) ) {
          VECT_SUM( f, f, IndexToDelta[direction] );
          VECT_SUM( f, f, IndexToDelta[v] );
          nf += 2;
        }
        voisin++;
      }
    }
  }

  VECT_DIV( f, f, nf );
  VECT_DIV( agn, agn, nagn );
  VECT_DIV( agnb, agnb, nagnb );

  VECT_DIFF( w, agn, agnb );


  /*   printf("Module^2 :(%d %d %d) %.3f\n",nf,nagn,nagnb, */
  /* 	 w[0]*w[0] + w[1]*w[1] + w[2]*w[2]); */

  module = sqrt( w[0] * w[0] + w[1] * w[1] + w[2] * w[2] );
  VECT_DIV( w, w, module );
  scalar = f[0] * w[0] + f[1] * w[1] + f[2] * w[2];

  return scalar;
}

float
cnfCurvatureBackground( Config c )
{

  return -1 * cnfCurvatureObject( c );
}

float
cnfCurvature( Config c )
{
  int direction;
  float nx = 0, ny = 0, nz = 0;
  int nbPoints = 0;

  for ( direction = 0; direction < 27; direction++ )
    if ( direction != CENTER_INDEX && ( c & ( 1 << direction ) ) ) {
      nx += IndexToDeltaX[direction];
      ny += IndexToDeltaY[direction];
      nz += IndexToDeltaZ[direction];
      nbPoints++;
    }

  if ( nbPoints ) {
    nx /= nbPoints;
    ny /= nbPoints;
    nz /= nbPoints;
  }
  return sqrt( nx * nx + ny * ny + nz * nz );
}

int
cnfCount( Config c )
{
  int res = 0;
  while ( c ) {
    if ( c & 1 ) res++;
    c >>= 1;
  }
  return res;
}

bool
adjacentPoints( UChar p1, UChar p2, int adjacency )
{
  char dx = abs( IndexToDeltaX[p1] - IndexToDeltaX[p2] );
  char dy = abs( IndexToDeltaY[p1] - IndexToDeltaY[p2] );
  char dz = abs( IndexToDeltaZ[p1] - IndexToDeltaZ[p2] );

  if ( !( dx + dy + dz ) ) return false;
  if ( dx > 1 || dy > 1 || dz > 1 ) return false;
  if ( dx + dy + dz == 1 ) return true;

  if ( adjacency == ADJ18 ) return ( dx + dy + dz == 2);
  if ( adjacency == ADJ26 ) return true;
  return false;
}

Config
cnfComplementary( Config c )
{
  return ( ~c ) & MASK_CONFIG;
}


Config
cnfRandom()
{
  char n;
  Config res = 0;
  for ( n = 0; n < 27; n++ ) {
    if ( ( rand() & 15 ) == 15 ) res |= 1 << n;
  }
  return res;
}

string
cnfToString( Config c )
{
  string result;;
  for (int n = 26; n >= 0; n-- ) {
    if ( n == 13 )  result += "|";
    if ( c & ( 1 << n ) ) result += "1";
    result += "0";
  }
  return result;
}

string
cnfTo3DString( Config c )
{
  char car[2] = { '.', 'O' };
  std::stringstream result;

  result << "    " << car[( c & ( 1 << 8 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 17 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 26 ) ) ? 1 : 0] << std::endl;

  result << "   " << car[( c & ( 1 << 5 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 14 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 23 ) ) ? 1 : 0] << std::endl;

  result << "  " << car[( c & ( 1 << 2 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 11 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 20 ) ) ? 1 : 0] << std::endl;

  result << "    " << car[( c & ( 1 << 7 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 16 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 25 ) ) ? 1 : 0] << std::endl;

  result << "   " << car[( c & ( 1 << 4 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 13 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 22 ) ) ? 1 : 0] << std::endl;

  result << "  " << car[( c & ( 1 << 1 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 10 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 19 ) ) ? 1 : 0] << std::endl;

  result << "    " << car[( c & ( 1 << 6 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 15 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 24 ) ) ? 1 : 0] << std::endl;

  result << "   " << car[( c & ( 1 << 3 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 12 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 21 ) ) ? 1 : 0] << std::endl;

  result << "  " << car[( c & ( 1 << 0 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 9 ) ) ? 1 : 0]
         << " " << car[( c & ( 1 << 18 ) ) ? 1 : 0] << std::endl;
  return result.str();
}

ostream &
cnfWriteInStream(ostream & stream, Config c)
{
  for ( int n = 0; n < 27; n++ ) {
    if ( n == 13 )
      stream.put( '|' );
    if ( c & ( 1 << n ) )
      stream.put( '1' );
    else
      stream.put( '0' );
    if ( n == 13 )
      stream.put( '|' );
  }
  return stream;
}


Config
cnfReadFromStream( std::istream & stream )
{
  Config result = 0;
  for ( int n = 0; n < 27; n++ )
    if ( stream.get() == '1' )
      result |= ( 1 << n );
  return result;
}


/**
 * Builts and returns the (6,26)-geodesic beighborhood
 * (rank 2) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 *
 * @return The 6 geodesic neighborhood.
 */
Config
cnfGeodesic_6Neighborhood( Config cnf )
{
  Config result = 0;
  Config toAdd = 0;
  result = cnf & MASK_6_NEIGHBORS;

  if ( result & MASK_4 )  toAdd |= The6Neighbors[4];
  if ( result & MASK_10 ) toAdd |= The6Neighbors[10];
  if ( result & MASK_12 ) toAdd |= The6Neighbors[12];
  if ( result & MASK_14 ) toAdd |= The6Neighbors[14];
  if ( result & MASK_16 ) toAdd |= The6Neighbors[16];
  if ( result & MASK_22 ) toAdd |= The6Neighbors[22];

  result |= ( cnf & toAdd );
  return result;
}

/**
 * Builts and returns the 6-geodesic neighborhood
 * (with rank 3) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 *
 * @return The 6 geodesic neighborhood.
 */
Config
cnfGeodesic_6Neighborhood_3( Config cnf )
{
  Config result = 0;
  Config toAdd = 0;
  int n;

  result = cnf & MASK_6_NEIGHBORS;

  if ( result & MASK_4 )  toAdd |= The6Neighbors[4];
  if ( result & MASK_10 ) toAdd |= The6Neighbors[10];
  if ( result & MASK_12 ) toAdd |= The6Neighbors[12];
  if ( result & MASK_14 ) toAdd |= The6Neighbors[14];
  if ( result & MASK_16 ) toAdd |= The6Neighbors[16];
  if ( result & MASK_22 ) toAdd |= The6Neighbors[22];
  result |= ( cnf & toAdd );

  toAdd = 0;
  for ( n = 0; n < 27; n++ ) {
    if ( result & ( 1 << n ) )
      toAdd |= The6Neighbors[n];
  }

  result |= ( cnf & toAdd );
  return result;
}


/**
 * Builts and returns the (6,18)-geodesic neighborhood
 * (with rank 3) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 *
 * @return The (6,18) geodesic neighborhood.
 */
Config
cnfGeodesic_6PNeighborhood( Config cnf )
{
  Config result = cnf & MASK_6_NEIGHBORS;
  Config toAdd = 0;

  if ( result & MASK_4 )  toAdd |= The6Neighbors[4];
  if ( result & MASK_10 ) toAdd |= The6Neighbors[10];
  if ( result & MASK_12 ) toAdd |= The6Neighbors[12];
  if ( result & MASK_14 ) toAdd |= The6Neighbors[14];
  if ( result & MASK_16 ) toAdd |= The6Neighbors[16];
  if ( result & MASK_22 ) toAdd |= The6Neighbors[22];

  result |= ( cnf & toAdd );
  toAdd = 0;
  for ( int n = 0; n < 27; n++ )
    if ( result & ( 1 << n ) )
      toAdd |= The6Neighbors[n];

  result |= ( cnf & toAdd );
  return result;
}


/**
 * Builts and returns the (6,18)-geodesic neighborhood
 * (with rank 5) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 * @return The (6,18)-geodesic neighborhood.
 */
Config
cnfGeodesic_6PNeighborhood_5( Config config )
{
  Config result = 0, mask;
  const int *neighbor;
  static int queue[32];
  int order;
  int *head = queue;
  int *end = queue;
  int *limit;

  result = config & MASK_6_NEIGHBORS;
  if ( result & MASK_4 )  *( end++ ) = 4;
  if ( result & MASK_10 ) *( end++ ) = 10;
  if ( result & MASK_12 ) *( end++ ) = 12;
  if ( result & MASK_14 ) *( end++ ) = 14;
  if ( result & MASK_16 ) *( end++ ) = 16;
  if ( result & MASK_22 ) *( end++ ) = 22;

  order = 2;
  limit = end;
  while ( order < 6 && head < end ) {
    while ( head < limit ) {
      neighbor = The6NeighborsLists[ *head ];
      while ( ( *neighbor ) != 127 ) {
        mask = ( 1 << *neighbor );
        if ( ( mask & config ) && !( result & mask ) ) {
          *( end++ ) = *neighbor;
          result |= mask;
        }
        neighbor++;
      }
      head++;
    }
    limit = end;
    order++;
  }
  return result;
}


/**
 * Builts and returns the (18,6)-geodesic neighborhood
 * (with rank 3) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 * @return The (18,6)-geodesic neighborhood.
 */
Config
cnfGeodesic_18Neighborhood( Config config )
{
  Config result = config & MASK_18_NEIGHBORS;
  Config toAdd = 0;

  for ( int n = 0; n < 27; n++ )
    if ( result & ( 1 << n ) )
      toAdd |= The18Neighbors[n];

  result |= ( config & toAdd );
  toAdd = 0;
  for ( int n = 0; n < 27; n++ ) {
    if ( result & ( 1 << n ) )
      toAdd |= The18Neighbors[n];
  }

  result |= ( config & toAdd );
  return result;
}


/**
 * Builts and returns the (18,6)-geodesic neighborhood
 * (with rank 3) from a given configuration.
 *
 * @param cnf A 3x3x3 configuration.
 * @return The (18,6)-geodesic neighborhood.
 */
Config
cnfGGeodesic_18Neighborhood_3( Config config )
{
  Config result = 0, mask;

  static int file[32];
  int order = 1;
  int *head = file;
  int *end = file;
  const int *neighbor;

  *end++ = 13;
  while ( order < 4 && head < end ) {
    while ( head < end ) {
      neighbor = The18NeighborsLists[ *head ];
      while ( ( *neighbor ) != 127 ) {
        mask = ( 1 << *neighbor );
        if ( ( mask & config ) && !( result & mask ) ) {
          *( end++ ) = *neighbor;
          result |= mask;
        }
        neighbor++;
      }
      head++;
    }
    order++;
  }
  return result;
}

/**
 * Builts and returns the (26,6)-geodesic neighborhood
 * (rank 2) from a given configuration.
 *
 * @param config A 3x3x3 configuration.
 *
 * @return The (26,6) geodesic neighborhood.
 */
Config
cnfGeodesic_26Neighborhood( Config config )
{
  return config & MASK_26_NEIGHBORS;
}


bool
cnfSingle6CC( const Config c )
{
  Config remaining, current, next;
  int n;
  remaining = MASK_26_NEIGHBORS & c;
  if ( remaining ) {
    n = 0;
    while ( !( remaining & ( 1 << n ) ) ) n++;
    next = 1 << n;
    remaining ^= next;

    while ( next && remaining ) {
      current = next;
      next = 0;
      n = 0;
      while ( current ) {
        if ( current & 1 ) next |= The6Neighbors[n] & remaining;
        current >>= 1;
        n++;
      }
      remaining ^= next;
    }
    if ( !remaining ) return true;
  }
  return false;
}

bool
cnfSingle6CC_Queue(const Config c)
{
  Config remaining, mask;
  int n;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }

    *(end++) = n;
    remaining ^= mask;

    while ( head < end ) {
      pNeighbor = The6NeighborsLists[*head];

      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;

        if ( remaining & mask ) {
          *end = *pNeighbor;
          end++;
          remaining ^= mask;
        }
        pNeighbor++;
      }
      head++;
    }
    if ( !remaining )	return true;
    return false;
  }
  return false;
}

bool
cnfSingle18CC( const Config c )
{
  Config remaining, current, next;
  int n;
  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    while ( !( remaining & ( 1 << n ) ) )
      n++;
    next = 1 << n;
    remaining ^= next;

    while ( next && remaining ) {
      current = next;
      next = n = 0;
      while ( current ) {
        if ( current & 1 )
          next |= The18Neighbors[n] & remaining;
        current >>= 1;
        n++;
      }
      remaining ^= next;
    }
    if ( !remaining )	return true;
  }
  return false;
}

bool
cnfSingle18CC_Queue( const Config c )
{
  Config remaining, mask;
  int n;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    *(end++) = n;
    remaining ^= mask;
    while ( head < end ) {
      pNeighbor = The18NeighborsLists[*head];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;
        if ( remaining & mask ) {
          *end = *pNeighbor;
          end++;
          remaining ^= mask;
        }
        pNeighbor++;
      }
      head++;
    }
    if ( !remaining )
      return true;
    return false;
  }
  return false;
}

bool
cnfSingle26CC( const Config c )
{
  Config remaining, current, next;
  int n;
  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    while ( !( remaining & ( 1 << n ) ) )
      n++;
    next = 1 << n;
    remaining ^= next;
    while ( next && remaining ) {
      current = next;
      next = n = 0;
      while ( current ) {
        if ( current & 1 )
          next |= The26Neighbors[n] & remaining;
        current >>= 1;
        n++;
      }
      remaining ^= next;
    }
    if ( !remaining )	return 1;
  }
  return 0;
}

bool
cnfSingle26CC_Queue( const Config c )
{
  Config remaining, mask;
  int n;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    *(end++) = n;
    remaining ^= mask;

    while ( head < end ) {
      pNeighbor = The26NeighborsLists[ *head ];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;
        if ( remaining & mask ) {
          *end = *pNeighbor;
          end++;
          remaining ^= mask;
        }
        pNeighbor++;
      }
      head++;
    }
    if ( !remaining )	 return true;
    return false;
  }
  return false;
}

bool
cnfSingleCC_Queue( const Config c, const int neighborsLists[][32] )
{
  Config remaining, mask;
  int n;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    *(end++) = n;
    remaining ^= mask;

    while ( head < end ) {
      pNeighbor = neighborsLists[*head];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;
        if ( remaining & mask ) {	/* Si pas deja vu, on enfile */
          *(end++) = *pNeighbor;
          remaining ^= mask;	/* Et on marque deja vu */
        }
        pNeighbor++;
      }
      head++;
    }
    if ( !remaining )	 return true;
    return false;
  }
  return false;
}

bool
cnfTwoCC_Queue( const Config c, const int neighborsLists[][32] )
{
  Config remaining, mask;
  int n;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;
  remaining = MASK_26_NEIGHBORS & c;

  if ( remaining ) {
    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    *(end++) = n;
    remaining ^= mask;
    while ( head < end ) {
      pNeighbor = neighborsLists[*head];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;
        if ( remaining & mask ) {
          *(end++) = *pNeighbor;
          remaining ^= mask;
        }
        pNeighbor++;
      }
      head++;
    }
    if ( !remaining ) return false;

    n = 0;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    head = end = queue;
    *(end++) = n;
    remaining ^= mask;

    while ( head < end ) {
      pNeighbor = neighborsLists[*head];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;

        if ( remaining & mask ) {	/* Si pas deja vu, on enfile */
          *(end++) = *pNeighbor;
          remaining ^= mask;	/* Et on marque deja vu */
        }
        pNeighbor++;
      }
      head++;
    }
    return !remaining;
  }
  return false;
}

int
cnfCountCC_Queue( const Config c, const int neighborsLists[][32] )
{
  Config remaining, mask;
  int n = 0;
  char result = 0;
  unsigned char queue[32];
  unsigned char *head = queue;
  unsigned char *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & c;

  while ( remaining ) {
    n = 0;
    result++;
    head = end = queue;
    mask = 1;
    while ( !( remaining & mask ) ) {
      n++;
      mask <<= 1;
    }
    *(end++) = n;
    remaining ^= mask;
    while ( head < end ) {
      pNeighbor = neighborsLists[ *head ];
      while ( *pNeighbor != 127 ) {
        mask = 1 << *pNeighbor;
        if ( remaining & mask ) {
          *(end++) = *pNeighbor;
          remaining ^= mask;
        }
        pNeighbor++;
      }
      head++;
    }
  }
  return result;
}



bool
cnfSimple6( Config c )
{
  Config gn;  // Geodesic neighborhood

  gn = cnfGeodesic_6Neighborhood( c );
  if ( !cnfSingle6CC( gn ) ) return false;
  gn = cnfGeodesic_26Neighborhood( ( ~c ) & MASK_CONFIG );
  if ( !cnfSingle26CC( gn ) ) return false;
  return true;
}

bool
cnfSimple6P( Config c )
{
  Config gn; // Geodesic neighborhood

  gn = cnfGeodesic_6PNeighborhood( c );
  if ( !cnfSingle6CC( gn ) ) return false;
  gn = cnfGeodesic_18Neighborhood( ( ~c ) & MASK_CONFIG );
  if ( !cnfSingle18CC( gn ) ) return false;
  return true;
}

bool
cnfSimple18( Config c )
{
  Config gn; // Geodesic neighborhood

  gn = cnfGeodesic_18Neighborhood( c );
  if ( !cnfSingle18CC( gn ) ) return false;
  gn = cnfGeodesic_6PNeighborhood( ( ~c ) & MASK_CONFIG );
  if ( !cnfSingle6CC( gn ) )  return false;
  return true;
}

bool
cnfSimple26( Config c )
{
  Config gn; // Geodesic neighborhood

  gn = cnfGeodesic_26Neighborhood( c );
  if ( !cnfSingle26CC( gn ) )  return false;
  gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_CONFIG );
  if ( !cnfSingle6CC( gn ) ) return false;
  return 1;
}


bool
cnfPSimple6( Config c, Config mask_p )
{
  Config XminusP = c ^ mask_p;
  Config check, gn;
  int n;

  if ( !cnfSingle6CC( cnfGeodesic_6Neighborhood( XminusP ) ) ) return false;
  gn = cnfGeodesic_26Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( !cnfSingle26CC( gn ) ) return false;

  check = cnfGeodesic_6Neighborhood_3( XminusP );

  // Any point of P, 6-neighbor of x, must be adjacent to check

  if ( ( mask_p & MASK_4 ) && !( check & The6Neighbors[4] ) ) return false;
  if ( ( mask_p & MASK_10 ) && !( check & The6Neighbors[10] ) ) return false;
  if ( ( mask_p & MASK_12 ) && !( check & The6Neighbors[12] ) ) return false;
  if ( ( mask_p & MASK_14 ) && !( check & The6Neighbors[14] ) ) return false;
  if ( ( mask_p & MASK_16 ) && !( check & The6Neighbors[16] ) ) return false;
  if ( ( mask_p & MASK_22 ) && !( check & The6Neighbors[22] ) ) return false;

  check = ( ~c ) & MASK_26_NEIGHBORS;

  // Any point of P, 26-neighbor of x, must be adjacent to check
  n = 0;
  while ( n < 27 ) {
    if ( ( mask_p & ( 1 << n ) ) && !( check & The26Neighbors[n] ) )
      return false;
    n++;
  }
  return true;
}


bool
cnfPSimple6P( Config c, Config mask_p )
{
  Config XminusP = c ^ mask_p;
  Config check, gn;
  int n;
  const int *pNeighbor;

  if ( !cnfSingle6CC( cnfGeodesic_6PNeighborhood( XminusP ) ) )
    return false;
  gn = cnfGeodesic_18Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( !cnfSingle18CC( gn ) )  return false;

  check = cnfGeodesic_6PNeighborhood_5( XminusP );

  if ( ( mask_p & MASK_4 )
       && !( check & The6Neighbors[4] ) ) return false;
  if ( ( mask_p & MASK_10 )
       && !( check & The6Neighbors[10] ) ) return false;
  if ( ( mask_p & MASK_12 )
       && !( check & The6Neighbors[12] ) ) return false;
  if ( ( mask_p & MASK_14 )
       && !( check & The6Neighbors[14] ) ) return false;
  if ( ( mask_p & MASK_16 )
       && !( check & The6Neighbors[16] ) ) return false;
  if ( ( mask_p & MASK_22 )
       && !( check & The6Neighbors[22] ) ) return false;

  check = cnfGeodesic_18Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  pNeighbor = The18NeighborsLists[ CENTER_INDEX ];
  while ( ( *pNeighbor != 127 ) ) {
    n = *pNeighbor;
    if ( ( mask_p & ( 1 << n ) ) && !( check & The18Neighbors[n] ) )
      return false;
    pNeighbor++;
  }
  return true;
}


bool
cnfPSimple18( Config c, Config mask_p )
{
  Config XminusP = c ^ mask_p;
  Config check, gn;
  int n;
  const int *pNeighbor;

  if ( !cnfSingle18CC( cnfGeodesic_18Neighborhood( XminusP ) ) )
    return false;
  gn = cnfGeodesic_6PNeighborhood( ( ~c ) & MASK_26_NEIGHBORS );

  if ( !cnfSingle6CC( gn ) ) return false;

  check = cnfGeodesic_18Neighborhood( XminusP );

  // Any point of P, 18-neighbor of x, must be adjacent to check
  pNeighbor = The18NeighborsLists[ CENTER_INDEX ];
  while ( ( *pNeighbor != 127 ) ) {
    n = *pNeighbor;
    if ( ( mask_p & ( 1 << n ) ) && !( check & The18Neighbors[n] ) )
      return false;
    pNeighbor++;
  }

  check = cnfGeodesic_6PNeighborhood_5( ( ~c ) & MASK_26_NEIGHBORS );
  // Any point of P, 6-neighbor of x, must be adjacent to check

  if ( ( mask_p & MASK_4 )
       && !( check & The6Neighbors[4] ) ) return false;
  if ( ( mask_p & MASK_10 )
       && !( check & The6Neighbors[10] ) ) return false;
  if ( ( mask_p & MASK_12 )
       && !( check & The6Neighbors[12] ) ) return false;
  if ( ( mask_p & MASK_14 )
       && !( check & The6Neighbors[14] ) ) return false;
  if ( ( mask_p & MASK_16 )
       && !( check & The6Neighbors[16] ) ) return false;
  if ( ( mask_p & MASK_22 )
       && !( check & The6Neighbors[22] ) ) return false;
  return true;
}

bool
cnfPSimple26( Config c, Config mask_p )
{
  Config XminusP = c ^ mask_p;
  Config check, gn;
  int n;

  if ( !cnfSingle26CC( cnfGeodesic_26Neighborhood( XminusP ) ) )
    return false;
  gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( !cnfSingle6CC( gn ) ) return false;

  check = XminusP & MASK_26_NEIGHBORS;
  // Any point of P, 26-neighbor of x, must be 26-adjacent to check.
  n = 0;
  while ( n < 27 ) {
    if ( ( mask_p & ( 1 << n ) ) && !( check & The26Neighbors[n] ) )
      return false;
    n++;
  }

  check = cnfGeodesic_6Neighborhood_3( ( ~c ) & MASK_26_NEIGHBORS );
  // Any point of P, 6-neighbor of x, must be 6-adjacent to check.
  if ( ( mask_p & MASK_4 )
       && !( check & The6Neighbors[4] ) ) return false;
  if ( ( mask_p & MASK_10 )
       && !( check & The6Neighbors[10] ) ) return false;
  if ( ( mask_p & MASK_12 )
       && !( check & The6Neighbors[12] ) ) return false;
  if ( ( mask_p & MASK_14 )
       && !( check & The6Neighbors[14] ) ) return false;
  if ( ( mask_p & MASK_16 )
       && !( check & The6Neighbors[16] ) ) return false;
  if ( ( mask_p & MASK_22 )
       && !( check & The6Neighbors[22] ) ) return false;
  return true;
}


/* FONCTIONS DE TEST DES CARACTERISATIONS LOCALES DE SURFACES DISCRETES

   Les fonction de test renvoient un entier
   bit 0 : point satisfaisant partielement la propriete
   bit 1 : point satisfaisant la proprietes
*/


/*
 * Surface points characterization.
 */

UChar
cnfSurface6( Config c )
{
  Config gn = cnfGeodesic_26Neighborhood( MASK_26_NEIGHBORS & ~c );
  if ( cnfTwoCC_Queue( gn, The26NeighborsLists ) ) return 3;
  return 0;
}

UChar
cnfSurface6P( Config c )
{
  Config gn = cnfGeodesic_18Neighborhood( MASK_26_NEIGHBORS & ~c );
  if ( cnfTwoCC_Queue( gn, The18NeighborsLists ) ) return 3;
  return 0;
}

UChar
cnfSurface18( Config c )
{
  Config gn = cnfGeodesic_6PNeighborhood( MASK_26_NEIGHBORS & ~c );
  if ( cnfTwoCC_Queue( gn, The6NeighborsLists ) ) return 3;
  return 0;
}

UChar
cnfSurface26( Config c )
{
  Config gn = cnfGeodesic_6Neighborhood( MASK_26_NEIGHBORS & ~c );
  if ( cnfTwoCC_Queue( gn, The6NeighborsLists ) ) return 3;
  return 0;
}

/*
 * Isthmes points characterization.
 */

UChar
cnfIsthmus6( Config c )
{
  Config gn = cnfGeodesic_6Neighborhood( c );
  return ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 );
}

UChar
cnfIsthmus6P( Config c )
{
  Config gn = cnfGeodesic_6PNeighborhood( c );
  return ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 );
}

UChar
cnfIsthmus18( Config c )
{
  Config gn = cnfGeodesic_18Neighborhood( c );
  return ( cnfCountCC_Queue( gn, The18NeighborsLists ) >= 2 );
}

UChar
cnfIsthmus26( Config c )
{
  Config gn = cnfGeodesic_26Neighborhood( c );
  return ( cnfCountCC_Queue( gn, The26NeighborsLists ) >= 2 );
}

/*
 * Border points characterization.
 */

bool
cnfBorder6( Config c )
{
  return static_cast<bool>( ( ~c ) & MASK_26_NEIGHBORS );
}

bool
cnfBorder6P( Config c )
{
  return static_cast<bool>( ( ~c ) & MASK_18_NEIGHBORS );
}

bool
cnfBorder18( Config c )
{
  return static_cast<bool>( ( ~c ) & MASK_6_NEIGHBORS );
}

bool
cnfBorder26( Config c )
{
  return static_cast<bool>( ( ~c ) & MASK_6_NEIGHBORS );
}


/*
 *  Surface junction points characterization.
 */

UChar
cnfSurfaceJunction6( Config c )
{
  Config gn = cnfGeodesic_26Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The26NeighborsLists ) > 2
       && cnfSingle6CC( cnfGeodesic_6Neighborhood( c ) ) )
    return 3;
  return 0;
}

UChar
cnfSurfaceJunction6P( Config c )
{
  Config gn = cnfGeodesic_18Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The18NeighborsLists ) > 2
       && cnfSingle6CC( cnfGeodesic_6PNeighborhood( c ) ) )
    return 3;
  return 0;
}

UChar
cnfSurfaceJunction18( Config c )
{
  Config gn = cnfGeodesic_6PNeighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) > 2
       && cnfSingle18CC( cnfGeodesic_18Neighborhood( c ) ) )
    return 3;
  return 0;
}

UChar
cnfSurfaceJunction26( Config c )
{
  Config gn =  cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) > 2
       && cnfSingle26CC( cnfGeodesic_26Neighborhood( c ) ) )
    return 3;
  return 0;
}

UChar
cnfSurfaceBertrand6( Config c )
{
  Config gn = cnfGeodesic_26Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The26NeighborsLists ) >= 2 ) return 1;
  return 0;
}

UChar
cnfSurfaceBertrand6P( Config c )
{
  Config gn =  cnfGeodesic_18Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The18NeighborsLists ) >= 2 ) return 1;
  return 0;
}

UChar
cnfSurfaceBertrand18( Config c )
{
  Config gn = cnfGeodesic_6PNeighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 ) return 1;
  return 0;
}

UChar
cnfSurfaceBertrand26( Config c )
{
  Config gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 ) return 1;
  return 0;
}

/*
 * Bertrand's surface or isthmes characterizations.
 */

UChar
cnfSurfaceBertrandOrIsthmus6( Config c )
{
  char result = 0;
  Config gn = cnfGeodesic_26Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The26NeighborsLists ) >= 2 ) result |= 2;
  gn = cnfGeodesic_6Neighborhood( c );
  if ( cnfCountCC_Queue( gn ,The6NeighborsLists ) >= 2 ) result |= 1;
  return result;
}

UChar
cnfSurfaceBertrandOrIsthmus6P( Config c )
{
  char result = 0;
  Config gn = cnfGeodesic_18Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The18NeighborsLists ) >= 2 ) result |= 2;
  gn = cnfGeodesic_6PNeighborhood( c );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 ) result |= 1;
  return result;
}

UChar
cnfSurfaceBertrandOrIsthmus18( Config c )
{
  char result = 0;
  Config gn = cnfGeodesic_6PNeighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 ) result |= 2;
  gn = cnfGeodesic_18Neighborhood( c );
  if ( cnfCountCC_Queue( gn, The18NeighborsLists ) >= 2 ) result |= 1;
  return result;
}

UChar
cnfSurfaceBertrandOrIsthmus26( Config c )
{
  char result = 0;
  Config gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( cnfCountCC_Queue( gn, The6NeighborsLists ) >= 2 ) result |= 2;
  gn = cnfGeodesic_26Neighborhood( c );
  if ( cnfCountCC_Queue( gn, The26NeighborsLists ) >= 2 ) result |= 2;
  return result;
}


bool
cnfLabel6( Config cnf, Config & axx, Config & bxx )
{
  Config remaining, mask;
  UChar queue[32];
  UChar *head = queue;
  UChar *end = queue;
  const int *pNeighbor;

  remaining = MASK_26_NEIGHBORS & ( ~cnf );
  axx = bxx = 0;

  /* Look for Axx */
  pNeighbor = The6NeighborsLists[ CENTER_INDEX ];

  *end = 0;
  if ( remaining & MASK_4 ) *end = 4;
  if ( remaining & MASK_10 ) *end = 10;
  if ( remaining & MASK_12 ) *end = 12;
  if ( remaining & MASK_14 ) *end = 14;
  if ( remaining & MASK_16 ) *end = 16;
  if ( remaining & MASK_22 ) *end = 22;
  if ( !( *(end++) ) ) return false;

  mask = 1 << *head;
  remaining ^= mask;
  axx  |= mask;
  while ( head < end ) {
    pNeighbor = The6NeighborsLists[ *head ];
    while ( *pNeighbor != 127 ) {
      mask = 1 << ( *pNeighbor );
      if ( remaining & mask ) {
        *(end++) = *pNeighbor;
        remaining ^= mask;
        axx |= mask;
      }
      pNeighbor++;
    }
    head++;
  }

  /* On cherche Bxx */
  head = end = queue;
  *end = 0;
  if ( remaining & MASK_4 ) *end = 4;
  if ( remaining & MASK_10 ) *end = 10;
  if ( remaining & MASK_12 ) *end = 12;
  if ( remaining & MASK_14 ) *end = 14;
  if ( remaining & MASK_16 ) *end = 16;
  if ( remaining & MASK_22 ) *end = 22;
  if ( !( *(end++) ) ) return false;

  mask = 1 << *head;
  remaining ^= mask;
  bxx |= mask;
  while ( head < end ) {
    pNeighbor = The6NeighborsLists[ *head ];
    while ( *pNeighbor != 127 ) {
      mask = 1 << ( *pNeighbor );
      if ( remaining & mask ) {
        *(end++) = *pNeighbor;
        remaining ^= mask;
        bxx  |= mask;
      }
      pNeighbor++;
    }
    head++;
  }
  if ( axx && bxx && !( remaining & The6Neighbors[ CENTER_INDEX ] ) )
    return true;
  return false;
}

UChar
cnfStrong26Surface( Config c )
{
  Config axx, bxx;
  const int *pvoisin;
  bool result = false;
  Config gn, gnAxx, gnBxx;

  gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );

  if ( cnfTwoCC_Queue( gn, The6NeighborsLists ) )
    result = true;
  else
    return false;

  if ( !cnfLabel6( c, axx, bxx ) ) return result;
  gn = cnfGeodesic_6Neighborhood( ( ~c ) & MASK_26_NEIGHBORS );
  if ( !cnfTwoCC_Queue( gn, The6NeighborsLists ) ) return result;

  gnAxx = axx & MASK_26_NEIGHBORS;
  if ( !cnfSingleCC_Queue( gnAxx, The26NeighborsLists ) ) return result;
  gnBxx = bxx & MASK_26_NEIGHBORS;
  if ( !cnfSingleCC_Queue( gnBxx, The26NeighborsLists ) ) return result;

  pvoisin = The26NeighborsLists[ CENTER_INDEX ];
  while ( ( *pvoisin ) != 127 ) {
    if ( c & ( 1 << *pvoisin ) ) {
      if ( !( gnAxx & The26Neighbors[*pvoisin] ) ) return result;
      if ( !( gnBxx & The26Neighbors[*pvoisin] ) ) return result;
    }
    pvoisin++;
  }

  /*
    * Condition 4
    */
  gnAxx = cnfGeodesic_6Neighborhood( axx );
  if ( !cnfSingleCC_Queue( gnAxx, The6NeighborsLists ) )
    return result;
  gnBxx = cnfGeodesic_6Neighborhood( bxx );
  if ( !cnfSingleCC_Queue( gnBxx, The6NeighborsLists ) )
    return result;

  gnAxx = cnfGeodesic_6Neighborhood_3( axx );
  gnBxx = cnfGeodesic_6Neighborhood_3( bxx );
  if ( ( c & MASK_4 ) && !( gnAxx & The6Neighbors[4] ) ) return result;
  if ( ( c & MASK_4 ) && !( gnBxx & The6Neighbors[4] ) ) return result;
  if ( ( c & MASK_10 ) && !( gnAxx & The6Neighbors[10] ) ) return result;
  if ( ( c & MASK_10 ) && !( gnBxx & The6Neighbors[10] ) ) return result;
  if ( ( c & MASK_12 ) && !( gnAxx & The6Neighbors[12] ) ) return result;
  if ( ( c & MASK_12 ) && !( gnBxx & The6Neighbors[12] ) ) return result;
  if ( ( c & MASK_14 ) && !( gnAxx & The6Neighbors[14] ) ) return result;
  if ( ( c & MASK_14 ) && !( gnBxx & The6Neighbors[14] ) ) return result;
  if ( ( c & MASK_16 ) && !( gnAxx & The6Neighbors[16] ) ) return result;
  if ( ( c & MASK_16 ) && !( gnBxx & The6Neighbors[16] ) ) return result;
  if ( ( c & MASK_22 ) && !( gnAxx & The6Neighbors[22] ) ) return result;
  if ( ( c & MASK_22 ) && !( gnBxx & The6Neighbors[22] ) ) return result;
  return 3;
}

UChar
cnfStrong26SurfaceOrExtremity6( Config c )
{
  if ( cnfStrong26Surface( c ) ) return 3;
  if ( cnfExtremity6( c ) ) return 1;
  return 0;
}

UChar
cnfStrong26SurfaceOrExtremity18( Config c )
{
  if ( cnfStrong26Surface( c ) ) return 3;
  if ( cnfExtremity18( c ) ) return 1;
  return 0;
}

UChar
cnfStrong26SurfaceOrExtremity26( Config c )
{
  if ( cnfStrong26Surface( c ) ) return 3;
  if ( cnfExtremity26( c ) ) return 1;
  return 0;
}

UChar
cnfExtremity6( Config c )
{
  c &= MASK_26_NEIGHBORS;
  if ( ( c & MASK_4 ) == c ) return 1;
  if ( ( c & MASK_10 ) == c ) return 1;
  if ( ( c & MASK_12 ) == c ) return 1;
  if ( ( c & MASK_14 ) == c ) return 1;
  if ( ( c & MASK_16 ) == c ) return 1;
  if ( ( c & MASK_22 ) == c ) return 1;
  return 0;
}

UChar
cnfExtremity6P( Config c )
{
  c &= MASK_26_NEIGHBORS;
  if ( ( c & MASK_4 ) == c ) return 1;
  if ( ( c & MASK_10 ) == c ) return 1;
  if ( ( c & MASK_12 ) == c ) return 1;
  if ( ( c & MASK_14 ) == c ) return 1;
  if ( ( c & MASK_16 ) == c ) return 1;
  if ( ( c & MASK_22 ) == c ) return 1;
  return 0;
}

UChar
cnfExtremity18( Config c )
{
  c &= MASK_18_NEIGHBORS;
  return bit_count( c ) == 1;
}

UChar
cnfExtremity26( Config c )
{
  c &= MASK_26_NEIGHBORS;
  return bit_count( c ) == 1;
}

bool
cnfBoldPoint( Config c )
{
  return ( c && ( !( c & ~MASK_CUBE0 )
                  || !( c & ~MASK_CUBE1 )
                  || !( c & ~MASK_CUBE2 )
                  || !( c & ~MASK_CUBE3 )
                  || !( c & ~MASK_CUBE4 )
                  || !( c & ~MASK_CUBE5 )
                  || !( c & ~MASK_CUBE6 )
                  || !( c & ~MASK_CUBE7 ) ) );
}
