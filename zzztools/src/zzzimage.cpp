/** -*- mode: c++ -*-
 * @file   zzzimage.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:36:16 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "zzzimage.h"
#include "zzzimage_bool.h"
#include "image_header.h"

#ifdef _IS_UNIX_
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include <sstream>
#include <string>
#include <ctime>
#include <stack>
#include <vector>
#include <cstdio>
#include <cassert>

#include "tools.h"
#include "tools3d.h"

using std::string;
using std::stringstream;
using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::numeric_limits;
using std::stack;
using std::vector;

#ifdef _IS_UNIX_
namespace { const bool IsUnix = true; }
#else
namespace { const bool IsUnix = false; }
#endif //_IS_UNIX_

#ifdef _GZIP_AVAILABLE_
namespace { const bool HasGzip = true; }
#else
namespace { const bool HasGzip = false; }
#endif //_IS_UNIX_

bool
ImageProperties::operator==( const ImageProperties & other )
{
  return  ( depth == other.depth ) &&
      ( cols == other.cols ) &&
      ( rows == other.rows ) &&
      ( bands == other.bands ) &&
      ( arrayLayout == other.arrayLayout );
}

bool
ImageProperties::operator!=( const ImageProperties & other )
{
  return  ( depth != other.depth ) ||
      ( cols != other.cols ) ||
      ( rows != other.rows ) ||
      ( bands != other.bands ) ||
      ( arrayLayout != other.arrayLayout );
}

ostream &
operator<<( ostream & out, const ImageProperties & props )
{
  out << "ImageProps: Size("
      << props.cols << ","
      << props.rows << ","
      << props.depth << ","
      << props.bands << ")"
      << " ColorSpace: " << ColorSpaceNames[ props.colorspace ]
      << " Count: " << props.size
      << " Layout: " << ((props.arrayLayout==ImageProperties::ZeroFramedArrayLayout)?"ZeroFramed":"Normal");
  return out;
}

AbstractImage *
AbstractImage::create( int valueType,
                       SSize width, SSize height, SSize depth, SSize bands,
                       ImageProperties::DataArrayLayout arrayLayout )
{
  switch( valueType ) {
  case Po_ValUC: {
    ZZZImage<UChar> * p = new ZZZImage<UChar>( width, height, depth, bands,
                                               arrayLayout );
    return p;
  }
  case Po_ValSS: {
    ZZZImage<Short> *p = new ZZZImage<Short>( width, height, depth, bands,
                                              arrayLayout );
    return p;
  }
  case Po_ValUS: {
    ZZZImage<UShort> *p = new ZZZImage<UShort>( width, height, depth, bands,
                                                arrayLayout );
    return p;
  }
  case Po_ValSL: {
    ZZZImage<Int32> *p = new ZZZImage<Int32>( width, height, depth, bands,
                                              arrayLayout );
    return p;
  }
  case Po_ValUL: {
    ZZZImage<UInt32> *p = new ZZZImage<UInt32>( width, height, depth, bands,
                                                arrayLayout );
    return p;
  }
  case Po_ValSF: {
    ZZZImage<Float> *p = new ZZZImage<Float>( width, height, depth, bands,
                                              arrayLayout );
    return p;
  }
  case Po_ValSD: {
    ZZZImage<Double> *p = new ZZZImage<Double>( width, height, depth, bands,
                                                arrayLayout );
    return p;
  }
  case Po_ValB: {
    ZZZImage<Bool> *p = new ZZZImage<Bool>( width, height, depth, bands );
    return p;
  }
  case Po_ValUnknown:
    return 0;
  }
  return 0;
}

AbstractImage *
AbstractImage::create( int valueType,
                       SSize bands,
                       const Dimension3d & dim,
                       ImageProperties::DataArrayLayout arrayLayout )
{
  switch( valueType ) {
  case Po_ValUC: {
    ZZZImage<UChar> *p = new ZZZImage<UChar>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValSS: {
    ZZZImage<Short> *p = new ZZZImage<Short>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValUS: {
    ZZZImage<UShort> *p = new ZZZImage<UShort>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValSL: {
    ZZZImage<Int32> *p = new ZZZImage<Int32>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValUL: {
    ZZZImage<UInt32> *p = new ZZZImage<UInt32>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValSF: {
    ZZZImage<Float> *p = new ZZZImage<Float>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValSD: {
    ZZZImage<Double> *p = new ZZZImage<Double>( bands, dim, arrayLayout );
    return p;
  }
  case Po_ValB: {
    ZZZImage<Bool> *p = new ZZZImage<Bool>( bands, dim );
    return p;
  }
  case Po_ValUnknown:
    return 0;
  default:
    ERROR << "AbstractImage::create(): bad value type : "
          << valueType << std::endl;
  }
  return 0;
}

AbstractImage *
AbstractImage::create( Typobj objectType,
                       SSize width, SSize height, SSize depth, SSize bands,
                       ImageProperties::DataArrayLayout arrayLayout ) {
  switch ( objectType ) {
  case Po_Collection:
  case Po_Reg1d:
  case Po_Reg2d:
  case Po_Reg3d:
  case Po_Graph2d:
  case Po_Graph3d:
    return 0; // Should be new Collection; NOT HANDLED HERE
  case Po_Img1duc:
  case Po_Img2duc:
  case Po_Img3duc:
  case Po_Imc2duc:
  case Po_Imc3duc:
  case Po_Imx1duc:
  case Po_Imx2duc:
  case Po_Imx3duc: {
    ZZZImage<UChar> *p = new ZZZImage<UChar>( width, height, depth, bands,
                                              arrayLayout );
    p->poFileType( objectType );
    return p;
  }
  case Po_Img1dsl:
  case Po_Img2dsl:
  case Po_Img3dsl:
  case Po_Imc2dsl:
  case Po_Imc3dsl:
  case Po_Imx1dsl:
  case Po_Imx2dsl:
  case Po_Imx3dsl: {
    ZZZImage<Int32> *p = new ZZZImage<Int32>( width, height, depth, bands,
                                              arrayLayout );
    p->poFileType( objectType );
    return p;
  }

  case Po_Img1dss:
  case Po_Img2dss:
  case Po_Img3dss:
  case Po_Imc1dss:
  case Po_Imc2dss:
  case Po_Imc3dss:
  case Po_Imx1dss:
  case Po_Imx2dss:
  case Po_Imx3dss: {
    ZZZImage<Short> *p = new ZZZImage<Short>( width, height, depth, bands,
                                              arrayLayout );
    p->poFileType( objectType );
    return p;
  }

  case Po4_Imx1dul:
  case Po4_Imx2dul:
  case Po4_Imx3dul: {
    ZZZImage<UInt32> *p = new ZZZImage<UInt32>( width, height, depth, bands,
                                                arrayLayout );
    p->poFileType( objectType );
    return p;
  }

  case Po_Imx1dsd:
  case Po_Imx2dsd:
  case Po_Imx3dsd: {
    ZZZImage<Double> *p = new ZZZImage<Double>( width, height, depth, bands,
                                                arrayLayout );
    p->poFileType( objectType );
    return p;
  }

  case Po_Img1dsf:
  case Po_Img2dsf:
  case Po_Img3dsf:
  case Po_Imc2dsf:
  case Po_Imc3dsf:
  case Po_Imx1dsf:
  case Po_Imx2dsf:
  case Po_Imx3dsf: {
    ZZZImage<Float> *p = new ZZZImage<Float>( width, height, depth, bands,
                                              arrayLayout );
    p->poFileType( objectType );
    return p ;
  }

  case Po_Imx1db:
  case Po_Imx2db:
  case Po_Imx3db: {
    ZZZImage<Bool> *p = new ZZZImage<Bool>( width, height, depth, bands );
    p->poFileType( objectType );
    return p;
  }


  case Po_Imx1dus:
  case Po_Imx2dus:
  case Po_Imx3dus: {
    ZZZImage<UShort> *p = new ZZZImage<UShort>( width, height, depth, bands,
                                                arrayLayout );
    p->poFileType( objectType );
    return p;
  }

  case Po_Point1d:
    return 0; // new Point1d;
  case Po_Point2d:
    return 0; // new Point2d;
  case Po_Point3d:
    return 0; // new Point3d;
  default:
    return 0;
  }
}

AbstractImage *
AbstractImage::create( Typobj poType, SSize bands, const Dimension3d & dim,
                       ImageProperties::DataArrayLayout  arrayLayout )
{
  return AbstractImage::create( poType, dim.w, dim.h, dim.d, bands, arrayLayout );
}

AbstractImage *
AbstractImage::createFromFile( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return 0;
  TRACE << header << std::endl;
  AbstractImage *image = AbstractImage::create( header.poType(),
                                                header.width(),
                                                header.height(),
                                                header.depth(),
                                                header.bands() );
  bool ok = false;

  switch ( header.fileType() ) {
  case ImageHeader::TypePAN:
    ok = image->loadPandoreData( input, (POdimension[ header.poType() ] < 0), header.endian() != endian() );
    break;
  case ImageHeader::Type3DZ:
    ok = image->loadDataRLE( input );
    break;
  case ImageHeader::TypeVFF:
    ok = image->loadCharData( input );
    break;
  case ImageHeader::TypeASC:
    ok = image->loadDataASC( input );
    break;
  case ImageHeader::TypeVOL:
    ok = image->loadData( input, false );
    break;
  case ImageHeader::TypeNPZ:
  {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    ByteInputGZip gzInput( input );
    Endianness oppEndian = static_cast<Endianness>( 1 - ::endian() );
    const unsigned short NPIC_TEST16 = 0x1234;
    const unsigned int NPIC_TEST32 =  0x12345678;
    const double  NPIC_TESTDBL = 1.2345678987654321e-279;
    switch ( ValueTypeSize[ POvalueType[ header.poType() ] ] ) {
    case 1:
      header.endian( ::endian() );
      break;
    case 2: {
      unsigned short us;
      gzInput.read( reinterpret_cast<char*>( &us ), 2 );
      if ( us != NPIC_TEST16 )
        header.endian( oppEndian );
    }
      break;
    case 4: {
      unsigned int ui;
      gzInput.read( reinterpret_cast<char*>( &ui ), 4 );
      if ( ui != NPIC_TEST32 )
        header.endian( oppEndian );
    }
      break;
    case 8: {
      double d;
      gzInput.read( reinterpret_cast<char*>( &d ), 8 );
      if ( d != NPIC_TESTDBL )
        header.endian( oppEndian );
    }
      break;
    default:
      ERROR << "AbstractImage::createFromFile(): Data size is not handled.\n";
      break;
    }
    ok = image->loadData( gzInput, false );
#else
    ERROR << "Cannot read GZipped data.\n";
#endif //  defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  }
    break;
  default:
    ok = false;
    break;
  }
  if ( ok && ( header.endian() != endian() ) && ( header.fileType() !=  ImageHeader::TypePAN ) ) {
    image->reverseDataBytes();
  }
  if ( !ok ) image->free();
  return image;
}

AbstractImage *
AbstractImage::createFromFile( const char * fileName  )
{
  if ( !strcmp( fileName, "null" ) )
    return new ZZZImage<UChar>;

  AbstractImage * image = 0;
  if ( !strcmp( fileName, "-" )
       || extension( fileName, ".pan" )
       || extension( fileName, ".3dz" )
       || extension( fileName, ".vff" )
       || extension( fileName, ".vol" )
       || extension( fileName, ".asc" )
       || ( IsUnix && HasGzip && extension( fileName, ".npz" ) )
       || ( strstr( fileName, "." ) == 0 ) ) {
    ifstream file;
    ByteInput * input = 0;
    if ( !strcmp( fileName, "-" ) )
#if defined(_IS_UNIX_)
      input = new ByteInputFD( 0 );
#else
      input = new ByteInputStream( std::cin );
#endif
    else {
      file.open( fileName );
      if ( !file ) {
        perror("AbstractImage::createFromFile() failed");
        return new ZZZImage<UChar>;
      }
      input = new ByteInputStream( file );
    }
    image = AbstractImage::createFromFile( *input );
    delete input;
  }

  if (  IsUnix && HasGzip
        && ( extension( fileName, ".pan.gz" )
             || extension( fileName, ".3dz.gz" )
             || extension( fileName, ".vff.gz" )
             || extension( fileName, ".vol.gz" )
             || extension( fileName, ".asc.gz" ) ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    ifstream file( fileName );
    ByteInputStream input( file );
    ByteInputGZip gzInput( input );
    image = AbstractImage::createFromFile( gzInput );
#else
    ERROR << "Cannot read GZipped data.\n";
    image = 0;
#endif
  }
  else if ( extension( fileName, ".bmp" ) ) {
    ZZZImage<UChar> * v = new ZZZImage<UChar>;
    ifstream file( fileName );
    v->loadBMP( file );
    image = v;
  }
  else if ( extension( fileName, ".csv" ) ) {
    ZZZImage<UChar> * v = new ZZZImage<UChar>;
    ifstream file( fileName );
    v->loadCSV( file );
    image = v;
  }
  else if ( extension( fileName, ".xbm" ) ) {
    ZZZImage<UChar> * v = new ZZZImage<UChar>;
    ifstream file( fileName );
    v->loadXBM( file );
    image = v;
  }
  if ( !image ) {
    ERROR << "File type of " << baseName( fileName ) << " is not handled.\n";
  }
  return image;
}

int
volFileValueType( const char * filename )
{
  char line[1024];
  int voxelSize;
  bool end_of_header = false;
  std::ifstream file( filename );
  if ( ! file ) {
    perror("volFileValueType() failed:");
    return Po_ValUnknown;
  }
  if ( extension( filename, ".vol" ) )
    while ( ! end_of_header ) {
      file.getline( line, 1024 );
      if ( ( line[0] == '.' ) && ( line[1] == 0 ))
        end_of_header = true;
      if ( strstr( line, "Voxel-Size:" ) == line) {
        sscanf( line + 11, "%d", &voxelSize );
        switch ( voxelSize ) {
        case 1: return Po_ValUC; break;
        case 2: return Po_ValSS; break;
        case 4: return Po_ValUL; break;
        case 125: return Po_ValUL; break;
        default:
          return Po_ValUnknown;
        }
      }
    }
  if ( extension( filename, ".3dz" ) ) {
    int fileType = Po_Imx3duc;
    file.getline( line, 1024 );
    if ( strcmp( line , "3dz" ) ) {
      ERROR << "volFileValueType() failed: bad magic number.";
      return Po_ValUnknown;
    }
    while ( ! end_of_header ) {
      file.getline( line, 1024 );
      if ( ( line[0] == 12 ) && ( line[1] == 0 ))
        end_of_header = true;
      if ( strstr( line, "POTYPE=" ) == line ) {
        sscanf( line + 7, "%d", &fileType );
      }
    }
    return  POvalueType[ fileType ];
  }
  return Po_ValUnknown;
}

int
pobjectFileValueType( const char * filename )
{
  Po_header header;
  std::ifstream f( filename );

  if ( !f ) {
    ERROR << "pobjectFileValueType(): cannot open file "
          << filename << std::endl;
    return Po_ValUnknown;
  }

  f.read( (char*) &header, sizeof( header ) );
  header.magic[9] = '\0';
  if ( strncmp( header.magic, PO_MAGIC, sizeof( header.magic ) )
       && strncmp( header.magic, "PANDORE04", sizeof( header.magic ) )
       && strncmp( header.magic, "PANDORE03", sizeof( header.magic ) ) ) {
    ERROR << "Error: pobjectFileValueType(): Bad magic number "
          << PO_MAGIC << "|" << header.magic << "\n";
    return Po_ValUnknown;
  }
  TRACE << "PoType:" << sizeof(header.potype)
        << " " << header.potype << std::endl;
  if ( header.potype > 255 )
    reverseBytes< sizeof(header.potype) >( & header.potype );
  TRACE << "PoType:" << header.potype << std::endl;
  return POvalueType[ header.potype ];
}

template< typename T >
bool
ZZZImage<T>::alloc( SSize width, SSize height, SSize depth, SSize bands,
                    ImageProperties::DataArrayLayout arrayLayout )
{
  if ( width == _cols && height == _rows && depth == _depth && bands == _bands
       && arrayLayout == _arrayLayout ) {
    clear();
    return true;
  }
  free();
  _arrayLayout = arrayLayout;
  _depth = depth; _rows = height;  _cols = width;
  _bands = bands;

  if ( !depth || !height || !width || !bands )  {
    return true;
  }

  try {
    _bands_array = new T***[ bands ];
    T * array = 0;
    Size bandSize = 0;
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
      bandSize = ( width + 2 ) * ( height + 2 ) * ( depth + 2 );
    } else {
      bandSize = width * height * depth;
    }
    array = new T[ bandSize * bands ];
    memset( array, 0, bandSize * bands * sizeof( T ) );

    T ***matrix;
    for ( Size band = 0; band < bands; ++band ) {
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        matrix = new T**[ depth + 2 ];
        ++matrix;
        matrix[-1]  = new T*[ ( depth + 2 ) * ( height + 2 ) ];
        ++( matrix[-1] );
        Size stop = depth + 1;
        for ( Size z = 0; z < stop; z++ )
          matrix[ z ] = matrix[ z - 1 ] + height + 2;
        matrix[-1][-1] = array + (bandSize * band);
        ++( matrix[-1][-1] );
        stop = ( height + 2 ) * ( depth + 2 ) - 1;
        for (Size k = 0; k < stop; k++)
          matrix[-1][k] = matrix[-1][k - 1] + width + 2;
        _bands_array[band] = matrix;
      } else {
        matrix = new T **[ depth ];
        matrix[ 0 ] = new T*[ height * depth ];
        for ( Size i = 1; i < depth; ++i )
          matrix[ i ] = matrix[ i - 1 ] + height;
        matrix[ 0 ][ 0 ] = array + ( bandSize * band );
        Size stop = depth * height;
        for ( Size i = 1; i < stop; ++i )
          matrix[ 0 ][ i ] = matrix[ 0 ][ i - 1 ] + width;
        _bands_array[band] = matrix;
      }
    }
  } catch ( std::bad_alloc ) {
    ERROR << "Memory allocation error" << std::endl;
    ERROR << "for volume "
          << width << "x" << height << "x" << depth
          << " (" << bands << " bands)\n";
    exit(-1);
  }

  if ( _depth > 1 ) {
    poFileType( ImxFileType<T,3>::type );
  }
  return true;
}

template< typename T >
bool ZZZImage<T>::isEmpty() const
{
  return ! _bands_array;
}

template< typename T >
bool
ZZZImage<T>::poFileType( Typobj type )
{
  if ( POvalueType[ poType() ] == POvalueType[ type ] ) {
    _poFileType = type;
    return true;
  }
  return false;
}

template< typename T >
ZZZImage<T> *
ZZZImage<T>::clone() const
{
  ZZZImage<T> * result;
  result = new ZZZImage<T>( _cols, _rows, _depth, _bands, _arrayLayout );
  *result = *this;
  return result;
}

template< typename T >
AbstractImage *
ZZZImage<T>::clone( int valueType ) const
{

  switch( valueType ) {
  case Po_ValUC: {
    ZZZImage<UChar> *p = new ZZZImage<UChar>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValSS: {
    ZZZImage<Short> *p = new ZZZImage<Short>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValUS: {
    ZZZImage<UShort> *p = new ZZZImage<UShort>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValSL: {
    ZZZImage<Int32> *p = new ZZZImage<Int32>( _bands, size(), _arrayLayout  );
    *p = *this;
    return p;
  }
  case Po_ValUL: {
    ZZZImage<UInt32> *p = new ZZZImage<UInt32>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValSF: {
    ZZZImage<Float> *p = new ZZZImage<Float>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValSD: {
    ZZZImage<Double> *p = new ZZZImage<Double>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValB: {
    ZZZImage<Bool> *p = new ZZZImage<Bool>( _bands, size(), _arrayLayout );
    *p = *this;
    return p;
  }
  case Po_ValUnknown:
    return 0;
  default:
    ERROR << "AbstractImage::create(): bad value type : "
          << valueType << std::endl;
  }
  return 0;
}

template< typename T >
ZZZImage<T> &
ZZZImage<T>::operator=( const T val )
{
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
    for ( SSize b = 0; b < _bands; b++ ) {
      SSize x, y, z;
      T ***matrix = data( b );
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ )
            matrix[ z ][ y ][ x ] = val;
    }
  else
  {
    T *limit = dataVector( 0 ) + _bands * realBandVectorSize();
    for ( T * p = dataVector( 0 ); p < limit; *(p++) = val )
    {

    }
  }
  return *this;
}

template<>
ZZZImage<UChar> &
ZZZImage<UChar>::operator=( const UChar val )
{
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
    for ( SSize b = 0; b < _bands; b++ ) {
      SSize y, z;
      UChar ***matrix = data( b );
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ ) {
          memset( matrix[ z ][ y ], val, _cols );
        }
    }
  else memset( dataVector( 0 ), val, _bands * realBandVectorSize() );
  return *this;
}

template< typename T >
ZZZImage<T> &
ZZZImage<T>::operator=( const ZZZImage<T> & src )
{
  _poFileType = src._poFileType;
  _colorSpace = src._colorSpace;
  if ( props() != src.props() )
    alloc( src._cols, src._rows, src._depth, src._bands, src._arrayLayout );
  memcpy( dataVector( 0 ), src.dataVector( 0 ),
          _bands * realBandVectorSize() * sizeof( T ) );
  return *this;
}

template < typename T >
ZZZImage<T> &
ZZZImage<T>::operator+=( const T & value )
{
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )
    for ( SSize b = 0; b < _bands; b++ ) {
      SSize x, y, z;
      T ***matrix = data( b );
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ )
            matrix[ z ][ y ][ x ] += value;
    }
  else
  {
    T * p = dataVector( 0 );
    T * limit = p + _bands * realBandVectorSize();
    while ( p != limit )
      *(p++) += value;
  }
  return *this;
}

template< typename T >
T ZZZImage<T>::nth_value( SSize index, SSize band ) const
{
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout )  {
    const SSize psize = _cols * _rows;
    const SSize z = index / ( psize );
    const SSize offsetPlane = index % ( psize );
    const SSize y = offsetPlane / _cols;
    const SSize x = offsetPlane % _cols;
    return _bands_array[band][z][y][x];
  } else {
    return _bands_array[band][0][0][index];
  }
}

template< typename T >
const char *
ZZZImage<T>::valueString( SSize x, SSize y, SSize z ) const
{
  stringstream ss;
  static char result[512];
  if ( sizeof( T ) > 1 ) {
    ss << _bands_array[0][z][y][x];
    for ( SSize b = 1; b < _bands; b++)
      ss << ',' << _bands_array[b][z][y][x];
  } else {
    ss << static_cast<int>( _bands_array[0][z][y][x] );
    for ( SSize b = 1; b < _bands; b++)
      ss << ',' << static_cast<int>( _bands_array[b][z][y][x] );
  }
  strcpy( result, ss.str().c_str() );
  return result;
}

template< typename T >
const char *
ZZZImage<T>::valueString( const Triple<SSize> & voxel ) const
{
  stringstream ss;
  static char result[512];
  if ( sizeof( T ) > 1 ) {
    ss << operator()( voxel, static_cast<int>( 0 ) );
    for ( Size b = 1; b < _bands; b++)
      ss << ',' << operator()( voxel, b );
  } else {
    ss << static_cast<int>( operator()( voxel, 0 ) );
    for ( Size b = 1; b < _bands; b++)
      ss << ',' << static_cast<int>( operator()( voxel, b ) );
  }
  strcpy( result, ss.str().c_str() );
  return result;
}

template< typename T >
bool
ZZZImage<T>::nonZero( SSize x, SSize y, SSize z ) const
{
  for ( SSize band = 0; band < _bands; ++band )
    if ( _bands_array[band][z][y][x] )
      return true;
  return false;
}

/**
 * Zeros the data vector of the image.
 */
template<typename T>
void
ZZZImage<T>::clear() {
  if ( !_bands_array ) return;
  memset( (char*) dataVector( 0 ),
          0,
          sizeof( T ) * realBandVectorSize() * _bands );
}

template<typename T>
Dimension3d
ZZZImage<T>::dimension() {
  return Dimension3d( _depth, _rows, _cols );
}

template<typename T>
Bool
ZZZImage<T>::zero( const SSize x, const SSize y, const SSize z )
{
  const T zero = static_cast<T>( 0 );
  for ( SSize b = 0; b < _bands; ++b )
    if ( _bands_array[b][z][y][x] != zero )
      return false;
  return true;
}

template<typename T>
Triple<float>
ZZZImage<T>::gravityCenter( )
{
  SSize x, y, z;
  float sum_x = 0, sum_y = 0, sum_z = 0;
  float n = 0;
  T ***matrix = _bands_array[0];
  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ ) {
        if ( matrix[z][y][x] ) {
          n += 1;
          sum_x += x;
          sum_y += y;
          sum_z += z;
        }
      }
  return makeTriple( sum_x, sum_y, sum_z ) / n;
}

/*
 *
 * Input / Output methods
 *
 */

/**
 * Loads a file volume. Redirects to a specific loader according
 * to the filename extension.
 *
 * @param filename The path of the file to be loaded.
 * @return true if file has been succesfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::load( const char *filename, bool multiplexed ) {
  if ( !strcmp( filename, "null" ) )
    return true;
  bool ok = false;
  std::ifstream file;
  if ( strcmp( filename, "-" ) ) {
    file.open( filename );
    if ( ! file ) {
      perror("ZZZImage::load() failed:");
      return false;
    }
  }
  if ( !strcmp( filename, "-" )
       || extension( filename, ".pan" )
       || extension( filename, ".3dz" )
       || extension( filename, ".vff" )
       || extension( filename, ".vol" )
       || extension( filename, ".asc" )
       || extension( filename, ".npz" ) ) {
    ByteInput * input = 0;
    if ( !strcmp( filename, "-" ) )
#if defined(_IS_UNIX_)
      input = new ByteInputFD( 0 );
#else
      input = new ByteInputStream( std::cin );
#endif
    else {
      input = new ByteInputStream( file );
    }
    ok = load( *input );
    delete input;
  }
  else if ( IsUnix && HasGzip
            && ( extension( filename, ".pan.gz" )
                 || extension( filename, ".3dz.gz" )
                 || extension( filename, ".vff.gz" )
                 || extension( filename, ".vol.gz" )
                 || extension( filename, ".asc.gz" ) ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    ByteInputStream input( file );
    ByteInputGZip gzInput( input );
    ok = load( gzInput );
    file.close();
#else
    ERROR << "Cannot read GZipped data.\n";
    ok = false;
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  }
  else if ( extension( filename, ".raw" ) ) {
    ok = loadRAW( file, multiplexed );
  }
  else  if ( extension( filename, ".bmp" ) ) {
    ok = loadBMP( file );
  }
  else if ( extension( filename, ".xbm" ) ) {
    ok = loadXBM( file );
  }
  else if ( extension( filename, ".csv" ) ) {
    ok = loadCSV( file );
  }
  else if ( file && strstr( filename, "." ) == 0 ) {
    ok = load( file );
  }
  return ok;
}

template<typename T>
bool
ZZZImage<T>::load( istream & in )
{
  ByteInputStream input( in );
  return load( input );
}

template<typename T>
bool
ZZZImage<T>::load( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  bool ok = false;

  // 3DZ
  if ( header.fileType() == ImageHeader::Type3DZ ) {
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::load3DZ() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );
    ok = loadDataRLE( input );
  }

  // VFF
  if ( header.fileType() == ImageHeader::TypeVFF ) {
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );
    ok = loadCharData( input );
  }

  // ASC
  if ( header.fileType() == ImageHeader::TypeASC ) {
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth() );
    ok = loadDataASC( input );
  }

  // VOL
  if ( header.fileType() == ImageHeader::TypeVOL ) {
    if ( ValueTypeSize[ POvalueType[ header.poType() ] ] != ValueTypeSize[ POvalueType[ poType() ] ] ) {
      ERROR << "ZZZImage<>::loadVOL() failed: Voxel size mismatch Found: "
            << ValueTypeSize[ POvalueType[ header.poType() ] ]
          << ", Expected: "
          << ValueTypeSize[ POvalueType[ poType() ] ] << ").\n";
      return false;
    }
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );
    ok = loadData( input, false );
  }

  // PAN
  if ( header.fileType() == ImageHeader::TypePAN ) {
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::loadPAN() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );
    ok = loadPandoreData( input, ( POdimension[ header.poType() ] < 0 ), header.endian() != endian() );
  }

  // NPZ
  if ( header.fileType() == ImageHeader::TypeNPZ ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    const unsigned short NPIC_TEST16 = 0x1234;
    const unsigned int NPIC_TEST32 =  0x12345678;
    const double  NPIC_TESTDBL = 1.2345678987654321e-279;
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::loadNPZ() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );
    ByteInputGZip gzInput( input );
    Endianness oppEndian = static_cast<Endianness>( 1 - ::endian() );
    switch ( sizeof( T ) ) {
    case 1:
      header.endian( ::endian() );
      break;
    case 2: {
      unsigned short us;
      gzInput.read( reinterpret_cast<char*>( &us ), 2 );
      if ( us != NPIC_TEST16 )
        header.endian( oppEndian );
    }
      break;
    case 4: {
      unsigned int ui;
      gzInput.read( reinterpret_cast<char*>( &ui ), 4 );
      if ( ui != NPIC_TEST32 )
        header.endian( oppEndian );
    }
      break;
    case 8: {
      double d;
      gzInput.read( reinterpret_cast<char*>( &d ), 8 );
      if ( d != NPIC_TESTDBL )
        header.endian( oppEndian );
    }
      break;
    default:
      ERROR << "ZZZImage<T>::load(): Data size is not handled.\n";
      break;
    }
    ok = loadData( gzInput, false );
#else
    ERROR << "Cannot read GZipped data.\n";
    ok = false;
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  }

  if ( ok ) {
    if ( header.endian() != endian() && ( header.fileType() !=  ImageHeader::TypePAN ) ) {
      reverseDataBytes();
    }
    _aspect = header.aspect();
    _poFileType = header.poType();
  }
  return ok;
}


/**
 * Save volume into a file. Redirects to a specific writer according
 * to the filename extension.
 *
 * @param filename The path of the file to be written.
 * @return true if file has been succesfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::save( const char * filename ) const {
  if ( !strcmp( filename, "null" ) ) return true;

  ByteOutput *pOutput = 0;
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  ByteOutputGZip *pOutputGZip = 0;
#endif
  ByteOutputStream *pOutputStream = 0;
  ofstream file;
  bool ok = false;
  if ( ! strcmp( filename, "-" ) ) {
#if defined(_IS_UNIX_)
    pOutput = new ByteOutputFD( 1 );
#else
    pOutput = new ByteOutputStream( std::cout );
#endif
  } else {
    file.open( filename );
    if ( !file ) {
      perror("ZZZImage::save() failed");
      return false;
    }
    pOutputStream = new ByteOutputStream( file );
    if ( extension( filename, ".gz" ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
      pOutput = pOutputGZip = new ByteOutputGZip( *pOutputStream );
#else
      ERROR << "Cannot read GZipped data.\n";
      return false;
#endif
    } else {
      pOutput = pOutputStream;
    }
  }

  if ( extension( filename, ".vff" )
       || ( IsUnix && HasGzip && extension( filename, ".vff.gz" ) ) ) {
    ok =  saveVFF( *pOutput );
  }
  else if ( extension( filename, ".3dz" )
            || ( IsUnix && HasGzip && extension( filename, ".3dz.gz" ) ) ) {
    ok =  save3DZ( *pOutput );
  }
  else if ( extension( filename, ".asc" )
            || ( IsUnix && HasGzip && extension( filename, ".asc.gz" ) ) ) {
    ok =  saveASC( *pOutput );
  }
  else if ( extension( filename, ".vol" )
            || ( IsUnix && HasGzip && extension( filename, ".vol.gz" ) ) ) {
    ok =  saveVOL( *pOutput );
  }
  else if ( IsUnix && HasGzip && extension( filename, ".npz" ) ) {
    ok =  saveNPZ( *pOutput );
  }
  else if ( extension( filename, ".pan" )
            || ( IsUnix && HasGzip && extension( filename, ".pan.gz" ) )
            || !strcmp( filename, "-" ) ) {
    ok =  savePAN( *pOutput );
  }
  else if ( extension( filename, ".raw" )
            || ( IsUnix && HasGzip && extension( filename, ".raw.gz" ) ) ) {
    ok = saveRAW( *pOutput );
  }
  else if ( extension( filename, ".bmp" ) ) {
    ok =  saveBMP( *pOutput );
  }
  else if ( extension( filename, ".csv" ) ) {
    ok =  saveCSV( *pOutput );
  }
  else if ( extension( filename, ".jbm" ) ) {
    ok = saveJBM( *pOutput );
  }

  pOutput->flush();

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  delete pOutputGZip;
#endif
  delete pOutputStream;
  return ok;
}

/**
 * Save volume into a file. Redirects to a specific writer according
 * to the specified file format (e.g. "pan" or "PAN" for a Pandore file).
 *
 * @param out The output stream.
 * @param format The output file format.
 * @return true if file has been succesfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::save( ByteOutput & out, const char * format ) const
{

  char fmt[32];
  strToLower( fmt, format );

  ByteOutput *pOutput = 0;
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  ByteOutputGZip *pOutputGZip = 0;
#endif

  bool ok = false;
  if ( extension( fmt, ".gz" ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    pOutput = pOutputGZip = new ByteOutputGZip( out );
#else
    ERROR << "Cannot read GZipped data.\n";
    return false;
#endif
  } else {
    pOutput = &out;
  }

  if ( !strcmp( fmt, "vff" )
       || ( IsUnix && HasGzip && !strcmp( fmt, "vff.gz" ) ) ) {
    ok =  saveVFF( *pOutput );
  }
  else if ( !strcmp( fmt, "3dz" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "3dz.gz" ) ) ) {
    ok =  save3DZ( *pOutput );
  }
  else if ( !strcmp( fmt, "asc" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "asc.gz" ) ) ) {
    ok =  saveASC( *pOutput );
  }
  else if ( !strcmp( fmt, "vol" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "vol.gz" ) ) ) {
    ok =  saveVOL( *pOutput );
  }
  else if ( IsUnix && HasGzip && !strcmp( fmt, "npz" ) ) {
    ok =  saveNPZ( *pOutput );
  }
  else if ( !strcmp( fmt, "pan" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "pan.") ) ) {
    ok =  savePAN( *pOutput );
  }
  else if ( !strcmp( fmt, "raw" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "raw.gz" ) ) ) {
    ok = saveRAW( *pOutput );
  }
  else if ( !strcmp( fmt, "bmp" ) ) {
    ok =  saveBMP( *pOutput );
  }
  else if ( !strcmp( fmt, "csv" ) ) {
    ok =  saveCSV( *pOutput );
  }
  else if ( !strcmp( fmt, "jbm" ) ) {
    ok = saveJBM( *pOutput );
  }

  pOutput->flush();

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  delete pOutputGZip;
#endif
  return ok;
}


/**
 * ASCII file format loader.
 *
 * @param filename Path to the ".asc" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadASC( ByteInput & input )
{
  ImageHeader header( input );
  if ( ! header.isOk() )
    return false;
  alloc( header.width(), header.height(), header.depth() );
  return loadDataASC( input );
}

/**
 * ASCII Data loader.
 *
 * @return true if the data has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadDataASC( ByteInput & input )
{
  int plane = 0;
  int planes = 0;
  float value;
  T ***matrix = _bands_array[0];
  char line[1024];
  do {
    input.getline( line, 1024 );
    if ( ! sscanf( line, "PLAN %d", &plane ) ) {
      ERROR << "ZZZImage<>::readASC(): error reading plane "
            << plane + 1 << std::endl;
      return false;
    }
    for ( SSize y = _rows; y; y--) {
      input.getline( line, 1024 );
      for ( SSize x = 0; x < _cols; x++) {
        sscanf( line, "%f", &value);
        matrix[ plane ][ y - 1 ][ x ] = static_cast<T>( value );
      }
    }
    ++planes;
  } while ( planes < _depth );
  return true;
}

/**
 * ASCII file format writer.
 *
 * @param filename Path to the ".asc" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool ZZZImage<T>::saveASC( ByteOutput & out ) const
{
  SSize x, y, z;
  T ***matrix = _bands_array[ 0 ];
  out << "OBJET3D\n";
  out << "TX,TY,TZ:" << _cols << "," << _rows << "," << _depth << '\n';
  for ( z = 0; z < _depth; z++) {
    out << "PLAN " << z << '\n';
    for (y = _rows; y; y--) {
      for (x = 0; x < _cols; x++)
        out << " " << std::setw( 4 ) << matrix[ z ][ y - 1 ][ x ];
      out << '\n';
    }
  }
  return true;
}


/**
 * VFF file format loader.
 *
 * @param filename Path to the ".vff" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadVFF( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypeVFF ) {
    ERROR << "ZZZImage<>::loadVFF() failed: bad magic number\n";
    return false;
  }

  _aspect = header.aspect();
  _poFileType = header.poType();
  alloc( header.width(), header.height(), header.depth(), header.bands() );
  return loadCharData( input );
}

/**
 * VFF file format writer.
 *
 * @param filename Path to the ".vff" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveVFF( ByteOutput & out ) const
{
  out << "ncaa\n";
  out << "type=raster;\n";
  out << "format=slice;\n";
  out << "rank=3;\n";
  out << "bands=" << _bands << ";\n";
  out << "bits=" << sizeof( T ) * 8 << ";\n";
  out << "size=" << _cols
      << " " << _rows
      << " " << _depth << ";\n";
  out << "origin=0.0 0.0 0.0;\n";
  out << "extent=" << float( _cols ) << " "
      << float ( _rows ) << " "
      << float ( _depth ) << ";\n";
  out << "aspect=" << _aspect.first
      << " " << _aspect.second
      << " " << _aspect.third << ";\n";
  out << "creator=ZZZImage library (c) 1997-2100 Sebastien Fourey - GREYC;\n";
  out.put(12);
  out.put(10);

  for (SSize band = 0 ; band < _bands; band++ ) {
    T ***matrix = _bands_array[ band ];
    for (SSize z = 0; z < _depth; z++)
      for (SSize y = 0; y < _rows; y++)
        out.write( reinterpret_cast<char*>( matrix[ z ][ y ] ),
                   _cols * sizeof( T ) );
  }

  return true;
}

/**
 * VOL file format loader.
 *
 * @param filename Path to the ".vol" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadVOL( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypeVOL ) {
    ERROR << "ZZZImage<>::loadVOL() failed: bad magic number\n";
    return false;
  }

  _aspect = header.aspect();
  _poFileType = header.poType();
  if ( ValueTypeSize[ POvalueType[ _poFileType ] ] != ValueTypeSize[ POvalueType[ poType() ] ] ) {
    ERROR << "ZZZImage<>::loadVOL() failed: Voxel size mismatch Found: "
          << ValueTypeSize[ POvalueType[ _poFileType ] ]
          << ", Expected: "
          << ValueTypeSize[ POvalueType[ poType() ] ] << ").\n";
    return false;
  }

  alloc( header.width(), header.height(), header.depth(), header.bands() );
  bool ok = loadData( input, false );
  if ( ok && ( header.endian() != endian() ) ) {
    reverseDataBytes();
  }
  return ok;
}

/**
 * RAW file format loader.
 *
 * @param filename Path to the ".raw" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadRAW( ByteInput & input, bool multiplexed )
{
  if ( _depth * _rows * _cols == 0 ) {
    ERROR << "ZZZImage<>::loadRAW(): Error: Volume has null size.\n";
    return false;
  }
  return loadData( input, multiplexed );
}

template<typename T>
bool
ZZZImage<T>::loadRAW( std::istream & in, bool multiplexed )
{
  ByteInputStream input( in );
  return loadRAW( input, multiplexed );
}

/**
 * RAW file format loader with specified dimension.
 *
 * @param filename Path to the ".raw" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadRAW( std::istream & file, const Dimension3d & dim, bool multiplexed  )
{
  alloc( dim, 1  );
  ByteInputStream input( file );
  return loadData( input, multiplexed );
}

/**
 * CSV file format loader
 *
 * @param filename Path to the ".csv" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadCSV( std::istream & file )
{
  vector< Triple<SSize> > points;
  char line[1024];
  Triple<SSize> p;
  Triple<SSize> size;
  do {
    file.getline( line, 1024 );
    if ( file ) {
      sscanf( line,
              SSIZE_FORMAT ";" SSIZE_FORMAT ";" SSIZE_FORMAT ,
              &p.first, &p.second, &p.third  );
      maximize( size.first, p.first);
      maximize( size.second, p.second);
      maximize( size.third, p.third);
      points.push_back( p );
    }
  } while ( file );
  alloc( size.first+1, size.second+1, size.third+1, 1 );
  vector< Triple<SSize> >::iterator i = points.begin();
  vector< Triple<SSize> >::iterator end = points.end();
  T ***array = _bands_array[0];
  while ( i != end ) {
    array[ i->third ][ i->second ][ i->first ] = 1;
    ++i;
  }
  return true;
}

/**
 * XBM file format loader
 *
 * @param filename Path to the ".xbm" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadXBM( std::istream & file )
{
  int x, y;
  char c;
  Size buffSize = 0;
  char *buffer;
  char *pc;
  int octet;
  int line, column;

  while ( file.get(c) , c != '{' ) buffSize++;

  buffer = new char[ buffSize + 5 ];
  file.seekg( 0 );

  pc = buffer;
  while ( file.get(c) , c != '{' ) {
    *(pc++) = c;
  }
  *pc = '\0';

  pc = strstr( buffer, "width" ) + 5;
  sscanf( pc, "%d", &x );
  pc = strstr( buffer, "height" ) + 6;
  sscanf( pc, "%d", &y );
  disposeArray( buffer );

  alloc( x, y, 1, 1 );
  T ***matrix = data( 0 );

  for ( line = 0; line < _rows; line++ )
    for ( column = 0; column < _cols; column += 8 ) {
      file >> setbase(std::ios::hex) >> octet ;
      file.get( c );
      while ( c != ',' ) file.get( c );
      for ( int count = 0;
            count < 8 && count + column < _cols;
            count++ ) {
        if ( octet & ( 1 << count ) ) {
          matrix[column + count]
              [2 + ( _rows  - ( line + 1 ) )][0] = 1;
        }
      }
    }
  return true;
}

/**
 * VOL file format writer.
 *
 * @param filename Path to the ".vol" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveVOL( ByteOutput & out ) const
{
  out << "X: " << _cols << "\n";
  out << "Y: " << _rows << "\n";
  out << "Z: " << _depth << "\n";
  out << "Version: 2\n";
  out << "Voxel-Size: " << sizeof( T ) << "\n";
  out << "Alpha-Color: 0\n";
  if ( isLittleEndian() )
    out << "Int-Endian: 0123\n";
  else
    out << "Int-Endian: 3210\n";
  out << "Voxel-Endian: 0\n";
  out << "Res-X: 1.000000\n";
  out << "Res-Y: 1.000000\n";
  out << "Res-Z: 1.000000\n";
  out << ".\n";

  //   writeWordMSB( file, _cols );
  //   writeWordMSB( file, _rows );
  //   writeWordMSB( file, _depth );
  //   file.put('\n');

  T ***matrix = _bands_array[ 0 ];
  size_t rowLength = sizeof( T ) * _cols;

  for (SSize z = 0; z < _depth; z++) {
    for (SSize y = 0; y < _rows; y++) {
      out.write( reinterpret_cast<char*>( matrix[ z ][ y ] ),
                 rowLength );
    }
  }
  return true;
}

/**
 * NPZ file format writer.
 *
 * @param filename Path to the ".npz" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
template<typename T>
bool
ZZZImage<T>::saveNPZ( ByteOutput & out ) const
{
  time_t t;
  time( &t );

  out << "NPZ-11\n";
  out << "# Image file generated by QVox on " << ctime( &t );

  int d = POdimension[ poFileType() ];
  if ( _bands > 1 ) d = 4;
  char format[255];
  switch ( POvalueType[ poFileType() ] ) {
  case Po_ValUC:
    sprintf( format, "NPIC_IMAGE_%dC\n", d );
    break;
  case Po_ValSL:
    sprintf( format, "NPIC_IMAGE_%dL\n", d );
    break;
  case Po_ValSS:
    sprintf( format, "NPIC_IMAGE_%dS\n", d );
    break;
  case Po_ValSD:
    sprintf( format, "NPIC_IMAGE_%dD\n", d );
    break;
  case Po_ValB:
  case Po_ValSF:
  case Po_ValUL:
  case Po_ValUS:
  case Po_ValUnknown:
    ERROR << "Voxel format is not handled by NPZ file format. Operation aborted.\n";
    break;
  }
  out << "TYPE " << format;
  out << "COMP GZIP\n";
  out << "XMAX " << _cols << "\n";
  out << "YMAX " << _rows << "\n";
  out << "ZMAX " << _depth << "\n";
  out << "PROP Alpha-Color: 0\n";
  out << "PROP Voxel-Size: " << sizeof( T ) << "\n";
  if ( isLittleEndian() )
    out << "PROP Int-Endian: 0123\n";
  else
    out << "PROP Int-Endian: 3210\n";
  out << "PROP Voxel-Endian: 0\n";
  out << "PROP Res-X: 1.000000\n";
  out << "PROP Res-Y: 1.000000\n";
  out << "PROP Res-Z: 1.000000\n";
  out << "DATA\n";

  out.flush();

  T ***matrix = _bands_array[ 0 ];
  size_t rowLength = sizeof( T ) * _cols;
  ByteOutputGZip gzOutput( out );

  unsigned short NPIC_TEST16 = 0x1234;
  unsigned int NPIC_TEST32 =  0x12345678;
  double  NPIC_TESTDBL = 1.2345678987654321e-279;
  switch ( ValueTypeSize[ POvalueType[ poFileType() ] ] ) {
  case 1:
    break;
  case 2:
    gzOutput.write( reinterpret_cast<const char*>( &NPIC_TEST16 ), 2 );
    break;
  case 4:
    gzOutput.write( reinterpret_cast<const char*>( &NPIC_TEST32 ), 4 );
    break;
  case 8:
    gzOutput.write( reinterpret_cast<const char*>( &NPIC_TESTDBL ), 8 );
    break;
  }
  for (SSize z = 0; z < _depth; z++)
    for (SSize y = 0; y < _rows; y++)
      gzOutput.write( reinterpret_cast<char*>( matrix[ z ][ y ] ),
                      rowLength );
  return true;
}
#else
template<typename T>
bool
ZZZImage<T>::saveNPZ( ByteOutput & ) const
{
  ERROR << "Cannot handle the NPZ format.\n";
  return false;
}
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)

/**
 * RAW file format writer.
 *
 * @param filename Path to the ".raw" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveRAW( ByteOutput & out ) const
{
  T ***matrix;
  for ( SSize b = 0; b < _bands; ++b ) {
    matrix = _bands_array[ b ];
    size_t rowLength = sizeof( T ) * _cols;
    for ( SSize z = 0; z < _depth; z++ )
      for ( SSize y = 0; y < _rows ; y++ )
        out.write( reinterpret_cast<char*>( matrix[ z ][ y ] ),
                   rowLength );
  }
  return true;
}

/**
 * 3DZ file format writer.
 *
 * @param filename Path to the ".3dz" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::save3DZ( ByteOutput & out ) const
{
  int delta_plane = 2 * (_cols + 2) + 2;
  int delta_line = 2;
  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    delta_plane = delta_line = 0;
  }

  out << "3dz\n";
  out << "size=" << _depth << " " << _rows << " " << _cols << ";\n";
  out << "TYPE=" << name() << ";\n";
  out << "POTYPE=" << poType() << ";\n";
  out << "BANDS=" << _bands << ";\n";
  if ( sizeof(T) != 1 )
    out << "ENDIAN=" << ((endian()==BigEndian)?"Big":"Little") << ";\n";
  out.put(12);
  out.put(10);
  UChar seqLength;
  const UChar maxLength = UChar(-1);
  for ( SSize band = 0 ; band < _bands; ++band ) {
    T value, newValue;
    seqLength = 0;
    T *p = _bands_array[ band ][0][0];
    size_t size = _depth * _rows * _cols;
    size_t totalVoxels = 0;
    while ( totalVoxels < size ) {
      newValue = *p;
      if ( ! seqLength ) value = newValue;
      if ( newValue == value ) {
        seqLength++;
        if ( seqLength == maxLength ) {
          out.put( seqLength );
          out.write( reinterpret_cast<char*>( &value ), sizeof( T ) );
          seqLength = 0;
        }
      } else {
        out.put( seqLength );
        out.write( reinterpret_cast<char*>( &value ), sizeof(T) );
        value = newValue;
        seqLength = 1;
      }
      p++;
      totalVoxels++;
      if ( ! ( totalVoxels % ( _cols * _rows )) )
        p += delta_plane;
      else if ( ! ( totalVoxels % _cols ))
        p += delta_line;
    }
    if ( seqLength ) {
      out.put( seqLength );
      out.write( reinterpret_cast<char*>( &value ),  sizeof( T ) );
    }
  }
  return true;
}

/**
 * 3DZ file format loader.
 *
 * @param filename Path to the ".3dz" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::load3DZ( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::Type3DZ ) {
    ERROR << "ZZZImage<>::load3DZ() failed: bad magic number\n";
    return false;
  }
  _aspect = header.aspect();
  _poFileType = header.poType();
  if ( POvalueType[ _poFileType ] != POvalueType[ poType() ] ) {
    ERROR << "ZZZImage<>::load3DZ() failed: Value type mismatch (Found: "
          << POvalueType[ _poFileType ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
    return false;
  }

  ZZZImage<T>::alloc( header.width(), header.height(), header.depth(), header.bands() );

  bool ok = loadDataRLE( input );
  if ( ok && header.endian() != endian() ) {
    reverseDataBytes();
  }
  return ok;
}


/**
 * PAN file format loader.
 *
 * @param filename Path to the ".pan" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadPAN( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypePAN ) {
    ERROR << "ZZZImage<>::loadPAN() failed: bad magic number\n";
    return false;
  }

  if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
    ERROR << "ZZZImage<>::loadPAN() failed: Value type mismatch (Found: "
          << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
    return false;
  }

  alloc( header.width(), header.height(), header.depth(), header.bands() );

  bool ok = loadPandoreData( input, (POdimension[ header.poType() ] < 0), ( header.endian() != endian() ) );
  if ( ok ) {
    _aspect = header.aspect();
    _poFileType = header.poType();
  }
  return ok;
}

template< typename T >
bool
ZZZImage<T>::savePandoreHeader( ByteOutput & out, Typobj type ) const
{
  Po_header header;
  memset( & header, 0, sizeof( header ));
  std::string date = ::today();
  strcpy( header.ident, "QVox" );
  strcpy( header.date, date.c_str() );
  memcpy( header.magic, PO_MAGIC, sizeof( header.magic ) );
  header.potype = type;
  out.write( (const char*) &header, sizeof( header ) );
  return out;
}


template< typename T >
bool
ZZZImage<T>::savePAN( ByteOutput & out ) const {
  if ( savePandoreHeader( out, poFileType() ) &&
       savePandoreAttributes( out ) &&
       savePandoreData( out  ) ) return true;
  ERROR << "ZZZImage<>::savePAN() failed.\n";
  return false;
}

template<typename T>
bool
ZZZImage<T>::loadPandoreData( ByteInput & input, bool region, bool swapBytes )
{
  bool ok;
  if ( region  ){
    /*
     * Region image
     */
    T maxLabel;
    input.read( reinterpret_cast<char*>( &maxLabel ), sizeof( maxLabel ) );
    ok = loadRegionData( input, swapBytes, static_cast<UInt32>( maxLabel ) );
  } else {
    /*
     * Classical image
     */
    ok = loadData( input, false );
    if ( ok && swapBytes )
      reverseDataBytes();
  }
  return ok;
}


template<typename T>
bool
ZZZImage<T>::loadData( ByteInput & input, bool multiplexed )
{
  const size_t lineSize = sizeof( T ) * static_cast<size_t>( _cols );
  // Optimize to prevent many indirections from virtual method call
  if ( !multiplexed ) {
    if ( typeid( input ) == typeid( ByteInputStream ) ) {
      ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
      istream & in = inputFile.in();
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        for ( SSize b = 0; b < _bands; b++ )
          for ( SSize z = 0; z < _depth; ++z ) {
            for ( SSize y = 0; y < _rows; ++y ) {
              if ( ! in.read( (char*) _bands_array[ b ][ z ][ y ], lineSize ) ) {
                return in;
              }
            }
          }
      } else {
        for ( SSize b = 0; b < _bands; b++ )
          if ( ! in.read( (char *) dataVector( b ),
                          sizeof( T ) *  _depth * _rows * _cols  ) ) {
            return in;
          }
      }
    } else {
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        for ( SSize b = 0; b < _bands; b++ )
          for ( SSize z = 0; z < _depth; z++ ) {
            for ( SSize y = 0; y < _rows; y++ ) {
              if ( ! input.read( (char*) _bands_array[ b ][ z ][ y ], lineSize ) ) {
                return input;
              }
            }
          }
      } else {
        for ( SSize b = 0; b < _bands; b++ )
          if ( ! input.read( (char *) dataVector( b ),
                             sizeof( T ) * _depth * _rows * _cols  ) ) {
            return input;
          }
      }
    }
  } else { // Multiplexed
    if ( typeid( input ) == typeid( ByteInputStream ) ) {
      ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
      istream & in = inputFile.in();
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x )
            for ( SSize b = 0; b < _bands; ++b )
              in.read( reinterpret_cast<char*>(_bands_array[ b ][ z ][ y ] + x), lineSize );
    } else {
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x )
            for ( SSize b = 0; b < _bands; ++b )
              input.read( reinterpret_cast<char*>( _bands_array[ b ][ z ][ y ] + x ), lineSize );
    }
  }
  return true;
}

template<typename T>
bool
ZZZImage<T>::loadCharData( ByteInput & input )
{
  char c;
  T *** matrix = 0;
  // Optimize to prevent many indirections from virtual method call
  if ( typeid( input ) == typeid( ByteInputStream ) ) {
    ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
    istream & in = inputFile.in();
    for (SSize b = 0; b < _bands; b++) {
      matrix = _bands_array[b];
      for (SSize z = 0; z < _depth; z++)
        for (SSize y = 0; y < _rows; y++)
          for (SSize x = 0; x < _cols; x++) {
            in.get( c );
            matrix[ z ][ y ][ x ] = static_cast<T>( c );
          }
    }
    return in;
  } else {
    for (SSize b = 0; b < _bands; b++) {
      matrix = _bands_array[b];
      for (SSize z = 0; z < _depth; z++)
        for (SSize y = 0; y < _rows; y++)
          for (SSize x = 0; x < _cols; x++) {
            input.get( c );
            matrix[ z ][ y ][ x ] = static_cast<T>( c );
          }
    }
    return input;
  }
}

template<typename T>
bool
ZZZImage<T>::loadDataRLE( ByteInput & input )
{
  SSize planeSize = _rows * _cols;
  SSize delta_plane = 2 * (_cols + 2) + 2;
  SSize delta_line = 2;
  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    delta_plane = delta_line = 0;
  }

  // Optimize to prevent many indirections from virtual method call
  if ( typeid( input ) == typeid( ByteInputStream ) ) {
    ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
    istream & in = inputFile.in();
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = _bands_array[band];
      T *start = matrix[0][0];
      T *p = start;
      char c;
      Size totalVoxels = 0;
      Size size = static_cast<Size>(_depth) * _rows * _cols;
      UChar seqLength;
      T value;
      while ( totalVoxels < size ) {
        in.get( c );
        seqLength = static_cast<UChar>( c );
        in.read( reinterpret_cast<char*>( &value ), sizeof( T ) );
        while  ( seqLength-- ) {
          *( p++ ) = value;
          totalVoxels++;
          if ( ! ( totalVoxels % planeSize ) )
            p += delta_plane;
          else if ( ! ( totalVoxels % _cols ) )
            p += delta_line;
        }
      }
    }
  } else {
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = _bands_array[band];
      T *start = matrix[0][0];
      T *p = start;
      char c;
      Size totalVoxels = 0;
      Size size = static_cast<Size>(_depth) * _rows * _cols;
      UChar seqLength;
      T value;
      while ( totalVoxels < size ) {
        input.get( c );
        seqLength = static_cast<UChar>( c );
        input.read( reinterpret_cast<char*>( &value ), sizeof( T ) );
        while  ( seqLength-- ) {
          *( p++ ) = value;
          totalVoxels++;
          if ( ! ( totalVoxels % planeSize ) )
            p += delta_plane;
          else if ( ! ( totalVoxels % _cols ) )
            p += delta_line;
        }
      }
    }
  }
  return true;
}

/**
 * 3DZ file format loader (from a memory buffer).
 *
 * @param filename Path to the ".3dz" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadFileBuffer( const unsigned char *data, size_t size )
{
  ByteInputMemory input( reinterpret_cast<const char*>(data), size );
  return load( input );
}


template< typename T >
std::istream &
ZZZImage<T>::loadPandoreAttributes( std::istream & file, bool inversionMode )
{
  Int32 attr[5];
  memset( attr, 0, 5 * sizeof( Int32 ) );
  attr[0] = 1;

  file.read( reinterpret_cast<char*>( attr ), 4 );
  if ( inversionMode ) {
    reverseBytes<4>( attr );
    reverseBytes<4>( attr + 1 );
    reverseBytes<4>( attr + 2 );
    reverseBytes<4>( attr + 3 );
  }

  if ( ! POHasBands[ this->_poFileType ] ) attr[0] = 1;

  if ( POdimension[ this->_poFileType ] == 3 ||
       POdimension[ this->_poFileType ] == -3 )
    file.read( reinterpret_cast<char*>( attr + 1 ), 4 );

  if ( POdimension[ this->_poFileType ] >= 2 ||
       POdimension[ this->_poFileType ] <= -2 )
    file.read( reinterpret_cast<char*>( attr + 2 ), 4 );

  if ( POdimension[ this->_poFileType ] >= 1 ||
       POdimension[ this->_poFileType ] <= -1 )
    file.read( reinterpret_cast<char*>( attr + 3 ), 4 );

  attr[ 4 ] = UNKNOWN;
  if ( POColor[ this->_poFileType ] ) {
    file.read( reinterpret_cast<char*>( attr + 4 ), 4 );
    attr[0] = 3;
    if ( inversionMode )
      reverseBytes<4>( attr + 4 );
    this->_colorSpace = static_cast<ColorSpace>( attr[ 4 ] );
    TRACE << "COLORSPACE: " << this->_colorSpace
          << " " << ColorSpaceNames[ this->_colorSpace ] << "\n";
  }

  alloc( attr[0], attr[1], attr[2], attr[3] );
  return file;
}

template< typename T >
ByteOutput &
ZZZImage<T>::savePandoreAttributes( ByteOutput & out ) const
{
  Int32 attr[5];
  attr[0] = _bands;
  attr[1] = _depth;
  attr[2] = _rows;
  attr[3] = _cols;
  attr[4] = _colorSpace;

  // if ( POHasBands[ this->_poFileType ] )
  out.write( reinterpret_cast<char*>( attr ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] == 3 ||
       POdimension[ this->_poFileType ] == -3 )
    out.write( reinterpret_cast<char*>( attr + 1 ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] >= 2 ||
       POdimension[ this->_poFileType ] <= -2 )
    out.write( reinterpret_cast<char*>( attr + 2 ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] >= 1 ||
       POdimension[ this->_poFileType ] <= -1 )
    out.write( reinterpret_cast<char*>( attr + 3 ), sizeof( *attr ) );

  if ( POColor[ this->_poFileType ] )
    out.write( reinterpret_cast<char*>( attr + 4 ), sizeof( *attr ) );
  return out;
}

/*
 * Dave object data.
 */
template< typename T >
ByteOutput &
ZZZImage<T>::savePandoreData( ByteOutput & out ) const
{
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
    for ( SSize b = 0; b < _bands; b++ )
      for ( SSize z = 0; z < _depth; z++ )
        for ( SSize y = 0; y < _rows; y++) {
          if ( ! out.write( (char*) _bands_array[ b ][ z ][ y ],
                            sizeof( T ) *  _cols ) )
            return out;
        }
  } else {
    for ( SSize b = 0; b < _bands; b++ )
      if ( ! out.write( (char*) dataVector( b ),
                        sizeof( T ) *  _depth * _rows * _cols ) )
        return out;
  }
  return out;
}

/**
 * Save UInt32 data using the minimum of 1, 2 or 4 bytes according
 * to a maximum value specified.
 *
 * @param file
 * @param maximalValue The maximal value of the labels in a region image.
 *
 * @return The provided output stream.
 */
template< typename T >
ByteOutput &
ZZZImage<T>::saveRegionData( ByteOutput & out, UInt32 maximalValue ) const {

  T * start = _bands_array[ 0 ][0][0];
  T *p = start;
  size_t size = _depth * _rows * _cols;
  size_t totalVoxels = 0;

  if ( maximalValue < (T)( numeric_limits<UChar>::max() ) ) {
    UChar c;
    while ( totalVoxels < size ) {
      c = (UChar) *p;
      if ( ! out.write( (char*) &c, sizeof( c ) ) )
        return out;
      p++;
      totalVoxels++;
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        if ( ! ( totalVoxels % ( _cols * _rows )) )
          p += 2 * ( _cols + 2 ) + 2;
        else if ( ! ( totalVoxels % _cols ))
          p += 2;
      }
    }
    return out;
  }

  if ( maximalValue <= numeric_limits<UChar>::max() ) {
    Short s;
    while ( totalVoxels < size ) {
      s = (Short) *p;
      if ( ! out.write( (char*) &s, sizeof( s ) ) )
        return out;
      p++;
      totalVoxels++;
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        if ( ! ( totalVoxels % ( _cols * _rows )) )
          p += 2 * ( _cols + 2 ) + 2;
        else if ( ! ( totalVoxels % _cols ))
          p += 2;
      }
    }
    return out;
  }

  UInt32 l;
  while ( totalVoxels < size ) {
    l = static_cast<UInt32>( *p );
    if ( ! out.write( (char*) &l, sizeof( l ) ) )
      return out;
    p++;
    totalVoxels++;
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
      if ( ! ( totalVoxels % ( _cols * _rows )) )
        p += 2 * ( _cols + 2 ) + 2;
      else if ( ! ( totalVoxels % _cols ))
        p += 2;
    }
  }
  return out;
}

/**
 * Load UInt32 data using the minimum of 1, 2 or 4 bytes according
 * to a maximum value specified.
 *
 * @param file
 * @param maximalValue The maximal value of the labels in a region image.
 *
 * @return The provided input stream.
 */
template< typename T >
bool
ZZZImage<T>::loadRegionData( ByteInput & input, bool swapBytes, UInt32 maximalValue )  {

  T *start = _bands_array[ 0 ][0][0];
  T *p = start;
  size_t size = _depth * _rows * _cols;
  size_t totalVoxels = 0;
  UInt32 l;

  if ( maximalValue <= (T) (numeric_limits<UChar>::max()) ) {
    UChar c;
    while ( totalVoxels < size ) {
      if ( ! input.read( (char*) &c, sizeof( c ) ) )
        return input;
      *p = (T) c;
      totalVoxels++;
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        if ( ! ( totalVoxels % ( _cols * _rows )) )
          p += 2 * ( _cols + 2 ) + 2;
        else if ( ! ( totalVoxels % _cols ))
          p += 2;
      } else p++;
    }
  } else if ( maximalValue <= numeric_limits<UShort>::max() ) {
    Short s;
    while ( totalVoxels < size ) {
      if ( ! input.read( (char*) &s, sizeof( s ) ) )
        return input;
      if ( swapBytes ) reverseBytes<sizeof(s)>( & s );
      *p = (T) s;
      p++;
      totalVoxels++;
      if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
        if ( ! ( totalVoxels % ( _cols * _rows )) )
          p += 2 * ( _cols + 2 ) + 2;
        else if ( ! ( totalVoxels % _cols ))
          p += 2;
      }
    }
  } else while ( totalVoxels < size ) {
    if ( ! input.read( (char*) &l, sizeof( l ) ) )
      return input;
    if ( swapBytes ) reverseBytes<sizeof(l)>( & l );
    *p = (T) l;
    p++;
    totalVoxels++;
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
      if ( ! ( totalVoxels % ( _cols * _rows )) )
        p += 2 * ( _cols + 2 ) + 2;
      else if ( ! ( totalVoxels % _cols ))
        p += 2;
    }
  }
  return input;
}

/**
 * CSV file format writer.
 *
 * @param filename Path to the ".csv" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveCSV( ByteOutput & out ) const
{
  if ( _bands == 1 ) {
    T ***matrix = _bands_array[0];
    for (SSize z = 0; z < _depth; z++ ) {
      for ( SSize y = 0; y < _rows; y++ ) {
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( matrix[z][y][x] )
            out << x << ";" << y << ";" << z << "\n";
        }
      }
    }
    return true;
  }
  if ( _bands == 3 ) {
    T ***band0 = _bands_array[0];
    T ***band1 = _bands_array[1];
    T ***band2 = _bands_array[2];
    for (SSize z = 0; z < _depth; z++ ) {
      for ( SSize y = 0; y < _rows; y++ ) {
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( band0[z][y][x] || band1[z][y][x] || band2[z][y][x] )
            out << x << ";" << y << ";" << z << "\n";
        }
      }
    }
    return true;
  }
  return false;
}

/**
 * JBM file format writer.
 *
 * @param filename Path to the ".jbm" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveJBM( ByteOutput & out ) const
{
  T ***matrix = _bands_array[0];
  out << "{\n";

  for (SSize z = 0; z < _depth; z++ ) {
    out << "{";
    for ( SSize y = 0; y < _rows; y++ ) {
      out << "{";
      for ( SSize x = 0; x < _cols; x++ ) {
        if ( x < _cols - 1 )
          out << matrix[ z ][ y ][ x ] <<",";
        else
          out << matrix[ z ][ y ][ x ] <<"}";
      }
      if (y < _rows - 1)
        out << ",";
      else
        out << "},\n";
    }
  }
  out << "}\n";
  return true;
}

/**
 * BMP file format loader.
 *
 * @param filename Path to the ".bmp" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::loadBMP( std::istream & file )
{
  unsigned long s, w, h;
  unsigned long offset;
  unsigned char cmap[256][3];
  unsigned short bitsPerPixel;

  if ( _bands != 3 ) {
    ERROR << ("ZZZImage<>::loadBMP() failed because image is not a color image.");
    return false;
  }

  char magic[2];
  file.get( magic[ 0 ] );
  file.get( magic[ 1 ] );

  if ( magic[ 0 ]  != 'B' || magic[ 1 ] !=  'M') {
    ERROR << "ZZZImage<>::loadBMP() failed: bad header.";
    return false;
  }
  /* File size + 4 Reserved bytes */
  file.seekg( 8, std::ios::cur );

  offset = readWordLSB( file );
  s = readWordLSB( file );
  w = readWordLSB( file );
  h = readWordLSB( file );

  TRACE << "offset: " << offset << std::endl;
  TRACE << "s: " << s << std::endl;
  TRACE << "width: " << w << std::endl;
  TRACE << "height: " << h << std::endl;

  readShortLSB( file );		/* biPlanes */

  bitsPerPixel = readShortLSB( file );

  if ( props().bands != 3) {
    ERROR << "ZZZImage<>::readBMP(): object should have 3 planes.\n";
    return false;
  }

  if ( bitsPerPixel == 16 ) {	/* Bits per pixel */
    ERROR << "ZZZImage<>::readBMP(): only 8 or 24 bits images are handled\n";
    return false;
  }

  if ( readWordLSB( file ) ) {	/* Compression */
    ERROR << "ZZZImage<>::readBMP(): cannot handle compressed BMP format.\n";
    return false;
  }

  readWordLSB( file );		/* ImageDataSize */
  readWordLSB( file );		/* XPixelPerMeter */
  readWordLSB( file );		/* XPixelPerMeter */

  readWordLSB( file );		/* ColorsUsed */
  readWordLSB( file );		/* ColorImportants */

  _aspect = 1.0f;

  if ( bitsPerPixel == 8) {
    char c;
    for ( int i = 0; i < 256; i++) {
      file.read( reinterpret_cast<char*>( cmap[i] ), 3 );
      file.get( c ); // Dumb byte
    }

    alloc( w, h , 1l , 3);
    T ***matrixRed = _bands_array[ 0 ];
    T ***matrixGreen = _bands_array[ 1 ];
    T ***matrixBlue = _bands_array[ 2 ];
    unsigned char u;
    int full_line = ( (w + 3) / 4) * 4;
    for (unsigned int line = 0; line < h; line++ ) {
      unsigned int col;
      for ( col = 0; col < w; col++ ) {
        file.get( c );
        u = c;
        matrixRed[0][line][col] = cmap[u][0];
        matrixGreen[0][line][col] = cmap[u][1];
        matrixBlue[0][line][col] = cmap[u][2];
      }
      file.seekg( full_line - col, std::ios::cur );
    }
  } else { // bitsPerPixel == 24
    alloc( w, h , 1l , 3l );
    T ***matrixRed = _bands_array[ 0 ];
    T ***matrixGreen = _bands_array[ 1 ];
    T ***matrixBlue = _bands_array[ 2 ];
    char red,green,blue;
    int full_line = ( (w + 3) / 4) * 4;
    for ( unsigned int line = 0; line < h; line++ ) {
      unsigned int col;
      for ( col = 0; col < w; col++ ) {
        file.get( red );
        file.get( green );
        file.get( blue );
        matrixRed[0][line][col] = red;
        matrixGreen[0][line][col] = green;
        matrixBlue[0][line][col] = blue;
      }
      file.seekg( full_line - col, std::ios::cur );
    }
  }
  return true;
}

/**
 * BMP file format writer.
 *
 * @param filename Path to the ".bmp" file to be saved.
 * @return true if file has been successfully loaded, otherwise false.
 */
template<typename T>
bool
ZZZImage<T>::saveBMP( ByteOutput & out ) const
{
  if ( _bands != 3 ) {
    ERROR << "ZZZImage<>::saveBMP() failed because image is not a color image.";
    return false;
  }

  out.put('B');
  out.put('M');

  Size lineSize =  ( ( ( _cols * 24 ) + 31 ) / 32 ) * 4;

  writeWordLSB( out, 54 + ( _rows * lineSize ) );
  writeShortLSB( out, 0);
  writeShortLSB( out, 0);
  writeWordLSB( out, 54 );
  writeWordLSB( out, 40 );
  writeWordLSB( out, _cols );
  writeWordLSB( out, _rows );
  writeShortLSB( out, 1);
  writeShortLSB( out, 24);

  writeWordLSB( out, 0 );
  writeWordLSB( out, _rows * lineSize );
  writeWordLSB( out, 75*39 );
  writeWordLSB( out, 75*39 );
  writeWordLSB( out, 0 );
  writeWordLSB( out, 0 );

  T ***matrixRed = _bands_array[ 0 ];
  T ***matrixGreen = _bands_array[ 1 ];
  T ***matrixBlue = _bands_array[ 2 ];
  Size fullLine = ( (_cols + 3) / 4) * 4;

  for ( int l = 0; l < _rows; l++ ) {
    int c;
    for ( c = 0; c < _cols ; c++ ) {
      out.put( static_cast<char> ( matrixRed[0][l][c] ) );
      out.put( static_cast<char> ( matrixGreen[0][l][c] ) );
      out.put( static_cast<char> ( matrixBlue[0][l][c] ) );
    }
    for ( ; c < fullLine ; c++ ) out.put( 0 );
  }
  return true;
}

/*
 *
 * Tools methods
 *
 */

template< typename T >
void
ZZZImage<T>::reverseDataBytes()
{
  if ( sizeof(T) == 1 ) return;
  T * p = dataVector( 0 );
  T * limit = p + _bands * realBandVectorSize();
  while ( p != limit )
    reverseBytes<sizeof(T)>( p++ );
}

template< typename T >
Size
ZZZImage<T>::nonZeroVoxels()
{
  Size n = 0;
  if ( _bands == 1 ) {
    T * p = dataVector( 0 );
    T * limit = p + realBandVectorSize();
    while ( p != limit ) {
      if ( *p++ ) ++n;
    }
  } else {
    Size index = 0;
    Size limit = _cols * _rows * _depth;
    while ( index != limit ) {
      if ( zero( index++, 0, 0 ) ) ++n;
    }
  }
  return n;
}

template<typename T>
void
ZZZImage<T>::extract( ZZZImage<T>  & result,
                      SSize x, SSize y, SSize z,
                      SSize width, SSize height, SSize depth )
{
  result.alloc( width, height, depth, _bands );

  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;

  for ( SSize band = 0; band < _bands; ++band ) {
    T ***src = _bands_array[ band ];
    T ***dst = result._bands_array[ band ];
    for ( SSize d = 0; d < depth; d++ )
      for ( SSize h = 0; h < height; h++ )
        memcpy( dst[d][h], src[d+z][h+y] + x, width * sizeof( T ) );
  }
}

template<typename T>
ZZZImage<T> *
ZZZImage<T>::sub( SSize x, SSize y, SSize z,
                  SSize width, SSize height, SSize depth )
{
  ZZZImage<T> * result = new ZZZImage<T>( width, height, depth, _bands );

  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;

  for ( SSize b = 0; b < _bands; ++b ) {
    T ***src = _bands_array[ b ];
    T ***dst = result->_bands_array[ b ];
    for ( SSize d = 0; d < depth; d++ )
      for ( SSize h = 0; h < height; h++ )
        memcpy( dst[d][h], src[d+z][h+y] + x, width * sizeof( T ) );
  }
  return result;
}

template<typename T>
ZZZImage<T> *
ZZZImage<T>::slice( SSize x, SSize y, SSize z )
{
  ZZZImage<T> * result;
  if ( x >= 0 )  {
    result = new ZZZImage<T>( _rows, _depth, 1, _bands );
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = _bands_array[ b ];
      T ***dst = result->_bands_array[ b ];
      for ( SSize d = 0; d < _depth; d++ )
        for ( SSize h = 0; h < _rows; h++ )
          dst[0][d][h] = src[d][h][x];
    }
    return result;
  }
  if ( y >= 0 )  {
    result = new ZZZImage<T>( _cols, _depth, 1, _bands );
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = _bands_array[ b ];
      T ***dst = result->_bands_array[ b ];
      for ( SSize d = 0; d < _depth; d++ )
        for ( SSize w = 0; w < _cols; w++ )
          dst[0][d][w] = src[d][y][w];
    }
    return result;
  }
  if ( z >= 0 )  {
    result = new ZZZImage<T>( _cols, _rows, 1, _bands );
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = _bands_array[ b ];
      T ***dst = result->_bands_array[ b ];
      for ( SSize h = 0; h < _rows; h++ )
        for ( SSize w = 0; w < _cols; w++ )
          dst[0][h][w] = src[z][h][w];
    }
    return result;
  }
  ERROR << "ZZZImage<>::slice(): Wrong arguments";
  return 0;
}

template<typename T>
void
ZZZImage<T>::insert( SSize x, SSize y, SSize z,
                     AbstractImage * image )
{
  if ( typeid( *image ) != typeid( *this ) ) return;
  ZZZImage<T> * im = dynamic_cast< ZZZImage<T>* >( image );
  SSize width = im->width();
  SSize height = im->height();
  SSize depth = im->depth();
  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( SSize b = 0; b < _bands; ++b ) {
    T ***src = im->_bands_array[ b ];
    T ***dst = _bands_array[ b ];
    for ( SSize d = 0; d < depth; d++ )
      for ( SSize h = 0; h < height; h++ )
        memcpy( dst[z+d][y+h]+x, src[d][h], width * sizeof( T ) );
  }
}

template<typename T>
void
ZZZImage<T>::insertSlice( SSize x, SSize y, SSize z,
                          AbstractImage * image )
{
  if ( typeid( *image ) != typeid( *this ) ) return;
  ZZZImage<T> * im = dynamic_cast< ZZZImage<T>* >( image );
  SSize width = im->width();
  SSize height = im->height();
  if ( x >= 0 ) {
    if ( width > _rows ) width = _rows;
    if ( height > _depth ) height = _depth;
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = im->_bands_array[ b ];
      T ***dst = _bands_array[ b ];
      for ( SSize j = 0; j < height; j++ )
        for ( SSize i = 0; i < width; i++ )
          dst[j][i][x] = src[0][j][i];
    }
    return;
  }
  if ( y >= 0 ) {
    if ( width > _cols ) width = _cols;
    if ( height > _depth ) height = _depth;
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = im->_bands_array[ b ];
      T ***dst = _bands_array[ b ];
      for ( SSize j = 0; j < height; j++ )
        for ( SSize i = 0; i < width; i++ )
          dst[j][y][i] = src[0][j][i];
    }
    return;
  }
  if ( z >= 0 ) {
    if ( width > _cols ) width = _cols;
    if ( height > _rows ) height = _rows;
    for ( SSize b = 0; b < _bands; ++b ) {
      T ***src = im->_bands_array[ b ];
      T ***dst = _bands_array[ b ];
      for ( SSize j = 0; j < height; j++ )
        for ( SSize i = 0; i < width; i++ )
          dst[z][j][i] = src[0][j][i];
    }
    return;
  }
}


template< typename T >
ZZZImage<T> *
ZZZImage<T>::levelMap( SSize maxHeight )
{
  SSize w = _cols;
  SSize h = _rows;
  ZZZImage<T> * clone = new ZZZImage<T>( _cols, _rows, maxHeight, _bands );
  T maxVal = max();
  if ( _bands == 1 ) {
    T ***matrixSrc = data( 0 );
    T ***matrix = clone->data( 0 );
    for ( SSize row = 0; row < h; ++row ) {
      for ( SSize col = 0; col < w; ++col ) {
        T x = matrixSrc[ 0 ][ row ][ col ];
        SSize value = static_cast<SSize>( maxHeight * ( x / static_cast<double>(maxVal) ) );
        if ( !value ) value = 1;
        for ( SSize plane = 0; plane < value; plane++ )
          matrix[ plane ][ row ][ col ] = matrixSrc[ 0 ][ row ][ col ];
      }
    }
  } else {
    SSize v;
    double vf;
    T ***matrixSrcRed = data( 0 );
    T ***matrixSrcGreen = data( 1 );
    T ***matrixSrcBlue = data( 2 );
    T ***matrixRed = clone->data( 0 );
    T ***matrixGreen = clone->data( 1 );
    T ***matrixBlue = clone->data( 2 );
    for ( SSize row = 0; row < h; ++row ) {
      for ( SSize col = 0; col < w; ++col ) {
        vf =  0.299 * matrixSrcRed[0][row][col]/static_cast<double>(maxVal)
            + 0.587 * matrixSrcGreen[0][row][col]/static_cast<double>(maxVal)
            + 0.114 * matrixSrcBlue[0][row][col]/static_cast<double>(maxVal);
        v = static_cast<SSize>( maxHeight * vf );
        if ( !v ) v = 1;
        for ( SSize plane = 0; plane < v; plane++ ) {
          matrixRed[ plane ][ row ][ col ] = matrixSrcRed[ 0 ][ row ][ col ];
          matrixGreen[ plane ][ row ][ col ] = matrixSrcGreen[ 0 ][ row ][ col ];
          matrixBlue[ plane ][ row ][ col ] = matrixSrcBlue[ 0 ][ row ][ col ];
        }
      }
    }
  }
  return clone;
}

template< typename T >
T
ZZZImage<T>::gray( const Triple<SSize> & t )
{
  if ( bands() == 1 )
    return data( 0 )[ t.third ][ t.second ][ t.first ];
  else
    return static_cast<T>( data( 0 )[ t.third ][ t.second ][ t.first ]*0.299
        + data( 1 )[ t.third ][ t.second ][ t.first ]*0.587
        + data( 2 )[ t.third ][ t.second ][ t.first ]*0.114 );
}

template< typename T >
T
ZZZImage<T>::gray( const Pair<SSize> & t )
{
  if ( bands() == 1 )
    return data( 0 )[ 0 ][ t.second ][ t.first ];
  else
    return static_cast<T>( data( 0 )[ 0 ][ t.second ][ t.first ]*0.299
        + data( 1 )[ 0 ][ t.second ][ t.first ]*0.587
        + data( 2 )[ 0 ][ t.second ][ t.first ]*0.114 );
}

template< typename T >
T
ZZZImage<T>::gray( SSize x, SSize y, SSize z )
{
  if ( bands() == 1 )
    return data( 0 )[ z ][ y ][ x ];
  else
    return static_cast<T>( data( 0 )[ z ][ y ][ x ]*0.299
                           + data( 1 )[ z ][ y ][ x ]*0.587
                           + data( 2 )[ z ][ y ][ x ]*0.114 );
}

template< typename T >
void
ZZZImage<T>::insert( const ZZZImage<T> & other, SSize x, SSize y, SSize z )
{
  SSize width = other._cols;
  SSize height = other._rows;
  SSize depth = other._depth;
  T ***srcMatrix;
  T ***dstMatrix;

  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( SSize band = 0; band < _bands; ++band ) {
    srcMatrix = other._bands_array[ band ];
    dstMatrix = _bands_array[ band ];
    for ( SSize d = 0; d < depth; d++ )
      for ( SSize h = 0; h < height; h++ )
        memcpy( dstMatrix[z+d][y+h] + x, srcMatrix[d][h], width * sizeof( T ) );
  }
}



template< typename T >
bool
ZZZImage<T>::setValue( Triple<SSize> voxel,
                       UChar red, UChar green, UChar blue )
{
  T formerValue = 0;
  UChar gray = static_cast<UChar>( ( red*11.0 + green*16.0 + blue*5.0 )/32.0 );
  if ( _bands == 1 ) {
    formerValue = operator()( voxel, 0 );
    operator()( voxel, 0 ) = static_cast<T>( gray );
  } else {
    formerValue = operator()( voxel, 0 ) || operator()( voxel, 1 ) || operator()( voxel, 2 );
    operator()( voxel, 0 ) = static_cast<T>( red );
    operator()( voxel, 1 ) = static_cast<T>( green );
    operator()( voxel, 2 ) = static_cast<T>( blue );
  }
  return  ( formerValue && !gray ) || ( !formerValue && gray );
}

template< typename T >
void
ZZZImage<T>::negative()
{
  T max = numeric_limits<T>::max();
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
    SSize x, y, z, band;
    for ( band = 0; band < _bands; ++band )
      for ( x = 0; x < _cols; x++ )
        for ( y = 0; y < _rows; y++ )
          for ( z = 0; z < _depth; z++ ) {
            _bands_array[band][z][y][x] = max - _bands_array[band][z][y][x];
          }
  } else {
    for ( SSize band = 0; band < _bands; ++band ) {
      T *limit = dataVector( band) + _bands * realBandVectorSize();
      for ( T * p = dataVector( band ); p < limit; ++p ) {
        *p = max - *p;
      }
    }
  }
}

template< typename T >
void
ZZZImage<T>::grayShadeFromHue()
{
  WARNING << "Calling ZZZImage<T>::grayShadeFromHue() with T != UChar\n";
}

template< typename T >
void
ZZZImage<T>::grayShadeFromSaturation()
{
  WARNING << "Calling ZZZImage<T>::grayShadeFromSaturation() with T != UChar\n";
}

template<>
void
ZZZImage<UChar>::grayShadeFromHue()
{
  if ( _bands == 3 ) {
    ZZZImage<UChar> * result = new ZZZImage<UChar>( width(), height(), depth(), 1 );
    SSize x, y, z;
    double red,green,blue;
    double hue,value;
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          red = _bands_array[0][z][y][x] / 255.0;
          green = _bands_array[1][z][y][x] / 255.0;
          blue = _bands_array[2][z][y][x] / 255.0;
          double min = (red<green) ? red : green;
          if ( blue < min ) min = blue;
          unsigned char max = (red>green) ? red : green;
          if ( blue > max ) max = blue;
          if ( max == min ) {
            hue = 0.0;
          } else {
            const double diff = max - min;
            if ( max == red ) {
              hue = (green - blue) / diff;
            } else if ( max == green ) {
              hue = 2.0 + ( ( blue - red ) / diff );
            } else if ( max == blue ) {
              hue = 4.0 + ( ( red - green ) / diff );
            }
            hue *= 60.0;
            if ( hue < 0.0 ) hue += 360.0;
          }
          (*result)(x,y,z,0) = static_cast<UChar>( 255.0 * ( hue / 360.0 ) );
        }
    swapData(*result);
    delete result;
  }
}

template<>
void
ZZZImage<UChar>::grayShadeFromSaturation()
{
  if ( _bands == 3 ) {
    ZZZImage<UChar> * result = new ZZZImage<UChar>( width(), height(), depth(), 1 );
    SSize x, y, z;
    double red,green,blue;
    double saturation,value;
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          red = _bands_array[0][z][y][x] / 255.0;
          green = _bands_array[1][z][y][x] / 255.0;
          blue = _bands_array[2][z][y][x] / 255.0;
          double min = (red<green) ? red : green;
          if ( blue < min ) min = blue;
          unsigned char max = (red>green) ? red : green;
          if ( blue > max ) max = blue;
          value = max;
          if ( max == min ) {
            saturation = 0.0;
          } else {
            const double diff = max - min;
            saturation = diff / value;
          }
          (*result)(x,y,z,0) = static_cast<UChar>( 255.0 * saturation );
        }
    swapData(*result);
    delete result;
  }

}


template< typename T >
void
ZZZImage<T>::binarize( UChar value )
{
  if ( _bands == 1 ) {
    T *p = dataVector( 0 );
    T *limit = p + realBandVectorSize();
    while ( p != limit ) {
      if ( *p != static_cast<T>( 0 ) ) {
        *p = value;
      }
      ++p;
    }
    return;
  }
  Size index = 0;
  Size end = realBandVectorSize();
  SSize b;
  while ( index != end ) {
    if ( ! zero( index, 0, 0 ) )
      for ( b=0; b<_bands; ++b )
        operator()( index, 0, 0, b ) = value;
    ++index;
  }
}

template< typename T >
void
ZZZImage<T>::binarize( UChar red, UChar green, UChar blue )
{
  SSize x, y, z;
  if ( _bands == 3 ) {
    T ***redMatrix = data( 0 );
    T ***greenMatrix = data( 1 );
    T ***blueMatrix = data( 2 );
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          if ( redMatrix[z][y][x] != static_cast<T>( 0 )
               || greenMatrix[z][y][x] != static_cast<T>( 0 )
               || blueMatrix[z][y][x] != static_cast<T>( 0 ) ) {
            redMatrix[z][y][x] = static_cast<T>( red );
            greenMatrix[z][y][x] = static_cast<T>( green );
            blueMatrix[z][y][x] = static_cast<T>( blue );
          }
        }
  } else {
    T gray = static_cast<T>( ( red*11.0 + green*16.0 + blue*5.0 ) / 32.0 );
    T ***redMatrix = data( 0 );
    T ***greenMatrix = data( 1 );
    T ***blueMatrix = data( 2 );
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          if ( redMatrix[z][y][x] != static_cast<T>( 0 )
               || greenMatrix[z][y][x] != static_cast<T>( 0 )
               || blueMatrix[z][y][x] != static_cast<T>( 0 ) ) {
            redMatrix[z][y][x] = gray;
            greenMatrix[z][y][x] = gray;
            blueMatrix[z][y][x] = gray;
          }
        }
  }
}

template< typename T >
void
ZZZImage<T>::toGray()
{
  int grayLevel;
  if ( _bands < 3 ) return;
  ZZZImage<T> res( _cols, _rows, _depth, 1 );
  T ***gray = res.data( 0 );
  T ***red = data( 0 );
  T ***green = data( 1 );
  T ***blue = data( 2 );
  SSize x,y,z;
  for ( x = 0; x < _cols; x++ )
    for ( y = 0; y < _rows; y++ )
      for ( z = 0; z < _depth; z++ ) {
        grayLevel =  static_cast<int>( 0.299 * red[z][y][x]  + 0.597 * green[z][y][x] + 0.114 * blue[z][y][x] );
        if ( grayLevel > 255 ) gray[z][y][x] = 255;
        else gray[z][y][x] = static_cast<T>( grayLevel );
      }
  res.swapData( *this );
  _colorSpace = UNKNOWN;

  if ( _depth == 1 ) {
    if ( _rows == 1 )
      _poFileType = ImxFileType< T, 1 >::type;
    else
      _poFileType = ImxFileType< T, 2 >::type;
  } else _poFileType = ImxFileType< T, 3 >::type;
}


template< typename T >
void
ZZZImage<T>::toColor()
{
  if ( _bands != 1 ) return;
  ZZZImage<T> res( _cols, _rows, _depth, 3 );
  T ***gray = data( 0 );
  T ***red = res.data( 0 );
  T ***green = res.data( 1 );
  T ***blue = res.data( 2 );
  SSize x,y,z;
  for ( x = 0; x < _cols; x++ )
    for ( y = 0; y < _rows; y++ )
      for ( z = 0; z < _depth; z++ )
        red[z][y][x] = green[z][y][x]  = blue[z][y][x] = gray[z][y][x];
  res.swapData( *this );
}


template< typename T >
void
ZZZImage<T>::mirror( int axis )
{
  SSize x, y, z;
  Size limit;
  T tmp;
  switch ( axis ) {
  case 0:
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = data( band );
      limit = _cols / 2;
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < limit; ++x ) {
            tmp = matrix[z][y][x];
            matrix[z][y][x] = matrix[z][y][ (_cols - 1)  - x ];
            matrix[z][y][ ( _cols - 1 ) - x ] = tmp;
          }
    }
    break;
  case 1:
    for ( SSize band = 0; band < _bands ; ++band) {
      T ***matrix = data( band );
      limit = _rows / 2;
      for ( x = 0; x < _cols; ++x )
        for ( z = 0; z < _depth; ++z )
          for ( y = 0; y < limit; ++y ) {
            tmp = matrix[z][y][x];
            matrix[z][y][x] = matrix[ z ][ ( _rows - 1 ) - y ][ x ];
            matrix[z][ ( _rows - 1 ) - y ][ x ] = tmp;
          }
    }
    break;
  case 2:
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = data( band );
      limit = _depth / 2;
      for ( y = 0; y < _rows; ++y )
        for ( x = 0; x < _cols; ++x )
          for ( z = 0; z < limit; ++z ) {
            tmp = matrix[z][y][x];
            matrix[z][y][x] = matrix[ ( _depth - 1 ) - z ][y][x];
            matrix[ ( _depth - 1 ) - z ][y][x] = tmp;
          }
    }
    break;
  }
}

template< typename T >
void
ZZZImage<T>::rotate( int axis )
{
  SSize x,y,z;
  switch ( axis ) {
  case 0:
  {
    ZZZImage<T> result( _cols, _depth, _rows );
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = data( band );
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(x,_depth-(z+1),y,band)=matrix[z][y][x];
          }
    }
    swapData( result );
  }
    break;
  case 1:
  {
    ZZZImage<T> result( _depth, _rows, _cols );
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = data( band );
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(_depth-(z+1),y,x,band)=matrix[z][y][x];
          }
    }
    swapData( result );
  }
    break;
  case 2:
  {
    ZZZImage<T> result( _rows, _cols, _depth );
    for ( SSize band = 0; band < _bands ; ++band ) {
      T ***matrix = data( band );
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(y,_cols-(x+1),z,band)=matrix[z][y][x];
          }
    }
    swapData( result );
  }
    break;
  }
}

template< typename T >
bool
ZZZImage<T>::isBorder( SSize x, SSize y, SSize z, char adjacency ) const
{
  Config c = config( x, y, z );
  c = ( ~c ) & MASK_26_NEIGHBORS;
  switch ( adjacency ) {
  case ADJ26:
  case ADJ18:
    return ( c & MASK_6_NEIGHBORS );
    break;
  case ADJ6:
    return ( c & MASK_26_NEIGHBORS );
    break;
  case ADJ6P:
    return ( c & MASK_18_NEIGHBORS );
    break;
  }
  return false;
}

template< typename T >
void
ZZZImage<T>::getFramedBitMask( ZZZImage<Bool> & mask ) const
{
  mask.alloc( width()+2, height()+2, depth()+2, 1, true );
  for ( SSize z = 0; z < _depth; ++z )
    for ( SSize y = 0; y < _rows ; ++y )
      for ( SSize x = 0; x < _cols; ++x )
        mask(x+1,y+1,z+1) = nonZero(x,y,z);
}

template< typename T >
void
ZZZImage<T>::applyFramedBitMask( const ZZZImage<Bool> & mask )
{
  assert( mask.width() == width() + 2 );
  assert( mask.height() == height() + 2 );
  assert( mask.depth() == depth() + 2 );
  for ( SSize z = 0; z < _depth; ++z )
    for ( SSize y = 0; y < _rows ; ++y )
      for ( SSize x = 0; x < _cols; ++x ) {
        if ( ! mask(x+1,y+1,z+1) )
          for ( SSize band = 0; band < _bands; ++band )
            _bands_array[band][z][y][x] = static_cast<T>( 0 );
      }
}


template< typename T >
Config
ZZZImage<T>::config( SSize x, SSize y, SSize z ) const
{
  Config result = 0;
  T ***matrix = _bands_array[ 0 ];
  T *p = matrix[z - 1][y - 1] + ( x - 1 );
  T *origin = p;
  Config mask = 1;
  SSize inc_row = ( _cols + 2 ) - 2;
  SSize inc_depth = ( _rows + 2 ) * ( _cols + 2 );

  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    inc_row = _cols - 2;
    inc_depth = _rows * _cols;
  }
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;		/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;		/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;                     /* _ */
  if ( *p )  result |= mask;
  p = origin + inc_depth; mask <<= 1; /* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;		/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;            /* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p = origin + ( inc_depth << 1 );
  mask <<= 1;			        /* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;            /* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row; mask <<= 1;		/* _ */
  if ( *p )  result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p; mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  return result;
}

template< typename T >
Config
ZZZImage<T>::config( const Triple<SSize> & voxel ) const
{
  Config result = 0;
  T ***matrix = _bands_array[ 0 ];
  T *p = matrix[voxel.third - 1][voxel.second - 1] + ( voxel.first - 1 );
  T *origin = p;
  Config mask = 1;
  SSize inc_row = ( _cols + 2 ) - 2;
  SSize inc_depth = ( _rows + 2 ) * ( _cols + 2 );
  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    inc_row = _cols - 2;
    inc_depth = _rows * _cols;
  }

  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;                  /* _ */
  if ( *p )  result |= mask;
  p = origin + inc_depth;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p = origin + ( inc_depth << 1 );
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  p += inc_row;
  mask <<= 1;			/* _ */
  if ( *p )  result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  ++p;
  mask <<= 1;			/* _ */
  if ( *p ) result |= mask;
  return result;
}

template< typename T >
Config
ZZZImage<T>::config2D( SSize x, SSize y, SSize z ) const
{
  Config cnf = 0;
  T ***matrix = _bands_array[ 0 ];
  if ( matrix[z][y+1][x] ) cnf |=1;
  if ( matrix[z][y+1][x+1] ) cnf |=2;
  if ( matrix[z][y][x+1]  ) cnf |=4;
  if ( matrix[z][y-1][x+1] ) cnf |= 8;
  if ( matrix[z][y-1][x] ) cnf |= 16;
  if ( matrix[z][y-1][x-1] ) cnf |= 32;
  if ( matrix[z][y][x-1]) cnf |= 64;
  if ( matrix[z][y+1][x-1]) cnf |= 128;
  return cnf;
}

/* ATTENTION : Les points marques du bit 2 sont terminaux et donc pas dans P */
template< typename T >
void
ZZZImage<T>::configExtended( Config & res_conf, Config & res_P,
                             SSize x, SSize y, SSize z ) const
{
  T ***matrix = _bands_array[ 0 ];
  T *p = matrix[z - 1][y - 1] + ( x - 1 );
  T *origin = p;
  Config mask = 1;
  Config conf = 0, border = 0;
  SSize inc_row = ( _cols + 2 ) - 2;
  SSize inc_depth = ( _rows + 2 ) * ( _cols + 2 );;
  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    inc_row = _cols - 2;
    inc_depth = _rows * _cols;
  }

  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p = origin + inc_depth; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p = origin + ( inc_depth << 1 ); mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 )  border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }

  res_conf = conf;
  res_P = border;
}

/* ATTENTION : Les points marques du bit 2 sont terminaux et donc pas dans P */
template< typename T >
void
ZZZImage<T>::configExtended( Config & res_conf,
                             Config & res_P,
                             const Triple<SSize> & voxel ) const
{
  T ***matrix = _bands_array[ 0 ];
  T *p = matrix[ voxel.third - 1 ][ voxel.second - 1 ] + ( voxel.first - 1 );
  T *origin = p;
  Config mask = 1;
  Config conf = 0, border = 0;
  SSize inc_row = ( _cols + 2 ) - 2;
  SSize inc_depth = ( _rows + 2 ) * ( _cols + 2 );
  if ( _arrayLayout == ImageProperties::NormalArrayLayout ) {
    inc_row = _cols - 2;
    inc_depth = _rows * _cols;
  }

  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p;  mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p = origin + inc_depth; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p = origin + ( inc_depth << 1 ); mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 )  border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  p += inc_row; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }
  ++p; mask <<= 1;
  if ( *p ) {
    conf |= mask;
    if ( ( *p ) == 3 ) border |= mask;
  }

  res_conf = conf;
  res_P = border;
}

template<typename T>
void ZZZImage<T>::getColor( SSize x, SSize y, SSize z, UChar & red, UChar & green, UChar & blue )
{
#ifndef _RELEASE_
  assert( _bands == 3 );
#endif
  if ( _colorMode == CastColorMode ) {
    red = static_cast<UChar>(_bands_array[0][z][y][x]);
    green = static_cast<UChar>(_bands_array[1][z][y][x]);
    blue = static_cast<UChar>(_bands_array[2][z][y][x]);
  } else {
    
  }
}

template<typename T>
void ZZZImage<T>::getColor( const Triple<SSize> & position, UChar & red, UChar & green, UChar & blue )
{
#ifndef _RELEASE_
  assert( _bands == 3 );
#endif
  if ( _colorMode == CastColorMode ) {
    red = static_cast<UChar>(_bands_array[0][position.third][position.second][position.first]);
    green = static_cast<UChar>(_bands_array[1][position.third][position.second][position.first]);
    blue = static_cast<UChar>(_bands_array[2][position.third][position.second][position.first]);
  } else {
    
  }
}

template<typename T>
void ZZZImage<T>::getGrayLevel( SSize x, SSize y, SSize z, UChar & gray )
{
  if ( _colorMode == CastColorMode ) {
    gray = static_cast<UChar>(_bands_array[0][z][y][x]);
  } else {
    // TODO
  }
}

template<typename T>
void ZZZImage<T>::getGrayLevel( const Triple<SSize> & position, UChar & gray )
{
  if ( _colorMode == CastColorMode ) {
    gray = static_cast<UChar>(_bands_array[0][position.third][position.second][position.first]);
  } else {
    // TODO
  }
}


template<typename T>
void ZZZImage<T>::setColorMode(AbstractImage::ColorMode colorMode)
{
  _colorMode = colorMode;
}

template<typename T>
Triple<UChar> ZZZImage<T>::getColorFromLUT(const Triple<SSize> & voxel,
                                           const std::vector< Triple<UChar> > & lut) const
{
  static const Triple<UChar> black(0,0,0);
#ifdef _DEBUG_
  ValueType valueType = Po_type< ZZZImage<T> >::type ;
  Q_ASSERT( type == Po_ValUC || type == Po_ValUS || type == Po_ValUL );
#endif
  UInt32 value = (*this)(voxel,0);
  if ( value < lut.size() )
    return lut[value];
  else
    return black;
}

template< typename T >
void
ZZZImage<T>::mark26border( UChar value )
{
  T *p;
  SSize deltaZ;
  T ***matrix = _bands_array[ 0 ];
  SSize deltaZeroFramed = 0;
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) deltaZeroFramed = 2;
  deltaZ = ( _rows + deltaZeroFramed ) *
      ( _cols + deltaZeroFramed );

  for ( SSize z = 0; z < _depth; z++ )
    for ( SSize y = 0; y < _rows; y++ )
      for ( SSize x = 0; x < _cols; x++ ) {
        p = matrix[z][y] + x;
        if ( *p ) {
          *p = 1;
          if ( ! *( p + _cols + deltaZeroFramed ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - ( _cols + deltaZeroFramed ) ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p + 1 ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - 1 ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - deltaZ ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p + deltaZ ) )
            matrix[z][y][x] = static_cast<T>( value );
        }
      }
}


template< typename T >
void
ZZZImage<T>::mark18border( UChar value )
{
  T *p;
  SSize deltaZ;
  SSize deltaZeroFramed = 0;
  T ***matrix = _bands_array[ 0 ];

  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) deltaZeroFramed = 2;
  deltaZ = ( _rows + deltaZeroFramed ) *
      ( _cols + deltaZeroFramed );


  for ( SSize z = 0; z < _depth; z++ )
    for ( SSize y = 0; y < _rows; y++ )
      for ( SSize x = 0; x < _cols; x++ ) {
        p = matrix[z][y] + x;
        if ( *p ) {
          *p = 1;
          if ( !*( p + _cols + deltaZeroFramed ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - ( _cols + deltaZeroFramed ) ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p + 1 ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - 1 ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p - deltaZ ) )
            matrix[z][y][x] = static_cast<T>( value );
          else if ( !*( p + deltaZ ) )
            matrix[z][y][x] = static_cast<T>( value );
        }
      }
}


template< typename T >
void
ZZZImage<T>::mark6border( UChar value )
{
  SSize x, y, z;
  Config conf;
  T ***matrix = _bands_array[ 0 ];
  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ )
        if ( matrix[ z ][ y ][ x ] ) {
          conf = config( x, y, z );
          if ( ( ~conf ) & MASK_26_NEIGHBORS )
            matrix[ z ][ y ][ x ] = static_cast<T>( value );
        }
}


template< typename T >
void
ZZZImage<T>::mark6Pborder( UChar value )
{
  SSize x, y, z;
  Config conf;
  T ***matrix = _bands_array[ 0 ];

  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ )
        if ( matrix[ z ][ y ][ x ] ) {
          conf = config( x, y, z );
          if ( ( ~conf ) & MASK_18_NEIGHBORS )
            matrix[ z ][ y ][ x ] = static_cast<T>( value );
        }
}

template<typename T>
UChar
ZZZImage<T>::orientation( SSize x, SSize y, SSize z ) const
{
  UChar result = 0;
  T ***matrix = _bands_array[ 0 ];
  if ( !matrix[ z ][ y - 1][ x ] ) result |= 1 << NORTH;
  if ( !matrix[ z ][ y + 1][ x ] ) result |= 1 << SOUTH;
  if ( !matrix[ z ][ y ][ x + 1 ]) result |= 1 << EAST;
  if ( !matrix[ z ][ y ][ x - 1 ]) result |= 1 << WEST;
  if ( !matrix[ z + 1 ][ y ][ x ]) result |= 1 << BEHIND;
  if ( !matrix[ z - 1 ][ y ][ x ]) result |= 1 << BEFORE;
  return result;
}

template<typename T>
UChar
ZZZImage<T>::orientation( const Triple<SSize> & voxel ) const
{
  UChar result=0;
  T ***matrix = _bands_array[0];
  if(!matrix[voxel.third][voxel.second-1][voxel.first]) result|=1<<NORTH;
  if(!matrix[voxel.third][voxel.second+1][voxel.first]) result|=1<<SOUTH;
  if(!matrix[voxel.third][voxel.second][voxel.first+1]) result|=1<<EAST;
  if(!matrix[voxel.third][voxel.second][voxel.first-1]) result|=1<<WEST;
  if(!matrix[voxel.third+1][voxel.second][voxel.first]) result|=1<<BEHIND;
  if(!matrix[voxel.third-1][voxel.second][voxel.first]) result|=1<<BEFORE;
  return result;
}


template<typename T>
void
ZZZImage<T>::boundingBox( SSize & xMin, SSize & yMin, SSize & zMin,
                          SSize & xMax, SSize & yMax, SSize & zMax ) const {
  xMin = yMin = zMin = numeric_limits<SSize>::max();
  xMax = yMax = zMax = 0;
  if ( bands() == 1 ) {
    T ***matrix = data( 0 );
    for ( SSize z = 0 ; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( matrix[z][y][x] ) {
            minimize( xMin, x );
            minimize( yMin, y );
            minimize( zMin, z );
            maximize( xMax, x );
            maximize( yMax, y );
            maximize( zMax, z );
          }
        }
  } else {
    T ***matrixRed = data( 0 );
    T ***matrixGreen = data( 1 );
    T ***matrixBlue = data( 2 );
    for ( SSize z = 0 ; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( matrixRed[z][y][x] != 0
               || matrixGreen[z][y][x] != 0
               || matrixBlue[z][y][x] != 0 ) {
            minimize( xMin, x );
            minimize( yMin, y );
            minimize( zMin, z );
            maximize( xMax, x );
            maximize( yMax, y );
            maximize( zMax, z );
          }
        }
  }
}

template<typename T>
void
ZZZImage<T>::fitBoundingBox( SSize margin ) {
  SSize xMin, yMin, zMin;
  SSize xMax, yMax, zMax;

  boundingBox( xMin, yMin, zMin, xMax, yMax, zMax );

  ZZZImage<T> clone( *this );

  SSize depth = 2 * margin + 1 + zMax - zMin;
  SSize height = 2 * margin + 1 + yMax - yMin;
  SSize width = 2 * margin + 1 + xMax - xMin;
  SSize nbands = ZZZImage<T>::bands();

  alloc( width, height, depth, nbands );
  for ( SSize band = 0 ; band < nbands; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = clone.data( band );
    for ( SSize z = zMin ; z <= zMax ; z++ )
      for ( SSize y = yMin ; y <= yMax ; y++ )
        for ( SSize x = xMin ; x <= xMax ; x++ ) {
          matrix[ margin + z - zMin ][ margin + y - yMin ][ margin + x - xMin ] =
              matrixSrc[ z ][ y ][ x ];
        }
  }
}

template<typename T>
void
ZZZImage<T>::zeroPlane( SSize x, SSize y, SSize z )
{
  if ( x >= 0 ) {
    SSize b,y,z;
    for ( b = 0; b < _bands; ++b ) {
      T ***matrix = data( b );
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows; ++y )
          matrix[z][y][x] = 0;
    }
  }
  if ( y >= 0 ) {
    SSize b,x,z;
    for ( b = 0; b < _bands; ++b ) {
      T ***matrix = data( b );
      for ( z = 0; z < _depth; ++z )
        for ( x = 0; x < _cols; ++x )
          matrix[z][y][x] = 0;
    }
  }
  if ( z >= 0 ) {
    SSize b,x,y;
    for ( b = 0; b < _bands; ++b ) {
      T ***matrix = data( b );
      for ( y = 0; y < _rows; ++y )
        for ( x = 0; x < _cols; ++x )
          matrix[z][y][x] = 0;
    }
  }
}

template<typename T>
bool
ZZZImage<T>::merge( AbstractImage & other, int axis )
{
  ZZZImage<T> * pother = 0;
  try {
    pother = dynamic_cast< ZZZImage<T>* >( &other );
  } catch ( std::bad_cast ) {
    ERROR << "ZZZImage<T>::merge() : value type mismatch.\n";
    return false;
  }
  return merge( *pother, axis );
}

template<typename T>
bool
ZZZImage<T>::merge( ZZZImage<T> & other, int axis )
{
  if ( bands() != other.bands() ) return false;

  SSize nbands = bands();

  ZZZImage<T> left;
  left.swapData( *this );

  SSize leftDepth = left.depth();
  SSize leftHeight = left.height();
  SSize leftWidth = left.width();
  SSize rightDepth = other.depth();
  SSize rightHeight = other.height();
  SSize rightWidth = other.width();

  SSize depth = 0;
  SSize height = 0;
  SSize width = 0;

  SSize dx = 0, dy = 0, dz = 0;

  if ( axis == 0 ) {
    dx = leftWidth;
    depth = ::max( leftDepth, rightDepth );
    height = ::max( leftHeight, rightHeight );
    width = leftWidth + rightWidth;
  }
  if ( axis == 1 ) {
    dy = leftHeight;
    depth = ::max( leftDepth, rightDepth );
    height = leftHeight + rightHeight;
    width = ::max(leftWidth,  rightWidth );
  }
  if ( axis == 2 ) {
    dz = leftDepth;
    depth = leftDepth + rightDepth;
    height = ::max( leftHeight, rightHeight );
    width = ::max(leftWidth,  rightWidth );
  }
  if ( axis == 3 ) {
    depth = ::max( leftDepth, rightDepth );
    height = ::max( leftHeight, rightHeight );
    width = ::max(leftWidth,  rightWidth );
  }

  alloc( width, height, depth, nbands );

  for ( SSize band = 0 ; band < nbands; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = left.data( band );
    for ( SSize z = 0 ; z < leftDepth ; z++ )
      for ( SSize y = 0 ; y < leftHeight ; y++ )
        for ( SSize x = 0 ; x < leftWidth ; x++ )
          matrix[ z ][ y ][ x ] = matrixSrc[ z ][ y ][ x ];
    if ( axis == 3 ) {
      matrixSrc = other.data( band );
      for ( SSize z = 0 ; z < rightDepth ; z++ )
        for ( SSize y = 0 ; y < rightHeight ; y++ )
          for ( SSize x = 0 ; x < rightWidth ; x++ )
            if ( ! matrix[ z + dz ][ y + dy ][ x + dx ] )
              matrix[ z + dz ][ y + dy ][ x + dx ] = matrixSrc[ z ][ y ][ x ];
    } else {
      matrixSrc = other.data( band );
      for ( SSize z = 0 ; z < rightDepth ; z++ )
        for ( SSize y = 0 ; y < rightHeight ; y++ )
          for ( SSize x = 0 ; x < rightWidth ; x++ )
            matrix[ z + dz ][ y + dy ][ x + dx ] = matrixSrc[ z ][ y ][ x ];
    }
  }
  return true;
}

template<typename T>
void
ZZZImage<T>::trim( int axis, SSize n )
{
  SSize b = bands();
  ZZZImage<T> before;
  before.swapData( *this );

  SSize depth = before.depth();
  SSize height = before.height();
  SSize width = before.width();
  SSize dx = 0, dy = 0, dz = 0;

  if ( axis == 0 ) {
    dx = n;
    width = 1;
  }
  if ( axis == 1 ) {
    dy = n;
    height = 1;
  }
  if ( axis == 2 ) {
    dz = n;
    depth = 1;
  }
  this->alloc( width, height, depth, b );

  for ( SSize band = 0 ; band < b; band++ ) {
    T ***matrix = this->data( band );
    T ***matrixSrc = before.data( band );
    for ( SSize z = 0 ; z < depth ; z++ )
      for ( SSize y = 0 ; y < height ; y++ )
        for ( SSize x = 0 ; x < width ; x++ )
          matrix[ z ][ y ][ x ] = matrixSrc[ z + dz ][ y + dy ][ x + dx ];
  }
}

template<typename T>
void
ZZZImage<T>::subSample( UChar minCount ) {
  SSize d = depth();
  SSize h = height();
  SSize w = width();
  SSize nbands = bands();
  ZZZImage<T> clone( *this );
  UChar count;

  w >>= 1;
  h >>= 1;
  d >>= 1;
  if ( ! w ) w = 1;
  if ( ! h ) h = 1;
  if ( ! d ) d = 1;

  alloc( w, h, d, nbands );
  double sum;

  for ( SSize band = 0 ; band < nbands; band++ ) {
    T ***matrix = this->data( band );
    T ***matrixSrc = clone.data( band );
    for ( SSize z = 0 ; z < d ; z++ )
      for ( SSize y = 0 ; y < h ; y++ )
        for ( SSize x = 0 ; x < w ; x++ ) {
          count = 0;
          sum = 0.0;
          count += ( matrixSrc[ z<<1 ][ y<<1 ][ x<<1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ z<<1 ][ y<<1 ][ (x<<1) + 1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ z<<1 ][ (y<<1) + 1 ][ x<<1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ (z<<1) + 1 ][ y<<1 ][ x<<1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ z<<1 ][ (y<<1) + 1 ][ (x<<1) + 1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ (z<<1) + 1 ][ (y<<1) + 1 ][ x<<1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ (z<<1) + 1 ][ y<<1 ][ (x<<1) + 1 ] != static_cast<T>( 0 ) );
          count += ( matrixSrc[ (z<<1) + 1 ][ (y<<1) + 1 ][ (x<<1) + 1 ] != static_cast<T>( 0 ) );

          sum += matrixSrc[ z<<1 ][ y<<1 ][ x<<1 ];
          sum += matrixSrc[ z<<1 ][ y<<1 ][ (x<<1) + 1 ];
          sum += matrixSrc[ z<<1 ][ (y<<1) + 1 ][ x<<1 ];
          sum += matrixSrc[ (z<<1) + 1 ][ y<<1 ][ x<<1 ];
          sum += matrixSrc[ z<<1 ][ (y<<1) + 1 ][ (x<<1) + 1 ];
          sum += matrixSrc[ (z<<1) + 1 ][ (y<<1) + 1 ][ x<<1 ];
          sum += matrixSrc[ (z<<1) + 1 ][ y<<1 ][ (x<<1) + 1 ];
          sum += matrixSrc[ (z<<1) + 1 ][ (y<<1) + 1 ][ (x<<1) + 1 ];
          if ( count >= minCount )
            matrix[ z ][ y ][ x ] = static_cast<T>(  sum / count );
        }
  }
}

template<typename T>
void
ZZZImage<T>::scale( UChar direction, UChar factor )
{
  SSize w = width();
  SSize h = height();
  SSize d = depth();
  SSize b = bands();
  ZZZImage<T> clone( *this );

  switch ( direction ) {
  case 0: w *= factor; break;
  case 1: h *= factor; break;
  case 2: d *= factor; break;
  case 3: d *= factor; w *= factor; h *= factor; break;
  }
  alloc( w, h, d, b );

  for ( SSize band = 0 ; band < b; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = clone.data( band );
    switch ( direction ) {
    case 0:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            matrix[ z ][ y ][ x ] = matrixSrc[ z ][ y ][ x / factor ];
      break;
    case 1:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            matrix[ z ][ y ][ x ] = matrixSrc[ z ][ y / factor ][ x ];
      break;
    case 2:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            matrix[ z ][ y ][ x ] = matrixSrc[ z / factor ][ y ][ x ];
      break;
    case 3:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            matrix[ z ][ y ][ x ] = matrixSrc[ z / factor ][ y / factor ][ x / factor ];
      break;
    }
  }
}

template<typename T>
void
ZZZImage<T>::extrude( SSize d )
{
  if ( depth() != 1 ) return;
  SSize w = width();
  SSize h = height();
  SSize b = bands();

  ZZZImage<T> clone( *this );
  alloc( w, h, d, b );

  for ( SSize band = 0 ; band < b; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = clone.data( band );
    for ( SSize z = 0 ; z < d ; ++z )
      for ( SSize y = 0 ; y < h ; ++y )
        for ( SSize x = 0 ; x < w ; ++x )
          matrix[ z ][ y ][ x ] = matrixSrc[ 0 ][ y ][ x ];
  }
}

template<typename T>
void ZZZImage<T>::clamp( const Triple<Int32> & min, const Triple<Int32> & max)
{
  const T zero = static_cast<T>(0);
  T *p = dataVector( 0 );
  T *limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Int32>(*p) < min.first) || (static_cast<Int32>(*p) > max.first) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 1 ) return;
  p = dataVector( 1 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Int32>(*p) < min.second ) || (static_cast<Int32>(*p) > max.second) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 2 ) return;
  p = dataVector( 2 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Int32>(*p) < min.third ) || (static_cast<Int32>(*p) > max.third) ) {
      *p = zero;
    }
    ++p;
  }
}

template<typename T>
void ZZZImage<T>::clamp( const Triple<UInt32> & min, const Triple<UInt32> & max)
{
  const T zero = static_cast<T>(0);
  T *p = dataVector( 0 );
  T *limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<UInt32>(*p) < min.first) || (static_cast<UInt32>(*p) > max.first) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 1 ) return;
  p = dataVector( 1 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<UInt32>(*p) < min.second ) || (static_cast<UInt32>(*p) > max.second) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 2 ) return;
  p = dataVector( 2 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<UInt32>(*p) < min.third ) || (static_cast<UInt32>(*p) > max.third) ) {
      *p = zero;
    }
    ++p;
  }
}

template<typename T>
void ZZZImage<T>::clamp( const Triple<Float> & min, const Triple<Float> & max)
{
  const T zero = static_cast<T>(0);
  T *p = dataVector( 0 );
  T *limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Float>(*p) < min.first) || (static_cast<Float>(*p) > max.first) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 1 ) return;
  p = dataVector( 1 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Float>(*p) < min.second ) || (static_cast<Float>(*p) > max.second) ) {
      *p = zero;
    }
    ++p;
  }
  if ( _bands == 2 ) return;
  p = dataVector( 2 );
  limit = p + realBandVectorSize();
  while ( p != limit ) {
    if ( (static_cast<Float>(*p) < min.third ) || (static_cast<Float>(*p) > max.third) ) {
      *p = zero;
    }
    ++p;
  }
}

template<typename T>
void
ZZZImage<T>::spongify()
{
  (*this) = zzzSponge(*this);
}

template<typename T>
void
ZZZImage<T>::bitPlane( UChar bit, SSize band )
{
  SSize w,h,d,b;
  getDimension( w, h, d, b );
  if ( band >= b ) return;

  ZZZImage<T> clone( *this );
  alloc( w, h, d, b );
  UInt32 mask = 1;
  mask <<= bit;
  for ( SSize band = 0 ; band < b; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = clone.data( band );
    for ( SSize z = 0 ; z < d ; z++ )
      for ( SSize y = 0 ; y < h ; y++ )
        for ( SSize x = 0 ; x < w ; x++ )
          matrix[ z ][ y ][ x ] = ( static_cast<UInt32>( matrixSrc[ z ][ y ][ x ] ) & mask ) ? 1 : 0;
  }
}

template<typename T>
void
ZZZImage<T>::swapEndianness()
{
  T *p = dataVector( 0 );
  const T *limit = p + _bands * realBandVectorSize();
  while ( p != limit )
    reverseBytes<sizeof(T)>( p++ );
}

template<typename T>
void
ZZZImage<T>::quantize( UInt32 cardinality )
{
  T *p = dataVector( 0 );
  const T *limit = p + _bands * realBandVectorSize();
  while ( p != limit ) {
    if ( *p )
      *p = ::quantize( *p, cardinality );
    ++p;
  }
}

template<typename T>
ZZZImage<T> &
ZZZImage<T>::mask( const ZZZImage< UChar > & mask )
{
  const T * limit = dataVector( 0 ) + _bands * realBandVectorSize();
  UChar * po = mask.dataVector( 0 );
  for ( T *p = dataVector( 0 ); p < limit; ++p, ++po )
    if ( *po == 0  ) *p = 0;
  return *this;
}

template<typename T>
Pair<T>
ZZZImage<T>::range( SSize band )
{
  Pair<T> r( numeric_limits<T>::max(), numeric_limits<T>::min() );
  SSize x, y, z, b;
  if ( band == -1 ) {
    for ( b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            minimize( r.first, (*this)( x, y, z, b ) );
            maximize( r.second, (*this)( x, y, z, b ) );
          }
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          minimize( r.first, (*this)( x, y, z, band ) );
          maximize( r.second, (*this)( x, y, z, band ) );
        }
  }
  return r;
}

template<typename T>
T
ZZZImage<T>::min( SSize band )
{
  T r = numeric_limits<T>::max();
  SSize x, y, z, b;
  if ( band == -1 ) {
    for ( b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            minimize( r, (*this)( x, y, z, b ) );
          }
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          minimize( r, (*this)( x, y, z, band ) );
        }
  }
  return r;
}

template<typename T>
T
ZZZImage<T>::max( SSize band )
{
  T r = numeric_limits<T>::min();
  SSize x, y, z, b;
  if ( band == -1 ) {
    for ( b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            maximize( r, (*this)( x, y, z, b ) );
          }
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          maximize( r, (*this)( x, y, z, band ) );
        }
  }
  return r;
}

template<typename T>
ZZZImage< T > &
ZZZImage<T>::operator&=( const ZZZImage< T > & other )
{
  const T *limit = dataVector( 0 ) + _bands * realBandVectorSize();
  T *po = other.dataVector( 0 );
  for ( T *p = dataVector( 0 ); p < limit; ++p, ++po )
    if ( ! *po )
      *p = 0;
  return *this;
}

template<typename T>
ZZZImage< T > &
ZZZImage<T>::operator|=( const ZZZImage< T > & other )
{
  const T *limit = dataVector( 0 ) + _bands * realBandVectorSize();
  T *po = other.dataVector( 0 );
  for ( T * p = dataVector( 0 ); p < limit; ++p, ++po )
    if ( ! *p && *po )
      *p = *po;
  return *this;
}

template<typename T>
ZZZImage< T >
ZZZImage<T>::operator|( const ZZZImage< T > & other )
{
  ZZZImage<T> res(*this);
  res|=other;
  return res;
}

template<typename T>
ZZZImage< T > &
ZZZImage<T>::operator-=( const ZZZImage< T > & other )
{
  T *limit = dataVector( 0 ) + _bands * realBandVectorSize();
  T *po = other.dataVector( 0 );
  for ( T * p = dataVector( 0 ); p < limit; ++p, ++po )
    if ( *p && *po )
      *p = 0;
  return *this;
}

template<typename T>
void
ZZZImage<T>::bitPlaneGrayCode( UChar bit, SSize band )
{
  SSize w=0,h=0,d=0,b=0;
  getDimension( w, h, d, b );
  if ( band >= b ) return;

  ZZZImage<T> clone( *this );
  alloc( w, h, d, b );
  UInt32 mask = 1;
  mask <<= bit;
  for ( SSize band = 0 ; band < b; band++ ) {
    T ***matrix = data( band );
    T ***matrixSrc = clone.data( band );
    for ( SSize z = 0 ; z < d ; z++ )
      for ( SSize y = 0 ; y < h ; y++ )
        for ( SSize x = 0 ; x < w; x++ ) {
          UInt32 l = static_cast<UInt32>( matrixSrc[ z ][ y ][ x ] );
          l = l ^ ( l >> 1 );
          matrix[ z ][ y ][ x ] = ( l & mask ) ? 1 : 0;
        }
  }
}

template<typename T>
void
ZZZImage<T>::hollowOut( int adjacency )
{
  ZZZImage<T> im(*this);
  Config c;

  switch (adjacency) {
  case ADJ26:
  case ADJ18:
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          if ( im(x,y,z)
               && im(x+1,y,z) && im(x-1,y,z)
               && im(x,y+1,z) && im(x,y-1,z)
               && im(x,y,z+1) && im(x,y,z-1) ) {
            (*this)(x,y,z) = 0;
          }
        }
    break;
  case ADJ6:
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          if ( im(x,y,z) ) {
            c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
            if ( !( c & MASK_26_NEIGHBORS ) )
              (*this)(x,y,z) = 0;
          }
        }
    break;
  case ADJ6P:
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          if ( im(x,y,z) ) {
            c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
            if ( !(c & MASK_18_NEIGHBORS) )
              (*this)(x,y,z) = 0;
          }
        }
    break;
  }
}

/**
 *
 * 3D filling algorithm from:
 *   Lin Feng and Seah Hock Soon, "An effective 3D seed fill algorithm",
 *   Computers & Graphics, Volume 22, Issue 5, October 1998, Pages 641-644
 *
 * @param value
 */
template<typename T>
void
ZZZImage<T>::seedFill( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);

  if ( nonzero( triple( seed ) ) )
    return;

  const Triple<T> fillColor( static_cast<T>(value),
                             static_cast<T>(value),
                             static_cast<T>(value) );

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    triple(seed) = fillColor;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( isZero( triple( seed ) ) && holds( seed ) ) {
      triple(seed) = fillColor;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( isZero( triple( seed ) ) && holds( seed ) ) {
      triple(seed) = fillColor;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary
    // nor the previously completely filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Z+1
    // check the top scan line with 'z+1'
    ++seed.second;
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

template<typename T>
void
ZZZImage<T>::seedFillXY( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary
    // nor the previously completely filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}


template<typename T>
void
ZZZImage<T>::seedFillXZ( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Z+1
    // check the top scan line with 'z+1'
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

template<typename T>
void
ZZZImage<T>::seedFillYZ( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize ysave, yleft, yright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    ysave = seed.first;
    // fill the right span of the seed
    ++seed.second;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.second;
    }

    // save the x coordinate of the extreme right voxel
    yright = seed.second - 1;
    // reset the x coordinate to that of the seed
    seed.second = ysave;
    // fill the left span of the seed
    --seed.second;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.second;
    }

    // save the x coordinate of the extreme left voxel
    yleft = seed.second + 1;

    // Z+1
    // check the top scan line with 'z+1'
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.second = yleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.second < yright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first,seed.second-1,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.second;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first, seed.second-1, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.second = yleft;
    // store the status of the first voxel
    firstVoxelStatus = (!(*this)(seed) && holds( seed ) );
    ++seed.second;
    while ( seed.second < yright ) {
      secondVoxelStatus=(!(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first,seed.second-1,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.second;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first, seed.second-1, seed.third ) );
  }
}

/**
 *
 * 3D filling algorithm from:
 *   Lin Feng and Seah Hock Soon, "An effective 3D seed fill algorithm",
 *   Computers & Graphics, Volume 22, Issue 5, October 1998, Pages 641-644
 *
 * @param value
 */
template<typename T>
void
ZZZImage<T>::surfaceFill( UChar value )
{
  binarize( 1 );
  const Triple<T> borderColor(1,1,1);
  const Triple<T> fillColor(2,2,2);
  Triple<SSize> seed;

  bool found = false;
  for ( SSize z = 0; z < _depth && !found; ++z )
    for ( SSize y = 0; y < _rows && !found; ++y )
      for ( SSize x = 0; x < _cols && !found; ++x ) {
        if ( isZero( triple( x, y, z ) ) ) {
          seed.set( x, y, z );
          found = true;
        }
      }

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    triple(seed) = fillColor;    //(*this)(seed) = 2;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( triple(seed).copy() != borderColor  && holds( seed ) ) {
      triple(seed) = fillColor; // (*this)(seed) = 2;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( triple(seed) != borderColor  && holds( seed ) ) {
      triple(seed) = fillColor;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary
    // nor the previously completely filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Z+1
    // check the top scan line with 'z+1'
    ++seed.second;
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus )
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }

  UChar f = value;
  T ***matrix = 0;

  if ( value == 2 ) f = 3;
  for ( SSize b = 0; b < _bands; ++b ) {
    matrix = _bands_array[b];
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          if ( matrix[z][y][x] == 0 )
            matrix[z][y][x] = f;
        }
  }
  for ( SSize b = 0; b < _bands; ++b ) {
    matrix = _bands_array[b];
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          if ( matrix[z][y][x] == 2 )
            matrix[z][y][x] = 0;
        }
  }
  if ( f == 3 ) {
    for ( SSize b = 0; b < _bands; ++b ) {
      matrix = _bands_array[b];
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( matrix[z][y][x] == f )
              matrix[z][y][x] = value;
          }
    }
  }
}

template<typename T>
void
ZZZImage<T>::fillForHoleClosing()
{
  SSize x, y, z;
  if ( _bands == 3 ) {
    T ***red = data( 0 );
    T ***green = data( 1 );
    T ***blue = data( 2 );
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          if ( red[ z ][ y ][ x ] != static_cast<T>( 0 )
               || green[ z ][ y ][ x ] != static_cast<T>( 0 )
               || blue[ z ][ y ][ x ] != static_cast<T>( 0 ) )
            red[z][y][x] = green[z][y][x] = blue[z][y][x] = 5;
          else
            red[z][y][x] = green[z][y][x] = blue[z][y][x] = 1;
        }
  } else {
    T ***matrix = data( 0 );
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        for ( z = 0; z < _depth; z++ ) {
          if ( matrix[  z ][ y ][ x ] != static_cast<T>( 0 ) )
            matrix[z][y][x] = 5;
          else
            matrix[z][y][x] = 1;
        }
  }
}

template<typename T>
void
ZZZImage<T>::addBoundaries( UChar value, UChar thickness )
{
  SSize x = 0, y = 0, z = 0, band = 0;
  if ( thickness == 1 ) {
    for ( band = 0; band < _bands; ++band ) {
      T ***matrix = data( band );
      for ( z = 0; z < _depth; ++z ) {
        matrix[ z ][ 0 ][ 0 ] = value;
        matrix[ z ][ 0 ][ _cols - ( 1 ) ] = value;
        matrix[ z ][ _rows - ( 1 ) ][ 0 ] = value;
        matrix[ z ][ _rows - ( 1 ) ][ _cols - ( 1 ) ] = value;
      }
      for ( x = 0; x < _cols; ++x ) {
        matrix[ 0 ][ 0 ][ x ] = value;
        matrix[ _depth - ( 1 ) ][ 0 ][ x ] = value;
        matrix[ 0 ][ _rows - ( 1 ) ][ x ] = value;
        matrix[ _depth - ( 1 ) ][ _rows - ( 1 ) ][ x ] = value;
      }
      for ( y = 0; y < _rows; ++y ) {
        matrix[ 0 ][ y ][ 0 ] = value;
        matrix[ _depth - ( 1 ) ][ y ][ 0 ] = value;
        matrix[ 0 ][ y ][ _cols - ( 1 ) ] = value;
        matrix[ _depth - ( 1 ) ][ y ][ _cols - ( 1 ) ] = value;
      }
    }
  } else {
    for ( band = 0; band < _bands; ++band ) {
      T ***matrix = data( band );
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows; ++y )
          for ( x = 0; x < _cols; ++x ) {
            if  ( ( ( x < thickness
                      || ( x + thickness >= _cols )
                      || y < thickness
                      || ( y + thickness >= _rows ) )
                    && ( z < thickness || ( z + thickness ) >= _depth ) )
                  ||
                  ( ( z < thickness
                      || ( z + thickness >= _depth )
                      || y < thickness
                      || ( y + thickness >= _rows ) )
                    && ( x < thickness || ( x + thickness ) >= _cols ) ) )
              matrix[ z ][ y ][ x ] = value;
          }
    }
  }
}

template< typename T >
void
ZZZImage<T>::histogram( std::vector<Size> & h, T & minVal, T & maxVal, SSize band )
{
  // Compute maxVal
  maxVal = std::numeric_limits<T>::min();
  minVal = std::numeric_limits<T>::max();
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
    SSize x, y, z;
    T ***matrix = data( band );
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          if ( matrix[ z ][ y ][ x ] > maxVal )
            maxVal = matrix[ z ][ y ][ x ];
          if ( matrix[ z ][ y ][ x ] < minVal )
            minVal = matrix[ z ][ y ][ x ];
        }
  } else {
    T *limit = dataVector( band ) + realBandVectorSize();
    for ( T * p = dataVector( band ); p < limit; ++p ) {
      if ( *p > maxVal )
        maxVal = *p;
      if ( *p < minVal )
        minVal = *p;
    }
  }

  if ( h.size() )
    memset( &(h.front()), 0, h.size() * sizeof(Size) );

  if ( maxVal == std::numeric_limits<T>::min() ||
       minVal == std::numeric_limits<T>::max() ) return;

  long steps = h.size();
  long interval;
  double range = static_cast<double>(maxVal) - static_cast<double>(minVal);
  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
    SSize x, y, z;
    T ***matrix = data( band );
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          interval = static_cast<long>( (matrix[z][y][x] - minVal) * ( (steps-1) / range )  );
          if ( interval >= steps ) interval = steps - 1;
          h[interval]++;
        }
  } else {
    T *limit = dataVector( band ) + realBandVectorSize();
    for ( T * p = dataVector( band ); p < limit; ++p ) {
      interval = static_cast<long>( (*p - minVal) * ( (steps-1) / range )  );
      if ( interval >= steps ) interval = steps - 1;
      h[interval]++;
    }
  }
}

template< typename T >
void
ZZZImage<T>::free()
{
  if ( ! _bands_array )
    return;

  if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
    T * pt = _bands_array[0][-1][-1] - 1;
    delete[] pt;
  } else {
    delete[] _bands_array[0][0][0];
  }

  for ( SSize band = 0; band < _bands; ++band ) {
    if ( _arrayLayout == ImageProperties::ZeroFramedArrayLayout ) {
      T ** ppt = _bands_array[band][-1] - 1;
      delete[] ppt;
      T ***pppt = _bands_array[band] - 1;
      delete[] pppt;
    } else {
      delete[] _bands_array[band][ 0 ];
      delete[] _bands_array[band];
    }
  }
  disposeArray( _bands_array );
  _bands = 0L;
  _depth = _rows = _cols = 0L;
}

template< typename T >
void  ZZZImage<T>::accept( AbstractImageVisitor & visitor )
{
  visitor.visit(*this);
}

template<typename T>
const ZZZImage<T> ZZZImage<T>::null;


// Explicit instanciation of :
template class ZZZImage<UChar>;
template class ZZZImage<Int32>;
template class ZZZImage<UInt32>;
template class ZZZImage<Short>;
template class ZZZImage<Float>;
template class ZZZImage<Double>;
template class ZZZImage<UShort>;


