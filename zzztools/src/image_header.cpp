/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ImageHeader.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 *
 * @brief  Declaration of the class ImageHeader
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "image_header.h"
#include <map>
#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;


/**
 * Constructor
 */
ImageHeader::ImageHeader( ByteInput & in )
   : _in( in ),
     _fileType( UNKNOWN ),
     _poType( Po_Object ),
     _colorSpace( RGB ),
     _endian( ::endian() ),
     _width(0),
     _height(0),
     _depth(0),
     _bands(0),
     _aspect(1.0f,1.0f,1.0f),
     _isOk( false )
{
   memset( _magic, 0, 20 );
   if ( in ) {
      _isOk = read();
   }
}

ostream &
ImageHeader::flush( ostream & out ) const
{
   const char * str[] = { "BigEndian", "LittleEndian" };
   out << "MAGIC [" << _magic << "] " << str[ _endian ] << "\n";
   out << "FileType [" << _fileType << "] " << "\n";
   out << "size(" << _width << "," << _height << "," << _depth << ")\n";
   out << "bands:" << _bands << std::endl;
   out << " PoType = " << _poType << " " << POtypeName[ _poType ] <<  std::endl;
   return out;
}

std::ostream & operator<<( ostream & out, const ImageHeader & header )
{
   return header.flush( out );
}


ImageHeader::FileType
ImageHeader::fileType() const
{
   return _fileType;
}

Endianness
ImageHeader::endian() const
{
   return _endian;
}

bool
ImageHeader::read()
{
   const int State_error = 500;
   const int State_unknown = 501;
   const int State_pandore = 502;
   const int State_asc = 503;
   const int State_3dz = 504;
   const int State_vff = 505;
   const int State_vol = 506;
   const int State_npz = 507;

   FileType types[] = { UNKNOWN, UNKNOWN, TypePAN, TypeASC, Type3DZ, TypeVFF, TypeVOL, TypeNPZ };

   map< pair<int,char>, int > transitions;
   map< pair<int,char>, int >::iterator it;
   transitions[ make_pair(0,'P') ] = 1;
   transitions[ make_pair(1,'A') ] = 2;
   transitions[ make_pair(2,'N') ] = 3;
   transitions[ make_pair(3,'D') ] = 4;
   transitions[ make_pair(4,'O') ] = 5;
   transitions[ make_pair(5,'R') ] = 6;
   transitions[ make_pair(6,'E') ] = State_pandore;

   transitions[ make_pair(0,'O') ] = 7;
   transitions[ make_pair(7,'B') ] = 8;
   transitions[ make_pair(8,'J') ] = 9;
   transitions[ make_pair(9,'E') ] = 10;
   transitions[ make_pair(10,'T') ] = 11;
   transitions[ make_pair(11,'3') ] = 12;
   transitions[ make_pair(12,'D') ] = State_asc;

   transitions[ make_pair(0,'n') ] = 13;
   transitions[ make_pair(13,'c') ] = 14;
   transitions[ make_pair(14,'a') ] = 15;
   transitions[ make_pair(15,'a') ] = State_vff;

   transitions[ make_pair(0,'3') ] = 16;
   transitions[ make_pair(16,'d') ] = 17;
   transitions[ make_pair(17,'z') ] = State_3dz;

   transitions[ make_pair(0,'X') ] = 18;
   transitions[ make_pair(0,'Y') ] = 18;
   transitions[ make_pair(0,'Z') ] = 18;
   transitions[ make_pair(18,':') ] = 19;
   transitions[ make_pair(19,' ') ] = State_vol;

   transitions[ make_pair(0,'V') ] = 20;
   transitions[ make_pair(20,'o') ] = 21;
   transitions[ make_pair(21,'x') ] = 22;
   transitions[ make_pair(23,'e') ] = 24;
   transitions[ make_pair(24,'l') ] = 25;
   transitions[ make_pair(25,'-') ] = State_vol;

   transitions[ make_pair(0,'N') ] = 26;
   transitions[ make_pair(26,'P') ] = 27;
   transitions[ make_pair(27,'Z') ] = 28;
   transitions[ make_pair(28,'-') ] = State_npz;

   int state = 0;
   char c;
   char header[4096];
   size_t header_size = 0;

   while  ( state < 500 ) {
      if ( _in.get( c ) ) {
   header[ header_size++] = c;
   it = transitions.find( make_pair(state,c) );
   if ( it == transitions.end() ) {
      state = State_unknown;
   } else {
      state = it->second;
   }
      }
      else state = State_error;
   }


   if ( state == State_error ) {
      perror("ImageHeader::read()");
      return false;
   }
   if ( state == State_unknown ) {
      header[header_size] = '\0';
      ERROR << "ImageHeader::read(): Bad magic number [" << header << "]\n";
      return false;
   }
   _fileType = types[ state - State_error ];

   TSHOW( state );
   TSHOW( _fileType );

   switch ( _fileType ) {
   case TypePAN: return readPAN( header, header_size );
   case TypeVFF: return readVFF( header, header_size );
   case Type3DZ: return read3DZ( header, header_size );
   case TypeVOL: return readVOL( header, header_size );
   case TypeASC: return readASC( header, header_size );
   case TypeNPZ: return readNPZ( header, header_size );
   default: return false;
   }
   return false;
}

bool
ImageHeader::readVFF( char * header, size_t & header_size )
{
   memcpy( _magic, header, header_size );
   char c;
   while ( _in.get( c ) && c != 12 ) {
      header[ header_size++ ] = c;
   }
   header[ header_size ] = '\0';
   _in.get( c );

   char *pc = strstr( header, "\nsize=" );
   if ( !pc ) {
      ERROR << "ImageHeader::readVFF(): No size field\n";
      return false;
   }
   int x,y,z;
   sscanf( pc, "\nsize=%d %d %d;", &x, &y, &z);
   _bands = 1;
   _width = x;
   _height = y;
   _depth = z;
   _poType = Po_Imx3duc;

   pc = strstr( header, "\naspect=");
   if ( !pc ) {
      ERROR <<  "ImageHeader::readVFF(): No aspect field" ;
      _aspect = 1.0f;
   } else {
      sscanf(pc, "\naspect=%f %f %f",
       &_aspect.first,
       &_aspect.second,
       &_aspect.third );
   }

   TRACE << (*this);
   return true;
}

bool
ImageHeader::read3DZ( char * header, size_t & header_size )
{
   memcpy( _magic, header, header_size );
   char c;
   while ( _in.get( c ) && c != 12 ) {
      header[ header_size++ ] = c;
   }
   header[ header_size ] = '\0';
   _in.get( c );

   char *pc = strstr( header, "\nsize=" );
   if ( !pc ) {
      ERROR << "ImageHeader::read3DZ(): No size field\n";
      return false;
   }
   SSize depth, rows, cols;
   sscanf( pc, "\nsize=" SSIZE_FORMAT " " SSIZE_FORMAT " " SSIZE_FORMAT  ";",
     &depth, &rows, &cols );

   _width = cols;
   _height = rows;
   _depth = depth;

   _bands = 1;
   _poType = Po_Imx3duc;

   pc = strstr( header, "POTYPE=" );
   if ( pc ) {
      int t;
      sscanf( pc+7, "%d", &t );
      _poType = static_cast<Typobj>( t );
      TRACE << "3DZ FileTypeType: " << _poType
      << " ValueType: " << POvalueType[ _poType ] << std::endl;
   }
   pc = strstr( header, "BANDS=" );
   if ( pc ) {
      sscanf( pc + 6, SSIZE_FORMAT, &_bands );
      TRACE << "3DZ Bands: " << _bands << std::endl;
   }
   pc = strstr( header, "ENDIAN=Big" );
   if ( pc ) _endian = BigEndian;
   pc = strstr( header, "ENDIAN=Little" );
   if ( pc ) _endian = LittleEndian;
   TRACE << "Endianness: " << _endian << std::endl;
   TRACE << (*this);
   return true;
}

bool
ImageHeader::readPAN( char * header, size_t & header_size )
{
  TRACE << "ImageHeader::readPAN()\n";
  Po_header *pheader = reinterpret_cast<Po_header*>(header);
  bool inversionMode = false;

  _in.read( header+7, sizeof( Po_header ) - 7 );
  header_size += sizeof( Po_header ) - 7;
  pheader->magic[9] = '\0';

  strcpy( _magic, pheader->magic );

  if ( ! magicNumber( pheader->magic, "PANDORE0?" ) ) {
     ERROR << "ImageHeader::readPAN(): Bad magic number\n";
     ERROR << "         should be [PANDORE0?]\n";
     ERROR << "         but found [" << pheader->magic << "]\n";
     return false;
  }

  TRACE << "PoType:" << " "
        << pheader->potype << "(" << sizeof(pheader->potype) << " bytes )\n";

  if ( pheader->potype > 128 ) {
    reverseBytes< sizeof(pheader->potype) >( & pheader->potype );
    inversionMode =  true;
    _endian = static_cast<Endianness>( 1 - endian() );
  }

  TRACE << " PoType = "
        << pheader->potype << " " << POtypeName[ pheader->potype ] << std::endl;
  TRACE << " Dimension = " << POdimension[ pheader->potype ] << std::endl;
  TRACE << " ident =[" << pheader->ident << "]\n";
  TRACE << " date =[" << pheader->date << "]\n";

  _poType = pheader->potype;
  int fileType = pheader->potype;

  UInt32 ul;
  _in.read( reinterpret_cast<char*>( &ul ), 4 );
  if ( inversionMode ) reverseBytes32( ul );
  _bands = static_cast<SSize>(ul);
  TRACE << " bands = " << _bands << std::endl;

  if ( POdimension[ fileType ] == 3 ||
       POdimension[ fileType ] == -3 ) {
    _in.read( reinterpret_cast<char*>( &ul ), 4 );
    if ( inversionMode ) reverseBytes32( ul );
    _depth = static_cast<SSize>(ul);
  } else _depth = 1;
  TRACE << " depth = " << _depth << std::endl;

  if ( POdimension[ fileType ] >= 2 ||
       POdimension[ fileType ] <= -2 ) {
    _in.read( reinterpret_cast<char*>( &ul ), 4 );
    if ( inversionMode ) reverseBytes32( ul );
    _height = static_cast<SSize>(ul);
  } else _height = 1;
  TRACE << " rows = " << _height << std::endl;

  if ( POdimension[ fileType ] >= 1 ||
       POdimension[ fileType ] <= -1 ) {
    _in.read( reinterpret_cast<char*>( &ul ), 4 );
    if ( inversionMode ) reverseBytes32( ul );
    _width = static_cast<SSize>(ul);
  } else _width = 1;
  TRACE << " cols = " << _width << std::endl;

  if ( POColor[ fileType ] ) {
     _in.read( reinterpret_cast<char*>( &ul ), 4 );
     if ( inversionMode ) reverseBytes<4>( &ul );
     _colorSpace = static_cast<ColorSpace>( ul );
     TRACE << "COLORSPACE: " << _colorSpace
     << " " << ColorSpaceNames[ _colorSpace ] << "\n";
     _bands = 3;
     TRACE << " bands = " << _bands << std::endl;
  }

  TRACE << (*this);
  return true;
}

bool
ImageHeader::readVOL( char * header, size_t & header_size )
{
   strcpy( _magic, "VOL" );

   // Insert a dummy end of line as the first character
   memcpy( header+header_size+1, header, header_size );
   header[0] = '\n';
   memcpy( header+1, header+header_size+1, header_size );
   ++header_size;

   char line[4096];
   while ( _in.getline( line, 4096 )
     && strcmp( line, "." ) ) {
      strcpy( header + header_size, line );
      header_size += strlen( line );
      header[ header_size++ ] = '\n';
   }
   header[ header_size++ ] = '\0';

   TRACE << header ;
   _bands = 1;
   char *pc;

   pc = strstr( header, "\nX: " );
   if ( !pc ) {
      ERROR << "ImageHeader::readVOL(): No X size field.\n";
      return false;
   }
   sscanf( pc, "\nX: "SSIZE_FORMAT, &_width);

   pc = strstr( header, "\nY: " );
   if ( !pc ) {
      ERROR << "ImageHeader::readVOL(): No Y size field.\n";
      return false;
   }
   sscanf( pc, "\nY: "SSIZE_FORMAT, &_height);

   pc = strstr( header, "\nZ: " );
   if ( !pc ) {
      ERROR << "ImageHeader::readVOL(): No Z size field.\n";
      return false;
   }
   sscanf( pc, "\nZ: "SSIZE_FORMAT, &_depth);

   _poType = Po_Imx3duc;
   pc = strstr( header, "\nVoxel-Size: " );
   if ( pc ) {
      int voxel_size;
      sscanf( pc, "\nVoxel-Size: %d", &voxel_size );
      switch ( voxel_size ) {
      case 1: _poType = Po_Imx3duc; break;
      case 2: _poType = Po_Imx3dss; break;
      case 4: _poType = Po_Imx3dsl; break;
      case 125 /* ValueTypeSize[ Po_ValB ] */ : _poType = Po_Imx3db; break;
      }
   }

   pc = strstr( header, "\nInt-Endian: " );
   if ( pc ) {
      char str[125];
      sscanf( pc, "\nInt-Endian: %s", str );
      if ( strstr( str, "0123" ) )
   _endian = LittleEndian;
      if ( strstr( str, "3210" ) )
   _endian = BigEndian;
   }

   pc = strstr( header, "\nVoxel-Endian: " );
   if ( pc ) {
      char str[125];
      sscanf( pc, "\nVoxel-Endian: %s", str );
      if ( strstr( str, "01" ) )
   _endian = LittleEndian;
      if ( strstr( str, "10" ) )
   _endian = BigEndian;
   }

   int version = 1;
   pc = strstr( header, "\nVersion: " );
   if ( pc ) {
      sscanf( pc, "\nVersion: %d", &version );
   };
   if ( version == 1 ) {
      // Read a binary size line for version 1
      char c;
      while ( _in.get(c) && ( c!='\n') ) ;
   }
   TRACE << (*this);
   return true;
}

bool
ImageHeader::readNPZ( char * header, size_t & header_size )
{
   strcpy( _magic, "NPZ" );
   char line[4096];
   while ( _in.getline( line, 4096 )
     && strcmp( line, "DATA" ) ) {
      strcpy( header + header_size, line );
      header_size += strlen( line );
      header[ header_size++ ] = '\n';
   }
   header[ header_size++ ] = '\0';

   char *pc;
   pc = strstr( header, "\nTYPE " );
   if ( !pc ) {
      ERROR << "ImageHeader::readNPZ(): No TYPE field.\n";
      return false;
   }
   sscanf( pc, "\nTYPE %s", line );

   pc = strstr( header, "\nCOMP GZIP" );
   if ( !pc ) {
      ERROR << "ImageHeader::readNPZ(): No GZIP compression flag.\n";
      return false;
   }

   int dimension = 0;

   if (!strcmp(line, "NPIC_IMAGE_2B")) { dimension = 2; _poType = Po_Imx2db; }
   if (!strcmp(line, "NPIC_IMAGE_2C")) { dimension = 2; _poType = Po_Imx2duc; }
   if (!strcmp(line, "NPIC_IMAGE_2L")) { dimension = 2; _poType = Po_Imx2dsl; }
   if (!strcmp(line, "NPIC_IMAGE_2D")) { dimension = 2; _poType = Po_Imx2dsd; }
   if (!strcmp(line, "NPIC_IMAGE_2F")) { dimension = 2; _poType = Po_Imx2dsd; }

   if (!strcmp(line, "NPIC_IMAGE_3B")) { dimension = 3; _poType = Po_Imx3db; }
   if (!strcmp(line, "NPIC_IMAGE_3C")) { dimension = 3; _poType = Po_Imx3duc; }
   if (!strcmp(line, "NPIC_IMAGE_3L")) { dimension = 3; _poType = Po_Imx3dsl; }
   if (!strcmp(line, "NPIC_IMAGE_3D")) { dimension = 3; _poType = Po_Imx3dsd; }
   if (!strcmp(line, "NPIC_IMAGE_3F")) { dimension = 3; _poType = Po_Imx3dsd; }

   if (!strcmp(line, "NPIC_IMAGE_4B")) { dimension = 4; _poType = Po_Imx3db; }
   if (!strcmp(line, "NPIC_IMAGE_4C")) { dimension = 4; _poType = Po_Imx3duc; }
   if (!strcmp(line, "NPIC_IMAGE_4S")) { dimension = 4; _poType = Po_Imx3dss; }
   if (!strcmp(line, "NPIC_IMAGE_4L")) { dimension = 4; _poType = Po_Imx3dsl; }
   if (!strcmp(line, "NPIC_IMAGE_4D")) { dimension = 4; _poType = Po_Imx3dsd; }
   if (!strcmp(line, "NPIC_IMAGE_4F")) { dimension = 4; _poType = Po_Imx3dsd; }

   if ( !strcmp( line, "NPIC_IMAGE_2Q" ) ) {
      ERROR << "ImageHeader::readNPZ(): NPIC_IMAGE_2Q is not handled yet.\n";
   }

   pc = strstr( header, "\nXMAX " );
   if ( !pc ) {
      ERROR << "ImageHeader::readNPZ(): No XMAX field.\n";
      return false;
   } else {
      sscanf( pc, "\nXMAX "SSIZE_FORMAT, &_width );
   }

   pc = strstr( header, "\nYMAX " );
   if ( !pc ) {
      if ( dimension >= 2 ) {
   ERROR << "ImageHeader::readNPZ(): No YMAX field while dimension >= 2\n";
   return false;
      }
      _height = 1;
   } else {
      sscanf( pc, "\nYMAX "SSIZE_FORMAT, &_height );
   }

   pc = strstr( header, "\nZMAX " );
   if ( !pc ) {
      if ( dimension >= 3 ) {
   ERROR << "ImageHeader::readNPZ(): No ZMAX field while dimension >= 3\n";
   return false;
      }
      _depth = 1;
   } else {
      sscanf( pc, "\nZMAX "SSIZE_FORMAT, &_depth );
   }

   pc = strstr( header, "\nTMAX " );
   if ( !pc ) {
      if ( dimension >= 4 ) {
   ERROR << "ImageHeader::readNPZ(): No TMAX field while dimension >= 4\n";
   return false;
      }
      _bands = 1;
   } else {
      sscanf( pc, "\nTMAX "SSIZE_FORMAT, &_bands );
   }


   TRACE << header << "|\n" ;
   TRACE << (*this) ;
   return true;
}


bool
ImageHeader::readASC( char * header, size_t & header_size )
{
   memcpy( _magic, header, header_size );
   char c;
   char line[1024];

   _in.get(c);
   _in.getline( line, 1024 );
   sscanf( line, "TX,TY,TZ:"SSIZE_FORMAT","SSIZE_FORMAT","SSIZE_FORMAT,
     &_width, &_height, &_depth);

   _poType = Po_Imx3duc;
   _bands = 1;
   TRACE << (*this);
   return true;
}
