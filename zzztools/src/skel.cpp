/**
 * @file   skel.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:22 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "skel.h"
#include <set>
using std::set;
using std::vector;

namespace {
  inline void
  addNewBorderVoxels( vector< Triple<SSize> > & array,
                      ZZZImage<UChar> & image,
                      const int adjacency,
                      const Triple<SSize> & voxel ) {
    Triple<SSize> neighbor;
    switch ( adjacency ) {
    case ADJ26:
    case ADJ18:
      if ( image( ( neighbor = voxel + Triple<SSize>(0,0,1) ) ) == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      if ( image( ( neighbor = voxel + Triple<SSize>(0,0,-1) ) ) == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      if ( image( ( neighbor = voxel + Triple<SSize>(0,1,0) ) ) == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      if ( image( ( neighbor = voxel + Triple<SSize>(0,-1,0) ) )  == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      if ( image( ( neighbor = voxel + Triple<SSize>(1,0,0) ) ) == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      if ( image( ( neighbor = voxel + Triple<SSize>(-1,0,0) ) ) == 1 ) {
        image( neighbor ) |= 2;
        array.push_back( neighbor );
      }
      break;
    case ADJ6: {
      const Triple<SSize> *pn = NeighborsShifts26;
      const Triple<SSize> *pne = NeighborsShifts26 + 26;
      while ( pn != pne ) {
        if ( image( ( neighbor = voxel + *pn ) ) == 1 ) {
          image( neighbor ) |= 2;
          array.push_back( neighbor );
        }
        ++pn;
      }
    }
      break;
    case ADJ6P: {
      const Triple<SSize> *pn = NeighborsShifts18;
      const Triple<SSize> *pne = NeighborsShifts18 + 18;
      while ( pn != pne ) {
        if ( image( ( neighbor = voxel + *pn ) ) == 1 ) {
          image( neighbor ) |= 2;
          array.push_back( neighbor );
        }
        ++pn;
      }
    }
      break;
    }
  }
}

SkelSequentialVisitor::SkelSequentialVisitor( int iterations,
                                              const char * directionSequence,
                                              int adjacency,
                                              TerminalPointTest terminalTest,
                                              bool binarize )
  : _iterations(iterations),
    _directionSequence(directionSequence),
    _adjacency(adjacency),
    _terminalTest(terminalTest),
    _binarize(binarize),
    _removedCount(0)
{

}

void
SkelSequentialVisitor::visit( ZZZImage<Short> & )
{
  WARNING << "SkelSequentialVisitor::visit<Short> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<UShort> & )
{
  WARNING << "SkelSequentialVisitor::visit<UShort> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<Int32> & )
{
  WARNING << "SkelSequentialVisitor::visit<Int32> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<UInt32> & )
{
  WARNING << "SkelSequentialVisitor::visit<UInt32> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<Float> & )
{
  WARNING << "SkelSequentialVisitor::visit<Float> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<Double> & )
{
  WARNING << "SkelSequentialVisitor::visit<Double> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<Bool> & )
{
  WARNING << "SkelSequentialVisitor::visit<Bool> not implemented.\n";
}

void
SkelSequentialVisitor::visit( ZZZImage<UChar> & image )
{
  UChar remains[6];
  char defaultOrder[6] = { 0,1,2,3,4,5 };

  std::vector< Triple<SSize> > voxelListA;
  std::vector< Triple<SSize> > voxelListB;
  std::vector< Triple<SSize> > *border = &voxelListA;
  std::vector< Triple<SSize> > *nextBorder = &voxelListB;
  std::vector< Triple<SSize> > surfaceVoxels;

  SimplePointTest  simpleTest = SkelSimpleTests[ _adjacency ];
  int iteration=0;
  Config cnf;
  UChar orientation;
  UChar value;
  UChar mask_dir;

  if ( !_directionSequence )
    _directionSequence = defaultOrder;

  remains[5] = 0;
  for ( int n = 4; n >= 0; n--)
    remains[n] = remains[ n + 1 ] | ( 1 << _directionSequence[n + 1] );

  markBorder( image, *border, _adjacency, _binarize, -1 /* In all directions */ );

  Triple<SSize> voxel;
  _removedCount=0;
  Size removed = 1;
  Size nb_supp_direction;

  if ( !_iterations ) _iterations = -1;
  while( iteration != _iterations && removed  )  {
    ++iteration;
    removed = 0;
    for ( int dir = 0; dir < 6; ++dir ) {
      int direction = _directionSequence[ dir ];
      mask_dir = 1 << direction;
      if ( _terminalTest )
        markTerminalPoints( image, *border, surfaceVoxels, _terminalTest );
      nb_supp_direction=0;

      Triple<SSize> *pvoxel = 0;
      Triple<SSize> *pend = 0;
      if ( ! border->empty() ) {
        pvoxel = &(border->front());
        pend = &(border->back()) + 1;
      }
      while( pvoxel != pend ) {
        voxel = *pvoxel;
        orientation = image.orientation( voxel );
        value = image( voxel );
        if ( value && !(value & 4) && ( orientation & mask_dir )) {
          cnf = image.config( voxel );
          if ( simpleTest( cnf ) ) {
            nb_supp_direction++;
            image( voxel ) = 0;
            addNewBorderVoxels( *nextBorder, image, _adjacency, voxel );
          }
          else if ( !( orientation & ( remains[dir] ) ) )
            nextBorder->push_back( voxel );
        }
        ++pvoxel;
      }
      removed += nb_supp_direction;
    }
    _removedCount += removed;
    swapValues( border, nextBorder );
    nextBorder->clear();
  }
}

SkelParallelVisitor::SkelParallelVisitor( int iterations,
                                          int adjacency,
                                          TerminalPointTest terminalTest,
                                          bool binarize )
  : _iterations(iterations),
    _adjacency(adjacency),
    _terminalTest(terminalTest),
    _binarize(binarize),
    _removedCount(0)
{
}

void
SkelParallelVisitor::visit( ZZZImage<Short> & )
{
  WARNING << "SkelParallelVisitor::visit<Short> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<UShort> & )
{
  WARNING << "SkelParallelVisitor::visit<UShort> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<Int32> & )
{
  WARNING << "SkelParallelVisitor::visit<Int32> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<UInt32> & )
{
  WARNING << "SkelParallelVisitor::visit<UInt32> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<Float> & )
{
  WARNING << "SkelParallelVisitor::visit<Float> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<Double> & )
{
  WARNING << "SkelParallelVisitor::visit<Double> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<Bool> & )
{
  WARNING << "SkelParallelVisitor::visit<Bool> not implemented.\n";
}

void
SkelParallelVisitor::visit( ZZZImage<UChar> & image )
{
  std::vector< Triple<SSize> > borderA;
  std::vector< Triple<SSize> > borderB;
  std::vector< Triple<SSize> > *border = &borderA;
  std::vector< Triple<SSize> > *nextBorder = &borderB;
  std::vector< Triple<SSize> > toBeRemoved;
  std::vector< Triple<SSize> > surfacePoints;
  Config cnf,mask_p;
  PSimplePointTest pSimpleTest = SkelPSimpleTests[ _adjacency ];

  markBorder( image, *border, ADJ26 /* adjacency */, _binarize, -1 /* All directions */ );

  Triple<SSize> *pvoxel;
  Triple<SSize> *pend;

  // Bit 0 = 0/1 voxels
  // Bit 1 = border voxels
  // Bit 2 = terminal voxels
  int iteration = 0;
  Size removed = 1;
  _removedCount = 0;
  if ( !_iterations )
    _iterations = -1;
  while( iteration != _iterations && removed ) {
    ++iteration;
    if ( _terminalTest ) {
      markTerminalPoints( image, *border, surfacePoints, _terminalTest );
    }
    TRACE << "Iteration " << iteration << "..." << std::flush;
    toBeRemoved.clear();
    nextBorder->clear();
    pvoxel = pend = 0;
    if ( ! border->empty() ) {
      pvoxel = &(border->front());
      pend = 1 + &(border->back());
    }
    while( pvoxel != pend ) {
      if ( !( image(*pvoxel) & 4 ) ) {
        image.configExtended( cnf, mask_p, *pvoxel );
        if ( pSimpleTest( cnf, mask_p ) ) {
          toBeRemoved.push_back( *pvoxel );
        } else {
          nextBorder->push_back( *pvoxel );
        }
      }
      ++pvoxel;
    }
    removed = toBeRemoved.size();
    _removedCount += removed;
    pvoxel = pend = 0;
    if ( !toBeRemoved.empty() ) {
      pvoxel = &(toBeRemoved.front());
      pend = 1 + &(toBeRemoved.back());
    }
    while ( pvoxel != pend ) {
      image( *pvoxel ) = 0;
      addNewBorderVoxels( *nextBorder, image, ADJ26 /* adjacency */, *pvoxel );
      ++pvoxel;
    }
    swapValues( border, nextBorder );
    TRACE << "done.\n";
  }
}

void
markBorder( ZZZImage<UChar> & image, 
            std::vector< Triple<SSize> > & border,
            int adjacency,
            bool binarize,
            int direction )
{
  UChar ***matrix = image.data( 0 );
  Config conf;
  SSize depth = image.depth();
  SSize cols = image.width();
  SSize rows = image.height();
  TRACE << "markBorder()..." << std::flush;

  border.clear();

  if ( direction >= 0 ) {
    UChar orientation;
    UChar orientationMask = 1 << direction;
    switch ( adjacency ) {
    case ADJ26:
    case ADJ18:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              orientation = image.orientation( x, y, z );
              if ( orientation & orientationMask  ) {
                conf = image.config( x, y, z );
                if ( ( ~conf ) & MASK_6_NEIGHBORS ) {
                  matrix[ z ][ y ][ x ] |= 2;
                  border.push_back( Triple<SSize>( x, y, z ) );
                }
              }
            }
      break;
    case ADJ6:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              orientation = image.orientation( x, y, z );
              if ( orientation & orientationMask ) {
                conf = image.config( x, y, z );
                if ( ( ~conf ) & MASK_26_NEIGHBORS ) {
                  matrix[ z ][ y ][ x ] |= 2;
                  border.push_back( Triple<SSize>( x, y, z ) );
                }
              }
            }
      break;
    case ADJ6P:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              orientation = image.orientation( x, y, z );
              if ( orientation & orientationMask ) {
                conf = image.config( x, y, z );
                if ( ( ~conf ) & MASK_18_NEIGHBORS ) {
                  matrix[ z ][ y ][ x ] |= 2;
                  border.push_back( Triple<SSize>( x, y, z ) );
                }
              }
            }
      break;
    }
  } else { // direction < 0 (no specified direction)
    switch ( adjacency ) {
    case ADJ26:
    case ADJ18:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              conf = image.config( x, y, z );
              if ( ( ~conf ) & MASK_6_NEIGHBORS ) {
                matrix[ z ][ y ][ x ] |= 2;
                border.push_back( Triple<SSize>( x, y, z ) );
              }
            }
      break;
    case ADJ6:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              conf = image.config( x, y, z );
              if ( ( ~conf ) & MASK_26_NEIGHBORS ) {
                matrix[ z ][ y ][ x ] |= 2;
                border.push_back( Triple<SSize>( x, y, z ) );
              }
            }
      break;
    case ADJ6P:
      for ( SSize z = 0; z < depth; ++z )
        for ( SSize y = 0; y < rows; ++y )
          for ( SSize x = 0; x < cols; ++x )
            if ( matrix[ z ][ y ][ x ] ) {
              if ( binarize ) matrix[ z ][ y ][ x ] = 1;
              conf = image.config( x, y, z );
              if ( ( ~conf ) & MASK_18_NEIGHBORS ) {
                matrix[ z ][ y ][ x ] |= 2;
                border.push_back( Triple<SSize>( x, y, z ) );
              }
            }
      break;
    }
  }
  TRACE << "done.\n";
}

Size
skelSequential( ZZZImage<UChar> & image,
                int iterations,
                const char * directionSequence,
                int adjacency,
                TerminalPointTest terminalTest,
                bool binarize )
{
  UChar remains[6];
  char defaultOrder[6] = { 0,1,2,3,4,5 };

  std::vector< Triple<SSize> > voxelListA;
  std::vector< Triple<SSize> > voxelListB;
  std::vector< Triple<SSize> > *border = &voxelListA;
  std::vector< Triple<SSize> > *nextBorder = &voxelListB;
  std::vector< Triple<SSize> > surfaceVoxels;

  SimplePointTest  simpleTest = SkelSimpleTests[ adjacency ];
  int iteration=0;
  Config cnf;
  UChar orientation;
  UChar value;
  UChar mask_dir;

  if ( !directionSequence )
    directionSequence = defaultOrder;

  remains[5] = 0;
  for ( int n = 4; n >= 0; n--)
    remains[n] = remains[ n + 1 ] | ( 1 << directionSequence[n + 1] );

  markBorder( image, *border, adjacency, binarize, -1 /* In all directions */ );

  Triple<SSize> voxel;
  Size removedSum=0,removed=-1;
  Size nb_supp_direction;

  if ( !iterations ) iterations = -1;
  while( iteration != iterations && removed  )  {
    ++iteration;
    removed = 0;
    for ( int dir = 0; dir < 6; ++dir ) {
      int direction = directionSequence[ dir ];
      mask_dir = 1 << direction;
      if ( terminalTest )
        markTerminalPoints( image, *border, surfaceVoxels, terminalTest );
      nb_supp_direction=0;

      Triple<SSize> *pvoxel = 0;
      Triple<SSize> *pend = 0;
      if ( ! border->empty() ) {
        pvoxel = &(border->front());
        pend = &(border->back()) + 1;
      }
      while( pvoxel != pend ) {
        voxel = *pvoxel;
        orientation = image.orientation( voxel );
        value = image( voxel );
        if ( value && !(value & 4) && ( orientation & mask_dir )) {
          cnf = image.config( voxel );
          if ( simpleTest( cnf ) ) {
            nb_supp_direction++;
            image( voxel ) = 0;
            addNewBorderVoxels( *nextBorder, image, adjacency, voxel );
          }
          else if ( !( orientation & ( remains[dir] ) ) )
            nextBorder->push_back( voxel );
        }
        ++pvoxel;
      }
      removed += nb_supp_direction;
    }
    removedSum += removed;
    swapValues( border, nextBorder );
    nextBorder->clear();
  }
  return removedSum;
}

Size
skelParallel_UNOPTIMIZED( ZZZImage<UChar> & image,
                          int iterations,
                          int adjacency,
                          TerminalPointTest terminalTest,
                          bool binarize )
{
  std::vector< Triple<SSize> > border;
  std::vector< Triple<SSize> > remove;
  std::vector< Triple<SSize> > surfacePoints;
  Config cnf,mask_p;
  PSimplePointTest pSimpleTest = SkelPSimpleTests[ adjacency ];

  Triple<SSize> *pvoxel;
  Triple<SSize> *pend;
  Triple<SSize> neighbor;

  // Bit 0 = 0/1 voxels
  // Bit 1 = border voxels
  // Bit 2 = terminal voxels
  int iteration = 0;
  Size removed = -1;
  Size removedSum = 0;
  if ( !iterations )
    iterations = -1;
  while( iteration != iterations && removed ) {
    ++iteration;
    markBorder( image, border, adjacency, binarize, -1 /* All directions */ );
    if ( terminalTest ) {
      markTerminalPoints( image, border, surfacePoints, terminalTest );
    }
    TRACE << "Iteration " << iteration << "..." << std::flush;
    remove.clear();
    pvoxel = pend = 0;
    if ( ! border.empty() ) {
      pvoxel = &(border.front());
      pend = 1 + &(border.back());
    }
    while( pvoxel != pend ) {
      if ( !( image(*pvoxel) & 4 ) ) {
        image.configExtended( cnf, mask_p, *pvoxel );
        if ( pSimpleTest( cnf, mask_p ) ) {
          remove.push_back( *pvoxel );
        }
      }
      ++pvoxel;
    }
    removed = remove.size();
    removedSum += removed;
    pvoxel = pend = 0;
    if ( !remove.empty() ) {
      pvoxel = &(remove.front());
      pend = 1 + &(remove.back());
    }
    while ( pvoxel != pend ) {
      image( *pvoxel ) = 0;
      ++pvoxel;
    }
    TRACE << "done.\n";
  }
  return removedSum;
}

Size
skelParallel( ZZZImage<UChar> & image,
              int iterations,
              int adjacency,
              TerminalPointTest terminalTest,
              bool binarize )
{
  std::vector< Triple<SSize> > borderA;
  std::vector< Triple<SSize> > borderB;
  std::vector< Triple<SSize> > *border = &borderA;
  std::vector< Triple<SSize> > *nextBorder = &borderB;
  std::vector< Triple<SSize> > toBeRemoved;
  std::vector< Triple<SSize> > surfacePoints;
  Config cnf,mask_p;
  PSimplePointTest pSimpleTest = SkelPSimpleTests[ adjacency ];

  markBorder( image, *border, ADJ26 /* adjacency */, binarize, -1 /* All directions */ );

  Triple<SSize> *pvoxel;
  Triple<SSize> *pend;

  // Bit 0 = 0/1 voxels
  // Bit 1 = border voxels
  // Bit 2 = terminal voxels
  int iteration = 0;
  Size removed = -1;
  Size removedSum = 0;
  if ( !iterations )
    iterations = -1;
  while( iteration != iterations && removed ) {
    ++iteration;
    if ( terminalTest ) {
      markTerminalPoints( image, *border, surfacePoints, terminalTest );
    }
    TRACE << "Iteration " << iteration << "..." << std::flush;
    toBeRemoved.clear();
    nextBorder->clear();
    pvoxel = pend = 0;
    if ( ! border->empty() ) {
      pvoxel = &(border->front());
      pend = 1 + &(border->back());
    }
    while( pvoxel != pend ) {
      if ( !( image(*pvoxel) & 4 ) ) {
        image.configExtended( cnf, mask_p, *pvoxel );
        if ( pSimpleTest( cnf, mask_p ) ) {
          toBeRemoved.push_back( *pvoxel );
        } else {
          nextBorder->push_back( *pvoxel );
        }
      }
      ++pvoxel;
    }
    removed = toBeRemoved.size();
    removedSum += removed;
    pvoxel = pend = 0;
    if ( !toBeRemoved.empty() ) {
      pvoxel = &(toBeRemoved.front());
      pend = 1 + &(toBeRemoved.back());
    }
    while ( pvoxel != pend ) {
      image( *pvoxel ) = 0;
      addNewBorderVoxels( *nextBorder, image, ADJ26 /* adjacency */, *pvoxel );
      ++pvoxel;
    }
    swapValues( border, nextBorder );
    TRACE << "done.\n";
  }
  return removedSum;
}

Size
skelParallelDirectional( ZZZImage<UChar> & image,
                         int iterations,
                         const char * directionSequence,
                         int adjacency,
                         TerminalPointTest terminalTest,
                         bool binarize )
{
  std::vector< Triple<SSize> > borderA;
  std::vector< Triple<SSize> > borderB;
  std::vector< Triple<SSize> > *border = &borderA;
  std::vector< Triple<SSize> > *nextBorder = &borderB;
  std::vector< Triple<SSize> > remove;
  std::vector< Triple<SSize> > surfacePoints;
  Config cnf,mask_p;
  char defaultSequence[6] = { 0,1,2,3,4,5 };
  int dir,direction;
  int iteration = 0;

  PSimplePointTest simpleTest = SkelPSimpleTests[ adjacency ];

  if ( !directionSequence ) directionSequence = defaultSequence;
  if ( binarize ) image.binarize();

  Triple< SSize > *pvoxel;
  Triple< SSize > *pend;

  Size removed = 1;
  Size removedSum = 0;
  if ( !iterations ) iterations = -1;
  while( iteration != iterations && removed  ) {
    ++iteration;
    removed = 0;
    for ( dir = 0; dir<6; dir++) {
      direction = directionSequence[dir];
      markBorder( image, *border, adjacency, true, direction );
      if ( terminalTest )
        markTerminalPoints( image, *border, surfacePoints, terminalTest );
      remove.clear();
      nextBorder->clear();
      pvoxel = pend = 0;
      if ( !border->empty() ) {
        pvoxel = &(border->front());
        pend = 1 + &(border->back());
      }
      while( pvoxel != pend ) {
        if ( !( image( *pvoxel ) & 4) ) {
          image.configExtended( cnf, mask_p, *pvoxel );
          if ( simpleTest( cnf, mask_p ) )
            remove.push_back( *pvoxel );
          else
            nextBorder->push_back( *pvoxel );
        }
        ++pvoxel;
      }

      removed += remove.size();
      pvoxel = pend = 0;
      if ( !remove.empty() ) {
        pvoxel = &(remove.front());
        pend = 1 + &(remove.back());
      }
      while ( pvoxel != pend ) {
        image( *pvoxel ) = 0;
        ++pvoxel;
      }

      pvoxel = pend = 0;
      if ( !border->empty() ) {
        pvoxel = &(border->front());
        pend = 1 + &(border->back());
      }
      while ( pvoxel != pend ) {
        if ( image( *pvoxel ) == 3 ) image( *pvoxel ) = 1;
        ++pvoxel;
      }
    }
    removedSum += removed;
  }
  return removedSum;
}

Size
markTerminalPoints(ZZZImage<UChar> & image,
                   std::vector<Triple<SSize> > & border,
                   std::vector<Triple<SSize> > & surfacePoints,
                   TerminalPointTest terminalTest )
{
  int m;
  size_t pm = 0;
  char retour;
  Triple< SSize > neighbor;
  Config cnf;
  UChar value;
  Size nbp;

  std::vector< Triple<SSize> > src = surfacePoints;
  surfacePoints.clear();

  nbp = src.size();
  Triple<SSize> *pvoxel = 0;
  Triple<SSize> *pend = 0;
  if ( ! src.empty() ) {
    pvoxel = &( src.front() );
    pend = 1 + &(src.back());
  }
  while ( pvoxel != pend && nbp ) {
    nbp--;
    if ( image( *pvoxel ) ) {
      cnf = image.config( *pvoxel );
      if ( (retour = terminalTest( cnf ) ) & 2 ) {
        for ( m = 0; m < 27; m++ ) {
          neighbor = *pvoxel + ConfigShifts[m];
          value = image( neighbor );
          if ( value && !( value & 4 ) ) {
            image( neighbor ) |= ( 4 + 64 );
            surfacePoints.push_back( neighbor );
            ++pm;
          }
        }
      } else if ( retour & 1 ) {
        value = image( *pvoxel );
        if ( value && !( value & 4) ) {
          image( *pvoxel ) |= ( 4 + 64 );
          surfacePoints.push_back( *pvoxel );
          TRACE << "+" << std::flush;
          ++pm;
        }
      }
    }
    ++pvoxel;
  }

  pvoxel = pend = 0;
  if ( ! border.empty() ) {
    pvoxel = &(border.front());
    pend = 1 + &(border.back());
  }
  while( pvoxel != pend ) {
    if ( image( *pvoxel ) ) {
      cnf = image.config( *pvoxel );
      if ( ( retour = terminalTest( cnf ) ) & 2) {
        for ( m = 0; m < 27; m++ ) {
          neighbor = *pvoxel + ConfigShifts[ m ];
          value = image( neighbor );
          if ( value && !(value & 4) ) {
            image( neighbor ) |= ( 4 + 64 );
            surfacePoints.push_back( neighbor );
            ++pm;
          }
        }
      } else if ( retour & 1 ) {
        value = image( *pvoxel );
        if ( value && !(value & 4) ) {
          image( *pvoxel ) |= ( 4 + 64 );
          surfacePoints.push_back( *pvoxel );
          ++pm;
        }
      }
    }
    ++pvoxel;
  }

  TRACE << pm << " surface voxels found.\n";
  return pm;
}
