/**
 * @file   tools3d.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:18 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "tools3d.h"

#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <sstream>
#include "Triple.h"
#include "RefTriple.h"
#include "tools.h"
#include "converter.h"
#include "draw_line_visitor.h"

using std::vector;
using std::ifstream;
using std::istringstream;
using std::string;

ZZZImage<Int32>
filterCascade( const ZZZImage<UChar> & image, int finalSize ) {
  int n = std::max( static_cast<int>( image.width() ),
                    static_cast<int>( image.height() ) );
  int size = 2;
  while ( size < n ) size <<= 1;

  ZZZImage<UChar> charImage( size, size );
  charImage.insert( image, 0, 0 );

  SSize w = image.width();
  SSize h = image.height();
  if ( w % 2 ) {
    for ( int row = 0 ; row < image.height(); ++row )
      charImage( w, row ) = charImage( w - 1, row );
    ++w;
  }
  if ( h % 2 ) {
    for ( int col = 0 ; col < w; ++col )
      charImage( col, h  ) = charImage( col, h - 1 );
    ++h;
  }

  ZZZImage<Int32> result( size, size );
  result = charImage;

  while ( size > finalSize ) {

    ZZZImage<Int32> original;
    result.extract( original, 0, 0, 0, size, size, 1 );

    ZZZImage<Int32> filteredH( size / 2, size );
    ZZZImage<Int32> filteredHV( size / 2, size / 2 );

    /* Low-Pass Horizontal */
    for ( SSize row = 0; row < size; row++)
      for ( SSize col = 0; col < size; col += 2 )
        filteredH( col / 2, row ) = ( static_cast<int>( original( col, row ) )
                                      + original( col + 1, row ) ) / 2;

    /* Low-Pass Vertical */
    for ( SSize col = 0; col < size / 2; ++col )
      for ( SSize row = 0; row < size; row += 2 )
        filteredHV( col, row / 2 ) = ( filteredH( col, row ) + filteredH( col, row + 1  ) ) / 2;

    result.insert( filteredHV, 0, 0 ); // LL**
    // ****

    /* High-Pass Vertical */
    for ( SSize col = 0; col < size / 2; ++col )
      for ( SSize row = 0; row < size; row += 2 )
        filteredHV( col, row / 2 ) = abs( filteredH( col, row + 1  ) - filteredH( col, row ) ) / 2;

    result.insert( filteredHV, size / 2, 0 ); // **LH
    // ****

    /* High-Pass Horizontal */
    for ( SSize row = 0; row < size; row++)
      for ( SSize col = 0; col < size; col += 2 )
        filteredH( col / 2, row ) = abs( static_cast<int>( original( col + 1, row  ) )
                                         - original( col, row ) ) / 2;

    /* Low-Pass Vertical */
    for ( SSize col = 0; col < size / 2; ++col )
      for ( SSize row = 0; row < size; row += 2 )
        filteredHV( col, row / 2 ) = ( filteredH( col, row ) + filteredH( col, row + 1  ) ) / 2;

    result.insert( filteredHV, 0, size / 2 ); // ****
    // HL**

    /* High-Pass Vertical */
    for ( SSize col = 0; col < size / 2; ++col )
      for ( SSize row = 0; row < size; row += 2 )
        filteredHV( col, row / 2 ) = abs( filteredH( col, row + 1  ) - filteredH( col, row ) ) / 2;

    result.insert( filteredHV, size / 2, size / 2 ); // ****
    // **HH
    size >>= 1;
  }
  result += 1;
  return result;
}



template < typename T >
void zzzDrawPlane( ZZZImage<T> & image,
                   float a, float b, float c, float t,
                   T value )
{
  SSize x, y, z;
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  float ac, bc, tc;
  for ( int band = 0; band < bands; band++ ) {
    if ( c != 0.0 ) {
      ac = a / -c;  bc = b / -c ; tc = t / c;
      for ( x = 0; x < cols; x++ )
        for ( y = 0; y < rows; y++ ) {
          z = ( SSize ) ( ac * x + bc * y + tc );
          if ( z && z < depth - 1 )
            image(x,y,z,band) = value;
        }
    }
    if ( b != 0.0 ) {
      ac = a / ( -b ); bc = c / ( -b ); tc = t / b;
      for ( x = 0; x < cols ; x++ )
        for ( z = 0; z < depth ; z++ ) {
          y = (SSize) ( ac * x + bc * z + tc );
          if ( y && y < rows - 1 )
            image(x,y,z,band) = value;
        }
    }
    if ( a != 0.0 ) {
      ac = b / -a;  bc = c / -a; tc = t / a;
      for ( y = 0; y < rows; y++ )
        for ( z = 0; z < cols; z++ ) {
          x = ( SSize ) ( ac * y + bc * z + tc );
          if ( x && x < cols - 1 )
            image(x,y,z,band) = value;
        }
    }
  }
}

template < typename T >
void
zzzRotate( ZZZImage<T> & result,
           const ZZZImage<T> & image,
           int axis, int quater)
{
  const SSize cols = image.width();
  const SSize rows = image.height();
  const SSize depth = image.depth();
  const SSize bands = image.bands();
  SSize x, y, z;

  switch ( axis ) {
  case 0:
    switch ( quater ) {
    case 1: result.alloc( cols, depth, rows, bands ); break;
    case 2: result.alloc( cols, rows, depth, bands ); break;
    case 3: result.alloc( cols, depth, rows, bands ); break;
    }
  case 1:
    switch ( quater ) {
    case 1: result.alloc( depth, rows, cols , bands ); break;
    case 2: result.alloc( cols, rows, depth , bands ); break;
    case 3: result.alloc( depth, rows, cols, bands ); break;
    }
  case 2:
    switch ( quater ) {
    case 1: result.alloc( rows, cols, depth , bands ); break;
    case 2: result.alloc( cols, rows, depth , bands ); break;
    case 3: result.alloc( rows, cols, depth, bands ); break;
    }
  }
  result.clear();

  for ( SSize band = 0 ; band < bands; band++ ) {
    switch ( axis ) {
    case 0:
      switch ( quater ) {
      case 1:
        for ( z = 0 ; z < depth ; z++ )
          for ( x = 0 ; x < cols ; x++ )
            for ( y = 0 ; y < rows ; y++ )
              result(x,z,rows - y - 1,band) = image(x,y,z,band);
        break;
      case 2:
        for ( z = 0; z < depth; z++ )
          for ( x = 0; x < cols; x++ )
            for ( y = 0; y < rows; y++ )
              result(x,rows-y-1,depth-z-1,band) = image(x,y,z,band);
        break;
      case 3:
        for ( z = 0; z < depth; z++ )
          for ( x = 0; x < cols; x++ )
            for ( y = 0; y < rows; y++ )
              result(x,depth-z-1,rows-y-1,band) = image(x,y,z,band);
        break;
      }
      break;
    case 1:
      switch ( quater ) {
      case 1:
        for ( z = 0; z < depth; z++ )
          for ( x = 0; x < cols; x++ )
            for ( y = 0; y < rows; y++ )
              result(depth-z-1,y,x,band) = image(x,y,z,band);
        break;
      case 2:
        for ( z = 0; z < depth; z++ )
          for ( x = 0; x < cols; x++ )
            for ( y = 0; y < rows; y++ )
              result(depth-z-1,y,cols-x-1,band) = image(x,y,z,band);
        break;
      case 3:
        for ( z = 0; z < depth; z++ )
          for ( x = 0; x < cols; x++ )
            for ( y = 0; y < rows; y++ )
              result(x,y,cols-x-1,band) = image(x,y,z,band);
        break;
      }
      break;
    case 2:
      switch ( quater ) {
      case 1:
        for ( z = 0; z < depth; z++ )
          for ( y = 0; y < rows; y++ )
            for ( x = 0; x < cols; x++ )
              result(y,cols-x-1,z,band) = image(x,y,z,band);
        break;
      case 2:
        for ( z = 0; z < depth; z++ )
          for ( y = 0; y < rows; y++ )
            for ( x = 0; x < cols; x++ )
              result(cols-x-1,rows-y-1,z,band) = image(x,y,z,band);
        break;
      case 3:
        for ( z = 0; z < depth; z++ )
          for ( y = 0; y < rows; y++ )
            for ( x = 0; x < cols; x++ )
              result(rows-y-1,x,z,band) = image(x,y,z,band);
        break;
      }
      break;
    }
  }
}


template < typename T >
void
zzzRotate( ZZZImage<T> & result,
           const ZZZImage<T> & image,
           float rho, float theta, float phi )
{
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  SSize x, y, z;

  double xc, yc, zc;
  double cos_phi, cos_theta, cos_rho, sin_phi, sin_theta, sin_rho;
  SSize i, j, k;
  double dx, dy, dz;


  cos_theta = cos( theta );
  sin_theta = sin( theta );
  cos_phi = cos( phi );
  sin_phi = sin( phi );
  cos_rho = cos( rho );
  sin_rho = sin( rho );

  if ( result.bands() != bands || image.size() != result.size() )
    result.alloc( cols, rows, depth, bands );

  for ( SSize band = 0 ; band < bands; band++ ) {
    T v;
    result.clear();
    for ( k = 0; k < depth; k++ )
      for ( j = 0; j < rows; j++ )
        for ( i = 0; i < cols; i++ ) {
          dx = i - cols * 0.5;
          dy = j - rows * 0.5;
          dz = k - depth * 0.5;
          xc = dx * cos_theta * cos_phi - ( dy * sin_phi * cos_theta )
              + dz * sin_theta;
          yc = dx * ( cos_rho * sin_phi + sin_rho * cos_phi * sin_theta )
              + dy * ( cos_rho * cos_phi -  sin_rho * sin_phi * sin_theta )
              - dz * sin_rho * cos_theta;
          zc = dx * ( sin_rho * sin_phi - cos_rho * cos_phi * sin_theta )
              + dy * ( cos_phi * sin_rho + cos_rho * sin_phi * sin_theta )
              + dz * ( cos_theta * cos_phi );
          xc += cols * 0.5;
          yc += rows * 0.5;
          zc += depth * 0.5;
          if ( xc >= 0 && yc >= 0 && zc >= 0 ) {
            x = static_cast<SSize>( xc );
            y = static_cast<SSize>( yc );
            z = static_cast<SSize>( zc );
            if ( x < cols && y < rows &&  z < depth  && ( v = image(x,y,z,band) ) )
              result(i,j,k,band) = v;
          }
        }
  }
}

template < typename T >
void
zzzRotate( ZZZImage<T> & result,
           const ZZZImage<T> & image,
           int axis, float rho,
           SSize xcenter, SSize ycenter, SSize zcenter )
{
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  SSize x, y, z;

  if ( result.bands() != bands || image.size() != result.size() )
    result.alloc( cols, rows, depth, bands );
  else
    result.clear();

  double xc, yc, zc;
  SSize i, j, k;
  double dx, dy, dz;
  T v;

  double cos_rho, sin_rho;
  cos_rho = cos( rho );
  sin_rho = sin( rho );

  for ( SSize band = 0; band < bands; band++ ) {
    switch ( axis ) {
    case 2:
      for ( k = 0; k < depth; k++ )
        for ( j = 0; j < rows; j++ )
          for ( i = 0; i < cols; i++ ) {
            dx = i - xcenter;
            dy = j - ycenter;
            dz = k - zcenter;
            xc = dx * cos_rho - dy * sin_rho;
            yc = dx * sin_rho + dy * cos_rho;
            zc = dz;
            x = xc + xcenter;
            y = yc + ycenter;
            z = zc + zcenter;
            if ( x >= 0 && y >= 0 && z >= 0
                 && x < cols && y < rows && z < depth
                 && ( v = image(x,y,z,band) ) )
              result(i,j,k,band) = v;
          }
      break;
    case 1:
      for ( k = 0; k < depth; k++ )
        for ( j = 0; j < rows; j++ )
          for ( i = 0; i < cols; i++ ) {
            dx = i - xcenter; dy = j - ycenter; dz = k - zcenter;
            xc = dx * cos_rho + dz * sin_rho;
            yc = dy;
            zc = dz * cos_rho - dx * sin_rho;
            x = xc + xcenter;
            y = yc + ycenter;
            z = zc + zcenter;
            if ( x >= 0 && y >= 0 && z >= 0
                 && x < cols && y < rows && z < depth
                 && ( v = image(x,y,z,band) ) )
              result(i,j,k,band) = v;
          }
      break;
    case 0:
      for ( k = 0; k < depth; k++ )
        for ( j = 0; j < rows; j++ )
          for ( i = 0; i < cols; i++ ) {
            dx = i - xcenter; dy = j - ycenter; dz = k - zcenter;
            xc = dx;
            yc = dy * cos_rho - dz * sin_rho;
            zc = dy * sin_rho + dz * cos_rho;
            x = xc + xcenter;
            y = yc + ycenter;
            z = zc + zcenter;
            if ( x >= 0 && y >= 0 && z >= 0
                 && x < cols && y < rows && z < depth
                 && ( v = image(x,y,z,band) ) )
              result(i,j,k,band) = v;
          }
      break;
    }
  }
}

template< typename T >
bool
zzzInsert( ZZZImage<T> & result,
           const ZZZImage<T> & image,
           SSize dx, SSize dy, SSize dz )
{
  SSize atx, aty, atz;
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  SSize x, y, z;

  atx = image.width();
  aty = image.height();
  atz = image.depth();

  if ( atx + dx >= cols ) atx = cols - dx;
  if ( aty + dy >= rows ) aty = rows - dy;
  if ( atz + dz >= depth ) atz = depth - dz;
  for ( int band = 0; band < bands; band++ ) {
    for ( x = 0; x < atx; x++ )
      for ( y = 0; y < aty; y++ )
        for ( z = 0; z < atz; z++ ) {
          if ( !( result(x+dx,y+dy,z+dz,band) ) )
            result(x+dx,y+dy,z+dz,band) = image(x,y,z,band);
        }
  }
  return true;
}

template < typename T >
void
zzzRange( ZZZImage<T> & image, T & min, T & max )
{
  min = std::numeric_limits<T>::max();
  max = std::numeric_limits<T>::min();
  const SSize bands = image.bands();
  const Size end = image.realBandVectorSize();
  T value;
  for ( int band = 0; band < bands; band++ ) {
    Size index = 0;
    while ( index != end ) {
      if ( ( value = image(index,0,0,band) )) {
        if ( value > max ) max = value;
        if ( value < min ) min = value;
      }
      ++index;
    }
  }
}

template < typename T >
Size zzzReplace( ZZZImage<T> & image, T before, T after )
{
  Size res = 0;
  const SSize bands = image.bands();
  const Size limit = image.realBandVectorSize();
  for ( SSize band = 0; band < bands; band++ ) {
    Size index = 0;
    while ( index != limit ) {
      if ( image(index,0,0,band) == before ) {
        image(index,0,0,band) = after;
        ++res;
      }
      ++index;
    }
  }
  return res;
}

template < typename T >
void
zzzBoundingBox( ZZZImage<T> & image,
                SSize & x_min, SSize & y_min, SSize & z_min,
                SSize & x_max, SSize & y_max, SSize & z_max) {
  SSize x, y, z;
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  x_min = y_min = z_min = std::numeric_limits<SSize>::max();
  x_max = y_max = z_max = 0;
  for ( SSize band = 0; band < bands; band++ ) {
    for ( x = 0; x < cols; x++ )
      for ( y = 0; y < rows; y++ )
        for ( z = 0; z < depth; z++ ) {
          if ( image(x,y,z) ) {
            if ( x > x_max ) x_max = x;
            if ( x < x_min ) x_min = x;
            if ( y > y_max ) y_max = y;
            if ( y < y_min ) y_min = y;
            if ( z > z_max ) z_max = z;
            if ( z < z_min ) z_min = z;
          }
        }
  }
}

template < typename T >
Size
zzzTreshold( ZZZImage<T> & image, T min, T max, T value )
{
  const SSize bands = image.bands();
  const Size limit = image.realBandVectorSize();
  Size count = 0;
  for ( SSize band = 0; band < bands; band++ ) {
    Size index = 0;
    if ( value ) {
      while ( index != limit ) {
        if ( image(index,0,0,band) < min
             || image(index,0,0,band) > max )
          image(index,0,0,band) = 0;
        else {
          image(index,0,0,band) = value;
          ++count;
        }
        ++index;
      }
    } else {
      while ( index != limit ) {
        if ( image(index,0,0,band) < min
             || image(index,0,0,band) > max )
          image(index,0,0,band) = 0;
        else ++count;
        ++index;
      }
    }
  }
  return count;
}

template < typename T >
void
zzzDilate6( ZZZImage<T> & image )
{
  ZZZImage<T> copy(image);
  image.clear();
  SSize x, y, z;
  SSize cols = image.width();
  SSize rows = image.height();
  SSize depth = image.depth();
  SSize bands = image.bands();
  for ( SSize band = 0; band < bands; band++ ) {
    T value;
    for ( z = 0; z < depth; ++z )
      for ( y = 0; y < rows; ++y )
        for ( x = 0; x < cols; ++x ) {
          value = copy(x,y,z,band);
          if ( value ) {
            image(x,y,z,band) = value;
            image(x+1,y,z,band) = value;
            image(x-1,y,z,band) = value;
            image(x,y+1,z,band) = value;
            image(x,y-1,z,band) = value;
            image(x,y,z+1,band) = value;
            image(x,y,z-1,band) = value;
          }
        }
  }
}

template < typename T >
ZZZImage<T>
zzzSponge( const ZZZImage<T> & image )
{
  const Triple< SSize > shifts[20] = {
    Triple<SSize>(-1,-1,-1),Triple<SSize>(-1,-1,0),Triple<SSize>(-1,-1,1),
    Triple<SSize>(-1,0,-1),                        Triple<SSize>(-1,0,1),
    Triple<SSize>(-1,1,-1),Triple<SSize>(-1,1,0),Triple<SSize>(-1,1,1),
    Triple<SSize>(0,-1,-1),                     Triple<SSize>(0,-1,1),

    Triple<SSize>(0,1,-1),                       Triple<SSize>(0,1,1),
    Triple<SSize>(1,-1,-1),Triple<SSize>(1,-1,0),Triple<SSize>(1,-1,1),
    Triple<SSize>(1,0,-1),                     Triple<SSize>(1,0,1),
    Triple<SSize>(1,1,-1),Triple<SSize>(1,1,0),Triple<SSize>(1,1,1)
  };

  const Triple<SSize> one(1,1,1);

  const SSize width=image.width();
  const SSize height=image.height();
  const SSize depth=image.depth();
  const SSize bands=image.bands();
  ZZZImage<T> result( width*3, height*3, depth*3, bands );
  T value;
  Triple<SSize> voxel;
  for ( SSize b = 0; b < bands; ++b ) {
    for ( voxel.third = 0; voxel.third < depth; ++voxel.third )
      for ( voxel.second = 0; voxel.second < height; ++voxel.second )
        for ( voxel.first = 0; voxel.first < width; ++voxel.first ) {
          value = image(voxel,b);
          if ( value ) {
            Triple<SSize> center = voxel * static_cast<SSize>(3) + one;
            for ( int i = 0; i < 20; ++ i )
              result(center+shifts[i],b) = value;
          }
        }
  }
  return result;
}

template < typename T >
void
zzzDilate18( ZZZImage<T> & image )
{
  SSize x, y, z;
  SSize i, j, k;
  const SSize width=image.width();
  const SSize height=image.height();
  const SSize depth=image.depth();
  const SSize bands=image.bands();
  ZZZImage<T> copy( image );
  image.clear();
  const int * EighteenNeighbors = The18NeighborsLists[13];

  for ( SSize band = 0; band < bands; band++ ) {
    T value;
    for ( z = 0; z < depth; z++ )
      for ( y = 0; y < height; y++ )
        for ( x = 0; x < width; x++ ) {
          value = image(x,y,z,band);
          if ( value ) {
            image(x,y,z,band) = value;
            for ( int dir = 0; dir < 18; dir++ ) {
              i = x + IndexToDeltaX[EighteenNeighbors[dir]];
              j = y + IndexToDeltaY[EighteenNeighbors[dir]];
              k = z + IndexToDeltaZ[EighteenNeighbors[dir]];
              image(i,j,k,band) = value;
            }
          }
        }
  }
}

template < typename T >
void
zzzDilate26( ZZZImage<T> & image )
{
  SSize x, y, z;
  SSize i, j, k;
  const SSize width=image.width();
  const SSize height=image.height();
  const SSize depth=image.depth();
  const SSize bands=image.bands();
  ZZZImage<T> copy( image );
  image.clear();

  for ( SSize band = 0; band < bands; band++ ) {
    T value;
    for ( z = 0; z < depth; z++ )
      for ( y = 0; y < height; y++ )
        for ( x = 0; x < width; x++ ) {
          value = image(x,y,z,band);
          if ( value ) {
            for ( int dir = 0; dir < 27; dir++ ) {
              i = x + IndexToDeltaX[dir];
              j = y + IndexToDeltaY[dir];
              k = z + IndexToDeltaZ[dir];
              image(i,j,k,band) = value;
            }
          }
        }
  }
}

template < typename T >
void zzzAddNoise6( ZZZImage<T> & image, float ratio )
{
  SSize x, y, z;
  const SSize cols = image.width();
  const SSize rows = image.height();
  const SSize depth = image.depth();
  Size nbVoxels = 0;
  vector< Triple<SSize> > simples;

  image.binarize( 1 );
  for ( z = 0; z < depth; z++ )
    for ( y = 0; y < rows; y++ )
      for ( x = 0; x < cols; x++ )
        if ( cnfSimple6( image.config( x, y, z ) ) )
          simples.push_back( Triple<SSize>( x, y, z ) );

  nbVoxels = simples.size();

  Size limit = static_cast<Size>( ratio * nbVoxels );
  vector< Triple<SSize> >::iterator it = simples.begin();
  vector< Triple<SSize> >::iterator end = simples.end();
  srand(time(0));
  while ( it != end ) {
    x = it->first;
    y = it->second;
    z = it->third;
    if ( cnfSimple6( image.config( x, y, z ) ) ) {
      Size v =  1 + static_cast<int>(nbVoxels * (rand() / (RAND_MAX + 1.0)));
      if ( v <= limit )
        image(x,y,z) = 1 - image(x,y,z);
    }
    ++it;
  }
}

template < typename T >
void zzzAddNoise18( ZZZImage<T> & image, float ratio )
{
  SSize x, y, z;
  const SSize cols = image.width();
  const SSize rows = image.height();
  const SSize depth = image.depth();
  Size nbVoxels = 0;
  vector< Triple<SSize> > simples;

  image.binarize( 1 );
  for ( z = 0; z < depth; z++ )
    for ( y = 0; y < rows; y++ )
      for ( x = 0; x < cols; x++ )
        if ( cnfSimple18( image.config( x, y, z ) ) )
          simples.push_back( Triple<SSize>( x, y, z ) );

  nbVoxels = simples.size();

  Size limit = static_cast<Size>( ratio * nbVoxels );
  vector< Triple<SSize> >::iterator it = simples.begin();
  vector< Triple<SSize> >::iterator end = simples.end();
  srand(time(0));
  while ( it != end ) {
    x = it->first;
    y = it->second;
    z = it->third;
    if ( cnfSimple18( image.config( x, y, z ) ) ) {
      Size v =  1 + static_cast<int>(nbVoxels * (rand() / (RAND_MAX + 1.0)));
      if ( v <= limit )
        image(x,y,z) = 1 - image(x,y,z);
    }
    ++it;
  }
}

template < typename T >
void
zzzClearBorder( ZZZImage<T> & image )
{
  SSize x, y, z;
  const SSize cols = image.width();
  const SSize rows = image.height();
  const SSize depth = image.depth();
  const SSize bands = image.bands();
  for ( SSize band = 0; band < bands; band++ ) {
    for ( y = 0; y < rows; y++ )
      for ( z = 0; z < depth; z++ ) {
        image(0,y,z,band) = 0;
        image(cols-1,y,z,band) = 0;
      }
    for ( x = 0; x < cols; x++ )
      for ( z = 0; z < depth; z++ ) {
        image(x,0,z,band) = 0;
        image(x,rows-1,z,band) = 0;
      }
    for ( x = 0; x < cols; x++ )
      for ( y = 0; y < rows; y++ ) {
        image(x,y,0,band) = 0;
        image(x,y,depth-1,band) = 0;
      }
  }
}

template< typename T >
void
threshold( ZZZImage<UChar> & dst,
           ZZZImage<T> & src,
           Triple<T> min,
           Triple<T> max,
           std::vector< Triple<T> > & removedValues )
{
  SSize width, height, depth, bands;
  src.getDimension( width, height, depth, bands );

  typename std::vector< Triple<T> >::iterator it;
  if ( bands == 1 ) {
    for ( SSize z = 0 ; z < depth; z++ )
      for ( SSize y = 0 ; y < height; y++ )
        for ( SSize x = 0 ; x < width; x++ ) {
          T value = src(x,y,z);
          it = std::find( removedValues.begin(),
                          removedValues.end(),
                          Triple<T>( value, value, value ) );
          if ( it != removedValues.end() ||
               value < min.first ||
               value > max.first )
            dst(x,y,z) = 0;
        }
    return;
  }
  if ( bands == 3 ) {
    for ( SSize z = 0 ; z < depth; z++ )
      for ( SSize y = 0 ; y < height; y++ )
        for ( SSize x = 0 ; x < width; x++ ) {
          T value0 = src(x,y,z,0);
          T value1 = src(x,y,z,1);
          T value2 = src(x,y,z,2);
          it = std::find( removedValues.begin(),
                          removedValues.end(),
                          Triple<T>( value0, value1, value2 ) );
          if ( it != removedValues.end()
               || value0 < min.first || value0 > max.first
               || value1 < min.second || value1 > max.second
               || value2 < min.third || value2 > max.third ) {
            dst(x,y,z,0) = dst(x,y,z,1) = dst(x,y,z,2) = 0;
          }
        }
    return;
  }
  ERROR << "threshold() called with bands no int {1,3}.\n";
}

template < typename T >
void
fillTriangle( ZZZImage<T> & image,
              const Triple<SSize> & p1,
              const Triple<SSize> & p2,
              const Triple<SSize> & p3,
              Triple<UChar> color )
{
  /* Determine the supporting line */
  Triple<SSize> pixel, stop, c;
  double m1 = abs( p2 - p1 );
  double m2 = abs( p3 - p2 );
  double m3 = abs( p1 - p3 );
  if ( m1 <= m2 && m1 <= m3 ) {
    pixel = p1;
    stop = p2;
    c = p3;
  } else if ( m2 <= m1 && m2 <= m3 ) {
    pixel = p2;
    stop = p3;
    c = p1;
  } else {
    pixel = p1;
    stop = p3;
    c = p2;
  }

  Triple<SSize> delta = stop - pixel;
  SSize x_inc, y_inc, z_inc;
  x_inc = (delta.first < 0) ? -1 : 1;
  y_inc = (delta.second < 0) ? -1 : 1;
  z_inc = (delta.third < 0) ? -1 : 1;

  SSize width, height, depth;
  width = abs( delta.first );
  height = abs( delta.second );
  depth = abs( delta.third );
  Triple<SSize> delta2( width<<1, height<<1, depth<<1 );

  SSize err_1, err_2;
  SSize i;

  if ( (width >= height) && (width >= depth) ) {
    err_1 = delta2.second - width;
    err_2 = delta2.third - width;
    for ( i = 0; i < width; ++i ) {
      DrawLineVisitor drawLineVisitor(pixel,c,color);
      image.accept( drawLineVisitor );
      if ( err_1 > 0 ) {
        pixel.second += y_inc;
        err_1 -= delta2.first;
      }
      if ( err_2 > 0 ) {
        pixel.third += z_inc;
        err_2 -= delta2.first;
      }
      err_1 += delta2.second;
      err_2 += delta2.third;
      pixel.first += x_inc;
    }
  } else if ( (height >= width) && (height >= depth) ) {
    err_1 = delta2.first - height;
    err_2 = delta2.third - height;
    for (i = 0; i < height; ++i ) {
      DrawLineVisitor drawLineVisitor(pixel,c,color);
      image.accept( drawLineVisitor );
      if ( err_1 > 0 ) {
        pixel.first += x_inc;
        err_1 -= delta2.second;
      }
      if ( err_2 > 0 ) {
        pixel.third += z_inc;
        err_2 -= delta2.second;
      }
      err_1 += delta2.first;
      err_2 += delta2.third;
      pixel.second += y_inc;
    }
  } else {
    err_1 = delta2.second - depth;
    err_2 = delta2.first - depth;
    for ( i = 0; i < depth; ++i ) {
      DrawLineVisitor drawLineVisitor(pixel,c,color);
      image.accept( drawLineVisitor );
      if ( err_1 > 0 ) {
        pixel.second += y_inc;
        err_1 -= delta2.third;
      }
      if ( err_2 > 0 ) {
        pixel.first += x_inc;
        err_2 -= delta2.third;
      }
      err_1 += delta2.second;
      err_2 += delta2.first;
      pixel.third += z_inc;
    }
  }
  DrawLineVisitor drawLineVisitor(pixel,c,color);
  image.accept( drawLineVisitor );
}


#define INSTANCIATE( T ) \
  template void zzzDrawPlane( ZZZImage< T > & image, float a, float b, float c,\
  float t, T value );\
  template void zzzRotate( ZZZImage< T > & result, const ZZZImage<T> & image,\
  int axis, int quater);\
  template void zzzRotate( ZZZImage< T > & result, const ZZZImage<T> & image,\
  float rho, float theta, float phi );\
  template void zzzRotate( ZZZImage< T > & result, const ZZZImage<T> & image,\
  int axis, float rho,\
  SSize xcenter, SSize ycenter, SSize zcenter);\
  template bool zzzInsert( ZZZImage< T > & result, const ZZZImage<T> & image,\
  SSize dx, SSize dy, SSize dz );\
  template void zzzRange( ZZZImage< T > & image, T & min, T & max );\
  template Size zzzReplace( ZZZImage< T > & image, T before, T after );\
  template void zzzBoundingBox( ZZZImage< T > & image, \
  SSize & x_min, SSize & y_min, SSize & z_min,\
  SSize & x_max, SSize & y_max, SSize & z_max);\
  template Size zzzTreshold( ZZZImage< T > & image, T min, T max, T value );\
  template void zzzDilate6( ZZZImage< T > & image );\
  template void zzzDilate18( ZZZImage< T > & image );\
  template void zzzDilate26( ZZZImage< T > & image );\
  template void zzzClearBorder( ZZZImage< T > & image );\
  template void zzzAddNoise6( ZZZImage< T > & image, float ratio );\
  template void zzzAddNoise18( ZZZImage< T > & image, float ratio );\
  template void threshold( ZZZImage< UChar > & dst, \
  ZZZImage< T > & src,\
  Triple< T > min,\
  Triple< T > max,\
  std::vector< Triple< T > > & removedValues );\
  template ZZZImage< T > zzzSponge( const ZZZImage< T > & image );\
  template void fillTriangle( ZZZImage<T> & image,\
  const Triple<SSize> & p1, const Triple<SSize> & p2, const Triple<SSize> & p3,\
  Triple<UChar> color );

INSTANCIATE( Bool )
INSTANCIATE( UChar )
INSTANCIATE( Short )
INSTANCIATE( Int32 )
INSTANCIATE( UInt32 )
INSTANCIATE( Float )
INSTANCIATE( Double )
INSTANCIATE( UShort )


