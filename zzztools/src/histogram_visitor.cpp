/**
 * @file   histogram_visitor.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:22 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "histogram_visitor.h"

HistogramVisitor::HistogramVisitor(ZZZImage<UChar> & histogram)
  :_histogram( histogram )
{
}

void
HistogramVisitor::visit( ZZZImage<Short> & )
{
  WARNING << "HistogramVisitor::visit<Short> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<UShort> & )
{
  WARNING << "HistogramVisitor::visit<UShort> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<Int32> & )
{
  WARNING << "HistogramVisitor::visit<Int32> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<UInt32> & )
{
  WARNING << "HistogramVisitor::visit<UInt32> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<Float> & )
{
  WARNING << "HistogramVisitor::visit<Float> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<Double> & )
{
  WARNING << "HistogramVisitor::visit<Double> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<Bool> & )
{
  WARNING << "HistogramVisitor::visit<Bool> not implemented.\n";
}

void
HistogramVisitor::visit( ZZZImage<UChar> & image )
{
  _histogram.alloc(258,258,258,3);
  UChar *** histo0 = _histogram.data(0);
  UChar *** histo1 = _histogram.data(1);
  UChar *** histo2 = _histogram.data(2);
  SSize depth = image.depth();
  SSize width = image.width();
  SSize height = image.height();
  if ( image.bands() == 3 ) {
    UChar red, green, blue;
    UChar ***image0 = image.data(0);
    UChar ***image1 = image.data(1);
    UChar ***image2 = image.data(2);
    for ( SSize z = 0; z < depth; z++ ) {
      for ( SSize y = 0; y < height; y++ )
        for ( SSize x = 0; x < width; x++ ) {
          red = image0[z][y][x];
          green = image1[z][y][x];
          blue = image2[z][y][x];
          histo0[blue + 1l][green + 1l][red + 1l] = red;
          histo1[blue + 1l][green + 1l][red + 1l] = green;
          histo2[blue + 1l][green + 1l][red + 1l] = blue;
        }
    }
  } else {
    UChar ***image0 = image.data(0);
    UChar gray;
    for ( SSize z = 0; z < depth; z++ ) {
      for ( SSize y = 0; y < height; y++ )
        for ( SSize x = 0; x < width; x++ ) {
          gray = image0[z][y][x];
          histo0[gray + 1l][gray + 1l][gray + 1l] = gray;
          histo1[gray + 1l][gray + 1l][gray + 1l] = gray;
          histo2[gray + 1l][gray + 1l][gray + 1l] = gray;
        }
    }
  }

  for ( SSize x = 1;  x < 257; x++ ) {
    histo0[0][0][x] = x - 1;
    histo1[0][0][x] = 0;
    histo2[0][0][x] = 0;

    histo0[0][257][x] = x - 1;
    histo1[0][257][x] = 255;
    histo2[0][257][x] = 0;

    histo0[257][0][x] = x - 1;
    histo1[257][0][x] = 0;
    histo2[257][0][x] = 255;

    histo0[257][257][x] = x - 1;
    histo1[257][257][x] = 255;
    histo2[257][257][x] = 255;

    histo0[0][x][0] = 0;
    histo1[0][x][0] = x - 1;
    histo2[0][x][0] = 0;

    histo0[0][x][257] = 255;
    histo1[0][x][257] = x - 1;
    histo2[0][x][257] = 0;

    histo0[257][x][0] = 0;
    histo1[257][x][0] = x - 1;
    histo2[257][x][0] = 255;

    histo0[257][x][257] = 255;
    histo1[257][x][257] = x - 1;
    histo2[257][x][257] = 255;

    histo0[x][0][0] = 0;
    histo1[x][0][0] = 0;
    histo2[x][0][0] = x - 1;

    histo0[x][0][257] = 255;
    histo1[x][0][257] = 0;
    histo2[x][0][257] = x - 1;

    histo0[x][257][0] = 0;
    histo1[x][257][0] = 255;
    histo2[x][257][0] = x - 1;

    histo0[x][257][257] = 255;
    histo1[x][257][257] = 255;
    histo2[x][257][257] = x - 1;
  }
}
