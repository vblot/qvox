/**
 * @file   draw_line_visitor.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:22 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "draw_line_visitor.h"

DrawLineVisitor::DrawLineVisitor( SSize x1, SSize y1, SSize z1,
                                  SSize x2, SSize y2, SSize z2,
                                  UChar red, UChar green, UChar blue )
  : _x1(x1),_y1(y1),_z1(z1),_x2(x2),_y2(y2),_z2(z2), _red(red),_green(green),_blue(blue)
{

}

DrawLineVisitor::DrawLineVisitor( Triple<SSize> pixel1, Triple<SSize> pixel2, Triple<UChar> color )
  : _x1(pixel1.first),_y1(pixel1.second),_z1(pixel1.third),
    _x2(pixel2.first),_y2(pixel2.second),_z2(pixel2.third),
    _red(color.first),_green(color.second),_blue(color.third)
{

}


void DrawLineVisitor::visit( ZZZImage<UChar> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<Short> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<UShort> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<Int32> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<UInt32> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<Float> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<Double> & image )
{
  visitAny(image);
}

void DrawLineVisitor::visit( ZZZImage<Bool> & image )
{
  visitAny(image);
}


