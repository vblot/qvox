/** -*- mode: c++ -*-
 * @file   zzzimage_bool.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:36:16 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "zzzimage_bool.h"
#include "image_header.h"

#ifdef _IS_UNIX_
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include <sstream>
#include <string>
#include <cstring>
#include <cassert>
#include <ctime>
#include <stack>

#include "tools.h"
#include "tools3d.h"

using std::string;
using std::stringstream;
using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::numeric_limits;
using std::stack;

#ifdef _IS_UNIX_
namespace { const bool IsUnix = true; }
#else
namespace { const bool IsUnix = false; }
#endif //_IS_UNIX_

#ifdef _GZIP_AVAILABLE_
namespace { const bool HasGzip = true; }
#else
namespace { const bool HasGzip = false; }
#endif //_IS_UNIX_

bool
ZZZImage<Bool>::alloc( const SSize width, const SSize height, const SSize depth,
                       const SSize bands,
                       const bool clear )
{
  if ( width == _cols && height == _rows && depth == _depth && bands == _bands  ) {
    if ( clear )
      ZZZImage<Bool>::clear();
    return true;
  }
  free();
  _depth = depth;
  _rows = height;
  _cols = width;
  _bands = bands;
  if ( !depth || !height || !width || !bands )  {
    return true;
  }

  const SSize n = _cols*_rows*_depth;
  _band_size = (n & 0x1Ful) ? (n>>5)+1 : (n>>5);

  try {
    _bands_array = new UInt*[ bands ];
    _bands_array[0] = new UInt[ bands * _band_size ];
    for ( SSize b = 1; b < _bands; ++b )
      _bands_array[b] = _bands_array[b-1] + _band_size;
  } catch ( std::bad_alloc ) {
    ERROR << "Memory allocation error" << std::endl;
    ERROR << "for volume " << width << "x" << height << "x" << depth
          << " (" << bands << " bands)\n";
    exit(-1);
  }
  if ( clear )
    ZZZImage<Bool>::clear();
  return true;
}

void
ZZZImage<Bool>::free()
{
  if ( ! _bands_array ) return;
  disposeArray( _bands_array[0] );
  disposeArray( _bands_array );
  _bands = 0;
  _depth = _rows = _cols = 0;
}


bool
ZZZImage<Bool>::isEmpty() const
{
  return ! _bands_array;
}

bool
ZZZImage<Bool>::poFileType( Typobj type )
{
  if ( POvalueType[ poType() ] == POvalueType[ type ] ) {
    _poFileType = type;
    return true;
  }
  return false;
}

ZZZImage<Bool> *
ZZZImage<Bool>::clone() const
{
  return new ZZZImage<Bool>( *this );
}

AbstractImage *
ZZZImage<Bool>::clone( int valueType ) const
{
  switch( valueType ) {
  case Po_ValUC: {
    ZZZImage<UChar> *p = new ZZZImage<UChar>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValSS: {
    ZZZImage<Short> *p = new ZZZImage<Short>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValUS: {
    ZZZImage<UShort> *p = new ZZZImage<UShort>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValSL: {
    ZZZImage<Int32> *p = new ZZZImage<Int32>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValUL: {
    ZZZImage<UInt32> *p = new ZZZImage<UInt32>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValSF: {
    ZZZImage<Float> *p = new ZZZImage<Float>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValSD: {
    ZZZImage<Double> *p = new ZZZImage<Double>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValB: {
    ZZZImage<Bool> *p = new ZZZImage<Bool>( _bands, size() );
    *p = *this;
    return p;
  }
  case Po_ValUnknown:
    return 0;
  default:
    ERROR << "AbstractImage::create(): bad value type : " << valueType << std::endl;
  }
  return 0;
}


ZZZImage<Bool> &
ZZZImage<Bool>::operator=( const Bool val )
{
  if ( val )
    for ( SSize b = 0; b < _bands; ++b )
      memset( _bands_array[b], 0xFF, _band_size * sizeof(UInt) );
  else
    for ( SSize b = 0; b < _bands; ++b )
      memset( _bands_array[b], 0, _band_size * sizeof(UInt) );
  return *this;
}


ZZZImage<Bool> &
ZZZImage<Bool>::operator=( const ZZZImage<Bool> & src )
{
  _poFileType = src._poFileType;
  if ( props() != src.props() )
    alloc( src._cols, src._rows, src._depth, src._bands, false );
  memcpy( _bands_array[0],
      src._bands_array[0],
      _bands * _band_size * sizeof(UInt) );
  return *this;
}

ZZZImage<Bool> &
ZZZImage<Bool>::operator+=( const Bool & value )
{
  UInt mask = value ? 0xFFFFFFFFu : 0;
  UInt * p = _bands_array[0];
  UInt * end = p + _bands * _band_size;
  while ( p != end )
    *(p++) ^= mask;
  return *this;
}

const char *
ZZZImage<Bool>::valueString( SSize x, SSize y, SSize z ) const
{
  stringstream ss;
  static char result[512];
  ss << static_cast<int>( bit( x, y, z, 0 ) );
  for ( int b = 1; b < _bands; b++)
    ss << ',' << static_cast<int>( bit( x, y, z, b ) );
  strcpy( result, ss.str().c_str() );
  return result;
}

const char *
ZZZImage<Bool>::valueString(const Triple<SSize> & voxel ) const
{
  stringstream ss;
  static char result[512];
  ss << static_cast<int>( bit( voxel, 0 ) );
  for ( int b = 1; b < _bands; b++)
    ss << ','
       << static_cast<int>( (*this)( bit( voxel, b ) ) );
  strcpy( result, ss.str().c_str() );
  return result;
}

bool
ZZZImage<Bool>::nonZero( SSize x, SSize y, SSize z ) const
{
  for ( SSize band = 0; band < _bands; ++band )
    if ( bit(x,y,z,band) )
      return true;
  return false;
}

/**
 * Zeros the data vector of the image.
 */
void ZZZImage<Bool>::clear() {
  if ( !_bands_array ) return;
  memset( _bands_array[0], 0, _bands * _band_size * sizeof(UInt) );
}

Dimension3d ZZZImage<Bool>::dimension() {
  return Dimension3d( _depth, _rows, _cols );
}

Triple<float>
ZZZImage<Bool>::gravityCenter( )
{
  SSize x, y, z;
  float sum_x = 0, sum_y = 0, sum_z = 0;
  float n = 0;
  if ( _bands > 1 )
    for ( z = 0; z < _depth; z++ )
      for ( y = 0; y < _rows; y++ )
        for ( x = 0; x < _cols; x++ ) {
          if ( ! zero( x, y, z ) ) {
            n += 1;
            sum_x += x;
            sum_y += y;
            sum_z += z;
          }
        }
  else
    for ( z = 0; z < _depth; z++ )
      for ( y = 0; y < _rows; y++ )
        for ( x = 0; x < _cols; x++ ) {
          if ( bit( x, y, z ) ) {
            n += 1;
            sum_x += x;
            sum_y += y;
            sum_z += z;
          }
        }
  return makeTriple( sum_x, sum_y, sum_z ) / n;
}

/*
 *
 * Input / Output methods
 *
 */

/**
 * Loads a file volume. Redirects to a specific loader according
 * to the filename extension.
 *
 * @param filename The path of the file to be loaded.
 * @return true if file has been succesfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::load( const char *filename, bool multiplexed ) {
  if ( !strcmp( filename, "null" ) )
    return true;
  bool ok = false;

  std::ifstream file;
  if ( strcmp( filename, "-" ) ) {
    file.open( filename );
    if ( ! file ) {
      perror("ZZZImage::load() failed:");
      return false;
    }
  }
  if ( !strcmp( filename, "-" )
       || extension( filename, ".pan" )
       || extension( filename, ".3dz" )
       || extension( filename, ".vff" )
       || extension( filename, ".vol" )
       || extension( filename, ".asc" )
       || extension( filename, ".npz" ) ) {
    ByteInput * input = 0;
    if ( !strcmp( filename, "-" ) )
#if defined(_IS_UNIX_)
      input = new ByteInputFD( 0 );
#else
      input = new ByteInputStream( std::cin );
#endif
    else {
      input = new ByteInputStream( file );
    }
    ok = load( *input );
    delete input;
  }
  else if ( IsUnix && HasGzip
            && ( extension( filename, ".pan.gz" )
                 || extension( filename, ".3dz.gz" )
                 || extension( filename, ".vff.gz" )
                 || extension( filename, ".vol.gz" )
                 || extension( filename, ".asc.gz" ) ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    ByteInputStream input( file );
    ByteInputGZip gzInput( input );
    ok = load( gzInput );
    file.close();
#else
    ERROR << "Cannot read GZipped data.\n";
    ok = false;
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  }
  else if ( extension( filename, ".raw" ) ) {
    ok = loadRAW( file, multiplexed );
  }
  else  if ( extension( filename, ".bmp" ) ) {
    ok = loadBMP( file );
  }
  else if ( extension( filename, ".xbm" ) ) {
    ok = loadXBM( file );
  }
  else if ( file && strstr( filename, "." ) == 0 ) {
    ok = load( file );
  }
  return ok;
}

bool
ZZZImage<Bool>::load( istream & in )
{
  ByteInputStream input( in );
  return load( input );
}

bool
ZZZImage<Bool>::load( ByteInput & input )
{
  ImageHeader header( input );
  if ( ! header.isOk() )
    return false;
  bool ok = false;

  // 3DZ
  if ( header.fileType() == ImageHeader::Type3DZ ) {
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::load3DZ() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ]
            << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                           header.bands(), false );
    ok = loadDataRLE( input );
  }

  // VFF
  if ( header.fileType() == ImageHeader::TypeVFF ) {
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                           header.bands(), false );
    ok = loadCharData( input );
  }

  // ASC
  if ( header.fileType() == ImageHeader::TypeASC ) {
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(), header.bands(), false );
    ok = loadDataASC( input );
  }

  // VOL
  if ( header.fileType() == ImageHeader::TypeVOL ) {
    if ( ValueTypeSize[ POvalueType[ header.poType() ] ] != ValueTypeSize[ POvalueType[ poType() ] ] ) {
      ERROR << "ZZZImage<>::loadVOL() failed: Voxel size mismatch Found: "
            << ValueTypeSize[ POvalueType[ header.poType() ] ]
          << ", Expected: "
          << ValueTypeSize[ POvalueType[ poType() ] ] << ").\n";
      return false;
    }
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                           header.bands(), false );
    ok = loadData( input, false );
  }

  // PAN
  if ( header.fileType() == ImageHeader::TypePAN ) {
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::loadPAN() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                           header.bands(), false );
    ok = loadPandoreData( input, ( POdimension[ header.poType() ] < 0 ), header.endian() != endian() );
  }

  // NPZ
  if ( header.fileType() == ImageHeader::TypeNPZ ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    const unsigned int NPIC_TEST32 =  0x12345678;
    if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
      ERROR << "ZZZImage<>::loadNPZ() failed: Value type mismatch (Found: "
            << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
      return false;
    }
    ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                           header.bands(), false );
    ByteInputGZip gzInput( input );
    Endianness oppEndian = static_cast<Endianness>( 1 - ::endian() );
    unsigned int ui;
    gzInput.read( reinterpret_cast<char*>( &ui ), 4 );
    if ( ui != NPIC_TEST32 )
      header.endian( oppEndian );
    ok = loadData( gzInput, false );
#else
    ERROR << "Cannot read GZipped data.\n";
    ok = false;
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  }

  if ( ok ) {
    if ( header.endian() != endian() && ( header.fileType() !=  ImageHeader::TypePAN ) ) {
      reverseDataBytes();
    }
    _aspect = header.aspect();
    _poFileType = header.poType();
  }
  return ok;
}


/**
 * Save volume into a file. Redirects to a specific writer according
 * to the filename extension.
 *
 * @param filename The path of the file to be written.
 * @return true if file has been succesfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::save( const char * filename ) const {
  if ( !strcmp( filename, "null" ) ) return true;

  ByteOutput *pOutput = 0;
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  ByteOutputGZip *pOutputGZip = 0;
#endif
  ByteOutputStream *pOutputStream = 0;
  ofstream file;
  bool ok = false;
  if ( ! strcmp( filename, "-" ) ) {
#if defined(_IS_UNIX_)
    pOutput = new ByteOutputFD( 1 );
#else
    pOutput = new ByteOutputStream( std::cout );
#endif
  } else {
    file.open( filename );
    if ( !file ) {
      perror("ZZZImage::save() failed");
      return false;
    }
    pOutputStream = new ByteOutputStream( file );
    if ( extension( filename, ".gz" ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
      pOutput = pOutputGZip = new ByteOutputGZip( *pOutputStream );
#else
      ERROR << "Cannot read GZipped data.\n";
      return false;
#endif
    } else {
      pOutput = pOutputStream;
    }
  }

  if ( extension( filename, ".vff" )
       || ( IsUnix && HasGzip && extension( filename, ".vff.gz" ) ) ) {
    ok =  saveVFF( *pOutput );
  }
  else if ( extension( filename, ".3dz" )
            || ( IsUnix && HasGzip && extension( filename, ".3dz.gz" ) ) ) {
    ok =  save3DZ( *pOutput );
  }
  else if ( extension( filename, ".asc" )
            || ( IsUnix && HasGzip && extension( filename, ".asc.gz" ) ) ) {
    ok =  saveASC( *pOutput );
  }
  else if ( extension( filename, ".vol" )
            || ( IsUnix && HasGzip && extension( filename, ".vol.gz" ) ) ) {
    ok =  saveVOL( *pOutput );
  }
  else if ( IsUnix && HasGzip && extension( filename, ".npz" ) ) {
    ok =  saveNPZ( *pOutput );
  }
  else if ( extension( filename, ".pan" )
            || ( IsUnix && HasGzip && extension( filename, ".pan.gz" ) )
            || !strcmp( filename, "-" ) ) {
    ok =  savePAN( *pOutput );
  }
  else if ( extension( filename, ".raw" )
            || ( IsUnix && HasGzip && extension( filename, ".raw.gz" ) ) ) {
    ok = saveRAW( *pOutput );
  }
  else if ( extension( filename, ".bmp" ) ) {
    ok =  saveBMP( *pOutput );
  }
  else if ( extension( filename, ".jbm" ) ) {
    ok = saveJBM( *pOutput );
  }

  pOutput->flush();

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  delete pOutputGZip;
#endif

  delete pOutputStream;
  return ok;
}

/**
 * Save volume into a file. Redirects to a specific writer according
 * to the specified file format (e.g. "pan" or "PAN" for a Pandore file).
 *
 * @param out The output stream.
 * @param format The output file format.
 * @return true if file has been succesfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::save( ByteOutput & out, const char * format ) const
{
  char fmt[32];
  strToLower( fmt, format );

  ByteOutput *pOutput = 0;
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  ByteOutputGZip *pOutputGZip = 0;
#endif

  bool ok = false;
  if ( extension( fmt, ".gz" ) ) {
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
    pOutput = pOutputGZip = new ByteOutputGZip( out );
#else
    ERROR << "Cannot read GZipped data.\n";
    return false;
#endif
  } else {
    pOutput = &out;
  }

  if ( !strcmp( fmt, "vff" )
       || ( IsUnix && HasGzip && !strcmp( fmt, "vff.gz" ) ) ) {
    ok =  saveVFF( *pOutput );
  }
  else if ( !strcmp( fmt, "3dz" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "3dz.gz" ) ) ) {
    ok =  save3DZ( *pOutput );
  }
  else if ( !strcmp( fmt, "asc" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "asc.gz" ) ) ) {
    ok =  saveASC( *pOutput );
  }
  else if ( !strcmp( fmt, "vol" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "vol.gz" ) ) ) {
    ok =  saveVOL( *pOutput );
  }
  else if ( IsUnix && HasGzip && !strcmp( fmt, "npz" ) ) {
    ok =  saveNPZ( *pOutput );
  }
  else if ( !strcmp( fmt, "pan" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "pan.gz" ) ) ) {
    ok =  savePAN( *pOutput );
  }
  else if ( !strcmp( fmt, "raw" )
            || ( IsUnix && HasGzip && !strcmp( fmt, "raw.gz" ) ) ) {
    ok = saveRAW( *pOutput );
  }
  else if ( !strcmp( fmt, "bmp" ) ) {
    ok =  saveBMP( *pOutput );
  }
  else if ( !strcmp( fmt, "jbm" ) ) {
    ok = saveJBM( *pOutput );
  }

  pOutput->flush();

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  delete pOutputGZip;
#endif
  return ok;
}


/**
 * ASCII file format loader.
 *
 * @param filename Path to the ".asc" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadASC( ByteInput & input )
{
  ImageHeader header( input );
  if ( ! header.isOk() )
    return false;
  alloc( header.width(), header.height(), header.depth(), header.bands(), false );
  return loadDataASC( input );
}

/**
 * ASCII Data loader.
 *
 * @return true if the data has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadDataASC( ByteInput & input )
{
  int plane = 0;
  int planes = 0;
  float value;
  char line[1024];
  do {
    input.getline( line, 1024 );
    if ( ! sscanf( line, "PLAN %d", &plane ) ) {
      ERROR << "ZZZImage<Bool>::readASC(): error reading plane "
            << plane + 1 << std::endl;
      return false;
    }
    for ( SSize y = _rows; y; y--) {
      input.getline( line, 1024 );
      for ( SSize x = 0; x < _cols; x++) {
        sscanf( line, "%f", &value);
        if ( value != 0.0 )
          (*this)( x, y, plane, 0 ) = true;
      }
    }
    ++planes;
  } while ( planes < _depth );
  return true;
}

/**
 * ASCII file format writer.
 *
 * @param filename Path to the ".asc" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
bool ZZZImage<Bool>::saveASC( ByteOutput & out ) const
{
  SSize x, y, z;
  out << "OBJET3D\n";
  out << "TX,TY,TZ:" << _cols << "," << _rows << "," << _depth << '\n';
  for ( z = 0; z < _depth; z++) {
    out << "PLAN " << z << '\n';
    for (y = _rows; y; y--) {
      for (x = 0; x < _cols; x++)
        out << " " << std::setw( 4 ) << bit( x, y, z, 0 );
      out << '\n';
    }
  }
  return true;
}

/**
 * VFF file format loader.
 *
 * @param filename Path to the ".vff" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadVFF( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypeVFF ) {
    ERROR << "ZZZImage<>::loadVFF() failed: bad magic number.\n";
    return false;
  }

  _aspect = header.aspect();
  _poFileType = header.poType();
  alloc( header.width(), header.height(), header.depth(), header.bands(), false );
  return loadCharData( input );
}

/**
 * VFF file format writer.
 *
 * @param filename Path to the ".vff" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */

bool
ZZZImage<Bool>::saveVFF( ByteOutput & out ) const
{
  out << "ncaa\n";
  out << "type=raster;\n";
  out << "format=slice;\n";
  out << "rank=3;\n";
  out << "bands=" << _bands << ";\n";
  out << "bits=8;\n";
  out << "size=" << _cols
      << " " << _rows
      << " " << _depth << ";\n";
  out << "origin=0.0 0.0 0.0;\n";
  out << "extent="
      << float( _cols ) << " "
      << float ( _rows ) << " "
      << float ( _depth ) << ";\n";
  out << "aspect=" << _aspect.first
      << " " << _aspect.second
      << " " << _aspect.third << ";\n";
  out << "creator=ZZZImage library (c) 1997-2100 Sebastien Fourey - GREYC;\n";
  out.put(12);
  out.put(10);
  for (SSize band = 0 ; band < _bands; band++ )
    for (SSize z = 0; z < _depth; z++)
      for (SSize y = 0; y < _rows; y++)
        for (SSize x = 0; x < _cols; x++)
          out.put( bit( x, y, z, band ) );
  return true;
}

/**
 * VOL file format loader.
 *
 * @param filename Path to the ".vol" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadVOL( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypeVOL ) {
    ERROR << "ZZZImage<>::loadVOL() failed: bad magic number\n";
    return false;
  }

  _aspect = header.aspect();
  _poFileType = header.poType();
  if ( ValueTypeSize[ POvalueType[ _poFileType ] ] != ValueTypeSize[ POvalueType[ poType() ] ] ) {
    ERROR << "ZZZImage<>::loadVOL() failed: Voxel size mismatch Found: "
          << ValueTypeSize[ POvalueType[ _poFileType ] ]
          << ", Expected: "
          << ValueTypeSize[ POvalueType[ poType() ] ] << ").\n";
    return false;
  }

  alloc( header.width(), header.height(), header.depth(), header.bands(), false );
  bool ok = loadData( input, false );
  if ( ok && ( header.endian() != endian() ) ) {
    reverseDataBytes();
  }
  return ok;
}

/**
 * RAW file format loader.
 *
 * @param filename Path to the ".raw" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadRAW( ByteInput & input, bool multiplexed )
{
  if ( _depth * _rows * _cols == 0 ) {
    ERROR << "ZZZImage<>::loadRAW(): Volume has null size.\n";
    return false;
  }
  return loadData( input, multiplexed );
}

bool
ZZZImage<Bool>::loadRAW( std::istream & in, bool multiplexed )
{
  ByteInputStream input( in );
  return loadRAW( input, multiplexed );
}

/**
 * RAW file format loader with specified dimension.
 *
 * @param filename Path to the ".raw" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadRAW( std::istream & file,
                         const Dimension3d & dim,
                         bool multiplexed )
{
  alloc( dim, 1, false  );
  ByteInputStream input( file );
  return loadData( input, multiplexed );
}

/**
 * XBM file format loader
 *
 * @param filename Path to the ".xbm" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadXBM( std::istream & file )
{
  int x, y;
  char c;
  SSize buffSize = 0;
  char *buffer;
  char *pc;
  int octet;
  int line, column;

  while ( file.get(c) , c != '{' ) buffSize++;

  buffer = new char[ buffSize + 5 ];
  file.seekg( 0 );

  pc = buffer;
  while ( file.get(c) , c != '{' ) {
    *(pc++) = c;
  }
  *pc = '\0';

  pc = strstr( buffer, "width" ) + 5;
  sscanf( pc, "%d", &x );
  pc = strstr( buffer, "height" ) + 6;
  sscanf( pc, "%d", &y );
  disposeArray( buffer );

  alloc( x, y, 1, 1, true );
  for ( line = 0; line < _rows; line++ )
    for ( column = 0; column < _cols; column += 8 ) {
      file >> setbase(std::ios::hex) >> octet ;
      file.get( c );
      while ( c != ',' ) file.get( c );
      for ( int count = 0;
            count < 8 && count + column < _cols;
            count++ ) {
        if ( octet & ( 1 << count ) )
          operator()( column + count,
            2 + ( _rows  - ( line + 1 ) ),
            0 ) = true;
      }
    }
  return true;
}

/**
 * VOL file format writer.
 *
 * @param filename Path to the ".vol" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::saveVOL( ByteOutput & out ) const
{
  out << "X: " << _cols << "\n";
  out << "Y: " << _rows << "\n";
  out << "Z: " << _depth << "\n";
  out << "Version: 2\n";
  out << "Voxel-Size: " << 125 << "\n";
  out << "Alpha-Color: 0\n";
  if ( isLittleEndian() )
    out << "Int-Endian: 0123\n";
  else
    out << "Int-Endian: 3210\n";
  out << "Voxel-Endian: 0\n";
  out << "Res-X: 1.000000\n";
  out << "Res-Y: 1.000000\n";
  out << "Res-Z: 1.000000\n";
  out << ".\n";

  //   writeWordMSB( file, _cols );
  //   writeWordMSB( file, _rows );
  //   writeWordMSB( file, _depth );
  //   file.put('\n');

  out.write( reinterpret_cast<char*>( _bands_array[0] ),
      _bands * _band_size * sizeof(UInt) );
  return true;
}

/**
 * NPZ file format writer.
 *
 * @param filename Path to the ".npz" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
bool
ZZZImage<Bool>::saveNPZ( ByteOutput & out ) const
{
  time_t t;
  time( &t );

  out << "NPZ-11\n";
  out << "# Image file generated by QVox on " << ctime( &t );

  int d = POdimension[ poFileType() ];
  if ( _bands > 1 ) d = 4;
  char format[255];
  sprintf( format, "NPIC_IMAGE_%dB\n", d );
  out << "TYPE " << format;
  out << "COMP GZIP\n";
  out << "XMAX " << _cols << "\n";
  out << "YMAX " << _rows << "\n";
  out << "ZMAX " << _depth << "\n";
  out << "PROP Alpha-Color: 0\n";
  out << "PROP Voxel-Size: " << 125 << "\n";
  if ( isLittleEndian() )
    out << "PROP Int-Endian: 0123\n";
  else
    out << "PROP Int-Endian: 3210\n";
  out << "PROP Voxel-Endian: 0\n";
  out << "PROP Res-X: 1.000000\n";
  out << "PROP Res-Y: 1.000000\n";
  out << "PROP Res-Z: 1.000000\n";
  out << "DATA\n";
  out.flush();

  ByteOutputGZip gzOutput( out );
  unsigned int NPIC_TEST32 =  0x12345678;
  gzOutput.write( reinterpret_cast<const char*>( &NPIC_TEST32 ), 4 );
  for ( int b = 0; b < _bands; ++b ) {
    gzOutput.write( reinterpret_cast<char*>( _bands_array[ b ] ),
                    _band_size * sizeof( UInt ) );
  }
  return true;
}
#else
bool
ZZZImage<Bool>::saveNPZ( ByteOutput & ) const
{
  ERROR << "Cannot handle the NPZ format.\n";
  return false;
}
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)

/**
 * RAW file format writer.
 *
 * @param filename Path to the ".raw" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::saveRAW( ByteOutput & out ) const
{
  for ( int b = 0; b < _bands; ++b ) {
    out.write( reinterpret_cast<char*>( _bands_array[ b ] ),
               _band_size * sizeof( UInt ) );
  }
  return true;
}

/**
 * 3DZ file format writer.
 *
 * @param filename Path to the ".3dz" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::save3DZ( ByteOutput & out ) const
{
  out << "3dz\n";
  out << "size=" << _depth << " " << _rows << " " << _cols << ";\n";
  out << "TYPE=" << name() << ";\n";
  out << "POTYPE=" << poType() << ";\n";
  out << "BANDS=" << _bands << ";\n";
  out.put(12);
  out.put(10);
  UChar seqLength;
  const UChar maxLength = UChar(-1);
  const size_t count = _depth * _rows * _cols;
  for ( int band = 0 ; band < _bands; ++band ) {
    Bool value, newValue;
    seqLength = 0;
    size_t totalVoxels = 0;
    while ( totalVoxels < count ) {
      newValue = bit( totalVoxels, 0, 0, band );
      if ( !seqLength ) value = newValue;
      if ( newValue == value ) {
        seqLength++;
        if ( seqLength == maxLength ) {
          out.put( seqLength );
          out.write( reinterpret_cast<char*>( &value ), 1 );
          seqLength = 0;
        }
      } else {
        out.put( seqLength );
        out.write( reinterpret_cast<char*>( &value ), 1 );
        value = newValue;
        seqLength = 1;
      }
      totalVoxels++;
    }
    if ( seqLength ) {
      out.put( seqLength );
      out.write( reinterpret_cast<char*>( &value ),  1 );
    }
  }
  return true;
}

/**
 * 3DZ file format loader.
 *
 * @param filename Path to the ".3dz" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::load3DZ( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::Type3DZ ) {
    ERROR << "ZZZImage<>::load3DZ() failed: bad magic number\n";
    return false;
  }
  _aspect = header.aspect();
  _poFileType = header.poType();
  if ( POvalueType[ _poFileType ] != POvalueType[ poType() ] ) {
    ERROR << "ZZZImage<>::load3DZ() failed: Value type mismatch (Found: "
          << POvalueType[ _poFileType ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
    return false;
  }

  ZZZImage<Bool>::alloc( header.width(), header.height(), header.depth(),
                         header.bands(), false );

  bool ok = loadDataRLE( input );
  if ( ok && header.endian() != endian() ) {
    reverseDataBytes();
  }
  return ok;
}


/**
 * PAN file format loader.
 *
 * @param filename Path to the ".pan" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadPAN( ByteInput & input )
{
  ImageHeader header( input );
  if ( !header.isOk() )
    return false;
  if ( header.fileType() != ImageHeader::TypePAN ) {
    ERROR << "ZZZImage<>::loadPAN() failed: bad magic number\n";
    return false;
  }

  if ( POvalueType[ header.poType() ] != POvalueType[ poType() ] ) {
    ERROR << "ZZZImage<>::loadPAN() failed: Value type mismatch (Found: "
          << POvalueType[ header.poType() ] << ", Expected: " << POvalueType[ poType() ] << ").\n";
    return false;
  }

  alloc( header.width(), header.height(), header.depth(),
         header.bands(), false );

  bool ok = loadPandoreData( input, (POdimension[ header.poType() ] < 0), ( header.endian() != endian() ) );
  if ( ok ) {
    _aspect = header.aspect();
    _poFileType = header.poType();
  }
  return ok;
}

bool
ZZZImage<Bool>::savePandoreHeader( ByteOutput & out, Typobj type ) const
{
  Po_header header;
  memset( & header, 0, sizeof( header ));
  std::string date = ::today();
  strcpy( header.ident, "QVox" );
  strcpy( header.date, date.c_str() );
  memcpy( header.magic, PO_MAGIC, sizeof( header.magic ) );
  header.potype = type;
  out.write( (const char*) &header, sizeof( header ) );
  return out;
}

bool
ZZZImage<Bool>::savePAN( ByteOutput & out ) const {
  if ( savePandoreHeader( out, poFileType() ) &&
       savePandoreAttributes( out ) &&
       savePandoreData( out  ) ) return true;
  ERROR << "ZZZImage<>::savePAN() failed.\n";
  return false;
}

bool
ZZZImage<Bool>::loadPandoreData( ByteInput & input, bool region, bool swapBytes )
{
  bool ok;
  if ( region  ){
    Int32 maxLabel;
    input.read( reinterpret_cast<char*>( &maxLabel ), sizeof( maxLabel ) );
    ok = loadRegionData( input, swapBytes, static_cast<UInt32>( maxLabel ) );
  } else {
    ok = loadData( input, false );
    if ( ok && swapBytes ) reverseDataBytes();
  }
  return ok;
}

bool
ZZZImage<Bool>::loadData( ByteInput & input, bool multiplexed )
{
  if ( multiplexed ) {
    std::cerr << "ZZZImage<Bool>::loadData() not implemented for multiplexed data\n";
    memset( _bands_array[0], 0, sizeof(UInt) * _bands * _band_size );
    return false;
  }
  if ( ! input.read( reinterpret_cast<char*>(_bands_array[0]),
                     sizeof(UInt) * _bands * _band_size ) )
    return input;
  return true;
}

bool
ZZZImage<Bool>::loadCharData( ByteInput & input )
{
  char c;
  UInt shift;
  // Optimize to prevent many indirections from virtual method call
  if ( typeid( input ) == typeid( ByteInputStream ) ) {
    ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
    istream & in = inputFile.in();
    for (SSize b = 0; b < _bands; ++b )
      for (SSize z = 0; z < _depth; z++)
        for (SSize y = 0; y < _rows; y++)
          for (SSize x = 0; x < _cols; x++) {
            in.get( c );
            if ( static_cast<Bool>( c ) ) {
              UInt & w = word( x, y, z, b, shift );
              w |= 1l << shift;
            }
          }
    return in;
  } else {
    for (SSize b = 0; b < _bands; ++b )
      for (SSize z = 0; z < _depth; z++)
        for (SSize y = 0; y < _rows; y++)
          for (SSize x = 0; x < _cols; x++) {
            input.get( c );
            if ( static_cast<Bool>( c ) ) {
              UInt & w = word( x, y, z, b, shift );
              w |= 1l << shift;
            }
          }
    return input;
  }
}

bool
ZZZImage<Bool>::loadDataRLE( ByteInput & input )
{
  const SSize size = _depth * _rows * _cols;
  char c;

  // Optimize to prevent many indirections from virtual method call
  if ( typeid( input ) == typeid( ByteInputStream ) ) {
    ByteInputStream & inputFile = dynamic_cast<ByteInputStream &>( input );
    istream & in = inputFile.in();
    for ( int b = 0; b < _bands ; ++b ) {
      UInt * band = _bands_array[b];
      SSize totalVoxels = 0;
      UChar seqLength;
      while ( totalVoxels < size ) {
        in.get( c );
        seqLength = static_cast<UChar>( c );
        in.get( c );
        if ( c )
          while  ( seqLength-- ) {
            band[ totalVoxels>>5 ] |= (1<<(totalVoxels&31));
            ++totalVoxels;
          }
        else
          totalVoxels += seqLength;
      }
    }
  } else {
    for ( int b = 0; b < _bands ; ++b ) {
      UInt * band = _bands_array[b];
      SSize totalVoxels = 0;
      UChar seqLength;
      while ( totalVoxels < size ) {
        input.get( c );
        seqLength = static_cast<UChar>( c );
        input.get( c );
        if ( c )
          while  ( seqLength-- ) {
            band[ totalVoxels>>5 ] |= (1<<(totalVoxels&31));
            ++totalVoxels;
          }
        else
          totalVoxels += seqLength;
      }
    }
  }
  return true;
}

/**
 * 3DZ file format loader (from a memory buffer).
 *
 * @param filename Path to the ".3dz" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadFileBuffer( const unsigned char *data, size_t size )
{
  ByteInputMemory input( reinterpret_cast<const char*>(data), size );
  return load( input );
}

std::istream &
ZZZImage<Bool>::loadPandoreAttributes( std::istream & file, bool inversionMode )
{
  Int32 attr[5];
  memset( attr, 0, 5 * sizeof( Int32 ) );
  attr[0] = 1;

  file.read( reinterpret_cast<char*>( attr ), 4 );
  if ( inversionMode ) {
    reverseBytes<4>( attr );
    reverseBytes<4>( attr + 1 );
    reverseBytes<4>( attr + 2 );
    reverseBytes<4>( attr + 3 );
  }

  if ( ! POHasBands[ this->_poFileType ] ) attr[0] = 1;

  if ( POdimension[ this->_poFileType ] == 3 ||
       POdimension[ this->_poFileType ] == -3 )
    file.read( reinterpret_cast<char*>( attr + 1 ), 4 );

  if ( POdimension[ this->_poFileType ] >= 2 ||
       POdimension[ this->_poFileType ] <= -2 )
    file.read( reinterpret_cast<char*>( attr + 2 ), 4 );

  if ( POdimension[ this->_poFileType ] >= 1 ||
       POdimension[ this->_poFileType ] <= -1 )
    file.read( reinterpret_cast<char*>( attr + 3 ), 4 );

  attr[ 4 ] = UNKNOWN;
  if ( POColor[ this->_poFileType ] ) {
    file.read( reinterpret_cast<char*>( attr + 4 ), 4 );
    attr[0] = 3;
    if ( inversionMode )
      reverseBytes<4>( attr + 4 );
    this->_colorSpace = static_cast<ColorSpace>( attr[ 4 ] );
    TRACE << "COLORSPACE: " << this->_colorSpace
          << " " << ColorSpaceNames[ this->_colorSpace ] << "\n";
  }

  alloc( attr[0], attr[1], attr[2], attr[3], false );
  return file;
}

ByteOutput &
ZZZImage<Bool>::savePandoreAttributes( ByteOutput & out ) const
{
  Int32 attr[5];
  attr[0] = _bands;
  attr[1] = _depth;
  attr[2] = _rows;
  attr[3] = _cols;
  attr[4] = _colorSpace;

  // if ( POHasBands[ this->_poFileType ] )
  out.write( reinterpret_cast<char*>( attr ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] == 3 ||
       POdimension[ this->_poFileType ] == -3 )
    out.write( reinterpret_cast<char*>( attr + 1 ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] >= 2 ||
       POdimension[ this->_poFileType ] <= -2 )
    out.write( reinterpret_cast<char*>( attr + 2 ), sizeof( *attr ) );

  if ( POdimension[ this->_poFileType ] >= 1 ||
       POdimension[ this->_poFileType ] <= -1 )
    out.write( reinterpret_cast<char*>( attr + 3 ), sizeof( *attr ) );

  if ( POColor[ this->_poFileType ] )
    out.write( reinterpret_cast<char*>( attr + 4 ), sizeof( *attr ) );

  TRACE << " Is color ? " << POColor[ this->_poFileType ]  << "\n";
  TRACE << " ColorSpace ? " << _colorSpace  << "\n";
  return out;
}

/*
 * Dave object data.
 */
ByteOutput &
ZZZImage<Bool>::savePandoreData( ByteOutput & out ) const
{
  for ( int b = 0; b < _bands; b++ )
    if ( ! out.write( reinterpret_cast<char*>( _bands_array[b] ),
                      sizeof( UInt ) * static_cast<size_t>( _band_size ) ) )
      return out;
  return out;
}

/**
 * Save UInt32 data using the minimum of 1, 2 or 4 bytes according
 * to a maximum value specified.
 *
 * @param file
 * @param maximalValue The maximal value of the labels in a region image.
 *
 * @return The provided output stream.
 */
ByteOutput &
ZZZImage<Bool>::saveRegionData( ByteOutput & out, UInt32 /* maximalValue */ ) const {
  const size_t size = _depth * _rows * _cols;
  size_t totalVoxels = 0;
  while ( totalVoxels < size ) {
    out.put( bit( totalVoxels, 0 ) );
    ++totalVoxels;
  }
  return out;
}

/**
 * Load UInt32 data using the minimum of 1, 2 or 4 bytes according
 * to a maximum value specified.
 *
 * @param file
 * @param maximalValue The maximal value of the labels in a region image.
 *
 * @return The provided input stream.
 */
bool
ZZZImage<Bool>::loadRegionData( ByteInput & input, bool swapBytes, UInt32 maximalValue )  {
  size_t size = _depth * _rows * _cols;
  size_t totalVoxels = 0;
  UInt32 l;
  UInt * band = _bands_array[0];

  if ( maximalValue <= numeric_limits<UChar>::max() ) {
    UChar c;
    while ( totalVoxels < size ) {
      if ( ! input.read( (char*) &c, 1 ) )
        return input;
      if ( c )
        band[ totalVoxels >> 5 ] |= (1ul << (totalVoxels&0x1F));
      ++totalVoxels;
    }
  } else if ( maximalValue <= numeric_limits<UInt32>::max() ) {
    Int32 s;
    while ( totalVoxels < size ) {
      if ( ! input.read( (char*) &s, sizeof( s ) ) )
        return input;
      if ( swapBytes ) reverseBytes<sizeof(s)>( &s );
      if ( s )
        band[ totalVoxels >> 5 ] |= (1ul << (totalVoxels&0x1F));
      ++totalVoxels;
    }
  } else while ( totalVoxels < size ) {
    if ( ! input.read( (char*) &l, sizeof( l ) ) )
      return input;
    if ( swapBytes ) reverseBytes<sizeof(l)>( & l );
    if ( l )
      band[ totalVoxels >> 5 ] |= (1ul << (totalVoxels&0x1F));
    totalVoxels++;
  }
  return input;
}

/**
 * JBM file format writer.
 *
 * @param filename Path to the ".jbm" file to be written.
 * @return true if file has been successfully saved, otherwise false.
 */
bool
ZZZImage<Bool>::saveJBM( ByteOutput & out ) const
{
  out << "{\n";
  for (SSize z = 0; z < _depth; z++ ) {
    out << "{";
    for ( SSize y = 0; y < _rows; y++ ) {
      out << "{";
      for ( SSize x = 0; x < _cols; x++ ) {
        if ( x < _cols - 1 )
          out << bit( x, y, z, 0 ) <<",";
        else
          out << bit( x, y, z, 0 ) <<"}";
      }
      if (y < _rows - 1)
        out << ",";
      else
        out << "},\n";
    }
  }
  out << "}\n";
  return true;
}

/**
 * BMP file format loader.
 *
 * @param filename Path to the ".bmp" file to be loaded.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::loadBMP( std::istream & file )
{
  unsigned long s, w, h;
  unsigned long offset;
  unsigned char cmap[256][3];
  unsigned short bitsPerPixel;

  if ( _bands != 3 ) {
    ERROR << ("ZZZImage<>::loadBMP() failed because volume is not 3 bands.");
    return false;
  }

  char magic[2];
  file.get( magic[ 0 ] );
  file.get( magic[ 1 ] );

  if ( magic[ 0 ]  != 'B' || magic[ 1 ] !=  'M') {
    ERROR << "ZZZImage<>::loadBMP() failed: bad header.";
    return false;
  }
  /* File size + 4 Reserved bytes */
  file.seekg( 8, std::ios::cur );

  offset = readWordLSB( file );
  s = readWordLSB( file );
  w = readWordLSB( file );
  h = readWordLSB( file );

  TRACE << "offset: " << offset << std::endl;
  TRACE << "s: " << s << std::endl;
  TRACE << "width: " << w << std::endl;
  TRACE << "height: " << h << std::endl;

  readShortLSB( file );		/* biPlanes */

  bitsPerPixel = readShortLSB( file );

  if ( props().bands != 3) {
    ERROR << "ZZZImage<>::readBMP(): object should have 3 planes.\n";
    return false;
  }

  if ( bitsPerPixel == 16 ) {	/* Bits per pixel */
    ERROR << "ZZZImage<>::readBMP(): only 8 or 24 bits images are handled\n";
    return false;
  }

  if ( readWordLSB( file ) ) {	/* Compression */
    ERROR << "ZZZImage<>::readBMP(): cannot handle compressed BMP format.\n";
    return false;
  }

  readWordLSB( file );		/* ImageDataSize */
  readWordLSB( file );		/* XPixelPerMeter */
  readWordLSB( file );		/* XPixelPerMeter */

  readWordLSB( file );		/* ColorsUsed */
  readWordLSB( file );		/* ColorImportants */

  _aspect = 1.0f;

  if ( bitsPerPixel == 8) {
    char c;
    for ( int i = 0; i < 256; i++) {
      file.read( reinterpret_cast<char*>( cmap[i] ), 3 );
      file.get( c ); // Dumb byte
    }

    alloc( w, h , 1l , 3, true);
    UInt *red_band = _bands_array[ 0 ];
    UInt *green_band = _bands_array[ 1 ];
    UInt *blue_band = _bands_array[ 2 ];
    unsigned char u;
    int full_line = ( (w + 3) / 4) * 4;
    SSize offset = 0;
    for ( unsigned int line = 0; line < h; line++ ) {
      unsigned int col;
      for ( col = 0; col < w; col++ ) {
        file.get( c );
        u = c;
        if ( cmap[u][0] ) red_band[ offset>>5 ] |= (1ul << (offset&0x1F));
        if ( cmap[u][1] ) green_band[ offset>>5 ] |= (1ul << (offset&0x1F));
        if ( cmap[u][2] ) blue_band[ offset>>5 ] |= (1ul << (offset&0x1F));
        ++offset;
      }
      file.seekg( full_line - col, std::ios::cur );
    }
  } else { // bitsPerPixel == 24
    alloc( w, h , 1l , 3l, true );
    UInt *red_band = _bands_array[ 0 ];
    UInt *green_band = _bands_array[ 1 ];
    UInt *blue_band = _bands_array[ 2 ];
    char red,green,blue;
    int full_line = ( (w + 3) / 4) * 4;
    SSize offset = 0;
    for ( unsigned int line = 0; line < h; line++ ) {
      unsigned int col;
      for ( col = 0; col < w; col++ ) {
        file.get( red );
        file.get( green );
        file.get( blue );
        if ( red ) red_band[ offset>>5 ] |= (1ul << (offset&0x1F));
        if ( green ) green_band[ offset>>5 ] |= (1ul << (offset&0x1F));
        if ( blue ) blue_band[ offset>>5 ] |= (1ul << (offset&0x1F));
      }
      file.seekg( full_line - col, std::ios::cur );
    }
  }
  return true;
}

/**
 * BMP file format writer.
 *
 * @param filename Path to the ".bmp" file to be saved.
 * @return true if file has been successfully loaded, otherwise false.
 */
bool
ZZZImage<Bool>::saveBMP( ByteOutput & out ) const
{
  if ( _bands != 3 ) {
    ERROR << "ZZZImage<>::saveBMP() failed because image is not a color image.";
    return false;
  }
  out.put('B');
  out.put('M');
  int lineSize =  ( ( ( _cols * 24 ) + 31 ) / 32 ) * 4;
  writeWordLSB( out, 54 + ( _rows * lineSize ) );
  writeShortLSB( out, 0);
  writeShortLSB( out, 0);
  writeWordLSB( out, 54 );
  writeWordLSB( out, 40 );
  writeWordLSB( out, _cols );
  writeWordLSB( out, _rows );
  writeShortLSB( out, 1);
  writeShortLSB( out, 24);
  writeWordLSB( out, 0 );
  writeWordLSB( out, _rows * lineSize );
  writeWordLSB( out, 75*39 );
  writeWordLSB( out, 75*39 );
  writeWordLSB( out, 0 );
  writeWordLSB( out, 0 );

  UInt * red_band = _bands_array[ 0 ];
  UInt * green_band = _bands_array[ 1 ];
  UInt * blue_band = _bands_array[ 2 ];
  SSize offset = 0;
  int fullLine = ( (_cols + 3) / 4) * 4;

  for ( int l = 0; l < _rows; l++ ) {
    int c;
    for ( c = 0; c < _cols ; c++ ) {
      out.put( ( red_band[offset>>5] & (1ul<<(offset&0x1F) ) ) != 0 );
      out.put( ( green_band[offset>>5] & (1ul<<(offset&0x1F) ) ) != 0 );
      out.put( ( blue_band[offset>>5] & (1ul<<(offset&0x1F) ) ) != 0 );
      ++offset;
    }
    for ( ; c < fullLine ; c++ ) out.put( 0 );
  }
  return true;
}

/*
 *
 * Tools methods
 *
 */

void
ZZZImage<Bool>::reverseDataBytes()
{
}

Pair<Bool>
ZZZImage<Bool>::range( SSize band )
{
  Pair<Bool> r( true, false );
  SSize x, y, z;
  if ( band == -1 ) {
    for ( SSize b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            if ( bit( x, y, z, b ) )
              r.second = true;
            else
              r.first = false;
            if ( !r.first && r.second ) return r;
          }
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          if ( bit( x, y, z, 0 ) )
            r.second = true;
          else
            r.first = false;
          if ( !r.first && r.second ) return r;
        }
  }
  return r;
}

Bool
ZZZImage<Bool>::min( SSize band )
{
  SSize x, y, z;
  if ( band == -1 ) {
    for ( SSize b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            if ( !bit( x, y, z, b ) ) return false;
          }
    return true;
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          if ( !bit( x, y, z, 0 ) ) return false;
        }
    return true;
  }
}

Bool
ZZZImage<Bool>::max( SSize band )
{
  SSize x, y, z;
  if ( band == -1 ) {
    for ( SSize b = 0 ; b < _bands; b++ )
      for ( z = 0 ; z < _depth; z++ )
        for ( y = 0 ; y < _rows; y++ )
          for ( x = 0 ; x < _cols; x++ ) {
            if ( bit( x, y, z, b ) ) return true;
          }
    return false;
  } else {
    for ( z = 0 ; z < _depth; z++ )
      for ( y = 0 ; y < _rows; y++ )
        for ( x = 0 ; x < _cols; x++ ) {
          if ( bit( x, y, z, 0 ) ) return true;
        }
    return false;
  }
}


Size
ZZZImage<Bool>::nonZeroVoxels()
{
  Size n = 0;
  Size index = 0;
  Size limit = _cols * _rows * _depth;
  if ( _bands == 1 ) {
    while ( index != limit ) {
      if ( bit( index++, 0, 0 ) ) ++n;
    }
  } else {
    while ( index != limit ) {
      if ( zero( index++, 0, 0 ) ) ++n;
    }
  }
  return n;
}


ZZZImage<Bool> *
ZZZImage<Bool>::levelMap( SSize )
{
  return clone();
}

void
ZZZImage<Bool>::extract( ZZZImage<Bool>  & result,
                         SSize x, SSize y, SSize z,
                         SSize width, SSize height, SSize depth )
{
  result.alloc( width, height, depth, false );
  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( int band = 0; band < _bands; ++band ) {
    for ( SSize k=0; k<depth; ++k )
      for ( SSize j=0; j<height; ++j )
        for ( SSize i=0; i<width; ++i ) {
          result( i, j, k, band ) = operator()(x+i, y+j, z+k, band );
        }
  }
}

ZZZImage<Bool> *
ZZZImage<Bool>::sub( SSize x, SSize y, SSize z,
                     SSize width, SSize height, SSize depth )
{
  ZZZImage<Bool> * result = new ZZZImage<Bool>( width, height, depth, _bands );

  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( int b = 0; b < _bands; ++b ) {
    for ( int k = 0; k < depth; k++ )
      for ( int j = 0; j < height; j++ )
        for ( int i = 0; i < width; i++ )
          (*result)(i,j,k,b) = (*this)(x+i,y+j,z+k,b);
  }
  return result;
}

ZZZImage<Bool> *
ZZZImage<Bool>::slice( SSize x, SSize y, SSize z )
{
  ZZZImage<Bool> * result;
  if ( x >= 0 )  {
    result = new ZZZImage<Bool>( _rows, _depth, 1, _bands );
    for ( int b = 0; b < _bands; ++b ) {
      for ( int d = 0; d < _depth; d++ )
        for ( int h = 0; h < _rows; h++ )
          (*result)(h,d,0,b) = (*this)(x,h,d,b);
    }
    return result;
  }
  if ( y >= 0 )  {
    result = new ZZZImage<Bool>( _cols, _depth, 1, _bands );
    for ( int b = 0; b < _bands; ++b ) {
      for ( int d = 0; d < _depth; d++ )
        for ( int w = 0; w < _cols; w++ )
          (*result)(w,d,0,b) = (*this)(w,y,d,b);
    }
    return result;
  }
  if ( z >= 0 )  {
    result = new ZZZImage<Bool>( _cols, _rows, 1, _bands );
    for ( int b = 0; b < _bands; ++b ) {
      for ( int h = 0; h < _rows; h++ )
        for ( int w = 0; w < _cols; w++ )
          (*result)(w,h,0,b) = (*this)(w,h,z,1);
    }
    return result;
  }
  ERROR << "ZZZImage<Bool>::slice(): Wrong arguments";
  return 0;
}

void
ZZZImage<Bool>::insert( SSize x, SSize y, SSize z,
                        AbstractImage * image )
{
  if ( typeid( *image ) != typeid( *this ) ) return;
  ZZZImage<Bool> * im = dynamic_cast< ZZZImage<Bool>* >( image );

  SSize width = im->width();
  SSize height = im->height();
  SSize depth = im->depth();
  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( int b = 0; b < _bands; ++b ) {
    for ( int k = 0; k < depth; k++ )
      for ( int j = 0; j < height; j++ )
        for ( int i = 0; i < height; i++ )
          (*this)(x+i,y+j,z+k,b) = (*im)(i,j,k,b);
  }
}

void
ZZZImage<Bool>::insertSlice( SSize x, SSize y, SSize z,
                             AbstractImage * image )
{
  if ( typeid( *image ) != typeid( *this ) ) return;
  ZZZImage<Bool> * im = dynamic_cast< ZZZImage<Bool>* >( image );
  SSize width = im->width();
  SSize height = im->height();
  if ( x >= 0 ) {
    if ( width > _rows ) width = _rows;
    if ( height > _depth ) height = _depth;
    for ( int b = 0; b < _bands; ++b ) {
      for ( int j = 0; j < height; j++ )
        for ( int i = 0; i < width; i++ )
          (*this)(x,i,j) = (*im)(i,j,0,b);
    }
    return;
  }
  if ( y >= 0 ) {
    if ( width > _cols ) width = _cols;
    if ( height > _depth ) height = _depth;
    for ( int b = 0; b < _bands; ++b ) {
      for ( int j = 0; j < height; j++ )
        for ( int i = 0; i < width; i++ )
          (*this)(i,y,j,b) = (*im)(i,j,0,b);
    }
    return;
  }
  if ( z >= 0 ) {
    if ( width > _cols ) width = _cols;
    if ( height > _rows ) height = _rows;
    for ( int b = 0; b < _bands; ++b ) {
      for ( int j = 0; j < height; j++ )
        for ( int i = 0; i < width; i++ )
          (*this)(i,j,z,b) = (*im)(i,j,0,b);
    }
    return;
  }
}

Bool
ZZZImage<Bool>::gray( const Triple<SSize> & t )
{
  return !zero( t.first, t.second, t.third );
}

Bool
ZZZImage<Bool>::gray( const Pair<SSize> & t )
{
  return !zero( t.first, t.second, 0 );
}

Bool
ZZZImage<Bool>::gray( SSize x, SSize y, SSize z )
{
  return !zero( x, y, z );
}

void
ZZZImage<Bool>::insert( const ZZZImage<Bool> & other, SSize x, SSize y, SSize z )
{
  SSize width = other._cols;
  SSize height = other._rows;
  SSize depth = other._depth;
  if ( x + width > _cols ) width = _cols - x;
  if ( y + height > _rows ) height = _rows - y;
  if ( z + depth > _depth ) depth = _depth - z;
  for ( int b = 0; b < _bands; ++b )
    for ( int k = 0; k < depth; ++k )
      for ( int j = 0; j < height; ++j )
        for ( int i = 0; i < width; ++i )
          operator()( x+i, y+j, z+k, b ) = other( i, j, k, b ); // TODO
}

bool
ZZZImage<Bool>::setValue( Triple<SSize> voxel,
                          UChar red, UChar green, UChar blue )
{
  Bool formerValue = ! isZero( voxel );
  UChar gray = static_cast<UChar>( ( red*11.0 + green*16.0 + blue*5.0 )/32.0 );
  for ( SSize b = 0; b < _bands; ++b )
    operator()( voxel, b ) = (gray != 0);
  return ( formerValue && !gray ) || ( !formerValue && gray );
}

void
ZZZImage<Bool>::negative()
{
  for ( SSize band = 0; band < _bands; ++band ) {
    UInt *p = _bands_array[band];
    UInt *end = p + _band_size;
    while ( p != end ) {
      *p = ~(*p);
      ++p;
    }
  }
}

void
ZZZImage<Bool>::grayShadeFromHue()
{
  WARNING << "Calling ZZZImage<Bool>::grayShadeFromHue() with T == Bool\n";
}

void
ZZZImage<Bool>::grayShadeFromSaturation()
{
  WARNING << "Calling ZZZImage<Bool>::grayShadeFromSaturation() with T == Bool\n";
}

void
ZZZImage<Bool>::binarize( UChar value )
{
  if ( !value )
    memset( _bands_array[0], 0, _band_size * _bands * sizeof(UInt) );
}

void
ZZZImage<Bool>::binarize( UChar red, UChar green, UChar blue )
{
  if ( !red && !green && !blue )
    memset( _bands_array[0], 0, _band_size * _bands * sizeof(UInt) );

  if ( _bands == 3 ) {
    const bool r = red;
    const bool g = green;
    const bool b = blue;
    for ( SSize x = 0; x < _cols; x++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize z = 0; z < _depth; z++ ) {
          if ( ! zero( x, y, z ) ) {
            operator()( x, y, z, 0 ) = r;
            operator()( x, y, z, 1 ) = g;
            operator()( x, y, z, 2 ) = b;
          }
        }
    return;
  }
}

void
ZZZImage<Bool>::toGray()
{
  if ( _bands < 3 ) return;
  ZZZImage<Bool> res( _cols, _rows, _depth, 1 );
  SSize x,y,z;
  for ( z = 0; z < _depth; z++ )
    for ( x = 0; x < _cols; x++ )
      for ( y = 0; y < _rows; y++ )
        res( x, y, z ) = ! zero( x, y, z );
  res.swapData( *this );
  if ( _depth == 1 ) {
    if ( _rows == 1 ) _poFileType = ImxFileType< Bool, 1 >::type;
    else _poFileType = ImxFileType< Bool, 2 >::type;
  } else _poFileType = ImxFileType< Bool, 3 >::type;
}

void
ZZZImage<Bool>::toColor()
{
  if ( _bands != 1 ) return;
  ZZZImage<Bool> res( _cols, _rows, _depth, 3 );
  SSize x,y,z;
  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ ) {
        const bool v = operator()( x, y, z );
        res( x, y, z, 0 ) = v;
        res( x, y, z, 1 ) = v;
        res( x, y, z, 2 ) = v;
      }
  res.swapData( *this );
}

void
ZZZImage<Bool>::mirror( int axis )
{
  SSize x, y, z;
  SSize limit;
  Bool tmp;
  switch ( axis ) {
  case 0:
    for ( int band = 0; band < _bands ; ++band ) {
      limit = _cols / 2;
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < limit; ++x ) {
            tmp = operator()(x,y,z,band);
            operator()(x,y,z,band) = operator()( (_cols-1)-x, y, z );
            operator()( (_cols-1)-x, y, z ) = tmp;
          }
    }
    break;
  case 1:
    for ( int band = 0; band < _bands ; ++band) {
      limit = _rows / 2;
      for ( x = 0; x < _cols; ++x )
        for ( z = 0; z < _depth; ++z )
          for ( y = 0; y < limit; ++y ) {
            tmp = operator()( x, y, z );
            operator()( x, y, z ) = operator()( x, (_rows-1)-y, z );
            operator()( x, (_rows-1)-y, z ) = tmp;
          }
    }
    break;
  case 2:
    for ( int band = 0; band < _bands ; ++band ) {
      limit = _depth / 2;
      for ( y = 0; y < _rows; ++y )
        for ( x = 0; x < _cols; ++x )
          for ( z = 0; z < limit; ++z ) {
            tmp = operator()( x, y, z );
            operator()( x, y, z ) = operator()( x, y, (_depth-1)-z );
            operator()( x, y, ( _depth - 1 ) - z ) = tmp;
          }
    }
    break;
  }
}

void
ZZZImage<Bool>::rotate( int axis )
{
  SSize x,y,z;
  switch ( axis ) {
  case 0:
  {
    ZZZImage<Bool> result( _cols, _depth, _rows );
    for ( int band = 0; band < _bands ; ++band ) {
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(x,_depth-(z+1),y,band) = operator()( x, y, z );
          }
    }
    swapData( result );
  }
    break;
  case 1:
  {
    ZZZImage<Bool> result( _depth, _rows, _cols );
    for ( int band = 0; band < _bands ; ++band ) {
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(_depth-(z+1),y,x,band) = operator()( x, y, z );
          }
    }
    swapData( result );
  }
    break;
  case 2:
  {
    ZZZImage<Bool> result( _rows, _cols, _depth );
    for ( int band = 0; band < _bands ; ++band ) {
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows ; ++y )
          for ( x = 0; x < _cols; ++x ) {
            result(y,_cols-(x+1),z,band) = operator()( x, y, z );
          }
    }
    swapData( result );
  }
    break;
  }
}

bool
ZZZImage<Bool>::isBorder( SSize x, SSize y, SSize z, char adjacency )
{
  Config c = config( x, y, z );
  c = ( ~c ) & MASK_26_NEIGHBORS;
  switch ( adjacency ) {
  case ADJ26:
  case ADJ18:
    return ( c & MASK_6_NEIGHBORS );
    break;
  case ADJ6:
    return ( c & MASK_26_NEIGHBORS );
    break;
  case ADJ6P:
    return ( c & MASK_18_NEIGHBORS );
    break;
  }
  return false;
}

void
ZZZImage<Bool>::getFramedBitMask( ZZZImage<Bool> & mask ) const
{
  mask.alloc( width()+2, height()+2, depth()+2, 1, true );
  for ( SSize z = 0; z < _depth; ++z )
    for ( SSize y = 0; y < _rows ; ++y )
      for ( SSize x = 0; x < _cols; ++x )
        mask(x+1,y+1,z+1) = nonZero(x,y,z);
}

void
ZZZImage<Bool>::applyFramedBitMask( const ZZZImage<Bool> & mask )
{
  assert( mask.width() == width() + 2 );
  assert( mask.height() == height() + 2 );
  assert( mask.depth() == depth() + 2 );
  for ( SSize z = 0; z < _depth; ++z )
    for ( SSize y = 0; y < _rows ; ++y )
      for ( SSize x = 0; x < _cols; ++x ) {
        if ( ! mask(x+1,y+1,z+1) )
          for ( SSize band = 0; band < _bands; ++band )
            (*this)(x,y,z,band) = false;
      }
}

#define BIT_FROM_OFFSET( offset ) (_bands_array[0][ offset >>5]  & (1ul<<( offset & 0x1Ful)))

Config
ZZZImage<Bool>::config( SSize x, SSize y, SSize z ) const
{
  Config result = 0;
  SSize origin  = (z-1)*(_cols*_rows)+(y-1)*_cols+(x-1);
  SSize index = origin;
  Config mask = 1;
  const int inc_row = _cols - 2;
  const int inc_depth = _rows * _cols;

  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;                     /* _ */
  if ( BIT_FROM_OFFSET( index ) )  result |= mask;
  index = origin + inc_depth; mask <<= 1; /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;            /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index = origin + ( inc_depth << 1 );
  mask <<= 1;			        /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row;
  mask <<= 1;            /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) )  result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  return result;
}

Config
ZZZImage<Bool>::config( const Triple<SSize> & voxel ) const
{
  Config result = 0;
  SSize origin  = (voxel.third-1)*(_cols*_rows)+(voxel.second-1)*_cols+(voxel.first-1);
  SSize index = origin;
  Config mask = 1;
  const int inc_row = _cols - 2;
  const int inc_depth = _rows * _cols;

  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;                     /* _ */
  if ( BIT_FROM_OFFSET( index ) )  result |= mask;
  index = origin + inc_depth; mask <<= 1; /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;            /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index = origin + ( inc_depth << 1 );
  mask <<= 1;			        /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row;
  mask <<= 1;            /* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  index += inc_row; mask <<= 1;		/* _ */
  if ( BIT_FROM_OFFSET( index ) )  result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  ++index; mask <<= 1;			/* _ */
  if ( BIT_FROM_OFFSET( index ) ) result |= mask;
  return result;
}

Config
ZZZImage<Bool>::config2D( SSize x, SSize y, SSize z )
{
  Config cnf = 0;
  if ( operator()( x, y+1, z ) ) cnf |=1;
  if ( operator()( x+1, y+1, z ) ) cnf |=2;
  if ( operator()( x+1, y, z ) ) cnf |=4;
  if ( operator()( x+1, y-1, z ) ) cnf |= 8;
  if ( operator()( x, y-1, z ) ) cnf |= 16;
  if ( operator()( x-1, y-1, z ) ) cnf |= 32;
  if ( operator()( x-1, y, z ) ) cnf |= 64;
  if ( operator()( x-1, y+1, z ) ) cnf |= 128;
  return cnf;
}

/* ATTENTION : Les points marques du bit 2 sont terminaux et donc pas dans P */
void
ZZZImage<Bool>::configExtended( Config & res_conf,
                                Config & res_P,
                                const ZZZImage<Bool> & imP,
                                SSize x, SSize y, SSize z )
{
  SSize origin  = (z-1)*(_cols*_rows)+(y-1)*_cols+(x-1);
  SSize index = origin;
  Config mask = 1;
  const int inc_row = _cols - 2;
  const int inc_depth = _rows * _cols;
  Config conf = 0, border = 0;

  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index = origin + inc_depth; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index = origin + ( inc_depth << 1 ); mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) )  border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }

  res_conf = conf;
  res_P = border;
}

void
ZZZImage<Bool>::configExtended( Config & res_conf,
                                Config & res_P,
                                const ZZZImage<Bool> & imP,
                                const Triple<SSize> & voxel )
{
  SSize origin  = (voxel.third-1)*(_cols*_rows)+(voxel.second-1)*_cols+(voxel.first-1);
  SSize index = origin;
  Config mask = 1;
  const int inc_row = _cols - 2;
  const int inc_depth = _rows * _cols;
  Config conf = 0, border = 0;

  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index;  mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index = origin + inc_depth; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index = origin + ( inc_depth << 1 ); mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) )  border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  index += inc_row; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }
  ++index; mask <<= 1;
  if ( BIT_FROM_OFFSET( index ) ) {
    conf |= mask;
    if ( imP.bit( index ) ) border |= mask;
  }

  res_conf = conf;
  res_P = border;
}

void ZZZImage<Bool>::getColor( SSize x, SSize y, SSize z, UChar & red, UChar & green, UChar & blue )
{
  if ( _colorMode == CastColorMode ) {
    red = 255 * bit(x,y,z,0);
    green = 255 * bit(x,y,z,1);
    blue = 255 * bit(x,y,z,2);
  } else {

  }
}

void ZZZImage<Bool>::getColor( const Triple<SSize> & position, UChar & red, UChar & green, UChar & blue )
{
  if ( _colorMode == CastColorMode ) {
    red = 255 * bit(position,0);
    green = 255 * bit(position,1);
    blue = 255 * bit(position,2);
  } else {

  }
}

void ZZZImage<Bool>::getGrayLevel(SSize x, SSize y, SSize z, UChar & gray )
{
  if ( _colorMode == CastColorMode ) {
    gray = 255 * bit(x,y,z,0);
  } else {

  }
}

void ZZZImage<Bool>::getGrayLevel( const Triple<SSize> & position, UChar & gray )
{
  if ( _colorMode == CastColorMode ) {
    gray = 255 * bit(position,0);
  } else {

  }
}

Triple<UChar> ZZZImage<Bool>::getColorFromLUT(const Triple<SSize> & voxel,
                                              const std::vector< Triple<UChar> > & lut) const
{
  UInt32 value = (*this)(voxel,0);
  return lut[value];
}



void ZZZImage<Bool>::setColorMode(AbstractImage::ColorMode colorMode)
{
  _colorMode = colorMode;
}


void
ZZZImage<Bool>::mark26border( ZZZImage<Bool> & mask )
{
  mask.alloc( size(), 1, true );
  if ( _bands == 1 )
    for ( SSize z = 0; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( bit( x, y, z, 0 ) ) {
            if ( ! bit( x+1, y, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( ! bit( x-1, y, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( ! bit( x, y-1, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( ! bit( x, y+1, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( ! bit( x, y, z-1 ) )
              mask( x, y, z, 0 ) = true;
            else if ( ! bit( x, y, z+1 ) )
              mask( x, y, z, 0 ) = true;
          }
        }
  else
    for ( SSize z = 0; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( bit( x, y, z, 0 ) ) {
            if ( zero( x+1, y, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( zero( x-1, y, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( zero( x, y-1, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( zero( x, y+1, z ) )
              mask( x, y, z, 0 ) = true;
            else if ( zero( x, y, z-1 ) )
              mask( x, y, z, 0 ) = true;
            else if ( zero( x, y, z+1 ) )
              mask( x, y, z, 0 ) = true;
          }
        }
}

void
ZZZImage<Bool>::mark18border( ZZZImage<Bool> & mask )
{
  mark26border( mask );
}

void
ZZZImage<Bool>::mark6border( ZZZImage<Bool> & mask )
{
  SSize x, y, z;
  Config conf;
  mask.alloc( size(), 1, true );
  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ )
        if ( bit( x, y, z ) ) {
          conf = config( x, y, z );
          if ( ( ~conf ) & MASK_26_NEIGHBORS )
            mask( x, y, z ) = true;
        }
}

void
ZZZImage<Bool>::mark6Pborder( ZZZImage<Bool> & mask )
{
  SSize x, y, z;
  Config conf;
  mask.alloc( size(), 1, true );
  for ( z = 0; z < _depth; z++ )
    for ( y = 0; y < _rows; y++ )
      for ( x = 0; x < _cols; x++ )
        if ( bit( x, y, z ) ) {
          conf = config( x, y, z );
          if ( ( ~conf ) & MASK_18_NEIGHBORS )
            mask( x, y, z ) = true;
        }
}

UChar
ZZZImage<Bool>::orientation( SSize x, SSize y, SSize z )
{
  UChar result = 0;
  if ( _bands == 1 ) {
    if ( !bit( x, y-1, z ) ) result |= 1 << NORTH;
    if ( !bit( x, y+1, z ) ) result |= 1 << SOUTH;
    if ( !bit( x+1, y, z ) ) result |= 1 << EAST;
    if ( !bit( x-1, y, z ) ) result |= 1 << WEST;
    if ( !bit( x, y, z+1 ) ) result |= 1 << BEHIND;
    if ( !bit( x, y, z-1 ) ) result |= 1 << BEFORE;
  } else {
    if ( zero( x, y-1, z ) ) result |= 1 << NORTH;
    if ( zero( x, y+1, z ) ) result |= 1 << SOUTH;
    if ( zero( x+1, y, z ) ) result |= 1 << EAST;
    if ( zero( x-1, y, z ) ) result |= 1 << WEST;
    if ( zero( x, y, z+1 ) ) result |= 1 << BEHIND;
    if ( zero( x, y, z-1 ) ) result |= 1 << BEFORE;
  }
  return result;
}

UChar
ZZZImage<Bool>::orientation( const Triple<SSize> & voxel )
{
  UChar result = 0;
  if ( _bands == 1 ) {
    if ( !bit( voxel.first, voxel.second-1, voxel.third ) ) result |= 1 << NORTH;
    if ( !bit( voxel.first, voxel.second+1, voxel.third ) ) result |= 1 << SOUTH;
    if ( !bit( voxel.first+1, voxel.second, voxel.third ) ) result |= 1 << EAST;
    if ( !bit( voxel.first-1, voxel.second, voxel.third ) ) result |= 1 << WEST;
    if ( !bit( voxel.first, voxel.second, voxel.third+1 ) ) result |= 1 << BEHIND;
    if ( !bit( voxel.first, voxel.second, voxel.third-1 ) ) result |= 1 << BEFORE;
  } else {
    if ( zero( voxel.first, voxel.second-1, voxel.third ) ) result |= 1 << NORTH;
    if ( zero( voxel.first, voxel.second+1, voxel.third ) ) result |= 1 << SOUTH;
    if ( zero( voxel.first+1, voxel.second, voxel.third ) ) result |= 1 << EAST;
    if ( zero( voxel.first-1, voxel.second, voxel.third ) ) result |= 1 << WEST;
    if ( zero( voxel.first, voxel.second, voxel.third+1 ) ) result |= 1 << BEHIND;
    if ( zero( voxel.first, voxel.second, voxel.third-1 ) ) result |= 1 << BEFORE;
  }
  return result;
}

void
ZZZImage<Bool>::boundingBox( SSize & xMin, SSize & yMin, SSize & zMin,
                             SSize & xMax, SSize & yMax, SSize & zMax ) const {
  xMin = yMin = zMin = numeric_limits<SSize>::max();
  xMax = yMax = zMax = 0;
  if ( bands() == 1 ) {
    for ( SSize z = 0 ; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( bit( x, y, z ) ) {
            minimize( xMin, x );
            minimize( yMin, y );
            minimize( zMin, z );
            maximize( xMax, x );
            maximize( yMax, y );
            maximize( zMax, z );
          }
        }
  } else {
    for ( SSize z = 0 ; z < _depth; z++ )
      for ( SSize y = 0; y < _rows; y++ )
        for ( SSize x = 0; x < _cols; x++ ) {
          if ( !zero( x, y, z ) ) {
            minimize( xMin, x );
            minimize( yMin, y );
            minimize( zMin, z );
            maximize( xMax, x );
            maximize( yMax, y );
            maximize( zMax, z );
          }
        }
  }
}

void
ZZZImage<Bool>::fitBoundingBox( SSize margin ) {
  SSize xMin, yMin, zMin;
  SSize xMax, yMax, zMax;
  boundingBox( xMin, yMin, zMin, xMax, yMax, zMax );
  ZZZImage<Bool> clone( *this );
  SSize depth = 2 * margin + 1 + zMax - zMin;
  SSize height = 2 * margin + 1 + yMax - yMin;
  SSize width = 2 * margin + 1 + xMax - xMin;
  alloc( width, height, depth, _bands, true );
  for ( SSize band = 0 ; band < _bands; ++band ) {
    for ( SSize z = zMin ; z <= zMax ; z++ )
      for ( SSize y = yMin ; y <= yMax ; y++ )
        for ( SSize x = xMin ; x <= xMax ; x++ ) {
          operator()( margin + x - xMin,
              margin + y - yMin,
              margin + z - zMin, band )
              = clone( x, y, z, band );
        }
  }
}

void
ZZZImage<Bool>::zeroPlane( SSize x, SSize y, SSize z )
{
  if ( x >= 0 ) {
    SSize b,y,z;
    for ( b = 0; b < _bands; ++b ) {
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows; ++y )
          operator()(x,y,z) = false;
    }
  }
  if ( y >= 0 ) {
    SSize b,x,z;
    for ( b = 0; b < _bands; ++b ) {
      for ( z = 0; z < _depth; ++z )
        for ( x = 0; x < _cols; ++x )
          operator()(x,y,z) = false;
    }
  }
  if ( z >= 0 ) {
    SSize b,x,y;
    for ( b = 0; b < _bands; ++b ) {
      for ( y = 0; y < _rows; ++y )
        for ( x = 0; x < _cols; ++x )
          operator()(x,y,z) = false;
    }
  }
}

bool
ZZZImage<Bool>::merge( AbstractImage & other, int axis )
{
  ZZZImage<Bool> * pother = 0;
  try {
    pother = dynamic_cast< ZZZImage<Bool>* >( &other );
  } catch ( std::bad_cast ) {
    ERROR << "ZZZImage<Bool>::merge() : value type mismatch.\n";
    return false;
  }
  return merge( *pother, axis );
}

bool
ZZZImage<Bool>::merge( ZZZImage<Bool> & other, int axis )
{
  if ( bands() != other.bands() ) return false;
  SSize nbands = bands();
  ZZZImage<Bool> left;
  left.swapData( *this );
  SSize leftDepth = left.depth();
  SSize leftHeight = left.height();
  SSize leftWidth = left.width();
  SSize rightDepth = other.depth();
  SSize rightHeight = other.height();
  SSize rightWidth = other.width();
  SSize depth = 0;
  SSize height = 0;
  SSize width = 0;
  SSize dx = 0, dy = 0, dz = 0;

  if ( axis == 0 ) {
    dx = leftWidth;
    depth = ::max( leftDepth, rightDepth );
    height = ::max( leftHeight, rightHeight );
    width = leftWidth + rightWidth;
  }
  if ( axis == 1 ) {
    dy = leftHeight;
    depth = ::max( leftDepth, rightDepth );
    height = leftHeight + rightHeight;
    width = ::max(leftWidth,  rightWidth );
  }
  if ( axis == 2 ) {
    dz = leftDepth;
    depth = leftDepth + rightDepth;
    height = ::max( leftHeight, rightHeight );
    width = ::max(leftWidth,  rightWidth );
  }
  if ( axis == 3 ) {
    depth = ::max( leftDepth, rightDepth );
    height = ::max( leftHeight, rightHeight );
    width = ::max(leftWidth,  rightWidth );
  }

  alloc( width, height, depth, nbands, true );
  for ( SSize band = 0 ; band < nbands; band++ ) {
    for ( SSize z = 0 ; z < leftDepth ; z++ )
      for ( SSize y = 0 ; y < leftHeight ; y++ )
        for ( SSize x = 0 ; x < leftWidth ; x++ )
          operator()( x, y, z, band ) = left( x, y, z, band );
    if ( axis == 3 ) {
      for ( SSize z = 0 ; z < rightDepth ; z++ )
        for ( SSize y = 0 ; y < rightHeight ; y++ )
          for ( SSize x = 0 ; x < rightWidth ; x++ )
            if ( zero( x + dx, y + dy, z + dz ) )
              operator()( x + dx, y + dy, z + dz, band ) = other( x, y, z, band );
    } else {
      for ( SSize z = 0 ; z < rightDepth ; z++ )
        for ( SSize y = 0 ; y < rightHeight ; y++ )
          for ( SSize x = 0 ; x < rightWidth ; x++ )
            operator()( x + dx, y + dy, z + dz, band ) = other( x, y, z, band );
    }
  }
  return true;
}

void
ZZZImage<Bool>::trim( int axis, SSize n )
{
  SSize b = bands();
  ZZZImage<Bool> before;
  before.swapData( *this );
  SSize depth = before.depth();
  SSize height = before.height();
  SSize width = before.width();
  SSize dx = 0, dy = 0, dz = 0;

  if ( axis == 0 ) {
    dx = n;
    width = 1;
  }
  if ( axis == 1 ) {
    dy = n;
    height = 1;
  }
  if ( axis == 2 ) {
    dz = n;
    depth = 1;
  }
  alloc( width, height, depth, b, true );
  for ( SSize band = 0 ; band < b; ++band ) {
    for ( SSize z = 0 ; z < depth ; z++ )
      for ( SSize y = 0 ; y < height ; y++ )
        for ( SSize x = 0 ; x < width ; x++ )
          (*this)( x, y, z, band ) = before( x + dx, y + dy, z + dz, band );
  }
}

void
ZZZImage<Bool>::subSample( UChar minCount ) {
  SSize d = depth();
  SSize h = height();
  SSize w = width();
  SSize nbands = bands();
  ZZZImage<Bool> clone( *this );
  UChar count;

  w >>= 1;
  h >>= 1;
  d >>= 1;
  if ( ! w ) w = 1;
  if ( ! h ) h = 1;
  if ( ! d ) d = 1;
  alloc( w, h, d, nbands, true );

  for ( SSize band = 0 ; band < nbands; band++ ) {
    for ( SSize z = 0 ; z < d ; z++ )
      for ( SSize y = 0 ; y < h ; y++ )
        for ( SSize x = 0 ; x < w ; x++ ) {
          count = 0;
          count += ( clone.bit( x<<1, y<<1, z<<1, band ) );
          count += ( clone.bit( (x<<1)+1, y<<1, z<< 1 ) );
          count += ( clone.bit( x<<1, (y<<1)+1, z<<1, band ) );
          count += ( clone.bit( x<<1, y<<1, (z<<1)+1, band ) );
          count += ( clone.bit( (x<<1)+1, (y<<1)+1, z<<1, band ) );
          count += ( clone.bit( x<<1, (y<<1)+1, (z<<1)+1, band ) );
          count += ( clone.bit( (x<<1)+1, y<<1, (z<<1)+1, band ) );
          count += ( clone.bit( (x<<1)+1, (y<<1)+1, (z<<1)+1, band ) );
          if ( count >= minCount )
            operator()( x, y, z, band ) = true;
        }
  }
}

void
ZZZImage<Bool>::scale( UChar direction, UChar factor )
{
  SSize d = depth();
  SSize h = height();
  SSize w = width();
  SSize b = bands();
  ZZZImage<Bool> clone( *this );

  switch ( direction ) {
  case 0: w *= factor; break;
  case 1: h *= factor; break;
  case 2: d *= factor; break;
  case 3: d *= factor; w *= factor; h *= factor; break;
  }
  alloc( w, h, d, b, true );

  for ( SSize band = 0 ; band < b; band++ ) {
    switch ( direction ) {
    case 0:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            operator()( x, y, z, band ) = clone( x/factor, y, z );
      break;
    case 1:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            operator()( x, y, z, band ) = clone( x, y/factor, z );
      break;
    case 2:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            operator()( x, y, z, band ) = clone( x, y, z/factor );
      break;
    case 3:
      for ( SSize z = 0 ; z < d ; z++ )
        for ( SSize y = 0 ; y < h ; y++ )
          for ( SSize x = 0 ; x < w ; x++ )
            operator()( x, y, z, band ) = clone( x/factor, y/factor, z/factor );
      break;
    }
  }
}

void
ZZZImage<Bool>::extrude( SSize d )
{
  SSize h = height();
  SSize w = width();
  SSize b = bands();
  if ( depth() != 1 ) return;
  ZZZImage<Bool> clone( *this );
  alloc( w, h, d, b, true );
  for ( SSize band = 0 ; band < b; band++ ) {
    for ( SSize z = 0 ; z < d ; z++ )
      for ( SSize y = 0 ; y < h ; y++ )
        for ( SSize x = 0 ; x < w ; x++ )
          operator()( x, y, z, band ) = clone( x, y, 0, band );
  }
}


void ZZZImage<Bool>::clamp( const Triple<Int32> & min, const Triple<Int32> & max)
{
}

void ZZZImage<Bool>::clamp( const Triple<UInt32> & min, const Triple<UInt32> & max)
{
}

void ZZZImage<Bool>::clamp( const Triple<Float> & min, const Triple<Float> & max)
{
}

void
ZZZImage<Bool>::spongify()
{
  (*this) = zzzSponge(*this);
}

void
ZZZImage<Bool>::bitPlane( UChar bit, SSize /* band */ )
{
  if ( bit != 0 )
    WARNING << "ZZZImage<Bool>::bitPlane() called with bit parameter different from 0.\n";
}

void
ZZZImage<Bool>::bitPlaneGrayCode( UChar, SSize )
{
  WARNING << "ZZZImage<Bool>::bitPlaneGrayCode() is undefined.\n";
}

void
ZZZImage<Bool>::quantize(UInt32 /* cardinality */ )
{
}

void
ZZZImage<Bool>::swapEndianness()
{
  UInt * p = _bands_array[0];
  UInt * end = p + _bands * _band_size;
  while ( p != end )
    reverseBytes<sizeof(UInt)>( p++ );
}

ZZZImage<Bool> &
ZZZImage<Bool>::mask( const ZZZImage< UChar > & mask )
{
  for ( SSize b = 0; b < _bands; ++b ) {
    UInt * band = _bands_array[b];
    const SSize count = _cols * _rows * _depth;
    UChar * po = mask.dataVector( b );
    SSize index = 0;
    while ( index != count ) {
      if ( ! *po  )
        band[ index >> 5 ] &= ~( 1ul << (index&0x1F)  );
      ++po;
      ++index;
    }
  }
  return *this;
}

ZZZImage<Bool> &
ZZZImage<Bool>::operator&=( const ZZZImage<Bool> & other )
{
  if ( size() != other.size() || _bands != other._bands ) {
    ERROR << "Calling ZZZImage<Bool>::operator&=() on images with different sizes.\n";
    return *this;
  }
  UInt *limit = _bands_array[0] + _bands * _band_size;
  UInt *po = other._bands_array[0] + _bands * _band_size;
  for ( UInt * p = _bands_array[0]; p < limit; ++p, ++po )
    *p &= *po;
  return *this;
}

ZZZImage<Bool> &
ZZZImage<Bool>::operator|=( const ZZZImage<Bool> & other )
{
  if ( size() != other.size() || _bands != other._bands ) {
    ERROR << "Calling ZZZImage<Bool>::operator|=() on images with different sizes.\n";
    return *this;
  }
  UInt *limit = _bands_array[0] + _bands * _band_size;
  UInt *po = other._bands_array[0] + _bands * _band_size;
  for ( UInt * p = _bands_array[0]; p < limit; ++p, ++po )
    *p |= *po;
  return *this;
}

ZZZImage<Bool>
ZZZImage<Bool>::operator|( const ZZZImage<Bool> & other )
{
  ZZZImage<Bool> res(*this);
  res|=other;
  return res;
}

ZZZImage<Bool> &
ZZZImage<Bool>::operator-=( const ZZZImage<Bool> & other )
{
  if ( size() != other.size() || _bands != other._bands ) {
    ERROR << "Calling ZZZImage<Bool>::operator-=() on images with different sizes.\n";
    return *this;
  }
  UInt *limit = _bands_array[0] + _bands * _band_size;
  UInt *po = other._bands_array[0] + _bands * _band_size;
  for ( UInt * p = _bands_array[0]; p < limit; ++p, ++po )
    *p ^= *po;
  return *this;
}

void
ZZZImage<Bool>::hollowOut( int adjacency )
{
  ZZZImage<Bool> im( *this );
  Config c;
  if ( _bands == 1 )
    switch (adjacency) {
    case ADJ26:
    case ADJ18:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( im.bit(x,y,z)
                 && im.bit(x+1,y,z) && im.bit(x-1,y,z)
                 && im.bit(x,y+1,z) && im.bit(x,y-1,z)
                 && im.bit(x,y,z+1) && im.bit(x,y,z-1) ) {
              (*this)(x,y,z) = false;
            }
          }
      break;
    case ADJ6:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( im.bit(x,y,z) ) {
              c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
              if ( !( c & MASK_26_NEIGHBORS ) )
                (*this)(x,y,z) = false;
            }
          }
      break;
    case ADJ6P:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( im.bit(x,y,z) ) {
              c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
              if ( !(c & MASK_18_NEIGHBORS) )
                (*this)(x,y,z) = false;
            }
          }
      break;
    }
  else {
    SSize b;
    switch (adjacency) {
    case ADJ26:
    case ADJ18:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( !im.zero(x,y,z)
                 && !im.zero(x+1,y,z) && !im.zero(x-1,y,z)
                 && !im.zero(x,y+1,z) && !im.zero(x,y-1,z)
                 && !im.zero(x,y,z+1) && !im.zero(x,y,z-1) ) {
              for (b=0;b<_bands;++b) (*this)(x,y,z,b) = false;
            }
          }
      break;
    case ADJ6:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( im.bit(x,y,z) ) {
              c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
              if ( !( c & MASK_26_NEIGHBORS ) )
                for (b=0;b<_bands;++b) (*this)(x,y,z,b) = false;
            }
          }
      break;
    case ADJ6P:
      for ( SSize z = 0; z < _depth; ++z )
        for ( SSize y = 0; y < _rows; ++y )
          for ( SSize x = 0; x < _cols; ++x ) {
            if ( im.bit(x,y,z) ) {
              c = ~(im.config( x, y, z )) & MASK_26_NEIGHBORS;
              if ( !(c & MASK_18_NEIGHBORS) )
                for (b=0;b<_bands;++b) (*this)(x,y,z,b) = false;
            }
          }
      break;
    }
  }
}

/**
 *
 * 3D filling algorithm from:
 *   Lin Feng and Seah Hock Soon, "An effective 3D seed fill algorithm",
 *   Computers & Graphics, Volume 22, Issue 5, October 1998, Pages 641-644
 *
 * @param value
 */
void
ZZZImage<Bool>::seedFill( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( ! isZero( triple( seed ) ) )
    return;

  const Triple<Bool> fillColor( value, value, value );

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    triple(seed) = fillColor;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( isZero( triple( seed ) ) && holds( seed ) ) {
      triple(seed) = fillColor;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( isZero( triple( seed ) ) && holds( seed ) ) {
      triple(seed) = fillColor;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary nor the previously completely
    // filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Z+1
    // check the top scan line with 'z+1'
    ++seed.second;
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple(seed) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( isZero( triple(seed) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

void
ZZZImage<Bool>::seedFillXY( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary nor the previously completely
    // filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

void
ZZZImage<Bool>::seedFillXZ( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Z+1
    // check the top scan line with 'z+1'
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

void
ZZZImage<Bool>::seedFillYZ( SSize x, SSize y, SSize z, UChar value )
{
  Triple<SSize> seed( x, y, z);
  if ( (*this)( seed ) )
    return;

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize ysave, yleft, yright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    (*this)(seed) = value;

    // save the x coordinate of the seed
    ysave = seed.first;
    // fill the right span of the seed
    ++seed.second;
    while ( !(*this)(seed)  && holds( seed ) ) {
      (*this)(seed) = value;
      ++seed.second;
    }

    // save the x coordinate of the extreme right voxel
    yright = seed.second - 1;
    // reset the x coordinate to that of the seed
    seed.second = ysave;
    // fill the left span of the seed
    --seed.second;
    while ( !(*this)(seed) && holds( seed ) ) {
      (*this)(seed) = value;
      --seed.second;
    }

    // save the x coordinate of the extreme left voxel
    yleft = seed.second + 1;

    // Z+1
    // check the top scan line with 'z+1'
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.second = yleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.first;
    while ( seed.second < yright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first,seed.second-1,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.second;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first, seed.second-1, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.second = yleft;
    // store the status of the first voxel
    firstVoxelStatus = ( !(*this)(seed) && holds( seed ) );
    ++seed.second;
    while ( seed.second < yright ) {
      secondVoxelStatus=( !(*this)(seed) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first,seed.second-1,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.second;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first, seed.second-1, seed.third ) );
  }
}

/**
 *
 * 3D filling algorithm from:
 *   Lin Feng and Seah Hock Soon, "An effective 3D seed fill algorithm",
 *   Computers & Graphics, Volume 22, Issue 5, October 1998, Pages 641-644
 *
 * @param value
 */
void
ZZZImage<Bool>::surfaceFill( UChar /* value */ )
{
  const Triple<Bool> fillColor(true,true,true);
  Triple<SSize> seed;

  bool found = false;
  for ( SSize z = 0; z < _depth && !found; ++z )
    for ( SSize y = 0; y < _rows && !found; ++y )
      for ( SSize x = 0; x < _cols && !found; ++x ) {
        if ( isZero( triple( x, y, z ) ) ) {
          seed.set( x, y, z );
          found = true;
        }
      }

  stack< Triple<SSize> > stk;
  stk.push(seed);
  SSize xsave, xleft, xright;
  bool firstVoxelStatus, secondVoxelStatus;
  while ( ! stk.empty() ) {
    seed = stk.top();
    stk.pop();
    triple(seed) = fillColor;    //(*this)(seed) = 2;

    // save the x coordinate of the seed
    xsave = seed.first;
    // fill the right span of the seed
    ++seed.first;
    while ( isZero( triple(seed) ) && holds( seed ) ) {
      triple(seed) = fillColor; // (*this)(seed) = 2;
      ++seed.first;
    }

    // save the x coordinate of the extreme right voxel
    xright = seed.first - 1;
    // reset the x coordinate to that of the seed
    seed.first = xsave;
    // fill the left span of the seed
    --seed.first;
    while ( isZero( triple(seed) )   && holds( seed ) ) {
      triple(seed) = fillColor;
      --seed.first;
    }

    // save the x coordinate of the extreme left voxel
    xleft = seed.first + 1;

    // Y+1
    // check that the front scan line with 'y+1' is neither a boundary nor the previously completely
    // filled one; if not, seed the scan line
    ++seed.second;
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Y-1
    // check the back scan line with 'y-1'
    seed.second -= 2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );


    // Z+1
    // check the top scan line with 'z+1'
    ++seed.second;
    ++seed.third;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus )
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );

    // Z-1
    // check the bottom scan line with 'z-1'
    seed.third -=2;
    // do the same as that of 'y+1'
    // start at the left extreme of the scan line
    seed.first = xleft;
    // store the status of the first voxel
    firstVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
    ++seed.first;
    while ( seed.first < xright ) {
      secondVoxelStatus = ( isZero( triple( seed ) ) && holds( seed ) );
      // find the boundary between inside and outside voxels
      if ( ( firstVoxelStatus != secondVoxelStatus ) && firstVoxelStatus)
        stk.push( Triple<SSize>(seed.first-1,seed.second,seed.third) );
      firstVoxelStatus = secondVoxelStatus;
      ++seed.first;
    }
    // check the last voxel
    if ( firstVoxelStatus )
      stk.push( Triple<SSize>( seed.first-1, seed.second, seed.third ) );
  }
}

void
ZZZImage<Bool>::addBoundaries( UChar value, UChar thickness )
{
  SSize x = 0, y = 0, z = 0, band = 0;
  Bool bValue = value;
  if ( thickness == 1 ) {
    for ( band = 0; band < _bands; ++band ) {
      for ( z = 0; z < _depth; ++z ) {
        operator()(0,0,z) = bValue;
        operator()(_cols-1, 0, z ) = bValue;
        operator()(0, _rows-1, z ) = bValue;
        operator()(_cols-1, _rows-1, z ) = bValue;
      }
      for ( x = 0; x < _cols; ++x ) {
        operator()( x, 0, 0 ) = bValue;
        operator()( x, 0, _depth-1 ) = bValue;
        operator()( x, _rows-1, 0 ) = bValue;
        operator()( x, _rows-1, _depth-1 ) = bValue;
      }
      for ( y = 0; y < _rows; ++y ) {
        operator()( 0, y, 0 ) = bValue;
        operator()( 0, y, _depth-1 ) = bValue;
        operator()( _cols-1, y, 0 ) = bValue;
        operator()( _cols-1, y, _depth-1 ) = bValue;
      }
    }
  } else {
    for ( band = 0; band < _bands; ++band ) {
      for ( z = 0; z < _depth; ++z )
        for ( y = 0; y < _rows; ++y )
          for ( x = 0; x < _cols; ++x ) {
            if  ( ( ( x < thickness
                      || ( x + thickness >= _cols )
                      || y < thickness
                      || ( y + thickness >= _rows ) )
                    && ( z < thickness || ( z + thickness ) >= _depth ) )
                  ||
                  ( ( z < thickness
                      || ( z + thickness >= _depth )
                      || y < thickness
                      || ( y + thickness >= _rows ) )
                    && ( x < thickness || ( x + thickness ) >= _cols ) ) )
              operator()( x, y, z ) = bValue;
          }
    }
  }
}

void
ZZZImage<Bool>::histogram( std::vector<Size> & h, Bool & minVal, Bool & maxVal, int band )
{
  if ( h.size() < 2 )
    return;
  memset( &(h.front()), 0, h.size() * sizeof(SSize) );
  SSize index = 0;
  SSize limit = _cols * _rows * _depth;

  minVal=false;
  maxVal=true;
  for ( index = 0; index < limit && bit( index, 0, 0, band ); ++index ) ;
  if ( index == limit ) minVal = true;
  for ( index = 0; index < limit && !bit( index, 0, 0, band ); ++index ) ;
  if ( index == limit ) maxVal = false;

  for ( index = 0;  index < limit; ++index ) {
    if ( bit( index, 0, 0, band ) )
      h[ h.size() - 1 ]++;
    else
      h[ 0 ]++;
  }
}

template< typename U >
ZZZImage<Bool> &
ZZZImage<Bool>::operator=( const ZZZImage< U > & src )
{
  if ( size() != src.size() || _bands != src.bands() ) {
    alloc( src.size(), src.bands(), false );
  }
  for ( int band = 0; band < _bands; ++band ) {
    for ( SSize z = 0; z < _depth; ++z )
      for ( SSize y = 0; y < _rows; ++y )
        for ( SSize x = 0; x < _cols; ++x ) {
          (*this)(x,y,z,band) = static_cast<Bool>( src(x,y,z,band) );
        }
  }
  return *this;
}

void
ZZZImage<Bool>::accept( AbstractImageVisitor & visitor )
{
  visitor.visit(*this);
}



template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<UChar> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<Short> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<Int32> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<UInt32> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<Float> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<Double> & src );
template ZZZImage<Bool> & ZZZImage<Bool>::operator=( const ZZZImage<UShort> & src );


const ZZZImage<Bool> ZZZImage<Bool>::null;

template class ZZZImage<Bool>;
