/**
 * @file   distance_transform.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:18 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "distance_transform.h"

/*
 * This file is an almost verbatim copy of the code written
 * by David Coeurjolly, put on the Code repository of the
 * IAPR TC18's website.
 * http://www.cb.uu.se/~tc18/code_data_set/code.html
 *
 * The Euclidean distance tranform is computed in linear time by the function
 * below. This is thanks to the results of the following papers:
 *
 *  [ST94] T. Saito and J. I. Toriwaki. New algorithms for Euclidean distance
 *  transformations of an /n/-dimensional digitized picture with applications.
 *  Pattern Recognition, 27:1551-1565, 1994.
 *
 *  [MRH00] A. Meijster, J.B.T.M. Roerdink, and W. H. Hesselink.
 *  A general algorithm for computing distance transforms in linear time.
 *  In Mathematical Morphology and its Applications to Image and Signal Processing,
 *  pages 331-340. Kluwer, 2000.
 *
 *  [Hir96] T. Hirata. A unified linear-time algorithm for computing distance maps.
 *  /Information Processing Letters/, 58(3):129-133, May 1996.
 *
 */

static const Int32 Infinity = std::numeric_limits<Int32>::max();

inline Int32 sum( Int32 a, Int32 b )
{
  if ( (a == Infinity) || ( b == Infinity ) ) return Infinity;
  return a + b;
}

inline Int32 prod( Int32 a, Int32 b )
{
  if ( ( a == Infinity ) || ( b == Infinity ) ) return Infinity;
  return a * b;
}

inline Int32 opposite( Int32 a ) {
  if ( a == Infinity ) return Infinity;
  return -a;
}

inline Int32 divide( Int32 a, Int32 b ) {
  if ( b == 0 ) return  Infinity;
  if ( a == Infinity ) return  Infinity;
  return  a / b;
}

inline Int32 F( int x, int i, Int32 gi2 )
{
  return sum( (x-i)*(x-i), gi2 );
}

Int32 sep( int i, int u, Int32 gi2, Int32 gu2 ) {
  return divide( sum( sum( static_cast<Int32>( u*u - i*i ), gu2),
          opposite( gi2 ) ),
     2 * ( u - i ) );
}

template< typename T >
void
saitoX( const ZZZImage<T> & V, ZZZImage<Int32> & edt_x )
{
  Int32 ***distance = edt_x.data( 0 );
  T ***volume = V.data(0);
  SSize depth = V.depth();
  SSize width = V.width();
  SSize height = V.height();
  for ( int z = 0; z < depth ; z++ )
    for ( int y = 0; y < height ; y++ ) {
      if ( volume[z][y][0] == 0) distance[z][y][0] = 0;
      else  distance[z][y][0] = Infinity;

      // Forward scan
      for ( int x = 1; x < width ; x++)
  if ( volume[z][y][x] == 0) distance[z][y][x] = 0;
  else distance[z][y][x] = sum( 1, distance[z][y][x-1] );

      //Backward scan
      for ( int x = width - 2; x >= 0; x-- )
  if ( distance[z][y][x+1] < distance[z][y][x] )
    distance[z][y][x] = sum( 1, distance[z][y][x+1] );
    }
}


void
saitoY( const ZZZImage<Int32> & sdt_x, ZZZImage<Int32> & sdt_xy )
{
  SSize depth = sdt_x.depth();
  SSize width = sdt_x.width();
  SSize height = sdt_x.height();
  int * s;
  int * t;
  int q, w;
  Int32 ***mx = sdt_x.data( 0 );
  Int32 ***mxy = sdt_xy.data( 0 );

  s = new int [ height ];
  t = new int [ height ];

  for ( int z = 0; z < depth; z++)
    for ( int x = 0; x < width; x++) {
      s[0] = t[0] = q = 0;
      //Forward Scan
      for (int u = 1; u < height ; u++) {
  while ( (q >= 0) &&
    ( F( t[q], s[q], prod( mx[z][s[q]][x], mx[z][s[q]][x] ) ) >
      F( t[q], u, prod( mx[z][u][x], mx[z][u][x] ) ) ) ) q--;

  if (q<0) { q=0; s[0]=u; }
  else {
    w = 1 + sep( s[q], u,
           prod( mx[z][s[q]][x], mx[z][s[q]][x] ),
           prod( mx[z][u][x], mx[z][u][x] ) );
    if ( w < height ) {
      q++; s[q]=u; t[q]=w;
    }
  }
      }

      //Backward Scan
      for ( int u = height - 1; u >= 0; --u ) {
  mxy[z][u][x] = F( u, s[q],
        prod( mx[z][s[q]][x], mx[z][s[q]][x] ) );
  if ( u == t[q] ) q--;
      }
    }
  disposeArray( s );
  disposeArray( t );
}

void
saitoZ( const ZZZImage<Int32> & sdt_xy, ZZZImage<Int32> & sdt_xyz )
{
  SSize depth = sdt_xy.depth();
  SSize width = sdt_xy.width();
  SSize height = sdt_xy.height();
  int *s;
  int *t;
  int q, w;
  Int32 ***mxy = sdt_xy.data( 0 );
  Int32 ***mxyz = sdt_xyz.data( 0 );

  s = new int[ depth ];
  t = new int[ depth ];

  for ( int y = 0; y < height; y++)
    for ( int x = 0; x < width; x++) {
      s[0] = t[0] = q = 0;
      //Forward Scan
      for ( int u = 1; u < depth ; u++) {
  while ( (q >= 0) &&
    ( F( t[q], s[q], mxy[s[q]][y][x] ) >
      F( t[q], u, mxy[u][y][x] ) ) ) q--;
  if (q<0) { q=0; s[0]=u; }
  else {
    w = 1 + sep( s[q], u, mxy[s[q]][y][x], mxy[u][y][x] );
    if ( w < depth ) {
      q++; s[q]=u; t[q]=w;
    }
  }
      }
      //Backward Scan
      for ( int u = depth - 1; u >= 0; --u ) {
  mxyz[u][y][x] = F( u, s[q], mxy[s[q]][y][x] );
  if ( u == t[q] ) q--;
      }
    }
  disposeArray( s );
  disposeArray( t );
}

template<typename T>
void
distanceTransform( const ZZZImage< T > & image, ZZZImage< Int32 > & distMap )
{
  SSize width = image.width();
  SSize height = image.height();
  SSize depth = image.depth();
  // distMap.alloc( width, height, depth, 1 );
  ZZZImage<Int32> sdt_xy( width, height, depth, 1 );
  saitoX( image, distMap );
  saitoY( distMap, sdt_xy );
  saitoZ( sdt_xy, distMap );
}


template< typename T >
void
saitoXNoBorder( const ZZZImage<T> & V, ZZZImage<Int32> & edt_x )
{
  Int32 ***distance = edt_x.data( 0 );
  T ***volume = V.data(0);
  SSize depth = V.depth();
  SSize width = V.width();
  SSize height = V.height();
  for ( int z = 0; z < depth ; z++ )
    for ( int y = 0; y < height ; y++ ) {
      if ( volume[z][y][0] == 0) distance[z][y][0] = 0;
      else  distance[z][y][0] = Infinity;
      // Forward scan
      for ( int x = 1; x < width ; x++)
  if ( volume[z][y][x] == 0) distance[z][y][x] = 0;
  else distance[z][y][x] = sum( 1, distance[z][y][x-1] );
      //Backward scan
      for ( int x = width - 2; x >= 0; x-- )
  if ( distance[z][y][x+1] < distance[z][y][x] )
    distance[z][y][x] = sum( 1, distance[z][y][x+1] );
    }
}

void
saitoYNoBorder( const ZZZImage<Int32> & sdt_x, ZZZImage<Int32> & sdt_xy )
{
  SSize depth = sdt_x.depth();
  SSize width = sdt_x.width();
  SSize height = sdt_x.height();
  int * s;
  int * t;
  int q, w;
  Int32 ***mx = sdt_x.data( 0 );
  Int32 ***mxy = sdt_xy.data( 0 );

  s = new int[ height ];
  t = new int[ height ];

  for ( int z = 0; z < depth; ++z )
    for ( int x = 0; x < width; ++x ) {
      s[0] = t[0] = q = 0;
      //Forward Scan
      for (int u = 1; u < height ; ++u ) {
  while ( (q >= 0) &&
    ( F( t[q], s[q], prod( mx[z][s[q]][x], mx[z][s[q]][x] ) ) >
      F( t[q], u, prod( mx[z][u][x], mx[z][u][x] ) ) ) ) q--;

  if ( q < 0 ) s[ q=0 ] = u;
  else {
    w = 1 + sep( s[q], u,
           prod( mx[z][s[q]][x], mx[z][s[q]][x] ),
           prod( mx[z][u][x], mx[z][u][x] ) );
    if ( w < height ) { q++; s[ q ] = u; t[ q ] = w; }
  }
      }
      //Backward Scan
      for ( int u = height - 1; u >= 0; --u ) {
  mxy[z][u][x] = F( u, s[q],
        prod( mx[z][s[q]][x], mx[z][s[q]][x] ) );
  if ( u == t[q] ) --q;
      }
    }
  disposeArray( s );
  disposeArray( t );
}

void
saitoZNoBorder( const ZZZImage<Int32> & sdt_xy, ZZZImage<Int32> & sdt_xyz )
{
  SSize depth = sdt_xy.depth();
  SSize width = sdt_xy.width();
  SSize height = sdt_xy.height();
  int *s;
  int *t;
  int q, w;
  Int32 ***mxy = sdt_xy.data( 0 );
  Int32 ***mxyz = sdt_xyz.data( 0 );

  s = new int[ depth ];
  t = new int[ depth ];

  for ( int y = 0; y < height; y++)
    for ( int x = 0; x < width; x++) {
      s[0] = t[0] = q = 0;
      //Forward Scan
      for ( int u = 1; u < depth ; u++) {
  while ( (q >= 0) &&
    ( F( t[q], s[q], mxy[s[q]][y][x] ) >
      F( t[q], u, mxy[u][y][x] ) ) ) q--;
  if (q<0) s[ q = 0 ] = u;
  else {
    w = 1 + sep( s[q], u, mxy[s[q]][y][x], mxy[u][y][x] );
    if ( w < depth ) {
      ++q; s[q] = u; t[q] = w;
    }
  }
      }
      //Backward Scan
      for ( int u = depth - 1; u >= 0; --u ) {
  mxyz[u][y][x] = F( u, s[q], mxy[s[q]][y][x] );
  if ( u == t[q] ) --q;
      }
    }
  disposeArray( s );
  disposeArray( t );
}

template<typename T>
void
distanceTransformNoBorder( const ZZZImage< T > & image,
                           ZZZImage< Int32 > & distMap )
{
  SSize width = image.width();
  SSize height = image.height();
  SSize depth = image.depth();
  ZZZImage<Int32> sdt_xy( width, height, depth, 1 );
  saitoXNoBorder( image, distMap );
  saitoYNoBorder( distMap, sdt_xy );
  saitoZNoBorder( sdt_xy, distMap );
}

#define INSTANCIATE( TYPENAME ) \
template void distanceTransform( const ZZZImage< TYPENAME > & , \
                                 ZZZImage< Int32 > &  ); \
template void distanceTransformNoBorder( const ZZZImage< TYPENAME > & ,\
                                         ZZZImage< Int32 > &  );

INSTANCIATE( UChar )
INSTANCIATE( Short )
INSTANCIATE( Int32 )
INSTANCIATE( UInt32 )
INSTANCIATE( Float )
