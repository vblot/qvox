/**
 * @file   genesis.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:35 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "genesis.h"
#include "tools.h"

Rotation::Rotation()
{
  memset( this, 0, sizeof(Rotation) );
  valid = false;
}

Rotation::Rotation( Triple<double> center, double rho, double theta, double phi )
{
  double s_rho,c_rho,s_theta,c_theta,s_phi,c_phi;
  valid = true;
  Rotation::center = center;
  Rotation::rho = rho;
  Rotation::theta = theta;
  Rotation::phi = phi;

  s_rho   = sine_rho = sin(rho);       c_rho = cosine_rho = cos(rho);
  s_theta = sine_theta = sin(theta);  c_theta = cosine_theta = cos(theta);
  s_phi   = sine_phi = sin(phi);      c_phi = cosine_phi = cos(phi);

  mRotation.a11 = c_theta*c_phi;
  mRotation.a12 = s_rho*s_theta*c_phi - c_rho*s_phi;
  mRotation.a13 = s_rho*s_phi + c_rho*s_theta*c_phi;

  mRotation.a21 = c_theta*s_phi;
  mRotation.a22 = c_rho*c_phi + s_rho*s_theta*s_phi;
  mRotation.a23 = c_rho*s_theta*s_phi - s_rho*c_phi;

  mRotation.a31 = -s_theta;
  mRotation.a32 = s_rho*c_theta;
  mRotation.a33 = c_rho*c_theta;

  mInverseRotation.a11 = c_theta*c_phi;
  mInverseRotation.a12 = c_theta*s_phi;
  mInverseRotation.a13 = -s_theta;

  mInverseRotation.a21 = s_rho*s_theta*c_phi - c_rho*s_phi;
  mInverseRotation.a22 = c_rho*c_phi + s_rho*s_theta*s_phi;
  mInverseRotation.a23 = s_rho*c_theta;

  mInverseRotation.a31 = s_rho*s_phi + c_rho*s_theta*c_phi;
  mInverseRotation.a32 = c_rho*s_theta*s_phi - s_rho*c_phi;
  mInverseRotation.a33 = c_rho*c_theta;
}

void
Rotation::set( Triple<double> center, double rho, double theta, double phi )
{
  double s_rho,c_rho,s_theta,c_theta,s_phi,c_phi;

  valid = true;
  Rotation::center = center;
  Rotation::rho = rho;
  Rotation::theta = theta;
  Rotation::phi = phi;

  s_rho   = sine_rho = sin(rho);       c_rho = cosine_rho = cos(rho);
  s_theta = sine_theta = sin(theta);  c_theta = cosine_theta = cos(theta);
  s_phi   = sine_phi = sin(phi);      c_phi = cosine_phi = cos(phi);

  mRotation.a11 = c_theta*c_phi;
  mRotation.a12 = s_rho*s_theta*c_phi - c_rho*s_phi;
  mRotation.a13 = s_rho*s_phi + c_rho*s_theta*c_phi;

  mRotation.a21 = c_theta*s_phi;
  mRotation.a22 = c_rho*c_phi + s_rho*s_theta*s_phi;
  mRotation.a23 = c_rho*s_theta*s_phi - s_rho*c_phi;

  mRotation.a31 = -s_theta;
  mRotation.a32 = s_rho*c_theta;
  mRotation.a33 = c_rho*c_theta;

  mInverseRotation.a11 = c_theta*c_phi;
  mInverseRotation.a12 = c_theta*s_phi;
  mInverseRotation.a13 = -s_theta;

  mInverseRotation.a21 = s_rho*s_theta*c_phi - c_rho*s_phi;
  mInverseRotation.a22 = c_rho*c_phi + s_rho*s_theta*s_phi;
  mInverseRotation.a23 = s_rho*c_theta;

  mInverseRotation.a31 = s_rho*s_phi + c_rho*s_theta*c_phi;
  mInverseRotation.a32 = c_rho*s_theta*s_phi - s_rho*c_phi;
  mInverseRotation.a33 = c_rho*c_theta;
}

Triple<double>
Rotation::apply( Triple<double> point )
{
  if ( valid )
    return center + ( mRotation * (point - center) );
  else
    return point;
}

Triple<double>
Rotation::reverse( Triple<double> point )
{
  if ( valid )
    return center + ( mInverseRotation * (point - center) );
  else
    return point;
}

void
Rotation::clear()
{
  set( Triple<double>( 0,0,0 ), 0, 0, 0 );
}

template< typename T >
void
genesisSphere(ZZZImage<T> & image,
              Triple<SSize> center, SSize radius,
              T value,
              Triple<double> scale )
{
  SSize depth, width, height, bands;
  SSize i,j,k;

  image.getDimension( width, height, depth, bands );
  double x,y,z;
  // double r = radius*(radius+1);
  double r = radius*radius;

  T *** matrix = image.data( 0 );
  for ( i = 0; i < width; i++ )
    for ( j = 0 ; j < height ; j++ )
      for ( k = 0 ; k < depth ; k++ ) {
  x = ( ( i - center.first ) / scale.first );
  y = ( ( j - center.second ) / scale.second );
  z = ( ( k - center.third ) / scale.third );
  if ( x*x + y*y + z*z <= r )
          matrix[ k ][ j ][ i ] = value;
      }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}


template< typename T >
void genesisEllipsoid( ZZZImage< T > & image,
           Triple<SSize> center,
           Triple<SSize> radii,
           T value,
           Rotation rotation,
           Triple<double> scale )
{
  SSize depth, width, height, bands;
  SSize i,j,k;
  image.getDimension( width, height, depth, bands );
  double x,y,z;
  Triple<double> point;

  for ( i = 0 ; i < width ; i++)
    for ( j = 0 ; j < height ; j++)
        for ( k = 0 ; k < depth ; k++) {
          point.first = i;
    point.second = j;
    point.third = k;
    point = rotation.reverse( point );
    x = ( ( point.first - center.first ) / scale.first );
    y = ( ( point.second - center.second ) / scale.second );
    z = ( ( point.third - center.third ) / scale.third );

    if ( sqrt( fsquare( x / radii.first)
         + fsquare( y / radii.second )
         + fsquare( z / radii.third ) )  <= 1 ) {
      image( i, j, k ) = value;
    }
        }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}



template< typename T >
void
genesisHyperbolicParaboloid( ZZZImage<T> & image,
           Triple<SSize> center,
           double a,
           double b,
           T value,
           Rotation rotation,
           Triple<double> scale )
{
  SSize depth, width, height, bands;
  SSize i,j,k;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  Triple<double> fcenter = center;
  for ( i = 0 ; i < width ; i++)
    for ( j = 0 ; j< height ; j++)
      for ( k = 0 ; k < depth ; k++) {
  point.first = i;
  point.second = j;
  point.third = k;
  point = rotation.reverse( point );
  point -= fcenter;
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;
  point += Triple<double>( 0.2, 0.1, -0.33 );
  if ( point.third <= fsquare(point.first/b) - fsquare(point.second/a) ) {
    image( i, j, k ) = value;
  }
      }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}

template< typename T >
void
genesisParaboloid( ZZZImage<T> & image,
       Triple<SSize> center,
       double a,
       double b,
       T value,
       Rotation rotation,
       Triple<double> scale )
{
  SSize depth, width, height, bands;
  SSize i,j,k;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  Triple<double> fcenter = center;
  for ( i = 0 ; i < width ; i++)
    for ( j = 0 ; j< height ; j++)
      for ( k = 0 ; k < depth ; k++) {
  point.first = i;
  point.second = j;
  point.third = k;
  point = rotation.reverse( point );
  point -= fcenter;
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;
  if ( point.third <= fsquare(point.second/b) + fsquare(point.first/a) )  {
    image( i, j, k ) = value;
  }
      }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}


template< typename T >
void
genesisPlane( ZZZImage<T> & image,
        Triple<SSize> center,
        Triple<double> normal,
        T value,
        Rotation rotation )
{
  SSize depth, width, height, bands;
  SSize i,j,k;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  Triple<double> fcenter = center;
  for ( i = 0 ; i < width ; i++)
    for ( j = 0 ; j< height ; j++)
      for ( k = 0 ; k < depth ; k++) {
  point.first = i;
  point.second = j;
  point.third = k;
  point = rotation.reverse( point );
  point -= fcenter;
  if ( point * normal <= 0 )  {
    image( i, j, k ) = value;
  }
      }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}


template< typename T >
void
genesisTorus( ZZZImage<T> & image,
        Triple<SSize> center,
        SSize largeRadius,
        SSize smallRadius,
        T value,
        Rotation rotation,
        Triple<double> scale )
{
  SSize depth, width, height, bands;
  SSize i,j,k;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  Triple<double> fcenter = center;
  for ( i = 0 ; i < width ; i++)
    for ( j = 0 ; j< height ; j++)
      for ( k = 0 ; k < depth ; k++) {
  point.first = i;
  point.second = j;
  point.third = k;
  point = rotation.reverse( point );
  point -= fcenter;
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;

  if ( dsquare( point.third )
       + dsquare( largeRadius
                  - sqrt( dsquare(point.first) + dsquare(point.second) ) )
       < smallRadius * (smallRadius+1) )
    image( i, j, k ) = value;

      }
  if ( bands > 1 ) {
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}


template <typename T>
void
genesisBox( ZZZImage<T> & image,
      Triple<SSize> p1,
      Triple<SSize> p2,
      T value,
      Rotation rotation,
      Triple<double> scale,
      Triple<SSize> translation ) {

  if ( p1.first > p2.first )
    swapValues( p1.first, p2.first );
  if ( p1.second > p2.second )
    swapValues( p1.second, p2.second );
  if ( p1.third > p2.third )
    swapValues( p1.third, p2.third );

  Triple<double> p1f = p1;
  Triple<double> p2f = p2;

  SSize depth, width, height, bands;
  Triple<SSize> p;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  for ( p.first = 0 ; p.first < width ; p.first++)
    for ( p.second = 0 ; p.second< height ; p.second++)
      for ( p.third = 0 ; p.third < depth ; p.third++) {
  point = p;
  point = rotation.reverse( point );
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;
  if ( point.first >= p1f.first &&
       point.second >= p1f.second &&
       point.third >= p1f.third &&
       point.first <= p2f.first &&
       point.second <= p2f.second &&
       point.third <= p2f.third &&
       image.holds( p + translation ) )
    image( p + translation ) = value;
      }

  if ( bands > 1 )
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
}

template< typename T >
void
genesisCylinder( ZZZImage< T > & image,
     Triple<SSize> center,
     SSize radius,
     SSize h,
     T value,
     Rotation rotation,
     Triple<double> scale,
     Triple<SSize> translation)
{
  SSize depth, width, height, bands;
  Triple<SSize> p;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  for ( p.first = 0 ; p.first < width ; p.first++)
    for ( p.second = 0 ; p.second< height ; p.second++)
      for ( p.third = 0 ; p.third < depth ; p.third++) {
  point = p;
  point = rotation.reverse( point );
  point.first -= center.first;
  point.second -= center.second;
  point.third -= center.third;
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;
  if ( fabs( point.third ) <= (h/2)
       &&
       dsquare( point.first ) + dsquare( point.second ) < radius * (radius + 1)
       &&
       image.holds( p + translation ) )
    image( p + translation ) = value;
      }

  if ( bands > 1 )
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
}

template< typename T >
void
genesisAxis( ZZZImage< T > & image, Triple<SSize> center, T value )
{
  SSize depth, width, height, bands;
  Triple<SSize> p;
  image.getDimension( width, height, depth, bands );
  p = center;
  for ( ; p.first < width; ++p.first )
    image( p ) = value;
  p = center;
  for ( ; p.second < height; ++p.second )
    image( p ) = value;
  p = center;
  for ( ; p.third < depth; ++p.third )
    image( p ) = value;
}

template< typename T >
void genesisConical( ZZZImage< T > & image,
         Triple<SSize> center,
         Triple<Double> axis,
         double angle,
         SSize h,
         T value,
         Rotation rotation,
         Triple<double> scale )
{
  SSize depth, width, height, bands;
  Triple<SSize> p;
  image.getDimension( width, height, depth, bands );
  Triple<double> point;
  double c = (axis*axis)*dsquare( cos( angle ) );
  for ( p.first = 0 ; p.first < width ; p.first++)
    for ( p.second = 0 ; p.second< height ; p.second++)
      for ( p.third = 0 ; p.third < depth ; p.third++) {
  point = p;
  point = rotation.reverse( point );
  point.first -= center.first;
  point.second -= center.second;
  point.third -= center.third;
  point.first /= scale.first;
  point.second /= scale.second;
  point.third /= scale.third;
  if ( dsquare( point * axis ) > (point*point)*c-0.5 &&
       fabs( point * axis ) <= h )
    image( p ) = value;
      }

  if ( bands > 1 )
    for ( SSize b = 1; b < bands; b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
}


template< typename T >
void
genesisPyramid( ZZZImage< T > & image,
    Triple<Double> baseCenter,
    Triple<Double> topDirection,
    double height,
    Triple<Double> baseSmallSideDirection,
    double baseWidth,
    double baseHeight,
    T value )
{
  normalize( topDirection );
  normalize( baseSmallSideDirection );

  Triple<double> baseBottomDirection = baseSmallSideDirection ^ topDirection;
  Triple<double> top = baseCenter + height * topDirection;

  Triple<double> tr = baseCenter - ( 0.5 * baseHeight * baseBottomDirection )
    + ( 0.5 * baseWidth * baseSmallSideDirection );
  Triple<double> tl = baseCenter - ( 0.5 * baseHeight * baseBottomDirection )
    - ( 0.5 * baseWidth * baseSmallSideDirection );
  Triple<double> br = baseCenter + ( 0.5 * baseHeight * baseBottomDirection )
    + ( 0.5 * baseWidth * baseSmallSideDirection );
  Triple<double> bl = baseCenter + ( 0.5 * baseHeight * baseBottomDirection )
    - ( 0.5 * baseWidth * baseSmallSideDirection );

  Triple<double> plane0Normal = (br-bl)^(top-bl);
  Triple<double> plane0Center = (bl+br+top)/3.0;
  Triple<double> plane1Normal = (tr-br)^(top-br);
  Triple<double> plane1Center = (br+tr+top)/3.0;
  Triple<double> plane2Normal = (tl-tr)^(top-tr);
  Triple<double> plane2Center = (tr+tl+top)/3.0;
  Triple<double> plane3Normal = (bl-tl)^(top-tl);
  Triple<double> plane3Center = (tl+bl+top)/3.0;

  SSize iMin = min( top.first, min( min( br.first, bl.first ),
           min( tr.first, tl.first ) ) );
  SSize jMin = min( top.second, min( min( br.second, bl.second ),
            min( tr.second, tl.second ) ) );
  SSize kMin = min( top.third, min( min( br.third, bl.third ),
           min( tr.third, tl.third ) ) );
  SSize iMax = max( top.first, max( max( br.first, bl.first ),
           max( tr.first, tl.first ) ) );
  SSize jMax = max( top.second, max( max( br.second, bl.second ),
            max( tr.second, tl.second ) ) );
  SSize kMax = max( top.third, max( max( br.third, bl.third ),
           max( tr.third, tl.third ) ) );

  SSize i,j,k;
  Triple<double> p;
  for ( i = iMin ; i <= iMax ; ++i)
    for ( j = jMin ; j <= jMax ; ++j)
      for ( k = kMin ; k <= kMax ; ++k)
  if ( image.holds( i, j, k ) ) {
    p.set( i, j, k );
    if ( ( ( p - plane0Center ) * plane0Normal ) - 0.5 <= 0  &&
         ( ( p - plane1Center ) * plane1Normal ) - 0.5 <= 0  &&
         ( ( p - plane2Center ) * plane2Normal ) - 0.5 <= 0  &&
         ( ( p - plane3Center ) * plane3Normal ) - 0.5 <= 0  &&
         ( ( p - baseCenter ) * topDirection ) + 0.5 >= 0 ) {
      image( i, j, k ) = value;
    }
  }

  if ( image.bands() > 1 ) {
    for ( SSize b = 1; b < image.bands(); b++ )
      memcpy( image.dataVector( b ), image.dataVector( 0 ),
        image.realBandVectorSize() * sizeof( T ) );
  }
}

#define INSTANTIATE_007( T ) \
template void  genesisSphere( ZZZImage< T > & image, Triple<SSize> center, \
            SSize radius, T value, Triple<double> scale ); \
template void genesisEllipsoid( ZZZImage< T > & image, \
                                Triple<SSize> center, Triple<SSize> radii, \
                                T value, \
                                Rotation rotation, Triple<double> scale ); \
template void genesisHyperbolicParaboloid( ZZZImage< T > & image, \
             Triple<SSize> center, \
             double a, double b, \
             T value, \
             Rotation rotation, Triple<double> scale ); \
template void genesisParaboloid( ZZZImage< T > & image, Triple<SSize> center, \
         double a, double b, \
         T value, Rotation rotation, Triple<double> scale ); \
template void genesisPlane( ZZZImage< T > & image, \
          Triple<SSize> center, Triple<double> normal, \
          T Value, \
          Rotation rotation ); \
template void genesisTorus( ZZZImage< T > & image, \
          Triple<SSize> center, \
          SSize largeRadius, SSize smallRadius, \
          T value, \
          Rotation rotation, Triple<double> scale ); \
template void genesisBox( ZZZImage< T > & image, \
        Triple<SSize> p1, Triple<SSize> p2, \
        T value, \
        Rotation rotation, \
        Triple<double> scale, \
        Triple<SSize> translation ); \
template void genesisCylinder( ZZZImage< T > & image, \
             Triple<SSize> center, \
             SSize radius, \
             SSize height, \
             T value,	\
             Rotation rotation, \
             Triple<double> scale, \
             Triple<SSize> translation); \
template void genesisAxis( ZZZImage< T > & image, \
         Triple<SSize> center, \
         T value ); \
template void genesisConical( ZZZImage< T > & image, \
            Triple<SSize> center, \
            Triple<Double> axis, \
            double angle, \
            SSize h, \
            T value, \
            Rotation rotation, \
            Triple<double> scale ); \
template void genesisPyramid( ZZZImage< T > & image, \
         Triple<Double> baseCenter, \
         Triple<Double> topDirection, \
         double height, \
         Triple<Double> baseSmallSideDirection, \
         double baseWidth, \
         double baseHeight, \
         T value );

INSTANTIATE_007( UChar )
INSTANTIATE_007( Short )
INSTANTIATE_007( UShort )
INSTANTIATE_007( Int32 )
INSTANTIATE_007( UInt32 )
INSTANTIATE_007( Float )
INSTANTIATE_007( Double )
