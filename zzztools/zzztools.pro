LANGUAGE = C++
CONFIG	= staticlib
include("../global_options.qprefs")
release { DEFINES += _RELEASE_ }
TEMPLATE = lib

TARGET = ../libs/zzztools

INCLUDEPATH	+= .. ./include ../tools/include

TARGET = ../libs/zzztools

HEADERS	+=  ../globals.h \
            ./include/config.h \
            ./include/dimension.h \
            ./include/image_header.h \
            ./include/distance_transform.h \
            ./include/genesis.h \
            ./include/panfile.h \
            ./include/point.h \
            ./include/skel.h \
            ./include/tools3d.h \
            ./include/zzzimage.h \
            ./include/zzzimage_bool.h \            
    include/image_visitor.h \
    include/histogram_visitor.h \
    include/draw_line_visitor.h \
    include/mesh_importer_visitor.h \
    src/mesh_importer_visitor.h

SOURCES	+= ./src/config.cpp \
           ./src/distance_transform.cpp \
           ./src/genesis.cpp \
           ./src/image_header.cpp \
           ./src/panfile.cpp \
           ./src/skel.cpp \
           ./src/tools3d.cpp \
           ./src/zzzimage.cpp \
           ./src/zzzimage_bool.cpp \
    src/image_visitor.cpp \
    src/histogram_visitor.cpp \
    src/draw_line_visitor.cpp \
    src/mesh_importer_visitor.cpp

OBJECTS_DIR = objs

#unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  QMAKE_CXXFLAGS_RELEASE +=  -ffast-math -O3 -Wall -W -pedantic -ansi
  QMAKE_CXXFLAGS_DEBUG += -ffast-math -Wall -W -Wextra -pedantic -ansi
  DEFINES += _IS_UNIX_
#}

win32-g++ {
  QMAKE_CXXFLAGS_RELEASE += -ffast-math -W -Wall -ansi -pedantic
  QMAKE_CXXFLAGS_DEBUG +=  -W -Wall -ansi -pedantic
}
