/* -*- mode: c++ -*- */
/**
 * @file   Transforms.h
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Aug 18 2007
 * 
 * @brief
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _BOARD_TRANSFORMS_H_
#define _BOARD_TRANSFORMS_H_

#include <limits>
#include <vector>
#include <cmath>

namespace LibBoard {

struct Rect;
struct Shape;
struct ShapeList;

/**
 * The base class for transforms.
 * @brief 
 */
struct Transform {
public:
  inline Transform();
  virtual ~Transform() { };
  virtual double mapX( double x ) const;
  virtual double mapY( double y ) const = 0;
  virtual void apply( double & x, double & y ) const;
  virtual double scale( double x ) const;
  virtual double rounded( double x ) const;
  virtual void setBoundingBox( const Rect & rect,
			       const double pageWidth,
			       const double pageHeight,
			       const double margin ) = 0;

  static inline double round( const double & x );

protected:
  double _scale;
  double _deltaX;
  double _deltaY;
  double _height;
};

/**
 * The TransformEPS structure.
 * @brief Structure representing a scaling and translation
 * suitable for an EPS output.
 */
struct TransformEPS : public Transform {
public:
  double mapY( double y ) const;
  void setBoundingBox( const Rect & rect,
		       const double pageWidth,
		       const double pageHeight,
		       const double margin );
};

/**
 * The TransformFIG structure.
 * @brief Structure representing a scaling and translation
 * suitable for an XFig output.
 */
struct TransformFIG : public Transform {
public:
  inline TransformFIG();
  double rounded( double x ) const;
  double mapY( double y ) const;
  int mapWidth( double width ) const; 
  void setBoundingBox( const Rect & rect,
		       const double pageWidth,
		       const double pageHeight,
		       const double margin );
  void setDepthRange( const ShapeList & shapes );
  int mapDepth( int depth ) const;
private:
  int _maxDepth;
  int _minDepth;
};

/**
 * The TransformSVG structure.
 * @brief Structure representing a scaling and translation
 * suitable for an SVG output.
 */
struct TransformSVG : public Transform {
public:
  double rounded( double x ) const;
  double mapY( double y ) const;
  double mapWidth( double width ) const; 
  void setBoundingBox( const Rect & rect,
		       const double pageWidth,
		       const double pageHeight,
		       const double margin );
};

/**
 * The TransformTikZ structure.
 * @brief Structure representing a scaling and translation
 * suitable for an TikZ output.
 */
struct TransformTikZ : public TransformSVG {
};


#include "Transforms.ih"

} // namespace LibBoard

#endif /* _TRANSFORMS_H_ */
