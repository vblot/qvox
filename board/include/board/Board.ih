/* -*- mode: c++ -*- */
/**
 * @file   Board.ih
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Aug 18 2007
 * 
 * @brief  Definition of inline methods of the Board class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

namespace LibBoard {

inline
void
Board::clear( unsigned char red, unsigned char green, unsigned char blue )
{
  clear( Color( red, green, blue ) );
}

inline Board &
Board::setLineStyle( Shape::LineStyle style )
{
  _state.lineStyle = style;
  return *this;
}

inline
Board &
Board::setLineCap( Shape::LineCap cap )
{
  _state.lineCap = cap;
  return *this;
}
  
inline
Board &
Board::setLineJoin( Shape::LineJoin join )
{
  _state.lineJoin = join;
  return *this;
}

inline
void
Board::fillGouraudTriangle( const double x1, const double y1,
			    const Color & color1,
			    const double x2, const double y2, 
			    const Color & color2,
			    const double x3, const double y3,
			    const Color & color3,
			    unsigned char divisions,
			    int depth /* = -1 */ )
{
  fillGouraudTriangle( Point( x1, y1 ), color1,
		       Point( x2, y2 ), color2,
		       Point( x3, y3 ), color3,
		       divisions, depth );		       
}

inline
void
Board::fillGouraudTriangle( const double x1, const double y1,
			    const float brightness1,
			    const double x2, const double y2, 
			    const float brightness2,
			    const double x3, const double y3,
			    const float brightness3,
			    unsigned char divisions,
			    int depth /* = -1 */ )
{
  fillGouraudTriangle( Point( x1, y1 ), brightness1,
		       Point( x2, y2 ), brightness2,
		       Point( x3, y3 ), brightness3,
		       divisions, depth );
}

} // namespace LibBoard
