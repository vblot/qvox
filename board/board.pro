LANGUAGE = c++
CONFIG	= warn_on release staticlib
TEMPLATE = lib

INCLUDEPATH	+= .. ./include

HEADERS	+=  ./include/Board.h \
            ./include/board/Color.h \
            ./include/board/Point.h \
            ./include/board/PSFonts.h \
            ./include/board/Path.h \
            ./include/board/Rect.h \
            ./include/board/ShapeList.h \
            ./include/board/Shapes.h \
            ./include/board/Tools.h \
            ./include/board/Transforms.h


SOURCES	+= ./src/Board.cpp \
           ./src/Color.cpp \
           ./src/PSFonts.cpp \
           ./src/Path.cpp \
           ./src/Rect.cpp \
           ./src/ShapeList.cpp \
           ./src/Shapes.cpp \
           ./src/Tools.cpp \
           ./src/Transforms.cpp

TARGET = ../libs/board

release { DEFINES += _RELEASE_ }

OBJECTS_DIR = obj

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  QMAKE_CXXFLAGS_RELEASE +=  -ffast-math -O3 -Wall -W -pedantic -ansi
  QMAKE_CXXFLAGS_DEBUG += -ffast-math -Wall -W -pedantic -ansi
  DEFINES += _IS_UNIX_
}

win32-g++ {
  QMAKE_CXXFLAGS_RELEASE += -ffast-math -W -Wall -ansi -pedantic
  QMAKE_CXXFLAGS_DEBUG += -W -Wall -ansi -pedantic
}


