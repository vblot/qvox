/* -*- mode: c++ -*- */
/**
 * @file   PSFonts.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Aug 18 2007
 * 
 * @brief  The Point structure.
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

namespace LibBoard {

const char * PSFontNames[] = {
  "Times-Roman",
  "Times-Italic",
  "Times-Bold",
  "Times-Bold-Italic",
  "AvantGarde-Book",
  "AvantGarde-Book-Oblique",
  "AvantGarde-Demi",
  "AvantGarde-Demi-Oblique",
  "Bookman-Light",
  "Bookman-Light-Italic",
  "Bookman-Demi",
  "Bookman-Demi-Italic",
  "Courier",
  "Courier-Oblique",
  "Courier-Bold",
  "Courier-Bold-Oblique",
  "Helvetica",
  "Helvetica-Oblique",
  "Helvetica-Bold",
  "Helvetica-Bold-Oblique",
  "Helvetica-Narrow",
  "Helvetica-Narrow-Oblique",
  "Helvetica-Narrow-Bold",
  "Helvetica-Narrow-Bold-Oblique",
  "New-Century-Schoolbook-Roman",
  "New-Century-Schoolbook-Italic",
  "New-Century-Schoolbook-Bold",
  "New-Century-Schoolbook-Bold-Italic",
  "Palatino-Roman",
  "Palatino-Italic",
  "Palatino-Bold",
  "Palatino-Bold-Italic",
  "Symbol",
  "Zapf-Chancery-Medium-Italic",
  "Zapf-Dingbats"
};

}
