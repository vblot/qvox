/* -*- mode: c++ -*- */
/**
 * @file   Color.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Aug 18 2007
 * 
 * @brief  
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "board/Color.h"
#include "board/Tools.h"
#include <cstdio>
#include <cmath>
using std::string;

namespace LibBoard {

const Color Color::None(false);
const Color Color::Black((unsigned char)0,(unsigned char)0,(unsigned char)0);
const Color Color::Gray((unsigned char)128,(unsigned char)128,(unsigned char)128);
const Color Color::White((unsigned char)255,(unsigned char)255,(unsigned char)255);
const Color Color::Red((unsigned char)255,(unsigned char)0,(unsigned char)0);
const Color Color::Green((unsigned char)0,(unsigned char)128,(unsigned char)0);
const Color Color::Lime((unsigned char)0,(unsigned char)255,(unsigned char)0);
const Color Color::Blue((unsigned char)0,(unsigned char)0,(unsigned char)255);
const Color Color::Cyan((unsigned char)0,(unsigned char)255,(unsigned char)255);
const Color Color::Magenta((unsigned char)255,(unsigned char)0,(unsigned char)255);
const Color Color::Yellow((unsigned char)255,(unsigned char)255,(unsigned char)0);
const Color Color::Silver((unsigned char)190,(unsigned char)190,(unsigned char)190);
const Color Color::Purple((unsigned char)128,(unsigned char)128,(unsigned char)128);
const Color Color::Navy((unsigned char)0,(unsigned char)0,(unsigned char)128);
const Color Color::Aqua((unsigned char)0,(unsigned char)255,(unsigned char)255);

Color::Color( const unsigned int rgb, unsigned char alpha )
 :_alpha( alpha )
{
  _red = ( rgb & 0xFF0000u ) >> 16;
  _green = ( rgb & 0xFF00u ) >> 8;
  _blue = rgb & 0xFF;
}

Color &
Color::setRGBf( float red,
		float green,
		float blue,
		float alpha  ) {
  if ( red > 1.0f ) red = 1.0f;
  if ( red < 0.0f ) red = 0.0f;
  _red = static_cast<unsigned char>( 255 * red );
  if ( green > 1.0f ) green = 1.0f;
  if ( green < 0.0f ) green = 0.0f;
  _green = static_cast<unsigned char>( 255 * green );
  if ( blue > 1.0f ) blue = 1.0f;
  if ( blue < 0.0f ) blue = 0.0f;
  _blue = static_cast<unsigned char>( 255 * blue );
  if ( alpha > 1.0f ) alpha = 1.0f;
  if ( alpha < 0.0f ) alpha = 0.0f;
  _alpha = static_cast<unsigned char>( 255 * alpha );
  return *this;
}

bool
Color::operator==( const Color & other ) const
{
  return _red == other._red 
    && _green == other._green
    && _blue == other._blue
    && _alpha == other._alpha;
}

bool
Color::operator!=( const Color & other ) const
{
  return _red != other._red  
    || _green != other._green
    || _blue != other._blue
    || _alpha != other._alpha;
}

bool
Color::operator<( const Color & other ) const
{
  if ( _red < other._red )
    return true;
  if ( _red == other._red ) {
    if ( _green < other._green )
      return true;
    if ( _green == other._green ) { 
      if ( _blue < other._blue )
	return true;
      if ( _blue == other._blue )
	return _alpha < other._alpha;
    }
  }
  return false;
}

void
Color::flushPostscript( std::ostream & stream ) const
{
  stream << (_red/255.0) << " "
	 << (_green/255.0) << " "
	 << (_blue/255.0) << " srgb\n";
}

string
Color::postscript() const
{
  char buffer[255];
  secured_sprintf( buffer, 255, "%.4f %.4f %.4f", _red/255.0, _green/255.0, _blue/255.0 );
  return buffer;
}

string
Color::svg() const
{
  char buffer[255];
  if ( *this == Color::None ) return "none";
  secured_sprintf( buffer, 255, "rgb(%d,%d,%d)", _red, _green, _blue );
  return buffer;
}

string
Color::svgAlpha( const char * prefix ) const
{
  char buffer[255];
  if ( _alpha == 255 || *this == Color::None ) return "";
  secured_sprintf( buffer, 255, " %s-opacity=\"%f\"", prefix, _alpha/255.0f );
  return buffer;
}

string
Color::tikz() const
{
  // see tex/generic/pgf/utilities/pgfutil-plain.def for color definitions
  char buffer[255];
  if ( *this == Color::None ) return "none";
  if ( *this == Color::Black ) return "black";
  if ( *this == Color::Gray ) return "gray";
  if ( *this == Color::White ) return "white";
  if ( *this == Color::Red ) return "red";
  if ( *this == Color::Green ) return "green!50!black";
  if ( *this == Color::Lime ) return "green";
  if ( *this == Color::Blue ) return "blue";
  if ( *this == Color::Silver ) return "white!75!black";
  if ( *this == Color::Purple ) return "gray"; // ???: Is Color::Purple meant to be equal to Color::Gray?
  if ( *this == Color::Navy ) return "blue!50!black";
  secured_sprintf( buffer, 255, "{rgb,255:red,%d;green,%d;blue,%d}", _red, _green, _blue );
  return buffer;
}



} // namespace LibBoard
