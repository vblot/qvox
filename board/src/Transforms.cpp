/* -*- mode: c++ -*- */
/**
 * @file   Transforms.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Aug 18 2007
 * 
 * @brief  
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "board/Rect.h"
#include "board/Shapes.h"
#include "board/ShapeList.h"
#include "board/Transforms.h"
#include <cmath>

namespace {
  const float ppmm = 720.0f / 254.0f;
  const float fig_ppmm = 1143 / 25.4f;
}

namespace LibBoard {

//
// Transform
// 

double
Transform::rounded( double x ) const
{
  return Transform::round( 1000000*x ) / 1000000;
} 

double
Transform::mapX( double x ) const
{
  return rounded( x * _scale + _deltaX );
}

double
Transform::scale( double x ) const
{
  return rounded( x * _scale );
}

void
Transform::apply( double & x, double & y ) const
{
  x = mapX( x );
  y = mapY( y );
}

//
// TransformEPS
// 

double
TransformEPS::mapY( double y ) const
{
  return rounded( y * _scale + _deltaY );
}

void
TransformEPS::setBoundingBox( const Rect & rect,
			      const double pageWidth,
			      const double pageHeight,
			      const double margin )
{
  if ( pageWidth <= 0 || pageHeight <= 0 ) {
    _scale = 1.0f;
    // _deltaX = - rect.left;
    _deltaX = 0.5 * 210 * ppmm - ( rect.left + 0.5 * rect.width );
    // _deltaY = - ( rect.top - rect.height );
    _deltaY = 0.5 * 297 * ppmm - ( rect.top - 0.5 * rect.height );  
    _height = rect.height;
  } else {
    const double w = pageWidth - 2 * margin;
    const double h = pageHeight - 2 * margin;
    if ( ( rect.height / rect.width ) > ( h / w ) ) {
      _scale = h * ppmm / rect.height;
    } else {
      _scale = w * ppmm / rect.width;
    }
    _deltaX = 0.5 * pageWidth * ppmm - _scale * ( rect.left + 0.5 * rect.width );
    _deltaY = 0.5 * pageHeight * ppmm - _scale * ( rect.top - 0.5 * rect.height );  
    _height = pageHeight * ppmm;
  }
}

//
// TransformFIG
// 

double
TransformFIG::rounded( double x ) const
{
  return Transform::round( x );
}

double
TransformFIG::mapY( double y ) const
{
  return rounded( _height - ( y * _scale + _deltaY ) );
}

int
TransformFIG::mapWidth( double width ) const
{
  // FIG thickness unit is 1/80 inch, reduced to 1/160 when exporting to EPS.
  // Postscript points are 1/72 inch
  if ( width == 0.0 )
    return 0;
  int result = static_cast<int>( Transform::round( 160 * ( width / 72.0 ) ) );
  return result>0?result:1;
}

void
TransformFIG::setBoundingBox( const Rect & rect,
			      const double pageWidth,
			      const double pageHeight,
			      const double margin )
{
  if ( pageWidth <= 0 || pageHeight <= 0 ) {
    _scale = fig_ppmm / ppmm;
    _deltaX = 0.5 * 210 * fig_ppmm - _scale * ( rect.left + 0.5 * rect.width );
    //_deltaX = - rect.left;
    _deltaY = 0.5 * 297 * fig_ppmm - _scale * ( rect.top - 0.5 * rect.height );  
    // _deltaY = - rect.top;
    // _deltaY = - ( rect.top - rect.height );
    //_height = rect.height;
    _height = 297 * fig_ppmm;
  } else {
    const double w = pageWidth - 2 * margin;
    const double h = pageHeight - 2 * margin;
    if ( rect.height / rect.width > ( h / w ) ) {
      _scale = ( h * fig_ppmm ) / rect.height;
    } else {
      _scale = ( w * fig_ppmm ) / rect.width;
    }
    _deltaX = 0.5 * pageWidth * fig_ppmm - _scale * ( rect.left + 0.5 * rect.width );
    _deltaY = 0.5 * pageHeight * fig_ppmm - _scale * ( rect.top - 0.5 * rect.height );  
    _height = pageHeight * fig_ppmm;
  }
  // float ppmm = (1200/25.4);
}

void
TransformFIG::setDepthRange( const ShapeList & shapes )
{
  _maxDepth = shapes.maxDepth();
  _minDepth = shapes.minDepth();
}

int
TransformFIG::mapDepth( int depth ) const
{
  if ( depth > _maxDepth ) return 999;
  if ( _maxDepth - _minDepth > 998 ) {
    double range = _maxDepth - _minDepth;
    int r = static_cast<int>( 1 + Transform::round( ( ( depth - _minDepth ) / range ) * 998 ) );
    return r>=0?r:0;
  } else {
    int r = 1 + depth - _minDepth;
    return r>=0?r:0;
  }
}

//
// TransformSVG
// 

double
TransformSVG::rounded( double x ) const
{
  return Transform::round( 100*x ) / 100.0f;
} 

double
TransformSVG::mapY( double y ) const
{
  return rounded( _height - ( y * _scale + _deltaY ) );
}

double
TransformSVG::mapWidth( double width ) const
{
  // return Transform::round( 1000 * width / ppmm ) / 1000.0;
  return Transform::round( 1000 * width  / ppmm  ) / 1000.0;
}

void
TransformSVG::setBoundingBox( const Rect & rect,
			      const double pageWidth,
			      const double pageHeight,
			      const double margin )  
{
  if ( pageWidth <= 0 || pageHeight <= 0 ) {
    _scale = 1.0f;
    // _deltaX = 0.5 * 210 * ppmm - ( rect.left + 0.5 * rect.width );
    _deltaX = - rect.left;
    // _deltaY = 0.5 * 297 * ppmm - ( rect.top - 0.5 * rect.height );
    _deltaY = - ( rect.top - rect.height );
    // _height = 297 * fig_ppmm;
    _height = rect.height;
  } else {
    const double w = pageWidth - 2 * margin;
    const double h = pageHeight - 2 * margin;
    if ( rect.height / rect.width > ( h / w ) ) {
      _scale = h * ppmm / rect.height;
    } else {
      _scale = w * ppmm / rect.width;
    }
    _deltaX = 0.5 * pageWidth * ppmm - _scale * ( rect.left + 0.5 * rect.width );
    _deltaY = 0.5 * pageHeight * ppmm - _scale * ( rect.top - 0.5 * rect.height );
    _height = pageHeight * ppmm;
  }
}

} // namespace LibBoard
