/**
 * @file   globals.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:30:21 2005
 *
 * @brief  Whole project global definitions
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <iostream>
#include <iomanip>
#include <utility>
#include <climits>

#include "arch_info.h"

#define VERSION_STRING "2.8.0Beta-2"

#if defined(_RELEASE_) || defined(RELEASE)
/*
 *   RELEASE OPTIONS
 */
#define _WITH_FORK
#define TRACE if ( false ) std::cerr
#define TSHOW( V )
#else
/*
 *   DEBUG OPTIONS
 */
#define _WITH_FORK
#define _TRACE if ( false ) std::cerr
#define TRACE std::cerr
#define TSHOW( V ) std::cout << " " #V " = " << ( V ) << std::endl
#define _HAVE_QWT
#define HAVE_EXPERIMENTAL
#define _TRACE_ALLOCATIONS
#endif

#define SHOW( V ) std::cout << " " #V " = " << ( V ) << std::endl
#define MAX_ZOOM 256
#define HALF_ZOOM 128
#define SWAP(X,Y,TMP) { (TMP) = (X); (X) = (Y); (Y) = (TMP); }

#define SGR_ESC "\x1B["
#define SGR_BOLD SGR_ESC "1m"
#define SGR_NORMAL SGR_ESC "0m"
#define SGR_RED SGR_ESC "31;47;1m"

#define INFO    std::cerr << "Information: "
#define WARNING std::cerr << SGR_BOLD << "Warning (" << __FILE__ << ":" << __LINE__ << "): " << SGR_NORMAL
#define ERROR   std::cerr << SGR_RED << "Error (" << __FILE__ << ":" << __LINE__ << "): " << SGR_NORMAL

typedef std::pair<int,int> (*ProgressFunction)(int, const char *);

enum Orientation { NORTH=0,
                   SOUTH,
                   EAST,
                   WEST,
                   BEFORE,
                   BEHIND,
                   NO_DIRECTION
                 };

typedef bool            Bool;
typedef	char		Char;
typedef	unsigned char	UChar;
typedef	short		Short;
typedef	unsigned short	UShort;
typedef int             Int;
typedef unsigned int    UInt;
typedef	float		Float;
typedef	double		Double;

#if USHRT_MAX != 65535
  #error Width of short integers is not 16-bit in this environment
#endif

#if ULONG_MAX == 4294967295UL
  typedef signed   long   Int32;
  typedef unsigned long   UInt32;
#define INT32_FORMAT "%ld"
#define UINT32_FORMAT "%lu"
#elif UINT_MAX == 4294967295U
  typedef signed   int    Int32;
  typedef unsigned int    UInt32;
#define INT32_FORMAT "%d"
#define UINT32_FORMAT "%u"
#else
  #error None of int or long is 32-bit in this environment
#endif

#ifndef _IS_UNIX_
typedef long ssize_t;
#endif

typedef signed long SSize;
#define SSIZE_FORMAT "%ld"
typedef unsigned long Size;
#define SIZE_FORMAT "%lu"

template< typename T >
inline void dispose( T * & pointer ) {
  delete pointer;
  pointer = 0;
}

template< typename T >
inline void disposeArray( T * & pointer ) {
  delete[] pointer;
  pointer = 0;
}

#define CONS(ID) TRACE << "++" #ID "()\n"
#define DEST(ID) TRACE << "--" #ID "()\n"

#endif // _GLOBALS_H_
