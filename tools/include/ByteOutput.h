/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ByteOutput.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 * 
 * @brief  Declaration of the class ByteOutput
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _BYTEOUTPUT_H_
#define _BYTEOUTPUT_H_

#include <iostream>
#include <exception>
#include <typeinfo>
#include <sstream>
#include <vector>
#include <cstdio>

/** 
 * Output stream abstract class
 */
class ByteOutput {
 public:
   ByteOutput() { }
   virtual ~ByteOutput() { };
   virtual bool put( char c ) = 0;
   virtual bool puts( const char *s ) = 0;
   virtual bool write( const char *s, size_t n ) = 0;
   virtual ByteOutput & operator<<( const char * str );
   virtual void flush() = 0;
   virtual operator bool() const { return true; }
};

/** 
 * Output stream implemented by an STL ostream.
 */
class ByteOutputStream : public ByteOutput {
 public:
   ByteOutputStream( std::ostream & out  );
   ByteOutputStream( const ByteOutputStream & other  );
   ~ByteOutputStream() { }
   bool put( char c );
   bool puts( const char *s );
   bool write( const char* s, size_t n );
   void flush();
   operator bool() const;
   std::ostream & out() { return _out; }
protected:
   std::ostream & _out;
};

#ifdef _IS_UNIX_

/** 
 * Output stream implemented by a UNIX file descriptor.
 */
class ByteOutputFD : public ByteOutput {
 public:
   ByteOutputFD( int fd  );
   ByteOutputFD( const ByteOutputFD & other  );
   ~ByteOutputFD();
   bool put( char c );
   bool puts( const char *buff );
   bool write( const char *buff, size_t n );
   void flush();
   operator bool() const;
   int fd() { return _fd; }
protected:
   int _fd;
   bool _ok;
};

#endif // _IS_UNIX_

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_) 

/** 
 * Output stream through a GZIP process.
 */
class ByteOutputGZip : public ByteOutputFD {
 public:
   ByteOutputGZip( ByteOutput & output  );
   bool write( const char *buff, size_t n );
   ~ByteOutputGZip();
   ByteOutput & out() { return _out; }
private:
   ByteOutputGZip( const ByteOutputGZip & other  );
protected:
   ByteOutput & _out;
   int _pidWriter;		/**< PID of the Stream output writer */
   int _pidGZip;		/**< PID of the GZip process */
};

#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_) 

/** 
 * Output stream to a memory buffer.
 */
class ByteOutputMemory : public ByteOutput {
 public:
   ByteOutputMemory();
   ~ByteOutputMemory() { }

   bool put( char c );
   bool puts( const char *s );
   bool write( const char *s, size_t n );
   void flush();
   operator bool() const;

   char * buffer();
   size_t size() const;
protected:
   std::vector<char> _vector;
   bool _ok;
};

template<typename T>
ByteOutput & operator<<( ByteOutput & out, const T & t )
{
   std::ostringstream s;
   s << t;
   out.puts( s.str().c_str() );
   return out;
}

#endif // _BYTEINPUT_H_
