/** -*- mode: c++ -*-
 * @file   tools.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2005
 * 
 * @brief  All purpose routines.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _TOOLS_H_
#define _TOOLS_H_

#include <globals.h>
#include <vector>
#include <utility>
#include <limits>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

enum Endianness { BigEndian = 0, LittleEndian = 1 }; 

class ByteOutput;

/** 
 * Returns today's date as a string dd/mm/yy
 * 
 * @return Today's date as a string dd/mm/yy.
 */
std::string today();

void strToUpper( char * dst, const char * src );

void strToLower( char * dst, const char * src );

/** 
 * Return a binary representation of a long integer.
 * 
 * @param l 
 * 
 * @return A string like "11100101"
 */
std::string binary( UInt32 l );

/** 
 * Checks if the system is little endian.
 * 
 * @return 
 */
bool isLittleEndian();


Endianness endian();


/** 
 * Finds the last dot in a string.
 * 
 * @param s 
 * @return A pointer to the last char equal to '.', or
 *         a pointer to the ending '\0' if none is found.
 */
const char * lastDotOrEnd( const char *s );

/** 
 * Finds the last dot in a string.
 * 
 * @param s 
 * @return A pointer to the last char equal to '.', or
 *         a pointer to the ending '\0' if none is found.
 */
char * lastDotOrEnd( char *s );

/** 
 * Check is extension matches the given filename.
 * (Case insensitive.)
 * 
 * @param str 
 * @param extension 
 * 
 * @return 
 */
bool extension( const char * str, const char * extension ); 


/** 
 * Returns a pointer to the filename part of a path. 
 * 
 * @param filename 
 * 
 * @return 
 */
const char * baseName( const char * filename );

/** 
 * Returns a pointer to the filename part of a path. 
 * 
 * @param filename 
 * 
 * @return 
 */
char * baseName( char * filename );

/** 
 * Checks if file has the given magic number and
 * move the file pointer right after it.
 *
 * @param file An input stream;
 * @param str A magic number as a null terminated C string.
 * @return true if the full magic number can be read, false otherwise.
 */
bool magicNumber( std::istream & file, const char * str );

/** 
 * Checks if strings are equal up to a '?' joker.
 *
 * @param strA A string which may contain '?'.
 * @param strB A string which may contain '?'.
 * @return true if the two string match up to '?' characters. 
 */
bool magicNumber( const char * , const char * str );


/** 
 * Prints a numbered tracing message.
 * 
 * @param i The sequence number to be printed.
 */
void trace( int i );

/** 
 * Initialize the pseudo-random sequence.
 * 
 */
void initRandom();

/** 
 * Finds a given option in the command line array
 * of strings, null the string if found, and returns a pointer
 * to the following string.
 * 
 * @param argc Size of the array of parameters.
 * @param argv Command line array of strings.
 * @param param String to be found in the array.
 * @return If the string is found, it is nulled in the array of
 *         arguments and a pointer to the next argument
 *         is returned (to the nulled string itself if last).
 *         Otherwise, the null pointer is returned.
 */
char * findArg(int argc, char * argv[], const char * param);

/** 
 * Asks a question on the console.
 * 
 * @param question A null terminated string.
 * @return true of false according to the answer. 
 */
bool ask( const char *question );

/** 
 * Asks for an integer on the console.
 * 
 * @param message A message to be printed.
 * @return The input integer.
 */
int readInteger( const char *message );

/** 
 * Writes a 4 bytes integer with Most Significant Byte first
 * in a file.
 * 
 * @param file A file.
 * @param i Integer to be written in the file.
 */
void fwriteWordMSB( FILE * file, unsigned int i );

/** 
 * Writes a 4 bytes integer with Least Significant Byte first
 * in a file.
 * 
 * @param file A file.
 * @param i Integer to be written in the file.
 */
void fwriteWordLSB( FILE * file, unsigned int i );

/** 
 * Reads a 4 bytes integer with Least Significant Byte first
 * from a file.
 *
 * @param file A file from which the integer is to be read. 
 * @return Integer read.
 */
unsigned int readWordMSB( FILE * file );

/** 
 * Reads a 4 bytes integer with Most Significant Byte first
 * from a file.
 *
 * @param file A file from which the integer is to be read. 
 * @return Integer read.
 */
unsigned int readWordLSB( FILE * fichier );

/** 
 * Writes a 4 bytes integer with Most Significant Byte first
 * in a file.
 * 
 * @param file An output stream.
 * @param i Integer to be written in the file.
 */
std::ostream & writeWordMSB( std::ostream & file, unsigned long i );

/** 
 * Writes a 4 bytes integer with Least Significant Byte first
 * in a file.
 * 
 * @param file An output stream.
 * @param i Integer to be written in the file.
 */
std::ostream & writeWordLSB( std::ostream & file, unsigned long i );

/** 
 * Reads a 4 bytes integer with Least Significant Byte first
 * from a file.
 *
 * @param file An input stream;
 * @return Integer read as an unsigned long.
 */
unsigned long readWordMSB( std::istream & file );

/** 
 * Reads a 4 bytes integer with Most Significant Byte first
 * from a file.
 *
 * @param file An input stream.
 * @return Integer read as an unsigned long.
 */
unsigned long readWordLSB( std::istream & file );


/** 
 * Reads a 2 bytes integer with Least Significant Byte first.
 * 
 * @param file Input stream from which the short is to be read.
 * @return The unsigned short integer read in file.
 */
unsigned short readShortLSB( std::istream & file );

/** 
 * Reads a 2 bytes integer with Most Significant Byte first.
 * 
 * @param file Input stream from which the short is to be read.
 * @return The unsigned short integer read in file.
 */
unsigned short readShortMSB( std::istream & file );

/** 
 * Writes a 2 bytes integer with Least Significant Byte first.
 * 
 * @param file Output stream in which the short is to be written.
 * @param s Unsigned short to write.
 */
std::ostream & writeShortLSB( std::ostream & file, unsigned short s );

/** 
 * Writes a 2 bytes integer with Least Significant Byte first.
 * 
 * @param file Output stream in which the short is to be written.
 * @param s Unsigned short to write.
 */
std::ostream & writeShortMSB( std::ostream & file, unsigned short s );


/** 
 * Writes a 4 bytes integer with Least Significant Byte first
 * in an output stream.
 * 
 * @param out An output stream.
 * @param i Integer to be written in the file.
 */
void writeWordLSB( ByteOutput & out,  int u );

/** 
 * Writes a 4 bytes integer with Most Significant Byte first
 * in an output stream.
 * 
 * @param out An output stream.
 * @param i Integer to be written in the file.
 */
void writeWordMSB( ByteOutput & out,  int u );

/** 
 * Writes a 2 bytes integer with Least Significant Byte first.
 * 
 * @param out Output stream in which the short is to be written.
 * @param s Unsigned short to write.
 */
void writeShortLSB( ByteOutput & out, unsigned short s );

/** 
 * Writes a 2 bytes integer with Least Significant Byte first.
 * 
 * @param file Output stream in which the short is to be written.
 * @param s Unsigned short to write.
 */
void writeShortMSB( ByteOutput & out, unsigned short s );


/** 
 * Reverse bytes order in a 32 bits integer.
 * @param u An integer into which bytes will be swapped.
 */
void reverseBytes32( unsigned int &u );

/** 
 * Reverse bytes order in a 32 bits integer.
 * @param u An integer into which bytes will be swapped.
 */
void reverseBytes32( unsigned long &u );

/** 
 * Reverse bytes order depending on a size parameter.
 * 
 * @param p Number of bytes.
 */
template<int n>
void reverseBytes( void * p )
{
  unsigned char tmp;
  unsigned char *p1 = reinterpret_cast<unsigned char*>( p );
  unsigned char *p2 = reinterpret_cast<unsigned char*>( p ) + n - 1 ;
  while ( p1 < p2 ) {
    tmp = *p1;
    (*p1) = *p2; 
    (*p2) = tmp;
    p1++;
    p2--;
  }
}

/** 
 * Reverse bytes of an array of elements.
 * 
 * @param data Beginning of the data array.
 * @param n Number of elements.
 * @param esize Size of each element in bytes.
 */
template<int esize>
void reverseBytes( void * data, const size_t n )
{
  if ( ! ( esize & 1 ) ) {
    throw "Cannot handle odd sizes in reverse";
    return;
  }
  unsigned char *p = reinterpret_cast<unsigned char*>( data );
  unsigned char *end = p + n * esize;
  unsigned char tmp;
  unsigned char *left;
  unsigned char *right;
  unsigned char *stop;
  const int n2 = n >> 1;
  const int esize2 = esize - 1;
  int i;
  while ( p < end ) {
    left = p;
    right = left + esize2;
    stop = p + n2;
    while ( left < stop ) {
      tmp = *left;
      *left = *right;
      *right = tmp;
      ++left;
      --right;
    }
    p += esize;
  }
}

template<>
void reverseBytes<2>( void * data, const size_t n );

template<>
void reverseBytes<4>( void * data, const size_t n );

template<>
void reverseBytes<8>( void * data, const size_t n );


/** 
 * Reverse bytes order depending on a size parameter.
 * Specialized version that does nothing if var is one byte large.
 *
 * @param p Number of bytes.
 */
template<> void reverseBytes<1>( void * p );

/** 
 * Reverse bytes order depending on a size parameter.
 * Specialized version for values with two bytes width.
 *
 * @param p Number of bytes.
 */
template<> void reverseBytes<2>( void * p );

/** 
 * Reverse bytes order depending on a size parameter.
 * Specialized version for values with four bytes width.
 *
 * @param p Number of bytes.
 */
template<> void reverseBytes<4>( void * p );

/** 
 * Reverse bytes order depending on a size parameter.
 * Specialized version for values with height bytes width.
 *
 * @param p Number of bytes.
 */
template<> void reverseBytes<8>( void * p );

/** 
 * Square of a float.
 * 
 * @param x A float.
 * @return Square of x.
 */
inline float
fsquare( const float x )
{ 
  return x * x;
}

/** 
 * Square of a double.
 * 
 * @param x A float.
 * @return Square of x.
 */
inline double
dsquare( const double x )
{ 
  return x * x;
}


/** 
 * Absolute value of an integer.
 * 
 * @param i An integer.
 * 
 * @return The absolute value of i.
 */
inline int
iabs( const int i )
{
  return (i<0)?-i:i;
}


/** 
 * Returns the min of two integers.
 * 
 * @param a An integer.
 * @param b An integer.
 * @return The min of a and b.
 */
template<typename T>
inline const T &
min( const T & a, const T & b )
{ 
  return ( a > b ) ? b : a;
}

/** 
 * Returns the min of two integers.
 * 
 * @param a An integer.
 * @param b An integer.
 * @return The min of a and b.
 */
template<typename T>
inline const T &
max( const T & a, const T & b )
{ 
  return ( a > b ) ? a : b;
}

/** 
 * Minimizes a value considering a candidate inferior other value.
 * 
 * @param value A value to minimize.
 * @param candidate A candidate minorant.
 */
template<typename T>
inline void
minimize( T & value, const T candidate )
{ 
  if ( candidate < value ) value = candidate;
}

template<typename T>
inline void
maximize( T & value,  const T candidate )
{ 
  if ( candidate > value ) value = candidate;
}

/** 
 * Converts radians to degrees.
 * 
 * @param r An angle in radians.
 * 
 * @return The angle in degrees.
 */
inline double rad2deg( double r )
{
  return 180.0*(r/M_PI); 
}

/** 
 * Returns the sign of a value.
 * 
 * @param x A value
 * 
 * @return +/-1 depending on the sign of x.
 */
template<typename T>
inline
T sign( T x )
{
  if ( x < 0 ) return static_cast<T>( -1 );
  return static_cast<T>( 1 );
}

/** 
 * Swaps two values passed by reference.
 * 
 * @param left The first value.
 * @param right The second value.
 */
template<typename T>
inline void
swapValues( T & left, T & right )
{
  T tmp = left;  left = right; right = tmp;
}

/** 
 * Copy elements of the right vector into the left one,
 * casting them if needed. The vector is resized accordingly.
 * 
 * @param left The vector to be set. 
 * @param right The vector to be copied.
 */
template< typename T, typename U >
void
cast_copy(  std::vector< T > & left, std::vector< U > & right )
{
  left.resize( right.size() );
  typename std::vector< T >::iterator ileft = left.begin();
  typename std::vector< U >::iterator iright = right.begin();
  typename std::vector< U >::iterator eright = right.end();
  while ( iright != eright ) {
    *ileft = static_cast<T>( *iright );
    ++iright;
    ++ileft;
  }
}

/** 
 * Applies a uniform quantization to a range with 
 * cardinality values among the value domain.
 * 
 * @param value A value.
 * @param cardinality The required cardinality of the target range.
 * 
 * @return A quantized value.
 */
template< typename T >
inline T
quantize( T value, UInt32 cardinality )
{
  double divide = std::numeric_limits< T >::max() / static_cast<double>( cardinality * 2 );
  double result = floor( 0.5 + ( value ) / divide );
  if ( result < (value/divide) ) result += 1;
  if ( result < 1 )  result = 1.0;
  if ( result > ( 2 * cardinality - 1 ) ) result = 2 * cardinality - 1; 
  if ( ! ( static_cast<UInt32>( result ) & 1 ) ) result += 1;
  return static_cast<T>( result * divide );
}

/** 
 * Applies a uniform quantization to a range with 
 * cardinality values among the value domain. This 
 * is a specialization for double values.
 * 
 * @param value A value.
 * @param cardinality The required cardinality of the target range.
 * 
 * @return A quantized value.
 */
template<>
inline double
quantize<double>( double value, UInt32 cardinality )
{
  UInt32 l = static_cast<UInt32>( 1.0 + ( value / ( std::numeric_limits<double>::max() / cardinality ) ) ) ;
  return l * ( std::numeric_limits<double>::max() / cardinality );
}

/** 
 * Applies a uniform quantization to a range with 
 * cardinality values among the value domain. This 
 * is a specialization for float values.
 * 
 * @param value A value.
 * @param cardinality The required cardinality of the target range.
 * 
 * @return A quantized value.
 */
template<>
inline float
quantize<float>( float value, UInt32 cardinality )
{
  float l =  1 + ( value / ( std::numeric_limits<float>::max() / cardinality ) );
  return static_cast<UInt32>( l ) * ( std::numeric_limits<float>::max() / cardinality );
}

/** 
 * 
 * 
 * @return 
 */
template<typename U, typename V>
class LexicoLess {
 public:
  bool operator()( const std::pair<U,V> a, const std::pair<U,V> b )
  {
    return ( ( a.first < b.first ) && 
	     ( a.first == b.first && a.second < b.second ) );
  }
};

/** 
 * 
 * @return 
 */
template<typename U, typename V>
class LexicoGreater {
 public:
  bool operator()( const std::pair<U,V> a, const std::pair<U,V> b )
  {
    return ( ( a.first > b.first ) && 
	     ( a.first == b.first && a.second > b.second ) );
  }
};

#endif /* ifdef _TOOLS_H_ */
