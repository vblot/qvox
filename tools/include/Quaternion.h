/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   Quaternion.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2007
 * 
 * @brief  Declaration of the class Quaternion
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _QUATERNION_H_
#define _QUATERNION_H_

#include "Matrix.h"
#include <iostream>

/**
 *  The Quaternion class.
 */
class Quaternion {

 public:
  /**
   * Constructor with no arguments.
   */
   inline Quaternion( double a = 0.0, double b = 0.0, double c = 0.0, double d = 0.0 );

   inline Quaternion( const Quaternion & other );

   Quaternion & operator*=( const Quaternion & other );

   Quaternion operator*( const Quaternion & other ) const;

   Quaternion & operator=( const double x ) {
      _values[0] = _values[1] = _values[2] = _values[3] = x;
      return *this;
   }

   inline Quaternion & set( double a = 0.0,
			    double b = 0.0,
			    double c = 0.0,
			    double d = 0.0 ); 
   
   void normalize();

   Quaternion normalized() const;

   Matrix rotationMatrix() const;

   void rotationMatrix( Matrix & ) const;

   void setRotation( double axis[3], double angle );

   void setRotation( Triple<double> axis, double angle );

   static Quaternion rotation( double axis[3], double angle );

   static Quaternion rotation( Triple<double> axis, double angle );

   static const Triple<double> AxisX;
   static const Triple<double> AxisY;
   static const Triple<double> AxisZ;

   std::istream & readFrom( std::istream & in );

   std::ostream & flush( std::ostream & out ) const;

 protected:

 private:
   double _values[4];
};


std::ostream & operator<<( std::ostream & out, const Quaternion & q );

std::istream & operator>>( std::istream & in, Quaternion & q );

Quaternion::Quaternion( double a, double b, double c, double d )
{
   _values[0] = a;
   _values[1] = b;
   _values[2] = c;
   _values[3] = d;
}

inline
Quaternion::Quaternion( const Quaternion & other )
{
   _values[0] = other._values[0];
   _values[1] = other._values[1];
   _values[2] = other._values[2];
   _values[3] = other._values[3];
}


Quaternion &
Quaternion::set( double a, double b, double c, double d )
{
   _values[0] = a;
   _values[1] = b;
   _values[2] = c;
   _values[3] = d;
   return *this;
}

#endif // _QUATERNION_H_
