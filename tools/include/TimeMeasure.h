/** -*- mode: c++ -*-
 * @file   TimeMeasure.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2013
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _TIMEMEASURE_H_
#define _TIMEMEASURE_H_

#ifndef _RELEASE_

#include <map>
#include <vector>
#include <string>

class TimeMeasure {
public:
  TimeMeasure();
  ~TimeMeasure();

  void setFilename( const std::string & );

  void start( const std::string & name );
  void stop( const std::string & name );


private:

  static unsigned long getTime();

  std::string _filename;
  std::map< std::string, long > _starts;
  std::map< std::string, std::vector<long> > _durations;
};


extern TimeMeasure globalTimeMeasure;

#define TIME_MEASURE_START( NAME ) globalTimeMeasure.start(NAME)
#define TIME_MEASURE_STOP( NAME ) globalTimeMeasure.stop(NAME)


#else

#define TIME_MEASURE_START( NAME )
#define TIME_MEASURE_STOP( NAME )

#endif

#endif /* ifdef _TIMEMEASURE_H_ */
