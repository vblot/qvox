/** -*- mode: c++ -*-
 * @file   bit_ref.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:16:35 2005
 * 
 * @brief  Bit reference class.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */ 
#ifndef _BIT_REF_H_
#define _BIT_REF_H_
#include <iostream>
#include <cstdlib>
#include "globals.h"

template <bool big_endian>
struct char2word_shift {
  static int shift( const int bit, const UChar & c, const UInt & word ) {
    // This is never called. Only specialized versions are used.
    throw -1;
    return -1;
  }
};

template <>
struct char2word_shift<true> {
  inline static int shift( const int bit, const UChar & c, const UInt & word ) {
    return bit + ((3 - ((&c) - reinterpret_cast<const UChar*>(&word)) )<<3);
  }
};

template <>
struct char2word_shift<false> {
  inline static int shift( const int bit, const UChar & c, const UInt & word ) {
    return bit + (((&c) - reinterpret_cast<const UChar*>(&word))<<3);
  }
};

/**
 * A BitRef acts as a reference to a given bit in an unsigned int. 
 * 
 */
class BitRef {  
 public:
  BitRef() : _word( _dummy ), _mask(1) {
    std::cerr << "Warning: BitRef::BitRef() should never be called.\n";
  }

  BitRef( const BitRef & other )
    : _word( const_cast<UInt&>(other._word)), _mask(other._mask) { }
  
  BitRef( UInt & word, int bit )
    : _word(word), _mask(1ul<<bit) { }

  BitRef( Int & word, int bit )
    : _word(reinterpret_cast<UInt&>(word)), _mask(1ul<<bit) { }

  BitRef( UChar & c, int bit )
    : _word( * reinterpret_cast<UInt*>( (reinterpret_cast<size_t>(&c)>>2)<<2 ) ) {
    _mask = 1ul << char2word_shift< _IS_BIG_ENDIAN_ >::shift( bit, c, _word );
  }
  
  BitRef & operator=( const BitRef & other ) {
    if ( other._word & other._mask )
      _word |= _mask;
    else
      _word &= ~_mask;
    return *this;
  }
  
  BitRef & operator=( const Bool & b  ) {
    if ( b ) 
      _word |= _mask;
    else
      _word &= ~_mask;
    return *this;
  }
  
  operator bool() const {
    return _word & _mask;
  }

  bool operator!() const {
    return !( _word & _mask );
  }

 private:

  UInt & _word;		 /**< Reference to the word containing the bit */
  UInt _mask;		 /**< Position of the bit */
  static UInt _dummy;
  friend class ConstBitRef;
};

/**
 * A ConstBitRef acts as a const reference to a given bit in an unsigned int. 
 * 
 */
class ConstBitRef {  

 public:

  ConstBitRef() : _word( _dummy ), _mask(1) {
    std::cerr << "Warning: ConstBitRef::ConstBitRef() should never be called.\n";
  }

  ConstBitRef( const ConstBitRef & other )
    : _word(other._word), _mask(other._mask) { }

  ConstBitRef( const BitRef & br )
    : _word(br._word), _mask(br._mask) { }
  
  ConstBitRef( const UInt & word, int bit )
    : _word(word), _mask(1ul<<bit) { }

  ConstBitRef( const Int & word, int bit )
    : _word(reinterpret_cast<const UInt&>(word)), _mask(1ul<<bit) { }

  ConstBitRef( const UChar & c, int bit )
    : _word( * reinterpret_cast<const UInt*>( (reinterpret_cast<size_t>(&c)>>2)<<2 ) ) {
    _mask = 1ul << char2word_shift< _IS_BIG_ENDIAN_ >::shift( bit, c, _word );
  }

  operator bool() const {
    return _word & _mask;
  }

  bool operator!() const {
    return !( _word & _mask );
  }
  
 private:
  
  ConstBitRef & operator=( const ConstBitRef & ) {
    
    return *this;
  }
  
  ConstBitRef & operator=( const Bool & ) {
    return *this; 
  }

  const UInt & _word;	 /**< Reference to the word containing the bit */
  UInt _mask;		 /**< Position of the bit */
  static const UInt _dummy = 0;
};

#endif // _BIT_REF_H_
