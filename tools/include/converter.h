/** -*- mode: c++ -*-
 * @file   converters.h
 * @author Sebastien Fourey (GREYC)
 * @date   Feb 2006
 * 
 * @brief  Class template of a Triple of elements.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _CONVERTER_H_
#define _CONVERTER_H_

#include <iostream>
#include <limits>
#include "globals.h"

/*
 *  Convertible (numerical) types.
 */

template < typename FROM, typename TO >
struct convertible {
  static const bool valid = std::numeric_limits<FROM>::is_specialized
    && std::numeric_limits<TO>::is_specialized;
};

template <typename T>
struct convertible< T, T > {
  static const bool valid = true;
};

/*
 * Effective converter.
 */

template < typename FROM, typename TO, bool CONVERTIBLE >
struct effective_converter {
  static inline TO apply( const FROM &  ) { 
    std::cerr << "Error: Unspecialized effective_converter should never be applied.\n";
    return TO();
  }
};

template < typename FROM, typename TO >
struct effective_converter< FROM, TO, true > {
  static inline TO apply( const FROM & x ) { 
    return static_cast<TO>( x );
  }
};

template < typename FROM, typename TO >
struct effective_converter< FROM, TO, false > {
  static inline TO apply( const FROM &  ) { 
    std::cerr << "Warning: effective_converter called for non-numerical types.\n";
    return TO();
  }
};

/*
 * Converter
 */

template < typename FROM, typename TO >
struct converter {
  static inline TO apply( const FROM & x ) { 
    return effective_converter< FROM, TO, convertible<FROM,TO>::valid >::apply( x );
  }
};

template < typename T >
struct converter < T, T > {
  static inline T apply( const T & x ) { 
    return x;
  }
};


#endif // _CONVERTER_H_
