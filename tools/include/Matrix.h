/** -*- mode: c++ -*-
 * @file   Matrix.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2005
 *
 * @brief  Matrix operations.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _MATRIX_H_
#define _MATRIX_H_
#include <cstring>
#include <ostream>
#include <iomanip>
#include <Triple.h>

/**
 * Type synonym for a matrix
 */
struct Matrix {
  double a11,a12,a13;
  double a21,a22,a23;
  double a31,a32,a33;

  inline Matrix();
  inline Matrix( const Matrix & other );

  /**
   * Fills as an indentity matrix.
   */
  void identity();

  /**
   * Fills as a diagonal matrix
   */
  Matrix & setDiagonal( double x11, double x22, double x33 );
  Matrix & setRotationOx( double angle );
  Matrix & setRotationOy( double angle );
  Matrix & setRotationOz( double angle );
  Matrix & setRotation( double rho, double theta, double phi );
  Matrix & transpose();
  Matrix transposed();

  static Matrix rotation( double rho, double theta, double phi );

};

Matrix operator*( const Matrix & a, const Matrix & b );

/**
 * Multplies a 3x3 matrix with a vector.
 */

inline
Triple<double> operator*( const Matrix & m,
                          const Triple<double> & v )
{
  return makeTriple( v.first*m.a11 + v.second*m.a12 + v.third*m.a13,
                     v.first*m.a21 + v.second*m.a22 + v.third*m.a23,
                     v.first*m.a31 + v.second*m.a32 + v.third*m.a33 );
}

/**
 * Multiplies a matrix with a vector in the given vector.
 */
void mxMultiplyVectorOnPlace( Matrix m, Triple<double> & v );

/**
 * Outputs a matrix in a stream.
 */
std::ostream & operator<<( std::ostream & out, const Matrix & m );

/**
 * Computes the absolute difference between two matrices.
 *
 * @param a
 * @param b
 *
 * @return
 */
double mxDistance( Matrix a, Matrix b );

/*
 * Definitions of inline methods
 */

Matrix::Matrix() {
  memset( this, 0, 9 * sizeof( double ) );
}

Matrix::Matrix( const Matrix & other ) {
  (*this) = other;
}

#endif /* ifdef _MATRIX_H_ */
