/** -*- mode: c++ -*-
 * @file   Array.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2005
 * 
 * @brief  Bi-dimensional array template. (Data is organized rows after rows in
 * memory.)
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */ 
#ifndef _ARRAY_H_
#define _ARRAY_H_
#include <globals.h>
#include <tools.h>
#include <Pair.h>
#include <cstdlib>
#include <cstring>

/**
 * The Array<> class template definition
 * Warning: Data is organized rows after rows in memory.
 */
template<typename T>
class Array {  
 
 public:

  typedef T value_type;         /**< The type of the data. */

  /** 
   * Initialize a 2D array with a given size.
   * 
   * @param cols The number of columns.
   * @param rows  The number of rows.
   */
  Array( SSize cols = 0, SSize rows = 1 ); 
  
  /** 
   * Initialize a copy of another array.
   * 
   * @param other The array to be copied. 
   */
  Array( const Array<T> & other  );

  /** 
   * Frees the memory used by the array.
   */
  ~Array();
  
  /** 
   * Set all cells of the array with a given value.
   * 
   * @param value The value to which all the array cells should be set. 
   * 
   * @return A reference to the left object.
   */
  Array<T> & operator=( const T value );
  
  /** 
   * Resizes the array and copy values from another one.
   * 
   * @param right The array to be copied. 
   * 
   * @return A reference to the left object.
   */
  Array<T> & operator=( const Array<T> & right );

  /** 
   * Resizes the array and copy values from another one, cell
   * after cell with a cast.
   * 
   * @param right The array to be "cast copied".
   * 
   * @return A reference to the left objetc.
   */
  template< typename U > 
  Array<T> & operator=( const Array< U > & right );
  
  /** 
   * Allows access to a given row.
   * Warning: The data is indeed organized rows after rows in memory.
   * 
   * @param row The row to be returned.
   * 
   * @return A pointer to the row of value.
   */
  inline T * operator[] ( SSize row );

  /** 
   * Provides access to a given cell specified by a coordinates pair.
   * 
   * @param p The coordinate as (column,row).
   * 
   * @return A reference to the cell.
   */  
  inline T & operator[] ( const Pair<SSize> & p );
  
  /** 
   * Provides access to a given cell specified by a coordinates pair as
   * a constant ref.
   * 
   * @param p The coordinates as (column,row).
   * 
   * @return A const reference to the cell.
   */    
  inline const T & operator[] ( const Pair<SSize> & p ) const;

  /** 
   * Provides access to a given cell specified by a coordinates pair.
   * 
   * @param p The coordinates as (column,row).
   * 
   * @return A reference to the cell.
   */  
  inline T & operator() ( const Pair<SSize> & p );
  
  /** 
   * Provides access to a given cell specified by a coordinates pair as
   * a constant ref.
   * 
   * @param p The coordinate as (column,row).
   * 
   * @return A const reference to the cell.
   */
  inline const T & operator() ( const Pair<SSize> & p ) const;
  
  /** 
   * Provides access to a given cell specified by two coordinates.
   * 
   * @param col The column.
   * @param row The row.
   * 
   * @return A reference to the cell.
   */
  inline T & operator()( SSize col, SSize row = 0 );
  
  /** 
   * Provides access to a given cell specified by two coordinates as
   * a constant ref.
   * 
   * @param col The column.
   * @param row The row.
   * 
   * @return A const reference to the cell.
   */
  inline const T & operator()( SSize col, SSize row ) const;
  
  /** 
   * Zeros all the cells.
   */
  void clear();
  
  /** 
   * Resizes the array, destroying current contents.
   * 
   * @param cols The desired number of cols. 
   * @param rows The desired number of rows. 
   */
  void resize( SSize cols, SSize rows );

  /** 
   * Swaps the internal data with another array.
   * 
   * @param other The array from which data should be swaped.
   */
  void swapData( Array<T> & other );
  
  /** 
   * Creates and returns a copy of the object.
   *
   * @return A pointer to the new object.
   */
  Array<T> * clone() const;
  
  /** 
   * Returns the size of the array as a pair (columns,rows).
   * 
   * @return The size of the array (columns,rows).
   */
  inline Pair<SSize> size();
  
  /** 
   * Gets the size of the array.
   * 
   * @param cols Reference to the number of cols to be set.
   * @param rows Reference to the number of rows to be set.
   */
  inline void getSize( int & cols, int & rows );

  /** 
   * Returns the size of the internal 1-dimensional data array.
   * 
   * @return 
   */
  inline Size vectorSize() const;

  /** 
   * Returns the number of columns of the array.
   * 
   * @return The number of columns of the array.
   */
  inline SSize cols() const;
  
  /** 
   * Returns the number of rows of the array.
   * 
   * @return The number of rows of the array.
   */
  inline SSize rows() const;

  /** 
   * Checks whether a column belongs to the array.
   * 
   * @param c A column index.
   * @return true if the column is within the array width.
   */
  inline bool holds( SSize c );

  /** 
   * Checks whether a position falls within the array.
   * 
   * @param c A column index.
   * @param r A row index.
   * @return true if the postion belong to the array. 
   */
  inline bool holds( SSize x, SSize y );

  /** 
   * Provides access to the internal 1-dimensional array.
   *
   * @return The pointer to the first data cell.
   */
  inline T * data();
  
 protected:
  
  /** 
   * Allocate the internal data sctrucure for a given size. Previous 
   * structure is freed if needed.
   * 
   * @param cols The number of columns.
   * @param rows The number of rows.
   */
  void alloc( SSize cols, SSize rows ) throw ();
  
  /** 
   * Deallocates the internal data structure.
   */
  void free();

  T *_data;			/**< Array of pointers to each row. */
  SSize _cols;			/**< Number of columns. */
  SSize _rows;			/**< Number of rows. */
};

#include "Array.ih"

#endif /* _ARRAY_H_ */
