/** -*- mode: c++ -*-
 * @file   Pair.h
 * @author Sebastien Fourey (GREYC)
 * @date   Feb 2006
 *
 * @brief  Class template of a Pair of elements.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _PAIR_H_
#define _PAIR_H_

#include <iostream>
#include <cmath>
#include "converter.h"

/**
 * Pair struct template
 *
 */
template< typename T >
struct Pair {

 public:

  T first, second;

  /**
   * Defaut constructor with no arguments.
   */
  Pair():first(T()), second(T()) { }

  /**
   * Copy constructor.
   */
  Pair( const Pair<T> & other )
  :first(other.first), second(other.second) {
  }

  /**
   * Constructor with two arguments.
   *
   * @param first
   * @param second
   */
  Pair( const T & first, const T & second )
  :first(first), second(second) {
  }

  template< typename U >
  Pair( const Pair<U> & other ) {
    *this = other;
  }

  Pair( const T * array ) {
    first = *(array++);
    second = *array;
  }

  Pair & operator=( const T & value ) {
    first = second =value;
    return *this;
  }

  template< typename U >
  Pair & operator=( const Pair<U> & other ) {
//     first = static_cast<T>( other.first );
//     second = static_cast<T>( other.second );
    first = other.first;
    second = other.second;
    return *this;
  }

  Pair<Float> roundf() const {
    return Pair<Float>( std::floor( 0.5f + converter<T,Float>::apply( first ) ),
                        std::floor( 0.5f + converter<T,Float>::apply( second ) ) );
  }

  Pair<Double> round() const {
    return Pair<Double>( std::floor( 0.5f + converter<T,Double>::apply( first ) ),
                         std::floor( 0.5f + converter<T,Double>::apply( second ) ) );
  }

  template<typename TO>
  Pair<TO> to() const {
    return Pair<TO>( converter<T,TO>::apply( first ),
                     converter<T,TO>::apply( second ) );
  }

};

template< typename T>
std::ostream & operator<<( std::ostream & out, const Pair< T > & pair )
{
  out << "(" << pair.first << "," << pair.second << ")";
  return out;
}

template< typename T >
inline bool operator==( const Pair<T> & a, const Pair<T> & b )  {
  return ( a.first == b.first ) && ( a.second == b.second );
}

template< typename T >
inline bool operator!= ( const Pair<T> & a, const Pair<T> & b ) {
  return ( a.first != b.first ) || ( a.second != b.second );
}

template< typename T >
inline bool operator< ( const Pair<T> & a, const Pair<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first && a.second < b.second ) return true;
  return false;
}

template< typename T >
inline bool operator<= ( const Pair<T> & a, const Pair<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first && a.second <= b.second ) return true;
  return false;
}

template< typename T >
inline bool operator> ( const Pair<T> & a, const Pair<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first &&  a.second > b.second ) return true;
  return false;
}

template< typename T >
inline bool operator>= ( const Pair<T> & a, const Pair<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first && a.second >= b.second ) return true;
  return false;
}

template<typename T>
inline Pair<T> makePair( const T & first, const T & second )
{
  return Pair<T>( first, second );
}



/*
 * Arithmetical operations
 */

template< typename T >
inline Pair<T> operator+( const Pair<T> & a, const Pair<T> & b )
{
  return Pair<T>( a.first + b.first, a.second + b.second );
}

template< typename T >
inline Pair<T> & operator+=( Pair<T> & a, const Pair<T> & b )
{
  a.first += b.first;
  a.second += b.second;
  return a;
}

template< typename T >
inline Pair<T> operator-( const Pair<T> & a, const Pair<T> & b )
{
  return Pair<T>( a.first - b.first, a.second - b.second );
}

template< typename T >
inline Pair<T> & operator-=( Pair<T> & a, const Pair<T> & b )
{
  a.first -= b.first;
  a.second -= b.second;
  return a;
}

template< typename T >
inline Pair<T> operator*( const Pair<T> & a, float x )
{
  return Pair<T>( a.first * x, a.second * x );
}

/**
 * Dot product.
 *
 * @param a
 * @param b
 *
 * @return
 */
template< typename T >
inline T operator*( const Pair<T> & a, const Pair<T> & b )
{
  return ( a.first * b.first + a.second * b.second );
}

template< typename T >
inline Pair<T> operator*( float x, const Pair<T> & a )
{
  return Pair<T>( x * a.first, x * a.second );
}

template< typename T >
inline Pair<T> & operator*=( Pair<T> & a, float x )
{
  a.first *= x;
  a.second *= x;
  return a;
}

template< typename T >
inline Pair<T> operator/( const Pair<T> & a, float x )
{
  return Pair<T>( a.first / x, a.second / x );
}

template< typename T >
inline Pair<T> & operator/=( Pair<T> & a, float x )
{
  a.first /= x;
  a.second /= x;
  return a;
}

template< typename T >
inline float abs( const Pair<T> & t )
{
  return sqrt( ( t.first * t.first ) + ( t.second * t.second ) );
}

template< typename T >
inline void normalize( Pair<T> & p )
{
  float norm;
  norm = std::sqrt( ( p.first * p.first ) + ( p.second * p.second ) );
  if ( norm < 0.0001 ) {
    p = 0.0f;
    return;
  }
  p.first /= norm;
  p.second /= norm;
}

template< typename T >
Pair<T>
normalized( const Pair<T> & p )
{
  float norm;
  norm = std::sqrt( ( p.first * p.first ) + ( p.second * p.second ) );
  if ( norm < 0.001 ) return Pair<T>(0.0f, 0.0f);
  return Pair<T>( p.first / norm, p.second / norm );
}

template< typename T >
Pair<float>
roundf( const Pair<T> & p )
{
  return Pair<float>( std::floor( p.first + 0.5f ),
          std::floor( p.second + 0.5f ) );
}


template< typename T >
Pair<double>
round( const Pair<T> & p )
{
  return Pair<double>( std::floor( p.first + 0.5 ),
                       std::floor( p.second + 0.5 ) );
}

#endif /* ifdef _PAIR_H_ */

