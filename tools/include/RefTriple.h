/** -*- mode: c++ -*-
 * @file   RefTriple.h
 * @author Sebastien Fourey (GREYC)
 * @date   Feb 2006
 * 
 * @brief  Class template of a Triple of elements.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _REFTRIPLE_H_
#define _REFTRIPLE_H_

#include <cmath>
#include <iostream>
#include "Triple.h"
#include "converter.h"

template< typename T >
struct RefTriple { 
  T & first;
  T & second;
  T & third;
  static T dummy;

  RefTriple():first(dummy),second(dummy),third(dummy) {
    std::cerr << "Warning: RefTriple() default constructor has been called!\n";
  }

  RefTriple( T & first, 
	     T & second, 
	     T & third )
  : first(first), second(second), third(third) { }
  
  RefTriple( const RefTriple<T> & other )
    : first( const_cast<T&>(other.first) ),
      second( const_cast<T&>(other.second) ),
      third( const_cast<T&>(other.third) ) {
  }

  RefTriple( Triple<T> & t )
    : first( t.first ),
      second( t.second ),
      third( t.third ) {
  }
  
  RefTriple( T * array )
    : first(array[0]),second(array[1]),third(array[2]) {
  }

  const RefTriple<T> & operator=( const Triple<T> & other ) {
    first = other.first;
    second = other.second;
    third = other.third;
    return *this;
  }

  template < typename U >
  const RefTriple<T> & operator=( const Triple<U> & other ) {
    first = static_cast<T>( other.first );
    second = static_cast<T>( other.second );
    third = static_cast<T>( other.third );
    return *this;
  }
  
  RefTriple<T> & operator=( const T & value ) {
    first = second = third = value;
    return *this;
  }

  void set( const T & first, const T & second, const T & third ) {
    RefTriple<T>::first = first;
    RefTriple<T>::second = second;
    RefTriple<T>::third = third;
  }
  
  Triple<T> copy() const { 
    return Triple<T>( first, second, third );
  }
  
  operator Triple<T>() const { 
    return Triple<T>( first, second, third );
  }

private:
  RefTriple<T> & operator=( const RefTriple<T> & ) { }
};

template< typename T>
std::ostream & 
operator<<( std::ostream & out, const RefTriple< T > & rt )
{
  out << "R(" << rt.first
      << "," << rt.second
      << "," << rt.third << ")";
  return out;
}

template< typename T >
std::istream & operator>>( std::istream & in, RefTriple< T > & t )
{
  return in >> t.first >> t.second >> t.third;
}

template< typename T >
inline bool operator==( const RefTriple<T> & a, const RefTriple<T> & b )  {
  return ( a.first == b.first )
    && ( a.second == b.second )
    && ( a.third == b.third );
}

template< typename T >
inline bool operator==( const Triple<T> & a, const RefTriple<T> & b )  {
  return ( a.first == b.first )
    && ( a.second == b.second )
    && ( a.third == b.third );
}

template< typename T >
inline bool operator==( const RefTriple<T> & a, const Triple<T> & b )  {
  return ( a.first == b.first )
    && ( a.second == b.second )
    && ( a.third == b.third );
}

template< typename T >
inline bool operator!= ( const RefTriple<T> & a, const RefTriple<T> & b ) {
  return ( a.first != b.first ) 
    || ( a.second != b.second )
    || ( a.third != b.third );
}

template< typename T >
inline bool operator!= ( const Triple<T> & a, const RefTriple<T> & b ) {
  return ( a.first != b.first ) 
    || ( a.second != b.second )
    || ( a.third != b.third );
}

template< typename T >
inline bool operator!= ( const RefTriple<T> & a, const Triple<T> & b ) {
  return ( a.first != b.first ) 
    || ( a.second != b.second )
    || ( a.third != b.third );
}

template< typename T >
inline bool operator< ( const RefTriple<T> & a, const RefTriple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third < b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator< ( const RefTriple<T> & a, const Triple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third < b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator< ( const Triple<T> & a, const RefTriple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third < b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator<= ( const RefTriple<T> & a, const RefTriple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third <= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator<= ( const Triple<T> & a, const RefTriple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third <= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator<= ( const RefTriple<T> & a, const Triple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third <= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator> ( const RefTriple<T> & a, const RefTriple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third > b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator> ( const Triple<T> & a, const RefTriple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third > b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator> ( const RefTriple<T> & a, const Triple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third > b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator>= ( const RefTriple<T> & a, const RefTriple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third >= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator>= ( const Triple<T> & a, const RefTriple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third >= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator>= ( const RefTriple<T> & a, const Triple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third >= b.third ) return true;
  }
  return false;
}

/* Arithmetical operators */

template< typename T > 
inline Triple<T> operator+( const RefTriple<T> & a, const RefTriple<T> & b )
{
  return Triple<T>( a.first + b.first,
		    a.second + b.second,
		    a.third + b.third );
}

template< typename T > 
inline Triple<T> operator+( const Triple<T> & a, const RefTriple<T> & b )
{
  return Triple<T>( a.first + b.first,
		    a.second + b.second,
		    a.third + b.third );
}

template< typename T > 
inline Triple<T> operator+( const RefTriple<T> & a, const Triple<T> & b )
{
  return Triple<T>( a.first + b.first,
		    a.second + b.second,
		    a.third + b.third );
}

template< typename T > 
inline RefTriple<T> & operator+=( RefTriple<T> & a, const Triple<T> & b )
{
  a.first += b.first;
  a.second += b.second;
  a.third += b.third;
  return a;
}

template< typename T > 
inline RefTriple<T> & operator-=( RefTriple<T> & a, const Triple<T> & b )
{
  a.first -= b.first;
  a.second -= b.second;
  a.third -= b.third;
  return a;
}

template< typename T > 
inline RefTriple<T> & operator*=( RefTriple<T> & a, const T & x )
{
  a.first *= x;
  a.second *= x;
  a.third *= x;
  return a;
}

template< typename T > 
inline RefTriple<T> & operator/=( RefTriple<T> & a, const T & x )
{
  a.first /= x;
  a.second /= x;
  a.third /= x;
  return a;
}

template< typename T > 
inline Triple<T> operator-( const RefTriple<T> & a )
{
  return Triple<T>( -a.first, -a.second, -a.third );
}

template< typename T > 
inline Triple<T> operator-( const RefTriple<T> & a, const RefTriple<T> & b )
{
  return Triple<T>( a.first - b.first, a.second - b.second, a.third - b.third );
}

template< typename T > 
inline Triple<T> operator-( const Triple<T> & a, const RefTriple<T> & b )
{
  return Triple<T>( a.first - b.first, a.second - b.second, a.third - b.third );
}

template< typename T > 
inline Triple<T> operator-( const RefTriple<T> & a, const Triple<T> & b )
{
  return Triple<T>( a.first - b.first, a.second - b.second, a.third - b.third );
}

template< typename T > 
inline T operator*( const RefTriple<T> & a, const RefTriple<T> & b )
{
  return ( a.first * b.first + a.second * b.second + a.third * b.third );
}

template< typename T > 
inline T operator*( const Triple<T> & a, const RefTriple<T> & b )
{
  return ( a.first * b.first + a.second * b.second + a.third * b.third );
}

template< typename T > 
inline T operator*( const RefTriple<T> & a, const Triple<T> & b )
{
  return ( a.first * b.first + a.second * b.second + a.third * b.third );
}

template< typename T > 
inline Triple<T> operator*( const RefTriple<T> & a, const T & x )
{
  return Triple<T>( a.first * x, a.second * x, a.third * x );
}

template< typename T > 
inline Triple<T> operator*( const T & x, const RefTriple<T> & a )
{
  return Triple<T>( x * a.first, x * a.second, x * a.third );
}

template< typename T > 
inline Triple<T> operator/( const RefTriple<T> & a, const T & x )
{
  return Triple<T>( a.first / x, a.second / x, a.third / x );
}

template< typename T > 
double abs( const RefTriple<T> & t )
{
  return sqrt( t.first * t.first
	       + t.second * t.second
	       + t.third * t.third );
}

template< typename T > 
float fabs( const RefTriple<T> & t )
{
  return sqrtf( t.first * t.first
		+ t.second * t.second
		+ t.third * t.third );
}

template< typename T >
inline double norm( const RefTriple<T> & t )
{
  return sqrt( t.first * t.first 
	       + t.second * t.second
	       + t.third * t.third );
}

template< typename T >
inline double distance( const RefTriple<T> & a,
			const RefTriple<T> & b )
{
  double dx = b.first - a.first;
  double dy = b.second - a.second;
  double dz = b.third - a.third;
  return std::sqrt( dx * dx + dy * dy + dz * dz );
}

template< typename T >
inline double distance( const Triple<T> & a,
			const RefTriple<T> & b )
{
  double dx = b.first - a.first;
  double dy = b.second - a.second;
  double dz = b.third - a.third;
  return std::sqrt( dx * dx + dy * dy + dz * dz );
}

template< typename T >
inline double distance( const RefTriple<T> & a,
			const Triple<T> & b )
{
  double dx = b.first - a.first;
  double dy = b.second - a.second;
  double dz = b.third - a.third;
  return std::sqrt( dx * dx + dy * dy + dz * dz );
}

inline float distance( const RefTriple<float> & a,
		       const RefTriple<float> & b )
{
  float dx = b.first - a.first;
  float dy = b.second - a.second;
  float dz = b.third - a.third;
  return sqrtf( dx * dx + dy * dy + dz * dz );
}

inline float distance( const Triple<float> & a,
		       const RefTriple<float> & b )
{
  float dx = b.first - a.first;
  float dy = b.second - a.second;
  float dz = b.third - a.third;
  return sqrtf( dx * dx + dy * dy + dz * dz );
}

inline float distance( const RefTriple<float> & a,
		       const Triple<float> & b )
{
  float dx = b.first - a.first;
  float dy = b.second - a.second;
  float dz = b.third - a.third;
  return sqrtf( dx * dx + dy * dy + dz * dz );
}

inline void normalize( RefTriple<float> & t )
{
  float norm = sqrt( t.first * t.first
		     + t.second * t.second
		     + t.third * t.third );
  if ( norm < 0.00001 ) { 
    t.first = t.second = t.third = 0.0f;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

inline void normalize( RefTriple<double> & t )
{
  double norm = sqrt( t.first * t.first
		     + t.second * t.second
		     + t.third * t.third );
  if ( norm < 0.00001 ) { 
    t.first = t.second = t.third = 0.0;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

inline void normalize( RefTriple<long double> & t )
{
  long double norm = sqrt( t.first * t.first
			   + t.second * t.second
			   + t.third * t.third );
  if ( norm < 0.00001 ) { 
    t.first = t.second = t.third = 0.0;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

template< typename T >
Triple<T> normalized( const RefTriple<T> & t )
{
  return normalized( t.copy() );
}

template< typename T >
Triple<float> roundf( const RefTriple<T> & t )
{
  return Triple<float>( roundf( t.first ),
			roundf( t.second ),
			roundf( t.third ) );
}

template< typename T >
Triple<double> round( const RefTriple<T> & t )
{
  return round( t.copy() );
}

template< typename T >
void lower( RefTriple<T> & t, const Triple<T> & other  )
{
  if ( t.first > other.first ) t.first = other.first;
  if ( t.second > other.second ) t.second = other.second;
  if ( t.third > other.third ) t.third = other.third;
}

template< typename T >
void raise( RefTriple<T> & t, const Triple<T> & other  )
{
  if ( t.first < other.first ) t.first = other.first;
  if ( t.second < other.second ) t.second = other.second;
  if ( t.third < other.third ) t.third = other.third;
}

template<typename T>
inline RefTriple<T> make_ref_triple( T & first, T & second, T & third )
{
  return RefTriple<T>( first, second, third );
}

template<typename T>
Triple<T> wedgeProduct( const RefTriple<T> & a, const RefTriple<T> & b )
{
  Triple<T> r;
  r.first = a.second * b.third - b.second * a.third;
  r.second = b.first * a.third - a.first * b.third;
  r.third = a.first * b.second - b.first * a.second;
  return r;
} 

template<typename T>
Triple<T> wedgeProduct( const Triple<T> & a, const RefTriple<T> & b )
{
  Triple<T> r;
  r.first = a.second * b.third - b.second * a.third;
  r.second = b.first * a.third - a.first * b.third;
  r.third = a.first * b.second - b.first * a.second;
  return r;
} 

template<typename T>
Triple<T> wedgeProduct( const RefTriple<T> & a, const Triple<T> & b )
{
  Triple<T> r;
  r.first = a.second * b.third - b.second * a.third;
  r.second = b.first * a.third - a.first * b.third;
  r.third = a.first * b.second - b.first * a.second;
  return r;
} 

template<typename T>
inline Triple<T> operator^( const RefTriple<T> & a, const RefTriple<T> & b )
{
  return wedgeProduct(a,b);
} 

template<typename T>
inline Triple<T> operator^( const Triple<T> & a, const RefTriple<T> & b )
{
  return wedgeProduct(a,b);
} 

template<typename T>
inline Triple<T> operator^( const RefTriple<T> & a, const Triple<T> & b )
{
  return wedgeProduct(a,b);
} 

template<typename T>
Triple<T> scale( const RefTriple<T> & a, const RefTriple<T> & b )
{
  return Triple<T>( a.first * b.first,
		    a.second * b.second,
		    a.third * b.third );
} 

template<typename T>
inline bool nonzero( const RefTriple<T> & a )
{
  return a.first|a.second|a.third;
} 

template<>
inline bool nonzero<Float>( const RefTriple<Float> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
} 

template<>
inline bool nonzero<Double>( const RefTriple<Double> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
} 

template<>
inline bool nonzero<long double>( const RefTriple<long double> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
} 

template<typename T>
inline bool isZero( const RefTriple<T> & a )
{
  return ! static_cast<bool>( a.first|a.second|a.third );
} 

template<>
inline bool isZero<Float>( const RefTriple<Float> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
} 

template<>
inline bool isZero<Double>( const RefTriple<Double> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
} 

template<>
inline bool isZero<long double>( const RefTriple<long double> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
} 

template< typename T >
T RefTriple<T>::dummy;

#endif // _REFTRIPLE_H_
