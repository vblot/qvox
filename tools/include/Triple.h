/** -*- mode: c++ -*-
 * @file   Triple.h
 * @author Sebastien Fourey (GREYC)
 * @date   Feb 2006
 *
 * @brief  Class template of a Triple of elements.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _TRIPLE_H_
#define _TRIPLE_H_

#include <cmath>
#include <iostream>
#include "converter.h"

template< typename T >
struct Triple {
  T first;
  T second;
  T third;

  Triple():first(T()), second(T()), third(T()) { }

  Triple( const T & first,
          const T & second,
          const T & third )
    : first(first), second(second), third(third) { }

  Triple( const T * array ) {
    first = *(array++);
    second = *(array++);
    third = *array;
  }

  template < typename U >
  Triple( const Triple<U> & other ) {
    (*this) = other;
  }

  template < typename U >
  const Triple<T> & operator=( const Triple<U> & other ) {
    //     first = static_cast<T>( other.first );
    //     second = static_cast<T>( other.second );
    //     third = static_cast<T>( other.third );
    first = other.first;
    second = other.second;
    third = other.third;
    return *this;
  }

  Triple<T> & operator=( const T & value ) {
    first = second = third = value;
    return *this;
  }

  void set( const T & first, const T & second, const T & third ) {
    Triple<T>::first = first;
    Triple<T>::second = second;
    Triple<T>::third = third;
  }

  Triple<Float> roundf() const {
    return Triple<Float>( std::floor( 0.5f + converter<T,Float>::apply( first ) ),
                          std::floor( 0.5f + converter<T,Float>::apply( second ) ),
                          std::floor( 0.5f + converter<T,Float>::apply( third ) ) );
  }

  Triple<Double> round() const {
    return Triple<Double>( std::floor( 0.5f + converter<T,Double>::apply( first ) ),
                           std::floor( 0.5f + converter<T,Double>::apply( second ) ),
                           std::floor( 0.5f + converter<T,Double>::apply( third ) ) );
  }


  template<typename TO>
  Triple<TO> to() const {
    return Triple<TO>( converter<T,TO>::apply( first ),
                       converter<T,TO>::apply( second ),
                       converter<T,TO>::apply( third ) );
  }


  Triple<Int> toInt() const {
    return Triple<Int>( converter<T,Int>::apply( first ),
                        converter<T,Int>::apply( second ),
                        converter<T,Int>::apply( third ) );
  }

  Triple<UInt> toUInt() const {
    return Triple<UInt>( converter<T,UInt>::apply( first ),
                         converter<T,UInt>::apply( second ),
                         converter<T,UInt>::apply( third ) );
  }

  Triple<Int32> toInt32() const {
    return Triple<Int32>( converter<T,Int32>::apply( first ),
                          converter<T,Int32>::apply( second ),
                          converter<T,Int32>::apply( third ) );
  }

  Triple<UInt32> toUInt32() const {
    return Triple<UInt32>( converter<T,UInt32>::apply( first ),
                           converter<T,UInt32>::apply( second ),
                           converter<T,UInt32>::apply( third ) );
  }

  Triple<Float> toFloat() const {
    return Triple<Float>( converter<T,Float>::apply( first ),
                          converter<T,Float>::apply( second ),
                          converter<T,Float>::apply( third ) );
  }

  Triple<Double> toDouble() const {
    return Triple<Double>( converter<T,Double>::apply( first ),
                           converter<T,Double>::apply( second ),
                           converter<T,Double>::apply( third ) );
  }

};

template< typename T>
std::ostream &
operator<<( std::ostream & out, const Triple< T > & triple )
{
  out << "(" << triple.first
      << "," << triple.second
      << "," << triple.third << ")";
  return out;
}

template< typename T >
std::istream & operator>>( std::istream & in, Triple< T > & t )
{
  return in >> t.first >> t.second >> t.third;
}

template< typename T >
inline bool operator==( const Triple<T> & a, const Triple<T> & b )  {
  return ( a.first == b.first )
      && ( a.second == b.second )
      && ( a.third == b.third );
}

template< typename T >
inline bool operator!= ( const Triple<T> & a, const Triple<T> & b ) {
  return ( a.first != b.first )
      || ( a.second != b.second )
      || ( a.third != b.third );
}

template< typename T >
inline bool operator< ( const Triple<T> & a, const Triple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third < b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator<= ( const Triple<T> & a, const Triple<T> & b ) {
  if ( a.first < b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second < b.second ) return true;
    if ( a.second == b.second && a.third <= b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator> ( const Triple<T> & a, const Triple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third > b.third ) return true;
  }
  return false;
}

template< typename T >
inline bool operator>= ( const Triple<T> & a, const Triple<T> & b ) {
  if ( a.first > b.first ) return true;
  if ( a.first == b.first ) {
    if ( a.second > b.second ) return true;
    if ( a.second == b.second && a.third >= b.third ) return true;
  }
  return false;
}

/*
 * Arithmetical operators
 */

template< typename T >
inline Triple<T> operator+( const Triple<T> & a, const Triple<T> & b )
{
  return Triple<T>( a.first + b.first,
                    a.second + b.second,
                    a.third + b.third );
}

template< typename T >
inline Triple<T> & operator+=( Triple<T> & a, const Triple<T> & b )
{
  a.first += b.first;
  a.second += b.second;
  a.third += b.third;
  return a;
}

template< typename T >
inline Triple<T> operator-( const Triple<T> & a )
{
  return Triple<T>( -a.first, -a.second, -a.third );
}

template< typename T >
inline Triple<T> operator-( const Triple<T> & a, const Triple<T> & b )
{
  return Triple<T>( a.first - b.first, a.second - b.second, a.third - b.third );
}

template< typename T >
inline Triple<T> & operator-=( Triple<T> & a, const Triple<T> & b )
{
  a.first -= b.first;
  a.second -= b.second;
  a.third -= b.third;
  return a;
}

template< typename T >
inline T operator*( const Triple<T> & a, const Triple<T> & b )
{
  return ( a.first * b.first + a.second * b.second + a.third * b.third );
}

template< typename T >
inline Triple<T> operator*( const Triple<T> & a, const T & x )
{
  return Triple<T>( a.first * x, a.second * x, a.third * x );
}

template< typename T >
inline Triple<T> operator*( const T & x, const Triple<T> & a )
{
  return Triple<T>( x * a.first, x * a.second, x * a.third );
}

template< typename T >
inline Triple<T> & operator*=( Triple<T> & a, const T & x )
{
  a.first *= x;
  a.second *= x;
  a.third *= x;
  return a;
}

template< typename T >
inline Triple<T> operator/( const Triple<T> & a, const T & x )
{
  return Triple<T>( a.first / x, a.second / x, a.third / x );
}

template< typename T >
inline Triple<T> & operator/=( Triple<T> & a, const T & x )
{
  a.first /= x;
  a.second /= x;
  a.third /= x;
  return a;
}

template< typename T >
double abs( const Triple<T> & t )
{
  return sqrt( t.first * t.first
               + t.second * t.second
               + t.third * t.third );
}

template< typename T >
float fabs( const Triple<T> & t )
{
  return sqrtf( t.first * t.first
                + t.second * t.second
                + t.third * t.third );
}

template< typename T >
inline double norm( const Triple<T> & t )
{
  return sqrt( t.first * t.first
               + t.second * t.second
               + t.third * t.third );
}

template< typename T >
inline double distance( const Triple<T> & a,
                        const Triple<T> & b )
{
  double dx = b.first - a.first;
  double dy = b.second - a.second;
  double dz = b.third - a.third;
  return std::sqrt( dx * dx + dy * dy + dz * dz );
}

inline float distance( const Triple<float> & a,
                       const Triple<float> & b )
{
  float dx = b.first - a.first;
  float dy = b.second - a.second;
  float dz = b.third - a.third;
  return sqrtf( dx * dx + dy * dy + dz * dz );
}

inline void normalize( Triple<float> & t )
{
  float norm = sqrt( t.first * t.first
                     + t.second * t.second
                     + t.third * t.third );
  if ( norm < 0.00001 ) {
    t.first = t.second = t.third = 0.0f;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

inline void normalize( Triple<double> & t )
{
  double norm = sqrt( t.first * t.first
                      + t.second * t.second
                      + t.third * t.third );
  if ( norm < 0.00001 ) {
    t.first = t.second = t.third = 0.0;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

inline void normalize( Triple<long double> & t )
{
  long double norm = sqrt( t.first * t.first
                           + t.second * t.second
                           + t.third * t.third );
  if ( norm < 0.00001 ) {
    t.first = t.second = t.third = 0.0;
    return;
  }
  t.first /= norm;
  t.second /= norm;
  t.third /= norm;
}

template< typename T >
Triple<T> normalized( const Triple<T> & t )
{
  float norm = sqrt( t.first * t.first
                     + t.second * t.second
                     + t.third * t.third );
  if ( norm < 0.001 )
    return Triple<T>( 0.0f, 0.0f, 0.0f );
  return Triple<T>( t.first / norm, t.second / norm, t.third / norm );
}

template< typename T >
Triple<float> roundf( const Triple<T> & t )
{
  return Triple<float>( std::floor( 0.5f + t.first ),
                        std::floor( 0.5f + t.second ),
                        std::floor( 0.5f + t.third ) );
}

template< typename T >
Triple<double> round( const Triple<T> & t )
{
  return Triple<double>( std::floor( 0.5f + t.first ),
                         std::floor( 0.5f + t.second ),
                         std::floor( 0.5f + t.third ) );
}

template< typename T >
void lower( Triple<T> & t, const Triple<T> & other  )
{
  if ( t.first > other.first ) t.first = other.first;
  if ( t.second > other.second ) t.second = other.second;
  if ( t.third > other.third ) t.third = other.third;
}

template< typename T >
void raise( Triple<T> & t, const Triple<T> & other  )
{
  if ( t.first < other.first ) t.first = other.first;
  if ( t.second < other.second ) t.second = other.second;
  if ( t.third < other.third ) t.third = other.third;
}

template<typename T>
inline Triple<T> makeTriple( const T & first, const T & second, const T & third )
{
  return Triple<T>( first, second, third );
}

template<typename T>
Triple<T> wedgeProduct( const Triple<T> & a, const Triple<T> & b )
{
  Triple<T> r;
  r.first = a.second * b.third - b.second * a.third;
  r.second = b.first * a.third - a.first * b.third;
  r.third = a.first * b.second - b.first * a.second;
  return r;
}

template<typename T>
Triple<T> operator^( const Triple<T> & a, const Triple<T> & b )
{
  Triple<T> r;
  r.first = a.second * b.third - b.second * a.third;
  r.second = b.first * a.third - a.first * b.third;
  r.third = a.first * b.second - b.first * a.second;
  return r;
}

template<typename T>
Triple<T> scale( const Triple<T> & a, const Triple<T> & b )
{
  return Triple<T>( a.first * b.first,
                    a.second * b.second,
                    a.third * b.third );
}

template<typename T>
inline bool nonzero( const Triple<T> & a )
{
  return a.first|a.second|a.third;
}

template<>
inline bool nonzero<Float>( const Triple<Float> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
}

template<>
inline bool nonzero<Double>( const Triple<Double> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
}

template<>
inline bool nonzero<long double>( const Triple<long double> & a )
{
  return a.first!=0.0 || a.second!=0.0 || a.third!=0;
}

template<typename T>
inline bool isZero( const Triple<T> & a )
{
  return (!a.first) && (!a.second) && (!a.third);
}

template<>
inline bool isZero<Float>( const Triple<Float> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
}

template<>
inline bool isZero<Double>( const Triple<Double> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
}

template<>
inline bool isZero<long double>( const Triple<long double> & a )
{
  return a.first == 0.0 && a.second == 0.0 && a.third == 0.0;
}



#endif // _TRIPLE_H_
