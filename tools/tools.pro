LANGUAGE = C++
CONFIG	= staticlib
include("../global_options.qprefs")

TEMPLATE = lib

INCLUDEPATH	+= .. ./include

HEADERS	+=  ../globals.h \
            ./include/Array.h \
            ./include/Matrix.h \
            ./include/stats.h \
            ./include/Pair.h \
            ./include/tools.h \
            ./include/Triple.h \
            ./include/BitRef.h \
            ./include/Quaternion.h \
            ./include/ByteInput.h \
            ./include/ByteOutput.h \
            ./include/converter.h \
            ./include/TimeMeasure.h

SOURCES	+= ./src/Matrix.cpp \
           ./src/Array.cpp \
           ./src/stats.cpp \
           ./src/tools.cpp \
           ./src/BitRef.cpp \
           ./src/Quaternion.cpp \
           ./src/ByteInput.cpp \
           ./src/ByteOutput.cpp \
           ./src/TimeMeasure.cpp

TARGET = ../libs/tools

release { DEFINES += _RELEASE_ }

#unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = ./objs
  QMAKE_CXXFLAGS_RELEASE +=  -ffast-math -O3 -Wall -W -pedantic -ansi
  QMAKE_CXXFLAGS_DEBUG += -ffast-math -Wall -W -Wextra -pedantic -ansi
  DEFINES += _IS_UNIX_
#}

win32-g++ {
  QMAKE_CXXFLAGS_RELEASE += -ffast-math -W -Wall -ansi -pedantic
  QMAKE_CXXFLAGS_DEBUG += -W -Wall -ansi -pedantic
  OBJECTS_DIR = ./objs
}


