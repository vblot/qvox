/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ByteOutput.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 * 
 * @brief  Declaration of the class ByteOutput
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ByteOutput.h"
#include <cstdio>

#ifdef _IS_UNIX_
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#endif

#include <cstring>
#include <cstdlib>
#include <sstream>

/*
ByteOutput & 
ByteOutput::operator<<( const int i )
{
   std::stringstream ss;
   ss << i;
   this->puts( ss.str().c_str() );
   return *this;
}
*/

ByteOutput & 
ByteOutput::operator<<( const char * s )
{
   this->write( s, strlen( s ) );
   return *this;
}

/*
 *  Output to an ostream.
 */

ByteOutputStream::ByteOutputStream( std::ostream & out  )
   : _out(out)
{  
}

ByteOutputStream::ByteOutputStream( const ByteOutputStream & other  )
   : ByteOutput(),_out( other._out )
{
}

bool
ByteOutputStream::put( char c )
{
   return _out.put( c );
}

bool
ByteOutputStream::puts( const char *s )
{
   return _out.write( s, strlen( s ) );
}

bool
ByteOutputStream::write( const char *s, size_t n )
{
   return _out.write( s, n );
}

void
ByteOutputStream::flush()
{
   _out.flush();
}

ByteOutputStream::operator bool() const
{
   return _out;
}

/*
 * Input from a C file descriptor.
 */

#ifdef _IS_UNIX_
ByteOutputFD::ByteOutputFD( int fd  )
   : _fd( fd ),_ok(true)
{
}

ByteOutputFD::ByteOutputFD( const ByteOutputFD & other  )
   : ByteOutput(), _fd( other._fd ),_ok(true)
{
}

ByteOutputFD::~ByteOutputFD()
{
}

bool
ByteOutputFD::put( char c )
{
   ssize_t n = ::write( _fd, &c, 1 );
   if ( n == -1 ) perror( "put()" );
   return _ok = ( n > 0 );
}

bool
ByteOutputFD::puts( const char *buff )
{
   return this->write( buff, strlen( buff ) );
}

bool
ByteOutputFD::write( const char *buff, size_t n )
{
   size_t count = 0;
   ssize_t nb = 0;
   while ( ( count < n )
	   && ( ( nb = ::write( _fd, buff, n - count ) ) > 0 ) ) {
      buff += nb;
      count += nb;
   }
   if ( nb == -1 ) perror( "write()" );
   return _ok = ( count == n );
}

void
ByteOutputFD::flush()
{
   // ::fsync( _fd );
}

ByteOutputFD::operator bool() const
{
   return _ok && ( _fd >= 0 ) ;
}

#endif // _IS_UNIX_


#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_) 

/*
 * Input for a gzipped byte input
 */

ByteOutputGZip::ByteOutputGZip( ByteOutput & output  )
   : ByteOutputFD( -1 ), _out( output ),
     _pidWriter( 0 ), _pidGZip( 0 )
{
   int gz_input[2];
   int gz_output[2];
   if ( ( pipe( gz_input ) == -1  )||
	( pipe( gz_output ) == -1 ) ) {
      perror("pipe()");
      _fd = -1;
      return;
   }

   // Fork the data writer
   _pidWriter = fork();
   if ( _pidWriter == -1 ) { 
      perror( "fork():" );
      return;
   }
   if ( _pidWriter == 0 ) {
      ::close( gz_input[0] );
      ::close( gz_input[1] );
      ::close( gz_output[1] );
      ::close( 0 );
      ::close( 1 );
      int n;
      const size_t buffer_size = 1000*1024;
      char *buffer = new char[buffer_size];
      while ( ( n = ::read( gz_output[0], buffer, buffer_size ) ) > 0 ) {
	 _out.write( buffer, n );
      }
      output.flush();
      delete[] buffer;
      execlp( "true", "true", NULL );
   }
   ::close( gz_output[0] );
   
   // Fork the zip process
   _pidGZip = fork();
   if ( _pidGZip == -1 ) { 
      perror( "fork():" );
      return;
   }
   if ( _pidGZip == 0 ) {
      ::close( gz_input[1] );
      ::close( 0 );
      dup2( gz_input[0], 0 );
      ::close( gz_input[0] );
      ::close( 1 );
      dup2( gz_output[1], 1 );
      ::close( gz_output[1] );
      if ( execlp( "gzip", "gzip", "-c", NULL ) == -1 ) {
	 perror("execlp()");
	 ::close(0);
	 ::close(1);
	 execlp( "false", "false", NULL );
	 exit(0);
      }
   }
   ::close( gz_input[0] );
   ::close( gz_output[1] );
   _fd = gz_input[1];
}

ByteOutputGZip::ByteOutputGZip( const ByteOutputGZip & other  )
   : ByteOutputFD( other ), _out( other._out ),
     _pidWriter( -1 ), _pidGZip( -1 )
{  
}

bool
ByteOutputGZip::write( const char *buff, size_t n )
{
   if ( _fd == -1 ) return _ok = false;
   return ByteOutputFD::write( buff, n );
}


ByteOutputGZip::~ByteOutputGZip()
{
   ::close( _fd );
   int status;
   if ( _pidGZip > 0 && _pidGZip !=  waitpid( _pidGZip, &status, 0 ) ) {
      perror( "waitpid()" );
   }
   if ( _pidWriter > 0 && _pidWriter != waitpid( _pidWriter, &status, 0 ) ) {
      perror( "waitpid()" );
   }     
}

#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)


/*
 *  Input from a memory buffer.
 */

ByteOutputMemory::ByteOutputMemory()
   : _ok( true )
{     
}

bool
ByteOutputMemory::put( char c )
{
   _vector.push_back( c );
   return true;
}

bool
ByteOutputMemory::puts( const char *s )
{
   return this->write( s, strlen( s ) );
}

bool
ByteOutputMemory::write( const char *s, size_t n )
{
   size_t current_size = _vector.size();
   try {
      _vector.insert( _vector.end(), n, 0 );
   } catch ( std::bad_alloc ) {
      return _ok = false;
   }
   char * dst = &(_vector[current_size]);
   memcpy( dst, s, n );
   return true;
}

void
ByteOutputMemory::flush()
{
}

ByteOutputMemory::operator bool() const
{
   return _ok;
}


char *
ByteOutputMemory::buffer()
{
   return &(_vector[0]);
}

size_t
ByteOutputMemory::size() const
{
   return _vector.size();
}
