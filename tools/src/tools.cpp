/**
 * @file   tools.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2005
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "globals.h"
#include "tools.h"
#include <cstdio>
#include <string>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cctype>
#include "ByteOutput.h"

using namespace std;

string
today()
{
   char date[12];
   time_t t;
   struct tm *lt = 0;
   time( &t );
   lt = localtime( &t );
   sprintf( date, "%.3d/%.2d/%.2d",
	    lt->tm_year - 100, lt->tm_mon + 1, lt->tm_mday );
   return string( date );
}

void
strToUpper( char * dst, const char * src )
{
  const char * pc = src;
  while ( *pc ) {
    *dst++ = static_cast<char>( std::toupper( *pc++ ) );
  }
  *dst = 0;
}

void
strToLower( char * dst, const char * src )
{
  const char * pc = src;
  while ( *pc ) {
    *dst++ = static_cast<char>( std::tolower( *pc++ ) );
  }
  *dst = 0;
}

string binary( UInt32 l )
{
  string result="";
  UChar bit = ( sizeof(UInt32) * 8 ) - 1;
  bool start = false;
  do {
    if ( l & ( 1 << bit ) ) {
      start = true;
      result+="1";
    } else {
      if ( start ) result += "0";
    }
  } while ( bit-- );
  if ( result == "" ) result = "0";
  return result;
}

const char *
lastDotOrEnd( const char *s )
{
  const char *result = 0;
  while ( *s ) {
    if ( *s == '.' ) result = s;
    s++;
  }
  return result ? result : s;
}

char *
lastDotOrEnd( char *s )
{
  char *result = 0;
  while ( *s ) {
    if ( *s == '.' ) result = s;
    s++;
  }
  return result ? result : s;
}

void
initRandom()
{
 time_t horloge = clock();
 struct tm *p_heure;
 const time_t mask = (~0) ^ 0xFF;
 int n;
 time( &horloge );
 n = horloge & mask;
 p_heure = localtime( &horloge );
 n += p_heure->tm_hour + p_heure->tm_min + p_heure->tm_sec;
 srand( n );
}

char *
findArg(int argc, char * argv[], const char * param) {
  int i = 0;
  for ( i = 1 ; i < argc ; i++ )
    if ( !strcmp( argv[i], param ) ) {
      argv[ i ] [ 0 ] = 0;
      if ( i + 1 < argc ) return argv[ i + 1 ];
      return argv[ i ];
    }
  return 0;
}

bool
ask( const char *question )
{
 char answer[10];
 cout << question << " (y/n) >";
 if ( fgets( answer, 10, stdin ) )
   return ( strstr( answer, "yes" ) ||
            *answer == 'y' ||
            *answer == 'Y' );
 else return false;
}

int
readInteger( const char *message )
{
 int res;
 cout << message << " >" ;
 cin >> res;
 return res;
}

void
fwriteWordMSB( FILE * file, unsigned int u )
{
  fputc( u >> 24, file );
  fputc( ( u >> 16 ) & 0xFF, file );
  fputc( ( u >> 8 ) & 0xFF, file );
  fputc( u & 0xFF, file );
}

void
fwriteWordLSB( FILE * file, unsigned int u )
{
  fputc( u & 0xFF, file );
  fputc( ( u >> 8 ) & 0xFF, file );
  fputc( ( u >> 16 ) & 0xFF, file );
  fputc( u >> 24, file );
}


void
writeWordMSB( ByteOutput & out,  int u )
{
  out.put( static_cast<unsigned char>( u >> 24 ) );
  out.put( static_cast<unsigned char>( ( u >> 16 ) & 0xFF ) );
  out.put( static_cast<unsigned char>( ( u >> 8 ) & 0xFF ) );
  out.put( static_cast<unsigned char>( u & 0xFF ) );
}

void
writeWordLSB( ByteOutput & out,  int u )
{
  out.put( static_cast<unsigned char>( u & 0xFF ) );
  out.put( static_cast<unsigned char>( ( u >> 8 ) & 0xFF ) );
  out.put( static_cast<unsigned char>( ( u >> 16 ) & 0xFF ) );
  out.put( static_cast<unsigned char>( u >> 24 ) );
}

void writeShortMSB( ByteOutput & out, unsigned short s )
{
  out.put( static_cast<unsigned char>( ( s >> 8 ) & 0xFF ) );
  out.put( static_cast<unsigned char>( s & 0xFF ) );
}

void writeShortLSB( ByteOutput & out, unsigned short s )
{
  out.put( static_cast<unsigned char>( s & 0xFF ) );
  out.put( static_cast<unsigned char>( ( s >> 8 ) & 0xFF ) );
}

unsigned int
readWordMSB( FILE * file )
{
 unsigned int result = 0;
 result = fgetc( file ) << 24;
 result += fgetc( file ) << 16;
 result += fgetc( file ) << 8;
 result += fgetc( file );
 return result;
}

unsigned int
readWordLSB( FILE * file )
{
  unsigned int u = 0;
  u = fgetc( file );
  u += fgetc( file ) << 8;
  u += fgetc( file ) << 16;
  u += fgetc( file ) << 24;
  return u;
}

ostream &
writeWordMSB( ostream & file, unsigned long u )
{
  file.put( u >> 24 );
  file.put( ( u >> 16 ) & 0xFF );
  file.put( ( u >> 8 ) & 0xFF );
  file.put( u & 0xFF );
  return file;
}

ostream &
writeWordLSB( ostream & file , unsigned long u )
{
  file.put( u & 0xFF );
  file.put( ( u >> 8 ) & 0xFF );
  file.put( ( u >> 16 ) & 0xFF );
  file.put( u >> 24 );
  return file;
}

unsigned long
readWordMSB( istream & file )
{
 unsigned int u = 0;
 unsigned char buffer[4], *p = buffer;
 file.read( (char *) buffer, 4 );
 u += ((unsigned long) (*(p++))) << 24;
 u += ((unsigned long) (*(p++))) << 16;
 u += ((unsigned long) (*(p++))) << 8;
 u += ((unsigned long) (*(p++)));
 return u;
}

unsigned long
readWordLSB( istream & file )
{
  unsigned long u = 0;
  unsigned char buffer[4], *p = buffer;
  file.read( (char*) buffer, 4 );
  u = *(p++);
  u += ((unsigned long) (*(p++))) << 8;
  u += ((unsigned long) (*(p++))) << 16;
  u += ((unsigned long) (*(p++))) << 24;
  return u;
}

unsigned short
readShortLSB( istream & file )
{
   unsigned short u = 0;
   char c;
   file.get( c ); u = (unsigned char) c;
   file.get( c ); u |= ( (unsigned short) c ) << 8;
   return u;
}

unsigned short
readShortMSB( istream & file )
{
   unsigned short u = 0;
   char c;
   file.get( c ); u = ( (unsigned short) c) << 8 ;
   file.get( c ); u |= (unsigned char) c;
   return u;
}

ostream &
writeShortLSB( ostream & file, unsigned short s )
{
  return file.put( s & 0xff ) ? file.put( ( s >> 8 ) & 0xff ) : file;
}

ostream &
writeShortMSB( ostream & file, unsigned short s )
{
  return file.put( ( s >> 8 ) & 0xff ) ? file.put( s & 0xff ) : file;
}

void
reverseBytes32( unsigned int & u )
{
 unsigned long v = u;
 u = 0;
 u |= ( v & 0xFF000000 ) >> 24;
 u |= ( v & 0x00FF0000 ) >> 8;
 u |= ( v & 0x0000FF00 ) << 8;
 u |= ( v & 0x000000FF ) << 24;
}

void
reverseBytes32( unsigned long & u )
{
 unsigned long v = u;
 u = 0;
 u |= ( v & 0xFF000000 ) >> 24;
 u |= ( v & 0x00FF0000 ) >> 8;
 u |= ( v & 0x0000FF00 ) << 8;
 u |= ( v & 0x000000FF ) << 24;
}

bool
isLittleEndian()
{
  unsigned int dummy = 1;
  unsigned char *p;
  p = reinterpret_cast<unsigned char*>( &dummy );
  return ( *p == 1 );
}

Endianness
endian()
{
  unsigned int dummy = 1;
  unsigned char *p = reinterpret_cast<unsigned char*>( &dummy );
  if ( *p == 1 )
    return LittleEndian;
  return BigEndian;
}


template<>
void reverseBytes<2>( void * data, const size_t n )
{
  unsigned char *p = reinterpret_cast<unsigned char*>( data );
  unsigned char *end = p + ( n << 1 );
  register unsigned short s;
  register unsigned short r;
  while ( p < end ) {
    s  = * reinterpret_cast<unsigned short *>( p );
    r = ( s & 0xff ) << 8;
    s >>= 8;
    r |= ( s & 0xff );
    * ( reinterpret_cast<unsigned short *>( p ) ) = r ;
    p += 2;
  }
}

template<>
void reverseBytes<4>( void * data, const size_t n )
{
  unsigned char *p = reinterpret_cast<unsigned char*>( data );
  unsigned char *end = p + ( n << 2 );
  register UInt i;
  register UInt r;
  while ( p < end ) {
    i = * reinterpret_cast<UInt*>( p );
    r = 0;
    r |= (i & 0xff);  r <<= 8; i >>= 8;
    r |= (i & 0xff);  r <<= 8; i >>= 8;
    r |= (i & 0xff);  r <<= 8; i >>= 8;
    r |= (i & 0xff);
    *( reinterpret_cast<UInt*>( p ) ) = r;
    p += 4;
  }
}

template<>
void reverseBytes<8>( void * data, const size_t n )
{
  unsigned char *p = reinterpret_cast<unsigned char*>( data );
  unsigned char *end = p + (n << 3 );
  unsigned char tmp;
  unsigned char *low;
  unsigned char *high;
  while ( p < end ) {
    low = p;
    high = p + 7;
    tmp = *low; *(low++) = *(high); *(high--) = tmp;
    tmp = *low; *(low++) = *(high); *(high--) = tmp;
    tmp = *low; *(low++) = *(high); *(high--) = tmp;
    tmp = *low; *(low++) = *(high); *(high--) = tmp;
    p += 8;
  }
}

template<>
void reverseBytes<1>( void * )
{  
}

template<>
void reverseBytes<2>( void * p )
{
  register unsigned short s = * reinterpret_cast<unsigned short *>(p);
  register unsigned short r = 0;
  r = ( s & 0xff ) << 8;
  s >>= 8;
  r |= ( s & 0xff );
  * ( reinterpret_cast<unsigned short *>( p ) ) = r ;
}

template<>
void reverseBytes<4>( void * p )
{
  register UInt i = * reinterpret_cast<UInt*>( p );
  register UInt r = 0;
  r |= (i & 0xff);  r <<= 8; i >>= 8;
  r |= (i & 0xff);  r <<= 8; i >>= 8;
  r |= (i & 0xff);  r <<= 8; i >>= 8;
  r |= (i & 0xff);
  *( reinterpret_cast<UInt*>( p ) ) = r;
}

template<>
void reverseBytes<8>( void * p )
{
  unsigned char tmp;
  unsigned char *low = reinterpret_cast<unsigned char*>( p );
  unsigned char *high = reinterpret_cast<unsigned char*>( p ) + 7;
  tmp = *low; *(low++) = *(high); *(high--) = tmp;
  tmp = *low; *(low++) = *(high); *(high--) = tmp;
  tmp = *low; *(low++) = *(high); *(high--) = tmp;
  tmp = *low; *(low++) = *(high); *(high--) = tmp;
}

bool
extension( const char * str, const char * ext )
{
  int lstr = strlen( str );
  int lext = strlen( ext );
  if ( lstr < lext ) return false;
  str += lstr - lext;
  while ( (*str)
	  && (*ext)
	  && ( ( (*str == '.' ) && ( *ext == '.' ) )
	       || ( toupper( *str ) == toupper( *ext ) ) ) ) {
    str++; ext++;
  }
  return ( *str == 0 ) && ( *ext == 0 );
}

const char *
baseName( const char * filename )
{
  static const char current[] = ".";
  if ( ! *filename ) return current;    // Empty filename
  const char * pc = filename;
  const char * next = filename;
  while ( ( next = strstr( next, "/" ) ) ) {
    ++next;
    pc = next;
  }
  if ( pc == filename ) return filename; // No '/' found.
  if ( *pc ) return pc;

  --pc;
  while ( *pc == '/' && pc > filename ) --pc;
  while ( *pc != '/' && pc > filename ) --pc;
  ++pc;
  if ( ! *pc ) --pc;
  return pc;
}

char *
baseName( char * filename )
{
  static char current[] = ".";
  if ( ! *filename ) return current;    // Empty filename
  char * pc = filename;
  char * next = filename;
  while ( ( next = strstr( next, "/" ) ) ) {
    ++next;
    pc = next;
  }
  if ( pc == filename ) return filename; // No '/' found.
  if ( *pc ) return pc;

  --pc;
  while ( *pc == '/' && pc > filename ) --pc;
  while ( *pc != '/' && pc > filename ) --pc;
  ++pc;
  if ( ! *pc ) --pc;
  return pc;
}

bool
magicNumber( istream & file, const char * str )
{
   const char * pc = str;
   char c;
   while ( *pc ) {
     file.get( c );
     if ( *pc != c &&  *pc != '?' ) return false;
     pc++;
   }
   return true;
}

bool
magicNumber( const char * strA, const char * strB )
{
   const char * pa = strA;
   const char * pb = strB;
   while ( ( *pa )  && ( *pb )  ) {
     if ( ( *pa != *pb )  &&  ( *pa != '?' ) && ( *pb != '?' )  ) return false;
     ++pa; 
     ++pb;
   }
   if ( *pa != *pb ) return false;
   return true;
}

