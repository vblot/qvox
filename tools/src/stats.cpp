/**
 * @file   stats.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2005
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "globals.h"
#include "stats.h"
#include <limits>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include "tools.h"

using namespace std;

void
stats( const std::vector<double> & v, 
       double & min, 
       double & max, 
       double & average, 
       double & stdDeviation )
{
  min = numeric_limits<double>::max();
  max = -numeric_limits<double>::max();
  long double size = v.size();
  long double sum_x2 = 0.0;
  long double sum_x = 0.0;

  vector<double>::const_iterator it = v.begin();
  while ( it != v.end() ) {
    sum_x += (*it);
    sum_x2 += (*it)*(*it);
    if ( *it > max )
      max = *it;
    if ( *it < min )
      min = *it;
    ++it;
  }
  
  average = 0.0;
  stdDeviation = 0.0;
  if ( v.size() ) {
    stdDeviation = sqrt(static_cast<double>(((size*sum_x2) - (sum_x*sum_x)) / (size*size)));
    average = static_cast<double>( sum_x / size );
  }
}

void histo( const std::vector<double> & v,
	    unsigned long bins,
	    const char * filename, 
	    std::map<double,double> * result )
{
  if ( !bins )
    bins = static_cast<unsigned long>( floor( sqrt( v.size() ) ) );

  double maxV = *max_element( v.begin(), v.end() );
  double minV = *min_element( v.begin(), v.end() );
  double binSize =  (maxV-minV)/bins;
  vector<unsigned long> h(bins);
  vector<double>::const_iterator it = v.begin();
  while ( it != v.end() ) {
    if ( *it == maxV ) 
      h[ bins - 1 ]++;
    else
      h[ static_cast<unsigned long>(floor(((*it)-minV)/binSize)) ]++;
    ++it;
  }
  
  double halfBinSize = binSize / 2.0;
  if ( result ) {
    for ( unsigned long i = 0; i < bins; ++i ) {
      (*result)[ i*binSize + halfBinSize ] = h[i];
    }
  }
  if ( filename ) {
    ofstream f( filename );
    for ( unsigned long i = 0; i < bins; ++i ) {
      f << (i*binSize + halfBinSize) << " " <<  h[i] << endl;
    }    
  }
}
