/**
 * @file   Matrix.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:42:46 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Matrix.h"
#include "tools.h"

/** 
 * Fills an indentity matrix.
 * 
 * @param m A matrix.
 */
void
Matrix::identity()
{
  memset( this, 0, 9 * sizeof( double ) );
  a11 = a22 = a33 = 1.0f;
}

/** 
 * Fills a diagonal matrix
 * 
 * @param m A matrix.
 * @param x11 Value of m[1,1].
 * @param x22 Value of m[2,2].
 * @param x33 Value of m[3,3].
 */

Matrix &
Matrix::setDiagonal( double x11, double x22, double x33 )
{
  memset( this, 0, 9 * sizeof( double ) );
  a11 = x11;  a22 = x22;  a33 = x33;
  return *this;
}

/** 
 * Fills a rotation matrix around Oz.
 * 
 * @param m A matrix.
 * @param angle The angle (rad).
 */
Matrix &
Matrix::setRotationOz( double angle )
{
  a11 = cos( angle );
  a12 = -sin( angle );
  a21 = sin( angle );
  a22 = cos( angle );
  a33 = 1.0;
  a13 = a23 = a31 = a32 = 0.0f;
  return *this;
}


/** 
 * Fills a rotation matrix around Oy.
 * 
 * @param m A matrix.
 * @param angle The angle (rad).
 */
Matrix &
Matrix::setRotationOy( double angle )
{
  a11 = cos( angle );
  a13 = sin( angle );
  a22 = 1.0;
  a31 = -sin( angle );
  a33 = cos( angle ); 
  a21 = a23 = a12 = a32 = 0.0f;
  return *this;
}

/** 
 * Fills a rotation matrix around Ox.
 * 
 * @param m A matrix.
 * @param angle The angle (rad).
 */
Matrix &
Matrix::setRotationOx( double angle )
{
  a11 = 1.0;
  a22 = cos( angle );
  a23 = -sin( angle );
  a32 = sin( angle );
  a33 = cos( angle );
  a12 = a13 = a21 = a31 = 0.0f;
  return *this;
}

/** 
 * Fills a rotation matrix.
 * 
 * @param m A matrix.
 * @param rho Rotation angle around Ox (rad).
 * @param theta Rotation angle around Oy (rad).
 * @param phi Rotation angle around Oz (rad).
 */
Matrix &
Matrix::setRotation( double rho, double theta, double phi )
{
 double s_rho, c_rho, s_theta, c_theta, s_phi, c_phi;
 s_rho = sin( rho );
 c_rho = cos( rho );
 s_theta = sin( theta );
 c_theta = cos( theta );
 s_phi = sin( phi );
 c_phi = cos( phi );

 a11 = c_theta*c_phi;
 a12 = s_rho*s_theta*c_phi - c_rho*s_phi;
 a13 = s_rho*s_phi + c_rho*s_theta*c_phi;
 
 a21 = c_theta*s_phi;
 a22 = c_rho*c_phi + s_rho*s_theta*s_phi;
 a23 = c_rho*s_theta*s_phi - s_rho*c_phi;
 
 a31 = -s_theta;
 a32 = s_rho*c_theta;
 a33 = c_rho*c_theta;
 return *this;
}


/** 
 * Multiplies a matrix with a vector in the given vector.
 * 
 * @param m A matrix.
 * @param v A vector (read and overwritten with the result of 
 *          the product.
 */
void
mxMultiplyVectorOnPlace( Matrix m,
			 Triple<double> & v )
{
 double x,y,z;
 x = v.first * m.a11 + v.second * m.a12 + v.third * m.a13;
 y = v.first * m.a21 + v.second * m.a22 + v.third * m.a23;
 z = v.first * m.a31 + v.second * m.a32 + v.third * m.a33;
 v.first = x;  v.second = y; v.third = z;
}

/** 
 * Computes the product of two matrices.
 * 
 * @param result The resulting matrix.
 * @param a The first operand of the product. 
 * @param b The second operand of the product.
 */
Matrix operator*( const Matrix & left, const Matrix & right ) 
{
  Matrix result;
  double *pr;
  const double *pa;
  
  pr = &(result.a11);
  pa = &(left.a11);                 // r[0,0]
  (*pr) += left.a11 * right.a11;
  (*pr) += left.a12 * right.a21;
  (*pr) += left.a13 * right.a31;

  ++pr; 
  pa = &(left.a11);                 // r[0,1]
  *pr += *(pa++) * right.a12;
  *pr += *(pa++) * right.a22;
  *pr +=   (*pa) * right.a32;

  ++pr; 
  pa = &(left.a11);                 // r[0,2]
  *pr += *(pa++) * right.a13;
  *pr += *(pa++) * right.a23;
  *pr +=   (*pa) * right.a33;

  pr = &(result.a21);          // r[1,0]
  pa = &(left.a21); 
  *pr += *(pa++) * right.a11;
  *pr += *(pa++) * right.a21;
  *pr +=   (*pa) * right.a31;

  ++pr; 
  pa = &(left.a21);                 // r[1,1]
  *pr += *(pa++) * right.a12;
  *pr += *(pa++) * right.a22;
  *pr +=   (*pa) * right.a32;

  ++pr; 
  pa = &left.a21;                 // r[1,2]
  *pr += *(pa++) * right.a13 ;
  *pr += *(pa++) * right.a23 ;
  *pr +=   (*pa) * right.a33 ;

  pr = &(result.a31); 
  pa = &(left.a31);                  // r[2,0]
  *pr += *(pa++) * right.a11;
  *pr += *(pa++) * right.a21;
  *pr +=   (*pa) * right.a31;

  ++pr; 
  pa = &(left.a31);                  // r[2,1]
  *pr += *(pa++) * right.a12;
  *pr += *(pa++) * right.a22;
  *pr +=   (*pa) * right.a32;

  ++pr; 
  pa = &(left.a31);                  // r[2,2]
  *pr += *(pa++) * right.a13;
  *pr += *(pa++) * right.a23;
  *pr +=   (*pa) * right.a33;
  return result;
}

Matrix &
Matrix::transpose()
{
  swapValues( a21, a12 );
  swapValues( a31, a13 );
  swapValues( a32, a23 );
  return *this;
}

Matrix
Matrix::transposed()
{
  Matrix r( *this );
  r.transpose();
  return r;
}


/** 
 * Outputs a matrix in s stream.
 * 
 * @param out An output stream of chars.
 * @param m A matrix.
 */
std::ostream &
operator<<( std::ostream & out, const Matrix & m )
{
  using namespace std;
  out << "[ " << setw(4) << m.a11 << " " << m.a12 << " " << m.a13 << endl;
  out << "[ " << setw(4) << m.a21 << " " << m.a22 << " " << m.a23 << endl;
  out << "[ " << setw(4) << m.a31 << " " << m.a32 << " " << m.a33 << endl;
  return out;
}

double mxDistance( Matrix m1, Matrix m2 )
{
  return fabs( m1.a11 - m2.a11 ) +
    fabs( m1.a12 - m2.a12 ) +
    fabs( m1.a13 - m2.a13 ) +
    fabs( m1.a21 - m2.a21 ) +
    fabs( m1.a22 - m2.a22 ) +
    fabs( m1.a23 - m2.a23 ) +
    fabs( m1.a31 - m2.a31 ) +
    fabs( m1.a32 - m2.a32 ) +
    fabs( m1.a33 - m2.a33 );
}

Matrix
Matrix::rotation( double rho, double theta, double phi )
{
  return Matrix().setRotation( rho, theta, phi );
}
