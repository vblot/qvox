/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   Quaternion.h
 * @author Sebastien Fourey (GREYC)
 * @date   Dec 2007
 * 
 * @brief  Declaration of the class Quaternion
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Quaternion.h"
#include "tools.h"
#include "Triple.h"

const Triple<double> Quaternion::AxisX( 1.0, 0.0, 0.0 );
const Triple<double> Quaternion::AxisY( 0.0, 1.0, 0.0 );
const Triple<double> Quaternion::AxisZ( 0.0, 0.0, 1.0 );


Quaternion &
Quaternion::operator*=( const Quaternion & other )
{
   Triple<double> v1( _values[0], _values[1], _values[2] );
   Triple<double> v2( other._values[0], other._values[1], other._values[2] );

   Triple<double> tmp1 = v1; // HeTb_vcopy(q1,t1);
   tmp1 *= other._values[3]; // HeTb_vscale(t1,q2[3]);

   Triple<double> tmp2 = v2; // HeTb_vcopy(q2,t2);
   tmp2 *= _values[3]; // HeTb_vscale(t2,q1[3]);

   Triple<double> tmp3 = wedgeProduct( v2, v1 ); // HeTb_vcross(q2,q1,t3);
   Triple<double> tf = tmp1 + tmp2 + tmp3; // HeTb_vadd(t1,t2,tf);   HeTb_vadd(t3,tf,tf);

   _values[0] = tf.first;
   _values[1] = tf.second;
   _values[2] = tf.third;
   _values[3] = _values[3] * other._values[3] - ( v1 * v2 );
   return *this;
}

Quaternion
Quaternion::operator*( const Quaternion & other ) const
{
   Triple<double> v1( _values[0], _values[1], _values[2] );
   Triple<double> v2( other._values[0], other._values[1], other._values[2] );

   Triple<double> tmp1 = v1; // HeTb_vcopy(q1,t1);
   tmp1 *= other._values[3]; // HeTb_vscale(t1,q2[3]);

   Triple<double> tmp2 = v2; // HeTb_vcopy(q2,t2);
   tmp2 *= _values[3]; // HeTb_vscale(t2,q1[3]);

   Triple<double> tmp3 = wedgeProduct( v2, v1 ); // HeTb_vcross(q2,q1,t3);
   Triple<double> tf = tmp1 + tmp2 + tmp3; // HeTb_vadd(t1,t2,tf);   HeTb_vadd(t3,tf,tf);

   return Quaternion( tf.first,
		      tf.second,
		      tf.third,
		      _values[3] * other._values[3] - ( v1 * v2 ) );
}

void
Quaternion::normalize()
{
   double norm = _values[0]*_values[0] + _values[1]*_values[1]
      + _values[2]*_values[2] + _values[3]*_values[3];
   _values[0] /= norm;
   _values[1] /= norm;
   _values[2] /= norm;
   _values[3] /= norm;
}

Quaternion
Quaternion::normalized() const
{
   double norm = _values[0]*_values[0] + _values[1]*_values[1]
      + _values[2]*_values[2] + _values[3]*_values[3];
   return Quaternion( _values[0] / norm,
		      _values[1] / norm,
		      _values[2] / norm,
		      _values[3] / norm );
}


Matrix
Quaternion::rotationMatrix() const
{
   Matrix result;
   rotationMatrix( result );
   return result;
}

void
Quaternion::rotationMatrix( Matrix & m ) const
{
   m.a11 = static_cast<float>( 1.0 - 2.0 * ( dsquare( _values[1] ) + dsquare( _values[2] ) ) );
   m.a12 = static_cast<float>( 2.0 * (_values[0] * _values[1] - _values[2] * _values[3]) );
   m.a13 = static_cast<float>( 2.0 * (_values[2] * _values[0] + _values[1] * _values[3]) );
   
   m.a21 = static_cast<float>( 2.0 * (_values[0] * _values[1] + _values[2] * _values[3]) );
   m.a22= static_cast<float>( 1.0 - 2.0 * ( dsquare( _values[2] ) + dsquare( _values[0] ) ) );
   m.a23 = static_cast<float>( 2.0 * (_values[1] * _values[2] - _values[0] * _values[3]) );
   
   m.a31 = static_cast<float>( 2.0 * (_values[2] * _values[0] - _values[1] * _values[3]) );
   m.a32 = static_cast<float>( 2.0 * (_values[1] * _values[2] + _values[0] * _values[3]) );
   m.a33 = static_cast<float>( 1.0 - 2.0 * ( dsquare( _values[1] ) + dsquare( _values[0] ) ) );
}

void
Quaternion::setRotation( double axis[3], double angle )
{
   Triple<double> a( axis[0], axis[1], axis[2] );
   ::normalize( a );
   a *= sin( angle / 2.0 );
   _values[ 0 ] = a.first;
   _values[ 1 ] = a.second;
   _values[ 2 ] = a.third;
   _values[ 3 ] = cos( angle / 2.0 );
}

void
Quaternion::setRotation( Triple<double> axis, double angle )
{
   ::normalize( axis );
   axis *= sin( angle / 2.0 );
   _values[ 0 ] = axis.first;
   _values[ 1 ] = axis.second;
   _values[ 2 ] = axis.third;
   _values[ 3 ] = cos( angle / 2.0 );
}

Quaternion
Quaternion::rotation( double axis[3], double angle )
{
   Triple<double> a( axis[0], axis[1], axis[2] );
   ::normalize( a );
   a *= sin( angle / 2.0 );
   return Quaternion( a.first, a.second, a.third, cos( angle / 2.0 ) );
}

Quaternion
Quaternion::rotation( Triple<double> axis, double angle )
{
   ::normalize( axis );
   axis *= sin( angle / 2.0 );
   return Quaternion( axis.first, axis.second, axis.third, cos( angle / 2.0 ) );
}

std::istream &
Quaternion::readFrom( std::istream & in )
{
   in >> _values[0] 
      >> _values[1]
      >> _values[2]
      >> _values[3];
   return in;
}

std::ostream &
Quaternion::flush( std::ostream & out ) const
{
   out << _values[0] << " "
       << _values[1] << " "
       << _values[2] << " "
       << _values[3];
   return out;
}

std::ostream &
operator<<( std::ostream & out, const Quaternion & q )
{
   return q.flush( out );
}

std::istream &
operator>>( std::istream & in, Quaternion & q )
{
   return q.readFrom( in );
}



