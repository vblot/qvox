/**
 * @file   TimeMeasure.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   2013
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "TimeMeasure.h"

#ifndef _RELEASE_

#include <iostream>
#include <fstream>
#include <sys/time.h>
#include "globals.h"

using namespace std;

TimeMeasure globalTimeMeasure;

TimeMeasure::TimeMeasure()
{
  _filename = "time_measure.txt";
}

TimeMeasure::~TimeMeasure()
{
  ofstream file(_filename.c_str());
  map< string, vector<long> >::iterator it = _durations.begin();
  vector<long>::size_type count = 0;
  file << "# ";
  while ( it != _durations.end() ) {
    vector<long> & v = it->second;
    if ( v.size() > count ) count = v.size();
    file << it->first << "\t";
    ++it;
  }
  file << "\n";
  for (vector<long>::size_type n = 0; n < count; ++n) {
    it = _durations.begin();
    while ( it != _durations.end() ) {
      vector<long> & v = it->second;
      if ( n < v.size() )
        file << v[n] << "\t";
      else
        file << "0" << "\t";
      ++it;
    }
    file << "\n";
  }
}

void TimeMeasure::setFilename(const std::string & name )
{
  _filename = name;
}

void TimeMeasure::start(const string & name)
{
  _starts[name] = getTime();
}

void TimeMeasure::stop(const string & name)
{
  _durations[name].push_back( getTime() - _starts[name] );
  TRACE << "Operation " << name << " lasted " << _durations[name].back() << " ms\n";
}

unsigned long TimeMeasure::getTime()
{
  timeval tval;
  gettimeofday(&tval,0);
  return static_cast<unsigned long>(tval.tv_sec % (24*3600))*1000 + (tval.tv_usec / 1000);
}


#endif

