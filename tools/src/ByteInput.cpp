/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ByteInput.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 * 
 * @brief  Declaration of the class ByteInput
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ByteInput.h"

#ifdef _IS_UNIX_
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#endif

#include <fstream>
#include <cstdlib>
#include <cstring>

/*
 *  Input from an istream.
 */

namespace { 
   int fd_to_close;
   void terminate(int) {
#ifdef _IS_UNIX_
      if ( fd_to_close != -1 )
	 close( fd_to_close );
      execlp( "true", "true", NULL );
#endif // _IS_UNIX_
      exit( 0 );
   }
}

ByteInputStream::ByteInputStream( std::istream & in  )
   : _in(in)
{  
}

ByteInputStream::ByteInputStream( const ByteInputStream & other  )
   : ByteInput(),_in( other._in )
{
}

bool
ByteInputStream::get( char & c )
{
   if ( ! _in ) return false;
   _in.get( c );
   return _in.gcount() == 1;
}

bool
ByteInputStream::getline( char* s, size_t n, char delim )
{
   if ( !_in ) return false;
   _in.getline( s, n, delim );
   return _in.gcount() > 0;
}

bool
ByteInputStream::read( char* s, size_t n )
{
   if ( _in.eof() ) {
      return false;
   }
   _in.read( s, n );
   return _in.gcount() > 0;
}

size_t
ByteInputStream::gcount() const
{
   return _in.gcount();
}

ByteInputStream::operator bool() const
{
   return _in;
}

/*
 * Input from a C file descriptor.
 */

#ifdef _IS_UNIX_
ByteInputFD::ByteInputFD( int fd  )
   : _fd( fd ),_gcount( 0 )
{
}

ByteInputFD::ByteInputFD( const ByteInputFD & other  )
   : ByteInput(), _fd( other._fd ), _gcount( other._gcount )
{
}

ByteInputFD::~ByteInputFD()
{
}

bool
ByteInputFD::get( char & c )
{
   _gcount = ::read( _fd, &c, 1 );
   return _gcount >= 0;
}

bool
ByteInputFD::getline( char *buff, size_t n, char delim )
{
   _gcount = 0;
   ssize_t max = n - 1;
   ssize_t nb = 0;
   memset( buff, static_cast<char>( ~delim ), n );
   while ( ( _gcount < max )
	   && ( ( nb = ::read( _fd, buff, 1 ) ) == 1 )
	   && ( (*buff) != delim ) ) {
      ++buff;
      ++_gcount;
   }
   if ( *buff == delim ) { 
      ++_gcount;
   }
   *buff = '\0';
   return ( _gcount > 0 );
}

bool
ByteInputFD::read( char* buff, size_t n )
{
   _gcount = ::read( _fd, buff, n );
   return _gcount >= 0;
}

size_t
ByteInputFD::gcount() const
{
   return (_gcount == -1) ? 0 : static_cast<size_t>( _gcount );
}

ByteInputFD::operator bool() const
{
   return ( _gcount >= 0 ) && ( _fd >= 0 ) ;
}
#endif // _IS_UNIX_

#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_) 

/*
 * Input for a gzipped byte input
 */

ByteInputGZip::ByteInputGZip( ByteInput & input  )
   : ByteInputFD( -1 ), _in( input ),
     _pidReader( 0 ), _pidGZip( 0 )
{
   int gz_input[2];
   pipe( gz_input );
   int gz_output[2];
   pipe( gz_output );

   // Fork the gunzip process
   _pidGZip = fork();
   if ( _pidGZip == -1 ) { 
      perror( "fork():" );
      return;
   }
   if ( _pidGZip == 0 ) {
      close( gz_input[1] );
      close( gz_output[0] );
      close( 0 );
      dup2( gz_input[0], 0 );
      close( gz_input[0] );
      close( 1 );
      dup2( gz_output[1], 1 );
      close( gz_output[1] );
      if ( execlp( "gunzip", "gunzip", "-c", NULL ) == -1 ) {
	 perror("execlp()");
	 close(0);
	 close(1);
	 execlp( "false", "false", NULL );
	 exit(0);
      }
   }
   close( gz_output[1] );
   close( gz_input[0] );

   // Fork the gzipped data reader
   _pidReader = fork();
   if ( _pidReader == -1 ) { 
      perror( "fork():" );
      return;
   }
   if ( _pidReader == 0 ) {
      signal( SIGTERM, &terminate );
      fd_to_close = gz_input[1];
      const size_t buffer_size = 1000 * 1024;
      char *buffer = new char[buffer_size];
      int nb;
      while ( input.read( buffer, buffer_size ) && 
	      ( ( nb = input.gcount() ) > 0 ) ) {
	 write( gz_input[1], buffer, nb ); 
      }
      close( gz_input[1] );
      delete[] buffer;
      fd_to_close = -1;
      pause();
      execlp( "true", "true", NULL );
   }   
   close( gz_input[1] );
   _fd = gz_output[0];
}

ByteInputGZip::ByteInputGZip( const ByteInputGZip & other  )
   : ByteInputFD( other ), _in( other._in ),
     _pidReader( 0 ), _pidGZip( 0 )
{
}

bool
ByteInputGZip::read( char* buff, size_t n )
{
   if ( _fd == -1 ) return false;
   size_t count = 0;
   while ( count < n ) {
      if ( ! ByteInputFD::read( buff, n - count ) ) return false;
      buff += gcount();
      count += gcount();
   }
   return true;
}

ByteInputGZip::~ByteInputGZip()
{
   close( _fd );
   int status;
   if ( _pidReader > 0 ) { 
      kill( _pidReader, SIGTERM );
      if ( _pidReader != waitpid( _pidReader, &status, 0 ) )
	 perror( "waitpid()" );
   }
   if ( _pidGZip > 0 ) { 
      kill( _pidGZip, SIGTERM );
      if ( _pidGZip != waitpid( _pidGZip, &status, 0 ) )
	 perror( "waitpid()" );
   }
}
#endif // defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_) 

/*
 *  Input from a memory buffer.
 */

ByteInputMemory::ByteInputMemory( const char * buffer, size_t size  )
   : _buffer( buffer ),
     _current( buffer ),
     _end( buffer + size ),
     _gcount( 0 )
{
}

bool
ByteInputMemory::get( char & c )
{
   if ( _current == _end ) {
      _gcount = -1;
      return false;
   }
   c = *(_current++);
   _gcount = 1;
   return true;
}

bool
ByteInputMemory::getline( char* s, size_t n, char delim  )
{
   _gcount = 0;
   if ( _current == _end ) {
      _gcount = -1;
      return false;
   }
   ssize_t max = n - 1;
   while ( _current < _end && _gcount < max && *_current != delim ) {
      *(s++) = *(_current++);
      ++_gcount;
   }
   *s = '\0';
   if ( _current < _end && _gcount < max && *_current == delim ) {
      ++_current;
      ++_gcount;
   }
   return true;
}

bool
ByteInputMemory::read( char* s, size_t n )
{
   if ( _current == _end ) {
      _gcount = -1;
      return false;
   }
   if ( _current + n > _end ) {
      _gcount = _end - _current;
   } else {
      _gcount = n;
   }
   memcpy( s, _current, _gcount );
   _current += _gcount;
   return true;
}

size_t
ByteInputMemory::gcount() const
{
   if ( _gcount == -1 ) return 0;
   return static_cast<size_t>( _gcount );
}

ByteInputMemory::operator bool() const
{
   return ( _gcount >= 0 );
}
