/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   Dialog2DView.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class Dialog2DView
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIALOG2DVIEW_H_
#define _DIALOG2DVIEW_H_

#include <QDialog>
#include "ui_dialog2dview.h"

class Document;
class ColorMap;
class QButtonGroup;
class View2DWidget;

/**
 *  The Dialog2DView class.
 */
class Dialog2DView : public QDialog, public Ui::Dialog2DView {
  Q_OBJECT
 public:
  /**
   * Constructor with no arguments.
   */
  Dialog2DView(QWidget *);
  ~Dialog2DView();

  void document( Document * document );

  View2DWidget * viewWidget();
  bool showSlice();

 signals:

  void trim( int plane, int depth);
  void askForDisplayIn3D(bool);
  void closing();

 public slots:

  void changeMaxDepth(int maxDepth );
  void changeZoom();
  void refresh3D();
  void done( int r );
  void changeDepth( int depth, int plane );
  void changePlane( int plane );
  void changeAction( int );
  void callBackButtonsPlane( int id );
  void zoomIn();
  void zoomOut();
  void zoomFit();
  void zoomOne();
  void trimPushed();
  void selectedColorChanged( int value );
  void colorMapChanged();
  void changeInfo( QString text);

 protected:
  void closeEvent( QCloseEvent * event );

 private:
  View2DWidget * _view2DWidget;
  ColorMap * _colorMap;
  QButtonGroup *actionGroup;
  QButtonGroup *planeButtonGroup;
};

#endif // _DIALOG2DVIEW_H_

