/** -*- mode: c++ -*-
 * @file   FloatValidator.h
 * @author Sebastien Fourey (GREYC)
 * @date   Thu Jan 12 10:23:58 2006
 * 
 * @brief  
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _FLOATVALIDATOR_H_
#define _FLOATVALIDATOR_H_

#include <QValidator>

class FloatValidator : public QRegExpValidator {
 public:
  
  FloatValidator( QObject * parent, float min, float max, int decimals );
  
  float min() { return _min; }
  void min(float m) { _min = m; }
  float max() { return _max; }
  void max(float m) { _max = m; }
  int decimals() { return _decimals; }
  void decimals(int d) { _decimals = d; }
  State validate ( QString & input, int & pos ) const;
  void fixup ( QString & input ) const;
 protected: 
  void updateRegExp();
  
 private:
  float _min, _max;
  int _decimals;
};

#endif // _FLOATVALIDATOR_H_
