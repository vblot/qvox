/** -*- mode: c++ -*-
 * @file   ColorMapToolBar.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Jan 23 10:55:55 2006
 *
 * @brief  ColorMap Toolbar Widget.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _COLORMAPTOOLBAR_H_
#define _COLORMAPTOOLBAR_H_

#include "Settings.h"

#include <iostream>

#include <QWidget>
#include <QToolBar>
#include <QStringList>
class QBoxLayout;
class QComboBox;
class QFrame;
class QLabel;
class QPushButton;
class QSlider;
class QSpinBox;

class ColorMap;
class ColorMapWidget;

namespace QVoxGui {
enum ColorCellType { NormalCell, FramedCell };
}

class ColorMapWidget : public QWidget {
  Q_OBJECT
public:
  ColorMapWidget( QWidget *, ColorMap * );
  virtual ~ColorMapWidget();

  void mousePressEvent ( QMouseEvent * e );
  void paintEvent( QPaintEvent *);
  void cellsSize( const QSize & size );
  const QSize & cellsSize() { return _cellsSize; }
  QSize sizeHint () const;
  void setSizeHint( const QSize &);
  UChar selectedColor() { return _selectedColor; }
  void mouseDoubleClickEvent ( QMouseEvent * e );
  int heightForWidth( int ) const; // Virtual

public slots:
  void update();
  void selectColor(int);

protected:
  void redraw();

private:
  QSize _cellsSize;
  ColorMap *_colorMap;
  UChar _selectedColor;
  QSize _sizeHint;
  int _topMargin, _leftMargin;
  int _hcells, _vcells;  
};

class ColorMapToolBar : public QToolBar {
  Q_OBJECT

public:
  ColorMapToolBar( QMainWindow *, ColorMap * );
  ~ColorMapToolBar() {
  }

  void addLUTFilename(const QString &);

signals:
 void colorLUTFileSelected(QString);

public slots:
  void undock();
  void pbMaterialPushed();
  void selectColor( int n );
  void addLUT();
  void removeLUT();
  void onLUTSelected(int);

signals:
  void aboutToHide();
  void aboutToShow();

protected:
  void hideEvent( QHideEvent * );
  void showEvent( QShowEvent * );

protected slots:
  void colorMapChanged( int );

private:
  ColorMapWidget *_colorMapWidget;
  ColorMap *_colorMap;
  QSpinBox * _spinBoxColor;
  QComboBox * _cbType;
  QBoxLayout *_layout;
  QLabel *_labelMin;
  QSlider *_sliderMin;
  QLabel *_titleMaterial;
  QLabel *_labelMaterial;
  QPushButton *_pbMaterial;
  QFrame * _selectedColorFrame;
  QComboBox * _cbLUT;
  QPushButton * _pbAddLUT;
  QPushButton * _pbRemoveLUT;
  QStringList _lutFilenames;
};



#endif
