/** -*- mode: c++ -*-
 * @file   ViewParamsToolBar.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Jan 23 10:55:55 2006
 * 
 * @brief  ColorMap Toolbar Widget.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _VIEWPARAMSTOOLBAR_H_
#define _VIEWPARAMSTOOLBAR_H_

#include <QWidget>
#include <QTime>
#include <QMainWindow>
#include <QLayout>
#include <QToolBar>
#include <QToolTip>

#include <iostream>

class View3DWidget;
class QCheckBox;
class QComboBox;
class QPushButton;
class QBomboBox;

class ViewParamsToolBar : public QToolBar {
  Q_OBJECT
public:
  ViewParamsToolBar( QMainWindow *, View3DWidget * );
  ~ViewParamsToolBar() {}
 public slots:
   void setOrientation ( Qt::Orientation );
   void shadingChanged(int );
   void settingsChanged();
   void lightButtonToggled(bool);
   void undock();
 signals:
  void info( const QString & );
 private:  
   View3DWidget * _view3DWidget;
   QBoxLayout *_layout;
   QComboBox * _comboShading;
   QCheckBox * _cbFineDrawingMode;
#if defined(_EXTRA_FUNCTIONS_)
   QCheckBox * _cbMultithread;
   QPushButton *_pbActivateShading;
   QComboBox *_cbMaxThreads;
   QTime _timer;
#endif
};


#endif // defined( _VIEWPARAMSTOOLBAR_H_ )
