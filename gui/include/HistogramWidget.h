/** -*- mode: c++ -*-
 * @file   HistogramWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  A simple histogram widget.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _HISTOGRAMWIDGET_H_
#define _HISTOGRAMWIDGET_H_

#include <vector>

#include <QApplication>
#include <QImage>
#include <QPainter>
#include <QColor>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>

#include "zzzimage.h"

class Document;
class Dialog2DView;
class HistogramView;

class HistogramWidget : public QWidget { 
  Q_OBJECT

public:

  HistogramWidget( QWidget * parent );
  ~HistogramWidget();
 
  void showEvent( QShowEvent * );
  void hideEvent( QHideEvent * );
  void paintEvent( QPaintEvent * e);
  void resizeEvent( QResizeEvent * );
  void document( Document * document, int band );
  HistogramView & histogramView() { return *_histogramView; }

 signals:

  void minChanged(const QString&);
  void maxChanged(const QString&);

 public slots:

   void update();
   void setMin(const QString& );
   void setMax(const QString& );
   void viewMinChanged(int);
   void viewMaxChanged(int);

 private:

  Document *_document;
  std::vector<Size> _histogram;
  int _band;
  QString _strMin;
  QString _strMax;
  QLabel *_labelMin;
  QLabel *_labelMax;
  HistogramView *_histogramView;
};

#endif // _HISTOGRAMWIDGET_H_
