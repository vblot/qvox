/** -*- mode: c++ -*-
 * @file   Document.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:32:05 2005
 *
 * @brief  Volume document class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DOCUMENT_H_
#define _DOCUMENT_H_

#include <map>
#include <vector>

#include <QObject>
#include <QImage>
#include <QFileInfo>

#include "zzzimage.h"
#include "genesis.h"
#include "distance_transform.h"
#include "skel.h"
#include "QVoxVolume.h"

#include "Settings.h"
#include "tools.h"
#include "Triple.h"

class PluginDialog;
class QProcess;

class Document : public QObject {
  Q_OBJECT

public:
  
  enum SkelMethod { SkelSequential, SkelParallel };

public:

  Document();
  ~Document();

  bool modified() { return _modified; }
  void modified( bool );
  void save();
  void saveAs( const QString & filename );
  int valueType() { return _valueType; }
  int bands() { return _image->bands(); }

  bool valid() { return _valid; }
  void valid( bool on );

  AbstractImage * image() { return _image; }
  
  void qvoxLogo();
  const QString & fileName() { return _fileName; }
  void fileName( const QString & fileName ) { _fileName = fileName; }
  void notify();

  /**
   * Change the color of a voxel in the volume.
   *
   * @param x
   * @param y
   * @param z
   * @param red
   * @param green
   * @param blue
   * @param refresh
   *
   * @return true if the change created or removed a voxel (i.e. with non zero value).
   */
  bool setVoxelValue( SSize x, SSize y, SSize z,
                      UChar colorIndex,
                      UChar red, UChar green, UChar blue,
                      bool refresh = true );

  bool setVoxelValue( Triple<SSize> voxel,
                      UChar colorIndex,
                      UChar red, UChar green, UChar blue,
                      bool refresh = true );
  
  void drawLine( SSize x1, SSize y1, SSize z1,
                 SSize x2, SSize y2, SSize z2,
                 UChar colorIndex,
                 UChar red, UChar green, UChar blue,
                 bool refresh );

  void drawLine( Triple<SSize> p1,
                 Triple<SSize> p2,
                 UChar colorIndex,
                 UChar red, UChar green, UChar blue,
                 bool refresh );


  void create( SSize width, SSize height, SSize depth,
               SSize bands,
               ValueType valueType );

  void histo3D();
  void raiseLevelMap( SSize maxHeight = 255  );

  void fitBoundingBox( SSize margin = 0 );
  void trim( int plane, int n );
  void mirror( int axis );
  void rotate( int axis );
  void distanceMap();
  void negative();
  void grayShadeFromHue();
  void grayShadeFromSaturation();
  void binarize( UChar red = 255, UChar green = 255, UChar blue = 255  );
  void clear();
  void toGray();
  void toColor();
  void subSample( UChar minCount = 4 );
  void scale( UChar direction, UChar factor = 2 );
  void extrude( SSize depth );
  void spongify();
  void hollowOut( int adjacency );
  void surfaceFill();
  void fill();

  void applyDWT( int finalSize );

  void addBoundaries( UChar value, UChar thickness );

  void bitPlane( UChar bit, SSize band );
  void bitPlaneGrayCode( UChar bit, SSize band );
  void quantize( UInt32 cardinality );
  void addNoise( float ratio );
  void demoPlugin();
  
  void sphere( SSize x, SSize y, SSize z, SSize radius );

  void hyperbolicParaboloid( Triple<SSize> center,
                             float c1, float c2,
                             Rotation rotation,
                             Triple<float> scale );

  void paraboloid( Triple<SSize> center,
                   float c1, float c2,
                   Rotation rotation,
                   Triple<float> scale );

  void plane( Triple<SSize> center,
              Triple<float> normal,
              Rotation rotation );

  void torus( Triple<SSize> center,
              SSize largeRadius,
              SSize smallRadius,
              Rotation rotation,
              Triple<float> scale );

  void skel( bool singlePass, int adjacency, SkelMethod method, int terminalTest );
  
  void clamp( const Triple<Int32> & min, const Triple<Int32> & max );

  void clamp( const Triple<UInt32> & min, const Triple<UInt32> & max );

  void clamp( const Triple<Float> & min, const Triple<Float> & max );

  bool load( const char * filename, bool newFileName = true );
  bool load( ByteInput & in );

  bool merge( const char * filename, int axis );
  bool loadRaw( const char * filename, bool swapEndianness, bool multiplexed );

  bool convert( int valueType );

  Size rawSize();

  bool loadPixmap( const char * filename );
  bool importMesh( const QString & fileName, bool filled );
  bool loadablePixmap( const char * filename );

  PluginDialog * activePlugin();
  void activePlugin( PluginDialog * plugin );

public slots:

  void message( const QString & str, int timeout = 0 );


signals:

  void invalidated();
  void volumeChanged();
  void volumeContentChanged();
  void info( const QString &, int timeout = 0 );
  void pluginActivated();
  void runningPlugin(PluginDialog* );

private:

  void checkFullBox();

  void free();
  
  AbstractImage *_image;
  ZZZImage<UChar> *_volume;

  QString _fileName;
  bool _modified;
  bool _equalization;
  int _valueType;
  bool _valid;
  PluginDialog * _activePlugin;
};

template< typename T >
void realignment( ZZZImage<UChar> & result,
                  const ZZZImage<T> & source );

void realignment( ZZZImage<UChar> & result,
                  const ZZZImage<Bool> & source );

template< typename T >
void equalization( ZZZImage<UChar> & result,
                   const ZZZImage<T> & source );

void equalization( ZZZImage<UChar> & result,
                   const ZZZImage<Bool> & source );


void realignmentDistanceMap( ZZZImage<UChar> & result,
                             const ZZZImage<Int32> & source );

template< typename T >
void cast_copy( ZZZImage<UChar> & result, const ZZZImage<T> & source );

void cast_copy( ZZZImage<UChar> & result, const ZZZImage<Bool> & source );


#endif // _DOCUMENT_H_

