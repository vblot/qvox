/** -*- mode: c++ -*-
 * @file   ColorMap.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Jan 23 10:56:18 2006
 * 
 * @brief  ColorMap class.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _COLORMAP_H_
#define _COLORMAP_H_

#include "globals.h"

#include <cstring>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <QObject>
#include <QColor>
#include <QImage>

#include "tools.h"

using std::memset;
using std::floor;
using std::rand;
using std::srand;

class ColorMap : public QObject {

  Q_OBJECT
 
public:

  enum Type {
    Gray,
    Red,
    Green,
    Blue,
    Random,
    Contrast,
    Colors,
    HueScale,
    Curvature,
    Light,
    Toon,
    Undefined
  };

  
  ColorMap(int size = 256);
  
  virtual ~ColorMap();
  
  UChar * pixels( int color ) {
    return _array[ color ];
  }
  
  UChar ** pixels() { return _array; }

  UChar * singleLine() { return _singleLine; } 
  
  void setSingleLineColor( const QColor & color );
  
  QColor singleLineColor() const { return _singleLineColor; }

  const UChar * backgroundLine() const { return _backgroundLine; }
  
  void setBackgroundColor( const QColor & color );
  
  QColor backgroundColor() const { return _backgroundColor; }

  int type() const { return _type; }

  void setColor( UChar index, const QColor & color );

  const QColor * qcolors() const { return _qcolors; }

  const QColor & qcolor( int n ) const { return _qcolors[ n ]; }

  UChar selectedColor() const { return _selectedColor; }

  QColor selectedQColor() const { return _qcolors[ _selectedColor ]; }

  QImage image() const;


  /** 
   * Draw a 256x25 image of the colormap at a given position.
   * 
   */
  void draw(QPainter & , int x, int y ) const;

  void setMaterialColor( QColor );

  const QColor & toonColor() const { return _toonColor; }

  void toonColor( const QColor & color );

  const QColor & operator[]( int i ) const { return _qcolors[ i ]; }

  int size() const { return _size; }

  void trueColor( UChar red, UChar green, UChar blue );
  
 public slots:

  void type( int type, int min = -1, int max = -1 );
  void setMin( int min );
  void setMax( int max );
  void reverse();
  void selectColor(int color);

 signals:
  void changed( int );
  void colorSelected( int );

 private:

  ColorMap(const ColorMap &):QObject(),_bytesPerPixel(32) { }
  ColorMap & operator=(const ColorMap &) { return *this; }
  void HSVtoRGB(float &r, float &g, float &b,
		float h, float s, float v) const;  
  void alloc( int size );

  UChar ** _array;
  QColor * _qcolors;
  UChar * _singleLine;
  QColor _singleLineColor;
  UChar * _backgroundLine;
  QColor _backgroundColor;
  QColor _toonColor;
  int _type;
  int _size;
  int _lineSize;
  int _min, _max;
  int _selectedColor;
  const int _bytesPerPixel;
};

#endif // _COLORMAP_H_
