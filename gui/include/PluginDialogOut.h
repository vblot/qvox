/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginDialogOut.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginDialogOut
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _PLUGINDIALOGOUT_H_
#define _PLUGINDIALOGOUT_H_

#include "PluginDialog.h"

/**
 *  The PluginDialogOut class.
 */
class PluginDialogOut : public PluginDialog {
   Q_OBJECT
 public:

   PluginDialogOut( QWidget * parent,
		    const QDomElement & xmlPlugin,
		    Document & document,
		    const QString & xmlPath,
		    const QString & binPath,
		    const QString & libPath,
		    const QString & workingDir );

   Type type() const;
   QProcess::ProcessState state() const;

 public slots:

   void launch(); // virtual
   int execute(); // virtual
   void processFinished( int exitCode, QProcess::ExitStatus exitStatus ); // virtual
   void processError( QProcess::ProcessError ); // virtual

   void okClicked();
   void applyClicked();
   void closeClicked();
   void terminate();

 signals: 

   void runningPlugin( bool );
    
 protected:

 private:

   QString _outputFilename;
   QProcess * _process;
};

#endif // _PLUGINDIALOGOUT_H_
