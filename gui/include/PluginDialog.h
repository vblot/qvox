/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginDialog.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginDialog
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _PLUGINDIALOG_H_
#define _PLUGINDIALOG_H_

#include <QObject>
#include <QDialog>
#include <QStatusBar>
#include <QtXml>
#include <QString>

class QGridLayout;
class Document;
class QLabel;
class QPushButton;
class QGridLayout;
class QCheckBox;

/**
 *  The PluginDialog class.
 */
class PluginDialog : public QDialog {
   Q_OBJECT

 public:


   enum Type { InputOutput, Input, Output };
   enum ParamType { IntegerParam, TextParam, FloatParam, RadioParam, CheckBoxParam,
		    FileNameParam, SpinIntegerParam, SpinDoubleParam, SelectParam }; 

   PluginDialog( QWidget * parent,
		 const QDomElement & xmlPlugin,
		 Document & document,
		 const QString & xmlPath,
		 const QString & binPath,
		 const QString & libPath,
		 const QString & workingDir );

   virtual void addParametersWidgets( int & row );

   QString command() const;
   QString inputExtension() const;
   QString outputExtension() const; 
   int paramsCount() const;
   virtual Type type() const = 0;
   virtual QProcess::ProcessState state() const = 0;

   static PluginDialog * createPluginDialog( QWidget * parent,
					     const QDomElement & xmlPlugin,
					     Document & document,
					     const QString & xmlPath, 
					     const QString & binPath,
					     const QString & libPath,
					     const QString & workingDir );

								  
 public slots:

   virtual int execute() = 0;
   virtual void launch();
   virtual void processFinished( int exitCode, QProcess::ExitStatus exitStatus ) = 0;
   virtual void processError( QProcess::ProcessError ) = 0;

   virtual void okClicked() = 0;
   virtual void applyClicked() = 0;
   virtual void closeClicked() = 0;
   virtual void terminate() = 0;
   void defaults();

 signals:
  
   void runningPlugin( bool );

 protected:

   void setProcessEnv( QProcess * process );
   void setWToolTip( QWidget * w, const QDomNode & n );

   QPushButton * _pbOk;
   QPushButton * _pbApply;
   QPushButton * _pbClose;
   QPushButton * _pbDefaults;
   QLabel * _topLabel;
   QCheckBox *_cbShowConsole;
   Type _type;
   QString _name;
   QDomElement _xmlPlugin;
   std::vector<QWidget*> _fields;
   std::vector<ParamType> _paramTypes;
   QString _command;
   Document * _document;
   QStatusBar * _statusBar;
   QGridLayout * _grid;

   QString _xmlPath;
   QString _binPath;
   QString _libPath;
   QString _workingDir;

   static QString attrValue( const QDomNode & node, const char * parameter );
};

#endif // _PLUGINDIALOG_H_
