/** -*- mode: c++ -*-
 * @file   Dialogs.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Jan 23 10:51:09 2006
 * 
 * @brief  Commom dialogs
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIALOGS_H_
#define _DIALOGS_H_

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QString>
#include <QIntValidator>
#include <QSpinBox>
#include <QCheckBox>
#include <QMessageBox>

#include "Settings.h"

class  QSlider;
class  QLabel;
class  QPushButton;

#include <iostream>

#include "globals.h"

class MainWindow;

enum FileFilter { VolumeFile,
		  ViewParametersFile, 
		  ViewExportFile,
		  SurfaceExportFile,
		  RawFile,
		  QImagesSequence
};

class Dialogs : public QObject {
  Q_OBJECT
 public:


  Dialogs( QWidget * parent );
  ~Dialogs();

  QString askSaveFileName( FileFilter filter, QWidget * parent = 0 );
  QString askOpenFileName( FileFilter filter, QWidget * parent = 0 );
  const QString & selectedFilter() { return _selectedFilter; }
  
  void errorOpening( const char * fileName, const QString & details = QString::null );
  QMessageBox::StandardButton confirmDoNotSave( const QString & fileName, QWidget *parent );

 private:
  QWidget  * _parent;
  QString _selectedFilter;
  static QString _path;
};

class IntegerDialog : public QDialog { 
  Q_OBJECT
 public:
  IntegerDialog( QWidget * parent = 0 );
  static int ask( QWidget * parent, int min, int max, bool & ok, int value = -1 );
  void range( int min, int max, int value );
  int value() { return _spinBox->value(); }
 protected:
  QSlider * _slider;
  QSpinBox *_spinBox;
  QIntValidator _validator;
  QLabel *_label;
  QLabel *_minLabel;
  QLabel *_maxLabel;
  QPushButton * _pbOk;
  QPushButton * _pbCancel;
 private:
};


class OptionalDialog : public QDialog {
  Q_OBJECT
 public:
  OptionalDialog( QWidget * parent,
		  const QString & caption,
		  const QString & text,
		  Settings::OptionalMessage option );


  ~OptionalDialog();

 public slots:
 
  static int message( QWidget * parent, 
		      const QString & caption, 
		      const QString & text, 
		      Settings::OptionalMessage option );

  void checkBoxToggled( bool );

  
  
protected:
  Settings::OptionalMessage _optionalMessage;
  QLabel *_textLabel;
  QPushButton * _pbOk;
  QPushButton * _pbCancel;
  QCheckBox * _checkBox;
};


#endif

