/** -*- mode: c++ -*-
 * @file   Settings.h
 * @author Sebastien Fourey (GREYC)
 * @date   Sun Jan  8 12:57:37 2006
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include "globals.h"
#include "Triple.h"

#include <vector>
#include <set>

#include <QObject>
#include <QString>
#include <QSettings>
#include <QColor>
#include <QStringList>
#include <QAction>

namespace QVoxSettings {
  enum CurvatureType { Mean = 0 , Gaussian, Maximum };
}

class Settings : public QObject {
  Q_OBJECT
 public:

  Settings();
  ~Settings();

  enum OptionalMessage { FullBox };

  void load();
  void save();

  void saveOnExit( bool on );
  inline bool saveOnExit();

  inline QAction * fullVolumeBoxAction();
  inline bool transparency();
  inline int opacity();
  inline bool showFPS();
  inline bool drawAxis();
  inline bool drawColorMap();
  inline int shading();
  inline const QColor & bgColor() ;
  inline const QColor & exportOFFColor();
  inline int drawRadius();
  inline UShort normalsEstimateIterations();
  inline bool normalsEstimateAdaptive();
  inline UShort curvatureEstimateIterations();
  inline UShort curvatureEstimateAveragingIterations();
  inline bool curvatureEstimateAdaptive();
  inline QVoxSettings::CurvatureType curvatureType();
  inline UShort exportOFFIterations();
  inline bool exportOFFsurfelsColors();
  inline bool exportOFFAdaptive();
  inline UShort fitBoundingBoxFrameSize();
  inline UShort levelMapMaxHeight();
  inline UShort dwtFinalSize();
  inline int adjacencyPair();
  inline int colormap();
  inline QStringList & activeToolBarsList();
  inline std::vector< QString > & recentFiles();
  inline int rotationBoxMaxFrames();
  inline bool multiThreading();
  inline int maxThreads();
  inline bool fineDrawingMode();
  inline int consoleFontSize();
  inline bool computeVextexIndices();

  void addRecentFile( const QString & fileName );
  bool hasOptionalMessage( OptionalMessage m );
  QStringList & pluginsFiles();


 public slots:

   void transparency( bool on );
   void opacity( int alpha );
   void showFPS( bool on );
   void drawAxis( bool on );
   void drawColorMap( bool on );
   void drawRadius( int );
   void normalsEstimateIterations( int n );
   void normalsEstimateAdaptive( bool b );
   void curvatureEstimateIterations( int n );
   void curvatureEstimateAveragingIterations( int n );
   void curvatureEstimateAdaptive( bool b );
   void curvatureType( int t );
   void shading( int t );
   void exportOFFIterations( int n );
   void exportOFFsurfelsColors( bool b );
   void exportOFFColor( const QColor & c );
   void exportOFFAdaptive( bool b );
   void fitBoundingBoxFrameSize( int n );
   void levelMapMaxHeight( int n );
   void dwtFinalSize( int n );
   void adjacencyPair( int pair );
   void colormap( int );
   void bgColor( const QColor & );
   void addOptionalMessage( OptionalMessage m );
   void removeOptionalMessage( OptionalMessage m );
   void consoleFontSize( int );
   void multiThreading( bool );
   void rotationBoxMaxFrames(int n);
   void fineDrawingMode( bool );
   void maxThreads(int );
   void computeVertexIndices(bool);

 signals:

   void notify();
   void shadingChanged(int );
   void adjacencyPairChanged( int pair );
   void opacityChanged(int );

 private:
   QAction * _fullVolumeBoxAction;
   bool _showFPS;
   bool _drawAxis;
   bool _drawColorMap;
   int _shading;
   int _drawRadius;
   bool _transparency;
   int  _opacity;
   unsigned short _normalsEstimateIterations;
   bool _normalsEstimateAdaptive;
   unsigned short _curvatureEstimateIterations;
   unsigned short _curvatureEstimateAveragingIterations;
   bool _curvatureEstimateAdaptive;
   unsigned short _exportOFFIterations;
   bool _exportOFFsurfelsColors;
   QColor _exportOFFColor;
   bool _exportOFFAdaptive;
   unsigned short _fitBoundingBoxFrameSize;
   unsigned short _levelMapMaxHeight;
   unsigned short _dwtFinalSize;
   QVoxSettings::CurvatureType _curvatureType;
   bool _saveOnExit;
   int _adjacencyPair;
   int _colormap;
   std::vector< QString > _recentFiles;
   QStringList _activeToolBarsList;
   QStringList _pluginsFiles;
   QColor _bgColor;
   int _consoleFontSize;
   bool _multiThreading;
   std::set<OptionalMessage> _optionalMessages;
   int _rotationBoxMaxFrames;
   bool _fineDrawingMode;
   int _maxThreads;
   bool _computeVertexIndices;
};

extern Settings * globalSettings;

/*
 * Inline methods definitions
 */

bool Settings::saveOnExit() { return _saveOnExit; }
QAction * Settings::fullVolumeBoxAction() { return _fullVolumeBoxAction; }
bool Settings::transparency() { return _transparency; }
int Settings::opacity() { return _opacity; }
bool Settings::showFPS() { return _showFPS; }
bool Settings::drawAxis() { return _drawAxis; }
bool Settings::drawColorMap() { return _drawColorMap; }
int Settings::shading() { return _shading; }
const QColor & Settings::bgColor() { return _bgColor; }
const QColor & Settings::exportOFFColor() { return _exportOFFColor; }
int Settings::drawRadius() { return _drawRadius; }
QVoxSettings::CurvatureType Settings::curvatureType() { return _curvatureType; }
UShort Settings::exportOFFIterations() { return _exportOFFIterations; }
bool Settings::exportOFFsurfelsColors() { return _exportOFFsurfelsColors; }
bool Settings::exportOFFAdaptive() { return _exportOFFAdaptive; }
UShort Settings::fitBoundingBoxFrameSize() { return _fitBoundingBoxFrameSize; }
UShort Settings::levelMapMaxHeight() { return _levelMapMaxHeight; }
UShort Settings::dwtFinalSize() { return _dwtFinalSize; }
int Settings::adjacencyPair() { return _adjacencyPair; }
int Settings::colormap() { return _colormap; }
QStringList & Settings::activeToolBarsList() { return _activeToolBarsList; }
std::vector< QString > & Settings::recentFiles() { return _recentFiles; }
int Settings::rotationBoxMaxFrames() { return _rotationBoxMaxFrames; }
int Settings::maxThreads() { return _maxThreads; }
int Settings::consoleFontSize() { return _consoleFontSize; }
bool Settings::computeVextexIndices() { return _computeVertexIndices; }

UShort Settings::normalsEstimateIterations() {
  return _normalsEstimateIterations;
}
bool Settings::normalsEstimateAdaptive() { return _normalsEstimateAdaptive; }
UShort Settings::curvatureEstimateIterations() {
  return _curvatureEstimateIterations;
}
UShort Settings::curvatureEstimateAveragingIterations() {
  return _curvatureEstimateAveragingIterations;
}
bool Settings::curvatureEstimateAdaptive() {
  return _curvatureEstimateAdaptive;
}
bool Settings::multiThreading() {
  return _multiThreading;
}
bool Settings::fineDrawingMode() {
  return _fineDrawingMode;
}

#endif

