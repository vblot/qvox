/** -*- mode: c++ -*-
 * @file   DialogThresholds.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:21:02 2005
 *
 * @brief  2D volume view Widget class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIALOGTHRESHOLDS_H_
#define _DIALOGTHRESHOLDS_H_

#include <QVariant>
#include <QDialog>
#include <QValidator>

#include "Document.h"
#include "Triple.h"
#include "HistogramWidget.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QCheckBox;
class QLabel;
class QLineEdit;
class QFrame;
class QTableWidget;
class QPushButton;
class Surface;

class DialogThresholds : public QDialog
{
  Q_OBJECT
public:

  DialogThresholds( QWidget* parent = 0, Qt::WindowFlags wf = 0 );
  ~DialogThresholds();
  void setDocument( Document * );
  void setSurface( Surface * );

public slots:

  void volumeChanged();
  void applyVolume();
  void applySurface();

protected:

  Document * _document;
  Surface * _surface;

  QDoubleValidator * _doubleValidator;
  QIntValidator * _intValidator;
  bool _changed;

  QLineEdit* leMax1;
  QLineEdit* leMin1;
  QLineEdit* leMax0;
  QLineEdit* leMax2;
  QLineEdit* leMin0;
  QLineEdit* leMin2;

  QCheckBox *cbShowMin;

  QPushButton* buttonCancel;
  QPushButton* buttonApplyVolume;
  QPushButton* buttonApplySurface;
  QPushButton* buttonRefresh;
  QPushButton* buttonClose;

  HistogramWidget *_histogramWidget[3];

  QLabel* textLabel1_2;
  QLabel* textLabel1_2_3;
  QLabel* textLabel1_2_4;
  QLabel* textLabel1_2_5;
  QLabel* textLabel1_2_2;


protected slots:
  virtual void languageChange();

private:
  void init();
  void destroy();

};

#endif // _DIALOGTHRESHOLDS_H_

