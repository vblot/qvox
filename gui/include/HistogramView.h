/** -*- mode: c++ -*-
 * @file   HistogramView.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 *
 * @brief  A simple histogram widget.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _HISTOGRAMVIEW_H_
#define _HISTOGRAMVIEW_H_

#include "globals.h"

#include <QColor>
#include <vector>
#include <QWidget>

class HistogramView : public QWidget { 
  Q_OBJECT
public:
  HistogramView( const std::vector<Size> & histogram,
                 QWidget * parent = 0 );
  ~HistogramView();

  void paintEvent( QPaintEvent * e);
  void resizeEvent( QResizeEvent * );
  void mousePressEvent( QMouseEvent * e );
  void mouseReleaseEvent( QMouseEvent * e );
  void mouseMoveEvent( QMouseEvent * e );
  void keyPressEvent( QKeyEvent * e );
  void color( const QColor & color );

public slots:

  void update();
  void showMin( bool on );
  void setMin( int );
  void setMax( int );

signals:
  void valueChanged(int);
  void minChanged(int);
  void maxChanged(int);

private:
  void draw();
  void computeViewParams();
  UInt bin(int x);
  int _underMouseCol, _underMouseRow;
  int _prevX, _prevY;
  bool _leftButton, _rightButton;
  const std::vector<Size> & _histogram;
  Size _maxCount;
  QColor _penColor;
  bool _showMin;
  UInt _xPos;
  UInt _minPos;
  UInt _maxPos;
  QPixmap _pixmap;
};

#endif // _HISTOGRAMVIEW_H_

