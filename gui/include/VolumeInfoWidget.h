/** -*- mode: c++ -*-
 * @file   VolumeInfoWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:19:06 2005
 * 
 * @brief  View of informations about a volume document.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _VOLUMEINFOWIDGET_H_
#define _VOLUMEINFOWIDGET_H_

#include "Document.h"
#include "zzzimage.h"

#include <QLabel>
class Surface;

class VolumeInfoWidget : public QLabel {
  Q_OBJECT
 public:
  VolumeInfoWidget( QWidget * parent );
  ~VolumeInfoWidget();

  void document( Document * document ); 
  void surface( Surface * surface );
  void surfels( Size n );

  void message(const QString &);


 public slots:
    void updateDocument();
    void updateSurface();
    void clear();

 private: 
  Document * _document;
  Surface * _surface;
  SSize _bands, _width, _height, _depth;
  Size _surfels;
  QString _volumeSizeString;
};


#endif

