/** -*- mode: c++ -*-
 * @file   View2DWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:21:02 2005
 * 
 * @brief  2D volume view Widget class.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _VIEW2DWIDGET_H_
#define _VIEW2DWIDGET_H_

#include <iostream>

#include <QWidget>
#include <QRect>
#include <QImage>

#include "zzzimage.h"
#include "ColorMap.h"

class Document;
class Dialog2DView;
class QScrollArea;

class View2DWidget : public QWidget { 
  Q_OBJECT    
public:
  View2DWidget( QWidget * parent = 0 );
  ~View2DWidget();

  enum Plane { PlaneOyz = 0, PlaneOxz, PlaneOxy };
  enum Mode { Draw = 0, Fill, Move, Brush };
  
  void document( Document * document );
  void scrollArea( QScrollArea * scrollArea );

  Document * document() { return _document; }

  void colorMap( ColorMap * colorMap );  

  void paintEvent( QPaintEvent * e);
  void resizeEvent( QResizeEvent * );
  void mousePressEvent( QMouseEvent * e );
  void mouseReleaseEvent( QMouseEvent * e );
  void mouseMoveEvent( QMouseEvent * e );
  void keyPressEvent( QKeyEvent * e );  
  int zoom() { return _zoom; }
  int maxZoom() { return _maxZoom; }
  void zoomIn();
  void zoomOut();
  void zoomFit();
  void zoomOne();

  void redrawPixel(int col, int row);
  
  int maxOriginRow() { return _maxOriginRow; }
  int maxOriginCol() { return _maxOriginCol; }
  int originCol() { return _originCol; }
  int originRow() { return _originRow; }
  
  void setPlane( Plane plane, bool force=false );
  Plane plane() { return _plane; }
  int depth() { return _depth; }

  void size( int w, int h );

  QSize sizeHint() const;
  QSize minimumSizeHint() const;

  /** 
   * Single shot consult of the status "volumeModified".
   * Status will be set to false before returning the
   * current value;
   * 
   * @return True if the volume has been modified throught this view 
   * since the last time this function has been called.
   */
  bool volumeModified();

  void animateSlices();
  
  const QImage * image() { return & _image; }

  Mode mode() { return _mode; }

 public slots:

   void mode( Mode mode );
   void copyPlane();
   void cutPlane();
   void pastePlane();
   void setDepth( int depth );
   void updateDoc(); 
   void slide( int dx, int dy );


 signals:

  void moveOrigin( int x, int y );
  void maxDepthChanged(int);
  void depthChanged(int, int);
  void planeChanged(int);
  void infoChanged( QString );
  void zoomChanged();
  void writeViewRequest();
  

 protected:
  void hideEvent( QHideEvent * );
  void showEvent( QShowEvent * );
  void wheelEvent( QWheelEvent * event );

 private:
  void forceRedraw();
  void draw( int & xOut, int & yOut, const QRect & clip );
  void drawVoxel( int x, int y);
  void drawLine( int x1, int y1, int x2, int y2 );
  void fillPlane( int x, int y);
  void clearVoxel( int x, int y);

  ColorMap * _colorMap;
  int _cols, _rows;
  int _zoom, _maxZoom;
  QRect _clipRect;
  int _width, _height;
  QScrollArea * _scrollArea;

  AbstractImage * _clipBoard;


  int _originCol, _originRow, _depth;
  int _maxOriginCol, _maxOriginRow;
  int _topMargin, _leftMargin;
  int _volumeWidth, _volumeHeight, _volumeDepth; /*  */
  Plane _plane;
  bool _drawingMode;
  int _x, _y, _z;  
  int _originCols[3];
  int _originRows[3];
  AbstractImage * _volume;
  Document *_document;
  QImage _image;
  int _underMouseCol, _underMouseRow;
  int _bands;
  int _prevX, _prevY;
  bool _leftButton, _rightButton;
  bool _volumeModified;
  bool _valid;
  bool _needToRedraw;
  Mode _mode;
};

#endif
