/** -*- mode: c++ -*-
 * @file   AnimationToolBar.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Jan 23 10:56:41 2006
 * 
 * @brief  Toolbar to display/edit animation parameters.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _ANIMATIONTOOLBAR_H_
#define _ANIMATIONTOOLBAR_H_

#include <iostream>

#include <QToolBar>

class QLineEdit;
class QPushButton;
class QToolBar;
class QLabel;
class QComboBox;
class QCheckBox;

class View3DWidget;

class AnimationToolBar : public QToolBar {
  Q_OBJECT

public:
  AnimationToolBar( QMainWindow * );
  ~AnimationToolBar() {
  }

  void setOrientation ( Qt::Orientation );
  
  const QString & path() { return _path; }
  QString nextFileName();
  void setView3DWidget( View3DWidget * widget );
  
public slots:
  void choosePath();
  void changeFormat( const QString & );
  const QString & format() { return _format; }
  void undock();
  void reset();
  void saveImage();
  void animate();
  void endInteractive();
  void captureFileSelect();

private:
  QLineEdit *_lineEdit;
  QPushButton *_pbChoosePath;
  QPushButton *_pbReset;
  QPushButton *_pbWriteImage;

  QPushButton *_pbCapture;
  QPushButton *_pbCaptureFile;
  QString _path;
  QString _format;
  QComboBox *_cbFormats;
  QCheckBox *_checkInteractive;
  int _imageNumber;
  View3DWidget *_view3DWidget; 

};



#endif
