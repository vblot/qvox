/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   MainWindow.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class MainWindow
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_


class Dialogs;
class DialogAbout;
class DialogSettings;
class DialogInputSize;
class DialogAspect;
class DialogMerge;
class DialogSkel;
class DialogThresholds;
class DialogExperiments;
class ColorMapToolBar;
class VolumeInfoWidget;
class ColorMapView;
class Dialog2DView;
class AnimationToolBar;
class ToolsToolBar;
class ActionsToolBar;
class ExperimentToolBar;
class ViewParamsToolBar;
class QProgressBar;
class QMenu;
class QAction;
class QActionGroup;

class PluginDialog;
class PluginProgressWidget;

#include "ui_mainwindow.h"

#include "Document.h"

#include <QList>
#include <QtXml>
#include <vector>

class PluginDialog;

/**
 *  The MainWindow class.
 */
class MainWindow : public QMainWindow, public Ui::MainWindow {
   Q_OBJECT
 public:
  /**
   * Constructor with no arguments.
   */
  MainWindow(QWidget * parent = 0, Qt::WindowFlags flags = 0 );
  ~MainWindow();

  void setApplication( QApplication *application);
  Document & document();
  Dialogs * dialogs();

  void show();
  void setFileName( const QString & fileName );
  void openFile( QString fileName );

  bool confirmNoSave();
  void resize3DView( const QSize & size );
  View3DWidget & view3DWidget();

 public slots:

  void actionSelected( QAction *action );
  void adjacencyPairChanged( int index );
  void dialogSkelClosing();
  void fileExit();
  void fileImport();
  void fileMerge();
  void fileNew();
  void fileOpen();
  void fileOpenRecent( QAction* );
  void filePrint();
  void fileSave();
  void fileSaveAs();
  void helpAbout();
  void popupRecentFileOpened();
  void popupShadingOpened();
  void moveLight( bool );
  void selectedColorChanged( int );
  void shadingChanged( int );
  void opacityChanged( int );
  void shadingMenuSelected( QAction * );
  void show2DView();
  void showSettings();
  void showSkelDialog();
  void surfaceColorizeBorder();
  void surfaceExport();
  void volumeClamp();
  void toolBarsMenuAboutToShow();
  void toolBarsMenuActivated( QAction *action );
  void updateCaption();
  void view2DClosing();
  void viewAspect();
  void viewExport();
  void viewLoadParameters();
  void viewSaveParameters();
  void volumeAddBoundaries();
  void volumeBinarize();
  void volumeChanged();
  void volumeClear();
  void volumeColorHistogram();
  void volumeDWT();
  void volumeDistanceMap();
  void volumeExtrude();
  void volumeSpongify();
  void volumeFit();
  void volumeFitFrame();
  void volumeHollowOut();
  void volumeSurfaceFill();
  void volumeHoleFill();
  void volumeNegative();
  void volumeGrayShadeFromHue();
  void volumeGrayShadeFromSaturation();
  void volumeMirror( QAction * );
  void volumeRotate( QAction * );
  void volumeQuantize();
  void volumeAddNoise();
  void volumeRaiseLevelMap();
  void volumeScale( QAction* );
  void volumeSubSample();
  void volumeToColor();
  void volumeToGray();
  void volumeTrim(int, int);
  void volumeDemoPlugin();
  void changeDisplay2DIn3D( bool display );
  void dumpVolume( bool on ) { _dumpVolume = on; }
  void drawAxis( bool on );
  void pluginMenuActivated( QAction *action );
  void volumeConvertAction( QAction * ); 
  void runningPlugin(PluginDialog* );
  void cutPlane(bool);
  void dialogSettingsClosed();
  void colorLUTSelected(QString);
#ifdef HAVE_EXPERIMENTAL
  void showExperiments( bool on); 
#endif

   void buildPluginsMenus(); 


 protected:

  void closeEvent( QCloseEvent *e );
  void connections();
  Dialog2DView *_dialog2DView;

   void createMenus();
   void createToolBars();
   void createDialogs();

   QMenu * buildPluginsMenu( QDomNode node, QString title,
			     const QString & xmlPath,
			     const QString & binPath,
			     const QString & libPath,
			     const QString & workingDir ); 

 private:

   Document _document;

   Dialogs *_dialogs;
   DialogAbout *_dialogAbout;
   DialogSettings *_dialogSettings;
   DialogInputSize *_dialogInputSize;
   DialogAspect *_dialogAspect;
   DialogSkel *_dialogSkel;
   DialogThresholds *_dialogThresholds;
   DialogExperiments *_dialogExperiments;
   DialogMerge *_dialogMerge;
   ColorMapToolBar *_colorMapToolBar;
   ColorMapView *_colorMapView;
   QProgressBar *_progressBar;
   AnimationToolBar *_animationToolBar;
   ToolsToolBar *_toolsToolBar;
   ViewParamsToolBar *_viewParamsToolBar;
   ExperimentToolBar *_experimentToolBar;
   VolumeInfoWidget *_volumeInfoWidget;

   bool _dumpVolume;

   QList<QDomDocument*> _pluginsXmlDocs;
   QList<PluginDialog*> _plugins;
   PluginProgressWidget * _pluginProgressWidget;
   QMenu * _pluginsMenu;


   QMenu *_recentFilesMenu;
   QMenu *_shadingMenu;
   QMenu *_toolBarsMenu;
   QActionGroup *_shadingActions;
   ActionsToolBar *_actionsToolBar;
   QList<QToolBar*> _toolBars;
   QApplication *_application;
   QString _fileName;
   std::vector<QAction*> _convertActions;
};

#endif // _MAINWINDOW_
