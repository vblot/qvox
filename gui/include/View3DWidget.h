/** -*- mode: c++ -*-
 * @file   View3DWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:20:41 2005
 *
 * @brief  3D surface view Widget class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _VIEW3DWIDGET_H_
#define _VIEW3DWIDGET_H_

#include <QImage>
#include <QTime>
#include <iostream>
#include <ctime>
#include "Surface.h"
#include "Contour.h"
#include "Document.h"
#include "ColorMap.h"
#include "Quaternion.h"

class AnimationToolBar;
class EdgelList;
class QPainter;
class QApplication;

enum Action {
   ActionNone,
   ActionMove,
   ActionInfo,
   ActionDrawVoxel,
   ActionDrawSurfel,
   ActionDrawSimpleVoxel,
   ActionRemoveVoxel,
   ActionRemoveSurfel,
   ActionRemoveSimpleVoxel,
   ActionCutByPlane,
   ActionMoveLight
};

class View3DWidget : public QWidget {
   Q_OBJECT

public:

   View3DWidget( QWidget * parent );

   ~View3DWidget();

   void setApplication( QApplication * application );

   void drawEdgelList( EdgelList & );

   Surface & surface() { return _surface; }

   ColorMap & colorMap() { return _colorMap; }

   int surfaceShading() { return _surface.shading(); }
   Action action() { return _action; }

   void document( Document * );
   Document * document() { return _document; }

   void setAnimationToolBar( AnimationToolBar *toolBar ) {
      _animationToolBar = toolBar;
   }

   void recordRotation();
   void clear();
   bool animate() { return _animate; }
   void animate( bool status ) { _animate = status; }
   int drawRadius() { return _drawRadius; }
   void colorizeBorder();

   bool animateAspect() { return _animateAspect; }
   void animateAspect( bool status );

   void aspect( float x, float y, float z);

   float framesPerSec() { return _framesPerSec; }

   const QImage &  axisImage();

   Surfel* getCloserSurfel( int x, int y );

   void logo();

   void highlightSlice( bool onOff );

   void virtualPointer( QProcess * process );

   QProcess *  virtualPointer();

   /**
   *
   *
   * @param fileName filename
   */
   void captureFileName( const QString & fileName );
   const QString &  captureFileName() { return _captureFileName; }

   void view2DImage( const QImage * image );



protected:


   void paintEvent( QPaintEvent * );
   void showEvent( QShowEvent * );
   void hideEvent( QHideEvent * );
   void keyPressEvent ( QKeyEvent * );
   void resizeEvent( QResizeEvent * );
   void mousePressEvent ( QMouseEvent * );
   void mouseReleaseEvent ( QMouseEvent * );
   void mouseMoveEvent ( QMouseEvent * );
   void wheelEvent ( QWheelEvent * );
   void drawAxis( QImage & );
   void contour();
   

public slots:

   void action( int a );
   void zoomOut();
   void zoomIn();
   void zoomOne();
   void zoomFit();
   void changeBackground();
   void setShading( int );
   void reverseColorMap();
   void edges( bool );
   void writeAnimation( bool onOff );
   void writeView( const char * fileName = 0, const char * format = 0 );
   void volumeChanged();
   void fullVolume( bool on );
   void drawRadius(int );
   void drawAscii();
   void highlightSlice( int slice, int axis );
   void showColorMap(bool);
   void forceRedraw(bool fine=false);
   void virtualPointerMotion();
   void checkFPS();

   /**
     * Enables/disable the raw capture.
     *
     * @param bool
     */
   void capture( bool );

signals:

   void info( const QString & );
   void clearInfo();

private:

   void refresh(bool rebuildImage = true);
   void refreshFine(bool rebuildImage = true);

   Document * _document;
   Surface _surface;
   Contour _contour;

   QColor _backgroundColor;
   bool _leftButton, _middleButton, _rightButton;
   int _oldX, _oldY;
   Quaternion _currentRot;
   Quaternion _animateRotationQ;

   bool _mouseHasMoved;
   bool _firstPaint;
   int _drawRadius;

   bool _highlightSlice;
   int _highlightedSliceAxis, _highlightedSlice;

   bool _capture;
   std::ofstream _captureFile;
   QString _captureFileName;
   size_t _captureSize;

   QApplication *_application;
   float _animRho, _animTheta, _animPhi;
   bool _animate;
   Action _action;
   bool _writeAnimation;
   AnimationToolBar * _animationToolBar;
   Matrix _recordedRotation;
   bool _animateAspect;
   QImage _image;
   QImage _axisImage;
   bool _needToRedraw;

   bool _rotationBox;

   const QImage * _view2DImage;

   ColorMap _colorMap;
   float _framesPerSec;
   bool _shown;
   bool _logo;

   bool _fineRefreshAsked;
   bool _needToRebuild;

   QTime _time;

   QProcess * _virtualPointer;

};

#endif
