/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   DialogExperiments.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 * 
 * @brief  Declaration of the class DialogExperiments
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIALOGEXPERIMENTS_H_
#define _DIALOGEXPERIMENTS_H_

#include "ui_dialogexperiments.h"

class EdgelList;
#include "Contour.h"
class View3DWidget;
#include "Evolver.h"
#include "Surface.h"

/**
 *  The DialogExperiments class.
 */
class DialogExperiments : public QDialog, public Ui::DialogExperiments {
  Q_OBJECT
 public:
  /**
   * Constructor with no arguments.
   */
  DialogExperiments(QWidget *parent);

  static EdgelList * snake;

   void view3DWidget( View3DWidget * w ) { _view3DWidget = w; }
   void breadthVisit( Surfel *surfel,
		      int size,
		      std::vector<Surfel*> & surfels );

 protected:

 public slots:

   void cbTorusOptimalCurvature();
   void cbSphereOptimalCurvature();
   void cbSphereCurvature();
   void cbSphereNormals();
   void cbSphereOptimal();
   void cbPlanes();
   void cbHyperbolicParaboloids();
   void cbParaboloids();
   void cbTorusNormals();
   void cbTorusCurvature();
   void cbTorusOptimal();
   void cbTorusExactNormals();
   void cbCheck();
   void cbMarkSurfel( bool on );
   void extractBitPlane();
   void resetEvolve();
   void evolve3D();
   void response();
   void initResponse();
   void breadthVisiting();
   void extract();
   void showGraph( bool on );
   void shapeFromShading();
   void clearSurfelsColors();
   void surfaceThinning();
   void prune();   
   void fill();
   void computeIterations();
   void cbConvexHull();

 signals:
   void askFullVolume();
   void sphere( int );
   void paraboloid( int );

 private:
   Z3toR3 _borderMap;
   QString _maskFileName;
   Contour _contour;
   View3DWidget *_view3DWidget;
#ifdef HAVE_QWT
   QDialog *_plotDialog;
   QwtPlot *_plotC, *_plotR;
   long _curve1, _curve2, _curve3, _curve4;
   QCheckBox *_cbShowGraph;
#endif
};

#endif // _DIALOGEXPERIMENTS_H_
