/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogAspect.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class DialogAspect
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _DIALOGASPECT_H_
#define _DIALOGASPECT_H_

#include "ui_dialogaspect.h"
class View3DWidget;
class FloatValidator;

/**
 *  The DialogAspect class.
 */
class DialogAspect : public QDialog, public Ui::DialogAspect {
  Q_OBJECT
 public:
  /**
   * Constructor with no arguments.
   */
  DialogAspect(QWidget *parent);
  ~DialogAspect();

  bool enabled();
  void getAspect( float & x, float & y, float & z);
  void setView3DWidget( View3DWidget * );
  void commitAspect();

 public slots:

  void toggleAspect( bool on );
  void slXMoved( int pos );
  void slYMoved( int pos );
  void slZMoved( int pos );
  void defaultValues();

  void leAspectXChanged();
  void leAspectYChanged();
  void leAspectZChanged();

 protected:

  float _aspectX, _aspectY, _aspectZ;
  View3DWidget *_view3DWidget;
  FloatValidator *_validator;
 private:

};

#endif // _DIALOGASPECT_H_

