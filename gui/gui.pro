TEMPLATE	= app
LANGUAGE	= C++
CONFIG	+= qt static
QT += widgets xml
include("../global_options.qprefs")

unix { TARGET = qvox }

INCLUDEPATH += .. ui include ../tools/include ../zzztools/include ../surface/include ./plugins
DEPENDPATH += ../libs ./include ./plugins

include("plugins/plugins.pro")

HEADERS	+= \
	include/QTools.h \
	include/ActionsToolBar.h \
	include/ToolsToolBar.h \
	include/AnimationToolBar.h \
	include/ColorMap.h \
	include/ColorMapToolBar.h \
	include/ColorMapView.h \
        include/FileFieldWidget.h \
	include/Dialog2DView.h \
	include/DialogAbout.h \
	include/DialogAspect.h \
	include/DialogInputSize.h \
	include/DialogMerge.h \
	include/DialogSettings.h \
	include/DialogSkel.h \
	include/DialogThresholds.h \
	include/DialogExperiments.h \
	include/Dialogs.h \
	include/Document.h \
	include/PluginDialog.h \
	include/PluginDialogIn.h \
	include/PluginDialogOut.h \
	include/PluginDialogInOut.h \
        include/PluginProgressWidget.h \
        include/PluginConsole.h \
	include/FloatValidator.h \
	include/HistogramView.h \
	include/HistogramWidget.h \
	include/MainWindow.h \
	include/OfflineRenderer.h \
	include/Progress.h \
	include/QVoxVolume.h \
	include/Settings.h \
        include/ByteInputQIO.h \
        include/ByteOutputQIO.h \
	include/View2DWidget.h \
	include/ViewParamsToolBar.h \
	include/VolumeInfoWidget.h \
        include/View3DWidget.h

SOURCES	+= \
	src/QTools.cpp \
	src/ActionsToolBar.cpp \
	src/AnimationToolBar.cpp \
	src/ToolsToolBar.cpp \
	src/ColorMap.cpp \
	src/ColorMapToolBar.cpp \
	src/ColorMapView.cpp \
        src/FileFieldWidget.cpp \
	src/Dialog2DView.cpp \
	src/DialogAbout.cpp \
	src/DialogAspect.cpp \
	src/DialogMerge.cpp \
	src/DialogSettings.cpp \
	src/DialogSkel.cpp \
	src/DialogThresholds.cpp \
	src/DialogExperiments.cpp \
	src/Dialogs.cpp \
	src/Document.cpp \
	src/PluginDialog.cpp \
	src/PluginDialogIn.cpp \
	src/PluginDialogOut.cpp \
	src/PluginDialogInOut.cpp \
        src/PluginProgressWidget.cpp \
        src/PluginConsole.cpp \
	src/FloatValidator.cpp \
	src/HistogramView.cpp \
	src/HistogramWidget.cpp \
	src/OfflineRenderer.cpp \
	src/Progress.cpp \
	src/QVoxVolume.cpp \
	src/Settings.cpp \
        src/ByteInputQIO.cpp \
        src/ByteOutputQIO.cpp \
	src/View2DWidget.cpp \
	src/View3DWidget.cpp \
	src/ViewParamsToolBar.cpp \
	src/VolumeInfoWidget.cpp \
        src/DialogInputSize.cpp \
        src/MainWindow.cpp \
        src/QVox.cpp

HEADERS += plugins/DemoPlugin.h
SOURCES += plugins/DemoPlugin.cpp

FORMS	= \
          ui/mainwindow.ui \
          ui/dialog2dview.ui \
          ui/dialogabout.ui \
          ui/dialogaspect.ui \
          ui/dialoginputsize.ui \
          ui/dialogmerge.ui \
          ui/dialogsettings.ui \
          ui/dialogexperiments.ui \
          ui/dialogskel.ui

RESOURCES = ui/pixmaps.qrc ui/licence.qrc

macx { DEFINES += _NO_FORK_ }

release { DEFINES += _RELEASE_ }

#unix {
 LIBS += ../libs/libsurface.a ../libs/libboard.a ../libs/libzzztools.a ../libs/libtools.a 
 PRE_TARGETDEPS += ../libs/libsurface.a ../libs/libboard.a ../libs/libzzztools.a ../libs/libtools.a
 QMAKE_LFLAGS_DEBUG =
 #debug { LIBS += -lqwt }
 debug { LIBS +=  }
 UI_DIR = .ui
 MOC_DIR = .moc
 RCC_DIR = .qrc
 OBJECTS_DIR = .obj
#
# "-pedantic" is too much for Qt with linux-g++ ...
# ... but "-Wno-long-long" makes it fine! 
#
 QMAKE_CXXFLAGS_RELEASE +=  -ffast-math -O3 -Wall -W -Wno-long-long -pedantic
 QMAKE_CXXFLAGS_DEBUG += -ffast-math -Wall -W -Wextra -Wno-long-long -pedantic
 DEFINES += _IS_UNIX_
#}

win32 {
 TEMPLATE = app
 LIBS += ../libs/libsurface.a ../libs/libboard.a  ../libs/libzzztools.a ../libs/libtools.a
 UI_DIR = ui_files
 INCLUDEPATH	+= ui_files 
 MOC_DIR = moc_files
 OBJECTS_DIR = obj_files
}

win32-g++ {
 QMAKE_CXXFLAGS_RELEASE += -ffast-math -W -Wall -Wno-long-long -pedantic
 QMAKE_CXXFLAGS_DEBUG += -W -Wall -Wno-long-long -pedantic
}
