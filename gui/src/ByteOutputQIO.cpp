/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ByteOutputQIO.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class ByteOutputQIO
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ByteOutputQIO.h"
#include <cstdio>
#include <QIODevice>

/**
 * Constructor
 */

ByteOutputQIO::ByteOutputQIO( QIODevice & io )
   : _io( io ), _ok( io.isOpen() && io.isWritable() ) {
   
}



bool
ByteOutputQIO::put( char c )
{
   return ( _ok = _io.putChar( c ) );
}

bool
ByteOutputQIO::puts( const char *s )
{
   return write( s, strlen( s ) );
}

bool
ByteOutputQIO::write( const char *s, size_t n )
{
   qint64 count = 0;
   qint64 nb = 0;
   qint64 limit = static_cast<qint64>( n );
   while ( ( count < limit )
	   && ( ( nb = _io.write( s, limit - count ) ) > 0 ) ) {
      s += nb;
      count += nb;
   }
   if ( nb == -1 ) perror( "Error: ByteOutputQt::write()" );
   return _ok = ( count == limit );
}

void ByteOutputQIO::flush()
{
   
}

ByteOutputQIO::operator bool() const
{
   return _ok && _io.isOpen();
}
