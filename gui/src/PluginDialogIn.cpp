/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginDialogIn.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginDialogIn
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "PluginDialogIn.h"

#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QPushButton>
#include <QMessageBox>
#include <QCheckBox>
#include <QStringList>

#include "Document.h"
#include "ByteOutputQIO.h"
#include "PluginConsole.h"

#include <vector>
using std::vector;

/**
 * Constructor
 */
PluginDialogIn::PluginDialogIn( QWidget * parent, 
				const QDomElement & xmlPlugin,
				Document & document,
				const QString & xmlPath,
				const QString & binPath,
				const QString & libPath,
				const QString & workingDir )
   : PluginDialog( parent, xmlPlugin, document,
		   xmlPath, binPath, libPath, workingDir ) {

   _topLabel->setText( QString("<h3>&gt;&gt; %2 </h3>").arg( _name ) );
}

PluginDialog::Type
PluginDialogIn::type() const
{
   return Input;
}

void
PluginDialogIn::terminate()
{

}

QProcess::ProcessState
PluginDialogIn::state() const
{
   return QProcess::NotRunning;
}

void
PluginDialogIn::launch()
{
   _command = command();
   if ( _command.contains( " $i" ) ) {
      QString extension = inputExtension();
      QTemporaryFile tmpFile;
      QString pluginInputFileName;
      tmpFile.open();
      pluginInputFileName = tmpFile.fileName() + "." + extension;
      
      _document->image()->save( pluginInputFileName.toLatin1().constData() );
      TRACE << "PLUGIN Wrote file: " 
	    << pluginInputFileName.toLatin1().constData() << std::endl;      
      _command.replace( " $i", QChar(' ') + pluginInputFileName );
      TRACE << "Execute: [" 
	    << _command.toLatin1().constData() << "]\n";
      QProcess * pluginProcess = new QProcess( this );
      setProcessEnv( pluginProcess );
      _tmpFileNames.push_back( pluginInputFileName );
      _processes.push_back( pluginProcess );
      connect( pluginProcess, SIGNAL( finished( int, QProcess::ExitStatus ) ),
	       this, SLOT( processFinished(int, QProcess::ExitStatus ) ) );
      connect( pluginProcess, SIGNAL( error( QProcess::ProcessError ) ),
	       this, SLOT( processError( QProcess::ProcessError ) ) );
      if ( _cbShowConsole->isChecked() ) {
	 PluginConsole * console 
	    = new PluginConsole( dynamic_cast<QWidget*>( parent() ) );
	 console->command( _command );
	 console->process( pluginProcess, true, true );
	 console->show();
      }
      if ( _workingDir.size() )
	 pluginProcess->setWorkingDirectory( _workingDir );
      pluginProcess->start( "sh", QStringList() << "-c" <<  _command,
			    QIODevice::ReadOnly );
   } else  {
      QString extension = inputExtension();      
      TRACE << "Execute: [" << _command.toLatin1().constData() << "]\n";
      QProcess *pluginProcess = new QProcess( this );
      setProcessEnv( pluginProcess );
      connect( pluginProcess, SIGNAL( finished( int, QProcess::ExitStatus ) ),
	       this, SLOT( processFinished(int, QProcess::ExitStatus ) ) );
      connect( pluginProcess, SIGNAL( error( QProcess::ProcessError ) ),
	       this, SLOT( processError( QProcess::ProcessError ) ) );
      if ( _cbShowConsole->isChecked() ) {
	 PluginConsole * console 
	    = new PluginConsole( dynamic_cast<QWidget*>( parent() ) );
	 console->command( _command );
	 console->process( pluginProcess, true, true );
	 console->show();
      }
      if ( _workingDir.size() )
	 pluginProcess->setWorkingDirectory( _workingDir );
      pluginProcess->start( "sh", QStringList() << "-c" <<  _command,
			    QIODevice::ReadWrite );
      ByteOutputQIO out( *pluginProcess );
      _document->image()->save( out, extension.toLatin1().constData() ); 
   }

   if ( paramsCount() )
      _statusBar->showMessage( "DataSink command launched.", 4000 );
   _document->message( "SataSink command launched.", 4000 );
}

void
PluginDialogIn::processFinished( int exitCode, QProcess::ExitStatus  )
{
   TRACE << "PLUGIN DONE ExitCode:" << exitCode << std::endl;
   
   if ( exitCode ) 
      _statusBar->showMessage( QString("Plugin error (%1).").arg(exitCode), 3000 );
   else
      _statusBar->showMessage("Plugin done.", 3000 );

   _document->message( "A plugin has ended.", 3000 );

   vector<QProcess*>::iterator ip = _processes.begin();
   vector<QProcess*>::iterator end = _processes.end();
   while ( ip != end ) {
      if (  (*ip)->state() == QProcess::NotRunning ) {
	 vector<QProcess*>::size_type i = ip - _processes.begin();
	 TRACE << "PLUGIN Removing : " 
	       << _tmpFileNames[ i ].toLatin1().constData() << "\n";
	 QFile file( _tmpFileNames[ i ] );
	 if ( file.exists() )
	    file.remove();
	 delete *ip;
	 _processes.erase( ip );
	 _tmpFileNames.erase( _tmpFileNames.begin() + i );
	 return;
      }
      ++ip;
   }
}

void
PluginDialogIn::processError( QProcess::ProcessError e )
{
   TRACE << "PLUGIN Process failed:" << e << std::endl;
   ERROR << "Could not execute external command.\n";
   ERROR << "   command> " << _command.toLatin1().constData() << std::endl;
   QMessageBox::critical( this, 
			  QString( "Error" ), 
			  QString( "A command could not be started or crashed."
				   "\nThe command used was:\n" ) + _command
			  );

   vector<QProcess*>::iterator ip = _processes.begin();
   vector<QProcess*>::iterator end = _processes.end();
   while ( ip != end ) {
      if (  (*ip)->state() == QProcess::NotRunning ) {
	 vector<QProcess*>::size_type i = ip - _processes.begin();
	 TRACE << "PLUGIN Removing : " 
	       << _tmpFileNames[ i ].toLatin1().constData() << "\n";
	 QFile file( _tmpFileNames[ i ] );
	 if ( file.exists() )
	    file.remove();
	 delete *ip;
	 _processes.erase( ip );
	 _tmpFileNames.erase( _tmpFileNames.begin() + i );
	 return;
      }
      ++ip;
   }   
}

int
PluginDialogIn::execute()
{
   if ( paramsCount() )
      show();
   else 
      launch();
   return 1;
}

void
PluginDialogIn::okClicked()
{
   launch();
   done( 0 );
}

void
PluginDialogIn::applyClicked()
{
   launch();
}

void
PluginDialogIn::closeClicked()
{
   done( 0 );
}
