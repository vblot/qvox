/**
 * @file   VolumeInfoWidget.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:36:50 2006
 * 
 * \@copyright  
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "VolumeInfoWidget.h"
#include "Surface.h"

VolumeInfoWidget::VolumeInfoWidget( QWidget * parent )
  :QLabel( parent ) {
  CONS( VolumeInfoWidget );
  
  setMinimumWidth( 150 );
  setMargin( 1 );
  setSizePolicy( QSizePolicy::Maximum, QSizePolicy::MinimumExpanding );
  setAlignment( Qt::AlignLeft | Qt::AlignVCenter );
  setText( QString(" Volume informations " ) );
  _document = 0;
  _surface = 0;
  _volumeSizeString = QString("%6 (X:%1,Y:%2,Z:%3,B:%4) %5 surfels");
  _surfels = 0;
}

VolumeInfoWidget::~VolumeInfoWidget()
{

}

void VolumeInfoWidget::surfels( Size n )
{
  _surfels = n;
  update();
}

void VolumeInfoWidget::message(const QString & message)
{
   setText(message);
}

void VolumeInfoWidget::clear()
{
   setText("(Processing...)");
}

void VolumeInfoWidget::document( Document * document )
{
   if ( _document ) disconnect( _document, 0, this, 0 );
   _document = document;
   if ( _document ) {
      connect( _document, SIGNAL( volumeChanged() ),
               this, SLOT( updateDocument() ) );
      connect( _document, SIGNAL(invalidated()),
               this, SLOT(clear()) );
   }
   updateDocument();
}

void VolumeInfoWidget::surface( Surface * surface )
{
   if ( _surface ) disconnect( _surface, 0, this, 0 );
   _surface = surface;
   if ( _surface )
      QObject::connect( _surface, SIGNAL( changed() ),
                        this, SLOT( updateSurface() ) );
   updateSurface();
}


void VolumeInfoWidget::updateDocument()
{
   if ( _document ) {
      _bands = _document->image()->bands();
    _width = _document->image()->width();
    _height = _document->image()->height();
    _depth = _document->image()->depth();
    QString valueTypeStr( POShortValueTypeName[ _document->valueType() ] );
    setText( _volumeSizeString
	     .arg(_width).arg(_height).arg(_depth).arg(_bands)
	     .arg( _surfels ).arg( valueTypeStr ) );
  } else
    setText( "Document unavailable..." );
}

void VolumeInfoWidget::updateSurface()
{
  _surfels = _surface->surfelList().size();
  QString valueTypeStr( POShortValueTypeName[ _document->valueType() ] );
  setText( _volumeSizeString.arg(_width).arg(_height).arg(_depth)
	   .arg(_bands)
	   .arg( _surfels ).arg( valueTypeStr ) );
}


