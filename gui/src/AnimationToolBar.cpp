/**
 * @file   AnimationToolBar.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:45:29 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "AnimationToolBar.h"

#include <QFileDialog>
#include <QImage>
#include <QLayout>
#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>
#include <QImageWriter>
#include <QCheckBox>

#include "View3DWidget.h"
#include "Dialogs.h"
#include "tools.h"

AnimationToolBar::AnimationToolBar( QMainWindow * mainWindow ):
  QToolBar( QString("Animation"), mainWindow )
{
  CONS( AnimationToolBar  );
  QLabel * label;
  setWindowTitle("Animation");
  
  addWidget( new QLabel("Animation") );
  addWidget( label = new QLabel(" Filename:") );
  label->setSizePolicy( QSizePolicy::Fixed,
   			QSizePolicy::Preferred );

  addWidget( _lineEdit = new QLineEdit );
  _lineEdit->setMaxLength( 40 );
			    
  addWidget( _pbChoosePath = new QPushButton( QString("Path...") ) );
  connect( _pbChoosePath, SIGNAL( clicked() ),
	   this, SLOT( choosePath() ) );

  addWidget( new QLabel("Format:") );
  addWidget( _cbFormats = new QComboBox() );
  
  QList<QByteArray> formats = QImageWriter::supportedImageFormats();
  QList<QByteArray>::const_iterator it = formats.begin(), end = formats.end();
  while ( it != end ) 
    _cbFormats->addItem( *(it++) );
  
  _cbFormats->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Preferred );
  _format = _cbFormats->currentText();
  
  QWidget * w= new QWidget;
  addWidget( w );
  w->setMinimumWidth( 5 );
  w->setSizePolicy( QSizePolicy::Fixed,
		    QSizePolicy::Preferred );
  
  addWidget( _checkInteractive = new QCheckBox( QString("Interactive") ) );
  _checkInteractive->setSizePolicy( QSizePolicy::Fixed,
				    QSizePolicy::Preferred );

  addWidget( _pbReset = new QPushButton( QString("Reset") ) );

  _pbReset->setSizePolicy( QSizePolicy::Fixed,
				    QSizePolicy::Preferred );
  addWidget( _pbWriteImage = new QPushButton( QString("Save") ) );
  _pbWriteImage->setSizePolicy( QSizePolicy::Fixed,
				QSizePolicy::Preferred );

  
  addWidget( _pbCapture = new QPushButton( QString("Record") ) );
  _pbCapture->setCheckable( true );

  addWidget( _pbCaptureFile = new QPushButton( QString("...") ) );
  
  connect( _pbWriteImage, SIGNAL( clicked() ),
	   this, SLOT( saveImage() ) );
  
  connect( _pbReset, SIGNAL( clicked() ),
	   this, SLOT( reset() ) );

  connect( _checkInteractive, SIGNAL( clicked() ),
	   this, SLOT( animate() ) );
	   
  connect( _cbFormats, SIGNAL( activated(const QString&) ),
	   this, SLOT( changeFormat(const QString&) ) );

  _view3DWidget = 0;

  connect( _pbCaptureFile, SIGNAL( clicked() ),
	   this, SLOT( captureFileSelect() ) ); 

  reset();
  _path = QString::null;

  addWidget( w = new QWidget );
  w->setMinimumWidth( 5 );
  w->setSizePolicy( QSizePolicy::MinimumExpanding, 
		    QSizePolicy::MinimumExpanding );
  // w->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
}

void
AnimationToolBar::setView3DWidget( View3DWidget * w )
{
  if ( _view3DWidget ) 
    disconnect( _pbCapture, SIGNAL( toggled(bool) ),
	     	_view3DWidget, SLOT( capture(bool) ) );
  _view3DWidget = w;
  if ( _view3DWidget ) 
    connect( _pbCapture, SIGNAL( toggled(bool) ),
	     _view3DWidget, SLOT( capture(bool) ) ); 
}



void
AnimationToolBar::reset()
{
  _lineEdit->setText( "anim%N." + _cbFormats->currentText().toLower() ); 
  _imageNumber = 0;
  _pbWriteImage->setText("Save 000");
}

void
AnimationToolBar::setOrientation ( Qt::Orientation )
{

}

void
AnimationToolBar::undock()
{

}

void
AnimationToolBar::choosePath()
{
  _path = QFileDialog::getExistingDirectory( this, 
					     "Select target directory",
					     _path );
}

void
AnimationToolBar::animate()
{
  _view3DWidget->writeAnimation( _checkInteractive->isChecked() );
}
void
AnimationToolBar::endInteractive()
{
  _checkInteractive->setChecked( false );
  _view3DWidget->writeAnimation( false );
}

void
AnimationToolBar::changeFormat( const QString & format )
{
  _format = format;
  char fileName[1024];
  QString str = _lineEdit->text();
  strcpy( fileName, str.toLatin1() );
  char * pc = lastDotOrEnd( fileName );
  *(pc++) = '.';
  str = format.toLower();
  strcpy( pc, str.toLatin1() );
  _lineEdit->setText( QString( fileName ) );
}

QString
AnimationToolBar::nextFileName()
{
  char number[10];
  sprintf( number, "%03d", _imageNumber++ );
  QString result = _lineEdit->text();
  result.replace( QString( "%N" ), QString( number ), Qt::CaseSensitive );
  result.replace( QString( "%n" ), QString("%1").arg( _imageNumber - 1 ), Qt::CaseSensitive  );
  sprintf( number, "%03d", _imageNumber );
  _pbWriteImage->setText( QString("Save ") + number );
  return result;
}

void
AnimationToolBar::saveImage()
{
  QString fileName =  _path + nextFileName();
  _view3DWidget->writeView( fileName.toLatin1()  , _format.toLatin1() );
}

void
AnimationToolBar::captureFileSelect( )
{
  Dialogs dialogs(this);
  QString name = dialogs.askSaveFileName( QImagesSequence, this );
  if ( ! name.isNull() ) {
    _view3DWidget->captureFileName( name );
  }    
}



