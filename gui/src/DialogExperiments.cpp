/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   DialogExperiments.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2007
 *
 * @brief  Definition of the class DialogExperiments
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <iostream>
#include <fstream>
#include <algorithm>
#include <QMessageBox>
#include "DialogExperiments.h"
#include "EdgelList.h"
#include "View3DWidget.h"
#include "SurfaceThinning.h"
#include "Convolution.h"
#include "Dialogs.h"
#include "zzzimage.h"
#include "stats.h"

#include <fstream>
#include <cstdio>
#include <map>

using namespace std;

/**
 * Constructor
 */
DialogExperiments::DialogExperiments(QWidget *parent)
  :QDialog( parent )
{
  CONS( DialogExperiments );
  setupUi( this );

#ifdef HAVE_QWT
  _plotDialog = new QDialog( dynamic_cast<QMainWindow*>( parent() ) );
  _plotDialog->setGeometry( 10,10, 640, 480 );
  new QVBoxLayout( _plotDialog  );
  _plotDialog->layout()->setAutoAdd( true );

  new QLabel( QString("Curvatures and Averaged curvatures"), _plotDialog );
  _plotC = new QwtPlot("Curvatures", _plotDialog , "curvatureplot" );
  _plotR = new QwtPlot("Radius", _plotDialog , "radiusplot" );

  _curve1 = _plotC->insertCurve("Curvatures");
  _curve2 = _plotC->insertCurve("Averaged curvatures");
  _curve3 = _plotR->insertCurve("Radius");
  _curve4 = _plotR->insertCurve("Averaged Radius");
  _plotC->setCurvePen( _curve2, QColor("red") );
  _plotR->setCurvePen( _curve4, QColor("red") );
  _cbShowGraph = new QCheckBox("Graph",this);
  _cbShowGraph->setChecked( false );
  connect( _cbShowGraph, SIGNAL( toggled(bool) ),
           this, SLOT( showGraph(bool) ) );
  vbox->addWidget(_cbShowGraph);
#endif

  QPushButton *pb = _pbEvolve3D;
  connect( pb, SIGNAL( clicked() ),
           this, SLOT( evolve3D() ) );

  pb = _pbRaz;
  connect( pb, SIGNAL( clicked() ),
           this, SLOT( resetEvolve() ) );

  pb = _pbXtract;
  connect( pb, SIGNAL( clicked() ),
           this, SLOT( extract() ) );
  connect( _pbResponse, SIGNAL( clicked() ),
           this, SLOT( response() ) );
  connect( _pbInitResponse, SIGNAL( clicked() ),
           this, SLOT( initResponse() ) );
  connect( _pbSphereCurvature, SIGNAL( clicked() ),
           this, SLOT( cbSphereCurvature() ) );
  connect( _pbSphereNormals, SIGNAL( clicked() ),
           this, SLOT( cbSphereNormals() ) );
  connect( _pbSphereOptimal, SIGNAL( clicked() ),
           this, SLOT( cbSphereOptimal() ) );
  connect( _pbPlane, SIGNAL( clicked() ),
           this, SLOT( cbPlanes() ) );
  connect( _pbTorusC, SIGNAL( clicked() ),
           this, SLOT( cbTorusCurvature() ) );
  connect( _pbTorusN, SIGNAL( clicked() ),
           this, SLOT( cbTorusNormals() ) );
  connect( _pbTorusOptimal, SIGNAL( clicked() ),
           this, SLOT( cbTorusOptimal() ) );

  connect( _pbTorusExactNormals, SIGNAL( clicked() ),
           this, SLOT( cbTorusExactNormals() ) );

  connect( _pbHypParab, SIGNAL( clicked() ),
           this, SLOT( cbHyperbolicParaboloids() ) );
  connect( _pbParab, SIGNAL( clicked() ),
           this, SLOT( cbParaboloids() ) );
  connect( _pbBreadth, SIGNAL( clicked() ),
           this, SLOT( breadthVisiting() ) );
  connect( _pbCheck, SIGNAL( clicked() ),
           this, SLOT( cbCheck() ) );
  connect( _pbComputeIterations, SIGNAL( clicked() ),
           this, SLOT( computeIterations() ) );
  connect( _cbMarkSurfel, SIGNAL( toggled(bool) ),
           this, SLOT( cbMarkSurfel(bool) ) );
  

  connect( _pbSphereOptimalC, SIGNAL( clicked() ),
           this, SLOT( cbSphereOptimalCurvature() ) );
  connect( _pbTorusOptimalC, SIGNAL( clicked() ),
           this, SLOT( cbTorusOptimalCurvature() ) );

  _leRho->setValidator( new QDoubleValidator( _leRho ) );
  _leRho->setText( "0.0" );
  _leTheta->setValidator( new QDoubleValidator( _leTheta ) );
  _leTheta->setText( "0.0" );
  _lePhi->setValidator( new QDoubleValidator( _lePhi ) );
  _lePhi->setText( "0.0" );

  connect( _pbFill, SIGNAL( clicked() ),
           this, SLOT( fill() ) );

  _sbStart->setRange( 1, 512 );
  _sbStart->setValue( 1 );
  _sbStop->setRange( 1, 1024 );
  _sbStop->setValue( 1 );
  _sbStep->setRange( 1, 255 );
  _sbStep->setValue( 4 );
  _leC1->setValidator( new QDoubleValidator( _leC1 ) );
  _leC1->setText( "1.0" );
  _leC2->setValidator( new QDoubleValidator( _leC2 ) );
  _leC2->setText( "1.0" );
  _leC3->setValidator( new QDoubleValidator( _leC3 ) );
  _leC3->setText( "1.0" );

  connect( _pbClear, SIGNAL( clicked() ),
           this, SLOT( clearSurfelsColors() ) );
  _cbCurveSkeleton->setChecked( true );
  _comboSurfelAdjacency->insertItem( 0, "(v,e)" );
  _comboSurfelAdjacency->insertItem( 1, "(e,v)" );
  _surfaceThinningIterations->setRange( 0, 255 );
  connect( _pbThin, SIGNAL( clicked() ),
           this, SLOT( surfaceThinning() ) );
  connect( _pbPrune, SIGNAL( clicked() ),
           this, SLOT( prune() ) );
  connect( _pbConvexHull, SIGNAL( clicked() ),
           this, SLOT( cbConvexHull() ) );
}

void
DialogExperiments::fill()
{
  _sbNoiseLevel->setValue( 0.20 );
  _sbStart->setValue( 55 );
  _sbStop->setValue( 56 );
  _sbStep->setValue( 4 );

  //   double rho = 2.0;
  //   double theta = 1.0;
  //   double phi = 0.4;
  double rho = 2.5;
  double theta = 0.4;
  double phi = 1.8;

  INFO << "rho=" << (180.0*rho/M_PI) << std::endl;
  INFO << "theta=" << (180.0*theta/M_PI) << std::endl;
  INFO << "phi=" << (180.0*phi/M_PI) << std::endl;

  _leRho->setText( QString("%1").arg(rho) );
  _leTheta->setText( QString("%1").arg(theta) );
  _lePhi->setText( QString("%1").arg(phi) );
}

void
DialogExperiments::clearSurfelsColors()
{
  _view3DWidget->surface().surfelList().setColors( 0 );
  _view3DWidget->forceRedraw();
}

void
DialogExperiments::surfaceThinning()
{
  if ( _comboSurfelAdjacency->currentIndex() == 0 )
    surfaceThinningV( _view3DWidget->surface().surfelList(),
                      _surfaceThinningIterations->value(),
                      _cbCurveSkeleton->isChecked() );
  else
    surfaceThinningE( _view3DWidget->surface().surfelList(),
                      _surfaceThinningIterations->value(),
                      _cbCurveSkeleton->isChecked() );
  _view3DWidget->forceRedraw();
}

void
DialogExperiments::prune()
{
  if ( _comboSurfelAdjacency->currentIndex() == 0 )
    pruneVbranches( _view3DWidget->surface().surfelList(), 20 );
  else
    pruneEbranches( _view3DWidget->surface().surfelList(), 20 );
  _view3DWidget->forceRedraw();
}

void
DialogExperiments::extract()
{
  Document * document = _view3DWidget->document();
  if ( document->image()->depth() != 1 ) return;
  document->notify();
}

void
DialogExperiments::resetEvolve()
{
  _borderMap.clear();
  _view3DWidget->surface().scalarArrayFromMarkedSurfels( _view3DWidget->surface().surfelDataArray(), 255 );
}

void
DialogExperiments::evolve3D()
{
  Document * document = _view3DWidget->document();
  document->valid( false );
  ZZZImage<UChar> *ucVolume;
  try {
    ucVolume = dynamic_cast< ZZZImage<UChar> * >( document->image() );
  } catch ( std::bad_cast ) {
    std::cerr << "Document::image() Not a ZZZImage<UChar>.\n";
    return;
  }

  Evolver evolver( &( _view3DWidget->surface() ), ucVolume );
  UInt32 iterations = _sbIterations->value();
  globalProgressReceiver.pushInterval( iterations, "Evolve..." );
  for ( UInt32 iteration = 0; iteration < iterations; ++iteration ) {
    if ( ! ( iteration % 2 ) ) _borderMap.clear();
    evolver.evolve( globalSettings->curvatureEstimateIterations(),
                    globalSettings->curvatureEstimateAveragingIterations(),
                    _borderMap );
    globalProgressReceiver.setProgress(iteration);
  }
  globalProgressReceiver.popInterval();
  document->notify();
  if ( _cbAutoWrite->isChecked() ) _view3DWidget->writeView();
}

void
DialogExperiments::extractBitPlane()
{
  // Document *document = _view3DWidget->document();
  // document->bitPlane( _sbPlane->value(), _sbBand->value() );
}

void
DialogExperiments::cbSphereCurvature( )
{
  Document * document = _view3DWidget->document();
  Int32 radius = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  int old_cei = globalSettings->curvatureEstimateIterations();
  int old_ceai = globalSettings->curvatureEstimateAveragingIterations();
  // int old_cect = globalSettings->curvatureType();

  map<int,int> opt_iter;
  {
    // get the optimal iteration number
    // from previous experiments
    std::ifstream f("optimal_normal_sphere.txt");
    char line[1024];
    int radius, iter;
    f.getline( line, 1024 );
    while ( ! f.eof() ) {
      f.getline( line, 1024 );
      if ( ! f.eof() ) {
        sscanf( line, "%d %d", &radius, &iter );
        opt_iter[radius] = iter;
      }
    }
  }

  globalSettings->curvatureType( MEAN_CURVATURE );
  globalSettings->curvatureEstimateAveragingIterations( 0 );
  _view3DWidget->setShading( Surface::ColorFromCurvature );

  std::cout << "# Radius;Curvature;IterationsN;IterationsC;Minimum;Maximum;Average;Std. Dev.\n";

  while ( radius <= stop ) {
    //double h = 1.0 / ( 2 * radius );
    //int n1 = static_cast<int>( floor( 0.25 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );
    //int n2 = static_cast<int>( floor( 0.25 * exp( - ( 14.0 / 9.0 ) * log( h ) ) ) );
    int n1 = opt_iter[ radius ];
    int n2 = opt_iter[ radius ];
    INFO << " n1=" << n1 << " n2=" << n2 << endl;

    //     globalSettings->normalsEstimateIterations( n1/4 );
    //     globalSettings->curvatureEstimateIterations( n2/4 );
    //     globalSettings->curvatureEstimateAveragingIterations( n2/2 );
    globalSettings->normalsEstimateIterations( n1 );
    globalSettings->curvatureEstimateIterations( n1 + 1 );
    globalSettings->curvatureEstimateAveragingIterations( n2 );

    document->create( radius*2+3, radius*2+3, radius*2+3, 1, Po_ValUC );
    document->sphere( document->image()->width() / 2,
                      document->image()->height() / 2,
                      document->image()->depth() / 2,
                      radius );
    _view3DWidget->zoomFit();
    
    double curvature = 1.0 / radius;
    // for ( int i = 1 ; i < 41 ; i+= 1 ) {
    // globalSettings->curvatureEstimateIterations( i );
    // _view3DWidget->setShading( -1 );
    {
      /*
       * Compute the min, max, average and std. dev. of the estimate.
       */
      SurfelList & surfelList = _view3DWidget->surface().surfelList();
      Surfel *ps = surfelList.begin();
      Surfel *end = surfelList.end();
      double average = 0.0;
      double stddev = 0.0;
      double minCurvature = 0.0;
      double maxCurvature = 0.0;
      vector<double> v;
      while ( ps != end ) {
        v.push_back( ps->meanCurvature );
        ++ps;
      }
      stats( v, minCurvature, maxCurvature, average, stddev );
      std::cout << radius << " "
                << curvature << " "
                << n1 << " "
                << n2 << " "
                << minCurvature << " "
                << maxCurvature << " "
                << average << " "
                << stddev << "\n";
    }
    // }  // Iteration on the number of convolutions
    radius += step;
  }
  globalSettings->curvatureEstimateIterations( old_cei );
  globalSettings->curvatureEstimateAveragingIterations( old_ceai );
  // globalSettings->curvatureType( old_cect );
}

void
DialogExperiments::cbSphereNormals( )
{
  Document * document = _view3DWidget->document();
  Int32 radius = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  int old_nei = globalSettings->normalsEstimateIterations();

  _view3DWidget->setShading( Surface::ColorFromLight );

  std::cout << "h;Radius;Iterations (n);Maximum;Average;Std. Dev.\n";
  while ( radius <= stop ) {
    double h = 1.0 / ( 2 * radius);
    int n = static_cast<int>( floor( 0.75 * 0.5 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );
    globalSettings->normalsEstimateIterations( n );
    document->create( radius*2+4, radius*2+4, radius*2+4, 1, Po_ValUC );
    document->sphere( document->image()->width() / 2,
                      document->image()->height() / 2,
                      document->image()->depth() / 2,
                      radius );
    
    _view3DWidget->zoomFit();

    {
      // Compute the min, max, average and std. dev. of the angular error.

      SurfelList &surfelList = _view3DWidget->surface().surfelList();
      double average = 0.0;
      double stddev = 0.0;
      double minError;
      double maxError;
      Triple<double> normal;
      double cosine;
      double error;
      vector<double> v;
      Surfel *ps = surfelList.begin();
      Surfel *end = surfelList.end();
      while ( ps != end ) {
        normal = ps->center;
        normal /= norm( normal );
        cosine = normal * ps->normal;
        if ( cosine > 1.0 ) cosine = 1.0;
        if ( cosine < -1.0 ) cosine = -1.0;
        error = acos( cosine );
        v.push_back(error);
        ++ps;
      }
      stats( v, minError, maxError, average, stddev );
      std::cout << h << " "
                << radius << " "
                << n << " "
                << rad2deg(minError) << " "
                << rad2deg(maxError) << " "
                << rad2deg(average) << " "
                << rad2deg(stddev) << "\n";
    }
    radius += step;
  }

  globalSettings->normalsEstimateIterations(old_nei);
}

void
DialogExperiments::cbSphereOptimal()
{
  Document * document = _view3DWidget->document();
  Surface & surface = _view3DWidget->surface();
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Int32 radius = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  int old_nei = globalSettings->normalsEstimateIterations();

  globalSettings->normalsEstimateIterations( 1 );
  _view3DWidget->setShading( Surface::ColorFromLight );

  std::cout << "Radius;Iterations;Average;Std. Dev.\n";
  while ( radius <= stop ) {
    document->create( radius*2+3, radius*2+3, radius*2+3, 1, Po_ValUC );
    document->sphere( document->image()->width() / 2,
                      document->image()->height() / 2,
                      document->image()->depth() / 2,
                      radius );
    if ( _cbNoisy->isChecked() )
      document->addNoise( _sbNoiseLevel->value() );
    
    _view3DWidget->zoomFit();

    SurfelDataArray sdaSrc( surfelList.size() );
    SurfelDataArray sdaDest( surfelList.size() );
    surface.centersArray( sdaSrc );
    Surfel *ps, *end = surfelList.end();
    Surfel *begin = surfelList.begin();

    Int32 nbIterations = ( radius <= 100 ) ? 100 : 150;
    int iterations = 0;
    vector<double> errors;
    vector<double> stddevs;
    do {
      // Convolution of the centers Once
      ++iterations;
      ps = surfelList.begin();
      while ( ps != end ) {
        convolution421( ps, surfelList, sdaSrc, sdaDest );
        ++ps;
      }
      sdaSrc =  sdaDest;

      // Compute estimated normal vectors
      ps = surfelList.begin();
      Triple<double> dxdu, dxdv;
      while ( ps != end ) {
        dxdu = sdaSrc[ ps->neighbors[0] - begin ] - sdaSrc[ ps - begin ];
        dxdv = sdaSrc[ ps->neighbors[1] - begin ] - sdaSrc[ ps - begin ];
        ps->normal = wedgeProduct( dxdu, dxdv );
        ps->normal /= norm( ps->normal );
        ++ps;
      }
      _view3DWidget->forceRedraw();
      qApp->processEvents();

      //
      // Compute the min, max, average and std. dev. of the angular error.
      //
      vector<double> v;
      double error = 0.0;
      Triple<double> normal;
      double cosine;
      ps = surfelList.begin();
      while ( ps != end ) {
        normal = ps->center;
        normal /= norm( normal );
        cosine = normal * ps->normal;
        if ( cosine > 1.0 ) cosine = 1.0;
        if ( cosine < -1.0 ) cosine = -1.0;
        error = acos( cosine );
        v.push_back( error );
        ++ps;
      }

      double stddev;
      double minError;
      double maxError;
      double average;
      stats( v, minError, maxError, average, stddev );
      errors.push_back( average );
      stddevs.push_back( stddev );
      if ( iterations == 51 ) {
        histo( v, 0, "histo.txt", 0 );
        ofstream f("dump.txt");
        sort( v.begin(), v.end() );
        vector<double>::iterator it = v.begin();
        while ( it != v.end() ) {
          f << rad2deg(*it) << std::endl;
          ++it;
        }
      }
    } while ( iterations < nbIterations  );

    vector<double>::const_iterator minerr = min_element( errors.begin(), errors.end() );
    vector<double>::const_iterator mindev = stddevs.begin() + ( minerr - errors.begin() );
    cout << radius
         << " " << (( minerr - errors.begin() ) + 1)
         << " " << rad2deg(*minerr)
         << " " << rad2deg(*mindev)
         << endl;

    radius += step;
  }
  globalSettings->normalsEstimateIterations(old_nei);
}

void
DialogExperiments::cbPlanes()
{
  Document * document = _view3DWidget->document();
  AbstractImage * image = document->image();
  Triple<SSize> center( image->width() / 2,
                        image->height() / 2,
                        image->depth() / 2 );
  int old_nei = globalSettings->normalsEstimateIterations();

  Triple<double> normal( _leC1->text().toDouble(),
                         _leC2->text().toDouble(),
                         _leC3->text().toDouble() );
  normal /= norm( normal );

  _view3DWidget->setShading( Surface::ColorFromLight );

  document->binarize( 0 );
  document->plane( center, normal, Rotation() );

  _view3DWidget->zoomFit();

  for ( int i = 16 ; i < 15  ; i+= 1 ) {
    globalSettings->normalsEstimateIterations( i );
    _view3DWidget->setShading( -1 );
    {
      /*
       * Compute the min, max, average and std. dev. of the angle error.
       */
      SurfelList & surfelList = _view3DWidget->surface().surfelList();
      double surfelCount = 0;
      Surfel *ps = surfelList.begin();
      Surfel *end = surfelList.end();
      double average = 0.0;
      double minError = std::numeric_limits<double>::max();
      double maxError = -std::numeric_limits<double>::max();
      double error = 0.0;
      while ( ps != end ) {
        if ( ! image->boundary( ps->voxel ) ) {
          ++surfelCount;
          double cosine = normal * ps->normal;
          if ( cosine > 1.0 ) cosine = 1.0;
          error = acos( cosine );
          average += error;
          if ( error > maxError ) maxError = error;
          if ( error < minError ) minError = error;
        }
        ++ps;
      }
      average /= surfelCount;
      double stddev = 0.0;
      ps = surfelList.begin();
      while ( ps != end ) {
        if ( ! image->boundary( ps->voxel ) ) {
          double cosine = normal * ps->normal;
          if ( cosine > 1.0 ) cosine = 1.0;
          error = acos( cosine );
          stddev += dsquare( error - average );
        }
        ++ps;
      }
      stddev = sqrt( stddev / surfelCount );
      std::cout << i << " "
                << rad2deg(minError) << " "
                << rad2deg(maxError) << " "
                << rad2deg(average) << " "
                << rad2deg(stddev) << "\n";
    }
  }
  globalSettings->normalsEstimateIterations(old_nei);
}

void
DialogExperiments::cbTorusCurvature()
{
  Document * document = _view3DWidget->document();
  Int32 start = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();

  int old_nei = globalSettings->normalsEstimateIterations();
  int old_cei = globalSettings->curvatureEstimateIterations();
  int old_ceai = globalSettings->curvatureEstimateAveragingIterations();
  int old_cect = globalSettings->curvatureType();
  
  globalSettings->curvatureType( GAUSSIAN_CURVATURE );
  _view3DWidget->setShading( Surface::ColorFromCurvature );

  for ( int width = start; width <= stop ; width += step ) {
    int largeR = width;
    int smallR = width / 2;

    SHOW( smallR );
    SHOW( largeR );

    double h = 1.0 / ( 2 * largeR + 1 );
    int n1 = static_cast<int>( floor( 0.25 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );
    int n2 = static_cast<int>( floor( 0.25 * exp( - ( 14.0 / 9.0 ) * log( h ) ) ) );
    
    std::cout << "n1=" << n1 << " n2=" << n2 << endl;
    //     globalSettings->normalsEstimateIterations( n1/4 );
    //     globalSettings->curvatureEstimateIterations( n2/4 );
    //     globalSettings->curvatureEstimateAveragingIterations( n2/2 );
    globalSettings->normalsEstimateIterations( smallR );
    globalSettings->curvatureEstimateIterations( smallR + 1 );
    globalSettings->curvatureEstimateAveragingIterations( smallR );
    document->create( width * 4 + 1, width * 4 + 1, width * 4 + 1, 1, Po_ValUC );

    Triple<SSize> center( document->image()->width()/2,
                          document->image()->height()/2,
                          document->image()->depth()/2 );

    // Triple<double> normal = Rotation( Triple<double>(0,0,0),
    // 				      _leRho->text().toDouble(),
    // 				      _leTheta->text().toDouble(),
    // 				      _lePhi->text().toDouble() ).apply( Triple<double>( 0, 0, 1 ) );

    document->torus( center,
                     largeR, smallR,
                     Rotation( center,
                               _leRho->text().toDouble(),
                               _leTheta->text().toDouble(),
                               _lePhi->text().toDouble() ),
                     Triple<double>( 1.0, 1.0, 1.0) );
    
    {
      // Compute the true curvatures
      Triple<SSize> center( document->image()->width() / 2,
                            document->image()->height() / 2,
                            document->image()->depth() / 2 );
      Triple<double> xAxis = Rotation( Triple<double>(0,0,0),
                                       _leRho->text().toDouble(),
                                       _leTheta->text().toDouble(),
                                       _lePhi->text().toDouble() ).apply( Triple<double>( 1, 0, 0 ) );
      Triple<double> yAxis = Rotation( Triple<double>(0,0,0),
                                       _leRho->text().toDouble(),
                                       _leTheta->text().toDouble(),
                                       _lePhi->text().toDouble() ).apply( Triple<double>( 0, 1, 0 ) );
      Triple<double> zAxis = Rotation( Triple<double>(0,0,0),
                                       _leRho->text().toDouble(),
                                       _leTheta->text().toDouble(),
                                       _lePhi->text().toDouble() ).apply( Triple<double>( 0, 0, 1 ) );
      multimap<double,double> values;

      Surfel *begin = _view3DWidget->surface().surfelList().begin();
      Surfel *end = _view3DWidget->surface().surfelList().end();
      Surfel *ps = begin;
      Triple<double> point;
      Triple<double> c,a,d;
      double v, k, s;
      // double h;
      double errorSum = 0.0;
      while ( ps != end ) {
        point = ps->center;
        point.set( point * xAxis, point * yAxis, point * zAxis );
        c = point;
        c.third = 0;
        c /= norm ( c );
        a = c * (double)width; // Large radius
        d = point - a;
        d /= norm( d );
        s = d*c;
        if ( s > 1.0 ) s = 1.0;
        if ( s < -1.0 ) s = -1.0;
        v = acos( s );
        k = cos( v ) / ( smallR * ( largeR + smallR * cos( v ) ) );
        // ps->gaussianCurvature = k;
        // h = - ( ( largeR + (2*smallR) ) * cos( v ) ) /
        // ( 2 * smallR * ( largeR + smallR * cos( v ) ) );
        // ps->meanCurvature = h;
        if ( k > 1e-7 )
          errorSum += 100 * fabs( ( ps->gaussianCurvature - k ) / k );
        // 	std::cout << " Est.=" << ps->gaussianCurvature
        // 		  << " Real=" << k << " %" <<  fabs( ( ps->gaussianCurvature - k ) / k ) <<  std::endl;
        values.insert( pair<double,double>( k, ps->gaussianCurvature ) );
        ++ps;
      }

      double meanError = errorSum / ( end - begin );
      std::cout << " Gaussian C. total error (%): " << errorSum << std::endl;
      std::cout << " Gaussian C. average error (%): " << meanError << std::endl;
      {
        multimap<double,double>::iterator it = values.begin();
        multimap<double,double>::iterator end = values.end();
        Int32 i=0;
        if ( true ) {
          ofstream file("torus_c.txt");
          file << "#Ordre RealC EstimateC\n";
          while ( it != end ) {
            file << i << " " << it->first << "  " << it->second << "\n";
            ++i; ++it;
          }
        }
      }
    }
  }
  
  colorizeCurvature( _view3DWidget->surface().surfelList(), GAUSSIAN_CURVATURE );
  //colorizeCurvature( _view3DWidget->surface().surfelList(), MEAN_CURVATURE );
  //colorizeCurvature( _view3DWidget->surface().surfelList(), globalSettings->curvatureType() );

  globalSettings->normalsEstimateIterations( old_nei );
  globalSettings->curvatureEstimateIterations( old_cei );
  globalSettings->curvatureEstimateAveragingIterations( old_ceai );
  globalSettings->curvatureType( old_cect );
}

void
DialogExperiments::cbTorusExactNormals()
{
  Document * document = _view3DWidget->document();
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Int32 start = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  Rotation rot( Triple<double>(0,0,0),
                _leRho->text().toDouble(),
                _leTheta->text().toDouble(),
                _lePhi->text().toDouble() );

  Triple<double> xAxis = rot.apply( Triple<double>( 1, 0, 0 ) );
  Triple<double> yAxis = rot.apply( Triple<double>( 0, 1, 0 ) );
  Triple<double> zAxis = rot.apply( Triple<double>( 0, 0, 1 ) );

  int old_nei = globalSettings->normalsEstimateIterations();

  _view3DWidget->setShading( Surface::ColorFromLight );

  cout << "radius;n;average;stddev\n";

  for ( int width = start; width <= stop ; width += step ) {

    int smallR = width / 2;
    int largeR = width;

    double h = 1.0 / ( 2 * smallR + 1 );
    int n1 = static_cast<int>( floor( 0.5 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );

    globalSettings->normalsEstimateIterations( n1 );
    int size = width * 5;
    if ( size % 2 == 0 ) ++size;
    document->create( size, size, size, 1, Po_ValUC );
    Triple<SSize> center( size/2, size/2, size/2 );

    document->torus( center, largeR, smallR,
                     Rotation( center,
                               _leRho->text().toDouble(),
                               _leTheta->text().toDouble(),
                               _lePhi->text().toDouble() ),
                     Triple<double>( 1.0, 1.0, 1.0) );

    {
      // Error measurement
      // Exact normals file creation

      Surfel *begin = surfelList.begin();
      Surfel *end = surfelList.end();

      Triple<double> point;
      Triple<double> c,a,n;
      double s;
      double average;
      double stddev,minError,maxError;
      Surfel *ps = begin;
      vector<double> v;
      while ( ps != end ) {
        point = ps->center;
        point.set( point * xAxis, point * yAxis, point * zAxis );
        c = point;
        c.third = 0;
        c /= norm ( c );
        a = c * (double)width; // Large radius
        n = point - a;
        n = rot.apply( n );
        n /= norm( n ); // n is the exact normal at the surfel center
        std::cout << n << std::endl;
        s = ps->normal * n;
        if ( s > 1.0 ) s = 1.0;
        if ( s < -1.0 ) s = -1.0;
        v.push_back( rad2deg( acos( s ) ) );
        ++ps;
      }
      stats( v, minError, maxError, average, stddev );
      cout << width << " "
           << n1 << " "
           << average << " "
           << stddev << endl;
    }
  }
  globalSettings->normalsEstimateIterations( old_nei );

}

void
DialogExperiments::cbTorusNormals()
{
  Document * document = _view3DWidget->document();
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Int32 start = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  Rotation rot( Triple<double>(0,0,0),
                _leRho->text().toDouble(),
                _leTheta->text().toDouble(),
                _lePhi->text().toDouble() );
  
  Triple<double> xAxis = rot.apply( Triple<double>( 1, 0, 0 ) );
  Triple<double> yAxis = rot.apply( Triple<double>( 0, 1, 0 ) );
  Triple<double> zAxis = rot.apply( Triple<double>( 0, 0, 1 ) );
  
  int old_nei = globalSettings->normalsEstimateIterations();

  _view3DWidget->setShading( Surface::ColorFromLight );

  cout << "radius;n;average;stddev\n";

  for ( int width = start; width <= stop ; width += step ) {

    int smallR = width / 2;
    int largeR = width;

    double h = 1.0 / ( 2 * smallR + 1 );
    int n1 = static_cast<int>( floor( 0.5 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );

    globalSettings->normalsEstimateIterations( n1 );
    int size = width * 5;
    if ( size % 2 == 0 ) ++size;
    document->create( size, size, size, 1, Po_ValUC );
    Triple<SSize> center( size/2, size/2, size/2 );

    document->torus( center, largeR, smallR,
                     Rotation( center,
                               _leRho->text().toDouble(),
                               _leTheta->text().toDouble(),
                               _lePhi->text().toDouble() ),
                     Triple<double>( 1.0, 1.0, 1.0) );
    
    {
      // Error measurement
      Surfel *begin = surfelList.begin();
      Surfel *end = surfelList.end();

      Triple<double> point;
      Triple<double> c,a,n;
      double s;
      double average;
      double stddev,minError,maxError;
      Surfel *ps = begin;
      vector<double> v;
      while ( ps != end ) {
        point = ps->center;
        point.set( point * xAxis, point * yAxis, point * zAxis );
        c = point;
        c.third = 0;
        c /= norm ( c );
        a = c * (double)width; // Large radius
        n = point - a;
        n = rot.apply( n );
        n /= norm( n );
        s = ps->normal * n;
        if ( s > 1.0 ) s = 1.0;
        if ( s < -1.0 ) s = -1.0;
        v.push_back( rad2deg( acos( s ) ) );
        ++ps;
      }
      stats( v, minError, maxError, average, stddev );
      cout << width << " "
           << n1 << " "
           << average << " "
           << stddev << endl;
    }
  }
  globalSettings->normalsEstimateIterations( old_nei );
}

void
DialogExperiments::cbTorusOptimal()
{
  Document * document = _view3DWidget->document();
  Surface & surface = _view3DWidget->surface();
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Int32 radius = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  Rotation rot( Triple<double>(0,0,0),
                _leRho->text().toDouble(),
                _leTheta->text().toDouble(),
                _lePhi->text().toDouble() );
  Triple<double> xAxis = rot.apply( Triple<double>( 1, 0, 0 ) );
  Triple<double> yAxis = rot.apply( Triple<double>( 0, 1, 0 ) );
  Triple<double> zAxis = rot.apply( Triple<double>( 0, 0, 1 ) );

  int old_nei = globalSettings->normalsEstimateIterations();
  globalSettings->normalsEstimateIterations( 1 );

  _view3DWidget->setShading( Surface::ColorFromLight );

  std::cout << "#largeR;smallR;Iterations;Minimum;Maximum;Average;Std. Dev.\n";
  while ( radius <= stop ) {
    int largeR = radius * 2;
    int smallR = radius;
    int size = (smallR + largeR) * 3;
    if ( size % 2 == 0 ) ++size;
    document->create( size, size, size, 1, Po_ValUC );
    Triple<SSize> center( size/2, size/2, size/2 );
    document->torus( center,
                     largeR, smallR,
                     Rotation( center,
                               _leRho->text().toDouble(),
                               _leTheta->text().toDouble(),
                               _lePhi->text().toDouble() ),
                     Triple<double>( 1.0, 1.0, 1.0) );
    if ( _cbNoisy->isChecked() )
      document->addNoise( _sbNoiseLevel->value() );
    Int32 nbIterations = ( smallR <= 100 ) ? 100 : 150;

    _view3DWidget->zoomFit();
    
    SurfelDataArray sdaSrc( surfelList.size() );
    SurfelDataArray sdaDest( surfelList.size() );
    surface.centersArray( sdaSrc );
    Surfel *ps;
    Surfel *end = surfelList.end();
    Surfel *begin = surfelList.begin();

    int iterations = 0;
    double average = -1.0;
    double stddev = 0.0;
    double minError;
    double maxError;
    vector<double> errors;
    vector<double> stddevs;
    do {
      // Convolution of the centers Once
      ++iterations;
      ps = begin;
      while ( ps != end ) {
        convolution421( ps, surfelList, sdaSrc, sdaDest );
        ++ps;
      }
      sdaSrc = sdaDest;

      // Compute estimated normal vectors
      ps = begin;
      Triple<double> dxdu, dxdv;
      while ( ps != end ) {
        dxdu = sdaSrc[ ps->neighbors[0] - begin ] - sdaSrc[ ps - begin ];
        dxdv = sdaSrc[ ps->neighbors[1] - begin ] - sdaSrc[ ps - begin ];
        ps->normal = wedgeProduct( dxdu, dxdv );
        ps->normal /= norm( ps->normal );
        ++ps;
      }
      _view3DWidget->forceRedraw();
      qApp->processEvents();

      //
      // Compute the min, max, average and std. dev. of the angular error.
      //
      {
        Triple<double> point;
        Triple<double> c,a,n;
        double s;
        vector<double> v;
        ps = begin;
        while ( ps != end ) {
          point = ps->center;
          point.set( point * xAxis, point * yAxis, point * zAxis );
          c = point;
          c.third = 0;
          c /= norm ( c );
          a = c * (double)largeR; // Large radius
          n = point - a;
          n = rot.apply( n );
          n /= norm( n );
          s = ps->normal * n;
          if ( s > 1.0 ) s = 1.0;
          if ( s < -1.0 ) s = -1.0;
          v.push_back( rad2deg( acos( s ) ) );
          ++ps;
        }
        stats( v, minError, maxError, average, stddev );
      }
      errors.push_back( average );
      stddevs.push_back( stddev );
    } while ( iterations < nbIterations );
    vector<double>::const_iterator minerr = min_element( errors.begin(), errors.end() );
    vector<double>::const_iterator mindev = stddevs.begin() + ( minerr - errors.begin() );
    cout << largeR << " " << smallR << " " << (( minerr - errors.begin() ) + 1)
         << " " << (*minerr) << " " << (*mindev) << endl;
    radius += step;
  }
  globalSettings->normalsEstimateIterations(old_nei);
}


void
DialogExperiments::cbHyperbolicParaboloids()
{
  Document * document = _view3DWidget->document();
  Int32 start = _sbStart->value();
  Int32 stop = _sbStop->value();
  Int32 step = _sbStep->value();

  int old_nei = globalSettings->normalsEstimateIterations();
  int old_cei = globalSettings->curvatureEstimateIterations();
  int old_ceai = globalSettings->curvatureEstimateAveragingIterations();
  int old_cect = globalSettings->curvatureType();

  globalSettings->curvatureType( GAUSSIAN_CURVATURE );
  _view3DWidget->setShading( Surface::ColorFromCurvature );

  std::cout << "Rayon;Iterations;Minimum;Maximum\n";
  
  for ( int width = start; width <= stop ; width += step ) {
    
    double h = 1.0 / ( 2 * width + 1 );
    int n1 = static_cast<int>( floor( 0.5 * exp( - ( 4.0 / 3.0 ) * log( h ) ) ) );
    int n2 = static_cast<int>( floor( 0.5 * exp( - ( 14.0 / 9.0 ) * log( h ) ) ) );

    SHOW( n1 );
    SHOW( n2 );

    document->create( n2*2 + 1, n2*2 + 1 , n2*2 + 1, 1, Po_ValUC );
    Triple<SSize> center( document->image()->width()/2,
                          document->image()->height()/2,
                          document->image()->depth()/2 );

    Triple<double> normal = Rotation( Triple<double>(0,0,0),
                                      _leRho->text().toDouble(),
                                      _leTheta->text().toDouble(),
                                      _lePhi->text().toDouble() ).apply( Triple<double>( 0, 0, 1 ) );

    globalSettings->normalsEstimateIterations( n1 );
    globalSettings->curvatureEstimateIterations( n2 );
    globalSettings->curvatureEstimateAveragingIterations( 0 );
    document->hyperbolicParaboloid( center,
                                    sqrt( static_cast<double>(width) /*_leC1->text().toDouble()*/ ),
                                    sqrt( static_cast<double>(width) /*_leC2->text().toDouble()*/ ),
                                    Rotation( center,
                                              _leRho->text().toDouble(),
                                              _leTheta->text().toDouble(),
                                              _lePhi->text().toDouble() ),
                                    Triple<double>( 1.0, 1.0, 1.0) );
    _view3DWidget->zoomFit();
    
    // _view3DWidget->setShading( -1 ); // Useless ( change of the document forces the refresh )

    //
    // Statistics
    //

    // Find the center surfel
    SurfelList & surfelList = _view3DWidget->surface().surfelList();
    Surfel *begin = surfelList.begin();
    Surfel *surfel = begin;
    Surfel *end = surfelList.end();
    Surfel *seed  = 0;
    double minError = std::numeric_limits<double>::max();
    while ( surfel != end && !seed ) {
      double error = acos( surfel->normal * normal );
      if ( surfel->voxel == center && error < minError ) {
        seed = surfel;
        minError = error;
      }
      ++surfel;
    }
    if ( !seed ) {
      ERROR << "Could not find the center of the paraboloid.\n";
      exit( 0 );
    }

    // Get a set of surfels
    vector<Surfel*> surfels;
    breadthVisit( seed, 3 * width, surfels );   // is "width" optimal  ?

    Triple<double> xAxis = Rotation( Triple<double>(0,0,0),
                                     _leRho->text().toDouble(),
                                     _leTheta->text().toDouble(),
                                     _lePhi->text().toDouble() ).apply( Triple<double>( 1, 0, 0 ) );
    Triple<double> yAxis = Rotation( Triple<float>(0,0,0),
                                     _leRho->text().toDouble(),
                                     _leTheta->text().toDouble(),
                                     _lePhi->text().toDouble() ).apply( Triple<double>( 0, 1, 0 ) );
    Triple<double> zAxis = Rotation( Triple<float>(0,0,0),
                                     _leRho->text().toDouble(),
                                     _leTheta->text().toDouble(),
                                     _lePhi->text().toDouble() ).apply( Triple<double>( 0, 0, 1 ) );
    Triple<double> p,diff;
    
    multimap<double,double> values;

    // Compute the average error.
    double errorSum = 0.0f;
    vector<Surfel*>::iterator is = surfels.begin();
    vector<Surfel*>::iterator ise = surfels.end();
    while ( is != ise ) {
      diff = (*is)->center;
      p.set( diff * xAxis, diff * yAxis, diff * zAxis );
      double k = -4 / ( dsquare( width ) * dsquare( 1 + 4 * dsquare( p.first / width ) + 4 * dsquare( p.second / width ) ) );
      double h = ( 4 * dsquare( p.second ) / width - 4 * dsquare( p.first ) / width  ) /
          ( dsquare( width ) * exp( 1.5 * log( 1 + 4 * dsquare( p.first / width ) + 4 * dsquare( p.second / width )  ) ) );
      double error = 100 * fabs( ( (*is)->gaussianCurvature - k ) / k ) ;
      //double error = 100 * fabs( ( (*is)->meanCurvature - h ) / h ) ;
      // cout << "Est. " << (*is)->gaussianCurvature << " <-> Exact: " << k << " (" << error << "%)\n";
      // cout << (*is)->meanCurvature << " <-> " << h << " (" << error << "%)\n";
      // values.insert( pair<double,double>( k, (*is)->gaussianCurvature ) );
      values.insert( pair<double,double>( h, (*is)->meanCurvature ) );
      errorSum += error;
      ++is;
    }
    double averageError = errorSum / surfels.size();

    // Zero curvature everywhere except for the surfels in the vector "surfels".
    {
      vector<double> curvatures( surfels.size() );
      vector<Surfel*>::iterator is = surfels.begin();
      vector<Surfel*>::iterator isb = surfels.begin();
      vector<Surfel*>::iterator ise = surfels.end();
      while ( is != ise ) {
        curvatures[ is - isb ] = (*is)->gaussianCurvature;
        ++is;
      }
      Surfel * ps = _view3DWidget->surface().surfelList().begin();
      Surfel * end = _view3DWidget->surface().surfelList().end();
      while ( ps != end ) {
        ps->gaussianCurvature = 0;
        ++ps;
      }
      is = surfels.begin();
      while ( is != ise ) {
        (*is)->gaussianCurvature = curvatures[ is - isb ];
        ++is;
      }
    }
    
    // Compute the standard deviation.
    double sqDiffSum = 0.0f;
    is = surfels.begin();
    while ( is != ise ) {
      diff = (*is)->center;
      p.set( diff * xAxis, diff * yAxis, diff * zAxis );
      double k = -4 / ( dsquare( width ) * dsquare( 1 + 4 * dsquare( p.first / width ) + 4 * dsquare( p.second / width ) ) );
      /*double h = ( 4 * dsquare( p.second ) / width - 4 * dsquare( p.first ) / width  )
     / ( dsquare( width ) * exp( 1.5 * log( 1 + 4 * dsquare( p.first / width ) + 4 * dsquare( p.second / width )  ) ) );
   */
      double error = 100 * fabs( ( (*is)->gaussianCurvature - k ) / k ) ;
      //double error = 100 * fabs( ( (*is)->meanCurvature - h ) / h ) ;
      sqDiffSum += dsquare( error - averageError );
      // (*is)->gaussianCurvature = k;
      ++is;
    }
    double stddev = sqrt( sqDiffSum / surfels.size() );
    
    std::cout << "width=" << width
              << "\n iterations N=" << n1
              << "\n iterations C=" << n2
              << "\n exact normal is: " << normal
              << "\n average error (%): " << averageError
              << "\n std. dev. of the error (%): " << stddev
              << "\n over " << surfels.size() << " surfels "
              << std::endl;

    {
      multimap<double,double>::iterator it = values.begin();
      multimap<double,double>::iterator end = values.end();
      int i=0;
      if ( true ) while ( it != end ) {
        cout << i << " " << it->first << "  " << it->second << "\n";
        ++i; ++it;
      }
    }


  }

  colorizeCurvature( _view3DWidget->surface().surfelList(), GAUSSIAN_CURVATURE );

  globalSettings->normalsEstimateIterations( old_nei );
  globalSettings->curvatureEstimateIterations( old_cei );
  globalSettings->curvatureEstimateAveragingIterations( old_ceai );
  globalSettings->curvatureType( old_cect );
}

void
DialogExperiments::cbMarkSurfel( bool on )
{
  if ( on ) {
    _view3DWidget->surface().markSurfel( _sbMarkSurfel->value() );
  } else {
    _view3DWidget->surface().markSurfel( -1 );
  }
}

void
DialogExperiments::cbCheck()
{
  Document *document = _view3DWidget->document();
  AbstractImage & image = *(document->image());
  Triple<SSize> center;
  center.first = image.width() / 2;
  center.second = image.height() / 2;
  center.third = image.depth() / 2;

  Triple<double> fcenter = center;

  Rotation rotation( fcenter,
                     _leRho->text().toDouble(),
                     _leTheta->text().toDouble(),
                     _lePhi->text().toDouble() );
  Triple<double> normal( image.width() / 2,
                         image.height() / 2,
                         image.depth() / 2 + 40 );
  normal = rotation.apply( normal ) - fcenter;
  normal /= abs( normal );

  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Surfel *surfel = surfelList.begin();
  Surfel *end = surfelList.end();
  while ( surfel != end ) {
    if ( surfel->voxel == center ) {
      double angle = acos( surfel->normal * normal ) * 180.0f / M_PI;
      std::cout << surfel->voxel << ": "
                << surfel->normal << " <> " << normal
                << " Err: " << angle << " deg" << std::endl;
    }
    ++surfel;
  }

}

void
DialogExperiments::cbParaboloids( )
{
  Document * document = _view3DWidget->document();
  Int32 start = _sbStart->value();
  Int32 stop = _sbStop->value();
  Int32 step = _sbStep->value();

  int old_cei = globalSettings->curvatureEstimateIterations();
  int old_ceai = globalSettings->curvatureEstimateAveragingIterations();
  // int old_cect = globalSettings->curvatureType();
  // globalSettings->curvatureType( MEAN_CURVATURE );
  _view3DWidget->setShading( Surface::ColorFromCurvature );

  std::cout << "Rayon;Iterations;Minimum;Maximum\n";

  Triple<SSize> center( document->image()->width() / 2,
                        document->image()->height() / 2,
                        document->image()->depth() / 2 );

  document->clear();
  document->paraboloid( center,
                        _leC1->text().toDouble(),
                        _leC2->text().toDouble(),
                        Rotation( center, 0.0, 0.0, 0.0 ),
                        Triple<double>( 1.0, 1.0, 1.0) );
  
  _view3DWidget->zoomFit();
  
  for ( int i = start ; i < stop ; i+= step ) {
    globalSettings->curvatureEstimateIterations( i );
    _view3DWidget->setShading( -1 );
  }
  
  globalSettings->curvatureEstimateIterations( old_cei );
  globalSettings->curvatureEstimateAveragingIterations( old_ceai );
  //globalSettings->curvatureType( old_cect );
}

void 
DialogExperiments::shapeFromShading()
{
}

void 
DialogExperiments::showGraph( bool on )
{
#ifdef HAVE_QWT
  if ( !on && _plotDialog->isShown() )
    _plotDialog->hide();
  if ( on && ! _plotDialog->isShown() )
    _plotDialog->show();
#else
  TRACE << "SHOW GRAPH" <<  on << std::endl ;
#endif
}

void
DialogExperiments::initResponse()
{
  _view3DWidget->surface()
      .scalarArrayFromMarkedSurfels( _view3DWidget->surface().surfelDataArray(),
                                     255 );
}

void
DialogExperiments::response()
{  
  _view3DWidget->surface().convolutionResponse( _sbStop->value() );
  _view3DWidget->forceRedraw(true);
  if ( _cbAutoWrite->isChecked() )
    _view3DWidget->writeView();
}

void
DialogExperiments::breadthVisiting()
{
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  int iterations = _sbStop->value();
  std::vector<Surfel*> border1;
  std::vector<Surfel*> border2;
  std::vector<Surfel*> *borderA = &border1;
  std::vector<Surfel*> *borderB = &border2;
  Surfel *ps = surfelList.begin();
  Surfel *psend = surfelList.end();
  while ( ps < psend ) {
    if ( ps->color == 255 ) {
      borderA->push_back( ps );
      SHOW( ps );
    }
    ++ps;
  }
  
  UChar color = 0;
  while ( iterations-- ) {
    std::vector<Surfel*>::iterator ips = borderA->begin();
    std::vector<Surfel*>::iterator end = borderA->end();
    color += 1;
    if ( !color ) color = 1;
    while ( ips != end ) {
      std::vector<Surfel*> neighborhood = vNeighborhood( *ips );
      std::vector<Surfel*>::iterator it = neighborhood.begin();
      std::vector<Surfel*>::iterator ite = neighborhood.end();
      while ( it != ite ) {
        if ( (*it)->color == 0 ) {
          (*it)->color = color;
          borderB->push_back( *it );
        }
        ++it;
      }
      ++ips;
    }
    swap( borderA, borderB );
    borderB->clear();
  }
  _view3DWidget->forceRedraw(true);
}

void
DialogExperiments::breadthVisit( Surfel *surfel,
                                 int size,
                                 std::vector<Surfel*> & surfels )
{
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  int iterations = size;
  std::vector<Surfel*> border1;
  std::vector<Surfel*> border2;
  std::vector<Surfel*> *borderA = &border1;
  std::vector<Surfel*> *borderB = &border2;
  Surfel *ps = surfelList.begin();
  Surfel *psend = surfelList.end();
  while ( ps != psend ) {
    ps->color = 0;
    ++ps;
  }
  // Start with surfel in the list
  surfel->color = 1;
  borderA->push_back( surfel );
  surfels.clear();
  surfels.push_back( surfel );

  UChar color;
  while ( iterations-- ) {
    std::vector<Surfel*>::iterator ips = borderA->begin();
    std::vector<Surfel*>::iterator end = borderA->end();
    if ( ips != end ) color = (*ips)->color + 1;
    if ( !color ) color = 1;
    while ( ips != end ) {
      std::vector<Surfel*> neighborhood = vNeighborhood( *ips );
      std::vector<Surfel*>::iterator it = neighborhood.begin();
      std::vector<Surfel*>::iterator ite = neighborhood.end();
      while ( it != ite ) {
        if ( (*it)->color == 0 ) {
          (*it)->color = color;
          borderB->push_back( *it );
          surfels.push_back( *it );
        }
        ++it;
      }
      ++ips;
    }
    swap( borderA, borderB );
    borderB->clear();
  }
}

void
DialogExperiments::computeIterations()
{
  AbstractImage * image = _view3DWidget->document()->image();
  SSize xMin,yMin,zMin;
  SSize xMax,yMax,zMax;
  image->boundingBox( xMin, yMin, zMin,
                      xMax, yMax, zMax );
  xMax -= xMin;
  yMax -= yMin;
  zMax -= zMin;

  SSize size = ::max( xMax, ::max(yMax,zMax) );
  double h = 1.0 / size;
  int steps_n = static_cast<int>( floor( 0.2 * exp( -4/3.0 * log( h ) ) ) );
  int steps_c = static_cast<int>( floor( 0.2 * exp( -14/9.0 * log( h ) ) ) );

  globalSettings->normalsEstimateIterations( steps_n );
  globalSettings->curvatureEstimateIterations( steps_c );
}


namespace {
inline Surfel * next( Surfel * ps, int i )
{
  return ps->neighbors[ i ];
}
inline Surfel * nextnext( Surfel * ps, int i )
{
  int edge = ( edgeFromTable[ ps->direction ][ i ][ ps->neighbors[ i ]->direction ] + 2 ) % 4 ;
  return ps->neighbors[ i ]->neighbors[ edge ];
}
}

void
DialogExperiments::cbSphereOptimalCurvature()
{
  Document * document = _view3DWidget->document();
  Surface & surface = _view3DWidget->surface();
  SurfelList & surfelList = _view3DWidget->surface().surfelList();
  Int32 radius = _sbStart->value();
  Int32 step = _sbStep->value();
  Int32 stop = _sbStop->value();
  int old_nei = globalSettings->normalsEstimateIterations();
  int old_cei = globalSettings->curvatureEstimateIterations();

  globalSettings->normalsEstimateIterations( 0 );
  globalSettings->curvatureEstimateIterations( 1 );
  _view3DWidget->setShading( Surface::ColorFromCurvature );

  std::cout << "# Radius;Iterations;Average;Std. Dev.\n";
  while ( radius <= stop ) {
    document->create( radius*2+3, radius*2+3, radius*2+3, 1, Po_ValUC );
    document->sphere( document->image()->width() / 2,
                      document->image()->height() / 2,
                      document->image()->depth() / 2,
                      radius );
    if ( _cbNoisy->isChecked() )
      document->addNoise( _sbNoiseLevel->value() );
    
    _view3DWidget->zoomFit();

    SurfelDataArray sdaSrc( surfelList.size() );
    SurfelDataArray sdaDest( surfelList.size() );
    surface.centersArray( sdaSrc );
    Surfel *ps, *end = surfelList.end();
    Surfel *begin = surfelList.begin();

    Int32 nbIterations = ( radius <= 100 ) ? 100 : 150;
    int iterations = 0;
    vector<double> errors;
    vector<double> stddevs;
    do {

      // Convolution of the centers Once
      ++iterations;
      ps = surfelList.begin();
      while ( ps != end ) {
        convolution421( ps, surfelList, sdaSrc, sdaDest );
        ++ps;
      }
      sdaSrc =  sdaDest;

      // Compute the curvatures
      Triple<double> dxdu, dxdv, nu;
      Triple<double> d2xdu2, d2xdv2, d2xduv;
      Triple<double> tmp1, tmp2;
      int edge;
      ps = surfelList.begin();
      while ( ps != end ) {
        dxdu = 0.5 * ( sdaSrc[ next(ps,0) - begin ] - sdaSrc[ next(ps,2) - begin ] );
        dxdv = 0.5 * ( sdaSrc[ next(ps,1) - begin ] - sdaSrc[ next(ps,3) - begin ] );
        nu = wedgeProduct( dxdu, dxdv );
        nu /= norm( nu );
        ps->normal = nu;

        tmp1 = 0.5 * ( sdaSrc[ nextnext(ps,0) - begin ] - sdaSrc[ ps - begin ] );
        tmp2 = 0.5 * ( sdaSrc[ ps - begin ] - sdaSrc[ nextnext(ps,2) - begin ] );
        d2xdu2 = 0.5 * ( tmp1 - tmp2 );

        tmp1 = 0.5 * ( sdaSrc[ nextnext(ps,1) - begin ] - sdaSrc[ ps - begin ] );
        tmp2 = 0.5 * ( sdaSrc[ ps - begin ] - sdaSrc[ nextnext(ps,3) - begin ] );
        d2xdv2 = 0.5 * ( tmp1 - tmp2 );

        edge = ( edgeFromTable[ ps->direction ][ 1 ][ next(ps,1)->direction ] + 1 ) % 4;
        tmp1 = 0.5 * ( sdaSrc[ next(next(ps,1),edge) - begin ] - sdaSrc[ next(next(ps,1),(edge+2)%4) - begin ] );
        edge = ( edgeFromTable[ ps->direction ][ 3 ][ next(ps,3)->direction ] + 3 ) % 4;
        tmp2 = 0.5 * ( sdaSrc[ next(next(ps,3),edge) - begin ] - sdaSrc[ next(next(ps,3),(edge+2)%4) - begin ] );
        d2xduv = 0.5 * ( tmp1 - tmp2 );

        double E = dxdu * dxdu;
        double F = dxdu * dxdv;
        double G = dxdv * dxdv;
        double e = (-nu) * d2xdu2;
        double f = (-nu) * d2xduv;
        double g = (-nu) * d2xdv2;
        double EG_F2 = E*G - dsquare(F);
        double eg_f2 = e*g - dsquare(f);
        double eG_2fF_gE = e*G - 2*f*F + g*E;

        ps->gaussianCurvature = eg_f2 / EG_F2;
        ps->meanCurvature = eG_2fF_gE / ( 2 * EG_F2 );
        ++ps;
      }


      colorizeCurvature( _view3DWidget->surface().surfelList(),
                         MEAN_CURVATURE );
      _view3DWidget->forceRedraw();
      qApp->processEvents();

      //
      // Compute the min, max, average and std. dev. of the mean curvature error.
      //
      vector<double> v;
      double error = 0.0;
      double mean = 1.0 / radius;
      ps = surfelList.begin();
      while ( ps != end ) {
        error = 100.0 * fabs( ps->meanCurvature - mean ) / mean;
        // error = 100.0 * fabs( (1/ps->meanCurvature) - radius ) / radius;
        // error = fabs( ps->meanCurvature - mean );
        v.push_back( error );
        ++ps;
      }

      double stddev;
      double average;
      double minError;
      double maxError;
      stats( v, minError, maxError, average, stddev );

      errors.push_back( average );
      stddevs.push_back( stddev );
      if ( iterations == 74 ) {
        histo( v, 0, "histo.txt", 0 );
        ofstream f("dump.txt");
        sort( v.begin(), v.end() );
        vector<double>::iterator it = v.begin();
        while ( it != v.end() ) {
          f << (it - v.begin()) << " " << (*it) << std::endl;
          ++it;
        }
      }
    } while ( iterations < nbIterations  );

    vector<double>::const_iterator minerr = min_element( errors.begin(), errors.end() );
    vector<double>::const_iterator mindev = stddevs.begin() + ( minerr - errors.begin() );
    cout << radius
         << " " << (( minerr - errors.begin() ) + 1)
         << " " << (*minerr)
         << " " << (*mindev)
         << endl;

    radius += step;
  }
  globalSettings->normalsEstimateIterations(old_nei);
  globalSettings->curvatureEstimateIterations(old_cei);
}

void
DialogExperiments::cbTorusOptimalCurvature()
{

}

void
DialogExperiments::cbConvexHull()
{
  Document * document = _view3DWidget->document();
  //globalSettings->curvatureType( MEAN_CURVATURE );

  _view3DWidget->setShading( Surface::ColorFixed );
  _view3DWidget->setShading( Surface::ColorFromCurvature );
  
  ZZZImage<UChar> * volume;
  try {
    volume = dynamic_cast< ZZZImage<UChar> * >( document->image() );
  } catch ( std::bad_cast ) {
    std::cerr << "Document::image() Not a ZZZImage<UChar>.\n";
    return;
  }


  Size addedVoxels = 0;

  int n = _sbConvexHull->value();
  while (n--) {
    SurfelList & surfelList = _view3DWidget->surface().surfelList();
    Surfel *ps = surfelList.begin();
    Surfel *end = surfelList.end();
    Triple<SSize> voxel;
    while ( ps < end ) {
      double mean = ps->meanCurvature;
      if ( (mean < 0) && volume->holds(voxel = ps->voxel + NeighborsShifts6[ps->direction]) ) {
        (*volume)(voxel) = 1;
        ++addedVoxels;
      }
      ++ps;
    }
    SHOW(addedVoxels);
    document->notify();
  }
}

