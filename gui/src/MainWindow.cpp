/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   MainWindow.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 *
 * @brief  Declaration of the class MainWindow
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "MainWindow.h"

#include "globals.h"

#include <iostream>
#include <cstdlib>

#include "Dialog2DView.h"
#include "DialogAbout.h"
#include "DialogSettings.h"
#include "DialogInputSize.h"
#include "DialogAspect.h"
#include "DialogSkel.h"
#include "DialogThresholds.h"
#include "VolumeInfoWidget.h"
#include "ColorMapView.h"
#include "Dialog2DView.h"
#include "DialogMerge.h"
#include "Dialogs.h"
#include "Settings.h"
#include "SurfaceExporter.h"

#ifdef HAVE_EXPERIMENTAL
#include "DialogExperiments.h"
#endif // HAVE_EXPERIMENTAL

#include "AnimationToolBar.h"
#include "ToolsToolBar.h"
#include "ColorMapToolBar.h"
#include "ViewParamsToolBar.h"
#include "ActionsToolBar.h"
#include "PluginProgressWidget.h"

#include <QMessageBox>
#include <QStatusBar>
#include <QMenu>
#include <QProgressBar>
#include <QCloseEvent>
#include <QImageReader>
#include <QFileDialog>
#include <QDir>
#include <QInputDialog>
#include <QtXml>
#include <QActionGroup>
#include <QImageWriter>
#include "PluginDialog.h"

#include "QTools.h"

#include "globals.h"


/**
 * Constructor
 */
MainWindow::MainWindow(QWidget * parent, Qt::WindowFlags flags)
  : QMainWindow(parent, flags )
{
  CONS( MainWindow );
  setupUi(this);
  _view3DWidget->document( &_document );

  _dumpVolume = false;

  fileNewAction->setShortcut( QKeySequence::New );
  fileOpenAction->setShortcut( QKeySequence::Open );
  fileSaveAction->setShortcut( QKeySequence::Save );

#if QT_VERSION >= 0x040600
  /*
    * Default icon images from
    * http://standards.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
    */

  fileSaveAsAction->setShortcut( QKeySequence::SaveAs );
  fileExitAction->setShortcut( QKeySequence::Quit );
  fileNewAction->setIcon( QIcon::fromTheme( "document-new",
                                            QIcon(":toolbars/Pixmaps/filenew.png") ) );
  fileOpenAction->setIcon( QIcon::fromTheme( "document-open",
                                             QIcon(":toolbars/Pixmaps/fileopen.png") ) );
  fileSaveAction->setIcon( QIcon::fromTheme( "document-save",
                                             QIcon(":toolbars/Pixmaps/filesave.png") ) );
  fileSaveAsAction->setIcon( QIcon::fromTheme( "document-save-as",
                                               QIcon(":toolbars/Pixmaps/filesaveas.png") ) );
  fileExitAction->setIcon( QIcon::fromTheme( "application-exit",
                                             QIcon(":toolbars/Pixmaps/exit.png") ) );
  zoomInAction->setIcon( QIcon::fromTheme( "zoom-in",
                                           QIcon(":toolbars/Pixmaps/zoomin24.png") ) );
  zoomOutAction->setIcon( QIcon::fromTheme( "zoom-out",
                                            QIcon(":toolbars/Pixmaps/zoomout24.png") ) );
  zoomFitAction->setIcon( QIcon::fromTheme( "zoom-fit-best",
                                            QIcon(":toolbars/Pixmaps/zoomfit24.png") ) );
  zoomOneAction->setIcon( QIcon::fromTheme( "zoom-original",
                                            QIcon(":toolbars/Pixmaps/zoomOne24.png") ) );
  settingsAction->setIcon( QIcon::fromTheme( "preferences-system",
                                             QIcon(":toolbars/Pixmaps/package_settings.png") ) );
  moveAction->setIcon( QIcon::fromTheme( "object-rotate",
                                         QIcon(":DrawingToolbar/Pixmaps/object-rotate.png") ) );
  viewExportAction->setIcon( QIcon::fromTheme( "camera-photo",
                                               QIcon(":toolbars/Pixmaps/camera.png") ) );
#else
  fileSaveAsAction->setShortcut( QKeySequence("Ctrl+Shift+S") );
  fileExitAction->setShortcut( QKeySequence("Ctrl+Q") );
  fileNewAction->setIcon( QIcon(":toolbars/Pixmaps/filenew.png") );
  fileOpenAction->setIcon( QIcon(":toolbars/Pixmaps/fileopen.png") );
  fileSaveAction->setIcon( QIcon(":toolbars/Pixmaps/filesave.png") );
  fileSaveAsAction->setIcon( QIcon(":toolbars/Pixmaps/filesaveas.png") );
  fileExitAction->setIcon( QIcon(":toolbars/Pixmaps/exit.png") );
  zoomInAction->setIcon( QIcon(":toolbars/Pixmaps/zoomin24.png") );
  zoomOutAction->setIcon( QIcon(":toolbars/Pixmaps/zoomout24.png") );
  zoomFitAction->setIcon( QIcon(":toolbars/Pixmaps/zoomfit24.png") );
  zoomOneAction->setIcon( QIcon(":toolbars/Pixmaps/zoomOne24.png") );
  settingsAction->setIcon( QIcon(":toolbars/Pixmaps/package_settings.png") );
  moveAction->setIcon( QIcon(":DrawingToolbar/Pixmaps/object-rotate.png") );
  viewExportAction->setIcon( QIcon(":toolbars/Pixmaps/camera.png") );
#endif
  createDialogs();
  createMenus();
  createToolBars();

  _pluginProgressWidget = new PluginProgressWidget;

  _volumeInfoWidget = new VolumeInfoWidget( statusBar() );
  _volumeInfoWidget->document( &_document );
  _volumeInfoWidget->surface( &_view3DWidget->surface() );
  _colorMapView = new ColorMapView( statusBar(),
                                    &(_view3DWidget->colorMap()) );
  _progressBar = new QProgressBar( statusBar() );
  _progressBar->hide();
  globalProgressReceiver.setProgressBar( _progressBar );
  globalProgressReceiver.setStatusBar(statusBar());
  statusBar()->addPermanentWidget( _progressBar  );
  statusBar()->addPermanentWidget( _volumeInfoWidget );
  statusBar()->addPermanentWidget( _colorMapView );

  showColorMapAction->setChecked( globalSettings->drawColorMap() );
  showFPSAction->setChecked( globalSettings->showFPS() );
  showAxisAction->setChecked( globalSettings->drawAxis() );

  globalSettings->fullVolumeBoxAction()->setChecked( false );
  globalSettings->fullVolumeBoxAction()->setShortcut( Qt::CTRL + Qt::Key_F );

  selectedColorChanged( _view3DWidget->colorMap().selectedColor() );

  _view3DWidget->highlightSlice( false );

  connections();
  updateCaption();
}

MainWindow::~MainWindow()
{
  delete globalSettings;
  DEST( MainWindow );
}

void
MainWindow::createMenus()
{
  /*
    * Menus
    */
  TRACE << "Building menus...\n";

  QMenu *fileMenu = new QMenu( tr( "&File" ), this );
  fileMenu->addAction( fileNewAction );
  fileMenu->addAction( fileOpenAction );

  _recentFilesMenu = new QMenu( tr( "Open &recent" ), this );
  fileMenu->addMenu( _recentFilesMenu );
  connect( _recentFilesMenu, SIGNAL( aboutToShow() ),
           this, SLOT( popupRecentFileOpened() ) );
  connect( _recentFilesMenu, SIGNAL( triggered( QAction* ) ),
           this, SLOT( fileOpenRecent( QAction* ) ) );

  fileMenu->addAction( fileMergeAction );
  fileMenu->addSeparator();
  fileMenu->addAction( fileSaveAction );
  fileMenu->addAction( fileSaveAsAction );
  fileMenu->addAction( settingsAction );
  fileMenu->addAction( fileImportAction );
  fileMenu->addSeparator();
  fileMenu->addAction( fileExitAction );

  menuBar()->addMenu( fileMenu );

  QAction * action;
  _shadingMenu = new QMenu( tr( "&Surface shading" ), this );
  _shadingActions = new QActionGroup( this );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Orientation" ) );
  action->setData( static_cast<int>( Surface::ColorFromOrientation ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Depth" ) );
  action->setData( static_cast<int>( Surface::ColorFromDepth ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Curvature" ) );
  action->setData( static_cast<int>( Surface::ColorFromCurvature ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Voxel values" ) );
  action->setData( static_cast<int>( Surface::ColorFromMaterial ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "White" ) );
  action->setData( static_cast<int>( Surface::ColorWhite ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Fixed" ) );
  action->setData( static_cast<int>( Surface::ColorFixed ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Light" ) );
  action->setData( static_cast<int>( Surface::ColorFromLight ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Lighted material" ) );
  action->setData( static_cast<int>( Surface::ColorFromLightedMaterial ) );
  _shadingActions->addAction( action = _shadingMenu->addAction( "Lighted LUT" ) );
  action->setData( static_cast<int>( Surface::ColorFromLightedLUT ) );
  _shadingActions->setExclusive( true );

  {
    QList<QAction*> alist = _shadingActions->actions();
    QList<QAction*>::iterator it = alist.begin(), end = alist.end();
    while ( it < end ) {
      if ( (*it)->data().toInt() == globalSettings->shading() )
        (*it)->setChecked( true );
      ++it;
    }
  }

  connect( _shadingMenu, SIGNAL( aboutToShow() ),
           this, SLOT( popupShadingOpened() ) );

  connect( _shadingActions, SIGNAL( triggered(QAction* ) ),
           this, SLOT( shadingMenuSelected(QAction* ) ) );

  QMenu *viewMenu = new QMenu( tr( "&View" ), this );

  viewMenu->addAction( show2DViewAction );
  viewMenu->addAction( viewAspectAction );
  viewMenu->addMenu( _shadingMenu );
  viewMenu->addSeparator();
  viewMenu->addAction( viewLoadParamsAction );
  viewMenu->addAction( viewSaveParamsAction );
  viewMenu->addSeparator();
  viewMenu->addAction( viewExportAction );
  viewMenu->addSeparator();
  viewMenu->addAction( showEdgesAction );
  viewMenu->addAction( showAxisAction );
  viewMenu->addAction( showColorMapAction );
  // viewMenu->addAction( showFPSAction );

  menuBar()->addMenu( viewMenu );

  QMenu *surfaceMenu = new QMenu( tr( "&Surface" ), this );
  surfaceMenu->addAction( globalSettings->fullVolumeBoxAction() );
  surfaceMenu->addAction( surfaceExportAction );
  surfaceMenu->addSeparator();
  surfaceMenu->addAction( surfaceColorizeBorderAction );
  menuBar()->addMenu( surfaceMenu );

  QMenu *volumeMenu = new QMenu( tr( "&Volume" ), this );

  /* Volume > Colors */
  QMenu *colorsMenu = new QMenu( tr( "&Colors" ), this );
  colorsMenu->addAction( volumeBinarizeAction );
  colorsMenu->addAction( volumeClampAction );
  colorsMenu->addAction( volumeToGrayAction );
  colorsMenu->addAction( volumeToColorAction );
  colorsMenu->addAction( volumeNegativeAction );
  colorsMenu->addAction( volumeQuantizeAction );
  colorsMenu->addAction( volumeShadeFromHueChannelAction );
  colorsMenu->addAction( volumeShadeFromSaturationChannelAction );
  colorsMenu->addSeparator();
  colorsMenu->addAction( volumeColorHistogramAction );
  volumeMenu->addMenu( colorsMenu );

  QMenu *convertMenu = new QMenu( tr( "Convert voxel type" ), this );

  const char * names[] = { "to Bool", "to UChar", "to Short", "to UShort",
                           "to Long", "to ULong", "to Float", "to Double",
                           "to Bool" };
  int types[] = { Po_ValB, Po_ValUC, Po_ValSS, Po_ValUS,
                  Po_ValSL, Po_ValUL, Po_ValSF, Po_ValSD, Po_ValB  };
  QActionGroup *agroup = new QActionGroup( this );
  for ( int i = 0; i < 7; ++i ) {
    action = new QAction( names[i], this );
    action->setData( QVariant( types[i] ) );
    _convertActions.push_back( action );
    convertMenu->addAction( action );
    agroup->addAction( action );
  }
  volumeMenu->addMenu( convertMenu );
  connect( convertMenu, SIGNAL( triggered( QAction * ) ),
           this, SLOT( volumeConvertAction( QAction * ) ) );

  /* Volume > Scale */
  QMenu *scaleMenu = new QMenu( tr( "&Scale" ), this );
  action = new QAction( tr( "200% (&All directions)" ), this );
  action->setData( 3 );
  scaleMenu->addAction( action  );
  action = new QAction( tr( "200% O&x" ), this );
  action->setData( 0 );
  scaleMenu->addAction( action );
  action = new QAction( tr( "200% O&y" ), this );
  action->setData( 1 );
  scaleMenu->addAction( action );
  action = new QAction( tr( "200% O&z" ), this );
  action->setData( 2 );
  scaleMenu->addAction( action );
  volumeSubSampleAction->setData( -1 ); // Prevent from interferring.
  scaleMenu->addAction( volumeSubSampleAction );
  connect( scaleMenu, SIGNAL( triggered(QAction*) ),
           this, SLOT( volumeScale(QAction*) ) );
  volumeMenu->addMenu( scaleMenu );

  /* Volume > Mirror */
  QMenu *mirrorMenu = new QMenu( tr( "&Mirror" ), this );
  action = new QAction( tr( "O&x" ), this );
  action->setData( 0 );
  mirrorMenu->addAction( action );
  action = new QAction( tr( "O&y" ), this );
  action->setData( 1 );
  mirrorMenu->addAction( action );
  action = new QAction( tr( "O&z" ), this );
  action->setData( 2 );
  mirrorMenu->addAction( action );
  volumeMenu->addMenu( mirrorMenu );
  connect( mirrorMenu, SIGNAL( triggered(QAction*) ),
           this, SLOT( volumeMirror(QAction*) ) );

  /* Volume > Rotate */
  QMenu *rotateMenu = new QMenu( tr( "&Rotate" ), this );
  action = new QAction( tr( "90 degree around O&x" ), this );
  action->setData( 0 );
  rotateMenu->addAction( action );
  action = new QAction( tr( "90 degree around O&y" ), this );
  action->setData( 1 );
  rotateMenu->addAction( action );
  action = new QAction( tr( "90 degree around O&z" ), this );
  action->setData( 2 );
  rotateMenu->addAction( action );
  volumeMenu->addMenu( rotateMenu );
  connect( rotateMenu, SIGNAL( triggered(QAction*) ),
           this, SLOT( volumeRotate(QAction*) ) );

  /* Volume > Bounding box */
  QMenu *bboxMenu = new QMenu( tr( "&Bounding box" ), this );
  bboxMenu->addAction( volumeFitAction );
  bboxMenu->addAction( volumeFitFrameAction );
  bboxMenu->addAction( volumeAddBoundariesAction );
  volumeMenu->addMenu( bboxMenu );

  /* Volume > Tools */
  QMenu *toolsMenu = new QMenu( tr("&Tools"), this );
  toolsMenu->addAction( volumeClearAction );
  toolsMenu->addAction( volumeExtrudeAction );
  toolsMenu->addAction( volumeAddNoiseAction );
  toolsMenu->addAction( volumeRaiseLevelMapAction );
  toolsMenu->addAction( volumeDWTAction );
  toolsMenu->addAction( volumeDistanceMapAction );
  toolsMenu->addAction( volumeSkelAction );
  toolsMenu->addAction( volumeSpongifyAction );
  toolsMenu->addAction( volumeHollowOutAction );
  toolsMenu->addAction( volumeSurfaceFillAction );
  toolsMenu->addAction( volumeHoleFillAction );
  toolsMenu->addAction( volumeDemoPluginAction );
  volumeMenu->addMenu( toolsMenu );

  menuBar()->addMenu( volumeMenu );


#ifdef _IS_UNIX_

  _pluginsMenu = new QMenu( tr("&Plugins"), this );
  menuBar()->addMenu( _pluginsMenu );

  buildPluginsMenus();

#endif

  _toolBarsMenu = new QMenu( tr( "&Toolbars" ), this );
  menuBar()->addMenu( _toolBarsMenu );
  connect( _toolBarsMenu, SIGNAL( aboutToShow() ),
           this, SLOT( toolBarsMenuAboutToShow() ) );
  connect( _toolBarsMenu, SIGNAL( triggered(QAction*) ),
           this, SLOT( toolBarsMenuActivated(QAction*) ) );

  menuBar()->addSeparator();

  QMenu *helpMenu = new QMenu( tr( "&Help" ), this );
  helpMenu->addAction( helpAboutAction );
  helpMenu->addAction( helpAboutQtAction );
  menuBar()->addMenu( helpMenu );

  connect( helpAboutAction, SIGNAL( triggered() ),
           this, SLOT( helpAbout() ) );
  connect( helpAboutQtAction, SIGNAL( triggered() ),
           qApp, SLOT( aboutQt() ) );
  TRACE << "Menus OK\n";
}

void 
MainWindow::buildPluginsMenus()
{
  _pluginsMenu->clear();
  while ( ! _plugins.isEmpty() ) {
    delete _plugins.takeFirst();
  }
  while ( ! _pluginsXmlDocs.isEmpty() ) {
    delete _pluginsXmlDocs.takeFirst();
  }

  QString sharePath = QFileInfo( qApp->arguments()[0] ).absolutePath() + "/../share/qvox";
  QDir shareDir( sharePath );
  QStringList flist = shareDir.entryList( QStringList(QString("*.qpx")), QDir::NoFilter, QDir::Name );
  while ( !flist.isEmpty() ) {
    QString fname = sharePath + "/" + flist.front();
    if ( !globalSettings->pluginsFiles().contains( fname ) ) {
      globalSettings->pluginsFiles().push_back( fname  );
    }
    flist.pop_front();
  }

  QAction * managePluginsAction = new QAction( "Manage plugins...", this );
  connect( managePluginsAction, SIGNAL( triggered() ),
           _dialogSettings, SLOT( managePlugins() ) );

  _pluginsMenu->addAction( managePluginsAction );
  _pluginsMenu->addSeparator();

  QStringList::iterator istr = globalSettings->pluginsFiles().begin();
  QStringList::iterator estr = globalSettings->pluginsFiles().end();
  while ( istr != estr ) {
    QString fileName = *istr;
    TRACE << "Loading plugin file: "
          << fileName.toLatin1().constData() << std::endl;
    QFile pluginFile( fileName );
    if ( pluginFile.exists() )
      pluginFile.open( QIODevice::ReadOnly );
    if ( pluginFile.isOpen() ) {
      QDomDocument * dom = new QDomDocument;
      _pluginsXmlDocs.push_back( dom );
      if ( dom->setContent( &pluginFile ) ) {
        QDomElement domE = dom->elementsByTagName("document").at(0).toElement();
        QString name = domE.attributes().namedItem( "name" ).nodeValue();
        QString binPath = domE.attributes().namedItem( "bin-path" ).nodeValue();
        QString libPath = domE.attributes().namedItem( "lib-path" ).nodeValue();
        QString workingDir = domE.attributes().namedItem( "working-dir" ).nodeValue();
        QDomNode node = domE.firstChild();
        if ( name.isEmpty() )
          name = "NoName";
        _pluginsMenu->addMenu( buildPluginsMenu( node, name,
                                                 QFileInfo( fileName ).absolutePath(),
                                                 binPath,
                                                 libPath,
                                                 workingDir ) );
        pluginFile.close();
      }
    }
    ++istr;
  }

  {
    // Personnal plugins
    QFile file( QString("%1/.qvox_plugins.qpx").arg( std::getenv("HOME") ) );
    if ( file.exists() && file.open( QIODevice::ReadOnly ) ) {
      QDomDocument * dom = new QDomDocument;
      _pluginsXmlDocs.push_back( dom );
      if ( dom->setContent( &file ) ) {
        TRACE << "Loading plugin file: "
              << file.fileName().toLatin1().constData() << std::endl;
        QDomElement domE = dom->elementsByTagName("document").at(0).toElement();
        QDomNode node = domE.firstChild();
        QString binPath = domE.attributes().namedItem( "bin-path" ).nodeValue();
        QString libPath = domE.attributes().namedItem( "lib-path" ).nodeValue();
        QString workingDir = domE.attributes().namedItem( "working-dir" ).nodeValue();
        _pluginsMenu->addMenu( buildPluginsMenu( node, "&Personnal plugins",
                                                 QFileInfo( file ).absolutePath(),
                                                 binPath,
                                                 libPath,
                                                 workingDir ) );
        file.close();
      }
    } else {
      QMenu * menu = new QMenu( "&Personnal plugins" );
      menu->setEnabled( false );
      _pluginsMenu->addMenu( menu );
    }
  }


}

QMenu *
MainWindow::buildPluginsMenu( QDomNode node, QString title,
                              const QString & xmlPath,
                              const QString & binPath,
                              const QString & libPath,
                              const QString & workingDir )
{
  static UInt pluginID = 0;
  QMenu * menu = new QMenu( title );
  while ( !node.isNull() ) {
    QDomElement e = node.toElement();
    QString tag = e.tagName();
    if ( tag == "action" ) {
      QAction * action = new QAction( e.attributes().namedItem( "name" ).nodeValue(), this );
      PluginDialog * p = PluginDialog::createPluginDialog( this, e, _document, xmlPath, binPath, libPath, workingDir );
      if ( p ) {
        _plugins.push_back( p  );
        if ( _plugins.back()->paramsCount() > 0 ) {
          action->setText( action->text() + "..." );
        }
        action->setData( pluginID++ );
        menu->addAction( action );
        connect( action, SIGNAL( triggered(bool) ),
                 p, SLOT( execute() ) );
      }
    } else if ( tag == "submenu" ) {
      QMenu * sub = buildPluginsMenu( e.firstChild(),
                                      e.attributes().namedItem( "name" ).nodeValue(),
                                      xmlPath,
                                      binPath,
                                      libPath,
                                      workingDir );
      menu->addMenu( sub  );
    } else if ( tag == "include" ) {
      TRACE << "Include()\n";
      QString filename = e.attributes().namedItem( "file" ).nodeValue();
      if ( !filename.startsWith("/") )
        filename = xmlPath + "/" + filename;
      QFileInfo fi( filename );
      QFile pluginFile( fi.absoluteFilePath() );

      QString newBinPath = e.attributes().namedItem( "bin-path" ).nodeValue();
      QString newLibPath = e.attributes().namedItem( "lib-path" ).nodeValue();
      QString newWorkingDir = e.attributes().namedItem( "working-dir" ).nodeValue();
      if ( newBinPath.isEmpty() ) newBinPath = binPath;
      if ( newLibPath.isEmpty() ) newLibPath = libPath;
      if ( newWorkingDir.isEmpty() ) newWorkingDir = workingDir;

      if ( pluginFile.exists() )
        pluginFile.open( QIODevice::ReadOnly );
      if ( pluginFile.isOpen() ) {
        TRACE << "  File: "
              << fi.absoluteFilePath().toLatin1().constData()
              << std::endl;
        QDomDocument * dom = new QDomDocument;
        _pluginsXmlDocs.push_back( dom );
        QString errMsg;
        int errLine;
        if ( dom->setContent( pluginFile.readAll(), false, &errMsg, &errLine, 0 ) ) {
          QDomElement domE = dom->elementsByTagName("document").at(0).toElement();
          QString name = domE.attributes().namedItem( "name" ).nodeValue();
          menu->addMenu( buildPluginsMenu( domE.firstChild(), name, fi.absolutePath(),
                                           newBinPath, newLibPath, newWorkingDir  ) );
          pluginFile.close();
        } else {
          ERROR << "Reading XML file ("
                << fi.absoluteFilePath().toLatin1().constData() << ")\n";
          ERROR << errMsg.toLatin1().constData() << "\n";
        }
      }
    }
    node = node.nextSibling();
  }
  return menu;
}

void
MainWindow::createToolBars()
{
  /*
    * Tools
    */
  _toolsToolBar = new ToolsToolBar( this );
  _toolBars.push_back( _toolsToolBar );
  _toolsToolBar->addAction( fileNewAction );
  _toolsToolBar->addAction( fileOpenAction );
  _toolsToolBar->addAction( fileSaveAction );
  _toolsToolBar->addAction( fileSaveAsAction );
  _toolsToolBar->addAction( settingsAction );
  _toolsToolBar->addAction( zoomInAction );
  _toolsToolBar->addAction( zoomOutAction );
  _toolsToolBar->addAction( zoomFitAction );
  _toolsToolBar->addAction( zoomOneAction );
  _toolsToolBar->addAction( viewExportAction );
  _toolsToolBar->addAction( moveLightAction );
  _toolsToolBar->addAction( show2DViewAction );
  _toolsToolBar->addAction( cutPlaneAction );
  _toolsToolBar->addAction( bgColorAction );

#ifdef HAVE_EXPERIMENTAL
  _toolsToolBar->addAction( showExperimentsAction );
#endif // HAVE_EXPERIMENTAL

  QWidget *spacer = new QWidget;
  spacer->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
  spacer->setMinimumWidth( 50 );
  _toolsToolBar->addWidget( spacer );

  _toolsToolBar->addSeparator();

  _toolsToolBar->addAction( fileExitAction );

  QLabel *label = new QLabel;
  label->setPixmap( QPixmap( ":/toolbars/Pixmaps/qvoxtoolbar.png" ) );
  _toolsToolBar->addWidget( label );
  addToolBar( _toolsToolBar );

  /*
    * Colormap
    */
  _colorMapToolBar = new ColorMapToolBar( this, &(_view3DWidget->colorMap()));
  _toolBars.push_back( _colorMapToolBar );
  _colorMapToolBar->setAllowedAreas( Qt::LeftToolBarArea | Qt::RightToolBarArea );
  addToolBar( Qt::RightToolBarArea, _colorMapToolBar );

  /*
    * View parameters
    */
  _viewParamsToolBar = new ViewParamsToolBar( this, _view3DWidget );
  _toolBars.push_back( _viewParamsToolBar );
  _viewParamsToolBar->setAllowedAreas( Qt::TopToolBarArea | Qt::BottomToolBarArea );
  addToolBar( Qt::BottomToolBarArea, _viewParamsToolBar );

  /*
    * Actions
    */
  _actionsToolBar = new ActionsToolBar( this, _view3DWidget );
  _toolBars.push_back( _actionsToolBar );
  _actionsToolBar->setAllowedAreas( Qt::TopToolBarArea | Qt::BottomToolBarArea );
  addToolBar( Qt::BottomToolBarArea, _actionsToolBar );

  QActionGroup *actionsGroup = new QActionGroup( this );
  actionsGroup->addAction( drawVoxelAction );
  actionsGroup->addAction( drawSimpleVoxelAction );
  actionsGroup->addAction( eraseVoxelAction );
  actionsGroup->addAction( eraseSimpleVoxelAction );
  actionsGroup->addAction( drawSurfelAction );
  actionsGroup->addAction( eraseSurfelAction );
  actionsGroup->addAction( moveAction );
  actionsGroup->addAction( showInfoAction );

  _actionsToolBar->addAction( moveAction );
  moveAction->setChecked( true );
  _actionsToolBar->addAction( showInfoAction );
  _actionsToolBar->addAction( drawVoxelAction );
  _actionsToolBar->addAction( drawSimpleVoxelAction );
  _actionsToolBar->addAction( eraseVoxelAction );
  _actionsToolBar->addAction( eraseSimpleVoxelAction );
  _actionsToolBar->addAction( drawSurfelAction );
  _actionsToolBar->addAction( eraseSurfelAction );
  actionsGroup->setExclusive( true );

  connect( actionsGroup, SIGNAL( triggered( QAction * ) ),
           this, SLOT( actionSelected( QAction * ) ) );

  _actionsToolBar->addSeparator();

  _actionsToolBar->addWidget( new QLabel( "Adj. pair" , 0 ) );
  QComboBox *comboAdjacency = new QComboBox( _actionsToolBar );
  comboAdjacency->insertItem( 0, "(6,26)");
  comboAdjacency->insertItem( 1, "(6,18)" );
  comboAdjacency->insertItem( 2, "(18,6)" );
  comboAdjacency->insertItem( 3, "(26,6)" );
  comboAdjacency->setCurrentIndex( globalSettings->adjacencyPair() );
  connect( comboAdjacency, SIGNAL( activated(int) ),
           globalSettings, SLOT( adjacencyPair(int) ) );
  connect( globalSettings, SIGNAL( adjacencyPairChanged(int) ),
           comboAdjacency, SLOT( setCurrentIndex(int) ) );

  _actionsToolBar->addWidget( comboAdjacency );

  _actionsToolBar->addSeparator();

  _actionsToolBar->addWidget( new QLabel(" Draw radius: ", 0 ) );
  QSpinBox *sbDrawRadius = new QSpinBox( _actionsToolBar );
  sbDrawRadius->setRange( 1, 65535 );
  sbDrawRadius->setValue( 1 );
  connect( sbDrawRadius, SIGNAL( valueChanged(int) ),
           _view3DWidget, SLOT( drawRadius(int) ) );
  connect( sbDrawRadius, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( drawRadius(int) ) );
  _view3DWidget->drawRadius( 4 );
  sbDrawRadius->setValue( globalSettings->drawRadius() );
  _actionsToolBar->addWidget( sbDrawRadius );

#ifdef HAVE_EXPERIMENTAL
  /*
    * Animation
    */
  _animationToolBar = new AnimationToolBar( this );
  _toolBars.push_back( _animationToolBar );
  addToolBar( Qt::TopToolBarArea, _animationToolBar );
  _view3DWidget->setAnimationToolBar( _animationToolBar );
  _animationToolBar->setView3DWidget( _view3DWidget );

  /*
    * Experimental
    */
  _dialogExperiments = new DialogExperiments( this );
  _dialogExperiments->view3DWidget( _view3DWidget );
  connect( showExperimentsAction, SIGNAL( triggered(bool) ),
           this, SLOT( showExperiments(bool) ) );
#else
  _dialogExperiments = 0;
  _experimentToolBar = 0;
  _animationToolBar = 0;
  _view3DWidget->setAnimationToolBar( _animationToolBar );
#endif

  connect( cutPlaneAction, SIGNAL(triggered(bool)),
           this, SLOT(cutPlane(bool)));
}

void
MainWindow::createDialogs()
{
  _dialog2DView = new Dialog2DView( this );
  _dialog2DView->document( &_document );
  _dialogs = new Dialogs( this );
  _dialogAbout = new DialogAbout( this );
  _dialogSettings = new DialogSettings( this );
  _dialogInputSize = new DialogInputSize( this );
  _dialogInputSize->document( &_document );
  _dialogSkel = new DialogSkel( this );
  _dialogSkel->setView3DWidget( _view3DWidget );
  _dialogSkel->document( &_document );
  _dialogAspect = new DialogAspect( this );
  _dialogAspect->setView3DWidget( _view3DWidget );
  _dialogMerge = new DialogMerge( this );
  _dialogMerge->dialogs( _dialogs );
  _dialogThresholds = new DialogThresholds( this );
  _dialogThresholds->setDocument( &_document );
  _dialogThresholds->setSurface( & _view3DWidget->surface() );
}

void
MainWindow::shadingMenuSelected( QAction * action )
{
  int i = action->data().toInt();
  globalSettings->shading( i );
}

void
MainWindow::fileNew()
{
  if ( ! confirmNoSave() ) return;

  if ( _dialogInputSize->execNew() == QDialog::Accepted ) {
    _document.create( _dialogInputSize->width(),
                      _dialogInputSize->height(),
                      _dialogInputSize->depth(),
                      _dialogInputSize->bands(),
                      _dialogInputSize->valueType() );
    _document.notify();
  } else {
    _dialogInputSize->changeVolumeInfos();
  }
}

void
MainWindow::fileOpen()
{
  if ( ! confirmNoSave() ) return;
  QString s =  _dialogs->askOpenFileName( VolumeFile );
  repaint();
  if ( ! s.isEmpty() )
    openFile( s );
  return;
}

void
MainWindow::fileSave()
{
  if ( _document.fileName().isEmpty() ) {
    fileSaveAs();
  } else {
    _document.save();
    updateCaption();
  }
}

void
MainWindow::fileSaveAs()
{
  QString s =  _dialogs->askSaveFileName( VolumeFile );
  if ( s.isNull() ) return;
  _document.saveAs( s.toLatin1() );
  updateCaption();
}

void
MainWindow::filePrint()
{

}

void
MainWindow::fileExit()
{
  if ( ! confirmNoSave() ) return;
  _view3DWidget->animate( false );
  _view3DWidget->animateAspect( false );
  globalSettings->activeToolBarsList().clear();

  for ( int i = 0 ; i <  _toolBars.count(); i++ )
    if ( ! _toolBars.at( i )->isHidden() )
      globalSettings->activeToolBarsList().append( _toolBars.at( i )->windowTitle() );

  _application->closeAllWindows();
}

void
MainWindow::setApplication( QApplication *application)
{
  _application = application;
  _view3DWidget->setApplication( _application );
}

#ifdef HAVE_EXPERIMENTAL
void
MainWindow::showExperiments( bool on)
{
  if (on) _dialogExperiments->show();
  else _dialogExperiments->done(0);
}
#endif 

void
MainWindow::show2DView()
{
  show2DViewAction->setEnabled( false );
  if ( _dialog2DView->showSlice() ) {
    _view3DWidget->view2DImage( _dialog2DView->viewWidget()->image() );
  }
  else {
    _view3DWidget->view2DImage( 0 );
  }
  _view3DWidget->highlightSlice( true );
  _dialog2DView->show();
}

Document &
MainWindow::document()
{
  return _document;
}

Dialogs *
MainWindow::dialogs()
{
  return _dialogs;
}

void
MainWindow::connections()
{
  connect( qApp, SIGNAL( lastWindowClosed() ),
           qApp, SLOT( quit() ) );

  connect( fileExitAction, SIGNAL( triggered() ),
           this, SLOT( fileExit() ) );
  connect( fileImportAction, SIGNAL( triggered() ),
           this, SLOT( fileImport() ) );
  connect( fileMergeAction, SIGNAL( triggered() ),
           this, SLOT( fileMerge() ) );
  connect( fileNewAction, SIGNAL( triggered() ),
           this, SLOT( fileNew() ) );
  connect( fileOpenAction, SIGNAL( triggered() ),
           this, SLOT( fileOpen() ) );
  connect( fileSaveAction, SIGNAL( triggered() ),
           this, SLOT( fileSave() ) );
  connect( fileSaveAsAction, SIGNAL( triggered() ),
           this, SLOT( fileSaveAs() ) );
  connect( moveLightAction, SIGNAL( toggled(bool) ),
           this, SLOT( moveLight(bool) ) );
  connect( show2DViewAction, SIGNAL( triggered() ),
           this, SLOT( show2DView() ) );
  connect( surfaceColorizeBorderAction, SIGNAL( triggered() ),
           this, SLOT( surfaceColorizeBorder() ) );
  connect( surfaceExportAction, SIGNAL( triggered() ),
           this, SLOT( surfaceExport() ) );
  connect( volumeClampAction, SIGNAL( triggered() ),
           this, SLOT( volumeClamp() ) );
  connect( viewAspectAction, SIGNAL( triggered() ),
           this, SLOT( viewAspect() ) );
  connect( viewExportAction, SIGNAL( triggered() ),
           this, SLOT( viewExport() ) );
  connect( viewLoadParamsAction, SIGNAL( triggered() ),
           this, SLOT( viewLoadParameters() ) );
  connect( viewSaveParamsAction, SIGNAL( triggered() ),
           this, SLOT( viewSaveParameters() ) );
  connect( volumeAddBoundariesAction, SIGNAL( triggered() ),
           this, SLOT( volumeAddBoundaries() ) );
  connect( volumeBinarizeAction, SIGNAL( triggered() ),
           this, SLOT( volumeBinarize() ) );
  connect( volumeAddNoiseAction, SIGNAL( triggered() ),
           this, SLOT( volumeAddNoise() ) );
  connect( volumeClearAction, SIGNAL( triggered() ),
           this, SLOT( volumeClear() ) );
  connect( volumeColorHistogramAction, SIGNAL( triggered() ),
           this, SLOT( volumeColorHistogram() ) );
  connect( volumeDWTAction, SIGNAL( triggered() ),
           this, SLOT( volumeDWT() ) );
  connect( volumeDistanceMapAction, SIGNAL( triggered() ),
           this, SLOT( volumeDistanceMap() ) );
  connect( volumeExtrudeAction, SIGNAL( triggered() ),
           this, SLOT( volumeExtrude() ) );
  connect( volumeFitAction, SIGNAL( triggered() ),
           this, SLOT( volumeFit() ) );
  connect( volumeFitFrameAction, SIGNAL( triggered() ),
           this, SLOT( volumeFitFrame() ) );
  connect( volumeHollowOutAction, SIGNAL( triggered() ),
           this, SLOT( volumeHollowOut() ) );
  connect( volumeSurfaceFillAction, SIGNAL( triggered() ),
           this, SLOT( volumeSurfaceFill() ) );
  connect( volumeHoleFillAction, SIGNAL( triggered() ),
           this, SLOT( volumeHoleFill() ) );
  connect( volumeNegativeAction, SIGNAL( triggered() ),
           this, SLOT( volumeNegative() ) );
  connect( volumeShadeFromHueChannelAction, SIGNAL( triggered() ),
           this, SLOT( volumeGrayShadeFromHue() ) );
  connect( volumeShadeFromSaturationChannelAction, SIGNAL( triggered() ),
           this, SLOT( volumeGrayShadeFromSaturation() ) );
  connect( volumeQuantizeAction, SIGNAL( triggered() ),
           this, SLOT( volumeQuantize() ) );
  connect( volumeRaiseLevelMapAction, SIGNAL( triggered() ),
           this, SLOT( volumeRaiseLevelMap() ) );
  connect( volumeSkelAction, SIGNAL( triggered() ),
           this, SLOT( showSkelDialog() ) );
  connect( volumeSpongifyAction, SIGNAL( triggered() ),
           this, SLOT( volumeSpongify() ) );
  connect( volumeSubSampleAction, SIGNAL( triggered() ),
           this, SLOT( volumeSubSample() ) );
  connect( volumeToColorAction, SIGNAL( triggered() ),
           this, SLOT( volumeToColor() ) );
  connect( volumeToGrayAction, SIGNAL( triggered() ),
           this, SLOT( volumeToGray() ) );
  connect( volumeDemoPluginAction, SIGNAL( triggered() ),
           this, SLOT( volumeDemoPlugin() ) );

  connect( _dialog2DView, SIGNAL( closing() ),
           this, SLOT( view2DClosing() ) );
  connect( _dialog2DView, SIGNAL( trim(int, int ) ),
           this, SLOT( volumeTrim(int,int) ) );
  connect( _dialog2DView->viewWidget(), SIGNAL( depthChanged( int, int ) ),
           _view3DWidget, SLOT( highlightSlice( int, int ) ) );
  connect( _dialog2DView->viewWidget(), SIGNAL( writeViewRequest() ),
           _view3DWidget, SLOT( writeView() ) );
  connect( _dialog2DView , SIGNAL( askForDisplayIn3D(bool ) ),
           this, SLOT( changeDisplay2DIn3D(bool ) ) );

  connect( _dialogSkel, SIGNAL( closing() ),
           this, SLOT( dialogSkelClosing() ) );

  connect( & (_view3DWidget->colorMap()), SIGNAL( colorSelected(int) ),
           this, SLOT( selectedColorChanged( int ) ) ) ;

  connect( _view3DWidget, SIGNAL( info( const QString & ) ),
           statusBar(), SLOT( showMessage( const QString & ) ) ) ;

  connect( _view3DWidget, SIGNAL( clearInfo()) ,
           statusBar(), SLOT( clearMessage() ) ) ;

  connect( _viewParamsToolBar, SIGNAL( info( const QString & ) ),
           statusBar(), SLOT( showMessage( const QString & ) ) ) ;

  connect( globalSettings, SIGNAL( shadingChanged(int ) ),
           this, SLOT( shadingChanged(int ) ) );
  connect( globalSettings, SIGNAL( opacityChanged(int ) ),
           this, SLOT( opacityChanged(int ) ) );

  connect( zoomOutAction, SIGNAL(triggered(bool)),
           _view3DWidget, SLOT(zoomOut()) );
  connect( zoomInAction, SIGNAL(triggered(bool)),
           _view3DWidget, SLOT(zoomIn()) );
  connect( zoomOneAction, SIGNAL(triggered(bool)),
           _view3DWidget, SLOT(zoomOne()) );
  connect( zoomFitAction, SIGNAL(triggered(bool)),
           _view3DWidget, SLOT(zoomFit()) );
  connect( bgColorAction, SIGNAL(triggered(bool)),
           _view3DWidget, SLOT( changeBackground() ) );
  connect( settingsAction, SIGNAL( triggered(bool) ),
           this, SLOT( showSettings() ) );

  connect( showEdgesAction, SIGNAL( toggled(bool ) ),
           _view3DWidget, SLOT( edges(bool ) ) );

  connect( showAxisAction, SIGNAL( toggled(bool ) ),
           this, SLOT( drawAxis(bool ) ) );

  connect( showColorMapAction, SIGNAL( toggled(bool) ),
           _view3DWidget, SLOT( showColorMap(bool) ) );

  connect( globalSettings->fullVolumeBoxAction(), SIGNAL( triggered(bool) ),
           _view3DWidget, SLOT( fullVolume(bool) ) );

  connect( &_document, SIGNAL( volumeChanged() ),
           this, SLOT( volumeChanged() ) );

  connect( &_document, SIGNAL( info( const QString &, int  ) ),
           statusBar(), SLOT( showMessage( const QString &, int ) ) ) ;

  connect( &_document, SIGNAL( runningPlugin(PluginDialog* ) ),
           this, SLOT( runningPlugin(PluginDialog* ) ) );

  connect( _colorMapToolBar, SIGNAL(colorLUTFileSelected(QString )),
           this, SLOT(colorLUTSelected(QString)));

  connect( _dialogSettings, SIGNAL(accepted()),
           this, SLOT(dialogSettingsClosed()) );

  connect( _colorMapToolBar, SIGNAL(aboutToHide()),
           _colorMapView, SLOT(show()) );

  connect( _colorMapToolBar, SIGNAL(aboutToShow()),
           _colorMapView, SLOT(hide()) );

#ifdef HAVE_EXPERIMENTAL
  connect( _dialogExperiments, SIGNAL( askFullVolume() ),
           globalSettings->fullVolumeBoxAction(), SLOT( trigger() ) );
#endif

}

void
MainWindow::show()
{
  QMainWindow::show();

  for ( int i = 0 ; i < _toolBars.count(); i++ ) {
    if ( globalSettings->activeToolBarsList().contains( _toolBars.at( i )->windowTitle() ) )
      _toolBars.at( i )->show();
    else
      _toolBars.at( i )->hide();
  }

  bool logo = false;
  _view3DWidget->clear();

  if ( !_fileName.isEmpty() ) {
    globalProgressReceiver.showMessage("Loading volume...");
    if ( _document.loadablePixmap( _fileName.toLatin1() ) )
      _document.loadPixmap( _fileName.toLatin1() );
    else
      if ( !_document.load( _fileName.toLatin1() ) ) {
        _dialogs->errorOpening( _fileName.toLatin1() );
        _document.qvoxLogo();
        logo = true;
        _fileName = QString();
      }
    updateCaption();
    globalProgressReceiver.showMessage(0);
  } else  {
    _document.qvoxLogo();
    logo = true;
    _fileName = QString();
  }

  if ( _fileName == QString("-") ) {
    setFileName( QString() );
    _document.fileName( QString() );
    updateCaption();
  }

  if ( _dumpVolume ) {
    _document.image()->save( "-" );
  }

  volumeChanged();

  if ( logo )
    _view3DWidget->logo();
  moveLightAction->setEnabled( _view3DWidget->surfaceShading() == Surface::ColorFromLight ||
                               _view3DWidget->surfaceShading() == Surface::ColorFromLightedMaterial );
}

void
MainWindow::setFileName( const QString & fileName )
{
  _fileName = fileName;
}

void
MainWindow::helpAbout()
{
  _dialogAbout->show();
}

void
MainWindow::toolBarsMenuAboutToShow()
{
  _toolBarsMenu->clear();

  QAction * action = new QAction( tr("Show all"), _toolBarsMenu );
  action->setData( 0 );
  _toolBarsMenu->addAction( action );
  action = new QAction( tr("Hide all"), _toolBarsMenu );
  action->setData( 1 );
  _toolBarsMenu->addAction( action );

  _toolBarsMenu->addSeparator();

  for ( int i = 0 ; i < _toolBars.count(); ++i ) {
    action = new QAction( _toolBars.at( i )->windowTitle(), _toolBarsMenu );
    action->setData( 2 + i );
    action->setCheckable( true );
    _toolBarsMenu->addAction( action );
    if ( ! _toolBars.at( i )->isHidden() ) {
      action->setChecked( true );
    }
  }
}

void MainWindow::toolBarsMenuActivated( QAction *action )
{
  int n = _toolBars.count();
  int i = action->data().toInt();
  switch ( i ) {
  case 0:
    for ( int i = 0; i < n; i++ )
      _toolBars.at( i )->show();
    break;
  case 1:
    for ( int i = 0; i < n; i++ )
      _toolBars.at( i )->hide();
    break;
  }

  if ( i > 1 ) {
    bool shown = ! _toolBars.at( i - 2)->isHidden();
    if ( shown )
      _toolBars.at( i - 2)->hide();
    else
      _toolBars.at( i - 2)->show();
  }
}

void
MainWindow::closeEvent( QCloseEvent *e )
{
  if ( ! confirmNoSave() ) {
    e->ignore();
    return;
  }
  globalSettings->activeToolBarsList().clear();
  for ( int i = 0 ; i < _toolBars.count(); i++ ) {
    if ( ! _toolBars.at( i )->isHidden() ) {
      globalSettings->activeToolBarsList().append( _toolBars.at( i )->windowTitle() );
    }
  }
  if ( _view3DWidget->virtualPointer() ) {
    _view3DWidget->virtualPointer()->terminate();
    _view3DWidget->virtualPointer()->waitForFinished( -1 );
  }

  QMainWindow::closeEvent( e );
}

void
MainWindow::showSettings()
{
  _dialogSettings->show();
}


void
MainWindow::viewAspect()
{
  _dialogAspect->show();
}

void
MainWindow::viewLoadParameters()
{
  QString s =  _dialogs->askOpenFileName( ViewParametersFile );
  if ( ! s.isEmpty() ) {
    _view3DWidget->surface().loadViewParams( s.toLatin1().constData() );
    _view3DWidget->forceRedraw( true );
  }
}

void
MainWindow::viewSaveParameters()
{
  QString s =  _dialogs->askSaveFileName( ViewParametersFile );
  if ( s.isNull() ) return;
  _view3DWidget->surface().saveViewParams( s.toLatin1() );
  _view3DWidget->repaint();
}

void
MainWindow::selectedColorChanged( int n )
{  
  if ( _document.bands() == 1 ) {
    QString text = QString("&Binarize [%1]").arg( n );
    volumeBinarizeAction->setText( text );
  } else {
    QString text = QString("&Binarize [%1,%2,%3]")
        .arg( _view3DWidget->colorMap().qcolor(n).red() )
        .arg( _view3DWidget->colorMap().qcolor(n).green() )
        .arg( _view3DWidget->colorMap().qcolor(n).blue() );
    volumeBinarizeAction->setText( text );
  }
}

void
MainWindow::volumeColorHistogram()
{
  if ( ! confirmNoSave() ) { return; }

  globalProgressReceiver.showMessage("Building histogram volume...");
  globalSettings->shading( Surface::ColorFromMaterial );
  _document.histo3D();
  globalProgressReceiver.showMessage(0);
}

void
MainWindow::fileImport()
{
  if ( ! confirmNoSave() ) { return; }

  QList<QByteArray> formats = QImageReader::supportedImageFormats();
  QList<QByteArray>::const_iterator it = formats.begin(), end = formats.end();
  QString fileName;
  QString filter = "2D Pixmaps (";
  while ( it != end ) {
    filter += QString("*.") + it->toLower() + " ";
    if ( it->toLower() == "jpeg" )
      filter += QString("*.jpg ");
    ++it;
  }
  filter.truncate( filter.length() - 1 );
  filter += ");;Surface mesh (*.off *.obj)";

  fileName = QFileDialog::getOpenFileName( this,
                                           "Choose an image file to open",
                                           QString(),
                                           filter );
  if ( ! fileName.isNull() ) {
    bool ok = false;
    if ( fileName.endsWith(".off",Qt::CaseInsensitive)
         || fileName.endsWith(".obj",Qt::CaseInsensitive)  ) {
      int ans = QMessageBox::question( this,
                                       "Mesh import",
                                       "Should closed surfaces be filled?",
                                       QMessageBox::Yes | QMessageBox::Default,
                                       QMessageBox::No );
      globalProgressReceiver.showMessage("Importing file...");
      ok = _document.importMesh( fileName, ans == QMessageBox::Yes );
    } else {
      globalProgressReceiver.showMessage("Importing file...");
      ok = _document.loadPixmap( fileName.toLatin1() );
    }
    if ( !ok )
      _dialogs->errorOpening( _fileName.toLatin1() );
    else
      globalSettings->shading( Surface::ColorFromMaterial );
    globalProgressReceiver.showMessage(0);
    updateCaption();
  }
}

void
MainWindow::volumeFit()
{
  _document.fitBoundingBox();
}

void
MainWindow::volumeFitFrame()
{
  bool ok = false;
  int thickness = IntegerDialog::ask( this, 1, 255, ok, globalSettings->fitBoundingBoxFrameSize() );
  if ( ok )
    _document.fitBoundingBox( thickness );
}

void
MainWindow::volumeRaiseLevelMap()
{
  if ( ! confirmNoSave() ) { return; }
  globalProgressReceiver.showMessage("Raising level map...");
  _document.raiseLevelMap( globalSettings->levelMapMaxHeight() );
  updateCaption();
}

void
MainWindow::viewExport()
{
  QString s =  _dialogs->askSaveFileName( ViewExportFile );
  if ( s.isNull() ) return;

  QString filter = _dialogs->selectedFilter();
  char filterString[1024];

  // No extension given, get it from the filter
  if ( QFileInfo( s ).suffix().isEmpty() ) {
    // Get first extension of the filter
    strcpy( filterString, filter.toLatin1().constData() );
    char filterExt[255];
    char * pc = strstr( filterString, "(*." ) + 2;
    int i;
    for ( i = 0; i < 255 && pc[i] && ( pc[i] == '.' || QChar( pc[i] ).isLetterOrNumber() ); i++ )
      filterExt[i] = pc[i];
    filterExt[i] = '\0';

    s += QString(filterExt).toLower();

    if ( filter.contains( "bitmap file" ) ) {
      _view3DWidget->writeView( s.toLatin1() );
      return;
    }

    SurfaceExporter exporter( _view3DWidget->surface(),
                              _view3DWidget->colorMap() );
    if ( filter.startsWith( "Encapsulated" ) )
      exporter.writeEPS( s.toLatin1(), globalSettings->drawAxis() );
    if ( filter.startsWith( "XFig" ) )
      exporter.writeFIG( s.toLatin1() );
    if ( filter.startsWith( "SVG" ) )
      exporter.writeSVG( s.toLatin1() );
    if ( filter.startsWith( "Smoothed surface" ) ) {
      if ( filter.contains( "Gouraud" ) )
        exporter.writeSmoothedEPS( globalSettings->exportOFFIterations(), 2, s.toLatin1() );
      else
        exporter.writeSmoothedEPS( globalSettings->exportOFFIterations(), 0, s.toLatin1() );
    }
    return;
  }

  // Extension given, handle it.
  QString extension = QFileInfo( s ).suffix();

  // Look for known bitmap extensions
  QList<QByteArray> formats = QImageWriter::supportedImageFormats();
  QList<QByteArray>::const_iterator it = formats.begin(), end = formats.end();
  while ( it != end  ) {
    if ( QString( *it ).toLower() == extension ) {
      _view3DWidget->writeView( s.toLatin1() );
      return;
    }
    ++it;
  }

  SurfaceExporter exporter( _view3DWidget->surface(),
                            _view3DWidget->colorMap() );
  if ( s.endsWith( ".eps" ) ) {
    exporter.writeEPS( s.toLatin1(), globalSettings->drawAxis() );
    return;
  }
  if ( s.endsWith( ".fig") ) {
    exporter.writeFIG( s.toLatin1() );
    return;
  }
  if ( s.endsWith( ".svg") ) {
    exporter.writeSVG( s.toLatin1() );
    return;
  }
  if ( filter.endsWith( ".off" ) ) {
    exporter.writeSmoothedEPS( globalSettings->exportOFFIterations(), 0, s.toLatin1() );
    return;
  }
}

void
MainWindow::surfaceExport()
{
  QString s =  _dialogs->askSaveFileName( SurfaceExportFile );
  _application->processEvents();

  if ( s.isNull() ) return;

  QString filter = _dialogs->selectedFilter();
  SurfaceExporter exporter( _view3DWidget->surface(),
                            _view3DWidget->colorMap() );

  if ( filter.startsWith( "Geomview" ) ) {
    globalProgressReceiver.showMessage("Export Geomview OFF file...");
    exporter.writeOFF( s.toLatin1(),
                       globalSettings->exportOFFIterations(),
                       globalSettings->exportOFFAdaptive(),
                       !globalSettings->exportOFFsurfelsColors(),
                       globalSettings->exportOFFColor().red() / 255.0,
                       globalSettings->exportOFFColor().green() / 255.0,
                       globalSettings->exportOFFColor().blue() / 255.0 );
  }
  if ( filter.startsWith( "Surfels as Geomview" ) ) {
    globalProgressReceiver.showMessage("Export Geomview OFF...");
    exporter.writeSurfelsOFF( s.toLatin1() );
  }
  if ( filter.startsWith( "Wavefront" ) ) {
    globalProgressReceiver.showMessage( "Export Wavefront OBJ...");
    exporter.writeOBJ( s.toLatin1(),
                       globalSettings->exportOFFIterations() );
  }
}

void
MainWindow::volumeMirror( QAction *action )
{
  int id = action->data().toInt();
  _application->processEvents();
  globalProgressReceiver.showMessage("Mirroring...");
  _document.mirror( id );
  globalProgressReceiver.showMessage(0);
}

void
MainWindow::volumeRotate( QAction *action )
{
  int id = action->data().toInt();
  _application->processEvents();
  globalProgressReceiver.showMessage("Rotating...");
  _document.rotate( id );
  globalProgressReceiver.showMessage(0);
}

void
MainWindow::volumeBinarize()
{
  if ( _document.image()->bands() == 3 ) {
    QColor color = _view3DWidget->colorMap().selectedQColor();
    _document.binarize( color.red(), color.green(), color.blue() );
  } else {
    _document.binarize( _view3DWidget->colorMap().selectedColor() );
  }
  if ( _view3DWidget->surfaceShading() == Surface::ColorFromMaterial ||
       _view3DWidget->surfaceShading() == Surface::ColorFromLightedMaterial )
    _view3DWidget->forceRedraw( true );
}

void
MainWindow::volumeNegative()
{
  _application->processEvents();
  _document.negative();
}

void MainWindow::volumeGrayShadeFromHue()
{
  _document.grayShadeFromHue();
}

void MainWindow::volumeGrayShadeFromSaturation()
{
  _document.grayShadeFromSaturation();
}

void
MainWindow::volumeChanged()
{
  TRACE << "volumeChanged()\n";

  if ( ! _document.image()->isEmpty() ) {
    if ( _document.image()->depth() == 1 ) {
      volumeRaiseLevelMapAction->setEnabled( true );
      volumeExtrudeAction->setEnabled( true );
      volumeDWTAction->setEnabled( true );
    } else {
      volumeRaiseLevelMapAction->setEnabled( false);
      volumeExtrudeAction->setEnabled( false );
      volumeDWTAction->setEnabled( false );
    }
    if ( _document.image()->bands() == 1 ) {
      volumeToGrayAction->setEnabled( false );
      volumeToColorAction->setEnabled( true );
      volumeAddNoiseAction->setEnabled( true );
    } else {
      volumeToGrayAction->setEnabled( true );
      volumeToColorAction->setEnabled( false );
      volumeAddNoiseAction->setEnabled( false );
    }

    for ( std::vector<QAction*>::size_type i = 0; i < _convertActions.size(); ++i ) {
      QAction * action = _convertActions[i];
      action->setCheckable( true );
      if ( action->data().toInt() == _document.valueType() ) {
        action->setEnabled( false );
        action->setChecked( true );
      }
      else {
        action->setEnabled( true );
        action->setChecked( false );
      }
    }
  }
  selectedColorChanged( _view3DWidget->colorMap().selectedColor() );
}

void
MainWindow::volumeClamp()
{
  _dialogThresholds->show();
}

void
MainWindow::popupRecentFileOpened()
{
  _recentFilesMenu->clear();
  QAction * action;
  std::vector< QString >::iterator it = globalSettings->recentFiles().begin();
  std::vector< QString >::iterator end = globalSettings->recentFiles().end();
  while ( it !=  end ) {
    action = new QAction( *it, _recentFilesMenu );
    action->setData( *it );
    _recentFilesMenu->addAction( action );
    ++it;
  }
  _recentFilesMenu->addSeparator();
  if ( globalSettings->recentFiles().empty() ) {
    action = new QAction( "[ empty file list ]", _recentFilesMenu );
    action->setData( QString() );
    action->setEnabled( false );
    _recentFilesMenu->addAction( action );
  } else {
    action = new QAction( tr( "&Clear recent files history" ), _recentFilesMenu );
    action->setData( QString() );
    _recentFilesMenu->addAction( action );
  }
}

void
MainWindow::fileOpenRecent( QAction * action )
{
  QString filename = action->data().toString();
  if ( ! filename.isEmpty() )
    openFile( filename );
  else
    globalSettings->recentFiles().clear();
}

void
MainWindow::openFile( QString fileName )
{
  bool ok;
  bool rawFile = false;
  if ( fileName.endsWith(".raw", Qt::CaseInsensitive ) ) {
    rawFile = true;
    _dialogInputSize->guessSizeFromFilename(fileName);
    if ( _dialogInputSize->execRaw() == QDialog::Accepted ) {
      _document.create( _dialogInputSize->width(),
                        _dialogInputSize->height(),
                        _dialogInputSize->depth(),
                        _dialogInputSize->bands(),
                        _dialogInputSize->valueType() );
      Size rawSize = _document.rawSize();
      QFileInfo fileInfo( fileName );
      if ( rawSize != static_cast<Size>( fileInfo.size() ) ) {
        QMessageBox::critical( this,
                               "Error while loading a Raw file",
                               QString("Volume size mismatch.\n"
                                       "File size is %1\nbut volume size is %2")
                               .arg( fileInfo.size() )
                               .arg( rawSize )
                               );
        return;
      }
    } else return;
  }
  _view3DWidget->clear();
  _application->processEvents();
  globalProgressReceiver.showMessage("Loading volume...");
  if ( rawFile ) {
    ok = _document.loadRaw( fileName.toLatin1(),
                            endian() != _dialogInputSize->endianness(),
                            _dialogInputSize->multiplexed() );
  } else {
    ok = _document.load( fileName.toLatin1() );
  }
  globalProgressReceiver.showMessage(0);
  if ( ok )
    updateCaption();
  else
    _dialogs->errorOpening( _fileName.toLatin1() );
}

void
MainWindow::view2DClosing()
{
  show2DViewAction->setEnabled( true );
  _view3DWidget->highlightSlice( false );
  _view3DWidget->view2DImage( 0 );
}

void
MainWindow::dialogSkelClosing()
{
  volumeSkelAction->setEnabled( true );
}

void
MainWindow::showSkelDialog()
{
  volumeSkelAction->setEnabled( false );
  _dialogSkel->show();
}

void
MainWindow::adjacencyPairChanged( int index )
{
  globalSettings->adjacencyPair( index );
}

void
MainWindow::actionSelected( QAction *action )
{
  if ( action == drawVoxelAction )
    _view3DWidget->action( ActionDrawVoxel );
  if ( action == drawSimpleVoxelAction )
    _view3DWidget->action( ActionDrawSimpleVoxel );
  if ( action == drawSurfelAction )
    _view3DWidget->action( ActionDrawSurfel );
  if ( action == eraseVoxelAction )
    _view3DWidget->action( ActionRemoveVoxel );
  if ( action == eraseSimpleVoxelAction )
    _view3DWidget->action( ActionRemoveSimpleVoxel );
  if ( action == showInfoAction )
    _view3DWidget->action( ActionInfo );
  if ( action == moveAction )
    _view3DWidget->action( ActionMove );
}

void
MainWindow::volumeSubSample()
{
  globalProgressReceiver.showMessage("Subsampling...");
  _document.subSample( 4 );
  globalProgressReceiver.showMessage(0);
}

void
MainWindow::volumeScale( QAction* action )
{
  int id = action->data().toInt();
  if ( id >= 0 && id <= 3 ) {
    globalProgressReceiver.showMessage("Scaling...");
    _document.scale( id , 2 );
    globalProgressReceiver.showMessage(0);
  }
}

void
MainWindow::volumeExtrude()
{
  bool ok;
  int res = QInputDialog::getInt( this,
                                  "Depth",
                                  "Enter required depth:",
                                  64, 0, std::numeric_limits<int>::max(),
                                  1, &ok );
  if ( ok ) _document.extrude( res );
}

void
MainWindow::volumeSpongify()
{
  _document.spongify();
}

void
MainWindow::volumeHollowOut()
{
  _document.hollowOut( globalSettings->adjacencyPair() );
}

void
MainWindow::volumeSurfaceFill()
{
  _document.surfaceFill();
}

void
MainWindow::volumeHoleFill()
{
  _document.fill();
}

void
MainWindow::fileMerge()
{
  if ( _dialogMerge->exec() == QDialog::Accepted ) {
    _view3DWidget->clear();
    globalProgressReceiver.showMessage("Merging...");
    _document.merge( _dialogMerge->fileName().toLatin1(),
                     _dialogMerge->axis() );
    globalProgressReceiver.showMessage(0);
    updateCaption();
  }
}

void
MainWindow::volumeDistanceMap()
{
  _application->processEvents();
  _document.distanceMap();
}

void
MainWindow::volumeTrim( int direction, int depth )
{
  TRACE << "Trim " << direction << " " << depth << std::endl;
  _document.trim( direction, depth );
}

void
MainWindow::volumeToGray()
{
  _document.toGray();
}

void
MainWindow::volumeClear()
{
  _document.binarize( 0 );
}

void
MainWindow::volumeQuantize()
{
  bool ok;
  int res =
      QInputDialog::getInt( this,
                            "Quantization",
                            "Enter required number of values for each band:",
                            16, 0, 65536,
                            1, &ok );
  if ( ok ) _document.quantize( res );
}

void
MainWindow::volumeAddNoise()
{
  bool ok;
  double res = QInputDialog::getDouble( this,
                                        "Add noise",
                                        "Enter noise level (i.e., ratio):",
                                        0.1, 0.0, 1.0,
                                        2, &ok );
  if ( ok )
    _document.addNoise( static_cast<float>( res ) );
}

void
MainWindow::updateCaption()
{
  if ( ! _document.image()->isEmpty() ) {
    QString name = "No name";
    if ( ! _document.fileName().isEmpty() )
      name = QFileInfo( _document.fileName() ).fileName();
    setWindowTitle( QString( "QVox " VERSION_STRING " [%1]")
                    .arg( name ) );
  } else setWindowTitle( "QVox " VERSION_STRING " [No document]" );
}

void
MainWindow::moveLight( bool on )
{
  if ( on )
    _view3DWidget->action( ActionMoveLight );
  else
    _view3DWidget->action( ActionMove );
}

void
MainWindow::shadingChanged( int index )
{
  moveLightAction->setEnabled( index == Surface::ColorFromLight ||
                               index == Surface::ColorFromLightedMaterial||
                               index == Surface::ColorFromLightedColorMap||
                               index == Surface::ColorFromLightedLUT );
}

void
MainWindow::opacityChanged( int alpha )
{
  _view3DWidget->forceRedraw(true);
  statusBar()->showMessage( QString( "Opacity : %1%" ).arg( static_cast<int>( 100 * alpha/255.0 ) ) );
}

void
MainWindow::volumeAddBoundaries()
{
  bool ok = false;
  int min = std::max( static_cast<SSize>(1),
                      std::min( std::min( _document.image()->width() / 2,
                                          _document.image()->height() / 2 ),
                                _document.image()->depth() / 2 ) );


  int thickness = IntegerDialog::ask( this, 1, min, ok );
  if ( ok )
    _document.addBoundaries( _view3DWidget->colorMap().selectedColor(),
                             thickness );
}

void
MainWindow::volumeToColor()
{
  _document.toColor();
}

void
MainWindow::surfaceColorizeBorder()
{
  _view3DWidget->colorizeBorder();
}

void
MainWindow::popupShadingOpened()
{
  int s = globalSettings->shading();
  QList<QAction*> actions = _shadingMenu->actions();
  QList<QAction*>::iterator it = actions.begin(), end = actions.end();
  while ( it != end ) {
    if ( (*it)->data().toInt() == s )
      (*it)->setChecked( true );
    ++it;
  }
}

void
MainWindow::volumeDWT()	
{
  if ( ! confirmNoSave() ) { return; }
  globalProgressReceiver.showMessage("DWT...");
  _document.applyDWT( globalSettings->dwtFinalSize() );
  globalProgressReceiver.showMessage(0);
  updateCaption();
}

void
MainWindow::changeDisplay2DIn3D( bool display )
{
  if ( display )
    _view3DWidget->view2DImage( _dialog2DView->viewWidget()->image() );
  else
    _view3DWidget->view2DImage( 0 );
  _view3DWidget->repaint();
}

void
MainWindow::drawAxis( bool on )
{
  globalSettings->drawAxis( on );
  _view3DWidget->forceRedraw( true );
}

void
MainWindow::volumeDemoPlugin()
{
  globalProgressReceiver.showMessage("Applying plugin...");
  _document.demoPlugin();
  globalProgressReceiver.showMessage(0);
}

void
MainWindow::volumeConvertAction( QAction * action )
{
  int type = action->data().toInt();
  _document.convert( type );
}

void
MainWindow::pluginMenuActivated( QAction *action )
{
  TRACE << "PLUGING ID : " << action->data().toInt() << std::endl;
  PluginDialog * plugin = _plugins[action->data().toInt()];
  plugin->execute();
  TRACE << "PLUGIN AFTER DIALOG: "
        << plugin->command().toLatin1().constData()
        << std::endl;
  TRACE << "PLUGIN IN: "
        << plugin->inputExtension().toLatin1().constData()
        << std::endl;
  TRACE << "PLUGIN OUT: "
        << plugin->outputExtension().toLatin1().constData()
        << std::endl;
}

void
MainWindow::runningPlugin(PluginDialog* plugin)
{
  TSHOW( plugin );
  if ( plugin ) {
    TRACE << "PLUGIN PROGRESS ON";
    statusBar()->insertPermanentWidget( 0, _pluginProgressWidget );
    _pluginProgressWidget->plugin(plugin);
    _pluginProgressWidget->show();
  } else {
    TRACE << "PLUGIN PROGRESS OFF";
    statusBar()->removeWidget( _pluginProgressWidget );
  }
}

void MainWindow::cutPlane(bool on)
{
  _view3DWidget->surface().setCutPlane(on);
  _view3DWidget->forceRedraw(true);
}

void MainWindow::dialogSettingsClosed()
{
  if ( _dialogSettings->minimalFPSChanged())
    _view3DWidget->checkFPS();
}

bool
MainWindow::confirmNoSave() {
  if ( _document.modified() ) {
    QMessageBox::StandardButton b;
    switch ( b = _dialogs->confirmDoNotSave( _document.fileName(), this ) ) {
    case QMessageBox::Cancel :
      return false;
      break;
    case QMessageBox::Save : {
      if ( _document.fileName().isEmpty() ) {
        QString s =  _dialogs->askSaveFileName( VolumeFile );
        if ( s.isEmpty() ) return false;
        _document.saveAs( s.toLatin1() );
      } else _document.save();
    }
      break;
    case QMessageBox::Ignore :
      _document.modified( false );
      break;
    default:
      ERROR << "Invalid response to confirmation dialog (" << b << ")\n";
      break;
    }
  }
  return true;
}

void
MainWindow::resize3DView( const QSize & size )
{
  _view3DWidget->resize( size );
}

View3DWidget &
MainWindow::view3DWidget()
{
  return *_view3DWidget;
}

void
MainWindow::colorLUTSelected(QString filename)
{
  _view3DWidget->surface().loadLUT(filename.toLatin1().constData());
  _view3DWidget->forceRedraw( true );
}
