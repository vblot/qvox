/**
 * @file   HistogramView.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:38 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "HistogramView.h"
#include "Document.h"

#include <algorithm>

#include <QPainter>
#include <QImage>
#include <QLayout>
#include <QKeyEvent>

using namespace std;

HistogramView::HistogramView( const std::vector<Size> & histogram,
                              QWidget * parent )
  :QWidget( parent ), _histogram( histogram )
{
  CONS( HistogramView );
  setAutoFillBackground(false);
  setToolTip("Histogram view (click and drag to change threshold values)");
  _underMouseCol = _underMouseRow = -1;
  _leftButton = _rightButton = false;
  _penColor.setRgb( 0, 0 , 0 );
  setSizePolicy( QSizePolicy::Expanding, QSizePolicy::MinimumExpanding );
  setMinimumHeight( 50 );
  _showMin = false;
  _minPos = 0;
  _maxPos = histogram.size() - 1;
}

HistogramView::~HistogramView()
{
  
}

UInt
HistogramView::bin(int x)
{
  if ( x < 0 )
    return 0;
  if ( x >= width()-1 )
    return _histogram.size();
  return static_cast<int>( ( static_cast<float>(x) / width() ) * _histogram.size() );
}


void
HistogramView::showMin( bool on )
{
  _showMin = on;
  repaint();
}

void
HistogramView::setMin( int min )
{
  if ( min < 0 ) return;
  _minPos = min;
  repaint();
}

void
HistogramView::setMax( int max )
{
  if ( static_cast<UInt>(max) >= _histogram.size() ) return;
  _maxPos = max;
  repaint();
}

void
HistogramView::color( const QColor & color )
{
  _penColor = color;
}

void
HistogramView::paintEvent( QPaintEvent *)
{
  draw();
}

void
HistogramView::computeViewParams()
{
  
}

void
HistogramView::resizeEvent( QResizeEvent * )
{
  _pixmap = QPixmap( size() );
  computeViewParams();
}

void
HistogramView::keyPressEvent( QKeyEvent * e )
{
  switch ( e->key() ) {
  case Qt::Key_Plus:
    break;
  case Qt::Key_Minus:
    break;
  case Qt::Key_PageUp:
    break;
  case Qt::Key_PageDown:
    break;
  }
}

void
HistogramView::mousePressEvent ( QMouseEvent * e  )
{
  _underMouseCol = e->pos().x();
  _underMouseRow = e->pos().y();
  
  if ( e->button() == Qt::LeftButton ) {
    unsigned int pos  = bin( e->pos().x() );
    if ( pos > _maxPos ) {
      _maxPos = pos;
      emit maxChanged( pos );
    } else if ( pos < _minPos ) {
      _minPos = pos;
      emit minChanged( pos );
    } else if ( fabs( pos - (float)_minPos ) < fabs( pos - (float)_maxPos ) ) {
      _minPos = pos;
      emit minChanged( pos );
    } else {
      emit maxChanged( pos );
      _maxPos = pos;
    }
    repaint();
  }

  if ( e->button() == Qt::LeftButton) {
    _prevX = e->pos().x();
    _prevY = e->pos().y();
    _leftButton = true;
  }

  if ( e->button() == Qt::RightButton ) {
    _prevX = e->pos().x();
    _prevY = e->pos().y();
    _rightButton = true;
  }
}

void
HistogramView::mouseReleaseEvent ( QMouseEvent * e )
{
  if ( e->button() == Qt::LeftButton ) _leftButton = false;
  if ( e->button() == Qt::RightButton ) _rightButton = false;
}

void
HistogramView::mouseMoveEvent( QMouseEvent * e )
{
  if ( e->buttons() & Qt::LeftButton ) {
    unsigned int pos  = bin( e->pos().x() );
    if ( pos > _maxPos ) {
      _maxPos = pos;
      emit maxChanged( pos );
    } else if ( pos < _minPos ) {
      _minPos = pos;
      emit minChanged( pos );
    } else if ( fabs( pos - (float)_minPos ) < fabs( pos - (float)_maxPos ) ) {
      _minPos = pos;
      emit minChanged( pos );
    } else {
      _maxPos = pos;
      emit maxChanged( pos );
    }
    repaint();
  }
}

void
HistogramView::update()
{
  if ( _maxPos >= _histogram.size() ) {
    _maxPos = _histogram.size() - 1;
    emit maxChanged( _maxPos );
  }
  repaint();
}

void
HistogramView::draw()
{
  int viewWidth = width();
  int viewHeight = height();
  QColor highlight = palette().color( QPalette::Highlight );

  vector<Size>::const_iterator i;
  if ( _showMin )
    i = max_element( _histogram.begin(), _histogram.end() );
  else
    i = max_element( _histogram.begin()+1, _histogram.end() );
  _maxCount = *i;

  QPainter painter( &_pixmap );
  
  painter.fillRect( _pixmap.rect(), palette().window().color() );

  if ( ! isEnabled() ) {
    painter.setPen( _penColor.light() );
    painter.drawLine(0,0,width()-1,height()-2);
    painter.drawLine(0,height()-2,width()-1,0);
    painter.setPen( QColor(0,0,0) );
  } else {
    painter.setPen( _penColor );
    double scale = static_cast<float>( viewHeight ) / _maxCount;
    if ( _histogram.size() ) {
      for ( int x = 0; x < viewWidth; ++x ) {
        UInt  position = bin( x );
        int h = _histogram[position];
        if ( position >= _minPos && position <= _maxPos ) {
          if ( _showMin || position ) {
            painter.setPen( _penColor );
            int y = viewHeight - static_cast<int>(scale*h);
            if ( y <= 0 ) y = 0;
            painter.drawLine(x, viewHeight - 1, x,  y );
            if ( y > 1 ) {
              painter.setPen( highlight );
              painter.drawLine(x, 0, x, y-1 );
            }
            painter.setPen( _penColor );
          } else {
            painter.setPen( highlight );
            painter.drawLine(x, viewHeight - 1, x, 0 );
            painter.setPen( _penColor );
          }
        } else {
          if ( _showMin || position ) {
            painter.setPen( _penColor.light() );
            painter.drawLine(x, viewHeight - static_cast<int>(scale*h), x, viewHeight );
          }
        }
      }
    }
  }
  
  painter.setPen( QColor(0,0,0) );
  painter.drawLine(0, viewHeight-1, viewWidth-1, viewHeight-1 );

  painter.end();
  painter.begin(this);
  painter.drawPixmap( 0, 0, _pixmap );
}


