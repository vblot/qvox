/**
 * @file   View2DWidget.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:38 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "View2DWidget.h"
#include "Document.h"

#include <limits>

#include <QApplication>
#include <QPainter>
#include <QResizeEvent>
#include <QKeyEvent>
#include <QScrollArea>
#include <QScrollBar>

View2DWidget::View2DWidget( QWidget * parent )
   : QWidget( parent ),
     _image( 100, 100, QImage::Format_RGB32 )
{
   CONS( View2DWidget );
   setAutoFillBackground(false);
   setFocusPolicy(Qt::StrongFocus);
   _document = 0;
   _colorMap = 0;
   _volume = 0;
   _volumeWidth = 0;
   _volumeHeight = 0;
   _volumeDepth = 0;
   _originRow = _originCol = 0;
   _originCols[0] = _originCols[1] = _originCols[2] = 0;
   _originRows[0] = _originRows[1] = _originRows[2] = 0;
   _zoom = 1;
   _x = _y = _z = 0;
   _volume = 0 ;
   _maxZoom = 64;
   _underMouseCol = _underMouseRow = -1;
   _leftButton = _rightButton = false;
   _plane = PlaneOxy;
   _depth = 0;
   _bands = 1;
   _valid = false;
   _volumeModified = false;
   _needToRedraw = true;
   _mode = Move;
   _clipBoard = 0;
   setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
}

View2DWidget::~View2DWidget()
{
   DEST( View2DWidget );
}

bool
View2DWidget::volumeModified()
{
   bool result = _volumeModified;
   _volumeModified = false;
   return result;
}

void
View2DWidget::document( Document * document )
{
   if ( _document )
      disconnect( _document, 0, this, 0 );
   _document = document;
   if ( _document )
      connect( _document, SIGNAL( volumeChanged() ),
               this, SLOT( updateDoc() ) );
   updateDoc();
}

void
View2DWidget::scrollArea( QScrollArea * area )
{
   _scrollArea = area;
}

void
View2DWidget::showEvent( QShowEvent * ) {
   connect( _document, SIGNAL( volumeChanged() ),
            this, SLOT( updateDoc() ) );
   connect( _colorMap, SIGNAL( changed(int) ),
            this, SLOT( updateDoc() ) );
   updateDoc();
   _valid = true;
}

void
View2DWidget::hideEvent( QHideEvent * ) {
   disconnect( _document, 0, this, 0 );
   disconnect( _colorMap, 0, this, 0 );
   _valid = false;
}

void
View2DWidget::updateDoc()
{
   if ( _document->image()->isEmpty() ) return;
   _volume = _document->image();
   _bands = _document->image()->bands();
   setPlane( _plane, true );
}

void
View2DWidget::mode( Mode mode )
{
   _mode = mode;
}

void
View2DWidget::colorMap( ColorMap * colorMap )
{
   _colorMap = colorMap;
}

void
View2DWidget::paintEvent( QPaintEvent * e )
{
   if ( !_valid ) return;
   QPainter painter( this );
   QRect clip = e->rect();
   //if ( clip.size() != _clipRect.size() )
   _image = QImage( clip.width() + 3*_zoom,
                    clip.height() + 3*_zoom,
                    QImage::Format_RGB32 );
   int x, y;
   //if ( _needToRedraw )
   draw( x, y, clip );
   painter.drawImage( x, y, _image );
   _clipRect = clip;
   _needToRedraw = false;
}

void
View2DWidget::forceRedraw()
{
   _needToRedraw = true;
   //repaint();
   update();
}

void
View2DWidget::resizeEvent( QResizeEvent * )
{
   forceRedraw();
}

void
View2DWidget::zoomIn()
{
   if ( _zoom >= _maxZoom ) return;
   _zoom *= 2;
   size( 2*width(), 2*height());
   forceRedraw();
   emit zoomChanged();
}

void
View2DWidget::zoomOut()
{
   if ( _zoom == 1 ) return;
   _zoom /= 2;
   size( width()/2, height()/2);
   forceRedraw();
   emit zoomChanged();
}

void
View2DWidget::zoomOne()
{
   if ( _zoom == 1 ) return;
   _zoom = 1;
   size( _volumeWidth, _volumeHeight);
   forceRedraw();
   emit zoomChanged();
}

void
View2DWidget::zoomFit()
{
   QWidget * w = dynamic_cast<QWidget*>( parent()->parent() );
   QRect rect = w->childrenRect();
   float zoomH = rect.width() / _volumeWidth;
   float zoomV = rect.height() / _volumeHeight;
   if ( zoomH < zoomV )
      zoomV = zoomH;
   int z = static_cast<int>( zoomV );
   int n = 0;
   while ( z && ++n)
      z >>= 1;
   z = 1 << (n-1);
   if ( z != _zoom ) {
      _zoom = z;
      size( _volumeWidth * _zoom, _volumeHeight * _zoom );
      forceRedraw();
      emit zoomChanged();
   }
}

void
View2DWidget::animateSlices()
{
   int currentDepth = _depth;
   _depth = 0;
   while ( _depth < _volumeDepth - 1 ) {
      setDepth( _depth + 1 );
      emit writeViewRequest();
   }
   setDepth( currentDepth );
}

void
View2DWidget::keyPressEvent( QKeyEvent * e )
{
   switch ( e->key() ) {
   case Qt::Key_Plus:
      zoomIn();
      break;
   case Qt::Key_Minus:
      zoomOut();
      break;
   case Qt::Key_Backslash:
      animateSlices();
      break;
   case Qt::Key_Equal:
      zoomFit();
      break;
   case Qt::Key_PageUp:
      setDepth( _depth + 1 );
      break;
   case Qt::Key_PageDown:
      setDepth( _depth - 1 );
      break;
   }
}

void
View2DWidget::drawVoxel( int x, int y  ) {
   bool modified = false;
   switch ( _plane ) {
   case PlaneOyz:
      modified = _document->setVoxelValue( _depth, x, y,
                                           _colorMap->selectedColor(),
                                           _colorMap->selectedQColor().red(),
                                           _colorMap->selectedQColor().green(),
                                           _colorMap->selectedQColor().red(),
                                           false );
      break;
   case PlaneOxz:
      modified = _document->setVoxelValue( x, _depth, y,
                                           _colorMap->selectedColor(),
                                           _colorMap->selectedQColor().red(),
                                           _colorMap->selectedQColor().green(),
                                           _colorMap->selectedQColor().red(),
                                           false );
      break;
   case PlaneOxy:
      modified = _document->setVoxelValue( x, y, _depth,
                                           _colorMap->selectedColor(),
                                           _colorMap->selectedQColor().red(),
                                           _colorMap->selectedQColor().green(),
                                           _colorMap->selectedQColor().red(),
                                           false );
      break;
   }
   forceRedraw();
   if ( modified ) _volumeModified = true;
}

void
View2DWidget::drawLine( int x1, int y1, int x2, int y2 ) {
   switch ( _plane ) {
   case PlaneOyz:
      _document->drawLine( _depth, x1, y1,
                           _depth, x2, y2,
                           _colorMap->selectedColor(),
                           _colorMap->selectedQColor().red(),
                           _colorMap->selectedQColor().green(),
                           _colorMap->selectedQColor().red(),
                           false );
      break;
   case PlaneOxz:
      _document->drawLine( x1, _depth, y1,
                           x2, _depth, y2,
                           _colorMap->selectedColor(),
                           _colorMap->selectedQColor().red(),
                           _colorMap->selectedQColor().green(),
                           _colorMap->selectedQColor().red(),
                           false );
      break;
   case PlaneOxy:
      _document->drawLine( x1, y1, _depth,
                           x2, y2, _depth,
                           _colorMap->selectedColor(),
                           _colorMap->selectedQColor().red(),
                           _colorMap->selectedQColor().green(),
                           _colorMap->selectedQColor().red(),
                           false );
      break;
   }
   forceRedraw();
   _volumeModified = true;
}

void
View2DWidget::fillPlane( int x, int y  ) {
   _document->valid( false );
   switch ( _plane ) {
   case PlaneOyz:
      _document->image()->seedFillYZ( _depth, x, y, _colorMap->selectedColor() );
      break;
   case PlaneOxz:
      _document->image()->seedFillXZ( x, _depth, y, _colorMap->selectedColor() );
      break;
   case PlaneOxy:
      _document->image()->seedFillXY( x, y, _depth, _colorMap->selectedColor() );
      break;
   }
   _document->valid( true );
   forceRedraw();
   _volumeModified = true;
}

void
View2DWidget::clearVoxel( int x, int y  ) {
   bool modified = false;
   switch ( _plane ) {
   case PlaneOyz:
      modified = _document->setVoxelValue( _depth, x, y,
                                           0,
                                           0, 0, 0,
                                           false );
      break;
   case PlaneOxz:
      modified = _document->setVoxelValue( x, _depth, y,
                                           0,
                                           0, 0, 0,
                                           false );
      break;
   case PlaneOxy:
      modified = _document->setVoxelValue( x, y, _depth,
                                           0,
                                           0, 0, 0,
                                           false );
      break;
   }
   forceRedraw();
   if ( modified ) _volumeModified = true;
}

void
View2DWidget::wheelEvent( QWheelEvent * e )
{
   if ( e->delta() < 0 )
      zoomOut();
   else
      zoomIn();
}

void
View2DWidget::mousePressEvent ( QMouseEvent * e  )
{
   QPoint p( e->pos() );
   if ( ! _document->valid() ) return;

   int x = p.x() / _zoom;
   int y = p.y() / _zoom;

   if ( _mode != Draw
        && _mode != Brush
        && _mode != Fill
        && x == _underMouseCol
        && y == _underMouseRow ) return;

   _underMouseCol = x;
   _underMouseRow = y;

   if ( _mode == Draw && e->button() == Qt::LeftButton ) drawVoxel( x, y );
   if ( _mode == Brush && e->button() == Qt::LeftButton ) {
      _underMouseCol = x;
      _underMouseRow = y;
   }
   if ( _mode == Fill && e->button() == Qt::LeftButton )
      fillPlane( x, y );
   if ( _mode == Draw && e->button() == Qt::RightButton )
      clearVoxel( x, y );

   if ( _bands == 3 ) {
      QString message("[%1,%2,%3]  Value=[%4]");
      switch ( _plane ) {
      case PlaneOyz:
         emit infoChanged( message.arg( _depth ).arg( x ).arg( y )
                           .arg( _document->image()->valueString(_depth, x, y) ) );
         break;
      case PlaneOxz:
         emit infoChanged( message.arg( x ).arg( _depth ).arg( y )
                           .arg( _document->image()->valueString(x,_depth,y) ) );
         break;
      case PlaneOxy:
         emit infoChanged( message.arg( x ).arg( y ).arg( _depth )
                           .arg( _document->image()->valueString(x,y,_depth) ) );
         break;
      }
   } else {
      QString message("[%1,%2,%3]  Value=[%4]");
      switch ( _plane ) {
      case PlaneOyz:
         emit infoChanged( message.arg( _depth ).arg( x ).arg( y )
                           .arg( _document->image()->valueString(_depth, x, y) ) );
         break;
      case PlaneOxz:
         emit infoChanged( message.arg( x ).arg( _depth ).arg( y )
                           .arg(  _document->image()->valueString(x,_depth,y) ) );
         break;
      case PlaneOxy:
         emit infoChanged( message.arg( x ).arg( y ).arg( _depth )
                           .arg( _document->image()->valueString(x,y,_depth) ) );
         break;
      }
   }
   if ( e->button() == Qt::LeftButton ) {
      if ( _mode != Draw && _mode != Brush ) {
         _prevX = e->pos().x();
         _prevY = e->pos().y();
      }
      _leftButton = true;
   }

   if ( e->button() == Qt::RightButton ) {
      if ( _mode != Draw && _mode != Brush ) {
         _prevX = e->pos().x();
         _prevY = e->pos().y();
      }
      _rightButton = true;
   }
}

void
View2DWidget::mouseReleaseEvent ( QMouseEvent * e )
{
   if ( e->button() == Qt::LeftButton ) _leftButton = false;
   if ( e->button() == Qt::RightButton ) _rightButton = false;
}

void
View2DWidget::mouseMoveEvent( QMouseEvent * e )
{
   bool update = false;
   if ( ! _document->valid() ) return;
   if ( ! _leftButton && ! _rightButton ) return;
   if ( _mode == Draw ) {
      int x = e->pos().x() / _zoom;
      int y = e->pos().y() / _zoom;
      if ( x == _underMouseCol && y == _underMouseRow ) return;
      _underMouseCol = x;
      _underMouseRow = y;
      if ( _leftButton ) drawVoxel( x, y );
      if ( _rightButton ) clearVoxel( x, y );
   } else if ( _mode == Brush ) {
      int x = e->pos().x() / _zoom;
      int y = e->pos().y() / _zoom;
      if ( x == _underMouseCol && y == _underMouseRow ) return;
      if ( _leftButton )
         drawLine( _underMouseCol, _underMouseRow,
                   x, y );
      _underMouseCol = x;
      _underMouseRow = y;
   } else {
      /* Sliding the view */
      int dx = e->pos().x() - _prevX;
      int dy = e->pos().y() - _prevY;
      // if (  dx >= _zoom || dx <= -_zoom ) {
      if (  dx ) {
         _prevX = e->pos().x();
         update = true;
      }
      // if ( dy >= _zoom || dy <= -_zoom ) {
      if ( dy ) {
         _prevY = e->pos().y();
         update = true;
      }
      if ( update )
         slide( dx, dy );
   }
}

void
View2DWidget::draw( int & xOut, int & yOut, const QRect & clip )
{
   int originCol = clip.x() / _zoom;
   int originRow = clip.y() / _zoom;

   if ( clip.y() >= _volumeHeight*_zoom) return;
   if ( clip.x() >= _volumeWidth*_zoom) return;

   if ( originCol > 0 )
      --originCol;
   if ( originRow > 0 )
      --originRow;

   xOut = originCol * _zoom;
   yOut = originRow * _zoom;

   if ( !_valid ||  !_document || !_colorMap || !_volume || ! _document->valid() ) return;

   _originCols[ static_cast<int>( _plane ) ] = originCol;
   _originRows[ static_cast<int>( _plane ) ] = originRow;

   UChar * scanLine = _image.scanLine( 0 );
   Size bytesPerLine = _image.scanLine(1) - _image.scanLine(0);
   UChar bytesPerPixel = 4;
   UChar **pixels = _colorMap->pixels();
   int x, y;
   QRgb *qrgb, *pPixel;
   QRgb pixel;

   int maxLine = clip.height();
   maxLine += 2*_zoom;

   while ( originRow + ( maxLine / _zoom )  >= _volumeHeight )
      --maxLine;

   int cols = clip.width() / _zoom;
   ++cols; ++cols;
   if ( clip.x() % _zoom )
      ++cols;
   while ( originCol + cols > _volumeWidth )
      cols--;

   int cellWidth = _zoom * bytesPerPixel;

   _image.fill( palette().window().color().rgb() );
   int inc;
   int dec = 0;
   int xStart = 0;

   inc = _zoom;
   while ( inc >>= 1 ) dec++;  // Shift to divide by zoom (power of 2).
   if ( _bands == 3 ) { // True colors mode
      UChar red,green,blue;
      if ( _zoom == 1 ) {
         switch ( _plane ) {
         case PlaneOyz:
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + ( line >> dec );
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(_depth,x,y,red,green,blue);
                  * reinterpret_cast<QRgb*>( scanLine + xStart ) =
                        QColor( red,green,blue ).rgb();
                  xStart += bytesPerPixel;
               }
               scanLine += bytesPerLine;
            }
            break;
         case PlaneOxz:
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + (line / _zoom);
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(x,_depth,y,red,green,blue);
                  * reinterpret_cast<QRgb*>( scanLine + xStart ) =
                        QColor( red,green,blue ).rgb();
                  xStart += bytesPerPixel;
               }
               scanLine += bytesPerLine;
            }
            break;
         case PlaneOxy:
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + line;
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(x,y,_depth,red,green,blue);
                  * reinterpret_cast<QRgb*>( scanLine + xStart ) =
                        QColor( red, green, blue ).rgb();
                  xStart += bytesPerPixel;
               }
               scanLine += bytesPerLine;
            }
            break;
         }
      } else  if ( _zoom == 2 ) {
         switch ( _plane ) {
         case PlaneOyz:
            inc = bytesPerPixel << 1;
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + (line / _zoom);
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(_depth,x,y,red,green,blue);
                  pixel = QColor( red, green, blue ).rgb();
                  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
                  *(pPixel++) = pixel;
                  *(pPixel++) = pixel;
                  xStart += inc;
               }
               scanLine += bytesPerLine;
            }
            break;
         case PlaneOxz:
            inc = bytesPerPixel << 1;
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + (line / _zoom);
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(x,_depth,y,red,green,blue);
                  pixel = QColor( red, green, blue ).rgb();
                  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
                  *(pPixel++) = pixel;
                  *(pPixel++) = pixel;
                  xStart += inc;
               }
               scanLine += bytesPerLine;
            }
            break;
         case PlaneOxy:
            inc = bytesPerPixel << 1;
            for ( int line = 0 ; line < maxLine ; line++) {
               y = originRow + ( line >> 1 );
               xStart = 0;
               for ( int col = 0; col < cols ; col++) {
                  x = originCol + col;
                  _volume->getColor(x,y,_depth,red,green,blue);
                  pixel = QColor( red, green, blue ).rgb();
                  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
                  *(pPixel++) = pixel;
                  *(pPixel++) = pixel;
                  xStart += inc;
               }
               scanLine += bytesPerLine;
            }
            break;
         }
      } else switch ( _plane ) {  // zoom is greater than 2
      case PlaneOyz:
         inc = _zoom * bytesPerPixel;
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + (line / _zoom);
            pPixel = reinterpret_cast<QRgb*>( scanLine );
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getColor(_depth,x,y,red,green,blue);
               pixel = QColor( red, green, blue ).rgb();
               x = _zoom;
               while ( x--) *(pPixel++) = pixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxz:
         inc = _zoom * bytesPerPixel;
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + (line / _zoom);
            pPixel = reinterpret_cast<QRgb*>( scanLine );
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getColor(x,_depth,y,red,green,blue);
               pixel = QColor( red, green, blue ).rgb();
               x = _zoom;
               while ( x--) *(pPixel++) = pixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxy:
         inc = _zoom * bytesPerPixel;
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + (line >> dec);
            pPixel = reinterpret_cast<QRgb*>( scanLine  );
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getColor(x,y,_depth,red,green,blue);
               pixel = QColor( red, green, blue ).rgb();
               x = _zoom;
               while ( x--) *(pPixel++) = pixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      }
   } // Single band => Use the Colormap
   else if ( _zoom == 1 ) {
      UChar value;
      switch ( _plane ) {
      case PlaneOyz:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + line;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(_depth,x,y,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[value] );
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               xStart += bytesPerPixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxz:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + line;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,_depth,y,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[value] );
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               xStart += bytesPerPixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxy:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + line;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,y,_depth,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[ value ]);
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               xStart += bytesPerPixel;
            }
            scanLine += bytesPerLine;
         }
         break;
      }
   } else  if ( _zoom == 2 ) {
      UChar value;
      switch ( _plane ) {
      case PlaneOyz:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + ( line>>1 );
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(_depth,x,y,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[value] );
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               *(pPixel++) = *qrgb;
               xStart +=  bytesPerPixel<<1;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxz:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + ( line>>1 );
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,_depth,y,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[value] );
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               *(pPixel++) = *qrgb;
               xStart +=  bytesPerPixel<<1;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxy:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + ( line>>1 );
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,y,_depth,value);
               qrgb =  reinterpret_cast<QRgb*>( pixels[value]);
               pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
               *(pPixel++) = *qrgb;
               *(pPixel++) = *qrgb;
               xStart +=  bytesPerPixel<<1;
            }
            scanLine += bytesPerLine;
         }
         break;
      }
   } else {                                            // Zoom is greater than 2
      UChar value;
      switch ( _plane ) {
      case PlaneOyz:
         for ( int line = 0 ; line < maxLine ; line++ ) {
            y = originRow + ( line >> dec );
            inc = _zoom * bytesPerPixel;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(_depth,x,y,value);
               memcpy(  scanLine + xStart,
                        pixels[value],
                        cellWidth );
               xStart += inc;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxz:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + ( line >> dec );
            inc = _zoom * bytesPerPixel;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,_depth,y,value);
               memcpy(  scanLine + xStart,
                        pixels[value],
                        cellWidth );
               xStart += inc;
            }
            scanLine += bytesPerLine;
         }
         break;
      case PlaneOxy:
         for ( int line = 0 ; line < maxLine ; line++) {
            y = originRow + ( line >> dec );
            inc = _zoom * bytesPerPixel;
            xStart = 0;
            for ( int col = 0; col < cols ; col++) {
               x = originCol + col;
               _volume->getGrayLevel(x,y,_depth,value);
               memcpy(  scanLine + xStart,
                        pixels[value],
                        cellWidth );
               xStart += inc;
            }
            scanLine += bytesPerLine;
         }
         break;
      }
   }

   if ( _zoom >= 16 ) {
      scanLine = _image.scanLine(0);
      int maxRow = maxLine / _zoom;
      for ( int r = 0 ; r <= maxRow  ; r++ ) {
         memset( scanLine,
                 0xC0,
                 cols * _zoom * bytesPerPixel );
         scanLine += bytesPerLine * _zoom;
      }
      scanLine = _image.scanLine(0);
      UChar *pc = scanLine;
      for (int line = 0 ; line < maxLine ; line++) {
         pc = scanLine;
         for ( int col = 0; col <= cols; col++) {
            *(pc++) = 0xC0;
            *(pc++) = 0xC0;
            *(pc++) = 0xC0;
            pc += _zoom * bytesPerPixel - 3;
         }
         scanLine += bytesPerLine;
      }
   }
}

void
View2DWidget::redrawPixel(int /*col*/, int /*row*/)
{

}

void
View2DWidget::setPlane( Plane plane, bool force )
{
   if ( !force && ( plane == _plane ) ) return;
   switch ( _plane ) {
   case PlaneOyz: _x = _depth; break;
   case PlaneOxz: _y = _depth; break;
   case PlaneOxy: _z = _depth; break;
   }
   _plane = plane;
   switch ( _plane ) {
   case PlaneOxy:
      _volumeWidth = _volume->width();
      _volumeHeight = _volume->height();
      _volumeDepth = _volume->depth();
      _depth = _z;
      break;
   case PlaneOxz:
      _volumeWidth = _volume->width();
      _volumeHeight = _volume->depth();
      _volumeDepth = _volume->height();
      _depth = _y;
      break;
   case PlaneOyz:
      _volumeWidth = _volume->height();
      _volumeHeight = _volume->depth();
      _volumeDepth = _volume->width();
      _depth = _x;
      break;
   }
   if ( _depth >= _volumeDepth ) {
      _depth = _volumeDepth - 1;
   }

   size( _volumeWidth * _zoom, _volumeHeight *_zoom );

   emit planeChanged( static_cast<int>( _plane ) );
   emit maxDepthChanged( std::numeric_limits<int>::max() );
   emit depthChanged( _depth, static_cast<int>( _plane ) );
   emit maxDepthChanged( _volumeDepth );
   forceRedraw();
}

void
View2DWidget::setDepth( int depth )
{
   if ( ( depth == _depth)  || (depth >= _volumeDepth) || (depth < 0) ) return;
   _depth = depth;
   emit depthChanged( _depth, static_cast<int>( _plane ) );
   forceRedraw();
}

void
View2DWidget::copyPlane()
{
   dispose( _clipBoard );
   switch ( _plane ) {
   case PlaneOxy:
      _clipBoard = _document->image()->slice( -1, -1, _depth );
      break;
   case PlaneOyz:
      _clipBoard = _document->image()->slice( _depth, -1, -1 );
      break;
   case PlaneOxz:
      _clipBoard = _document->image()->slice( -1, _depth, -1 );
      break;
   }
}

void
View2DWidget::cutPlane()
{
   dispose( _clipBoard );
   if ( _plane == PlaneOxy ) {
      _clipBoard = _document->image()->slice( -1, -1, _depth );
      _document->image()->zeroPlane(-1,-1,_depth);
   }
   if ( _plane == PlaneOyz ) {
      _clipBoard = _document->image()->slice( _depth, -1, -1 );
      _document->image()->zeroPlane(_depth,-1,-1);
   }
   if ( _plane == PlaneOxz ) {
      _clipBoard = _document->image()->slice( -1, _depth, -1 );
      _document->image()->zeroPlane(-1,_depth,-1);
   }
   forceRedraw();
}

void
View2DWidget::pastePlane()
{
   if ( _plane == PlaneOxy ) {
      _document->image()->insertSlice( -1, -1, _depth, _clipBoard );
   }
   if ( _plane == PlaneOyz ) {
      _document->image()->insertSlice( _depth, -1, -1, _clipBoard );
   }
   if ( _plane == PlaneOxz ) {
      _document->image()->insertSlice( -1, _depth, -1, _clipBoard );
   }
   forceRedraw();
}

void
View2DWidget::size( int w, int h )
{
   _width = w;
   _height = h;
   QWidget::resize( w, h );
}

QSize
View2DWidget::sizeHint() const
{
   return QSize( _width, _height );
}

QSize
View2DWidget::minimumSizeHint () const
{
   return QSize( _width, _height );
}

void
View2DWidget::slide( int dx, int dy )
{
   QScrollBar * hscroll = _scrollArea->horizontalScrollBar();
   QScrollBar * vscroll = _scrollArea->verticalScrollBar();

   if ( dx > 0 ) ++dx;
   if ( dx < 0) --dx;
   if ( dy > 0 ) ++dy;
   if ( dy < 0) --dy;

   if ( dx < 0 && dx > -_zoom ) dx = -_zoom;
   if ( dx > 0 && dx < _zoom ) dx = _zoom;
   if ( dy < 0 && dy > -_zoom ) dy = -_zoom;
   if ( dy > 0 && dy < _zoom ) dy = _zoom;

   float hscale = (hscroll->maximum() - hscroll->minimum()) / static_cast<float>( _volumeWidth * _zoom ) ;
   float vscale = (vscroll->maximum() - vscroll->minimum()) / static_cast<float>( _volumeHeight * _zoom );
   if ( hscale < 1 ) hscale = 1;
   if ( vscale < 1 ) vscale = 1;

   int zdx = static_cast<int>( floor( 0.5 + dx * hscale ) );
   int zdy = static_cast<int>( floor( 0.5 + dy * vscale ) );

   hscroll->setValue( hscroll->value() - zdx );
   vscroll->setValue( vscroll->value() - zdy );

}
