/**
 * @file   FloatValidator.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:43:39 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "FloatValidator.h"
#include "globals.h"
#include <iostream>

FloatValidator::FloatValidator( QObject * parent, 
				float min, float max, int decimals )
  :QRegExpValidator( QRegExp(""), parent ) {
  
  CONS( FloatValidator );

  _min = min;
  _max = max;
  _decimals = decimals;
  updateRegExp();
}

void 
FloatValidator::updateRegExp() {
  QString re = "";
  
  if ( _min < 0.0 ) {
    if ( _max < 0.0 ) 
      re += "-";
    else
      re += "-?"; 
  }
    re += "\\d+[.]\\d{0,%1}";    
    QString qsRe = re.arg( _decimals );
    QRegExpValidator::setRegExp( QRegExp( qsRe ) );
}

QValidator::State 
FloatValidator::validate ( QString & input, int & pos ) const {
  QValidator::State state = QRegExpValidator::validate( input, pos );
  float value;
  if ( state ==  Acceptable || state == Intermediate ) {
    value = input.toFloat(0);
    if ( _max > 0 && value > _max ) return Invalid;
    if ( _min < 0 && value < _min ) return Invalid;
    if ( value >= _min && value <= _max ) return Acceptable;
    else return Intermediate;
  }
  return state;
}

void
FloatValidator::fixup ( QString & input ) const {
  input = "1.0";
}
