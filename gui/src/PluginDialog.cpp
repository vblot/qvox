/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginDialog.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginDialog
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "PluginDialog.h"


#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QDoubleValidator>
#include <QDir>
#include <QGridLayout>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QSpinBox>
#include <QVBoxLayout>

#include "FileFieldWidget.h"

#include "PluginDialogIn.h"
#include "PluginDialogOut.h"
#include "PluginDialogInOut.h"

#include "Document.h"
#include "QTools.h"

PluginDialog *
PluginDialog::createPluginDialog( QWidget * parent,
				  const QDomElement & xmlPlugin,
				  Document & document,
				  const QString & xmlPath, 
				  const QString & binPath, 
				  const QString & libPath,
				  const QString & workingDir )
{
   QString str = attrValue( xmlPlugin, "type" );
   QString wDir = workingDir;
   if ( wDir.size() &&  ! wDir.startsWith( "/" ) )
      wDir = xmlPath + QDir::separator() + wDir;   
   if ( str == "input-output" )
      return new PluginDialogInOut( parent, xmlPlugin, document, 
				    xmlPath, binPath, libPath, wDir );
   if ( str == "input" ) 
      return new PluginDialogIn( parent, xmlPlugin, document,
				 xmlPath, binPath, libPath, wDir );
   if ( str == "output" )
      return new PluginDialogOut( parent, xmlPlugin, document,
				  xmlPath, binPath, libPath, wDir );
   return 0;
}

QString
PluginDialog::attrValue( const QDomNode & node, const char * parameter )
{
   return node.toElement().attributes().namedItem( parameter ).nodeValue();
}

/**
 * Constructor
 */
PluginDialog::PluginDialog( QWidget * parent,
			    const QDomElement & xmlPlugin,
			    Document & document,
			    const QString & xmlPath,
			    const QString & binPath,
			    const QString & libPath,
			    const QString & workingDir )
   : QDialog( parent ),
     _xmlPlugin( xmlPlugin ),
     _document( &document ),
     _xmlPath( xmlPath ),
     _binPath( binPath ),
     _libPath( libPath ),
     _workingDir( workingDir )
{
   setModal( false );

   int row = 0;
   _grid = new QGridLayout;
   setLayout( _grid );
   _grid->setSpacing( 0 );
   _grid->setContentsMargins( 2, 2, 2, 2 );
   setWindowTitle( "Command parameters" );

   _name = _xmlPlugin.attributes().namedItem( "name" ).nodeValue();
   _topLabel = new QLabel( "NoName", this );
   _topLabel->setAlignment( Qt::AlignHCenter );
   _grid->addWidget( _topLabel, row++, 0, 1, 2 );

   // Separator
   QFrame * frame = new QFrame( this );
   frame->setFrameShape( QFrame::HLine );
   frame->setFrameShadow( QFrame::Sunken );
   _grid->addWidget( frame, row++, 0, 1, 2 );

   addParametersWidgets( row );

   // Separator
   frame = new QFrame( this );
   frame->setFrameShape( QFrame::HLine );
   frame->setFrameShadow( QFrame::Sunken );
   _grid->addWidget( frame, row++, 0, 1, 2 );

   // Restore default button
   _pbDefaults = new QPushButton("&Default values", this);
   _grid->addWidget( _pbDefaults, row, 0, 1, 1 );
   _cbShowConsole = new QCheckBox( "Show console" );
   _cbShowConsole->setLayoutDirection( Qt::RightToLeft );
   _grid->addWidget( _cbShowConsole, row++, 1, 1, 1 );
   
   _grid->addItem( new QSpacerItem( 30, 10 ), row++, 0, 1, 2 ); 


   {
      QDomNode nodeCommand = _xmlPlugin.elementsByTagName("command").at(0);
      if ( attrValue( nodeCommand, "console" ) == "yes" )
	 _cbShowConsole->setChecked( true );
   }

   // Lower buttons
   QHBoxLayout * hbox = new QHBoxLayout;
   _grid->addLayout( hbox, row++, 0, 1, 2 );
   _pbOk = new QPushButton("&Ok", this );
   _pbApply = new QPushButton("&Apply", this );
   _pbClose = new QPushButton("&Close", this );
   hbox->addWidget( _pbOk );
   hbox->addWidget( _pbApply );
   hbox->addWidget( _pbClose );

   // Status bar
   _statusBar = new QStatusBar( this );
   _grid->addWidget( _statusBar, row++, 0, 1, 2 );

   QObject::connect( _pbOk, SIGNAL( clicked( bool ) ),
		     this, SLOT( okClicked() ) );
   QObject::connect( _pbApply, SIGNAL( clicked( bool ) ),
		     this, SLOT( applyClicked() ) );
   QObject::connect( _pbClose, SIGNAL( clicked( bool ) ),
		     this, SLOT( closeClicked() ) );
   QObject::connect( _pbDefaults, SIGNAL( clicked( bool ) ),
		     this, SLOT( defaults() ) );
}

void
PluginDialog::setWToolTip( QWidget * w, const QDomNode & n )
{
   QString tip = attrValue( n, "tooltip" );
   if ( tip.size() ) { 
      tip.replace("\\n","\n");
      w->setToolTip( tip );
      w->setToolTip( tip );
   }
}

void
PluginDialog::addParametersWidgets( int & row )
{
   QDomNode node = _xmlPlugin.firstChild();
   QLabel * label;
   QString str;
   while ( !node.isNull() ) {
      QDomElement e = node.toElement();
      QString tag = e.tagName();
      if ( tag == "parameter" ) {
	 QString ptype = e.attributes().namedItem( "type" ).nodeValue();
	 QString name = e.attributes().namedItem( "name" ).nodeValue();
	 QWidget * field = 0;
	 if ( ptype == QString("integer") ) {
	    QLineEdit * le = new QLineEdit("0", this );
	    QIntValidator * v = new QIntValidator(0,65535,this);
	    str = attrValue( e, "min" );
	    if ( str.size() ) v->setBottom( str.toInt() );
	    str = attrValue( e, "max" );
	    if ( str.size() ) v->setTop( str.toInt() );
	    le->setValidator( v );
	    str = attrValue( e, "default" );
	    if ( str.size() ) le->setText( str );
	    _grid->addWidget( label = new QLabel( name, this ), row, 0 );
	    _grid->addWidget( le, row, 1 );
	    setWToolTip( le, e );
	    setWToolTip( label, e );
	    _fields.push_back( field = le );
	    _paramTypes.push_back( IntegerParam );
	 }
	 else if ( ptype == QString("spin-integer") ) {
	    QSpinBox * sb = new QSpinBox( this );
	    sb->setReadOnly( false );
	    str = attrValue( e, "min" );
	    if ( str.size() ) sb->setMinimum( str.toInt() );
	    str = attrValue( e, "max" );
	    if ( str.size() ) sb->setMaximum( str.toInt() );
	    str = attrValue( e, "default" );
	    if ( str.size() ) sb->setValue( str.toInt() );
	    str = attrValue( e, "step" );
	    if ( str.size() ) sb->setSingleStep( str.toInt() );
	    _grid->addWidget( label = new QLabel( name, this ), row, 0 );
	    _grid->addWidget( sb, row, 1 );
	    setWToolTip( sb, e );
	    setWToolTip( label, e );
	    _fields.push_back( field = sb );
	    _paramTypes.push_back( SpinIntegerParam );
	 }
	 else if ( ptype == QString("spin-double") ) {
	    QDoubleSpinBox * sb = new QDoubleSpinBox( this );
	    sb->setReadOnly( false );
	    str = attrValue( e, "min" );
	    if ( str.size() ) sb->setMinimum( str.toDouble() );
	    str = attrValue( e, "max" );
	    if ( str.size() ) sb->setMaximum( str.toDouble() );
	    str = attrValue( e, "default" );
	    if ( str.size() ) sb->setValue( str.toDouble() );
	    str = attrValue( e, "step" );
	    if ( str.size() ) sb->setSingleStep( str.toDouble() );
	    _grid->addWidget( label = new QLabel( name, this ), row, 0 );
	    _grid->addWidget( sb, row, 1 );
	    setWToolTip( label, e );
	    setWToolTip( sb, e );
	    _fields.push_back( field = sb );
	    _paramTypes.push_back( SpinDoubleParam );
	 }
	 else if ( ptype == QString("text") ) {
	    QLineEdit * le = new QLineEdit("", this );
	    str = attrValue( e, "default" );
	    if ( str.size() ) le->setText( str );
	    _grid->addWidget( label = new QLabel( name, this ), row, 0 );
	    _grid->addWidget( le, row, 1 );
	    setWToolTip( le, e );
	    setWToolTip( label, e );
	    _fields.push_back( field = le );	    
	    _paramTypes.push_back( TextParam );
	 }
	 else if ( ptype == QString("float") ) {
	    QLineEdit * le = new QLineEdit("0.0", this );
	    le->setValidator( new QDoubleValidator(this) );
	    str = attrValue( e, "default" );
	    if ( str.size() ) le->setText( str );
	    _grid->addWidget( label = new QLabel( name, this ), row, 0 );
	    _grid->addWidget( le, row, 1 );
	    setWToolTip( le, e );
	    setWToolTip( label, e );
	    _fields.push_back( field = le );
	    _paramTypes.push_back( FloatParam );
	 }
	 else if ( ptype == QString("radio") ) {
	    _grid->addWidget( new QLabel( name, this ), row, 0 );	    
	    QHBoxLayout * hbox = new QHBoxLayout;
	    _grid->addLayout( hbox, row, 1 );
	    QButtonGroup * group = new QButtonGroup( this );
	    QRadioButton * radioOn = new QRadioButton( "On", this );
	    QRadioButton * radioOff = new QRadioButton( "Off", this );
	    str = attrValue( e, "labelOn" );
	    if ( str.size() )
	       radioOn->setText( str );
	    str = attrValue( e, "labelOff" );
	    if ( str.size() )
	       radioOff->setText( str );
	    group->addButton( radioOn );
	    group->addButton( radioOff );
	    hbox->addWidget( radioOn );
	    hbox->addWidget( radioOff );
	    radioOn->setChecked( true );
	    if ( attrValue( e, "default" ) == "off" )
	       radioOff->setChecked( true );
	    setWToolTip( radioOn, e );
	    setWToolTip( radioOff, e );
	    setWToolTip( label, e );
	    _fields.push_back( field = radioOn );
	    _paramTypes.push_back( RadioParam );
	 }
	 else if ( ptype == QString("checkbox") ) {
	    _grid->addWidget( new QLabel( name, this ), row, 0 );	    
	    QCheckBox * check = new QCheckBox( this );
	    _grid->addWidget( check, row, 1 );	    
	    check->setChecked( false );
	    if ( attrValue( e, "default" ) == "on" )
	       check->setChecked( true );
	    setWToolTip( label, e );
	    setWToolTip( check, e );
	    _fields.push_back( field = check );
	    _paramTypes.push_back( CheckBoxParam );
	 }
	 else if ( ptype == QString("filename") ) {
	    _grid->addWidget( new QLabel( name, this ), row, 0 );	    	    
	    FileFieldWidget * fileField = new FileFieldWidget( this );
	    _grid->addWidget( fileField, row, 1 );
	    str = attrValue( e, "mode" );
	    if ( str == "output" )
	       fileField->mode( FileFieldWidget::Output );
	    else
	       fileField->mode( FileFieldWidget::Input );
	    str = attrValue( e, "default" );
	    if ( str.size() )
	       fileField->lineEdit().setText( str );
	    str = attrValue( e, "path" );
	    if ( str.size() ) {
	       if ( str.startsWith( "/" ) )
		  fileField->path( str );
	       else
		  fileField->path( _xmlPath + "/" + str );
	    }
	    setWToolTip( label, e );
	    setWToolTip( fileField, e );
	    _fields.push_back( field = & fileField->lineEdit() );
	    _paramTypes.push_back( FileNameParam );
	 } else if ( ptype == QString("select") ) {
	    _grid->addWidget( new QLabel( name, this ), row, 0 );
	    QComboBox * combo = new QComboBox( this );
	    _grid->addWidget( combo, row, 1 );
	    QDomNode choice = e.firstChild();
	    while ( ! choice.isNull() ) {
	       combo->addItem( choice.toElement().text(),
			       QVariant( attrValue( choice.toElement(), "id" ).toInt() ) );
	       choice = choice.nextSibling();
	    }
	    str = attrValue( e, "default" );
	    if ( str.size() ) 
	       combo->setCurrentIndex( str.toInt() );
	    setWToolTip( label, e );
	    setWToolTip( combo, e );
	    _fields.push_back( field = combo );
	    _paramTypes.push_back( SelectParam );	    
	 } else if ( !field ) {
	    ERROR << "Unrecognized plugin parameter type in plugins fil ("
		  << ptype.toLatin1().constData() << ")\n";
	 }
	 ++row;
      } else if ( tag == QString( "hr" ) ) {
	 QFrame * frame = new QFrame(this);
	 QString width = attrValue( e, "height" );
	 if ( !width.isEmpty() )
	    frame->setMinimumHeight( width.toInt() );
	 _grid->addWidget( frame, row++, 0, 1, 2 );
	 frame->setFrameShape( QFrame::HLine );
	 frame->setFrameShadow( QFrame::Sunken );
      } else if ( tag == QString( "label" ) ) {
	 QString text = attrValue( e, "text" );
	 QLabel * label = new QLabel( QString("<b>%1</b>").arg(text), this );
	 label->setMargin( 5 );
	 label->setAlignment( Qt::AlignCenter );
	 _grid->addWidget( label, row++, 0, 1, 2 );
      }
      node = node.nextSibling();
   }
}

QString
PluginDialog::command() const
{
   int index = 0;
   QDomNode node = _xmlPlugin.firstChild();
   QString command = _xmlPlugin.elementsByTagName("command").at(0).toElement().text() + " ";
   while ( !node.isNull() ) {
      QDomElement e = node.toElement();
      QString tag = e.tagName();
      if ( tag == "parameter" ) {	
	 QString order = attrValue( e, "order" );
	 QRegExp pattern( QString("\\$") + order + QString("(\\D)") );
	 TRACE << pattern.pattern().toLatin1().constData() << std::endl;
	 switch ( _paramTypes[index] ) {
	 case IntegerParam:
	 case FloatParam:
	 case TextParam:
	    {
	       QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[ index ] );
	       QString val = le->text();
	       command.replace( pattern, val + "\\1"); 
	    }
	    break;
	 case SpinIntegerParam:
	 case SpinDoubleParam:
	    {
	       QAbstractSpinBox * sb = dynamic_cast<QAbstractSpinBox*>( _fields[ index ] );
	       QString val = sb->text();
	       command.replace( pattern, val + "\\1" ); 
	    }	    
	    break;
	 case RadioParam:
	    {
	       QRadioButton * rb = dynamic_cast<QRadioButton*>( _fields[ index ] );
	       QString textOn = attrValue( e, "textOn" );
	       QString textOff = attrValue( e, "textOff" );
	       if ( rb->isChecked() )  
		  command.replace( pattern, textOn + "\\1" );
	       else
		  command.replace( pattern, textOff + "\\1" );
	    }
	    break;
	 case CheckBoxParam:
	    {
	       QCheckBox * cb = dynamic_cast<QCheckBox*>( _fields[ index ] );
	       QString textOn = attrValue( e, "textOn" );
	       QString textOff = attrValue( e, "textOff" );
	       if ( cb->isChecked() )  
		  command.replace( pattern, textOn + "\\1" );
	       else
		  command.replace( pattern, textOff + "\\1" );
	    }
	    break;
	 case FileNameParam:
	    {
	       QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[ index ] );
	       QString text = le->text();
	       command.replace( pattern, text + "\\1" );
	    }
	    break;
	 case SelectParam:
	    {
	       QComboBox * cb = dynamic_cast<QComboBox*>( _fields[ index ] );
	       QDomNode choice = e.firstChild();
	       int selectedId = cb->itemData( cb->currentIndex() ).toInt();
	       QString text;
	       while ( ! choice.isNull() ) {
		  if ( attrValue( choice.toElement(), "id" ).toInt() == selectedId ) {
		     text = attrValue( choice.toElement(), "value");
		  }
		  choice = choice.nextSibling();
	       }
	       command.replace( pattern, text + "\\1" );
	    }
	 } 
	 ++index;
      }
      node = node.nextSibling();
   }

   return command.trimmed();
}

QString
PluginDialog::inputExtension() const
{
   QDomNode node = _xmlPlugin.elementsByTagName("command").at(0);
   return node.toElement().attributes().namedItem( "input" ).nodeValue();
}

QString
PluginDialog::outputExtension() const
{
   QDomNode node = _xmlPlugin.elementsByTagName("command").at(0);
   return node.toElement().attributes().namedItem( "output" ).nodeValue();
}

int
PluginDialog::paramsCount() const
{
   return _fields.size();
}

void
PluginDialog::launch()
{
   
}

int
PluginDialog::execute()
{
   return 1;
}

void
PluginDialog::defaults()
{
   QDomNode node = _xmlPlugin.firstChild();
   int index = 0;
   while ( !node.isNull() ) {
      QDomElement e = node.toElement();
      QString tag = e.tagName();
      if ( tag == "parameter" ) {
	 QString ptype = e.attributes().namedItem( "type" ).nodeValue();
	 if ( ptype == QString("integer") ) {
	    QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() ) 
	       le->setText( deflt );
	    else
	       le->setText( 0 );
	    ++index;
	 }
	 else if ( ptype == QString("spin-integer") ) {
	    QSpinBox * sb = dynamic_cast<QSpinBox*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() )
	       sb->setValue( deflt.toInt() );
	    else 
	       sb->setValue( 0 );
	    ++index;
	 }
	 else if ( ptype == QString("spin-double") ) {
	    QDoubleSpinBox * sb = dynamic_cast<QDoubleSpinBox*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() )
	       sb->setValue( deflt.toDouble() );
	    else
	       sb->setValue( 0.0 );
	    ++index;
	 }
	 else if ( ptype == QString("text") ) {
	    QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() )
	       le->setText( deflt );
	    else
	       le->setText( "" );
	    ++index;
	 }
	 else if ( ptype == QString("float") ) {
	    QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() )
	       le->setText( deflt );
	    else
	       le->setText( "0.0" );
	    ++index;
	 }
	 else if ( ptype == QString("radio") ) {
	    QRadioButton * radioOn = dynamic_cast<QRadioButton*>( _fields[index] );
	    if ( attrValue( e, "default" ) == "off" )
	       radioOn->setChecked( true );
	    else 
	       radioOn->setChecked( false );
	    ++index;
	 }
	 else if ( ptype == QString("checkbox") ) {
	    QCheckBox * check = dynamic_cast<QCheckBox*>( _fields[index] );
	    if ( attrValue( e, "default" ) == "on" )
	       check->setChecked( true );
	    else
	       check->setChecked( false );
	    ++index;
	 }
	 else if ( ptype == QString("filename") ) {
	    QLineEdit * le = dynamic_cast<QLineEdit*>( _fields[index] );
	    QString deflt = attrValue( e, "default" );
	    if ( ! deflt.isEmpty() )
	       le->setText( deflt );
	    else
	       le->setText( "" );
	    ++index;
	 } else if ( ptype == QString("select") ) {
	    QComboBox * combo = dynamic_cast<QComboBox*>( _fields[index] );
	    QString def = attrValue( e, "default" );
	    if ( ! def.isEmpty() ) 
	       combo->setCurrentIndex( def.toInt() );
	    else
	       combo->setCurrentIndex( 0 );
	    ++index;
	 }
      }
      node = node.nextSibling();
   }
   QDomNode nodeCommand = _xmlPlugin.elementsByTagName("command").at(0);
   if ( attrValue( nodeCommand, "console" ) == "yes" )
      _cbShowConsole->setChecked( true );
}

void
PluginDialog::setProcessEnv( QProcess * process )
{
   if ( !process )
      return;
   QStringList env = QProcess::systemEnvironment();
   QStringList::iterator it = env.begin();
   QStringList::iterator end = env.end();
   while ( it != end ) {
      if ( it->startsWith("PATH=") ) {
	 if ( _binPath.size() ) {
	    addPath( *it,  _xmlPath + ":" + _binPath, _xmlPath );
	 }
	 else {
	    addPath( *it, _xmlPath, _xmlPath );
	 }
      }
      if ( it->startsWith("LD_LIBRARY_PATH=") ) {
	 if ( _libPath.size() )
	    addPath( *it,  _xmlPath + ":" + _libPath, _xmlPath );
	 else
	    addPath( *it, _xmlPath, _xmlPath );
      }
      ++it;
   }
   process->setEnvironment( env );
}

