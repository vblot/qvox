/**
 * @file   ColorMapToolBar.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:57 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ColorMapToolBar.h"

#include <QMouseEvent>
#include <QMainWindow>
#include <QColorDialog>
#include <QPainter>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QPushButton>
#include <QFileInfo>
#include <QFileDialog>
#include "ColorMap.h"

ColorMapWidget::ColorMapWidget( QWidget *parent, ColorMap *colorMap ):
  QWidget( parent ), _colorMap(colorMap)
{
  CONS( ColorMapWidget );
  QSizePolicy policy(QSizePolicy::Fixed, QSizePolicy::Preferred);
  //policy.setHeightForWidth ( true );
  setSizePolicy( policy );
  _cellsSize.setWidth( 8 );
  _cellsSize.setHeight( 8 );
  _selectedColor = _colorMap->selectedColor();

  _sizeHint = QSize( 8 * 16 + 1, 8 * 16 + 1 );
  setMinimumWidth( _sizeHint.width() );
  setMinimumHeight( _sizeHint.height() );

  connect( colorMap, SIGNAL( changed(int) ),
	   this, SLOT( update() ) );
  connect( colorMap, SIGNAL( colorSelected( int ) ),
	   this, SLOT( selectColor( int ) ) );
}

ColorMapWidget::~ColorMapWidget()
{
  
}

void
ColorMapWidget::mousePressEvent ( QMouseEvent *e )
{
  if ( e->button() == Qt::LeftButton ) {
    int x = e->pos().x() / _cellsSize.width();
    int y = e->pos().y() / _cellsSize.height();
    if ( e->pos().x() < 0 || 
	 e->pos().y() < 0 || x >= _hcells || y >= _vcells || 
	 ( ( y == _vcells - 1) && (256 % _hcells) && ( x >= ( 256 % _hcells ) ) ) ) {
      return;
    }
    _colorMap->selectColor( x + y * _hcells );
  }
}

void ColorMapWidget::mouseDoubleClickEvent( QMouseEvent * e )
{
  if ( e->button() == Qt::LeftButton ) {
    int x = e->pos().x() / _cellsSize.width();
    int y = e->pos().y() / _cellsSize.height();
    if ( e->pos().x() < 0 || 
	 e->pos().y() < 0 || x >= _hcells || y >= _vcells || 
	 ( ( y == _vcells - 1) && (256 % _hcells) && ( x >= ( 256 % _hcells ) ) ) ) {
      return;
    }
    int c = x + y * _hcells;
    QColor result = QColorDialog::getColor( _colorMap->qcolors()[ c ]  );
    if ( result.isValid() ) _colorMap->setColor( c, result );
    _colorMap->selectColor( c );
  }
}

void
ColorMapWidget::selectColor( int color )
{
  _selectedColor = static_cast<UChar>( color );
  repaint();
}

void
ColorMapWidget::cellsSize( const QSize & size )
{
  if ( size != _cellsSize ) {
    _cellsSize = size;
    repaint();
  }
}

void
ColorMapWidget::update()
{
  repaint();
}

void
ColorMapWidget::paintEvent( QPaintEvent *)
{
  redraw();
}


void
ColorMapWidget::redraw()
{
  if ( ! isVisible() ) return;
  QPainter painter( this );
  const QColor * qcolors = _colorMap->qcolors();

  _hcells = (width()-1) /  _cellsSize.width();
  if ( !_hcells ) _hcells = 1;
  _vcells = 256 / _hcells;
  if ( 256 % _hcells )
    ++_vcells;
  int x, y;

  _cellsSize.setHeight( 8 );
  
  painter.setPen( QColor( 0, 0, 0 ) ); 
  for ( int i = 0; i < 256; i++) {
    y = _cellsSize.height() * (i / _hcells);
    x = ( i % _hcells  ) * _cellsSize.width();
    painter.setBrush( QBrush( qcolors[ i ] ) );
    painter.drawRect( x, y, _cellsSize.width(), _cellsSize.height() );
  }  
  // Selected color
  painter.setPen( QColor( 255, 0, 0 ) );
  y =  _cellsSize.height()  * ( _selectedColor / _hcells);
  x =  ( _selectedColor % _hcells  ) * _cellsSize.width();
  painter.setBrush( QBrush( qcolors[ _selectedColor ] ) );
  painter.drawRect( x, y, _cellsSize.width(), _cellsSize.height() );
}

int 
ColorMapWidget::heightForWidth ( int w ) const {
  int height = 2 + static_cast<int>( ceil( 256.0 / ( ( w - 2 ) / 7 ) ) )  * 7;
  return height;
}

void
ColorMapWidget::setSizeHint( const QSize & size )
{
  _sizeHint = size;
}

QSize
ColorMapWidget::sizeHint () const
{
  return _sizeHint;
}

ColorMapToolBar::ColorMapToolBar( QMainWindow * mainWindow,
                                  ColorMap * colorMap ) :
  QToolBar( QString( "Colormap" ), mainWindow ),
  _colorMap( colorMap )
{
  CONS( ColorMapWidget );
  QHBoxLayout *hbox = 0;
  QVBoxLayout *vbox = 0;
  QWidget * w;
  addWidget( new QLabel( "Colormap" ) );

  addWidget( w = new QWidget );
  vbox = new QVBoxLayout( w );
  vbox->setMargin(2);
  
  _colorMapWidget = new ColorMapWidget( w, _colorMap );
  vbox->addWidget( _colorMapWidget );
  vbox->setAlignment( _colorMapWidget, Qt::AlignHCenter );
  //_colorMapWidget->setPaletteBackgroundColor( QColor( 85, 113, 255 ));

  addWidget( w = new QWidget );
  hbox = new QHBoxLayout( w );
  hbox->setMargin(2);
  hbox->setSpacing(1);
  _selectedColorFrame = new QFrame( w );
  hbox->addWidget( _selectedColorFrame );
  _selectedColorFrame->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
  _selectedColorFrame->setFrameStyle( QFrame::Panel );
  _selectedColorFrame->setFrameShadow( QFrame::Sunken );
  _selectedColorFrame->setLineWidth( 2 );
  _selectedColorFrame->setMinimumWidth( 20 );
  _selectedColorFrame->setAutoFillBackground(true);
  QPalette palette = _selectedColorFrame->palette();
  //palette.setBrush( QPalette::Base, _colorMap->selectedQColor() );
  palette.setColor( QPalette::Window, _colorMap->selectedQColor() );
  _selectedColorFrame->setPalette( palette );  
  _spinBoxColor = new QSpinBox( w );
  _spinBoxColor->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
  _spinBoxColor->setMaximum( 255 );
  _spinBoxColor->setValue( _colorMap->selectedColor() );
  hbox->addWidget( _spinBoxColor );

  addWidget( new QLabel( " Type " ) );
  addWidget( _cbType = new QComboBox );
  _cbType->setSizePolicy( QSizePolicy::Preferred, 
			  QSizePolicy::Fixed );
  _cbType->insertItem( 0, "Grey" );
  _cbType->insertItem( 1, "Red" );
  _cbType->insertItem( 2, "Green");
  _cbType->insertItem( 3, "Blue" );
  _cbType->insertItem( 4, "Random" );
  _cbType->insertItem( 5, "Contrast" );
  _cbType->insertItem( 6, "Colors" );
  _cbType->insertItem( 7, "Hue shade" );
  _cbType->insertItem( 8, "Curvature" );
  _cbType->insertItem( 9, "Light" );
  _cbType->insertItem( 10, "Toon" );
  _cbType->setCurrentIndex( colorMap->type() );

  QPushButton * pbReverse;
  addWidget( pbReverse = new QPushButton( "Reverse" ) );
  pbReverse->setSizePolicy( QSizePolicy::Preferred, 
			    QSizePolicy::Fixed );

  addWidget( _labelMin = new QLabel( "Min" ) );
  _colorMap->setMin( 25 );
  _sliderMin = new QSlider( Qt::Horizontal );
  addWidget( _sliderMin );
  _sliderMin->setRange( 0, 255 );
  _sliderMin->setPageStep( 10 );
  _sliderMin->setValue( 25 );
  _sliderMin->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );  

  addWidget( _titleMaterial = new QLabel( "Material" ) );

  addWidget( w = new QWidget );
  hbox = new QHBoxLayout(w);
  hbox->setMargin(2);
  hbox->setSpacing(1);
  _labelMaterial = new QLabel( "#000000", w );
  _labelMaterial->setAutoFillBackground(true);
  _labelMaterial->setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );
  hbox->addWidget( _labelMaterial );
  palette = _labelMaterial->palette();
  palette.setBrush( QPalette::Window, QColor(128,128,128) );
  palette.setBrush( QPalette::Base, QColor(128,128,128) );
  _labelMaterial->setPalette( palette );
  
  _pbMaterial = new QPushButton( "...", w );
  hbox->addWidget( _pbMaterial );
  _pbMaterial->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  _colorMap->toonColor( QColor(128,128,128) );
  
  addSeparator();
  addWidget(new QLabel("LUT"));
  _cbLUT = new QComboBox(this);
  _cbLUT->addItem("(None)",QVariant("/dev/null"));
  _cbLUT->setEnabled(false);
  _lutFilenames << QString("/dev/null");
  addWidget(_cbLUT);
  addWidget( w = new QWidget );
  hbox = new QHBoxLayout(w);
  _pbRemoveLUT = new QPushButton("Remove",w);
  _pbRemoveLUT->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Fixed);
  _pbRemoveLUT->setEnabled(false);
  hbox->addWidget(_pbRemoveLUT);
  _pbAddLUT = new QPushButton("Add...",w);
  _pbAddLUT->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Fixed);
  hbox->addWidget(_pbAddLUT);

  connect( _pbAddLUT, SIGNAL(pressed()),
           this, SLOT(addLUT()));

  connect( _pbRemoveLUT, SIGNAL(pressed()),
           this, SLOT(removeLUT()));

  connect( _cbLUT, SIGNAL(currentIndexChanged(int)),
           this, SLOT(onLUTSelected(int)));

  connect( _colorMap, SIGNAL( changed(int ) ), 
	   this, SLOT( colorMapChanged(int ) ) );
  
  connect( _colorMap, SIGNAL( changed( int ) ),
	   this, SLOT( colorMapChanged(int) ) );  

  connect( _colorMap, SIGNAL( colorSelected(int) ), 
	   _spinBoxColor, SLOT( setValue( int ) ) ) ;

  connect( _colorMap, SIGNAL( colorSelected( int ) ),
	   this, SLOT( selectColor( int ) ) );

  connect( _cbType, SIGNAL( activated( int ) ), 
	   colorMap, SLOT( type( int ) ) );

  connect( _spinBoxColor, SIGNAL( valueChanged(int) ), 
	   _colorMap, SLOT( selectColor( int ) ) ) ;

  connect( pbReverse, SIGNAL( clicked() ), 
	   _colorMap, SLOT( reverse() ) );
  
  connect( _sliderMin, SIGNAL( valueChanged(int) ),
	   _colorMap, SLOT( setMin(int) ) );

  connect( _pbMaterial, SIGNAL( clicked() ),
	   this, SLOT( pbMaterialPushed() ) );

  colorMapChanged( colorMap->type() );
}

void ColorMapToolBar::hideEvent(QHideEvent *)
{
   emit aboutToHide();
}

void ColorMapToolBar::showEvent(QShowEvent *)
{
   emit aboutToShow();
}


void
ColorMapToolBar::colorMapChanged( int n )
{
  _cbType->setCurrentIndex( n );

  QPalette palette = _selectedColorFrame->palette();
  //palette.setBrush( QPalette::Base, _colorMap->selectedQColor() );
  palette.setColor( QPalette::Window, _colorMap->selectedQColor() );
  _selectedColorFrame->setPalette( palette );
  if ( n != ColorMap::Toon && n != ColorMap::Light ) {
    _labelMin->setEnabled( false );
    _sliderMin->setEnabled( false );
    _titleMaterial->setEnabled( false );
    _labelMaterial->setEnabled( false );
    _pbMaterial->setEnabled( false );
  } else {
    _labelMin->setEnabled( true );
    _sliderMin->setEnabled( true );
    _titleMaterial->setEnabled( true );
    _labelMaterial->setEnabled( true );
    _pbMaterial->setEnabled( true );
  }  
}

void
ColorMapToolBar::selectColor( int )
{
  QPalette palette = _selectedColorFrame->palette();
  //palette.setBrush( QPalette::Base, _colorMap->selectedQColor() );
  palette.setColor( QPalette::Window, _colorMap->selectedQColor() );
  _selectedColorFrame->setPalette( palette );
  _selectedColorFrame->repaint();
}


void ColorMapToolBar::addLUTFilename(const QString & name )
{
  _lutFilenames.push_back(name);
  QFileInfo info(name);
  QFile file(name);
  file.open(QFile::ReadOnly);
  char label[30];
  file.readLine(1024);
  file.readLine(label,30);
  _cbLUT->addItem(QString(label).trimmed(),name);
  if ( ! _cbLUT->isEnabled() ) {
    _cbLUT->setEnabled(true);
  }
}

void ColorMapToolBar::addLUT()
{
  QStringList list = QFileDialog::getOpenFileNames(this,"Select LUT files",".","*.lut",0,0);
  QStringList::iterator i = list.begin();
  while ( i != list.end() ) {
    addLUTFilename(*i);
    ++i;
  }
  if ( !list.isEmpty() )
    _cbLUT->setCurrentIndex(_cbLUT->count()-1);
}

void ColorMapToolBar::removeLUT()
{
  int index = _cbLUT->currentIndex();
  if (!index) return;
  _lutFilenames.removeAt(index);
  _cbLUT->removeItem(index);
  if ( _cbLUT->count() == 1 ) {
    _cbLUT->setEnabled(false);
    _pbRemoveLUT->setEnabled(false);
  }
}

void ColorMapToolBar::onLUTSelected(int index )
{
  if ( index > 0 ) {
    if ( ! _pbRemoveLUT->isEnabled() ) {
      _pbRemoveLUT->setEnabled(true);
    }
  } else {
    _pbRemoveLUT->setEnabled(false);
  }
  emit colorLUTFileSelected(_cbLUT->itemData(index).toString());
}

void
ColorMapToolBar::undock()
{

}

void 
ColorMapToolBar::pbMaterialPushed() {
  QColor color = _labelMaterial->palette().window().color();
  color = QColorDialog::getColor( color, this );
  if ( color.isValid() ) {
    QPalette palette = _labelMaterial->palette();
    palette.setBrush(QPalette::Base, color );
    palette.setBrush(QPalette::Window, color );
    _labelMaterial->setPalette( palette );
    _labelMaterial->setText( QString("#%1")
			     .arg( ( color.red() << 16 ) | ( color.green() << 8 ) | color.blue() , 6, 16 ) );
    _colorMap->setMaterialColor( color );
  }
}
