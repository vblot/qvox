/**
 * @file   Document.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:01 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Document.h"
#include <map>
#include <algorithm>
#include <limits>
#include <typeinfo>
using std::vector;
using std::map;
using std::numeric_limits;
using std::numeric_limits;

#include <QColor>
#include <QImageReader>
#include <QMessageBox>
#include <QTemporaryFile>
#include <QDialog>
#include <QMessageBox>
#include <QApplication>

#include "Array.h"
#include "genesis.h"
#include "tools3d.h"
#include "DemoPlugin.h"
#include "ByteOutput.h"
#include "ByteOutputQIO.h"
#include "ByteInputQIO.h"
#include "Dialogs.h"
#include "histogram_visitor.h"

#include "tools3d.h"
#include "draw_line_visitor.h"
#include "mesh_importer_visitor.h"

Document::Document()
{
  CONS( Document );
  _fileName = QString::null;
  _modified = false;
  _image = new ZZZImage<UChar>;
  _valueType = Po_ValUC;
  _equalization = false;
  _valid = false;
  _activePlugin = 0;
}

void
Document::create( SSize width, SSize height, SSize depth, SSize bands,
                  ValueType valueType ) {
  valid( false );
  free();
  _valueType = valueType;
  _image = AbstractImage::create( valueType, width, height, depth, bands );
  _modified = false;
  _fileName = QString::null;
  valid( true );
}

void
Document::valid( bool on ) {
  _valid = on;
  if ( !_valid )
    emit invalidated();
}

void
Document::notify()
{
  valid( true );
  emit volumeChanged();
}

void
Document::free()
{
  dispose( _image );
}

Document::~Document()
{
  free();
}

void
Document::message( const QString & str, int timeout )
{
  emit info( str, timeout );
}

void
Document::histo3D()
{
  valid( false );
  ZZZImage<UChar> * histogram = new ZZZImage<UChar>;
  HistogramVisitor visitor(*histogram);
  _image->accept(visitor);
  free();
  _image = histogram;
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::applyDWT( int finalSize )
{
  if ( _image->bands() > 1 || _image->depth() > 1 ) return;
  ZZZImage<Int32> image = ::filterCascade( *_volume, finalSize );
  valid( false );
  create( image.width(), image.height(), image.depth(), image.bands(), Po_ValUC );
  *( dynamic_cast< ZZZImage<UChar> * >( _image ) ) = image;
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::qvoxLogo()
{
  valid( false );
  free();
  _modified = false;
  _fileName = QString::null;
  _valueType = Po_ValUC;
  ZZZImage<UChar> *ucVolume = new ZZZImage<UChar>;
  ucVolume->loadFileBuffer( QVoxVolumeData3DZ, QVoxVolumeData3DZ_Size );
  _image = ucVolume;
  valid( true );
  _modified = false;
  emit volumeChanged();
}

bool
Document::loadablePixmap( const char * filename )
{
  QList<QByteArray> formats = QImageReader::supportedImageFormats();
  QString ext = QFileInfo( filename ).suffix().toUpper();
  if ( ext == QString("JPG") )
    ext = QString("JPEG");
  if ( formats.contains( ext.toLatin1() ) )
    return true;
  return false;
}

bool
Document::loadPixmap( const char * filename )
{
  valid( false );
  _fileName = QString::null;
  free();
  QImage image;
  if ( ! image.load( filename ) )
    return false;
  ZZZImage<UChar> *ucVolume = new ZZZImage<UChar>( image.width(), image.height(), 1, 3 );
  _image = ucVolume;
  UChar *** matrixRed = ucVolume->data( 0 );
  UChar *** matrixGreen = ucVolume->data( 1 );
  UChar *** matrixBlue = ucVolume->data( 2 );
  for ( int x = 0 ; x < image.width() ; x++ )
    for ( int y = 0; y < image.height() ; y++ ) {
      QRgb rgb = image.pixel( x, y );
      matrixRed[0][y][x] = qRed( rgb );
      matrixGreen[0][y][x] = qGreen( rgb );
      matrixBlue[0][y][x] = qBlue( rgb );
    }
  valid( true );
  _modified = false;
  emit volumeChanged();
  return true;
}

bool
Document::importMesh( const QString & fileName, bool filled )
{
  valid( false );
  _fileName = QString::null;
  QByteArray filename = fileName.toLatin1();
  if ( fileName.endsWith(".off",Qt::CaseInsensitive) ) {
    MeshOFFImporterVisitor v(filename,filled);
    _image->accept(v);
  } else {
    MeshOBJImporterVisitor v(filename,filled);
    _image->accept(v);
  }
  valid( true );
  _modified = false;
  emit volumeChanged();
  return true;
}


bool
Document::convert( int valueType )
{
  valid( false );
  bool ok = false;
  AbstractImage *image = _image->clone( valueType );
  if ( image && !image->isEmpty() ) {
    TRACE << image->props() << std::endl;
    ok = true;
    free();
    _image = image;
    _valueType = POvalueType[ _image->poType() ];
    valid( true );
    emit volumeChanged();
  } else valid( true );
  _modified = false;
  return ok;
}

bool
Document::load( const char * filename, bool newFileName )
{
  valid( false );
  bool ok = false;
  AbstractImage *image = AbstractImage::createFromFile( filename );
  bool needFullBoxCheck = _image->depth() != 1 && _image->bands() != 3;
  if ( image && !image->isEmpty() ) {
    TRACE << image->props() << std::endl;
    ok = true;
    free();
    if ( newFileName ) _fileName = filename;
    _image = image;
    _valueType = POvalueType[ _image->poType() ];
    if ( newFileName && strcmp( filename, "-" ) )
      globalSettings->addRecentFile( _fileName );
    if ( needFullBoxCheck ) checkFullBox();
    valid( true );
    emit volumeChanged();
  } else valid( true );
  _modified = false;
  return ok;
}

bool
Document::load( ByteInput & in )
{
  valid( false );
  bool needFullBoxCheck = _image->depth() != 1 && _image->bands() != 3;
  AbstractImage *image = AbstractImage::createFromFile( in );
  if ( image && !image->isEmpty() ) {
    TRACE << image->props() << std::endl;
    free();
    _image = image;
    _valueType = POvalueType[ _image->poType() ];
    _modified = true;
    if ( needFullBoxCheck ) checkFullBox();
    valid( true );
    emit volumeChanged();
    return true;
  }
  valid( true );
  return false;
}

bool
Document::loadRaw( const char * filename, const bool swapEndianness, bool multiplexed )
{
  valid( false );
  bool ok = false;
  bool needFullBoxCheck = _image->depth() != 1 && _image->bands() != 3;
  _fileName = filename;
  TRACE << "Type=" << _valueType
        << " ValueType= "
        << POvalueTypeName[ _valueType ]
           << std::endl;
  if ( (ok = _image->load( _fileName.toLatin1(), multiplexed ) ) ) {
    globalSettings->addRecentFile( _fileName );
    if ( swapEndianness )
      _image->swapEndianness();
  }
  if ( needFullBoxCheck ) checkFullBox();
  valid( true );
  emit volumeChanged();
  _modified = false;
  return ok;
}

void
Document::checkFullBox()
{
  if ( _image->bands() == 3 && POvalueType[ _image->poType() ] ==  Po_ValUC ) {
    globalSettings->fullVolumeBoxAction()->setChecked( true );
    OptionalDialog::message( QApplication::activeWindow(),
                             "Automatic full box",
                             " The volume appears to be an RGB volume (3 bands).\n"
                             "The \"Surface/Full volume (Ctrl+F)\" feature has been activated.",
                             Settings::FullBox );
  }
}

bool
Document::merge( const char * filename, int axis )
{
  bool ok = false;
  valid( false );

  AbstractImage * image = AbstractImage::createFromFile( filename );
  if ( POvalueType[ image->poType() ] != _valueType ) {
    ERROR << "Document::merge(): Cannot merge volumes with different value types.\n";
    dispose( image );
    return false;
  }
  ok = _image->merge( *image, axis );
  dispose( image );
  valid( true );
  emit volumeChanged();
  _modified = true;
  return true;
}

void
Document::trim( int plane, int n )
{
  valid( false );
  _image->trim( plane, n );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::fitBoundingBox( SSize margin )
{
  valid( false );
  _image->fitBoundingBox( margin );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::save()
{
  _image->save( _fileName.toLatin1() );
  globalSettings->addRecentFile( _fileName );
  _modified = false;
}

void
Document::saveAs( const QString & filename )
{
  _fileName = filename;
  globalSettings->addRecentFile( _fileName );
  save();
}

bool
Document::setVoxelValue( SSize x, SSize y, SSize z,
                         UChar colorIndex,
                         UChar red, UChar green, UChar blue,
                         bool refresh )
{
  Triple<SSize> voxel( x, y, z );
  bool changed = _image->setValue( voxel, red, green, blue );
  if ( _image->bands() == 1 )
    _image->setValue( voxel, colorIndex, colorIndex, colorIndex );
  else
    _image->setValue( voxel, red, green, blue );
  if ( refresh )
    emit volumeChanged();
  _modified = true;
  return changed;
}

void
Document::drawLine( SSize x1, SSize y1, SSize z1,
                    SSize x2, SSize y2, SSize z2,
                    UChar colorIndex,
                    UChar red, UChar green, UChar blue,
                    bool refresh )
{
  DrawLineVisitor v(x1,y1,z1,x2,y2,z2,red,green,blue);
  _image->accept(v);
  if ( refresh ) emit volumeChanged();
  _modified = true;
}

void
Document::drawLine( Triple<SSize> p1,
                    Triple<SSize> p2,
                    UChar colorIndex,
                    UChar red, UChar green, UChar blue,
                    bool refresh )
{
  DrawLineVisitor v(p1,p2,Triple<UChar>(red,green,blue));
  _image->accept(v);
  if ( refresh ) emit volumeChanged();
  _modified = true;
}

bool
Document::setVoxelValue( Triple<SSize> voxel,
                         UChar colorIndex,
                         UChar red, UChar green, UChar blue,
                         bool refresh )
{
  bool changed = _image->setValue( voxel, red, green, blue );
  if ( refresh )
    emit volumeChanged();
  _modified = true;
  return  changed;
}

void
Document::raiseLevelMap( SSize maxHeight )
{
  valid( false );
  if ( _image->depth() != 1 || ( _image->bands() != 1 && _image->bands() != 3 ) )
    return;
  AbstractImage * image = _image->levelMap( maxHeight );
  free();
  _image = image;
  _modified = true;
  globalSettings->fullVolumeBoxAction()->setChecked( false );
  valid( true );
  emit volumeChanged();
}

void
Document::mirror( int axis )
{
  valid( false );
  _image->mirror( axis );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::rotate( int axis )
{
  valid( false );
  _image->rotate( axis );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::negative()
{
  valid( false );
  _image->negative();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void Document::grayShadeFromHue()
{
  valid( false );
  _image->grayShadeFromHue();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void Document::grayShadeFromSaturation()
{
  valid( false );
  _image->grayShadeFromSaturation();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::quantize(UInt32 cardinality )
{
  valid( false );
  _image->quantize( cardinality );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::addNoise( float ratio )
{
  valid( false );
//  if ( typeid( *_image ) != typeid( *_volume ) )
//    return;
  zzzAddNoise6( *dynamic_cast< ZZZImage<UChar>* >(_image), ratio );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::clear()
{
  valid( false );
  _image->clear();
  valid( true );
  emit volumeChanged();
}

void
Document::binarize( UChar red, UChar green, UChar blue  )
{
  valid( false );
  if ( red || green || blue ) {
    if ( _image->bands() == 1 )
      _image->binarize( red );
    else
      _image->binarize( red, green, blue );
  } else _image->clear();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::toGray()
{
  valid( false );
  _image->toGray();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::toColor()
{
  valid( false );
  _image->toColor(  );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::hollowOut( int adjacency )
{
  valid( false );
  _image->hollowOut( adjacency );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::surfaceFill()
{
  valid( false );
  _image->surfaceFill(255);
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::fill()
{
  valid( false );
  _image->fillForHoleClosing();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::addBoundaries( UChar value, UChar thickness )
{
  valid( false );
  _image->addBoundaries( value, thickness );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::subSample( UChar minCount )
{
  valid( false );
  _image->subSample( minCount );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::scale( UChar direction, UChar factor )
{
  valid( false );
  _image->scale( direction, factor );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::extrude( SSize depth )
{
  valid( false );
  _image->extrude( depth );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::spongify()
{
  valid( false );
  _image->spongify();
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::bitPlane(UChar bit, SSize band )
{
  valid( false );
  _image->bitPlane( bit, band );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::bitPlaneGrayCode(UChar bit, SSize band )
{
  valid( false );
  _image->bitPlaneGrayCode( bit, band );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::skel( bool singlePass, int adjacency, SkelMethod method, int terminalTest )
{
  int iterations = singlePass;
  if ( bands() != 1 ) {
    QMessageBox::critical( 0,
                           "Error",
                           "Cannot skeletonize volumes with more than 1 band.<br/>"
                           "(You may use \"Volume/Colors/Convert to grayscale\".)" );
    return;
  }
  valid( false );
  TRACE << "SKEL pass:" << singlePass
        << " ADJ: " << adjacency
        << " Method: " << method
        << " Terminal: " << terminalTest << std::endl;

  if ( method == SkelSequential ) {
    SkelSequentialVisitor visitor( iterations,
                                   0, /* Directions sequence */
                                   adjacency,
                                   SkelTerminalTests[ terminalTest ][ adjacency ],
                                   true /* binarize */ );
    _image->accept(visitor);

  } else { // method == SkelParallel
    SkelParallelVisitor visitor( iterations,
                                 adjacency,
                                 SkelTerminalTests[ terminalTest ][ adjacency ],
                                 true  /* binarize */ );
    _image->accept(visitor);
  }
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::distanceMap()
{
  valid( false );
  ZZZImage<Int32> *slVolume;
  slVolume = new ZZZImage< Int32 >( _image->width(), _image->height(), _image->depth(), 1 );
  distanceTransformNoBorder( *_volume, *slVolume );
  free();
  _valueType = Po_ValSL;
  _modified = true;
  _fileName = QFileInfo( _fileName ).absolutePath()
      + QString("/edt_")
      + QFileInfo( _fileName ).fileName();
  _image = slVolume;
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::sphere( SSize x, SSize y, SSize z, SSize r )
{
  valid( false );
  if ( _valueType != Po_ValUC ) return;
  genesisSphere<UChar>( *dynamic_cast< ZZZImage<UChar> * >( _image ),
                        Triple<SSize>( x, y, z ), r,
                        static_cast<UChar>( 1 ),
                        Triple<float>(1,1,1) );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void Document::hyperbolicParaboloid( Triple<SSize> center,
                                     float c1, float c2,
                                     Rotation rotation, Triple<float> scale )
{
  valid( false );
  if ( _valueType != Po_ValUC ) return;
  genesisHyperbolicParaboloid<UChar>( *dynamic_cast< ZZZImage<UChar>* >( _image ),
                                      center, c1, c2, static_cast<UChar>( 1 ),
                                      rotation, scale );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::paraboloid( Triple<SSize> center,
                      float c1, float c2,
                      Rotation rotation, Triple<float> scale )
{
  valid( false );
  if ( _valueType != Po_ValUC ) return;
  genesisParaboloid<UChar>( *dynamic_cast< ZZZImage<UChar>* >( _image ),
                            center, c1, c2, static_cast<UChar>( 1 ),
                            rotation, scale );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::plane( Triple<SSize> center, Triple<float> normal, Rotation rotation )
{
  valid( false );
  if ( _valueType != Po_ValUC ) return;
  genesisPlane<UChar>( *dynamic_cast< ZZZImage<UChar>* >( _image ),
                       center, normal,
                       static_cast<UChar>( 1 ),
                       rotation );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::torus( Triple<SSize> center,
                 SSize largeRadius,
                 SSize smallRadius,
                 Rotation rotation,
                 Triple<float> scale )
{
  valid( false );
  if ( _valueType != Po_ValUC ) return;
  genesisTorus<UChar>( *dynamic_cast< ZZZImage<UChar>* >( _image ),
                       center,
                       largeRadius,
                       smallRadius,
                       static_cast<UChar>( 1 ),
                       rotation,
                       scale );
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::clamp( const Triple<Int32> & min, const Triple<Int32> & max)
{
  valid( false );
  _image->clamp(min,max);
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::clamp( const Triple<UInt32> & min, const Triple<UInt32> &  max)
{
  valid( false );
  _image->clamp(min,max);
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::clamp( const Triple<Float> & min, const Triple<Float> & max)
{
  valid( false );
  _image->clamp(min,max);
  _modified = true;
  valid( true );
  emit volumeChanged();
}

template<typename T>
inline bool positive( const T & x )
{
  return (numeric_limits<T>::is_signed)?(x>=0):true;
}

template<typename T>
void realignment( ZZZImage<UChar> & result,
                  const ZZZImage<T> & source )
{
  T smin, smax;
  const SSize bands = source.bands();
  const SSize depth = source.depth();
  const SSize height = source.height();
  const SSize width = source.width();
  if ( result.bands() != bands ||
       result.size() != source.size() )
    result.alloc( source.size(), source.bands() );

  for ( int band = 0; band < bands; band++ ) {
    smin = smax = source(0,0,0,band);
    for ( int plane = 0; plane < depth; ++plane )
      for ( int row = 0; row < height; ++row )
        for ( int col = 0; col < width; ++col ) {
          T v = source(col,row,plane,band);
          if ( v > smax ) smax = v;
          if ( v < smin ) smin = v;
        }
    Double slope;
    if ( positive( smin ) /*  >= static_cast<T>( 0 ) */ ) {
      smin = static_cast<T>( 0 );
      if ( smax <= static_cast<T>( 255 ) )
        slope = 1.0;
      else
        slope =  255.0 / smax;
    } else {
      if ( smax == smin )
        slope = 1.0;
      else
        slope = 255.0 / ( smax - smin );
    }
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
        for ( int col = 0; col < width; col++ )
          result(col,row,plane,band) =
              static_cast<UChar>( slope * ( source(col,row,plane,band) - smin ) );
  }
}

void realignment( ZZZImage<UChar> & result, const ZZZImage<Short> & source );
void realignment( ZZZImage<UChar> & result, const ZZZImage<UShort> & source );
void realignment( ZZZImage<UChar> & result, const ZZZImage<UInt32> & source );
void realignment( ZZZImage<UChar> & result, const ZZZImage<Int32> & source );
void realignment( ZZZImage<UChar> & result, const ZZZImage<Float> & source );
void realignment( ZZZImage<UChar> & result, const ZZZImage<Double> & source );

void realignment( ZZZImage<UChar> & result, const ZZZImage<Bool> & source )
{
  cast_copy( result, source );
}


void realignmentDistanceMap( ZZZImage<UChar> & result, const ZZZImage<Int32> & source )
{
  Int32 smin, smax;
  Int32 *** sarray;
  UChar *** darray;
  if ( source.bands() != 3 && source.bands() != 1 ) {
    ERROR << "realignementDistanceMap(): bad number of bands (" << source.bands() << ")\n";
    return;
  }

  SSize bands, depth, height, width;
  bands = source.bands();
  depth = source.depth();
  height = source.height();
  width = source.width();

  if ( result.bands() != bands ||
       result.size() != source.size() )
    result.alloc( source.size(), source.bands() );

  for ( int band = 0; band < bands; band++ ) {
    sarray = source.data( band );
    smin = sarray[0][0][0];
    smax = sarray[0][0][0];
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
        for ( int col = 0; col < width; col++ ) {
          Int32 v = sarray[plane][row][col];
          if ( v && ( v > smax ) ) smax = v;
          if ( v && ( v < smin ) ) smin = v;
        }
    Float slope;
    if ( smin >= static_cast<Int32>( 0 ) ) {
      if ( smax <= static_cast<Int32>( 255 ) )
        slope = 1.0F;
      else
        slope =  255.0F / smax;
    } else {
      if ( smax == smin )
        slope = 1.0F;
      else
        slope = 255.0F / ( smax - smin );
    }
    sarray = source.data( band );
    darray = result.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
        for ( int col = 0; col < width; col++ ) {
          if ( sarray[plane][row][col] )
            darray[plane][row][col] = 1 +
                static_cast<UChar>( slope * ( sarray[plane][row][col] - smin ) );
        }
  }
}

template<typename T>
void equalization( ZZZImage<UChar> & result, const ZZZImage<T> & source )
{
  map<T,Size> histo;
  map<T,Float> histoc;
  if ( source.bands() != 3 && source.bands() != 1 ) {
    ERROR << "equalization(): bad number of bands (" << source.bands() << ")\n";
    return;
  }

  const SSize bands = source.bands();
  const SSize depth = source.depth();
  const SSize height = source.height();
  const SSize width = source.width();

  if ( result.bands() != bands ||
       result.size() != source.size() )
    result.alloc( source.size(), source.bands() );

  for ( int band = 0; band < bands; band++ ) {
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
        for ( int col = 0; col < width; col++ )
          histo[ source(col,row,plane,band) ]++;
    Size sum = 0;
    typename map<T,Size>::const_iterator h;
    typename map<T,Size>::const_iterator end;
    h = histo.begin();
    T h0 = h->second;
    histoc[ h->first ] = 0.0F;
    h++;
    Float total = source.vectorSize() - h0;
    end = histo.end();
    for ( ; h != end; h++) {
      sum  = sum + h->second;
      histoc[ h->first ] = sum / total;
    }

    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
        for ( int col = 0; col < width; col++ )
          result(col,row,plane,band) =
              static_cast<UChar>( histoc[ source(col,row,plane,band) ] * 255.0F );
    histo.clear();
    histoc.clear();
  }
}

void equalization( ZZZImage<UChar> & result, const ZZZImage<Short> & source );
void equalization( ZZZImage<UChar> & result, const ZZZImage<UShort> & source );
void equalization( ZZZImage<UChar> & result, const ZZZImage<Int32> & source );
void equalization( ZZZImage<UChar> & result, const ZZZImage<UInt32> & source );
void equalization( ZZZImage<UChar> & result, const ZZZImage<Float> & source );
void equalization( ZZZImage<UChar> & result, const ZZZImage<Double> & source );

void equalization( ZZZImage<UChar> & result, const ZZZImage<Bool> & source ) {
  cast_copy( result, source );
}

template<typename T>
void cast_copy( ZZZImage<UChar> & result,
                const ZZZImage<T> & source )
{
  const SSize bands = source.bands();
  const SSize depth = source.depth();
  const SSize height = source.height();
  const SSize width = source.width();

  if ( result.bands() != bands ||
       result.size() != source.size() )
    result.alloc( source.size(), source.bands() );

  for ( int band = 0; band < bands; band++ )
    for ( int z = 0; z < depth; ++z )
      for ( int y = 0; y < height; ++y )
        for ( int x = 0; x < width; ++x )
          result(x,y,z,band) = static_cast<UChar>( source(x,y,z,band) );
}

void cast_copy( ZZZImage<UChar> & result,
                const ZZZImage<Bool> & source )
{
  const SSize bands = source.bands();
  const SSize depth = source.depth();
  const SSize height = source.height();
  const SSize width = source.width();

  if ( result.bands() != bands ||
       result.size() != source.size() )
    result.alloc( source.size(), source.bands() );

  for ( int band = 0; band < bands; band++ )
    for ( int z = 0; z < depth; ++z )
      for ( int y = 0; y < height; ++y )
        for ( int x = 0; x < width; ++x )
          result(x,y,z,band) = static_cast<UChar>( source.bit(x,y,z,band) );
}

Size
Document::rawSize()
{
  if ( _valueType == Po_ValB ) {
    return sizeof(UInt) * _image->realBandVectorSize() * _image->bands();
  }
  Size cellSize=0;
  switch( _valueType ) {
  case Po_ValUC:  cellSize = 1; break;
  case Po_ValSS:  cellSize = 2; break;
  case Po_ValUS:  cellSize = 2; break;
  case Po_ValSL:  cellSize = 4; break;
  case Po_ValUL:  cellSize = 4; break;
  case Po_ValSF:  cellSize = 4; break;
  case Po_ValSD:  cellSize = 8; break;
  }
  return cellSize * static_cast<Size>(_image->bands()) * _image->vectorSize();
}

void
Document::demoPlugin()
{
  if ( _valueType != Po_ValUC )
    return;
  valid( false );

  const ZZZImage<UChar> & src = dynamic_cast<const ZZZImage<UChar>&>( *_image );
  ZZZImage<Int32> *result = new ZZZImage<Int32>;

  ::demoPlugin( src,  *result);

  free();
  _valueType = Po_ValSL;
  _image = result;
  _modified = true;
  valid( true );
  emit volumeChanged();
}

PluginDialog *
Document::activePlugin()
{
  return _activePlugin;
}

void
Document::activePlugin( PluginDialog * plugin )
{
  _activePlugin = plugin;
  emit runningPlugin( plugin );
}

void
Document::modified( bool yesno )
{
  _modified = yesno;
}
