/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   QTools.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2008
 * 
 * @brief  Several tools related with Qt classes.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "QTools.h"

#include <QProcess>
#include <QStringList>
#include <iostream>
using namespace std;

QString getEnvVariable( const QString & var )
{
   QStringList env;
   QString prefix = QString("%1=").arg( var );
   QRegExp re( QString("^%1").arg( prefix ) );
   env = QProcess::systemEnvironment().filter( re );
   QString res = env.front();
   return res.replace( prefix, "" );
}


void
addPath( QString & pathVar, QString pathList, QString root )
{
   QStringList paths = pathList.split(":");
   QString newPath;
   QStringList::iterator it = paths.begin();
   QStringList::iterator end = paths.end();
   if ( root.length() > 1 && root.endsWith("/") )
      root.chop(1);
   while ( it != end ) {
      if ( !it->startsWith("/") ) 
	 pathVar += QString(":") + root + "/" + (*it);
      else
	 pathVar += QString(":") + (*it);
      ++it;
   }
}
