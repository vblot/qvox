/**
 * @file   ViewParamsToolBar.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:22 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "globals.h"
#include "ViewParamsToolBar.h"
#include "View3DWidget.h"
#include <QLabel>
#include <QAction>
#include <QCheckBox>
#include <QComboBox>
#include <QPushButton>
#include <QThread>

ViewParamsToolBar::ViewParamsToolBar( QMainWindow * mainWindow,
                                      View3DWidget * view3DWidget ):
  QToolBar( QString( "Surface view parameters" ), mainWindow ),
  _view3DWidget( view3DWidget )
{
  CONS( ViewParamsToolBar );

  addWidget( new QLabel( "Surface shading from " ) );

  _comboShading = new QComboBox( 0 );

  _comboShading->insertItem( static_cast<int>( Surface::ColorFromOrientation ),
                             "Orientation" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromDepth ),
                             "Depth" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromCurvature ),
                             "Curvature" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromMaterial ),
                             "Voxel values" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorWhite ), "White" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFixed ), "Fixed" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromLight ),
                             "Light" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromLightedMaterial ),
                             "Material" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromLightedColorMap ),
                             "ColorMap with light" );
  _comboShading->insertItem( static_cast<int>( Surface::ColorFromLightedLUT ),
                             "Colors LUT" );
  _comboShading->setCurrentIndex( globalSettings->shading() );
  _comboShading->setSizePolicy( QSizePolicy::Fixed,
                                QSizePolicy::Fixed );
  addWidget( _comboShading );

  QAction *refreshShadingAction
      = new QAction(QPixmap( ":/toolbars/Pixmaps/biggear.png" ), QString(), 0 );
  refreshShadingAction->setToolTip( "Refresh shading" );
  connect( refreshShadingAction, SIGNAL( triggered() ),
           &_view3DWidget->surface(), SLOT( refreshShading() ) );
  addAction( refreshShadingAction );

  QCheckBox *cbTransparency = new QCheckBox("Alpha", 0 );
  cbTransparency->setChecked( globalSettings->transparency() );
  connect( cbTransparency, SIGNAL( toggled(bool) ),
           globalSettings, SLOT( transparency(bool ) ) );
  addWidget( cbTransparency );

  QSlider *slider = new QSlider;
  slider->setOrientation( Qt::Horizontal );
  slider->setRange(0,255);
  slider->setValue( globalSettings->opacity() );
  addWidget( slider );
  slider->setMaximumWidth( 120 );
  connect( cbTransparency, SIGNAL( toggled(bool) ),
           slider, SLOT( setEnabled(bool ) ) );
  connect( slider, SIGNAL( valueChanged(int ) ),
           globalSettings, SLOT( opacity(int ) ) );

  _cbFineDrawingMode = new QCheckBox( "Fine drawing" );
  connect( _cbFineDrawingMode, SIGNAL( toggled(bool ) ),
           globalSettings, SLOT( fineDrawingMode(bool) ) );
  addWidget( _cbFineDrawingMode );

  addSeparator();
#if defined(_EXTRA_FUNCTIONS_)
  _cbMultithread = new QCheckBox( "Multithreaded" );
  _cbMultithread->setChecked( globalSettings->multiThreading() );
  addWidget( _cbMultithread );
  connect( _cbMultithread, SIGNAL( toggled(bool ) ),
           globalSettings, SLOT( multiThreading(bool ) ) );
  _pbActivateShading = new QPushButton( "Light" );
  _pbActivateShading->setCheckable( true );
  _pbActivateShading->setChecked( globalSettings->shading() == Surface::ColorFromLight );
  addWidget( _pbActivateShading );
  connect( _pbActivateShading, SIGNAL( toggled(bool) ),
           this, SLOT( lightButtonToggled(bool) ) );

  _cbMaxThreads = new QComboBox;
  addWidget( new QLabel( "Threads" ) );
  addWidget( _cbMaxThreads );
  int nb = QThread::idealThreadCount();
  _cbMaxThreads->insertItem( QString( "max (%1)" ).arg( nb ), -1 );
  for ( int i = 1; i<=nb; ++i )
    _cbMaxThreads->insertItem( QString( "%1" ).arg( i ), i );
  connect( _cbMaxThreads, SIGNAL( currentIndexChanged (int ) ),
           globalSettings, SLOT( maxThreads(int ) ) );
  if ( globalSettings->maxThreads() <= nb )
    _cbMaxThreads->setCurrentIndex( globalSettings->maxThreads() );
#endif

  // Shading combo is an Observer/View of the global shading value
  connect( globalSettings, SIGNAL( shadingChanged(int) ),
           this, SLOT( shadingChanged(int ) ) );

  // Shading combo may modify the global shading value
  connect( _comboShading, SIGNAL( activated(int) ),
           globalSettings, SLOT( shading(int) ) );
  connect( globalSettings, SIGNAL( notify() ),
           this, SLOT( settingsChanged() ) );

  settingsChanged();
}

void
ViewParamsToolBar::setOrientation ( Qt::Orientation orientation )
{
  QToolBar::setOrientation( orientation );
}

void
ViewParamsToolBar::undock()
{
}

void
ViewParamsToolBar::shadingChanged(int i)
{
  _comboShading->setCurrentIndex( i );
#if defined(_EXTRA_FUNCTIONS_)
  _pbActivateShading->setChecked( i == Surface::ColorFromLight );
#endif
}

void
ViewParamsToolBar::settingsChanged()
{
#if defined(_EXTRA_FUNCTIONS_)
  _cbMultithread->setChecked( globalSettings->multiThreading() );
  _cbMaxThreads->setCurrentIndex( globalSettings->maxThreads() );
#endif
  _cbFineDrawingMode->setChecked( globalSettings->fineDrawingMode() );
}

void
ViewParamsToolBar::lightButtonToggled(bool on)
{
  if ( on )
    globalSettings->shading( Surface::ColorFromLight );
  else
    globalSettings->shading( Surface::ColorFromOrientation );
}
