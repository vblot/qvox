/**
 * @file   OfflineRenderer.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:20:41 2005
 *
 * @brief  3D surface view Widget class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "OfflineRenderer.h"
#include <Settings.h>

#include "Array.h"

using std::cout;
using std::endl;
using std::numeric_limits;

OfflineRenderer::OfflineRenderer( const char * filename ):_image( 320, 200, QImage::Format_RGB32 )
{
  CONS( OfflineRenderer );
  _document.load( filename );
  _surface.setImage( _document.image(), false, Surface::KeepViewParams, ADJ18 );
  _surface.rotate( 2.8, 0.3, .7, Surface::RotateAllSurfels );
  _surface.zoomFit( 320, 200 );
  _surface.setShading( Surface::ColorFromLight );

  int iterations = globalSettings->normalsEstimateIterations();
  _surface.computeNormals( iterations, false  );

  _colorMap.type( ColorMap::Light );
  _surface.drawSurface( _image, _colorMap );
}

void
OfflineRenderer::save( const char * filename )
{
  if ( filename )
    _image.save( QString( filename ), QFileInfo( QString( filename ) ).suffix().toUpper().toLatin1() );
  else
    _image.save( QString( "QVox_Output.png" ), "PNG" );
}

void
OfflineRenderer::drawAscii()
{
#ifdef _IS_UNIX_
  char *var = getenv("COLUMNS");
  int w = var ? atoi(var) : 80;
  var = getenv( "LINES" );
  int h = var ? atoi(var) : 24;
#else
  int w = 80;
  int h = 24;
#endif

  char letters[] = " @OPLl05.<>�&/\\|+][#~_'`Wop";

  QImage img;
  QImage lettersImage(":/Pixmaps/letters.png","PNG");

  double diff, diffMin;
  int minLetter;
  int row, col, x, y, dx, dy, dx_letter, dy_letter;
  int letter;
  Array<char> text( w + 2, h );

  float angle = M_PI / 64.0;
  int n = static_cast<int>( 8 * M_PI / angle );

  srand( time(0) );
  float theta = ( rand() % 25 ) / 100.0;;
  float phi = ( rand() % 25 ) / 100.0;;

  while ( n-- ) {
    _surface.rotate( angle, theta, phi );
    _surface.drawSurface( _image, _colorMap );
    img = _image.scaled( w * 14, h * 17 );
    for ( row = 0; row < h; ++row ) {
      for ( col = 0; col < w; ++col ) {
        diffMin = numeric_limits<float>::max();
        minLetter = '@';
        dx = col * 14;
        dy = row * 17;
        for ( letter = 0 /* 32 */; letter < 10 /* 27 */ ; ++letter ) {
          dx_letter = letter * 15;
          dy_letter = 2;
          diff = 0.0;
          for ( x = 2; x < 12; ++x )
            for ( y = 2; y < 15; ++y ) {
              diff += fabs( qRed( img.pixel( x + dx, y + dy ) ) -
                            static_cast<float>( qRed( lettersImage.pixel( x + dx_letter, y + dy_letter ) ) ) );
            }
          if ( diff < diffMin ) { diffMin = diff; minLetter = letter; }
        }
        text( col, row ) = static_cast<char>( letters[ minLetter ] );
      }
      text( col, row ) = 0;
    }
    cout << endl;
    for ( row = 0; row < h; ++row )
      cout << endl << text[row];
    cout << std::flush;
  }

}
