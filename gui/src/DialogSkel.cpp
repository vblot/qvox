/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogSkel.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 *
 * @brief  Declaration of the class DialogSkel
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogSkel.h"

#include "Document.h"
#include "View3DWidget.h"
#include "Progress.h"

/**
 * Constructor
 */
DialogSkel::DialogSkel(QWidget *parent):QDialog(parent),_document(0),_view3DWidget(0) {
  CONS( DialogSkel );
  setupUi( this );
  
  cbAdj->insertItem( 0, "(6,26)");
  cbAdj->insertItem( 1, "(6,18)" );
  cbAdj->insertItem( 2, "(18,6)" );
  cbAdj->insertItem( 3, "(26,6)" );
  cbAdj->setCurrentIndex( globalSettings->adjacencyPair() );
  connect( cbAdj, SIGNAL( activated(int) ),
           globalSettings, SLOT( adjacencyPair(int) ) );
  connect( globalSettings, SIGNAL( adjacencyPairChanged(int) ),
           cbAdj, SLOT( setCurrentIndex(int) ) );
  connect( pbOnePass, SIGNAL( clicked() ),
           this, SLOT( applyOnePass() ) );
  connect( pbComplete, SIGNAL( clicked() ),
           this, SLOT( applyComplete() ) );
}

DialogSkel::~DialogSkel()
{
}

void
DialogSkel::done( int r )
{
  emit closing();
  QDialog::done( r );
}

void
DialogSkel::closeEvent( QCloseEvent * )
{
  emit closing();
}

void DialogSkel::document( Document * document )
{
  _document = document;
}

void
DialogSkel::applyOnePass()
{
  if ( !_document ) return;
  int terminalTest = 0;
  if ( rbBasicSurface->isChecked() ) terminalTest = 1;
  if ( rbIsthmus->isChecked() ) terminalTest = 2;
  if ( rbExtremity->isChecked() ) terminalTest = 3;
  if ( rbSurfaceJunction->isChecked() ) terminalTest = 4;
  if ( rbBertrand->isChecked() ) terminalTest = 5;
  if ( rbBertrandExt->isChecked() ) terminalTest = 6;
  if ( rbStrongSurfaces->isChecked() ) terminalTest = 7;
  if ( rbStrongSurfacesExt->isChecked() ) terminalTest = 8;
  globalProgressReceiver.showMessage("Thinning...");
  _document->skel( true,
                   globalSettings->adjacencyPair(),
                   (rbParallel->isChecked() ? Document::SkelParallel : Document::SkelSequential),
                   terminalTest );
  globalProgressReceiver.showMessage(0);
}

void
DialogSkel::applyComplete()
{
  if ( !_document ) return;
  int terminalTest = 0;
  if ( rbBasicSurface->isChecked() ) terminalTest = 1;
  if ( rbIsthmus->isChecked() ) terminalTest = 2;
  if ( rbExtremity->isChecked() ) terminalTest = 3;
  if ( rbSurfaceJunction->isChecked() ) terminalTest = 4;
  if ( rbBertrand->isChecked() ) terminalTest = 5;
  if ( rbBertrandExt->isChecked() ) terminalTest = 6;
  if ( rbStrongSurfaces->isChecked() ) terminalTest = 7;
  if ( rbStrongSurfacesExt->isChecked() ) terminalTest = 8;
  globalProgressReceiver.showMessage("Thinning...");
  _document->skel( false,
                   globalSettings->adjacencyPair(),
                   rbParallel->isChecked() ? Document::SkelParallel : Document::SkelSequential,
                   terminalTest );
  globalProgressReceiver.showMessage(0);
}

void
DialogSkel::setView3DWidget( View3DWidget * widget )
{
  _view3DWidget = widget;
}
