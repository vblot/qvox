/**
 * @file   ColorMapView.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:50 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ColorMapView.h"

#include "globals.h"

#include <QObject>
#include <QMenu>
#include <QCursor>
#include <QMouseEvent>
#include <QPainter>

#include "ColorMap.h"

ColorMapView::ColorMapView( QWidget *parent, ColorMap *colorMap ):
  QWidget( parent ), _colorMap(colorMap)
{
  CONS( ColorMapView );
  setAutoFillBackground(false);
  setMinimumWidth( 256 );
  setSizePolicy( QSizePolicy::Minimum, QSizePolicy::MinimumExpanding );
  connect( _colorMap, SIGNAL( changed( int ) ),
	   this, SLOT( repaint() ) );
}

ColorMapView::~ColorMapView()
{
}

void
ColorMapView::mouseReleaseEvent ( QMouseEvent *e )
{
  if ( e->button() == Qt::RightButton ) {
    int type = _colorMap->type() + 1;
    if ( type == ColorMap::Undefined ) type = 0;
    _colorMap->type( static_cast<ColorMap::Type>( type ) );
  }
  if ( e->button() == Qt::LeftButton ) {
    if ( _colorMap->type() == 0 ) 
      _colorMap->type( static_cast<ColorMap::Type>( ColorMap::Undefined - 1 ) );
    else 
      _colorMap->type( static_cast<ColorMap::Type>( _colorMap->type() - 1 ) );
  }  
}

void
ColorMapView::paintEvent( QPaintEvent *)
{
  redraw();
}

void
ColorMapView::redraw()
{
  QPainter painter( this );
  const QColor * qcolors = _colorMap->qcolors();
  int h = height();
  for ( int i = 0; i < 256; i++) {
    painter.setPen( qcolors[ i ]  );
    painter.drawLine( i, 0, i, h );
  }  
}


