/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogAspect.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class DialogAspect
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogAspect.h"
#include "View3DWidget.h"
#include "FloatValidator.h"

/**
 * Constructor
 */
DialogAspect::DialogAspect(QWidget *parent) : QDialog(parent) {
  CONS( DialogAspect );
  setupUi(this);

  connect( cbAspectEnabled, SIGNAL( toggled(bool) ),
           this, SLOT( toggleAspect(bool)));
  _validator = new FloatValidator( this, 0.1, 4.0, 3);
  leAspectX->setValidator( _validator );
  leAspectY->setValidator( _validator );
  leAspectZ->setValidator( _validator );
  connect( slAspectX, SIGNAL( valueChanged(int) ),
	   this, SLOT(slXMoved(int)));
  connect( slAspectY, SIGNAL( valueChanged(int) ),
	   this, SLOT(slYMoved(int)));
  connect( slAspectZ, SIGNAL( valueChanged(int) ),
	   this, SLOT(slZMoved(int)));
  connect( pbDefaultValues, SIGNAL( clicked()), 
	   this, SLOT( defaultValues() ) );
  connect( leAspectX, SIGNAL( editingFinished() ), 
	   this, SLOT( leAspectXChanged() ) );
  connect( leAspectY, SIGNAL( editingFinished() ),
	   this, SLOT( leAspectYChanged() ) );
  connect( leAspectZ, SIGNAL( editingFinished() ),
	   this, SLOT( leAspectZChanged() ) );
  connect( cbAspectEnabled, SIGNAL( toggled(bool) ), 
	   this, SLOT( toggleAspect(bool ) ) );
  cbAspectEnabled->setChecked( false);
  _view3DWidget = 0;
  defaultValues();
  toggleAspect( false );
}

DialogAspect::~DialogAspect()
{
  dispose( _validator );
}

void DialogAspect::toggleAspect( bool on )
{
  if ( ! on ) {
    leAspectX->setEnabled( false );
    leAspectZ->setEnabled( false );
    leAspectY->setEnabled( false );
    slAspectX->setEnabled( false );
    slAspectY->setEnabled( false );
    slAspectZ->setEnabled( false );
    pbDefaultValues->setEnabled( false );
    if ( _view3DWidget ) _view3DWidget->aspect(-1,-1,-1);
  } else {
    leAspectX->setEnabled( true );
    leAspectZ->setEnabled( true );
    leAspectY->setEnabled( true );
    slAspectX->setEnabled( true );
    slAspectY->setEnabled( true );
    slAspectZ->setEnabled( true );
    pbDefaultValues->setEnabled( true );
    commitAspect();
  }
}

bool DialogAspect::enabled()
{
  return cbAspectEnabled->isChecked();
}

void DialogAspect::getAspect( float & x, float & y, float & z)
{
  x = _aspectX;
  y = _aspectY;
  z = _aspectZ;
}

void DialogAspect::slXMoved( int pos )
{
  _aspectX = pos / 100.0;
  leAspectX->setText( QString("%1").arg( _aspectX, 0, 'f', 3 ));
  commitAspect();
}

void DialogAspect::slYMoved( int pos )
{
  _aspectY = pos / 100.0;
  leAspectY->setText( QString("%1").arg( _aspectY, 0, 'f', 3 ));
  commitAspect();
}

void DialogAspect::slZMoved( int pos )
{
  _aspectZ = pos / 100.0;
  leAspectZ->setText( QString("%1").arg( _aspectZ, 0, 'f', 3 ));
  commitAspect();	
}

void DialogAspect::defaultValues()
{
  _aspectX = _aspectY = _aspectZ = 1.0;
  slAspectX->setValue( 100 );
  slAspectY->setValue( 100 );
  slAspectZ->setValue( 100 );
  leAspectX->setText( "1.000" );
  leAspectY->setText( "1.000" );
  leAspectZ->setText( "1.000" );
  commitAspect();
}

void DialogAspect::leAspectXChanged()
{
  _aspectX = leAspectX->text().toFloat(0);
  slAspectX->setValue( static_cast<int>( _aspectX * 100 ) );
  commitAspect();	
}

void DialogAspect::leAspectYChanged()
{
  _aspectY = leAspectY->text().toFloat(0);
  slAspectY->setValue( static_cast<int>( _aspectY * 100 ) );
  commitAspect();	
}

void DialogAspect::leAspectZChanged()
{
  _aspectZ = leAspectZ->text().toFloat(0);
  slAspectZ->setValue( static_cast<int>( _aspectZ * 100 ) );
  commitAspect();
}

void DialogAspect::setView3DWidget( View3DWidget * widget )
{
  _view3DWidget = widget;
}

void DialogAspect::commitAspect()
{
  if ( _view3DWidget ) 
    _view3DWidget->aspect( _aspectX, _aspectY, _aspectZ );
}

