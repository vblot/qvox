/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogSettings.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 *
 * @brief  Declaration of the class DialogSettings
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogSettings.h"

#include "arch_info.h"
#include "Convolution.h"
#include "Settings.h"
#include <QColor>
#include <QPixmap>
#include <QIcon>
#include <QColorDialog>
#include <QButtonGroup>
#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QThreadPool>
#include <QApplication>

#include <cstdlib>

#include "MainWindow.h"

/**
 * Constructor
 */
DialogSettings::DialogSettings(MainWindow *parent):QDialog(parent) {
  CONS( DialogSettings );
  setupUi( this );

  _mainWindow = parent;
  settingsChanged();


  _minimalFPSChanged = false;

  QButtonGroup * bgCurvatureType = new QButtonGroup( this );
  bgCurvatureType->addButton( rbGaussian, GAUSSIAN_CURVATURE );
  bgCurvatureType->addButton( rbMean, MEAN_CURVATURE );
  bgCurvatureType->addButton( rbMaximum, MAXIMUM_CURVATURE );

  QPixmap p(60,40);
  p.fill( globalSettings->exportOFFColor() );
  pbExportOFFColor->setIcon( QIcon( p ) );

  pList->clear();
  QStringList::iterator it = globalSettings->pluginsFiles().begin();
  QStringList::iterator end = globalSettings->pluginsFiles().end();
  while ( it != end ) {
    pList->addItem( QFileInfo( *it ).fileName() );
    ++it;
  }

  connect( bgCurvatureType, SIGNAL( buttonClicked( int ) ),
           globalSettings, SLOT( curvatureType( int ) ) );
  connect( sbNormalsEstimateIterations, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( normalsEstimateIterations(int) ) );
  connect( cbNormalsEstimateAdaptive, SIGNAL( toggled(bool) ),
           globalSettings, SLOT( normalsEstimateAdaptive(bool) ) );
  connect( sbExportOFFIterations, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( exportOFFIterations(int) ) );
  connect( rbExportOFFsurfelsColors, SIGNAL( toggled(bool) ),
           globalSettings, SLOT( exportOFFsurfelsColors(bool) ) );
  connect( rbExportOFFfixedColor, SIGNAL( toggled(bool) ),
           this, SLOT( exportOFFfixedColor(bool) ) );
  connect( cbExportOFFAdaptive, SIGNAL( toggled(bool) ),
           globalSettings, SLOT( exportOFFAdaptive(bool) ) );
  connect( sbCurvatureEstimateIterations, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( curvatureEstimateIterations(int) ) );
  connect( sbCurvatureEstimateAveragingIterations, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( curvatureEstimateAveragingIterations(int) ) );
  connect( sbRotationBoxMaxFPS, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( rotationBoxMaxFrames(int) ) );
  connect( sbRotationBoxMaxFPS, SIGNAL( valueChanged(int) ),
           this, SLOT( rotationBoxMaxFPSChanged(int) ) );

  connect( cbCurvatureEstimateAdaptive, SIGNAL( toggled(bool) ),
           globalSettings, SLOT( curvatureEstimateAdaptive(bool) ) );
  connect( sbFitFrameSize, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( fitBoundingBoxFrameSize(int) ) );
  connect( comboDWTSize, SIGNAL( activated(int) ),
           this, SLOT( comboDWTSizeChanged(int) ) );
  connect( sbMaxHeight, SIGNAL( valueChanged(int) ),
           globalSettings, SLOT( levelMapMaxHeight(int) ) );
  connect( globalSettings, SIGNAL( notify() ),
           this, SLOT( settingsChanged() ) );
  connect( pbExportOFFColor, SIGNAL( clicked() ),
           this, SLOT( pbExportOFFColorClicked() ) );
  connect( pbSaveSettings, SIGNAL( clicked() ),
           this, SLOT( pbSaveSettingsClicked() ) );
  connect( cbSaveSettings, SIGNAL( toggled(bool) ),
           this, SLOT( cbSaveSettingsToggled(bool) ) );

  connect( pbAddPluginsMenu, SIGNAL( clicked() ),
           this, SLOT( addPluginsMenu() ) );
  connect( pbRemovePluginsMenu, SIGNAL( clicked() ),
           this, SLOT( removePluginsMenu() ) );
  connect( pbPluginUp, SIGNAL( clicked() ),
           this, SLOT( moveUpPluginsMenu() ) );
  connect( pbPluginDown, SIGNAL( clicked() ),
           this, SLOT( moveDownPluginsMenu() ) );
  connect( pbRebuildPluginsMenu, SIGNAL( clicked() ),
           _mainWindow, SLOT( buildPluginsMenus() ) );
  connect( pbClearPlugins, SIGNAL( clicked() ),
           this, SLOT( clearPluginsMenus() ) );

  connect( cbMultiThread, SIGNAL( toggled(bool ) ),
           globalSettings, SLOT( multiThreading(bool ) ) );

  cbComputeIndices->setChecked(globalSettings->computeVextexIndices());
  connect( cbComputeIndices, SIGNAL(toggled(bool)),
           globalSettings, SLOT(computeVertexIndices(bool)));


  int m = QThreadPool::globalInstance()->maxThreadCount();
  if ( m < globalSettings->maxThreads() )
    globalSettings->maxThreads(-1);
  if ( m >= 2 ) {
    for ( int i = 2; i < m; ++ i ) {
      cbMaxThreads->addItem(QString("%1 threads").arg(i),QVariant(i));
    }
    cbMaxThreads->addItem(QString("%1 threads (Max)").arg(m),QVariant(-1));
    if ( globalSettings->maxThreads() == -1 )
      cbMaxThreads->setCurrentIndex(cbMaxThreads->count()-1);
    else
      cbMaxThreads->setCurrentIndex(globalSettings->maxThreads()-2);
  } else {
    cbMaxThreads->addItem(QString("1 thread (Max)"),QVariant(1));
    cbMaxThreads->setCurrentIndex(0);
    cbMultiThread->setChecked(false);
    cbMultiThread->setEnabled(false);
    globalSettings->multiThreading(false);
    globalSettings->maxThreads(1);
  }
  cbMaxThreads->setEnabled(globalSettings->multiThreading());
  connect( cbMaxThreads, SIGNAL(currentIndexChanged(int)),
           this, SLOT(maxThreadsChanged(int)));
  connect( cbMultiThread, SIGNAL(toggled(bool)),
           this, SLOT(cbMultiThreadingToggled(bool)));

#ifdef _IS_UNIX_
  connect( pbCreateLocalPlugins, SIGNAL( clicked() ),
           this, SLOT( createLocalPluginsFile() ) );
#else
  pbCreateLocalPlugins->setEnabled( false );
#endif

  connect( buttonOk, SIGNAL( clicked() ),
           this, SLOT( accept() ) );

  _optionalMessagesCheckBoxes = new QButtonGroup( this );
  _optionalMessagesCheckBoxes->setExclusive( false );
  connect( _optionalMessagesCheckBoxes, SIGNAL( buttonClicked( QAbstractButton * ) ),
           this, SLOT( optMessageCheckClicked( QAbstractButton * ) ) );

  _optionalMessagesCheckBoxes->addButton( cbAutoFullBox, Settings::FullBox );
  cbAutoFullBox->setChecked( globalSettings->hasOptionalMessage( Settings::FullBox ) );

  if ( _NB_PROCESSORS_ == 1 ) {
    globalSettings->multiThreading(false);
    cbMultiThread->setChecked(  globalSettings->multiThreading() );
    cbMultiThread->setEnabled( false );
  }
}

DialogSettings::~DialogSettings()
{
}

bool DialogSettings::minimalFPSChanged()
{
  return _minimalFPSChanged;
}

void
DialogSettings::cbSaveSettingsToggled( bool on )
{
  globalSettings->saveOnExit( on );
}


void
DialogSettings::pbSaveSettingsClicked()
{
  globalSettings->save();
}

void
DialogSettings::comboDWTSizeChanged( int n )
{
  globalSettings->dwtFinalSize( 1 << (n+1) );
}

void
DialogSettings::exportOFFfixedColor( bool b )
{
  pbExportOFFColor->setEnabled( b );
}

void
DialogSettings::pbExportOFFColorClicked()
{
  QColor color = globalSettings->exportOFFColor();
  color = QColorDialog::getColor( );
  if ( color.isValid() ) {
    QPixmap p(60,40);
    p.fill( color );
    pbExportOFFColor->setIcon( QIcon( p ) );
    globalSettings->exportOFFColor( color );
  }
}

void
DialogSettings::optMessageCheckClicked( QAbstractButton * button )
{
  Settings::OptionalMessage message;
  message = static_cast<Settings::OptionalMessage>( _optionalMessagesCheckBoxes->id( button ) );
  if ( button->isChecked() ) {
    globalSettings->addOptionalMessage( message );
  } else {
    globalSettings->removeOptionalMessage( message );
  }
}

void
DialogSettings::createLocalPluginsFile()
{
#ifdef _IS_UNIX_
  QString sharePath = QFileInfo( qApp->arguments()[0] ).absolutePath() + "/../share/qvox";
  QFile pluginTemplate( sharePath + "/qvox_plugins.qpx" );
  QString pluginFileName = QString("%1/.qvox_plugins.qpx").arg( std::getenv("HOME") );
  QFile pluginFile( pluginFileName );
  bool creation = true;
  if ( pluginFile.exists() ) {
    creation = false;
    QMessageBox::StandardButton button;
    button = QMessageBox::warning( QApplication::activeWindow(),
                                   "File alreay exists",
                                   QString( "File already exists.\n(%1)\n\nAre you sure that you "
                                            "want to overwrite it?" ).arg( pluginFileName ) ,
                                   QMessageBox::Yes|QMessageBox::No,
                                   QMessageBox::No );
    if ( button == QMessageBox::Yes )
      pluginFile.remove();
    else
      return;
  }

  if ( QFile::copy( pluginTemplate.fileName(), pluginFile.fileName() ) && creation )
    QMessageBox::information( QApplication::activeWindow(),
                              "Information",
                              QString("A sample personnal plugin file has been created at:\n\n"
                                      "%1"
                                      "\n\nIt's an XML file you can easily adapt to your needs.\n"
                                      "(You must to restart QVox for menu entries to show up.)")
                              .arg( pluginFile.fileName() ),
                              QMessageBox::Ok,
                              QMessageBox::Ok );
  std::cout << "FNAME: "
            << pluginFile.fileName().toLatin1().constData()
            << std::endl;
#endif
}

void
DialogSettings::settingsChanged()
{
  sbNormalsEstimateIterations->setValue( globalSettings->normalsEstimateIterations() );
  cbNormalsEstimateAdaptive->setChecked( globalSettings->normalsEstimateAdaptive() );
  sbCurvatureEstimateIterations->setValue( globalSettings->curvatureEstimateIterations() );
  sbCurvatureEstimateAveragingIterations->setValue( globalSettings->curvatureEstimateAveragingIterations() );
  cbCurvatureEstimateAdaptive->setChecked( globalSettings->curvatureEstimateAdaptive() );

  sbFitFrameSize->setValue( globalSettings->fitBoundingBoxFrameSize() );
  sbMaxHeight->setValue( globalSettings->levelMapMaxHeight() );
  sbExportOFFIterations->setValue( globalSettings->exportOFFIterations() );
  rbExportOFFsurfelsColors->setChecked( globalSettings->exportOFFsurfelsColors() );
  rbExportOFFfixedColor->setChecked( ! globalSettings->exportOFFsurfelsColors() );
  cbSaveSettings->setChecked( globalSettings->saveOnExit() );
  cbExportOFFAdaptive->setChecked( globalSettings->exportOFFAdaptive() );

  pbExportOFFColor->setEnabled( ! globalSettings->exportOFFsurfelsColors() );

  cbMultiThread->setChecked( globalSettings->multiThreading() );
  sbRotationBoxMaxFPS->setValue( globalSettings->rotationBoxMaxFrames() );

  int i = 0;
  int n = globalSettings->dwtFinalSize();
  n >>= 2;
  while ( n ) {
    ++i; n >>= 1;
  }
  comboDWTSize->setCurrentIndex( i );

  if ( globalSettings->curvatureType() == QVoxSettings::Mean ) {
    rbMean->setChecked( true );
    rbGaussian->setChecked( false);
    rbMaximum->setChecked( false);
  }

  if ( globalSettings->curvatureType() == QVoxSettings::Gaussian ) {
    rbMean->setChecked( false );
    rbGaussian->setChecked( true );
    rbMaximum->setChecked( false);
  }

  if ( globalSettings->curvatureType() == QVoxSettings::Maximum ) {
    rbMean->setChecked( false );
    rbGaussian->setChecked( false );
    rbMaximum->setChecked( true );
  }

  cbAutoFullBox->setChecked( globalSettings->hasOptionalMessage( Settings::FullBox ) );
}

void DialogSettings::maxThreadsChanged(int i)
{
  globalSettings->maxThreads(cbMaxThreads->itemData(i).toInt());
}

void DialogSettings::cbMultiThreadingToggled(bool on)
{
  cbMaxThreads->setEnabled(on);
}

void DialogSettings::rotationBoxMaxFPSChanged(int)
{
  _minimalFPSChanged = true;
}

void DialogSettings::showEvent(QShowEvent *)
{
  _minimalFPSChanged = false;
}

void
DialogSettings::managePlugins()
{
  tabWidget->setCurrentIndex( 4 );
  show();
}

void
DialogSettings::addPluginsMenu()
{
  QString f = QFileDialog::getOpenFileName( this,
                                            "Select a plugin file",
                                            QString(),
                                            QString("QVox plugins description XML files (*.qpx)"),
                                            0,
                                            0 );
  if ( f.size() ) {
    pList->addItem( QFileInfo( f ).fileName() );
    globalSettings->pluginsFiles() << QFileInfo( f ).absoluteFilePath();
  }
}

void
DialogSettings::removePluginsMenu()
{
  int selectedItem = pList->currentRow();
  if ( selectedItem == -1 ) return;
  if ( pList->currentItem()->isSelected() ) {
    pList->model()->removeRows( selectedItem, 1 );
    globalSettings->pluginsFiles().removeAt( selectedItem );
  }
}

void
DialogSettings::moveUpPluginsMenu()
{
  int selectedItem = pList->currentRow();
  if ( selectedItem == -1 ) return;
  if ( pList->currentItem()->isSelected() && selectedItem > 0 ) {
    QString text = pList->item( selectedItem )->text();
    pList->model()->removeRows( selectedItem, 1 );
    globalSettings->pluginsFiles().removeAt( selectedItem );
    pList->insertItem( selectedItem-1, text );
    globalSettings->pluginsFiles().insert( selectedItem-1, text );
    pList->setCurrentItem( pList->item( selectedItem-1 ) );
  }
}

void
DialogSettings::moveDownPluginsMenu()
{
  int selectedItem = pList->currentRow();
  if ( selectedItem == -1 ) return;
  if ( pList->currentItem()->isSelected() && selectedItem < pList->model()->rowCount()-1 ) {
    QString text = pList->item( selectedItem )->text();
    pList->model()->removeRows( selectedItem, 1 );
    globalSettings->pluginsFiles().removeAt( selectedItem );
    pList->insertItem( selectedItem+1, text );
    globalSettings->pluginsFiles().insert( selectedItem+1, text );
    pList->setCurrentItem( pList->item( selectedItem+1 ) );
  }
}

void
DialogSettings::clearPluginsMenus()
{
  pList->clear();
  globalSettings->pluginsFiles().clear();
}

