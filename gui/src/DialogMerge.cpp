/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogMerge.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class DialogMerge
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogMerge.h"

#include "Dialogs.h"

#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDir>

/**
 * Constructor
 */
DialogMerge::DialogMerge( QWidget *parent ):QDialog( parent ) {
  CONS( DialogMerge );
  setupUi( this );

  connect( pbBrowse, SIGNAL( clicked() ),
			  this, SLOT( browse() ) );
  
  QFileInfo fileInfo( "dummy.tmp" );
  fileInfo.makeAbsolute();
  leFileName->setText( "" );
  labelDirectory->setText( fileInfo.dir().absolutePath() );  
  connect( leFileName, SIGNAL( textChanged( const QString &) ),
	   this, SLOT( fileNameChanged( const QString & ) ) );  
}



DialogMerge::~DialogMerge(){

}

void DialogMerge::browse()
{
  QString s =  _dialogs->askOpenFileName( VolumeFile,this );
  if ( s != QString::null )  {
    _fileName = s;
    QFileInfo fileInfo( s );
    leFileName->setText( fileInfo.fileName() );
    labelDirectory->setText( fileInfo.dir().absolutePath() );
  }
}


QString DialogMerge::fileName()
{
  return _fileName;
}

int DialogMerge::axis()
{
  if ( rbAxisOx->isChecked() ) return 0;
  if ( rbAxisOy->isChecked() ) return 1;
  if ( rbAxisOz->isChecked() ) return 2;
  if ( rbAxisNone->isChecked() ) return 3;
  return -1;
}

void DialogMerge::fileNameChanged( const QString & text )
{
  QFileInfo fileInfo( labelDirectory->text() + "/" + text );
  if ( fileInfo.isReadable() )
    _fileName = text;
}


void DialogMerge::dialogs( Dialogs * dialogs )
{
  _dialogs = dialogs;
}

