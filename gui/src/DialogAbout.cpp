/**  -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogAbout.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class DialogAbout
 * 
 * \@copyright
 * Copyright or © or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogAbout.h"

#include "tools.h"
#include <QColor>
#include <QFile>
#include <iostream>

DialogAbout::DialogAbout(QWidget *parent) : QDialog(parent) {
  CONS( DialogAbout );
  setupUi(this);
  _hue = 1;  
  QFile licence( ":/CeCILL_2.html" );
  licence.open(QIODevice::ReadOnly);
  QByteArray array = licence.readAll();
  array.push_back( '\0' );
  textLicence->setText( array.data() );
  
  
  textLabel->setText(

        QString::fromLatin1("<html><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"
        "<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; text-indent:0px;\">"
        "QVox " VERSION_STRING "<br />(c) 2005 Sébastien Fourey<br />Image Team<br />Research Group in Computer Science,<br/>Image, Control, and Instrumentation<br/> of Caen (GREYC)</p>"
        "<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
        "<a href=\"http://foureys.users.greyc.frQVox/\">http://foureys.users.greyc.fr/QVox/</a></p>"
        "<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
        "Sebastien.Fourey" "@" "ensicaen.fr</p></body></html>")
        );
}

void DialogAbout::done( int n )
{
   QDialog::done( n );
}

void DialogAbout::accept()
{
   QDialog::accept();
}

bool DialogAbout::event(QEvent *e)
{
   return QDialog::event(e);
}

