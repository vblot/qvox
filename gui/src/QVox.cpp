/**
 * @file   QVox.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:40:06 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <QImage>
#include <QPainter>
#include <QFileInfo>
#include <QImageWriter>
#include <QTextCodec>
#include "Settings.h"

#include "tools.h"
#include "MainWindow.h"
#include "Settings.h"
#include "OfflineRenderer.h"
#include <iostream>

#ifdef _IS_UNIX_
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#endif

int
main( int argc, char ** argv )
{
  QApplication a( argc, argv );
  QCoreApplication::setOrganizationName("GREYC");
  QCoreApplication::setApplicationName("QVox");

  globalSettings = new Settings;
  globalSettings->load();

  if ( findArg(argc, argv, "--help") ) {
    QString str;
    {
      QList<QByteArray> formats = QImageWriter::supportedImageFormats();
      QList<QByteArray>::const_iterator it = formats.begin(), end = formats.end();
      while ( it != end  ) {
        str += QString(" ") + QString( *it ).toLower();
        ++it;
      }
    }
    std::cerr << "Usage: \n"
              << "   qvox [options] [file | -] [-]\n"
              << "\n"
              << "  If - is given as an input file, stdin is read.\n"
              << "  If - is the second argument, the input file is\n"
              << "  sent to stdout as a PAN (Pandore) file.\n"
              << "\n"
              << " Options:\n"
             #if defined(_IS_UNIX_) && defined(WITH_FORK) && !defined(_NO_FORK_)
              << "  -nf | --no-fork : Do not fork. \n"
             #endif
              << "  --output file\n"
              << "     Just create a 2D view saved in filename.\n"
              << "     (Supported formats are:" << str.toLatin1().data() << ")\n"
              << "  --ascii\n"
              << "     Dump 2D views with ASCII art.\n"
              << "\n";
    exit( 0 );
  }
  
  MainWindow mainWindow;
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  char * arg;
  QSize size(0,0);

  if ( ( arg = findArg( argc, argv, "--geometry" ) ) ) {
    QStringList args = QString( arg ).split("x");
    size.setWidth( args[0].toInt() );
    size.setHeight( args[1].toInt() );
  }

  if ( ( arg = findArg( argc, argv, "--output" ) )  &&
       QFileInfo( argv[ argc - 1 ] ).isReadable() ) {
    OfflineRenderer renderer( argv[ argc - 1 ] );
    renderer.save( findArg( argc, argv, "--output" ) );
    exit( 0 );
  }

  if ( findArg( argc, argv, "--ascii" ) ) {
    OfflineRenderer renderer( argv[ argc - 1 ] );
    renderer.drawAscii();
    exit( 0 );
  }
#if defined(_IS_UNIX_)
  QProcess * virtualPointer = 0;
  if ( ( arg = findArg( argc, argv, "--pointer" ) ) ) {
    virtualPointer = new QProcess( &a );
    mainWindow.view3DWidget().virtualPointer( virtualPointer );
    virtualPointer->start( arg, QIODevice::ReadOnly );
    std::cout << "Virtual Pointer : " << arg << std::endl;
    arg[0] = '+';
  }
#endif

  if ( argc >= 3 &&  ( ( !strcmp( argv[ argc - 2 ] , "-" ) && !strcmp( argv[ argc - 1 ] , "-"  ) )
                       || ( QFileInfo( argv[ argc - 2 ] ).isReadable() && !strcmp( argv[ argc - 1 ] , "-" ) ) ) ) {
    mainWindow.dumpVolume( true );
    mainWindow.setFileName( argv[ argc - 2 ] );
  }
  else if ( argc >= 2 && ( !strcmp( argv[ argc - 1 ] , "-") || QFileInfo( argv[ argc - 1 ] ).isReadable() ) ) {
    mainWindow.setFileName( argv[ argc - 1] );
  } else {
    mainWindow.setFileName( QString() );
  }

#if defined(_IS_UNIX_) && defined(WITH_FORK) && !defined(_NO_FORK_)
  if ( ! findArg(argc,argv,"-nf" )  &&
       ! findArg(argc,argv,"--no-fork") ) {
    /*
     * fork(), then father returns immediatly.
     */
    int pid = fork();
    if ( pid == -1 ) {
      perror("fork()");
      exit( -1 );
    }
    if ( pid ) exit( 0 );
  }
#endif // defined(IS_UNIX) && defined(WITH_FORK)
  
  if ( size.width() * size.height() )
    mainWindow.resize( size );
  mainWindow.show();
  
  return a.exec();
}
