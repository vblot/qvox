/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginProgressWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginProgressWidget
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "PluginProgressWidget.h"

#include <QPushButton>
#include <QProgressBar>
#include <QHBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QShowEvent>
#include <QHideEvent>

#include "PluginDialog.h"

/**
 * Constructor
 */
PluginProgressWidget::PluginProgressWidget(QWidget * parent )
   : QWidget( parent ), _plugin( 0 ) {

   QHBoxLayout * hbox = new QHBoxLayout;
   setLayout( hbox );
   hbox->setContentsMargins( 0, 0, 0, 0 );
   hbox->setSpacing( 0 );

   QLabel * label = new QLabel( "Running plugin", this );
   _pushButton = new QPushButton( "Abort", this );
   _progress = new QProgressBar( this );
   hbox->addWidget( label );
   hbox->addWidget( _progress );
   hbox->addWidget( _pushButton );
   _progress->setRange( 0, 19 );
   _progress->setValue( 1 );
   _progress->setTextVisible( false );
   _progress->setMaximumWidth( _progress->sizeHint().width() / 2 );
   _timer = new QTimer( this );
   _timer->setInterval( 50 );
   connect( _timer, SIGNAL( timeout() ),
	    this, SLOT( tick() ) );
}

PluginProgressWidget::~PluginProgressWidget()
{
   disconnect( _timer, SIGNAL( timeout() ),
	       this, SLOT( tick() ) );   
   _timer->stop();
}


void
PluginProgressWidget::plugin( PluginDialog * plugin )
{
   if ( _plugin ) 
      disconnect( _pushButton, SIGNAL( clicked() ),
		  plugin, SLOT( terminate() ) );   
   _plugin = plugin;
   connect( _pushButton, SIGNAL( clicked() ),
	    plugin, SLOT( terminate() ) );
}

void
PluginProgressWidget::showEvent( QShowEvent * event )
{
   event->accept();
   _timer->start();
}

void
PluginProgressWidget::hideEvent( QHideEvent * event )
{
   event->accept();
   _timer->stop();
}

void
PluginProgressWidget::tick() {
   
   int val = _progress->value();
   val += 1;
   val %= 20;   
   if ( !val ) 
      _progress->setInvertedAppearance( ! _progress->invertedAppearance() );
   _progress->setValue( val + 1 );
}
