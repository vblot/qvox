/** -*- mode: c++ -*-
 * @file   DialogThresholds.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:21:02 2005
 *
 * @brief  2D volume view Widget class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogThresholds.h"

#include <QObject>
#include <QVariant>
#include <limits>
#include <vector>

#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QFrame>
#include <QTableView>
#include <QPushButton>
#include <QLayout>

#include "Triple.h"
#include "Settings.h"

#include "HistogramView.h"
#include "Surface.h"

using std::vector;
using std::numeric_limits;

DialogThresholds::DialogThresholds( QWidget* parent, Qt::WindowFlags wf )
  : QDialog( parent, wf )
{
  CONS( DialogThresholds );

  QVBoxLayout* dialogThresholdsLayout;
  QGridLayout* grid;
  QFrame* line;
  
  setWindowTitle( "Voxel values clamping" );
  setModal( false );
  
  setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Minimum );
  setSizeGripEnabled( true );
  
  delete layout();
  dialogThresholdsLayout = new QVBoxLayout;
  dialogThresholdsLayout->setMargin( 6 );
  setLayout( dialogThresholdsLayout );

  grid = new QGridLayout();

  // Line 0
  textLabel1_2 = new QLabel( this );
  textLabel1_2->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  grid->addWidget( textLabel1_2, 0, 1 );

  textLabel1_2_5 = new QLabel( this );
  textLabel1_2_5->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  grid->addWidget( textLabel1_2_5, 0, 2 );

  // Col 0

  textLabel1_2_4 = new QLabel( this );
  textLabel1_2_4->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  textLabel1_2_4->setAlignment( Qt::AlignVCenter | Qt::AlignRight );
  grid->addWidget( textLabel1_2_4, 1, 0 );

  textLabel1_2_2 = new QLabel( this );
  textLabel1_2_2->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  textLabel1_2_2->setAlignment( Qt::AlignVCenter | Qt::AlignRight );
  grid->addWidget( textLabel1_2_2, 2, 0 );

  textLabel1_2_3 = new QLabel( this );
  textLabel1_2_3->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  textLabel1_2_3->setAlignment( Qt::AlignVCenter | Qt::AlignRight );
  grid->addWidget( textLabel1_2_3, 3, 0 );

  //  QSpacerItem* spacer4 = new QSpacerItem( 61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
  //  grid->addItem( spacer4, 0, 0 );

  leMin0 = new QLineEdit( this );
  leMin0->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
  leMin0->setFocusPolicy( Qt::StrongFocus );
  grid->addWidget( leMin0, 1, 1 );

  leMax0 = new QLineEdit( this );
  leMax0->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
  grid->addWidget( leMax0, 1, 2 );

  leMin1 = new QLineEdit( this );
  leMin1->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addWidget( leMin1, 2, 1 );

  leMax1 = new QLineEdit( this );
  leMax1->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addWidget( leMax1, 2, 2 );

  leMin2 = new QLineEdit( this );
  leMin2->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addWidget( leMin2, 3, 1 );

  leMax2 = new QLineEdit( this );
  leMax2->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  grid->addWidget( leMax2, 3, 2 );


  dialogThresholdsLayout->addLayout( grid );
  
  {
    QHBoxLayout *hbox;
    dialogThresholdsLayout->addLayout( hbox = new QHBoxLayout );
    hbox->addWidget( cbShowMin = new QCheckBox(this) );
    hbox->addWidget( buttonRefresh = new QPushButton( this ) );
    buttonRefresh->setAutoDefault( true );
    buttonRefresh->setDefault( true );
  }
  
  QFrame *frame = new QFrame(this);
  new QVBoxLayout( frame );
  frame->setFrameShape( QFrame::Box );
  frame->layout()->setMargin( 1 );
  _histogramWidget[0] = new HistogramWidget( frame );
  frame->layout()->addWidget( _histogramWidget[0] );
  dialogThresholdsLayout->addWidget( frame );


  frame = new QFrame(this);
  new QVBoxLayout( frame );
  frame->setFrameShape( QFrame::Box );
  frame->layout()->setMargin( 1 );
  _histogramWidget[1] = new HistogramWidget( frame );
  frame->layout()->addWidget( _histogramWidget[1] );
  dialogThresholdsLayout->addWidget( frame );

  frame = new QFrame(this);
  new QVBoxLayout( frame );
  frame->setFrameShape( QFrame::Box );
  frame->layout()->setMargin( 1 );
  _histogramWidget[2] = new HistogramWidget( frame );
  frame->layout()->addWidget( _histogramWidget[2] );
  dialogThresholdsLayout->addWidget( frame );

  connect( cbShowMin, SIGNAL( toggled(bool) ),
           & (_histogramWidget[0]->histogramView()), SLOT( showMin(bool) ) );
  connect( cbShowMin, SIGNAL( toggled(bool) ),
           & (_histogramWidget[1]->histogramView()), SLOT( showMin(bool) ) );
  connect( cbShowMin, SIGNAL( toggled(bool) ),
           & (_histogramWidget[2]->histogramView()), SLOT( showMin(bool) ) );

  connect( _histogramWidget[0], SIGNAL( minChanged(const QString&) ),
      leMin0, SLOT( setText(const QString &) ) );
  connect( _histogramWidget[0], SIGNAL( maxChanged(const QString&) ),
      leMax0, SLOT( setText(const QString &) ) );
  connect( _histogramWidget[1], SIGNAL( minChanged(const QString&) ),
      leMin1, SLOT( setText(const QString &) ) );
  connect( _histogramWidget[1], SIGNAL( maxChanged(const QString&) ),
      leMax1, SLOT( setText(const QString &) ) );
  connect( _histogramWidget[2], SIGNAL( minChanged(const QString&) ),
      leMin2, SLOT( setText(const QString &) ) );
  connect( _histogramWidget[2], SIGNAL( maxChanged(const QString&) ),
      leMax2, SLOT( setText(const QString &) ) );


  connect( leMin0, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[0], SLOT( setMin(const QString&) ) );
  connect( leMax0, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[0], SLOT( setMax(const QString&) ) );

  connect( leMin1, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[1], SLOT( setMin(const QString&) ) );
  connect( leMax1, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[1], SLOT( setMax(const QString&) ) );

  connect( leMin2, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[2], SLOT( setMin(const QString&) ) );
  connect( leMax2, SIGNAL( textChanged(const QString&) ),
           _histogramWidget[2], SLOT( setMax(const QString&) ) );

  line = new QFrame( this );
  line->setFrameShape( QFrame::HLine );
  line->setFrameShadow( QFrame::Sunken );
  line->setFrameShape( QFrame::HLine );
  dialogThresholdsLayout->addWidget( line );

  //
  // Buttons
  //

  QHBoxLayout* hbox = new QHBoxLayout;


  buttonApplyVolume = new QPushButton( this );
  buttonApplyVolume->setAutoDefault( true );
  buttonApplyVolume->setDefault( true );
  hbox->addWidget( buttonApplyVolume );

  buttonApplySurface= new QPushButton( this );
  buttonApplySurface->setAutoDefault( true );
  buttonApplySurface->setDefault( true );
  hbox->addWidget( buttonApplySurface );

  QSpacerItem* spacer1 = new QSpacerItem( 180, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
  hbox->addItem( spacer1 );

  buttonClose = new QPushButton( this );
  buttonClose->setAutoDefault( true );
  buttonClose->setDefault( true );
  hbox->addWidget( buttonClose );

  dialogThresholdsLayout->addLayout( hbox );

  languageChange();
  resize( QSize(357, 546).expandedTo(minimumSizeHint()) );

  // signals and slots connections
  connect( buttonClose, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonRefresh, SIGNAL( clicked() ), _histogramWidget[0], SLOT( update() ) );
  connect( buttonRefresh, SIGNAL( clicked() ), _histogramWidget[1], SLOT( update() ) );
  connect( buttonRefresh, SIGNAL( clicked() ), _histogramWidget[2], SLOT( update() ) );
  connect( buttonApplyVolume, SIGNAL( clicked() ), this, SLOT( applyVolume() ) );

  // tab order
  setTabOrder( leMin0, leMax0 );
  setTabOrder( leMax0, leMin1 );
  setTabOrder( leMin1, leMax1 );
  setTabOrder( leMax1, leMin2 );
  setTabOrder( leMin2, leMax2 );
  setTabOrder( leMax2, buttonRefresh );
  setTabOrder( buttonRefresh, buttonApplyVolume );
  setTabOrder( buttonApplyVolume, buttonClose );

  _document = 0;
  _surface = 0;
  _changed = false;
  _intValidator = new QIntValidator( 0, 255, this);
  _doubleValidator = new QDoubleValidator( -numeric_limits<float>::max(),
                                           numeric_limits<float>::max() , 3, this );

  leMin0->setText( "1" );
  leMin1->setText( "1" );
  leMin2->setText( "1" );
  leMax0->setText( "1" );
  leMax1->setText( "1" );
  leMax2->setText( "1" );
}

DialogThresholds::~DialogThresholds()
{
  
}

void DialogThresholds::languageChange()
{
  setWindowTitle( tr( "Thresholds" ) );
  textLabel1_2->setText( QObject::tr( "Min (included)" ) );
  textLabel1_2_3->setText( QObject::tr( "Band 2" ) );
  textLabel1_2_4->setText( QObject::tr( "Band 0" ) );
  textLabel1_2_5->setText( QObject::tr( "Max (included)" ) );
  textLabel1_2_2->setText( QObject::tr( "Band 1" ) );

  buttonRefresh->setText( QObject::tr( "&Refresh" ) );
  buttonRefresh->setShortcut( QKeySequence( QObject::tr( "Alt+R" ) ) );
  buttonApplyVolume->setText( QObject::tr( "Apply to &volume" ) );
  buttonApplyVolume->setShortcut( QKeySequence( QObject::tr( "Alt+V" ) ) );
  buttonApplySurface->setText( QObject::tr( "Apply to &surface" ) );
  buttonApplySurface->setShortcut( QKeySequence( QObject::tr( "Alt+S" ) ) );
  buttonClose->setText( QObject::tr( "Close" ) );
  buttonClose->setShortcut( QKeySequence( QString::null ) );
  cbShowMin->setText( tr( "Show min values" ) ) ;
}

void DialogThresholds::destroy() {
  dispose( _intValidator );
  dispose( _doubleValidator );
}

void DialogThresholds::setDocument( Document * document )
{
  _changed = true;
  if ( _document ) disconnect( _document, 0, this, 0 );
  _document = document;
  _histogramWidget[0]->document( document, 0 );
  _histogramWidget[1]->document( document, 1 );
  _histogramWidget[2]->document( document, 2 );
  if ( _document )  {
    connect( _document, SIGNAL( volumeChanged() ),
             this, SLOT( volumeChanged() ) );
    leMin1->setEnabled(_document->bands() > 1);
    leMax1->setEnabled(_document->bands() > 1);
    leMin2->setEnabled(_document->bands() > 2);
    leMax2->setEnabled(_document->bands() > 2);
  }
}

void DialogThresholds::setSurface(Surface * surface)
{
  _surface = surface;
}

void DialogThresholds::volumeChanged()
{
  _changed = true;
  if ( !_document ) return;
  switch ( _document->valueType() ) {
  case Po_ValUC:
  case Po_ValSL:
  case Po_ValUL:
  case Po_ValSS:
  case Po_ValUS:
    leMin0->setValidator( _intValidator );
    leMin1->setValidator( _intValidator );
    leMin2->setValidator( _intValidator );
    leMax0->setValidator( _intValidator );
    leMax1->setValidator( _intValidator );
    leMax2->setValidator( _intValidator );
    break;
  case Po_ValSF:
    leMin0->setValidator( _doubleValidator );
    leMin1->setValidator( _doubleValidator );
    leMin2->setValidator( _doubleValidator );
    leMax0->setValidator( _doubleValidator );
    leMax1->setValidator( _doubleValidator );
    leMax2->setValidator( _doubleValidator );
    break;
  }

  switch ( _document->valueType() ) {
  case Po_ValUC:
  {
    _intValidator->setRange( 0, numeric_limits<unsigned char>::max() );
    QString strmax = QString("%1").arg( numeric_limits<unsigned char>::max());
    leMin0->setText( "1" );
    leMin1->setText( "1" );
    leMin2->setText( "1" );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  case Po_ValSL:
  {
    _intValidator->setRange( numeric_limits<Int32>::min(), numeric_limits<Int32>::max() );
    QString strmax = QString("%1").arg( numeric_limits<Int32>::max() );
    leMin0->setText( "1" );
    leMin1->setText( "1" );
    leMin2->setText( "1" );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  case Po_ValUL:
  {
    _intValidator->setRange( 0, numeric_limits<UInt32>::max() );
    QString strmax = QString("%1").arg( numeric_limits<UInt32>::max() );
    leMin0->setText( "1" );
    leMin1->setText( "1" );
    leMin2->setText( "1" );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  case Po_ValSS:
  {
    _intValidator->setRange( numeric_limits<Short>::min(), numeric_limits<Short>::max() );
    QString strmax = QString("%1").arg( numeric_limits<Short>::max() );
    leMin0->setText( "1" );
    leMin1->setText( "1" );
    leMin2->setText( "1" );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  case Po_ValUS:
  {
    _intValidator->setRange( 0, numeric_limits<UShort>::max() );
    QString strmax = QString("%1").arg( numeric_limits<UShort>::max() );
    leMin0->setText( "1" );
    leMin1->setText( "1" );
    leMin2->setText( "1" );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  case Po_ValSF:
  {
    QString strmin = QString("%1").arg( numeric_limits<Float>::epsilon() );
    QString strmax = QString("%1").arg( numeric_limits<Float>::max() );
    leMin0->setText( strmin );
    leMin1->setText( strmin );
    leMin2->setText( strmin );
    leMax0->setText( strmax );
    leMax1->setText( strmax );
    leMax2->setText( strmax );
  }
    break;
  }
  
  leMin1->setEnabled(_document->bands() > 1);
  leMax1->setEnabled(_document->bands() > 1);
  leMin2->setEnabled(_document->bands() > 2);
  leMax2->setEnabled(_document->bands() > 2);

}


void DialogThresholds::applyVolume()
{
  if (  !_document )
    return;
  
  Triple<Int32> slMin, slMax;
  Triple<UInt32> ulMin, ulMax;
  Triple<Float> sfMin, sfMax;
  slMin = std::numeric_limits<Int32>::min();
  slMax = std::numeric_limits<Int32>::max();
  ulMin = std::numeric_limits<UInt32>::min();
  ulMax = std::numeric_limits<UInt32>::max();
  sfMin = std::numeric_limits<Float>::min();
  sfMax = std::numeric_limits<Float>::max();

  QCursor currentCursor = cursor();
  setCursor( QCursor( Qt::WaitCursor ) );

  switch ( _document->valueType() ) {
  case Po_ValSS:
  case Po_ValSL:
  {
    slMin.first = leMin0->text().toLong( 0 );
    slMin.second = leMin1->text().toLong( 0 );
    slMin.third = leMin2->text().toLong( 0 );
    slMax.first = leMax0->text().toLong( 0 );
    slMax.second = leMax1->text().toLong( 0 );
    slMax.third = leMax2->text().toLong( 0 );
    _document->clamp( slMin, slMax );
  }
    break;
  case Po_ValUC:
  case Po_ValUS:
  case Po_ValUL:
  {
    ulMin.first = leMin0->text().toULong( 0 );
    ulMin.second = leMin1->text().toULong( 0 );
    ulMin.third = leMin2->text().toULong( 0 );
    ulMax.first = leMax0->text().toULong( 0 );
    ulMax.second = leMax1->text().toULong( 0 );
    ulMax.third = leMax2->text().toULong( 0 );
    _document->clamp( ulMin, ulMax );
  }
    break;
  case Po_ValSF: {
    sfMin.first = leMin0->text().toFloat();
    sfMin.second = leMin1->text().toFloat();
    sfMin.third = leMin2->text().toFloat();
    sfMax.first = leMax0->text().toFloat();
    sfMax.second = leMax1->text().toFloat();
    sfMax.third = leMax2->text().toFloat();
    _document->clamp( sfMin, sfMax );
  }
    break;
  }
  
  setCursor( currentCursor );
  _changed = false;
}

void DialogThresholds::applySurface()
{

}

