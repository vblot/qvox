/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   Dialog2DView.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class Dialog2DView
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Dialog2DView.h"

#include <QButtonGroup>
#include <QScrollBar>
#include <QHBoxLayout>
#include "MainWindow.h"

/**
 * Constructor
 */
Dialog2DView::Dialog2DView(QWidget *parent)
  : QDialog(parent) {
  CONS( Dialog2DView );
  setupUi( this );

  _colorMap = new ColorMap;  

  _view2DWidget = new View2DWidget;
  _view2DWidget->colorMap( _colorMap );
  _view2DWidget->scrollArea( scrollArea );

  //scrollArea->setLayout( new QHBoxLayout );
  //scrollArea->layout()->addWidget( _view2DWidget );

  scrollArea->setWidget( _view2DWidget );
    
  connect( _colorMap,  SIGNAL( changed(int ) ),
	   this, SLOT( colorMapChanged() ) );    
  connect( _colorMap,  SIGNAL( colorSelected(int) ),
	   this, SLOT( selectedColorChanged(int) ) );
  connect( sbBrush, SIGNAL( valueChanged(int) ),
	   _colorMap, SLOT( selectColor(int) ) );
  cbColorMap->insertItem( 0, "Grey" );
  cbColorMap->insertItem( 1, "Red" );
  cbColorMap->insertItem( 2, "Green");
  cbColorMap->insertItem( 3, "Blue" );
  cbColorMap->insertItem( 4, "Random" );
  cbColorMap->insertItem( 5, "Contrast" );
  cbColorMap->insertItem( 6, "Colors" );
  cbColorMap->insertItem( 7, "Hue shade" );
  cbColorMap->insertItem( 8, "Curvature" );
  cbColorMap->insertItem( 9, "Light" );
  cbColorMap->insertItem( 10, "Toon" );
  cbColorMap->setCurrentIndex( _colorMap->type() );

  connect( cbColorMap, SIGNAL( activated( int ) ), 
	   _colorMap, SLOT( type( int ) ) );

  selectedColorChanged( _colorMap->selectedColor() );

  actionGroup = new QButtonGroup( this );
  actionGroup->addButton( pbDraw, View2DWidget::Draw );
  actionGroup->addButton( pbFill, View2DWidget::Fill );
  actionGroup->addButton( pbMove, View2DWidget::Move );
  actionGroup->addButton( pbBrush, View2DWidget::Brush );
  pbMove->setChecked( true );

  connect( actionGroup, SIGNAL( buttonClicked(int) ),
	   this, SLOT( changeAction(int) ) );
  connect( pbCopy, SIGNAL( clicked() ),
	   _view2DWidget, SLOT(  copyPlane() ) );    
  connect( pbCut, SIGNAL( clicked() ),
	    _view2DWidget, SLOT(  cutPlane() ) );    
  connect( pbPaste, SIGNAL( clicked() ),
	    _view2DWidget, SLOT(  pastePlane() ) );
  connect( pbZoomIn, SIGNAL( clicked() ),
	   this, SLOT(  zoomIn() ) );
  connect( pbZoomOut, SIGNAL( clicked() ),
	   this, SLOT(  zoomOut() ) );
  connect( pbZoomFit, SIGNAL( clicked() ),
	   this, SLOT(  zoomFit() ) );
  connect( pbZoomOne, SIGNAL( clicked() ),
	   this, SLOT(  zoomOne() ) );
  connect( pbRefresh, SIGNAL(clicked()),
	   this, SLOT( refresh3D()) );
  connect( pbClose, SIGNAL( clicked() ),
	   this, SLOT( close() ) );  
  connect( pbTrim, SIGNAL( clicked() ),
	   this, SLOT( trimPushed() ) );
    
  connect( _view2DWidget, SIGNAL( zoomChanged() ),
	   this, SLOT( changeZoom() ));

  connect( _view2DWidget, SIGNAL( maxDepthChanged(int) ),
	   this, SLOT( changeMaxDepth(int) ) );
  connect( _view2DWidget, SIGNAL( depthChanged(int,int) ),
	   this, SLOT( changeDepth(int,int) ) );
  connect( _view2DWidget, SIGNAL( planeChanged(int) ),
	   this, SLOT( changePlane(int) ) );
  connect( _view2DWidget, SIGNAL( infoChanged(QString) ),
	   this, SLOT( changeInfo(QString) ) );

  connect( sliderPlane, SIGNAL( valueChanged(int) ),
	   _view2DWidget, SLOT( setDepth(int) ) );

  connect( sliderPlane, SIGNAL( valueChanged(int) ),
	   _view2DWidget, SLOT( setDepth(int) ) );
  connect( sbPlane, SIGNAL( valueChanged(int) ),
	   _view2DWidget, SLOT( setDepth(int) ) );
  
  sliderPlane->setTickPosition( QSlider::TicksAbove );
  sliderPlane->setTickInterval( 1 );

  planeButtonGroup = new QButtonGroup(this);
  planeButtonGroup->addButton( pbPlaneOxy, View2DWidget::PlaneOxy ); 
  planeButtonGroup->addButton( pbPlaneOxz, View2DWidget::PlaneOxz ); 
  planeButtonGroup->addButton( pbPlaneOyz, View2DWidget::PlaneOyz ); 
  connect( planeButtonGroup, SIGNAL( buttonClicked(int) ),
	   this, SLOT( callBackButtonsPlane(int) ) );
  connect( cbDisplayIn3DView, SIGNAL(toggled(bool)),
	   SIGNAL(askForDisplayIn3D(bool)) );

  if (  _view2DWidget->zoom() ==  _view2DWidget->maxZoom() ) 
    pbZoomIn->setEnabled( false );
  if (  _view2DWidget->zoom() ==  1 ) 
    pbZoomOut->setEnabled( false );

  scrollArea->setWidgetResizable(false);
  scrollArea->setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );
  //scrollArea->setAutoFillBackground(false);
  colorFrame->setAutoFillBackground(true);
}

Dialog2DView::~Dialog2DView()
{
  delete _colorMap;
}

void
Dialog2DView::changeMaxDepth(int maxDepth)
{
  maxDepth--;
  labelMaxPlane->setText( QString("%1").arg( static_cast<long>( maxDepth ) ));
  sliderPlane->setMaximum( maxDepth );
  sbPlane->setMaximum( maxDepth );
  sliderPlane->setTickInterval( 1 + maxDepth/100 ); 
}

void
Dialog2DView::changeDepth( int depth, int /* plane */ )
{
  if ( depth !=  sbPlane->value() ) {
    sbPlane->setValue( depth );
  }
  if ( depth !=  sliderPlane->value() ) {
    sliderPlane->setValue( depth );
  }	
  _view2DWidget->setDepth( depth );
}

void
Dialog2DView::changePlane( int plane )
{
  switch ( plane ) {
  case 0: labelAxis->setText("X:"); break;
  case 1: labelAxis->setText("Y:"); break;
  case 2: labelAxis->setText("Z:"); break;
  }
}

void
Dialog2DView::changeAction(int id)
{
  _view2DWidget->mode( static_cast<View2DWidget::Mode>( id ) );  
}

void
Dialog2DView::callBackButtonsPlane( int id )
{	
  _view2DWidget->setPlane( static_cast<View2DWidget::Plane>( id  ), true );
}

void
Dialog2DView::zoomIn()
{
  _view2DWidget->zoomIn();
  labelZoom -> setText( QString("1:%1").arg( _view2DWidget->zoom() ) );
  if (  _view2DWidget->zoom() ==  _view2DWidget->maxZoom() ) 
    pbZoomIn->setEnabled( false );
  if (  _view2DWidget->zoom() > 1 
	&& ! pbZoomOut->isEnabled() ) 
    pbZoomOut->setEnabled( true );
}

void
Dialog2DView::zoomOut()
{
  _view2DWidget->zoomOut();
  labelZoom -> setText( QString("1:%1").arg( _view2DWidget->zoom() ) );
  if (  _view2DWidget->zoom() <  _view2DWidget->maxZoom()
	&& ! pbZoomIn->isEnabled() ) 
    pbZoomIn->setEnabled( true );
  if (  _view2DWidget->zoom() ==  1 ) 
    pbZoomOut->setEnabled( false );
}

void
Dialog2DView::zoomFit()
{
  _view2DWidget->zoomFit();
  labelZoom->setText( QString("1:%1").arg( _view2DWidget->zoom() ) );
}

void
Dialog2DView::zoomOne()
{
  _view2DWidget->zoomOne();
  labelZoom->setText( QString("1:%1").arg( _view2DWidget->zoom() ) );
}

void
Dialog2DView::selectedColorChanged( int value )
{
  sbBrush->setValue( value );
  const QColor & color = _colorMap->selectedQColor();
  QPalette p = colorFrame->palette();
  p.setBrush( QPalette::Base, color );
  p.setBrush( QPalette::Window, color );
  p.setBrush( QPalette::Text, QColor( ~(color .rgb()) ));
  colorFrame->setPalette( p );
  colorFrame->setToolTip( QString("rgb(%1,%2,%3)")
			    .arg( color.red() )
			    .arg( color.green())
			    .arg( color.blue() ) );  
}

void
Dialog2DView::colorMapChanged()
{
  const QColor & color = _colorMap->selectedQColor();
  QPalette p = colorFrame->palette();
  p.setColor( QPalette::Window, color );
  p.setColor( QPalette::Text, QColor( ~(color .rgb()) ));
  colorFrame->setPalette( p );
  colorFrame->setToolTip( QString("rgb(%1,%2,%3)")
			    .arg( color.red() )
			    .arg( color.green())
			    .arg( color.blue() ) );  
}

void
Dialog2DView::changeInfo( QString text)
{
  labelInfo->setText( text );
}

void
Dialog2DView::changeZoom()
{
  if (  _view2DWidget->zoom() <  _view2DWidget->maxZoom()
	&& ! pbZoomIn->isEnabled() ) 
    pbZoomIn->setEnabled( true );
  if (  _view2DWidget->zoom() >  1 && ! ( pbZoomOut->isEnabled() ) ) 
    pbZoomOut->setEnabled( true );
  if (  _view2DWidget->zoom() ==  1 )
    pbZoomOut->setEnabled( false );
  if (  _view2DWidget->zoom() == _view2DWidget->maxZoom() ) 
    pbZoomIn->setEnabled( false );
  
}

void
Dialog2DView::closeEvent( QCloseEvent *)
{
  emit closing();
  refresh3D();
}

void
Dialog2DView::document( Document * document )
{
  _view2DWidget->document( document );
}

void
Dialog2DView::done( int r )
{
  emit closing();
  refresh3D();
  QDialog::done(r);
}	

void
Dialog2DView::refresh3D()
{
  Document * doc = _view2DWidget->document();
  if ( _view2DWidget->volumeModified() && doc )
    doc->notify();
}

void
Dialog2DView::trimPushed()
{
  emit trim( _view2DWidget->plane(), _view2DWidget->depth() );
}

View2DWidget *
Dialog2DView::viewWidget()
{
  return _view2DWidget;
}

bool
Dialog2DView::showSlice()
{
  return cbDisplayIn3DView->isChecked();
}

