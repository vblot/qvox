/**
 * @file   View3DWidget.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:29 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <iostream>

#include <QKeyEvent>
#include <QWhatsThis>
#include <QPainter>
#include <QApplication>
#include <QLabel>
#include <QColorDialog>
#include <QTime>
#include <QProcess>

#include "Array.h"
#include "View3DWidget.h"
#include "AnimationToolBar.h"
#include "ColorMapView.h"
#include "Settings.h"
#include "Progress.h"
#include "Dialogs.h"

#ifdef HAVE_EXPERIMENTAL
#include "DialogExperiments.h"
#endif // HAVE_EXPERIMENTAL

#include "EdgelList.h"
#include "Convolution.h"

using std::cout;
using std::endl;
using std::numeric_limits;

namespace {
/*
   * Part of this code, dedicated to the trackball implementation,
   * is inspired from:
   *
   * Implementation of a virtual trackball.
   * Implemented by Gavin Bell, lots of ideas from Thant Tessman and
   *   the August '88 issue of Siggraph's "Computer Graphics," pp. 121-129.
   *
   * This code has been adapted by Edouard Thiel (University of Marseille).
   */
const double TrackBallSize = 0.8;

double zOnSphere( double r, double x, double y )
{
  double d, t, z;
  d = sqrt( x*x + y*y );
  if ( d < r * 0.70710678118654752440 ) {
    z = sqrt( r*r - d*d );              /* Inside the sphere */
  } else {
    t = r / 1.41421356237309504880;     /* On hyperbola */
    z = t * t / d;
  }
  return -z;
}

/**
   * Given two points on a plane, returns a quaternion
   * that corresponds to a 3D rotation.
   */
Quaternion trackballQ( double x1, double y1,
                       double x2, double y2 )
{
  if ( x1 == x2 && y1 == y2)
    return Quaternion( 0, 0, 0, 1.0 );;

  Triple<double> p1( x1, y1, zOnSphere( TrackBallSize, x1, y1 ) );
  Triple<double> p2( x2, y2, zOnSphere( TrackBallSize, x2, y2 ) );
  Triple<double> axis = wedgeProduct( p1, p2 ); // Axis of rotation

  double t = norm( p1 - p2 ) / ( 2.0 * TrackBallSize );

  if ( t > 1.0 ) t = 1.0;
  if ( t < -1.0 ) t = -1.0;
  double phi = 2.0 * asin( t );
  phi = asin( norm( wedgeProduct( normalized(p1), normalized(p2) ) ) );
  return Quaternion::rotation( axis, phi );
}
}

View3DWidget::View3DWidget( QWidget * parent )
  : QWidget( parent ),
    _currentRot( 0, 0, 0, 1 ),
    _image(1,1,QImage::Format_ARGB32_Premultiplied),
    _axisImage(100,100,QImage::Format_ARGB32_Premultiplied),
    _rotationBox( false )
{
  CONS( View3DWidget );
  setAutoFillBackground(false);
  setAttribute( Qt::WA_OpaquePaintEvent );
  setFocusPolicy(Qt::StrongFocus);

  _shown = false;
  _logo = false;
  _framesPerSec = 0.0;
  _mouseHasMoved = false;
  _needToRedraw = true;
  _view2DImage = 0;

  _backgroundColor = globalSettings->bgColor();

  _animRho = _animTheta = _animPhi;
  _animate = false;
  _animateAspect = false;
  _document = 0;
  _writeAnimation = false;
  _action = ActionMove;
  _leftButton = _middleButton = _rightButton = false;
  _highlightSlice = false;
  _highlightedSlice = 0;
  _highlightedSliceAxis = 0;
  _capture = false;

  _surface.settings( globalSettings );
  _surface.setShading( globalSettings->shading() );
  _colorMap.type( globalSettings->colormap() );
  _needToRebuild = true;
  _virtualPointer = 0;

  connect( & _colorMap, SIGNAL( changed( int ) ) ,
           globalSettings , SLOT( colormap(int ) ) );

  connect( &_colorMap, SIGNAL( changed( int ) ),
           this, SLOT( forceRedraw() ) );

  connect( globalSettings, SIGNAL( shadingChanged(int ) ),
           this, SLOT( setShading(int ) ) );

  connect( &_surface, SIGNAL(info(QString)),
           this, SIGNAL(info(QString)));
}

View3DWidget::~View3DWidget()
{
}

void
View3DWidget::logo() {
  _logo = true;
  if ( _shown ) {
    _surface.rotate(  -0.4, .2, -0.3 );
    _colorMap.type( ColorMap::HueScale );
    zoomFit();
    int steps = 35;
    float angle = 2*3.1415 / steps;
    for ( int i = 0; i < steps; i++ ) {
      _surface.rotate(  0.0, angle, 0.0 );
      forceRedraw();
    }
    _logo = false;
  }
}

void
View3DWidget::showColorMap(bool on)
{
  globalSettings->drawColorMap(on);
  forceRedraw( true );
}

void
View3DWidget::showEvent( QShowEvent * )
{
  if ( _document && !_shown ) {
    _shown = true;
    if ( _logo ) logo();
  }
}

void
View3DWidget::hideEvent( QHideEvent * ) {
  //   disconnect( _document, SIGNAL( volumeChanged() ),
  // 	      this, SLOT( volumeChanged() ) );
  _shown = false;
}

void
View3DWidget::aspect( float x, float y, float z)
{
  _surface.aspect( x, y, z );
  _surface.rotate( Quaternion( 0.0, 0.0, 0.0, 1.0 ) );
  forceRedraw();
}

void
View3DWidget::action( int a )
{
  _action = static_cast<Action>( a );
}

void
View3DWidget::recordRotation()
{
  _recordedRotation = _surface.rotation();
}

void
View3DWidget::setApplication( QApplication * application )
{
  _application = application;
}

void
View3DWidget::document( Document * document )
{
  if ( _document ) {
    disconnect( _document, 0, this, 0 );
    disconnect( _document, 0, &_surface, 0 );
  }
  _document = document;
  if ( _document ) {
    connect( _document, SIGNAL( volumeChanged() ),
             this, SLOT( volumeChanged() ) );
    connect( _document, SIGNAL( invalidated() ),
             &_surface, SLOT( clear() ) );
  }
}

void
View3DWidget::fullVolume( bool on )
{
  globalProgressReceiver.showMessage("Building surface...");
  int volumeAdjacency = ADJ18;
  switch ( globalSettings->adjacencyPair() ) {
  case ADJ6:
  case ADJ6P:
    volumeAdjacency = ADJ6;
    break;
  case ADJ18:
  case ADJ26:
    volumeAdjacency = ADJ18;
    break;
  }

  if ( _document ) {
    _document->valid( false );
    _surface.setImage( _document->image(),
                       on,
                       Surface::KeepViewParams,
                       volumeAdjacency );
    _document->valid( true );
  } else {
    _surface.setImage( 0,
                       true,
                       Surface::KeepViewParams,
                       volumeAdjacency);
  }
  setShading( globalSettings->shading() );
  forceRedraw();
}

void
View3DWidget::volumeChanged()
{
  globalProgressReceiver.showMessage("Building surface...");
  int volumeAdjacency = ADJ18;
  switch ( globalSettings->adjacencyPair() ) {
  case ADJ6:
  case ADJ6P:
    volumeAdjacency = ADJ6;
    break;
  case ADJ18:
  case ADJ26:
    volumeAdjacency = ADJ18;
    break;
  }
  if ( _document ) {
    Quaternion q = _surface.rotationQuaternion();
    _surface.setImage( _document->image(),
                       globalSettings->fullVolumeBoxAction()->isChecked(),
                       Surface::KeepViewParams,
                       volumeAdjacency );
    setShading( globalSettings->shading() );

    // Determine if rotation mode should use a box (if volume
    // has too many surfels).
    _time.restart();
    _surface.setRotation( q );
    _surface.drawSurface( _image, _colorMap );
    _rotationBox = ( ( 1000.0 / _time.elapsed() ) < globalSettings->rotationBoxMaxFrames() );
  }  else  {
    _surface.setImage( 0,
                       true,
                       Surface::KeepViewParams,
                       volumeAdjacency );
    setShading( globalSettings->shading() );
    _rotationBox = false;
  }
}

Surfel*
View3DWidget::getCloserSurfel( int x, int y )
{
  float d;
  float d_min = 2;
  pSurfel *pps, *end;;
  Surfel *ps_min = 0;

  Triple<float> point, projected;
  point.set(  ( x - ( width() >> 1 ) ) / _surface.zoom(),
              ( y - ( height() >> 1 )  ) / _surface.zoom(),
              0 );

  pps = _surface.sortedSurfelsArray();
  end = _surface.sortedSurfelsEnd();

  while ( pps < end ) {
    projected = (*pps)->rotatedCenter;
    projected.third = 0;
    d = norm( projected - point );
    if ( d <= d_min ) {
      ps_min = *pps;
      d_min = d;
    }
    ++pps;
  }
  return ps_min;
}

void
View3DWidget::paintEvent(QPaintEvent *)
{
  // refreshFine(false);;
  if ( _fineRefreshAsked )
    refreshFine( _needToRebuild );
  else
    refresh( _needToRebuild );
  _needToRebuild = false;
  _fineRefreshAsked = false;
}

const QImage &
View3DWidget::axisImage() {
  int width = _axisImage.width();
  int height = _axisImage.height();
  int center = std::min( width/2, height/2);
  _axisImage = QImage( width, height, QImage::Format_ARGB32_Premultiplied );
  _axisImage.fill( _backgroundColor.rgb() );
  QPainter painter( &_axisImage );
  int zoom = 30;
  int fontSize;
  QColor lineColor;

  _backgroundColor = _colorMap.backgroundColor();

  lineColor.setRgb( ~ ( _backgroundColor.rgb() ) );
  painter.setPen( lineColor );

  Triple<float> normal = _surface.rotatedNormal( 0 );

  fontSize = static_cast<int>( 11 - 6 * normal.third );
  painter.setFont( QFont( "Times New Roman", fontSize, QFont::Normal, true  ) );
  painter.drawLine( center, center,
                    static_cast<int>( center + ( zoom * normal.first ) ),
                    static_cast<int>( center + ( zoom * normal.second ) ) );
  painter.drawText( static_cast<int>( center + ( (zoom+10) * normal.first ) ),
                    static_cast<int>( center + ( (zoom+10) * normal.second ) ),
                    "x" );

  normal = _surface.rotatedNormal( 2 );
  fontSize = static_cast<int>( 11 - 6 * normal.third );
  painter.setFont( QFont( "Times New Roman", fontSize, QFont::Normal, true  ) );
  painter.drawLine( center, center,
                    static_cast<int>( center + ( zoom * normal.first ) ),
                    static_cast<int>( center + ( zoom * normal.second ) ) );
  painter.drawText( static_cast<int>( center + ( (zoom+10) * normal.first ) ),
                    static_cast<int>( center + ( (zoom+10) * normal.second ) ),
                    "y");

  normal = _surface.rotatedNormal( 4 );
  fontSize = static_cast<int>( 11 - 6 * normal.third );
  painter.setFont( QFont( "Times New Roman", fontSize, QFont::Normal, true  ) );
  painter.drawLine( center, center,
                    static_cast<int>( center + ( zoom * normal.first ) ),
                    static_cast<int>( center + ( zoom * normal.second ) ) );
  painter.drawText( static_cast<int>( center + ( (zoom+10) * normal.first ) ),
                    static_cast<int>( center + ( (zoom+10) * normal.second ) ),
                    "z");
  return _axisImage;
}

void
View3DWidget::clear()
{
  _image.fill( _backgroundColor.rgb() ) ;
  bool v = _document->valid();
  _document->valid( false );
  forceRedraw();
  if ( v ) _document->valid( true );
}

void
View3DWidget::drawAscii()
{
#ifdef _IS_UNIX_
  char *var = getenv("COLUMNS");
  int w = atoi( var ? var : "80"  );
  var = getenv( "LINES" );
  int h = atoi( var ? var : "24" );
#else
  int w = 80;
  int h = 24;
#endif
  char letters[] = " @OPLl05.<>�&/\\|+][#~_'`Wop";

  QImage img = _image.scaled( w * 14, h * 17 );
  QImage lettersImage( ":/Pixmaps/letters.png", "PNG" );

  double diff, diffMin;
  int minLetter;
  int row, col, x, y, dx, dy, dx_letter, dy_letter;
  int letter;
  Array<char> text( w + 2, h );

  for ( row = 0; row < h; ++row ) {
    for ( col = 0; col < w; ++col ) {
      diffMin = numeric_limits<float>::max();
      minLetter = '@';
      dx = col * 14;
      dy = row * 17;
      for ( letter = 0 /* 32 */; letter < 27 /* 127 */ ; ++letter ) {
        dx_letter = letter * 15;
        dy_letter = 2;
        diff = 0.0f;
        for ( x = 2; x < 12; ++x )
          for ( y = 2; y < 15; ++y ) {
            diff += fabs( static_cast<float>(
                            qRed( img.pixel( x + dx, y + dy ) ) -
                            qRed( lettersImage.pixel( x + dx_letter, y + dy_letter ) ) ) );
          }
        if ( diff < diffMin ) { diffMin = diff; minLetter = letter; }
      }
      text( col, row ) = static_cast<char>( letters[ minLetter ] );
    }
    text( col, row ) = 0;
  }

  cout << endl;
  for ( row = 0; row < h; ++row )
    cout << endl << text[row];
  cout << std::flush;
}

void
View3DWidget::highlightSlice( bool onOff )
{
  _highlightSlice = onOff;
  _surface.activateSliceMark( onOff );
}

void
View3DWidget::highlightSlice( int slice, int axis )
{
  _highlightedSliceAxis = axis;
  _highlightedSlice = slice;
  if ( _highlightSlice )
    _surface.markSlice( axis, slice );
  else
    _surface.markSlice( -1, 0 );
  if ( !_shown ) return;
  forceRedraw();
}

void
View3DWidget::forceRedraw( bool fine )
{
  _needToRebuild = true;
  _fineRefreshAsked = fine;
  repaint();
}

void
View3DWidget::refresh(bool rebuildImage)
{
  if ( !_shown ) return;
  if ( rebuildImage && _document && _document->valid() ) {
    if ( _rotationBox && _leftButton )
      _surface.drawBoundingBox( _image, _colorMap );
    else
      _surface.drawSurface( _image, _colorMap );
    if ( globalSettings->drawAxis() ) { drawAxis( _image ); }
    if ( globalSettings->drawColorMap() ) {
      QPainter p(&_image);
      _colorMap.draw(p,
                     width() - (5 + 256),
                     height() - ( 5 + 25 ) );
    }
    if ( _view2DImage ) {
      QPainter p(&_image);
      p.drawImage( 10, 10,
                   *_view2DImage,
                   10, 10,
                   _view2DImage->width(), _view2DImage->height(),
                   Qt::AutoColor );
    }
  }
  QPainter painter( this );
  painter.setClipping( false );
  painter.drawImage( 0, 0, _image );
  if ( globalSettings->showFPS() ) { }

  if ( _capture ) {
    _captureFile.write( reinterpret_cast<char*>( _image.scanLine(0) ),
                        _captureSize );
    _captureFile.flush();
    emit info( QString("Capture (%1 MB)" )
               .arg( static_cast<Size>( _captureFile.tellp() ) >> 20 ) );
  }
}

void
View3DWidget::refreshFine(bool rebuildImage)
{
  QPainter painter(this);

  if ( !_document || ! _document->valid() || !_shown ) return;
  if ( rebuildImage )  {
    if ( _rotationBox || !globalSettings->fineDrawingMode() ) {
      _surface.drawSurface( _image, _colorMap );
    } else {
      _surface.drawFineSurface( _image, _colorMap );
    }

    if ( globalSettings->drawAxis() )
      drawAxis( _image );
    if ( globalSettings->drawColorMap() ) {
      QPainter p(&_image);
      _colorMap.draw(p,
                     width() - (5 + 256),
                     height() - ( 5 + 25 ) );
    }
    if ( _view2DImage ) {
      QPainter p( &_image );
      p.drawImage( 10, 10,
                   *_view2DImage,
                   10, 10,
                   _view2DImage->width(), _view2DImage->height(),
                   Qt::AutoColor );
    }
  }
  painter.setClipping( false );
  painter.drawImage( 0, 0, _image );
}

void
View3DWidget::keyPressEvent( QKeyEvent * e )
{
  if ( ! _document->valid() ) { e->ignore(); return; }

  float angle = M_PI / 16;
  if ( e->modifiers() & Qt::ShiftModifier )
    angle = M_PI / 2;
  if ( e->modifiers() & Qt::ControlModifier )
    angle = M_PI / 128;


  switch ( e->key() ) {
  case Qt::Key_A:
    animateAspect( ! _animateAspect );
    break;
  case  Qt::Key_C:
    contour();
    break;
  case Qt::Key_7:
    setShading( Surface::ColorFixed );
    break;
  case Qt::Key_Plus:
    e->accept();
    zoomIn();
    break;
  case Qt::Key_Minus:
    e->accept();
    zoomOut();
    break;
  case Qt::Key_Equal:
    e->accept();
    zoomFit();
    break;
  case Qt::Key_Space:
    if ( _action == ActionMoveLight )
      _surface.resetLight();
    else {
      _surface.resetPosition();
      _surface.viewCenterShift( 0, 0 );
    }
    forceRedraw(true);
    break;
  case Qt::Key_Bar:
    forceRedraw();
    break;
  case Qt::Key_Up :
    _surface.rotate( Quaternion::rotation( Quaternion::AxisX, -angle ) );
    forceRedraw();
    break;
  case Qt::Key_Down :
    _surface.rotate( Quaternion::rotation( Quaternion::AxisX, angle ) );
    forceRedraw();
    break;
  case Qt::Key_Right :
    _surface.rotate( Quaternion::rotation( Quaternion::AxisY, -angle ) );
    forceRedraw();
    break;
  case Qt::Key_Left :
    _surface.rotate( Quaternion::rotation( Quaternion::AxisY, angle ) );
    forceRedraw();
    break;
  case Qt::Key_PageDown:
    _surface.rotate( Quaternion::rotation( Quaternion::AxisZ, angle ) );
    forceRedraw();
    break;
  case Qt::Key_PageUp:
    _surface.rotate( Quaternion::rotation( Quaternion::AxisZ, -angle ) );
    forceRedraw();
    break;
  default:
    TRACE << "KEYP:" << e->key() << std::endl;
    break;
  }
}

void View3DWidget::wheelEvent( QWheelEvent * e )
{
  if ( ! _document->valid() ) { e->ignore(); return; }
  if ( _action == ActionMoveLight ) {
    if ( e->delta() > 0 )  _surface.pushLight();
    else _surface.pullLight();
    forceRedraw();
    e->accept();
    return;
  }
  if ( e->buttons() & Qt::RightButton ) {
    if ( e->delta() > 0 )
      _surface.moveCutPlaneForward();
    else
      _surface.moveCutPlaneBackward();
    forceRedraw( true );
  } else {
    if ( e->delta() > 0 )
      zoomIn();
    else
      zoomOut();
  }
  e->accept();
}

void
View3DWidget::mousePressEvent( QMouseEvent * e )
{
  if ( ! _document->valid() ) { e->ignore(); return; }
  _animate = false;
  Surfel * ps;
  switch ( e->button() ) {
  case Qt::LeftButton :
    _leftButton = true;
    _mouseHasMoved = false;
    _oldX = e->pos().x();
    _oldY = e->pos().y();
    switch ( _action ) {
    case ActionMove:
      if ( _rotationBox )
        forceRedraw( false );
      if ( e->modifiers() & Qt::ShiftModifier ) {
        _leftButton = false;
        int vertex = 0;
        int edge = 0;
        ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height(),
                                       &vertex, &edge );
        if ( ps ) {
          _surface.drawSurfel( _image, _colorMap, ps, vertex, edge );
          _needToRebuild = false;
          repaint();
          std::cout << " Surfel: " << ( ps - _surface.surfelList().begin() )
                    << " Vertex: " << vertex;
          if ( _surface.surfelVertexIndicesArray().size() ) {
            cout << " IndexV: " << _surface.surfelVertexIndicesArray()[ ps - _surface.surfelList().begin() ].indices[ vertex ];
          }
          cout << " Edge: " << edge;
          if ( _surface.surfelEdgeIndicesArray().size() ) {
            cout << " IndexE: " << _surface.surfelEdgeIndicesArray()[ ps - _surface.surfelList().begin() ].indices[ edge ];
          }
          cout << std::endl;
        }
      }
      break;
    case ActionInfo:
    {
      int vertex = 0;
      int edge = 0;
      ps = _surface.getCloserSurfel( _oldX, _oldY,
                                     width(), height(),
                                     &vertex, &edge );
      if ( ps ) {
        _surface.drawSurface( _image, _colorMap );
        _surface.highlightSurfel( _image,
                                  _colorMap,
                                  ps );
        forceRedraw( true );
        QString format( "Voxel: %1,%2,%3\n"
                        "Value: [%4]\n"
                        "Surfel: (%5) %6,%7,%8\n"
                        "Surfel color: %9\n"
                        "Orientation: %10\n" );
        QString info =
            format.arg( ps->voxel.first )
            .arg( ps->voxel.second )
            .arg( ps->voxel.third )
            .arg( _document->image()->valueString( ps->voxel ) )
            .arg( ps - _surface.surfelList().begin() )
            .arg( ps->rotatedCenter.first, 0, 'f', 3 )
            .arg( ps->rotatedCenter.second, 0, 'f', 3 )
            .arg( ps->rotatedCenter.third, 0, 'f', 3 )
            .arg( ps->color )
            .arg( ps->direction );
        info += _surface.surfelInfo( ps );
        info += QString( "\nVertex: %1 (Index %2)\nEdge: %3 (Index %4)\n" )
            .arg( vertex )
            .arg( (_surface.surfelVertexIndicesArray().size()) ? _surface.surfelVertexIndicesArray()[ ps - _surface.surfelList().begin() ].indices[ vertex ] : 0)
            .arg( edge )
            .arg( (_surface.surfelEdgeIndicesArray().size()) ? _surface.surfelEdgeIndicesArray()[ ps - _surface.surfelList().begin() ].indices[ edge ] : 0);
        QWhatsThis::showText( mapToGlobal( QPoint( _oldX + 20, _oldY + 20 ) ),
                              info,
                              this );
      }
    }
      break;
    case ActionRemoveVoxel:
      ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height() );
      if ( ps )
        _document->setVoxelValue( ps->voxel,
                                  0,
                                  0,0,0 );

      break;
    case ActionRemoveSimpleVoxel:
      ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height() );
      if ( ps ) {
        SimplePointTest simpleCheck = SkelSimpleTests[  globalSettings->adjacencyPair() ];
        if ( simpleCheck( _document->image()->config( ps->voxel ) ) )
          _document->setVoxelValue( ps->voxel,
                                    0,
                                    0,0,0 );
      }
      break;
    case ActionDrawVoxel:
      ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height() );
      if ( ps )
        _document->setVoxelValue( ps->voxel,
                                  _colorMap.selectedColor(),
                                  _colorMap.selectedQColor().red(),
                                  _colorMap.selectedQColor().green(),
                                  _colorMap.selectedQColor().blue() );
      break;
    case ActionDrawSimpleVoxel:
      ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height() );
      if ( ps ) {
        SimplePointTest simpleCheck = SkelSimpleTests[  globalSettings->adjacencyPair() ];
        if ( simpleCheck( _document->image()->config( ps->voxel ) ) )
          _document->setVoxelValue( ps->voxel,
                                    _colorMap.selectedColor(),
                                    _colorMap.selectedQColor().red(),
                                    _colorMap.selectedQColor().green(),
                                    _colorMap.selectedQColor().blue() );
      }
      break;
    case ActionDrawSurfel:
      ps = _surface.getCloserSurfel( _oldX, _oldY, width(), height() );
      if ( ps ) {
        ps->color = _colorMap.selectedColor();
        forceRedraw();
      }
      break;
    default:
      break;
    }
    break;
  case Qt::MidButton :
    _middleButton = true;
    break;
  case Qt::RightButton :
    _rightButton = true;
    _mouseHasMoved = false;
    _oldX = e->pos().x();
    _oldY = e->pos().y();
    break;
  default:
    e->ignore(); return;
    break;
  }
  e->accept();
}

void
View3DWidget::mouseMoveEvent( QMouseEvent * e )
{
  bool shift = e->modifiers() & Qt::ShiftModifier;
  bool control = e->modifiers() & Qt::ControlModifier;
  if ( ! _document->valid() ) { e->ignore(); return; }

  e->accept();

  if ( _leftButton || _rightButton ) {
    if ( _leftButton ) _mouseHasMoved = true;
    if ( _rightButton ) _animate = true;
    int deltaX = e->pos().x() - _oldX;
    int deltaY = e->pos().y() - _oldY;
    float theta_z = 0.0;
    float theta_y = (-1 * M_PI * deltaX) / width();
    float theta_x = (-1 * M_PI * deltaY) / height();

    if ( control ) {
      if ( fabs( static_cast<float>( deltaY ) ) >
           fabs( static_cast<float>( deltaX ) ) ) {
        theta_z = theta_x;
        if ( ( e->pos().x() < ( width() / 2 ) ) )
          theta_z = - theta_z;
      }
      else {
        theta_z = theta_y;
        if ( ( e->pos().y() > ( height() / 2 ) ) )
          theta_z = - theta_z;
      }
      theta_y = theta_x = 0.0;
    }
    if ( _action == ActionMove && !shift ) {
      {
        int x = e->pos().x();
        int y = e->pos().y();
        int w = width();
        int h = height();
        _animateRotationQ = trackballQ( ( 2.0 * _oldX - w ) / w,
                                        ( 2.0 * _oldY - h ) / h,
                                        ( 2.0 * x - w ) / w,
                                        ( 2.0 * y - h ) / h );
        if ( _rotationBox )
          _surface.rotate( _animateRotationQ, Surface::RotateNoSurfel );
        else
          _surface.rotate( _animateRotationQ );
      }
      _animRho = theta_x;
      _animTheta = theta_y;
      _animPhi = theta_z;
      forceRedraw();
      if ( _leftButton && _writeAnimation )
        writeView();
    } else if ( _action == ActionMoveLight ) {
      int x = e->pos().x();
      int y = e->pos().y();
      int w = width();
      int h = height();
      _surface.rotateLight( trackballQ( ( 2.0 * _oldX - w ) / w,
                                        ( 2.0 * _oldY - h ) / h,
                                        ( 2.0 * x - w ) / w,
                                        ( 2.0 * y - h ) / h ) );

      forceRedraw();
    }
    _oldX = e->pos().x();
    _oldY = e->pos().y();
    e->accept();
    return;
  } else {
    if ( shift ) {
#ifdef HAVE_EXPERIMENTAL
      setShading( Surface::ColorFixed );
      // Surfel *ps;
      _leftButton = false;
      //       ps = getCloserSurfel( e->pos().x(), e->pos().y() );
      //       if ( ps ) {
      // 	if ( true || _document->ucVolume()->depth() == 1 )
      // 	  _surface.drawSurfel( ps, _drawRadius, 88 );
      // 	else
      // 	  _surface.drawSurfel( ps, _drawRadius, 88 );
      // 	forceRedraw();
      //       }
#endif
    }
  }
  e->ignore();
}

void View3DWidget::mouseReleaseEvent( QMouseEvent * e )
{
  if ( ! _document->valid() ) {
    e->ignore();
    return;
  }
  switch ( e->button() ) {
  case Qt::LeftButton :
    if ( _action == ActionMove && _leftButton ) {
      _leftButton = false;
      if ( _rotationBox )
        _surface.rotateSurfels(Surface::RotateVisibleSurfels);
      forceRedraw(true);
    }
    break;
  case Qt::MidButton :
    _middleButton = false;
    break;
  case Qt::RightButton :
    _rightButton = false;
    recordRotation();
    _animRho = 0.5 * M_PI / ( static_cast<int>( M_PI / _animRho ) );
    _animTheta = 0.5 * M_PI / ( static_cast<int>( M_PI / _animTheta ) );
    _animPhi = 0.5 * M_PI / ( static_cast<int>( M_PI / _animPhi ) );
    while ( _animate ) {
      _surface.rotate(  _animateRotationQ );
      forceRedraw();
      if ( _writeAnimation ) {
        if ( mxDistance( _surface.rotation(),
                         _recordedRotation ) < 0.3 ) {
          _animationToolBar->endInteractive();
          _animate = false;
        } else 	writeView();
      }
      _application->processEvents();
    }
    break;
  default:
    e->ignore(); return;
    break;
  }
  e->accept();
}

void
View3DWidget::resizeEvent( QResizeEvent * e )
{
  int newWidth = e->size().width();
  int newHeight = e->size().height();
  if ( _capture ) {
    newWidth ^= 1;
    newHeight ^= 1;
  }
  if ( newWidth < _image.width() && newHeight < _image.width() ) {
    QImage clone( _image );
    _image = clone.copy( (_image.width()-newWidth)/2,
                         (_image.height()-newHeight)/2,
                         newWidth,
                         newHeight );

    _needToRebuild = false;
    _fineRefreshAsked = false;
    repaint();
  } else {
    _image = QImage( newWidth, newHeight, QImage::Format_ARGB32_Premultiplied );
    _surface.drawBoundingBox( _image, _colorMap );
    _needToRebuild = false;
    _fineRefreshAsked = false;
    repaint();
  }

  emit info( QString("3D view is %1x%2").arg( width() ).arg( height() ) );
}

void
View3DWidget::edges( bool on )
{
  if ( on != _surface.edges() ) {
    _surface.edges( on );
    forceRedraw();
  }
}

void
View3DWidget::changeBackground()
{
  QColor c = QColorDialog::getColor(  _backgroundColor );
  if ( !c.isValid() )  return;
  _backgroundColor = c;
  _colorMap.setBackgroundColor( _backgroundColor );
  globalSettings->bgColor( _backgroundColor );
  forceRedraw();
}

void
View3DWidget::zoomIn()
{
  _surface.zoomIn();
  emit info( QString("Zoom = %1").arg( _surface.zoom() ) );
  forceRedraw( true );
}

void
View3DWidget::zoomOut()
{
  _surface.zoomOut();
  emit info( QString("Zoom = %1").arg( _surface.zoom() ) );
  forceRedraw( true );
}

void
View3DWidget::zoomFit()
{
  _surface.zoomFit( width(), height() );
  emit info( QString("Zoom = %1").arg( _surface.zoom() ) );
  forceRedraw( true );
}

void
View3DWidget::zoomOne()
{
  _surface.zoom(1.0);
  emit info( QString("Zoom = %1").arg( _surface.zoom() ) );
  forceRedraw( true );
}

void
View3DWidget::setShading( int index )
{
  TRACE << " SHADING CHANGED IN View3DWidget " << index << "\n";
  if ( index == -1 )
    index = _surface.shading();

  if ( (! _surface.validNormals()) &&
       ( index == Surface::ColorFromLight ||
         index == Surface::ColorFromLightedMaterial ||
         index == Surface::ColorFromLightedColorMap ||
         index == Surface::ColorFromLightedLUT ) ) {
    globalProgressReceiver.showMessage( "Computing normals..." );
  }

  if ( index == Surface::ColorFromLight ||
       index == Surface::ColorFromLightedMaterial ) {
    _colorMap.type( ColorMap::Light );
  }

  if ( index == Surface::ColorFromCurvature ) {
    switch ( globalSettings->curvatureType() ) {
    case MEAN_CURVATURE:
      globalProgressReceiver.showMessage( "Computing mean curvature..." );
      break;
    case GAUSSIAN_CURVATURE:
      globalProgressReceiver.showMessage( "Computing Gaussian curvature..." );
      break;
    case MAXIMUM_CURVATURE:
      globalProgressReceiver.showMessage( "Computing maximum curvature..." );
      break;
    }
    _application->processEvents();
  }

#if defined(_EXTRA_FUNCTIONS_)
  _time.restart();
#endif
  if ( index != -1 )
    _surface.setShading( index );
  else
    _surface.refreshShading();
#if defined(_EXTRA_FUNCTIONS_)
  if ( index == Surface::ColorFromLight ||
       index == Surface::ColorFromLightedMaterial ||
       index == Surface::ColorFromLightedColorMap ||
       index == Surface::ColorFromLightedLUT )  {
    int elapsed = _time.elapsed();
    emit info( QString( "Computation time %1s%2ms" )
               .arg( elapsed / 1000 )
               .arg( elapsed % 1000 ) );
  }
#endif

  forceRedraw( true );

  if ( index == Surface::ColorFromLight ||
       index == Surface::ColorFromLightedMaterial ||
       index == Surface::ColorFromLightedColorMap ||
       index == Surface::ColorFromLightedLUT ||
       index == Surface::ColorFromCurvature ) {
    globalProgressReceiver.showMessage(0);
  }
}

void
View3DWidget::reverseColorMap( )
{
  _colorMap.reverse();
}

void
View3DWidget::drawRadius( int radius )
{
  _drawRadius = radius;
}

void
View3DWidget::writeView( const char * fileName, const char * format )
{
  QImage image( size(), QImage::Format_ARGB32_Premultiplied );
  _surface.drawFineSurface( image, _colorMap );

  if ( globalSettings->drawAxis() ) drawAxis( image );
  if ( globalSettings->drawColorMap() ) {
    QImage cmap = _colorMap.image();
    QPainter painter( &image );
    painter.drawImage( width() - (5 + cmap.width()),
                       height() - ( 5 + cmap.height() ),
                       cmap,  0, 0, -1, -1, Qt::AutoColor );
  }

  if ( fileName ) {
    image.save( QString( fileName ), format );
  } else {
    image.save( _animationToolBar->path() +
                _animationToolBar->nextFileName()  ,
                _animationToolBar->format().toLatin1() );
  }
}

void
View3DWidget::captureFileName( const QString & fileName )
{
  if ( _capture && fileName != _captureFileName )
    capture( false );
  _captureFileName = fileName;
}

void
View3DWidget::capture( bool on )
{
  if ( on == _capture ) return;
  if ( on ) {
    _captureFile.open( _captureFileName.toLatin1(), std::ios_base::app | std::ios_base::ate | std::ios_base::out );

    SSize w = width(), h = height();
    w &= ~1l;
    h &= ~1l;
    _image = QImage( w, h , QImage::Format_ARGB32_Premultiplied);
    emit info( QString("View image is %1x%2").arg( w ).arg( h ) );
    _captureSize = (_image.scanLine(1) - _image.scanLine(0)) * _image.height();
    if ( ! _captureFile.tellp() ) {
      _captureFile << w << std::endl;
      _captureFile << h << std::endl;
      _captureFile << (_image.scanLine(1) - _image.scanLine(0)) << " bytes per line" << std::endl;
      _captureFile << _captureSize << " bytes per frame" << std::endl;
    }
  }
  else {
    _captureFile.close();
    _captureSize = 0;
  }
  _capture = on;
}

void
View3DWidget::animateAspect( bool status )
{
  if ( !status ) {
    _animateAspect = false;
    _surface.aspect( -1, -1, -1 );
    return;
  }

  _animateAspect = true;
  float animateTime = 0;
  int animateFrequency[3];
  float animatePhase[3];

  srand( time( 0 ) );
  animateFrequency[0] = rand() % 3 + 1;
  animateFrequency[1] = rand() % 3 + 1;
  animateFrequency[2] = rand() % 3 + 1;
  animatePhase[0] = 0.5 * M_PI * ( rand() % 10) / 10.0;
  animatePhase[1] = 0.5 * M_PI * ( rand() % 10) / 10.0;
  animatePhase[2] = 0.5 * M_PI * ( rand() % 10) / 10.0;

  float ax, ay, az;
  while ( _animateAspect ) {
    ax = 1 + 0.3 * cos( animateFrequency[0] * animateTime + animatePhase[0] );
    ay = 1 + 0.3 * cos( animateFrequency[1] * animateTime + animatePhase[1] );
    az = 1 + 0.3 * cos( animateFrequency[2] * animateTime + animatePhase[2] );
    aspect( ax, ay, az );
    _application->processEvents();
    animateTime += 0.1;
    if ( _writeAnimation ) writeView();
  }
}

void
View3DWidget::drawAxis( QImage & target )
{
  const QImage & im = axisImage();
  int h = im.height();
  for ( int y = 0 ; y < h ; y++ ) {
    uint  * dst = reinterpret_cast<uint*>( target.scanLine( y + (target.height() - h ) ) ) + 10 ;
    const uint * src = reinterpret_cast<const uint*>( im.scanLine( y ) );
    int n = im.width();
    uint bg = _backgroundColor.rgb();
    while ( n-- ) {
      if ( *src != bg ) *dst = *src;
      dst++;
      src++;
    }
  }
}

void
View3DWidget::contour()
{
  static int iterations = 1;
  if ( _document && _document->image()->depth() == 1 ) {
    _contour.setImage( *(_document->image()), iterations, 1  );
    if ( ! globalSettings->fullVolumeBoxAction()->isChecked() )
      _surface.surfelList().colorize( _contour.edgelList() );
    forceRedraw();
    ++iterations;
  }
}

void
View3DWidget::colorizeBorder()
{
  Surfel *ps = _surface.surfelList().begin();
  Surfel *end = _surface.surfelList().end();
  while ( ps != end ) {
    _document->setVoxelValue( ps->voxel,
                              _colorMap.selectedColor(),
                              _colorMap.qcolors()[ ps->color ].red(),
        _colorMap.qcolors()[ ps->color ].green(),
        _colorMap.qcolors()[ ps->color ].blue(),
        false
        );
    ++ps;
  }
}

void
View3DWidget::view2DImage( const QImage * image )
{
  _view2DImage = image;
}

void
View3DWidget::drawEdgelList( EdgelList & edgelList )
{
  return;
  Surface & surface = _surface;
  _surface.drawEdgelList( edgelList,  _image, _colorMap,
                          surface.rotation(),
                          _surface.surfelList().centerTranslation() );
  forceRedraw();
}

void View3DWidget::virtualPointer( QProcess * process ) {
  _virtualPointer = process;
  connect( process, SIGNAL( readyRead() ),
           this, SLOT( virtualPointerMotion() ) );
}

QProcess *
View3DWidget::virtualPointer()
{
  return _virtualPointer;
}


namespace {
inline QPoint eyesPosition2Mouse( const QPoint & eyes,
                                  const QPoint & reference,
                                  const QSize & size )
{
  const int half_width = size.width()/2;
  const int half_height = size.height()/2;
  int x = static_cast<int>( half_width +
                            (0.5 * (eyes.x() - reference.x()) / 320.0) * half_width );
  int y = static_cast<int>( half_height +
                            (0.5 * (reference.y() - eyes.y()) / 240.0) * half_height );
  return QPoint( x, y );
}
inline QPoint eyesPosition2Shift( const QPoint & eyes,
                                  const QPoint & reference,
                                  const QSize & size )
{
  int x = static_cast<int>( (0.33 * (eyes.x() - reference.x()) / 640.0) * size.width() );
  int y = static_cast<int>( (0.33 * (reference.y() - eyes.y()) / 480.0) * size.height() );
  return QPoint( x, y );
}
inline QPoint position2Mouse( int x, int y, const int & width, const int & height )
{
  return QPoint( static_cast<int>( 0.75 * width * x / static_cast<double>( 640 ) ),
                 static_cast<int>( 0.75 * height * (480 - y) / static_cast<double>( 480 ) ) );
}
inline QPoint position2Shift( int x, int y, const int & width, const int & height )
{
  return QPoint( static_cast<int>( 0.15 * width * x / static_cast<double>( 640 ) ),
                 static_cast<int>( 0.15 * height * (480 - y) / static_cast<double>( 480 ) ) );
}
}

void View3DWidget::virtualPointerMotion()
{
  static int lastSize;
  static int diff;
  static QPoint referencePoint;
  struct Command {
    int x;
    int y;
    int size;
    int command;
  };
  QByteArray commands =  _virtualPointer->readAll();
  Command * c = reinterpret_cast<Command*>( commands.data() );
  QPoint point( c-> x, c->y );
  if ( c->command == 1 /* Down */ ) {
    referencePoint = point;
    QMouseEvent event( QEvent::MouseButtonPress,
                       // position2Mouse( c->x, c->y, size().width(), size().height() ),
                       eyesPosition2Mouse( point,
                                           referencePoint,
                                           size() ),
                       Qt::LeftButton,
                       Qt::LeftButton,
                       Qt::NoModifier );
    mousePressEvent( &event );
    lastSize = (c->size == -1) ? 0 : c->size;
  }
  if ( c->command == 0 /* Motion */ ) {
    QMouseEvent event( QEvent::MouseMove,
                       // point = position2Mouse( c->x, c->y, size().width(), size().height() ),
                       eyesPosition2Mouse( point,
                                           referencePoint,
                                           size() ),
                       Qt::NoButton,
                       Qt::LeftButton,
                       Qt::NoModifier );
    QPoint shift = eyesPosition2Shift( point, referencePoint, size() );
    _surface.viewCenterShift( shift.x(), shift.y() );

    mouseMoveEvent( &event );
    if ( c->size != -1 ) {
      diff = c->size - lastSize;
      if ( diff < 0 ) diff = -diff;
      double z = c->size / static_cast<double>( lastSize );
      _surface.zoom( _surface.zoom() * z );
      emit info( QString("Zoom = %1").arg( _surface.zoom() ) );
      lastSize = c->size;
    }
    forceRedraw( true );
  }
  if ( c->command == 2 /* Up */ ) {
    QMouseEvent event( QEvent::MouseButtonRelease,
                       position2Mouse( c->x, c->y, size().width(), size().height() ),
                       Qt::LeftButton,
                       Qt::NoButton,
                       Qt::NoModifier );
    mouseReleaseEvent( &event );
  }
}

void View3DWidget::checkFPS()
{
  if ( !_document ) return;
  Quaternion q = _surface.rotationQuaternion();
  _time.restart();
  _surface.setRotation( q );
  _surface.drawSurface( _image, _colorMap );
  _rotationBox = ( ( 1000.0 / _time.elapsed() ) < globalSettings->rotationBoxMaxFrames() );
}

void View3DWidget::writeAnimation( bool onOff )
{
  _writeAnimation = onOff;
}
