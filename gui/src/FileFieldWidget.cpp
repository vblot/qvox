/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   FileFieldWidget.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 * 
 * @brief  Declaration of the class FileFieldWidget
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "FileFieldWidget.h"

#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>

/**
 * Constructor
 */
FileFieldWidget::FileFieldWidget( QWidget * parent ) : QWidget(parent) {
   
   QHBoxLayout *hbox = new QHBoxLayout;
   setLayout( hbox  );
   _mode = Output;

   _le = new QLineEdit( this );
   _le->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );

   QPushButton * pb = new QPushButton("...", this );
   pb->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
   pb->setMinimumWidth( 30 );
   hbox->setSpacing(0);
   hbox->setMargin(0);
   
   //hbox->setAlignment( Qt::AlignRight );
   hbox->addWidget( _le, 0 );
   hbox->addWidget( pb, 0 );

   connect( pb, SIGNAL( clicked() ),
	    this, SLOT( pbClicked() ) );
}

void
FileFieldWidget::mode( Mode m )
{
   _mode = m;
}

void
FileFieldWidget::path( const QString & path )
{
   _path = path;
}


void
FileFieldWidget::pbClicked()
{
   QString dir = QFileInfo( _le->text() ).absolutePath(); 
   QString filename;
   if ( _mode == Input ) 
      filename = QFileDialog::getOpenFileName( this, 
					       "Input file",
					       _path.size()?_path:QString(), 
					       QString(),
					       0, 0 );
   else
      filename = QFileDialog::getSaveFileName( this,
					       "Output file",
					       _path.size()?_path:QString(),
					       QString(), 0, 0 );
   if ( !filename.isEmpty() ) {
      _le->setText( filename );
      _path = QFileInfo( filename ).absolutePath();
   }
}
  
