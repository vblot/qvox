/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginConsole.h
 * @author Sebastien Fourey (GREYC)
 * @date   Nov 2008
 * 
 * @brief  Declaration of the class PluginConsole
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "PluginConsole.h"

#include <QVBoxLayout>
#include <QProcess>
#include <QTextEdit>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QLabel>
#include <QPalette>
#include <QComboBox>
#include "globals.h"
#include "Settings.h"

/**
 * Constructor
 */
PluginConsole::PluginConsole(QWidget * parent)
   : QDialog( parent ),
     _foreground( 210, 110, 0 ) {
   QFont font( "Courier", 9 );
   QFont consoleFont( "Courier", globalSettings->consoleFontSize() );
   QVBoxLayout * vbox = new QVBoxLayout;
   setLayout( vbox );
   setWindowTitle("Plugin output");
   
   QHBoxLayout * hbox = new QHBoxLayout;
   vbox->addLayout( hbox );
   QLabel * label = new QLabel( "Command>", this );
   label->setFont( font );
   hbox->addWidget( label );
   _le = new QLineEdit(this);
   _le->setReadOnly( true );
   

   _le->setFont( font );
   hbox->addWidget( _le );

   _text = new QTextEdit( this );
   _text->setReadOnly( true );
   _text->setWordWrapMode( QTextOption::WrapAnywhere );
   _text->setSizePolicy( QSizePolicy::Preferred,  QSizePolicy::Preferred );
   _text->setFont( consoleFont );
   QPalette palette = _text->palette();
   QColor background(0,0,0);
   palette.setBrush( QPalette::Inactive, QPalette::Base, background );
   palette.setBrush( QPalette::Active, QPalette::Base, background );
   palette.setColor( QPalette::Inactive, QPalette::Base, background );
   palette.setColor( QPalette::Active, QPalette::Base, background );
   _text->setPalette( palette );

   vbox->addWidget( _text );

   hbox = new QHBoxLayout;
   vbox->addLayout( hbox );
   
   _labelBinPath = new QLabel("BinPath");
   _labelBinPath->setFrameStyle(QFrame::Panel | QFrame::Sunken);
   _labelBinPath->setFont( font );
   _labelLibPath = new QLabel("LibPath");
   _labelLibPath->setFrameStyle(QFrame::Panel | QFrame::Sunken);
   _labelLibPath->setFont( font );
   
   _cbFontSize = new QComboBox( this );
   QStringList l = QString("8,9,10,11,12,13,14,16").split( QChar(',') );
   QString strSize = QString("%1").arg( globalSettings->consoleFontSize() );				   
   QString s;
   int i = 0;
   int current = -1;
   while ( !l.isEmpty() ) {
      _cbFontSize->addItem( s = l.takeFirst() );
      if ( strSize == s )  {
	 changeFontSize( strSize );
	 current = i;
      }
      ++i;
   }
   if ( current >= 0 ) 
      _cbFontSize->setCurrentIndex( current );
   else {
      changeFontSize( strSize );
   }

   _cbFontSize->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Preferred );
   connect( _cbFontSize, SIGNAL( activated( const QString & ) ),
	    this, SLOT( changeFontSize( const QString & ) ) );

   _pbClose = new QPushButton( "&Close", this );
   hbox->addWidget( _labelBinPath );
   hbox->addWidget( _labelLibPath );
   label = new QLabel("Font size", this);
   label->setFont( font );
   label->setAlignment( Qt::AlignRight|Qt::AlignVCenter );
   hbox->addWidget( label );
   hbox->addWidget( _cbFontSize );
   hbox->addWidget( _pbClose );
   connect( _pbClose, SIGNAL( clicked() ),
	    this, SLOT( close() ) );
   setAttribute( Qt::WA_DeleteOnClose );
   _process = 0;
}

void
PluginConsole::command( const QString & str )
{
   _le->setText( str );
   _le->setCursorPosition( 0 );
}

void
PluginConsole::process( QProcess * process,
			bool stdOut,
			bool stdErr )
{
   _process = process;
   if ( !_process ) return;

   QStringList env = process->environment().filter( QRegExp("^PATH=.*") );
   if ( env.size() ) {
      env.front().replace( "PATH=", "" );
      env.front().replace( ":", "\n" );
      _labelBinPath->setToolTip( env.front() );
   }
   env = process->environment().filter( QRegExp("^LD_LIBRARY_PATH=.*") );
   if ( env.size() ) {
      env.front().replace( "LD_LIBRARY_PATH=", "" );
      env.front().replace( ":", "\n" );
      _labelLibPath->setToolTip( env.front() );
   }

   if ( stdOut ) 
      connect( _process, SIGNAL( readyReadStandardOutput() ),
	       this, SLOT( readSomeStdOut() ) );
   if ( stdErr ) 
      connect( _process, SIGNAL( readyReadStandardError() ),
	       this, SLOT( readSomeStdErr() ) );
}

void
PluginConsole::readSomeStdOut()
{
   _process->setReadChannel( QProcess::StandardOutput );
   _text->setTextColor( _foreground );
   _text->insertPlainText( _process->readAll() );
   _text->moveCursor( QTextCursor::End, QTextCursor::MoveAnchor );
}

void
PluginConsole::readSomeStdErr()
{
   _process->setReadChannel( QProcess::StandardError );
   _text->setTextColor( QColor( 255, 0, 0 ) );
   _text->insertPlainText( _process->readAll() );
   _text->moveCursor( QTextCursor::End, QTextCursor::MoveAnchor );
   _process->setReadChannel( QProcess::StandardOutput );
}

void
PluginConsole::changeFontSize( const QString & str )
{
   QFont f = _text->font();
   f.setPointSize( str.toInt() );
   _text->setFont( f );
   _text->setMinimumWidth( 80 * QFontMetrics( f ).width( QChar(' ') ) );
   _text->setMinimumHeight(25 * QFontMetrics( f ).height() );
   globalSettings->consoleFontSize( str.toInt() );
}
