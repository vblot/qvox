/** -*- c++ -*- c-basic-offset: 3 -*-
 * @file   DialogInputSize.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2007
 *
 * @brief  Declaration of the class DialogInputSize
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "DialogInputSize.h"

#include "Document.h"

/**
 * Constructor
 */
DialogInputSize::DialogInputSize(QWidget *parent):QDialog(parent) {
  CONS( DialogInputSize );
  setupUi( this );
  _document = 0;
  connect( buttonNBands, SIGNAL( toggled(bool) ),
           sbBands, SLOT( setEnabled(bool) ) );
}

DialogInputSize::~DialogInputSize() {

}

void
DialogInputSize::width( int w )
{
  sbWidth->setValue( w );
}


void
DialogInputSize::height( int h )
{
  sbHeight->setValue( h );
}


void
DialogInputSize::depth( int d )
{
  sbDepth->setValue( d );
}

void
DialogInputSize::endianness( Endianness e )
{
  if ( e == BigEndian ) rbBigEndian->setChecked( true );
  if ( e == LittleEndian ) rbLittleEndian->setChecked( true );
}

Endianness
DialogInputSize::endianness()
{
  return rbBigEndian->isChecked() ? BigEndian : LittleEndian;
}

long
DialogInputSize::width()
{
  return static_cast<long>( sbWidth->value() );
}

long
DialogInputSize::height()
{
  return static_cast<long>( sbHeight->value() );
}


long
DialogInputSize::depth()
{
  return static_cast<long>( sbDepth->value() );
}

long
DialogInputSize::bands()
{
  if ( buttonSingleBand->isChecked() ) return 1;
  if ( buttonThreeBands->isChecked() ) return 3;
  return sbBands->value();
}

void
DialogInputSize::bands( int b )
{
  if ( b == 1 ) {
    buttonSingleBand->setChecked( true );
    buttonThreeBands->setChecked( false );
    buttonNBands->setChecked( false );
    sbBands->setEnabled( false );
    return;
  }
  if ( b == 3 ) {
    buttonSingleBand->setChecked( false );
    buttonThreeBands->setChecked( true );
    buttonNBands->setChecked( false );
    sbBands->setEnabled( false );
    return;
  }
  buttonSingleBand->setChecked( false );
  buttonThreeBands->setChecked( false );
  buttonNBands->setChecked( true );
  sbBands->setEnabled( true );
  sbBands->setValue( b );
}

bool
DialogInputSize::multiplexed()
{
  return rbLayoutBWHD->isChecked();
}

void DialogInputSize::guessSizeFromFilename(const QString & filename )
{ 
  QFileInfo info(filename);
  unsigned long fileSize = info.size();
  QString str = info.completeBaseName();
  QString::iterator i = str.begin();
  while ( i != str.end() ) {
    if ( ! i->isDigit() )
      *i = QChar(' ');
    ++i;
  }
  str = str.trimmed();
  QStringList list = str.split(QRegExp("\\s+"));
  if ( list.size() >= 3 ) {
    for ( int i = 0; i <= list.size() - 3; ++i) {
      const unsigned long w = list[i].toInt();
      const unsigned long h = list[i+1].toInt();
      const unsigned long d = list[i+2].toInt();
      const unsigned long volume = w*h*d;
      int bands = 0;
      int valueTypeSize = 0;
      if ( ((volume == fileSize) && ((bands=1)) && ((valueTypeSize=1)))
           || ((volume*2 == fileSize) && ((bands=1)) && ((valueTypeSize=2)))
           || ((volume*4 == fileSize) && ((bands=1)) && ((valueTypeSize=4)))
           || ((volume*3 == fileSize) && ((bands=3)) && ((valueTypeSize=1)))
           || ((volume*6 == fileSize) && ((bands=3)) && ((valueTypeSize=2))) ) {
        sbWidth->setValue(w);
        sbHeight->setValue(h);
        sbDepth->setValue(d);
        switch (bands) {
        case 1 : buttonSingleBand->setChecked(true); break;
        case 3 : buttonThreeBands->setChecked(true); break;
        default: break;
        }
        switch (valueTypeSize) {
        case 1 : buttonUChar->setChecked(true); break;
        case 2 : buttonUShort->setChecked(true); break;
        case 4 : buttonULong->setChecked(true); break;
        default: break;
        }
      }
    }
  }
}

ValueType
DialogInputSize::valueType()
{
  if ( buttonBool->isChecked() ) return Po_ValB;
  if ( buttonUChar->isChecked() ) return Po_ValUC;
  if ( buttonSShort->isChecked() ) return Po_ValSS;
  if ( buttonUShort->isChecked() ) return Po_ValUS;
  if ( buttonSLong->isChecked() ) return Po_ValSL;
  if ( buttonULong->isChecked() ) return Po_ValUL;
  if ( buttonSFloat->isChecked() ) return Po_ValSF;
  if ( buttonSDouble->isChecked() ) return Po_ValSD;
  return Po_ValUnknown;
}

void
DialogInputSize::valueType( int t )
{
  switch ( static_cast<ValueType>(t) ) {
  case Po_ValB:
    buttonBool->setChecked( true );
    break;
  case Po_ValUC:
    buttonUChar->setChecked( true );
    break;
  case Po_ValSS:
    buttonSShort->setChecked( true );
    break;
  case Po_ValUS:
    buttonUShort->setChecked( true );
    break;
  case Po_ValUL:
    buttonULong->setChecked( true );
    break;
  case Po_ValSL:
    buttonSLong->setChecked( true );
    break;
  case Po_ValSF:
    buttonSFloat->setChecked( true );
    break;
  case Po_ValSD:
    buttonSDouble->setChecked( true );
    break;
  default:
    ERROR << "DialogInputSize(): Unrecognized value type (" << t << ")\n";
  }
}

void
DialogInputSize::document( Document * document )
{
  if ( _document )
    disconnect( _document, 0, this, 0 );
  _document = document;
  if ( _document )
    connect( _document, SIGNAL( volumeChanged() ),
             this, SLOT( changeVolumeInfos() ) );
}

void
DialogInputSize::changeVolumeInfos()
{
  sbWidth->setValue( _document->image()->width() );
  sbHeight->setValue( _document->image()->height() );
  sbDepth->setValue( _document->image()->depth() );
  bands( _document->image()->bands() );
  switch ( _document->valueType() ) {
  case Po_ValB:
    buttonBool->setChecked( true );
    break;
  case Po_ValUC:
    buttonUChar->setChecked( true );
    break;
  case Po_ValSS:
    buttonSShort->setChecked( true );
    break;
  case Po_ValUS:
    buttonUShort->setChecked( true );
    break;
  case Po_ValSF:
    buttonSFloat->setChecked( true );
    break;
  case Po_ValSD:
    buttonSDouble->setChecked( true );
    break;
  case Po_ValUL:
    buttonULong->setChecked( true );
    break;
  case Po_ValSL:
    buttonSLong->setChecked( true );
    break;
  }
  sbWidth->setValue( _document->image()->width() );
}

int
DialogInputSize::execRaw()
{
  setWindowTitle("Raw volume properties");
  gbEndianness->setEnabled(true);
  gbLayout->setVisible(true);
  int nb = bands();
  sbBands->setEnabled( nb != 1 && nb != 3 );
  endianness( endian() );
  return exec();
}

int
DialogInputSize::execNew()
{
  setWindowTitle("New volume properties");
  gbEndianness->setEnabled(false);
  gbLayout->setVisible(false);
  int nb = bands();
  sbBands->setEnabled( nb != 1 && nb != 3 );
  endianness( endian() );
  return exec();
}
