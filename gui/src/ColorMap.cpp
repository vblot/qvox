/**
 * @file   ColorMap.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:45:02 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ColorMap.h"
#include "Settings.h"
#include <QPainter>

namespace {
  const unsigned int SingleLineSize = 800;
  const unsigned int BackgroundLineSize = 2048;
  const unsigned char contrast_colormap_ramdom[3][256] = { 
    { 229,43,142,253,219,255,229,226,12,26,45,25,157,207,202,255,225,100,240,237,130,15,233,251,61,202,214,242,181,81,248,238,244,221,71,249,72,78,239,139,221,34,66,207,210,230,242,237,59,126,245,169,189,72,146,226,36,102,121,254,90,52,230,100,57,100,240,64,235,134,222,187,116,108,129,10,44,253,235,254,194,74,253,189,250,88,24,72,214,252,22,251,199,67,235,140,242,142,10,231,105,204,246,136,252,1,205,215,241,2,236,75,243,179,2,162,127,112,190,89,12,6,78,235,53,62,68,62,24,241,117,20,74,26,22,229,128,20,69,36,192,251,37,113,227,140,33,34,255,51,68,221,202,57,248,155,250,222,234,240,196,3,2,237,47,224,248,108,147,1,67,7,47,101,129,49,120,37,150,3,117,2,16,112,23,173,5,255,76,7,172,43,100,30,194,110,37,175,68,241,1,4,193,70,18,9,207,200,34,158,7,69,4,234,45,24,235,247,12,84,250,49,40,23,9,154,253,20,228,182,11,52,16,20,202,84,14,237,75,40,2,75,123,202,131,11,139,46,161,208,255,250,70,2,218,173  },
    { 19,18,3,35,244,131,89,53,10,31,252,22,54,225,5,173,63,7,250,253,9,11,215,105,7,96,175,112,107,205,9,0,83,252,19,236,29,9,166,163,210,245,130,225,206,172,43,15,229,11,97,5,22,229,16,46,22,92,244,125,14,157,221,225,36,137,123,71,204,36,76,126,212,33,64,20,254,102,187,255,16,82,237,239,241,25,123,18,3,231,90,186,196,13,236,163,238,184,63,223,181,111,146,56,223,147,2,38,239,73,55,248,129,176,107,225,245,132,131,92,125,173,87,142,196,239,87,47,254,244,30,222,231,152,135,52,195,34,94,254,35,30,227,5,250,18,72,226,150,224,251,83,111,73,185,203,202,39,107,112,233,240,120,176,254,35,233,213,43,92,216,37,137,4,16,225,177,3,169,5,9,130,226,42,247,229,75,45,248,83,98,0,122,135,157,240,28,229,222,241,211,90,18,252,5,253,138,15,186,228,228,11,139,20,33,22,21,38,209,206,11,9,240,15,227,7,255,2,246,209,231,10,232,87,152,238,72,245,250,232,176,21,11,45,215,152,44,170,126,235,243,235,233,105,241,94 },
    { 24,53,0,233,74,232,23,220,126,230,88,204,40,95,133,212,254,162,218,223,3,172,99,157,252,123,245,28,1,168,189,254,85,70,187,228,100,149,200,228,28,181,45,221,39,154,14,25,2,217,105,109,234,117,202,10,111,31,0,28,177,114,112,13,3,2,156,222,85,8,75,30,188,179,219,121,39,5,206,21,250,98,44,95,32,14,2,115,234,70,240,244,35,71,163,188,245,142,39,9,33,142,97,247,67,27,83,15,45,42,176,182,43,35,52,237,97,48,165,3,25,77,177,173,155,198,236,27,2,181,221,143,163,160,96,191,66,207,117,43,9,159,216,218,74,200,194,218,32,136,184,146,3,234,219,172,200,247,190,15,81,3,3,171,251,165,246,40,177,221,35,208,182,250,132,212,235,86,92,15,247,215,193,209,100,248,7,21,84,241,106,209,79,18,193,223,10,166,245,51,23,56,254,107,220,177,212,224,106,29,126,12,224,16,254,254,234,229,5,241,191,49,106,252,143,173,85,5,122,162,188,30,165,181,251,13,27,6,127,236,222,155,254,157,28,69,36,242,29,239,248,166,19,254,222,251 }
  };

  const unsigned char contrast_colormap_uniform[3][256] = { 
    { 112,112,144,144,80,176,112,112,144,144,80,176,80,176,80,176,112,208,48,144,48,208,112,112,144,48,144,112,208,208,48,80,176,80,176,112,144,176,112,80,176,144,80,80,48,208,176,48,240,48,144,144,144,176,80,16,176,240,80,80,208,80,144,240,16,240,16,144,80,176,176,208,16,144,176,80,80,48,208,48,208,208,48,208,48,208,48,240,16,240,16,240,48,48,144,112,208,48,208,240,16,16,112,240,176,80,144,80,80,80,240,16,16,240,176,240,208,16,240,16,240,208,16,48,16,240,240,16,16,240,16,240,16,240,208,48,240,48,240,48,16,240,16,208,16,240,16,176,240,16,176,16,240,144,112,112,16,240,240,16,144,208,48,208,16,240,16,208,176,176,48,48,80,208,80,208,176,48,80,208,80,112,80,112,240,16,112,176,48,240,208,176,48,144,48,240,112,48,144,112,16,112,112,240,16,176,208,16,208,112,208,48,176,176,80,144,112,112,144,112,144,112,208,80,144,80,176,48,208,176,48,144,112,48,144,144,208,208,48,112,80,176,80,176,112,176,112,144,80,176,80,144,112,144,144,112 },
    { 144,112,112,144,144,144,80,176,80,176,112,176,176,80,80,80,208,144,112,48,112,112,208,48,208,144,144,112,176,80,176,48,208,208,48,144,80,144,80,112,112,176,144,144,48,208,176,48,112,208,16,240,16,80,176,144,80,144,176,80,144,80,208,80,80,176,176,208,16,240,16,144,80,48,48,208,208,176,80,80,240,16,240,240,16,80,240,48,48,208,208,48,208,48,240,16,208,48,208,144,144,112,240,144,16,16,16,16,240,240,176,176,80,80,240,80,240,16,16,208,208,16,240,240,16,16,240,16,240,16,208,240,48,240,16,16,48,240,208,16,240,48,48,240,240,16,16,240,176,176,16,80,240,16,240,16,112,112,112,144,240,48,208,48,48,208,208,16,208,208,16,176,48,176,48,176,48,80,240,112,240,48,16,208,176,176,208,16,112,80,112,240,144,48,144,144,48,112,240,16,144,240,16,112,112,176,208,112,48,240,48,208,144,112,112,80,80,176,176,176,112,112,176,48,144,208,48,80,80,208,80,112,144,176,208,48,112,144,144,48,176,176,80,144,80,112,176,80,112,112,144,176,112,112,144,144 },
    { 96,160,96,160,160,96,96,160,160,96,96,160,96,96,160,160,96,160,160,96,96,96,160,160,160,96,32,224,96,160,160,96,96,160,96,32,224,224,32,224,32,224,32,224,96,160,32,160,160,96,160,96,96,224,32,160,32,160,224,32,224,224,32,96,96,96,96,224,160,160,160,32,160,32,224,224,32,224,32,32,160,160,96,96,160,224,160,96,96,160,160,160,32,224,32,32,224,32,32,224,224,32,224,32,32,224,224,32,224,32,224,32,224,32,224,224,32,160,96,224,32,224,32,224,32,32,32,224,224,224,32,96,32,224,32,224,224,32,224,32,160,32,224,224,96,160,96,32,32,224,224,32,160,32,32,224,224,32,224,32,224,32,224,224,160,96,96,96,32,224,96,32,224,224,32,32,32,224,96,224,160,224,96,32,160,160,224,96,32,160,32,96,224,224,32,96,32,224,160,160,96,96,96,96,160,224,96,96,160,160,96,160,32,224,32,32,224,224,32,32,224,32,160,160,224,96,160,96,96,160,160,32,224,96,96,160,160,96,160,96,160,96,96,160,160,160,96,96,160,96,96,160,96,160,96,160 }
  };
}

ColorMap::ColorMap( int size ):_bytesPerPixel(32)
{
  CONS( ColorMap );
  _array = 0;
  _size = 0;
  _selectedColor = 1;
  _lineSize = static_cast<int>( MAX_ZOOM * 1.5 );

  alloc( size );

  _singleLine = new UChar[ _bytesPerPixel * SingleLineSize ];
  _backgroundLine = new UChar[ _bytesPerPixel * BackgroundLineSize ];

  // One last color is allocated for TrueColor use... 
  // Therefore, color position "size" is meaningfull for
  // special purpose.
  _qcolors = new QColor[ size + 1 ];
  type( Gray );

  setSingleLineColor( QColor("green") );
  setBackgroundColor( globalSettings->bgColor() );

  _min = -1;
  _max = 256;
  _toonColor = QColor(255,255,255);
}

void ColorMap::alloc( int size )
{
  TRACE << "Colormap size " << size << std::endl;
  if ( size != _size ) {
    if ( _array ) {
      disposeArray( _array[ 0 ] );
      disposeArray( _array );
    }
    _size = size;
    _array = new UChar* [ size ];
    _array[ 0 ] = new UChar [ _bytesPerPixel * _lineSize * size ];
    for ( int i = 1; i < size ; i++ )
      _array[ i ] = _array[ i - 1 ] + _bytesPerPixel * _lineSize;
  }
  memset( _array[0], 0, _bytesPerPixel * _lineSize * size );
}

ColorMap::~ColorMap()
{
  if ( _array ) delete[] _array[ 0 ];
  delete[] _array;
  delete[] _singleLine;
  delete[] _backgroundLine;
  delete[] _qcolors;
}

void
ColorMap::selectColor( int color ) { 
  _selectedColor = color; 
  emit colorSelected( color );
}

void
ColorMap::trueColor( UChar red, UChar green, UChar blue )
{
  _qcolors[ _size ].setRgb( red, green, blue );
}

void
ColorMap::reverse()
{
  QColor qc;
  UChar * tmp;
  UChar *tmpLine  = new UChar[ _lineSize * _bytesPerPixel ];
  memcpy( tmpLine, _array[0], _lineSize * _bytesPerPixel );
  memcpy( _array[0], _array[ _size - 1 ],  _lineSize * _bytesPerPixel );
  memcpy( _array[ _size - 1 ], tmpLine,  _lineSize * _bytesPerPixel );

  qc = _qcolors[0];
  _qcolors[0] = _qcolors[ _size - 1 ];
  _qcolors[ _size - 1 ] = qc;
  int i = 1; 
  int j = _size - 2;
  
  while ( i < j ) {
    qc = _qcolors[i];
    _qcolors[i] = _qcolors[j];
    _qcolors[j] = qc;
    tmp = _array[ i ];
    _array[ i ] = _array[ j ];
    _array[ j ] = tmp;
    i++; j--;
  }
  emit changed( _type );
}

void
ColorMap::setSingleLineColor( const QColor & color )
{
  int count = SingleLineSize;
  _singleLineColor = color;
  QRgb * p = reinterpret_cast<QRgb*>( _singleLine );
  QRgb rgb = color.rgb();
  while ( count-- ) *(p++) = rgb; 
}

void
ColorMap::setBackgroundColor( const QColor & color )
{
  int count = BackgroundLineSize;
  _backgroundColor = color;
  QRgb * p = reinterpret_cast<QRgb*>( _backgroundLine );
  QRgb rgb = color.rgb();
  while ( count-- ) *(p++) = rgb;
}

void
ColorMap::setColor( UChar index, const QColor & color )
{
  QRgb * pcolor = reinterpret_cast<QRgb*>( _array[ index ] );
  QRgb rgb = color.rgb();
  int count = _lineSize;
  while ( count-- )
    *(pcolor++) = rgb;
  _qcolors[ index ] = color;
  emit changed( _type );
}

void
ColorMap::setMin( int min )
{
  _min = min;
  type( static_cast<ColorMap::Type>( _type ) );
}

void
ColorMap::setMax( int max )
{
  _max = max;
  type( static_cast<ColorMap::Type>( _type ) );
}


void
ColorMap::type( int type, int min, int max )
{
  if ( min == -1 ) {
    min = 0;
    max = _size - 1;
  }
  UChar value;
  Size count;
  
  QColor  qcolor;
  QRgb * pcolor, rgb;
  
  float range = static_cast<float>( max - min );
  _type = type;
  
  switch ( type ) {
  case Gray:
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) value = 127;
      else value = static_cast<UChar>( (max - min ) *
				       ( (color - min) /
					 range ) );
      qcolor.setRgb( value, value, value );
      _qcolors[ color ].setRgb( value, value, value );
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;

  case Red:
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) value = 127;
      else value = static_cast<UChar>( (max - min ) *
				       ( (color - min) / range ) );
      qcolor.setRgb( value, 0, 0 );
      _qcolors[ color ].setRgb( value, 0, 0 );
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;
    
  case Green:
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) value = 127;
      else value = static_cast<UChar>( (max - min ) *
				       ( (color - min) / range ));
      qcolor.setRgb( 0, value, 0 );
      _qcolors[ color ].setRgb( 0, value, 0 );
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;

  case Blue:
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) value = 127;
      else value = static_cast<UChar>( (max - min ) *
				       ( (color - min) / range ) );
      qcolor.setRgb( 0, 0, value );
      _qcolors[ color ].setRgb( 0, 0, value );
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;
  case Random:
    srand( time(0) % 0xffff );
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) qcolor.setRgb( 127, 127, 127);
      else qcolor.setRgb( rand() % 256,
			  rand() % 256,
			  rand() % 256 );
      _qcolors[ color ] = qcolor;
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;

  case Contrast:
    for ( int color = 0; color < _size; ++color ) {
      if ( color < min || color > max ) qcolor.setRgb( 127, 127, 127);
      else qcolor.setRgb( contrast_colormap_uniform[0][color],
			  contrast_colormap_uniform[1][color],
			  contrast_colormap_uniform[2][color] );
      _qcolors[ color ] = qcolor;
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;
  case Colors:
    float red,green,blue;
    
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) 
	qcolor.setRgb( 127, 127, 127);
      else {
	HSVtoRGB( red, green, blue,
		  static_cast<int>( 3600 + ( 5 * ( (color - min) / range ) * 360 )) % 360 ,
		  0.9,
		  1.0);
	qcolor.setRgb( static_cast<int>( red * 255 ),
		       static_cast<int>( green * 255 ),
		       static_cast<int>( blue * 255 ) );
      }
      _qcolors[ color ] = qcolor;
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;

  case HueScale:
    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) 
	qcolor.setRgb( 127, 127, 127);
      else {
	HSVtoRGB( red, green, blue,
		  ( (color - min) / range ) * 359,
		  0.9,
		  1.0 );
	qcolor.setRgb( static_cast<int>( red * 255 ),
		       static_cast<int>( green * 255 ),
		       static_cast<int>( blue * 255 ) );
      }
      _qcolors[ color ] = qcolor;
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;

  case Curvature:
    //qcolor.setRgb( 0, 0, 255 );
    qcolor.setHsv( 60, 255, 255 );
    int h,s,v;
    //qcolor.getHsv( &h, &s, &v );
    h = 58; s = 255; v = 255;

    for ( int color = 0; color < _size; color++ ) {
      if ( color < min || color > max ) 
	qcolor.setRgb( 127, 127, 127);
      else {
	if ( color < 128 ) { v -= 1; h+=1; }
	if ( color > 128 ) { v += 1; h+=1; }
	if ( color == 128 ) {
	  h = 180; s = 255; v = 128;
	} 

	qcolor.setHsv( h, s, v );
      }
      _qcolors[ color ] = qcolor;
      rgb = qcolor.rgb();
      pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;
  case Light:
    { 
      int hue, saturation, value,v;
      _toonColor.getHsv( &hue, &saturation, &value );
      for ( int c = 0; c < _size; c++ ) {
	//	v = static_cast<UChar>( 50 + c * 0.65 );
	v = static_cast<UChar>( 45 + c * 0.82 );
	qcolor.setHsv( hue, saturation , v);
	_qcolors[ c ].setHsv( hue, saturation, v );
	rgb = qcolor.rgb();
	pcolor = reinterpret_cast<QRgb*>( _array[ c ] );
	count = _lineSize;
	while ( count-- )
	  *(pcolor++) = rgb;
      }
    }
    break;
  case Toon:
    for ( int color = 0; color < _size; color++ ) {
      if ( color > _min ) { 
	qcolor = _toonColor;
	_qcolors[ color ] = _toonColor;
	rgb = qcolor.rgb();
	pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      }  else { 
	value = 0;
	qcolor.setRgb( value, value, value );
	_qcolors[ color ].setRgb( value, value, value );
	rgb = qcolor.rgb();
	pcolor = reinterpret_cast<QRgb*>( _array[ color ] );
      }
      count = _lineSize;
      while ( count-- )
	*(pcolor++) = rgb;
    }
    break;
  case Undefined:
    ERROR << "ColorMapUndefined should never happen\n";
    break;
  }  
  emit changed( _type );
}

void 
ColorMap::setMaterialColor( QColor color )
{
  if ( _type == Light ) { 
    toonColor( color );
    type( Light );
  }  
  if ( _type == Toon ) {
    toonColor( color );
    type( Toon );
  }
}

void
ColorMap::toonColor( const QColor & color ) {
  _toonColor = color;
}
  
void
ColorMap::HSVtoRGB(float &r, float &g, float &b, float h, float s, float v) const
{
  int i;
  float f, p, q, t;
  
  if( s == 0 ) {  // achromatic (grey)
    r = g = b = v;
    return;
  }

   h /= 60;			// sector 0 to 5
   i = static_cast<int>( floor( h ) );
   f = h - i;			// factorial part of h
   p = v * ( 1 - s );
   q = v * ( 1 - s * f );
   t = v * ( 1 - s * ( 1 - f ) );
   switch( i ) {
   case 0:
      r = v;
      g = t;
      b = p;
      break;
   case 1:
      r = q;
      g = v;
      b = p;
      break;
   case 2:
      r = p;
      g = v;
      b = t;
      break;
   case 3:
      r = p;
      g = q;
      b = v;
      break;
   case 4:
      r = t;
      g = p;
      b = v;
      break;
   default:		// case 5:
      r = v;
      g = p;
      b = q;
      break;
   }
}

void
ColorMap::draw( QPainter & painter, int x, int y ) const
{
  // QSize size( 256, 25 );
  for ( int c = 0; c < 256; c++ ) {
    painter.setPen( _qcolors[c] );
    painter.drawLine( x+c, y+10, x+c, y+24 ); 
  }
  painter.setPen( QColor( ~( backgroundColor().rgb() ) ) );
  painter.drawLine( x, y+5, x, y+10 ); 
  painter.drawLine( x+128, y+5, x+128, y+10 ); 
  painter.drawLine( x+255, y+5, x+255, y+10 ); 

  painter.drawRect( x, y+10, 255, 14 );
  
  QFont f("Courrier", 10 );
  QFontMetrics fm(f);
  QRect rect = fm.boundingRect( "256" );
  
  painter.setFont( f );
  painter.drawText( x+1, y+8, "0" );
  painter.drawText( x + 253 - rect.right(), y + 8, "256" );
}


QImage
ColorMap::image() const
{
  QSize size( 256, 25 );
  QPixmap result( size );
  QPainter p( &result );
  p.fillRect( 0, 0, 256, 25, backgroundColor() ) ;

  for ( int c = 0; c < 256; c++ ) {
    p.setPen( _qcolors[c] );
    p.drawLine( c, 10, c, 24 ); 
  }

  p.setPen( QColor( ~( backgroundColor().rgb() ) ) );
  p.drawLine( 0, 5, 0, 10 ); 
  p.drawLine( 128, 5, 128, 10 ); 
  p.drawLine( 255, 5, 255, 10 ); 

  p.drawRect( 0, 10, 256, 15 );
  
  QFont f("Courrier", 10 );
  QFontMetrics fm(f);
  QRect rect = fm.boundingRect( "256" );
  
  p.setFont( f );
  p.drawText( 1, 8, "0" );
  p.drawText( 253 - rect.right(), 8, "256" );
  p.end();
  
  QImage image( 256, 100, QImage::Format_RGB32 );
  image = result.toImage();
  return image;
}

