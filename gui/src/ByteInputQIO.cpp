/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   ByteInputQIO.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class ByteInputQIO
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "ByteInputQIO.h"
#include <cstdio>

#include <QIODevice>

/**
 * Constructor
 */

ByteInputQIO::ByteInputQIO( QIODevice & io )
   : _io( io ), _ok( io.isOpen() && io.isReadable() ), _gcount( -1 )
{
}

bool
ByteInputQIO::get( char & c )
{
   return ( _ok = ( _gcount = _io.getChar( &c ) ) );
}

bool
ByteInputQIO::getline( char* s, size_t n, char delim )
{  
   char c = 0;
   --n;
   _gcount = 0;
   do { 
      if ( _io.getChar( &c ) && ++_gcount && c != delim )
	 *s++ = c;
      else
	 c = delim;
   } while ( c != delim && static_cast<size_t>(_gcount) < n );
   *s = '\0';
   return _ok = ( _gcount > 0 );
}

bool
ByteInputQIO::read( char *s, size_t n )
{
   qint64 count = 0;
   qint64 nb = 0;
   if ( !n ) return true;
   SSize sn = static_cast<SSize>( n );
   do { 
      _io.waitForReadyRead(-1);
      nb = _io.read( s + count, sn - count );
      if ( nb > 0 ) count += nb;
   } while ( nb > 0 && count < sn );
   
   if ( nb == -1 ) { 
      ERROR << "ByteInputQt::read() ";
      perror("");
   }
   if ( nb == 0 ) 
      ERROR << "ByteInputQt::read() Nothing left to read.\n";
   _gcount = count;
   return _ok = ( count == sn );
}

size_t
ByteInputQIO::gcount() const
{
   return _gcount;
}

ByteInputQIO::operator bool() const
{
   return _ok;
}
