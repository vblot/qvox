/**
 * @file   Dialogs.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:15 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Dialogs.h"

#include "globals.h"

#include <QSpinBox>
#include <QSlider>
#include <QPushButton>
#include <QValidator>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QImage>
#include <QStringList>
#include <QLabel>
#include <QLayout>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QImageWriter>
#include <QDateTime>
#include <QCheckBox>

QString FileFilters[] = { 
  /* VolumeFile */
#if defined(_IS_UNIX_) && defined(_GZIP_AVAILABLE_)
  QString( "All types (*.3dz *.pan *.vff *.raw *.vol *.npz *.csv *.3dz.gz *.pan.gz *.vff.gz *.raw.gz *.vol.gz *.csv.gz);;"
	   "Pandore files (*.pan *.pan.gz);;"
	   "VFF files (*.vff *.vff.gz);;"
	   "3DZ files (*.3dz *.3dz.gz);;"
	   "RAW files (*.raw *.raw.gz);;"
	   "NPZ files (*.npz);;"
	   "VOL files (*.vol *.vol.gz);;"
	   "CSV files (*.csv *.csv.gz);;"
	   "Compressed volume (*.3dz.gz *.pan.gz *.vff.gz *.raw.gz *.vol.gz *.npz)"
	   ),
#else
  QString( "All types (*.3dz *.pan *.vff *.raw *.vol *.npz);;"
	   "Pandore files (*.pan);;"
	   "VFF files (*.vff);;"
	   "3DZ files (*.3dz);;"
	   "RAW files (*.raw);;"
	   "VOL files (*.vol);;"
	   "CSV files (*.csv)" ),
#endif

  /* ViewParameters File */
  QString( "QVox 3D View parameters (*.qvp)" ),

  /* ViewExportFile */
  QString( "Encapsulated Postscript (*.eps);;" 
	   "XFig figure file (*.fig);;"
	   "SVG Scalable Vector Graphic (*.svg);;"
	   "Smoothed surface mesh as EPS file (*.eps);;"
	   "Smoothed surface mesh as EPS / Gouraud (*.eps)"
	   ),

  /* SurfaceExportFile */
  QString( "Geomview Mesh (*.off);;" 
	   "Wavefront Mesh (*.obj);;" 
	   "Surfels as Geomview Mesh (*.off);;" 
	   "POV-Ray file (*.pov)"),

  /* RawFile */
  QString( "Raw files (*.raw);;"
	   "All files (*)"),
  /* RawFile */
  QString( "QImage sequence (*.qis);;"
	   "All files (*)")
};

QString Dialogs::_path = ".";

Dialogs::Dialogs( QWidget * parent )
{
  CONS( Dialogs );
  _parent = parent;
  QList<QByteArray> formats = QImageWriter::supportedImageFormats();
  QList<QByteArray>::const_iterator it = formats.begin(), end = formats.end();
  while ( it != end  ) {
    QString ext = QString("(*.") + QString( *it ).toLower() + ")";
    FileFilters[ ViewExportFile ] += QString(";;") + QString( *it ) + " bitmap file " + ext;
    ++it;
  }
}

Dialogs::~Dialogs()
{
}

void
Dialogs::errorOpening( const char * fileName,
		       const QString & details )
{
  QMessageBox::critical( _parent, 
			 "Error", 
			 QString("Error: File %1 cannot be read.\n").arg( fileName ) + details );
}

QString
Dialogs::askSaveFileName( FileFilter filter,
                          QWidget * parent ) {
  QString fileName;
  if ( ! parent ) parent = _parent;
  _selectedFilter.clear();


  

  fileName = QFileDialog::getSaveFileName(parent, "Save as...", _path, FileFilters[filter], & _selectedFilter );
  if ( fileName.isEmpty() )
    return QString::null;

  _path = QFileInfo( fileName ).path();
    
  if ( ! QFileInfo( fileName ).suffix().isEmpty() )
    return fileName;

  // Choose a default extension if required (when the filename does not have one).
  char extensions[1024];
  char *pc = extensions;
  bool hasExtension = false;
  QString firstExt(".");
  strcpy( extensions, _selectedFilter.toLatin1().constData() );
  while ( ( pc = strstr( pc, "*." ) ) ) {
    pc++;
    char ext[255];
    int i;
    for ( i = 0; i < 255 && pc[i] && ( pc[i] == '.' || QChar( pc[i] ).isLetterOrNumber() ); i++ )
      ext[i] = pc[i];
    ext[i] = '\0';
    if ( firstExt == QString(".")  )
      firstExt = ext;
    if ( strstr( fileName.toLatin1().constData(), ext ) )
      hasExtension = true;
    pc++;
  }
  
  if ( ! hasExtension && ! _selectedFilter.contains("*.*") )
    fileName += firstExt;
  return fileName;
}

QString
Dialogs::askOpenFileName( FileFilter filter, QWidget * parent ) {
  if ( ! parent )
    parent = _parent;
  QString fileName = QFileDialog::getOpenFileName(parent,"Open file...",_path,FileFilters[filter],&_selectedFilter);
  if ( fileName.isEmpty() )
    return QString::null;
  _path = QFileInfo( fileName ).path();
  return fileName;
}

QMessageBox::StandardButton
Dialogs::confirmDoNotSave( const QString & fileName, QWidget *parent )
{
  QString filename = "";
  if ( !fileName.isEmpty() )
    filename = QString( "[%1] " ).arg( QFileInfo( fileName ).fileName() ); 

  return QMessageBox::information( parent,
				   "File not saved",
				   QString( "Current volume %1has not been saved.\n"
					    "Do you want to save it before continuing?" ).arg( filename ),
				   QMessageBox::Save | QMessageBox::Ignore | QMessageBox::Cancel,
				   QMessageBox::Cancel );
}

/*
 *  Integer dialog
 */

IntegerDialog::IntegerDialog( QWidget *parent )
  :QDialog( parent ), _validator( this )
{
  QVBoxLayout * topLayout = new QVBoxLayout;
  delete layout();
  setLayout( topLayout );
  
  _label = new QLabel( "Enter an integer value", this );
  topLayout->addWidget( _label );

  QHBoxLayout * hbox = new QHBoxLayout;
  topLayout->addLayout( hbox = new QHBoxLayout );

  hbox->addWidget( _minLabel = new QLabel( "0", this ) );
  _minLabel->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::MinimumExpanding );
  hbox->addWidget( _slider = new QSlider( Qt::Horizontal, this ) );
  _slider->setMinimumWidth( 150 );
  
  hbox->addWidget( _maxLabel = new QLabel( "100", this ) );
  _maxLabel->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::MinimumExpanding );

  QWidget *w;
  hbox->addWidget( w = new QWidget(this) );
  w->setMinimumWidth( 10 );
  
  hbox->addWidget( _spinBox = new QSpinBox( this ) );
  
  topLayout->addLayout( hbox = new QHBoxLayout );  

  hbox->addWidget( _pbCancel = new QPushButton( "&Cancel", this ) );
  _pbCancel->setSizePolicy( QSizePolicy::Preferred,  QSizePolicy::Preferred );
  hbox->addWidget( _pbOk = new QPushButton( "&OK", this ) );
  _pbOk->setSizePolicy( QSizePolicy::Preferred,  QSizePolicy::Preferred );
  connect( _pbOk, SIGNAL( clicked() ),
	   this, SLOT( accept() ) );
  connect( _pbCancel, SIGNAL( clicked() ),
	   this, SLOT( reject() ) );
  connect( _slider, SIGNAL( valueChanged(int) ),
	   _spinBox, SLOT( setValue(int) ) );
  connect( _spinBox, SIGNAL( valueChanged( int ) ),
	   _slider, SLOT( setValue( int ) ) );
}

int
IntegerDialog::ask( QWidget * parent, int min, int max, bool & ok, int value )
{
  IntegerDialog * dialog = new IntegerDialog( parent );
  dialog->range( min, max, value );
  ok = ( dialog->exec() == QDialog::Accepted );
  if ( ok ) 
    return dialog->value();
  else 
    return 0;
}

void
IntegerDialog::range( int min, int max, int value )
{
  _slider->setRange( min, max );
  _spinBox->setRange( min, max );
  _minLabel->setText( QString("%1").arg( min ) );
  _maxLabel->setText( QString("%1").arg( max ) );
  if ( value == -1 ) 
    _spinBox->setValue( min  );
  else 
    _spinBox->setValue( value );
}


OptionalDialog::OptionalDialog( QWidget * parent,
				const QString & caption,
				const QString & text,
				Settings::OptionalMessage option )
  : QDialog( parent ), _optionalMessage( option )
{
  setModal( true );
  // setAttribute( Qt::WA_DeleteOnClose, true );

  setWindowTitle( caption );
  QVBoxLayout * vbox = new QVBoxLayout;
  setLayout( vbox );
  _textLabel = new QLabel( text );
  vbox->addWidget( _textLabel );
  
  _checkBox = new QCheckBox( "Always display this message.", this);
  _checkBox->setChecked( true );
  vbox->addWidget( _checkBox );

  QHBoxLayout * hbox = new QHBoxLayout;
  vbox->addLayout( hbox );
  hbox->setAlignment(  Qt::AlignHCenter );
  hbox->addWidget( _pbOk = new QPushButton( "Ok", this ) );
  hbox->addWidget( _pbCancel = new QPushButton( "Cancel", this ) );

  connect( _checkBox, SIGNAL( toggled(bool ) ),
	   this, SLOT( checkBoxToggled(bool ) ) );
  connect( _pbOk, SIGNAL( clicked(bool ) ),
	   this, SLOT( accept() ) );
  connect( _pbCancel, SIGNAL( clicked(bool ) ),
	   this, SLOT( reject() ) );
}

OptionalDialog::~OptionalDialog()
{
  
}

int
OptionalDialog::message( QWidget * parent, 
			 const QString & caption, 
			 const QString & text, 
			 Settings::OptionalMessage option )
{
  OptionalDialog * dialog = new OptionalDialog( parent, 
						caption,
						text,
						option );
  if ( globalSettings->hasOptionalMessage( option ) ) {
    int ans = dialog->exec();
    delete dialog;
    return ans;
  }
  return 0;
}

void
OptionalDialog::checkBoxToggled( bool on )
{
  if ( on )
    globalSettings->addOptionalMessage( _optionalMessage );
  else
    globalSettings->removeOptionalMessage( _optionalMessage );
}
