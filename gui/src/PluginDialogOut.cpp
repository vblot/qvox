/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   PluginDialogOut.h
 * @author Sebastien Fourey (GREYC)
 * @date   Oct 2008
 * 
 * @brief  Declaration of the class PluginDialogOut
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "PluginDialogOut.h"

#include <QGridLayout>
#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QPushButton>
#include <QMessageBox>
#include <QStatusBar>
#include <QCheckBox>
#include <QApplication>
#include <QStringList>

#include "Document.h"
#include "ByteInputQIO.h"
#include "PluginConsole.h"

/**
 * Constructor
 */
PluginDialogOut::PluginDialogOut( QWidget * parent,
				  const QDomElement & xmlPlugin,
				  Document & document,
				  const QString & xmlPath,
				  const QString & binPath,
				  const QString & libPath,
				  const QString & workingDir )
   : PluginDialog( parent, xmlPlugin, document, xmlPath, binPath, libPath, workingDir ) {

   _process = 0;
   _topLabel->setText( QString("<h3> %2 &gt;&gt;</h3>").arg( _name ) );
}

PluginDialog::Type
PluginDialogOut::type() const
{
   return Output;
}

QProcess::ProcessState
PluginDialogOut::state() const
{
   if ( !_process ) return QProcess::NotRunning;
   return _process->state();
}

void
PluginDialogOut::launch()
{  
   if ( _document->activePlugin() ) {
      QMessageBox::StandardButton ans;
      ans = QMessageBox::question( QApplication::activeWindow(),
				   QString("Error"),
				   QString("Cannot execute an external command"
					   " because a data source process is"
					   " already running.\n\nDo you want"
					   " to kill the running process?"),
				   QMessageBox::Yes|QMessageBox::Default,
				   QMessageBox::No );
      if ( ans == QMessageBox::Yes ) {
	 _document->activePlugin()->terminate();
      } else return;
   }

   _pbOk->setEnabled( false );
   _pbApply->setEnabled( false );
   _command = command();
   _outputFilename.clear();
  
   if ( _command.contains( " $o" ) ) {
      QString extension = outputExtension();
      QTemporaryFile tmpFile;
      tmpFile.open();
      _outputFilename = tmpFile.fileName() + "." + extension;
      _command.replace( " $o", QChar(' ') + _outputFilename );
      TRACE << "PLUGIN: Execute: [" 
	    << _command.toLatin1().constData()
	    << "]\n";      
      _process = new QProcess( this );
      setProcessEnv( _process );
      connect( _process, SIGNAL( finished( int, QProcess::ExitStatus ) ),
	       this, SLOT( processFinished( int, QProcess::ExitStatus ) ) );
      connect( _process, SIGNAL( error( QProcess::ProcessError ) ),
	       this, SLOT( processError( QProcess::ProcessError) ) );
      if ( _cbShowConsole->isChecked() ) {
	 PluginConsole * console 
	    = new PluginConsole( dynamic_cast<QWidget*>( parent() ) );
	 console->command( _command );
	 console->process( _process, true, true );
	 console->show();
      }
      if ( _workingDir.size() ) _process->setWorkingDirectory( _workingDir );
      _process->start( "sh", QStringList() << "-c" <<  _command,
		       QIODevice::ReadOnly );
   } else {
      TRACE << "PLUGIN: Execute: [" 
	    << _command.toLatin1().constData() 
	    << "]\n";      
      _process = new QProcess( this );
      setProcessEnv( _process );
      connect( _process, SIGNAL( finished( int, QProcess::ExitStatus ) ),
	       this, SLOT( processFinished( int, QProcess::ExitStatus ) ) );
      connect( _process, SIGNAL( error( QProcess::ProcessError ) ),
	       this, SLOT( processError( QProcess::ProcessError) ) );
      if ( _cbShowConsole->isChecked() ) {
	 PluginConsole * console 
	    = new PluginConsole( dynamic_cast<QWidget*>( parent() ) );
	 console->command( _command );
	 console->process( _process, false, true );
	 console->show();
      }
      if ( _workingDir.size() ) _process->setWorkingDirectory( _workingDir );
      _process->start( "sh", QStringList() << "-c" <<  _command,
		       QIODevice::ReadOnly );
   }

   if ( paramsCount() )
      _statusBar->showMessage( "DataSource command launched.", 4000 );
   _document->message( "DataSource command launched.", 4000 );

   _pbOk->setEnabled( false );
   _pbApply->setEnabled( false );
   _document->activePlugin( this );
}

void
PluginDialogOut::processFinished( int exitCode,
				  QProcess::ExitStatus exitStatus )
{
   TSHOW( exitCode );
   TSHOW( exitStatus );

   _document->activePlugin( 0 );

   if ( exitStatus == QProcess::CrashExit ) 
      _statusBar->showMessage( QString("Command crashed."),
			       4000 );
   else if ( exitCode ) 
      _statusBar->showMessage( QString("Command failed (%1).").arg(exitCode),
			       4000 );
   else
      _statusBar->showMessage("Command done.", 3000 );

   if ( _outputFilename.isEmpty() ) { 
      // Piped output
      if ( exitStatus == QProcess::NormalExit ) {
	 _process->setReadChannel( QProcess::StandardOutput );	 
	 ByteInputQIO in( *_process );
	 _document->load( in );
	 _document->modified(true);
      }
   } else {
      // Filed output
      TRACE << "Loading : " 
	    << _outputFilename.toLatin1().constData()
	    << std::endl;      
      QFile f( _outputFilename );
      if ( f.exists() ) {
	 if ( exitStatus == QProcess::NormalExit ) {
	    _document->load( _outputFilename.toLatin1().constData(), false );
	    _document->modified(true);
	 }
	 f.remove();
      } else {
	 ERROR << "External command: No file was produced.\n";
	 ERROR << "   command> "
	       <<  _command.toLatin1().constData() 
	       << std::endl;
      }
      _outputFilename.clear();
   }
   if ( _process ) {
      delete _process;
      _process = 0;
   }
   _pbOk->setEnabled( true );
   _pbApply->setEnabled( true );
   TRACE << "PLUGIN Finished.\n"; 
}

void
PluginDialogOut::processError( QProcess::ProcessError processError )
{
   TSHOW( processError );
   _document->activePlugin( 0 );
   ERROR << "Could not execute external command.\n";
   ERROR << "   command> " << _command.toLatin1().constData() << std::endl;

   if ( processError == QProcess::Crashed ) {
      QMessageBox::critical( this, 
			     QString( "Error" ), 
			     QString( "The process crashed.\n"
				      "The command used was:\n" ) + _command
			     );
   } else if ( processError == QProcess::FailedToStart ) {
      QMessageBox::critical( this, 
			     QString( "Error" ), 
			     QString( "The command failed to start.\n"
				      "The command used was:\n" ) + _command
			     );
   } else {
      QMessageBox::critical( this, 
			     QString( "Error" ), 
			     QString( "There was an error with the command.\n"
				      "The command used was:\n" ) + _command
			     );
   }

   if ( !_outputFilename.isEmpty() ) {
      TRACE << "PLUGIN Removing: " 
	    << _outputFilename.toLatin1().constData() 
	    << std::endl;      
      QFile f( _outputFilename );
      if ( f.exists() ) {
	 f.remove();
      }
      _outputFilename.clear();
   }

   if ( _process  ) {
      if ( _process->state() )
	 _process->kill();
   }
   _pbOk->setEnabled( true );
   _pbApply->setEnabled( true );
   _document->activePlugin( 0 );
   ERROR << " External plugin failed.\n"; 
}

void
PluginDialogOut::terminate()
{
   if ( _process && _process->state() ) {
      _process->kill();
   }
}

int
PluginDialogOut::execute()
{
   if ( paramsCount() )
      show();
   else
      launch();
   return 1;
}

void
PluginDialogOut::okClicked()
{
   launch();
   done( 0 );
}

void
PluginDialogOut::applyClicked()
{
   launch();
}

void
PluginDialogOut::closeClicked()
{
   done( 0 );
}
