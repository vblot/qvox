/**
 * @file   Settings.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:33 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Settings.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
using std::vector;
using std::set;

#include <QFileInfo>
#include <QSettings>

Settings *globalSettings;

#include "Surface.h"

Settings::Settings()
{
  CONS( Settings );
  _fullVolumeBoxAction = new QAction("Full volume box", this );
  _fullVolumeBoxAction->setCheckable( true );
  load();
}

Settings::~Settings()
{
  DEST( Settings );
  if ( _saveOnExit ) save();

  QSettings settings;

  settings.beginGroup( "/QVox" );

  for ( int n = 0; n < 10 ; n++ ) {
    QString key("/RecentFiles/File%1");
    settings.setValue( key.arg( n ), QString() ) ;
  }

  vector<QString>::iterator it = _recentFiles.begin();
  int number = 0;
  while ( it != _recentFiles.end() ) {
    QString key("/RecentFiles/File%1");
    settings.setValue( key.arg( number ), *it );
    number++;
    it++;
  }
  settings.endGroup();
}

void
Settings::drawRadius( int n ) {
  if ( n < 65536 && n != _drawRadius ) {
    _drawRadius = n;
    emit notify();
  }
}

void
Settings::maxThreads( int n ) {
  _maxThreads = n;
}

void
Settings::normalsEstimateIterations( int n ) {
  if ( n < 65536 && n != _normalsEstimateIterations ) {
    _normalsEstimateIterations = n;
    emit notify();
  }
}

void
Settings::normalsEstimateAdaptive( bool b ) {
  if ( b != _normalsEstimateAdaptive ) {
    _normalsEstimateAdaptive = b;
    emit notify();
  }
}

void
Settings::curvatureEstimateIterations( int n ) {
  if ( n < 65536 && n != _curvatureEstimateIterations ) {
    _curvatureEstimateIterations = n;
    emit notify();
  }
}

void
Settings::curvatureEstimateAveragingIterations( int n ) {
  if ( n < 65536 && n != _curvatureEstimateAveragingIterations ) {
    _curvatureEstimateAveragingIterations = n;
    emit notify();
  }
}

void
Settings::curvatureEstimateAdaptive( bool b ) {
  if ( b != _curvatureEstimateAdaptive ) {
    _curvatureEstimateAdaptive = b;
    emit notify();
  }
}

void
Settings::curvatureType( int i ) {
  if ( static_cast< QVoxSettings::CurvatureType >( i ) != _curvatureType ) {
    _curvatureType = static_cast< QVoxSettings::CurvatureType >( i );
    emit notify();
  }
}

void
Settings::exportOFFIterations( int n ) {
  if ( n < 65536 && n != _exportOFFIterations ) {
    _exportOFFIterations = n;
    emit notify();
  }
}

void
Settings::exportOFFsurfelsColors( bool b ) {
  if ( b != _exportOFFsurfelsColors ) {
    _exportOFFsurfelsColors = b;
    emit notify();
  }
}

void
Settings::exportOFFColor( const QColor & c ) {
  if ( c != _exportOFFColor ) {
    _exportOFFColor = c;
    emit notify();
  }
}

void
Settings::exportOFFAdaptive( bool b )
{
  if ( b != _exportOFFAdaptive ) {
    _exportOFFAdaptive = b;
    emit notify();
  }
}


void
Settings::fitBoundingBoxFrameSize( int n ) {
  if ( n < 65536 && n != _fitBoundingBoxFrameSize ) {
    _fitBoundingBoxFrameSize = n;
    emit notify();
  }
}

void
Settings::levelMapMaxHeight( int n ) {
  if ( n < 65536 && n != _levelMapMaxHeight ) {
    _levelMapMaxHeight = n;
    emit notify();
  }
}

void
Settings::dwtFinalSize( int n ) {
  if ( n < 65536 && n != _dwtFinalSize ) {
    _dwtFinalSize = n;
    emit notify();
  }
}

void
Settings::adjacencyPair( int pair )
{
  if ( pair != _adjacencyPair ) {
    _adjacencyPair = pair;
    emit adjacencyPairChanged( _adjacencyPair );
  }
}


void
Settings::transparency( bool on )
{
  if ( on != _transparency ) {
    _transparency = on;
    emit notify();
  }
}

void
Settings::opacity( int alpha )
{
  if ( alpha != _opacity ) {
    _opacity = alpha;
    emit opacityChanged( alpha );
    emit notify();
  }
}

void
Settings::showFPS( bool on ) {
  if ( on != _showFPS ) {
    _showFPS = on;
    emit notify();
  }
}

void
Settings::drawAxis( bool on ) {
  if ( on != _drawAxis ) {
    _drawAxis = on;
    emit notify();
  }
}

void
Settings::drawColorMap( bool on ) {
  if ( on != _drawColorMap ) {
    _drawColorMap = on;
    emit notify();
  }
}

void
Settings::shading( int i ) {
  if ( i != _shading ) {
    if ( i != -1 ) _shading = i;
    emit notify();
    emit shadingChanged(_shading);
  }
}

void
Settings::colormap( int c ) {
  if ( c != _colormap ) {
    _colormap = c;
    emit notify();
  }
}

void
Settings::bgColor( const QColor & color ) {
  if ( color != _bgColor ) {
    _bgColor = color;
    emit notify();
  }
}

void
Settings::saveOnExit( bool on ) {
  if ( on != _saveOnExit ) {
    _saveOnExit = on;
    if ( !on ) {
      QSettings settings;
      settings.beginGroup( "/QVox" );
      settings.setValue( "/Basic/SaveSettingsOnExit", false  );
      settings.endGroup();
    }
    emit notify();
  }
}

void
Settings::load()
{
  QSettings settings;
  settings.beginGroup( "/QVox" );

  _saveOnExit = settings.value( "/Basic/SaveSettingsOnExit", true  ).toBool();

  _opacity = settings.value( "/Basic/Opacity", 255  ).toInt();
  _transparency = settings.value( "/Basic/Transparency", false  ).toBool();
  _showFPS = settings.value( "/Basic/ShowFPS", false  ).toBool();
  _drawAxis = settings.value( "/Basic/DrawAxis", false  ).toBool();
  _drawColorMap = settings.value( "/Basic/DrawColorMap", false  ).toBool();

  _shading = settings.value( "/Basic/Shading", Surface::ColorFromLight  ).toInt();

  _drawRadius = settings.value( "/Basic/DrawRadius", 10  ).toInt();

  _consoleFontSize = settings.value( "/Basic/ConsoleFontSize", 8  ).toInt();

  _multiThreading = settings.value( "/Basic/MultiThreading", true  ).toBool();
  _maxThreads = settings.value( "/Basic/MaxThreads", -1  ).toInt();
  if ( _maxThreads == 0 ) { _maxThreads = -1; }
  _rotationBoxMaxFrames = settings.value( "/Basic/RotationBoxMaxFrames", 8 ).toInt();

  _normalsEstimateIterations =
    settings.value( "/Basic/NormalsEstimateIterations", 5  ).toInt();
  _normalsEstimateAdaptive = settings.value( "/Basic/NormalsEstimateAdaptive", false ).toBool();
  _curvatureEstimateIterations =
    settings.value( "/Basic/CurvatureEstimateIterations", 5 ).toInt();
  _curvatureEstimateAveragingIterations =
    settings.value( "/Basic/CurvatureEstimateAveragingIterations", 5 ).toInt();
  _curvatureEstimateAdaptive = settings.value( "/Basic/CurvatureEstimateAdaptive", false ).toBool();

  int i = settings.value( "/Basic/CurvatureType", QVoxSettings::Mean ).toInt();
  if ( i == static_cast<int>( QVoxSettings::Mean ) )
    _curvatureType = QVoxSettings::Mean;
  if ( i == static_cast<int>( QVoxSettings::Gaussian ) )
    _curvatureType = QVoxSettings::Gaussian;
  if ( i == static_cast<int>( QVoxSettings::Maximum ) )
    _curvatureType = QVoxSettings::Maximum;

  _exportOFFIterations = settings.value( "/Basic/ExportOFFIterations", 5 ).toInt();
  _exportOFFsurfelsColors = settings.value( "/Basic/ExportOFFsurfelsColors", true ).toBool();
  _exportOFFAdaptive = settings.value( "/Basic/ExportOFFAdaptive", false ).toBool();

  int red = settings.value( "/Basic/ExportOFFcolorRed", 128 ).toInt();
  int green = settings.value( "/Basic/ExportOFFcolorGreen", 128 ).toInt();
  int blue = settings.value( "/Basic/ExportOFFcolorBlue", 128 ).toInt();
  _exportOFFColor.setRgb( red, green, blue );

  _fitBoundingBoxFrameSize = settings.value( "/Basic/FitBoundingBoxFrameSize", 1 ).toInt();
  _levelMapMaxHeight = settings.value( "/Basic/LevelMapMaxHeight", 255  ).toInt();
  _dwtFinalSize = settings.value( "/Basic/DWTFinalSize", 32  ).toInt();
  _adjacencyPair = settings.value( "/Basic/AdjacencyPair", 0 ).toInt();
  _colormap = settings.value( "/Basic/ColorMap", 0 ).toInt();

  _computeVertexIndices = settings.value("/Misc/ComputeVertexIndices",false).toBool();

  _pluginsFiles = settings.value( "/Basic/PluginsFiles" ).toStringList();

  QStringList stringList;
  stringList.push_back( "Tools" );
  stringList.push_back( "Surface view parameters" );

  _activeToolBarsList = settings.value( "/GUI/ActiveToolBars" ).toStringList();
  if ( _activeToolBarsList.isEmpty() )
    _activeToolBarsList = stringList;


  red = settings.value("/Basic/BackgroundRed", 255 ).toInt();
  green = settings.value("/Basic/BackgroundGreen", 255 ).toInt();
  blue = settings.value("/Basic/BackgroundBlue", 255 ).toInt();
  _bgColor.setRgb( red, green, blue );

  for ( int n = 0; n < 10 ; n++ ) {
    QString key("/RecentFiles/File%1");
    QString fileName = settings.value( key.arg( n ), QString() ).toString() ;
    if ( ! fileName.isEmpty()  )
      _recentFiles.push_back( fileName );
  }

  QList<QVariant> defOptionalMessages;
  OptionalMessage om[] = { FullBox };
  for ( int i = 0; i < 1; ++i )
    defOptionalMessages.append( om[i] );

  QList<QVariant> optionalMessages;
  optionalMessages = settings.value( "/Basic/OptionalMessages", defOptionalMessages ).toList();
  QList<QVariant>::iterator it = optionalMessages.begin();
  QList<QVariant>::iterator e = optionalMessages.end();
  while ( it != e ) {
    _optionalMessages.insert( static_cast<OptionalMessage>( it->toInt() ) );
    ++it;
  }

  _fineDrawingMode = settings.value( "/Basic/FineDrawingMode", false ).toBool();

  settings.endGroup();
}

void
Settings::save()
{
  QSettings settings;
  settings.beginGroup( "/QVox" );
  settings.setValue( "/Basic/SaveSettingsOnExit", _saveOnExit  );

  settings.setValue( "/Basic/Opacity", _opacity );
  settings.setValue( "/Basic/Transparency", _transparency  );
  settings.setValue( "/Basic/ShowFPS", _showFPS  );
  settings.setValue( "/Basic/DrawAxis", _drawAxis  );
  settings.setValue( "/Basic/DrawColorMap", _drawColorMap  );

  settings.setValue( "/Basic/Shading", _shading  );

  settings.setValue( "/Basic/DrawRadius", _drawRadius  );

  settings.setValue( "/Basic/ConsoleFontSize", _consoleFontSize  );

  settings.setValue( "/Basic/FineDrawingMode", _fineDrawingMode );
  settings.setValue( "/Basic/MultiThreading", _multiThreading );
  settings.setValue( "/Basic/MaxThreads", _maxThreads );
  settings.setValue( "/Basic/RotationBoxMaxFrames", _rotationBoxMaxFrames );
  settings.setValue( "/Basic/NormalsEstimateIterations",
           _normalsEstimateIterations );
  settings.setValue( "/Basic/NormalsEstimateAdaptive",
           _normalsEstimateAdaptive );
  settings.setValue( "/Basic/CurvatureEstimateIterations",
           _curvatureEstimateIterations );
  settings.setValue( "/Basic/CurvatureEstimateAveragingIterations",
           _curvatureEstimateAveragingIterations );
  settings.setValue( "/Basic/CurvatureEstimateAdaptive",
          _curvatureEstimateAdaptive );

  settings.setValue( "/Basic/CurvatureType", static_cast<int>( _curvatureType  ) );

  settings.setValue( "/Basic/ExportOFFIterations", _exportOFFIterations );
  settings.setValue( "/Basic/ExportOFFsurfelsColors", _exportOFFsurfelsColors );

  settings.setValue( "/Basic/ExportOFFcolorRed", _exportOFFColor.red() );
  settings.setValue( "/Basic/ExportOFFcolorGreen", _exportOFFColor.green() );
  settings.setValue( "/Basic/ExportOFFcolorBlue", _exportOFFColor.blue() );

  settings.setValue( "/Basic/ExportOFFAdaptive", _exportOFFAdaptive );

  settings.setValue( "/Basic/FitBoundingBoxFrameSize", _fitBoundingBoxFrameSize );
  settings.setValue( "/Basic/LevelMapMaxHeight", _levelMapMaxHeight );
  settings.setValue( "/Basic/DWTFinalSize", _dwtFinalSize );

  settings.setValue( "/Basic/AdjacencyPair", _adjacencyPair );
  settings.setValue( "/Basic/ColorMap", _colormap );

  settings.setValue( "/Basic/BackgroundRed", _bgColor.red() );
  settings.setValue( "/Basic/BackgroundGreen", _bgColor.green() );
  settings.setValue( "/Basic/BackgroundBlue", _bgColor.blue() );

  settings.setValue( "/Basic/PluginsFiles", _pluginsFiles );

  settings.setValue( "/GUI/ActiveToolBars", _activeToolBarsList );

  settings.setValue("/Misc/ComputeVertexIndices",_computeVertexIndices);

  QList<QVariant> optionalMessages;
  set<OptionalMessage>::iterator it = _optionalMessages.begin();
  set<OptionalMessage>::iterator end = _optionalMessages.end();
  while ( it != end ) {
    optionalMessages.append( static_cast<int>( *it ) );
    ++it;
  }
  settings.setValue( "/Basic/OptionalMessages", QVariant( optionalMessages ) );

  settings.endGroup();
}

void
Settings::addRecentFile( const QString & fileName )
{
  QString fullFileName = QFileInfo( fileName ).absoluteFilePath();
  vector< QString >::iterator it = std::find( _recentFiles.begin(), _recentFiles.end(), fullFileName );
  if ( it != _recentFiles.end() ) {
    _recentFiles.erase( it );
    _recentFiles.insert( _recentFiles.begin(), fullFileName );
    return;
  }
  _recentFiles.insert( _recentFiles.begin(), fullFileName );
  if ( _recentFiles.size() > 10 )
    _recentFiles.pop_back();
}

void
Settings::addOptionalMessage( OptionalMessage m )
{
  _optionalMessages.insert( m );
  emit notify();
}

void
Settings::removeOptionalMessage( OptionalMessage m )
{
  _optionalMessages.erase( m );
  emit notify();
}

bool
Settings::hasOptionalMessage( OptionalMessage m )
{
  return _optionalMessages.count( m );
}

QStringList &
Settings::pluginsFiles()
{
  return _pluginsFiles;
}


void
Settings::consoleFontSize( int n )
{
  _consoleFontSize = n;
}

void
Settings::fineDrawingMode( bool on )
{
  _fineDrawingMode = on;
}

void
Settings::multiThreading( bool on )
{
  if ( on != _multiThreading ) {
    _multiThreading = on;
    emit notify();
  }
}

void
Settings::rotationBoxMaxFrames(int n)
{
  _rotationBoxMaxFrames = n;
}

void
Settings::computeVertexIndices(bool on)
{
  _computeVertexIndices = on;
}


