/**
 * @file   HistogramWidget.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:38 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "HistogramWidget.h"

#include <algorithm>
#include <QLayout>

#include "Document.h"
#include "HistogramView.h"

using namespace std;

HistogramWidget::HistogramWidget( QWidget * parent )
  :QWidget( parent ), _document( 0 ),_band( 0 )

{
  CONS( HistogramWidget );
  delete layout();
  QVBoxLayout *layout = new QVBoxLayout;
  setLayout( layout );

  layout->addWidget( _histogramView = new HistogramView( _histogram, this ) );

  QHBoxLayout *hbox;
  layout->addLayout( hbox = new QHBoxLayout );
  hbox->addWidget( _labelMin = new QLabel( "Min", this ) );
  _labelMin->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  _labelMin->setAlignment( Qt::AlignLeft );
  hbox->addWidget( _labelMax = new QLabel( "Max", this ) );
  _labelMax->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
  _labelMax->setAlignment( Qt::AlignRight );

  setSizePolicy( QSizePolicy::Expanding, QSizePolicy::MinimumExpanding );
  
  connect( _histogramView, SIGNAL( minChanged(int) ),
           this, SLOT( viewMinChanged(int) ) );
  connect( _histogramView, SIGNAL( maxChanged(int) ),
           this, SLOT( viewMaxChanged(int) ) );
}

HistogramWidget::~HistogramWidget()
{
}

void
HistogramWidget::viewMinChanged(int bin)
{
  double range = _labelMax->text().toDouble() - _labelMin->text().toDouble();
  double val = bin * ( range / _histogram.size() );
  emit minChanged( QString("%1").arg(val,0,'f',0) );
}

void
HistogramWidget::viewMaxChanged(int bin)
{
  double range = _labelMax->text().toDouble() - _labelMin->text().toDouble();
  double val = bin * ( range / _histogram.size() );
  emit maxChanged( QString("%1").arg(val,0,'f',0) );
}

void
HistogramWidget::setMin(const QString& min)
{
  double range = _labelMax->text().toDouble() - _labelMin->text().toDouble();
  _histogramView->setMin( static_cast<int>( min.toDouble() / ( range / _histogram.size() ) ) );
}

void
HistogramWidget::setMax(const QString& max)
{
  double range = _labelMax->text().toDouble() - _labelMin->text().toDouble();
  _histogramView->setMax( static_cast<int>( max.toDouble() / ( range / _histogram.size() ) ) );
}


void
HistogramWidget::document( Document * document, int band )
{
  _band = band;
  if ( _document ) disconnect( _document, 0, this, 0 );
  _document = document;
  if ( _document )
    connect( _document, SIGNAL( volumeChanged() ),
             this, SLOT( update() ) );

  switch ( band ) {
  case 0:
    if ( document->bands() == 1 )
      _histogramView->color(QColor(324,32,32));
    else
      _histogramView->color(QColor(255,0,0));
    break;
  case 1:
    _histogramView->color(QColor(0,255,0));
    break;
  case 2:
    _histogramView->color(QColor(0,0,255));
    break;
  }

  update();
}

void
HistogramWidget::showEvent( QShowEvent * ) {
  if ( _document ) {
    connect( _document, SIGNAL( volumeChanged() ),
             this, SLOT( update() ) );
  }
  update();
}

void
HistogramWidget::hideEvent( QHideEvent * ) {
  if ( _document )
    disconnect( _document, 0, this, 0 );
}

void
HistogramWidget::update()
{
  UChar ucMin,ucMax;
  Int32 slMin,slMax;
  Short ssMin, ssMax;
  UShort usMin, usMax;
  UInt32 ulMin,ulMax;
  Float sfMin,sfMax;

  if ( !_document ) return;

  _histogram.clear();
  _histogram.resize(256);

  setDisabled( _band >= _document->bands() );

  if ( ( _band >= _document->bands() ) || !(_document->bands()) || !(_document->valid()) ) {
    return;
  }

  if ( _band == 0 ) {
    if ( _document->bands() == 1 )
      _histogramView->color(QColor(32,32,32));
    else
      _histogramView->color(QColor(255,0,0));
  }

  switch ( _document->valueType() ) {
  case Po_ValUC:
    (dynamic_cast<ZZZImage<UChar>*>(_document->image()))->histogram( _histogram, ucMin, ucMax, _band );
    _labelMin->setText( QString("%1").arg( ucMin ) );
    _labelMax->setText( QString("%1").arg( ucMax ) );
    break;
  case Po_ValSL:
    (dynamic_cast<ZZZImage<Int32>*>(_document->image()))->histogram( _histogram, slMin, slMax, _band );
    _labelMin->setText( QString("%1").arg( slMin ) );
    _labelMax->setText( QString("%1").arg( slMax ) );
    break;
  case Po_ValUL:
    (dynamic_cast<ZZZImage<UInt32>*>(_document->image()))->histogram( _histogram, ulMin, ulMax, _band );
    _labelMin->setText( QString("%1").arg( ulMin ) );
    _labelMax->setText( QString("%1").arg( ulMax ) );
    break;
  case Po_ValSS:
    (dynamic_cast<ZZZImage<Short>*>(_document->image()))->histogram( _histogram, ssMin, ssMax, _band );
    _labelMin->setText( QString("%1").arg( ssMin ) );
    _labelMax->setText( QString("%1").arg( ssMax ) );
    break;
  case Po_ValUS:
    (dynamic_cast<ZZZImage<UShort>*>(_document->image()))->histogram( _histogram, usMin, usMax, _band );
    _labelMin->setText( QString("%1").arg( usMin ) );
    _labelMax->setText( QString("%1").arg( usMax ) );
    break;
  case Po_ValSF:
    (dynamic_cast<ZZZImage<Float>*>(_document->image()))->histogram( _histogram, sfMin, sfMax, _band );
    _labelMin->setText( QString("%1").arg( sfMin ) );
    _labelMax->setText( QString("%1").arg( sfMax ) );
    break;
  }
  _histogramView->update();
}

void
HistogramWidget::paintEvent( QPaintEvent *)
{

}

void
HistogramWidget::resizeEvent( QResizeEvent * )
{

}
