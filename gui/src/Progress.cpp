/**
 * @file   Progress.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:41:28 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "globals.h"
#include "Progress.h"
#include <QStatusBar>
#include <QApplication>
#include <QProgressBar>
#include <limits>

using namespace std;

ProgressReceiver globalProgressReceiver;

ProgressReceiver::ProgressReceiver()
{
  _progressBar = 0;
  _statusBar = 0;
}

void
ProgressReceiver::setProgressBar( QProgressBar * progressBar )
{
  _progressBar = progressBar;
  _progressBar->setRange(0,numeric_limits<int>::max());
}

void
ProgressReceiver::setStatusBar( QStatusBar * statusBar )
{
  _statusBar = statusBar;
}

void ProgressReceiver::pushInterval(int max, const char * message )
{
  if ( !_progressBar || !_statusBar ) return;
  if ( message )
    _statusBar->showMessage(message);
  bool first = _intervals.empty();
  _intervals.push_back(std::make_pair(0,max));
  if ( first ) {
    _progressBar->show();
    qApp->processEvents();
  }
}

void
ProgressReceiver::setProgress(int value)
{
  if ( !_progressBar || !_statusBar ) return;
  if ( !_intervals.size() ) return;
  if ( value > _intervals.back().second ) {
    _intervals.back().first = _intervals.back().second;
  } else {
    _intervals.back().first = value;
  }
  int currentRange = numeric_limits<int>::max();
  int currentValue = 0;
  Intervals::iterator it = _intervals.begin();
  while ( it != _intervals.end() ) {
    currentRange /= it->second;
    currentValue += it->first * currentRange;
    ++it;
  }
  _progressBar->setValue(currentValue);
  qApp->processEvents();
}

void ProgressReceiver::popInterval()
{
  if ( !_progressBar || !_statusBar ) return;
  if ( !_intervals.size() ) return;
  _intervals.back().first = _intervals.back().second;
  int currentRange = numeric_limits<int>::max();
  int currentValue = 0;
  Intervals::iterator it = _intervals.begin();
  while ( it != _intervals.end() ) {
    currentRange /= it->second;
    currentValue += it->first * currentRange;
    ++it;
  }
  _progressBar->setValue(currentValue);
  _intervals.pop_back();
  if ( ! _intervals.size() ) {
    _progressBar->hide();
    _progressBar->setValue(0);
    _statusBar->clearMessage();
  }
  qApp->processEvents();
}

void
ProgressReceiver::showMessage(const char * message)
{
  if ( !_statusBar ) return;
  if ( message )
    _statusBar->showMessage(message);
  else
    _statusBar->clearMessage();
}

