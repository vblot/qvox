/**
 * @file   longvol.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2008
 * 
 * @brief  Longvol interface class (wrapper of a ZZZImage<Long>).
 * 
 * Copyright 2008 Sebastien Fourey <Firstname.Lastname@greyc.ensicaen.fr>
 * Copyright 2002, 2003 Alexis Guillaume <aguillau@liris.univ-lyon2.fr> for
 *                       "Laboratoire LIRIS, université Lyon II, France."
 * Copyright 2002, 2003 David Coeurjolly <dcoeurjo@liris.univ-lyon2.fr> for
 *                        "Laboratoire LIRIS, université Lyon II, France."
 * 
 * This file was originally part of libvol.
 * It is a modified version intented to 
 * be used as a wrapper of the ZZZImage<Long> class
 * which present the interface of the Vol class.
 * (C) 2008 Sebastien Fourey
 * 
 */
#ifndef _LONGVOLZZZ_ADAPTER_
#define _LONGVOLZZZ_ADAPTER_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "zzzimage.h"
#include <fstream>


typedef Int32 lvoxel;	/**<  Voxel type. */

/* 
   Not implemented :
   
   * Handling of defaultAlpha in: 
     inline Vol( const char *fname, int sx, int sy, int sz, voxel defaultAlpha = 0 );

   * Handling of getHeaderValueAsInt("Axis", &axis) in the constructor from a filename.

 */

class Longvol {

 public:

  /** 
   * Default constructor.
   */
  inline Longvol();

  /** 
   * Constructor with a given volume size.
   * 
   * @param sx 
   * @param sy 
   * @param sz 
   * @param tcolor 
   */
  inline Longvol( int sx, int sy, int sz, lvoxel tcolor );

  /** 
   * Copy constructor.
   */
  Longvol( const Longvol & );

  /** 
   * Constructor with an associated ZZZImage.
   * 
   * @param volume 
   */
  inline Longvol( ZZZImage<Int32> * volume );

  /** 
   * Constructor with an associated ZZZImage.
   * 
   * @param volume Reference to a volume.
   */
  inline Longvol( ZZZImage<Int32> & volume );

  /**
  * Constructor to build a volume from a .vol file.
  * If filename is "", stdin is used.
  */
  inline Longvol( const char *fname );

  /**
   * Constructor to build a volume from a .raw file
   * *without* size informations.
   */
  inline Longvol( const char *fname, int sx, int sy, int sz, lvoxel defaultAlpha = 0 );

  /**
   * Destructor (not virtual, so don't inherit this class !)
   */
  inline ~Longvol();

  /**
   * Assignement operator.
   */
  inline const Longvol & operator=(const Longvol &);

  /**
   * Pixel access operator.
   */
  lvoxel & operator()(int x, int y, int z);

  /**
   * Pixel access operator. (const version)
   */
  lvoxel operator()(int x, int y, int z) const;

  lvoxel get( int x, int y, int z ) const { return (*this)(x, y, z); }

  void  set( int x, int y, int z, lvoxel c ) { (*this)(x, y, z) = c; }

  /**
   * Write this volume object into a .vol file.
   * If filename is "", stdout is used.
   * @return > 0 if error.
   */
  int dumpLongvol( const char *filename );

  /**
   * Write this volume object into a .raw file
   *  If filanem is "", stdout is used.
   * @return > 0 if error.
   */
  int dumpRaw( const char *filename );

  /**
   * Volume union.
   */
  Longvol &operator |= (const Longvol &);

  /**
   * Volume AND.
   */
  Longvol &operator &= (const Longvol &);

  /**
   * Volume DIFFERENCE.
   */
  Longvol &operator -= (const Longvol &v);

  /**
   * Dummy function for compatibility with the original Vol interface. 
   * DOES NOTHING
   */
  void setVolumeCenter( int x, int y, int z );

  /**
   * Draw Axis X, Y and Z.
   */
  void	drawAxis( );
	
  /**
   * Volume invert (/!\ All colors are lost)
   */
  void invert();
  
  /**
   * Rotate image in volume.
   */
  void rotate( double rx, double ry, double rz );
  
  /**
   * Will compute the symetry of all positives points in the volume.
   */
  void symetry( int maxx, int maxy, int maxz ); 

  /**
   * Translate contents of the volume
   * New voxels are transparents
   */
  void translate( int vx, int vy, int vz );


  /**
   * X-size of the 3D volume
   */
  inline int sizeX() const;
  /**
   * Y-size of the 3D volume
   */
  inline int sizeY() const;
  /**
   * Z-size of the 3D volume
   */
  inline int sizeZ() const;

  /**
   * The least x.
   */
  inline int minX() const;
  /**
   * The least y.
   */
  inline int minY() const;
  /**
   * The least y.
   */
  inline int minZ() const;

  /**
   * The greatest X ( warning : it is not included in bounds )
   */
  inline int maxX() const;
  /**
   * The greatest Y ( warning : it is not included in bounds )
   */
  inline int maxY() const;
  /**
   * The greatest Z ( warning : it is not included in bounds )
   */
  inline int maxZ() const;
	
  /**
   * The X-coordinate of the center.
   */
  inline int cX() const;
  /**
   * The Y-coordinate of the center.
   */
  inline int cY() const;
  /**
   * The Z-coordinate of the center.
   */
  inline int cZ() const;
	
  /**
   * Check if the point is valid for this volume.
   */
  inline bool inBounds( int x, int y, int z ) const;

  /**
   * Returns alpha color.
   */
  lvoxel	alpha() const { return 0; };

  bool isOK() { return ! _zzzImage->isEmpty(); }
	
 private:

  //! Internal resize method
  void resize( int nsx, int nsy, int nsz );


  bool	rotatePoint( int, int, int,
		     double, double, double,
		     int &, int &, int & );

  int sx() const { return _zzzImage->width(); }
  int sy() const { return _zzzImage->height(); }
  int sz() const { return _zzzImage->depth(); }
    
  ZZZImage<Int32> & zzzImage() { return *_zzzImage; }
  const ZZZImage<Int32> & zzzImage() const { return *_zzzImage; }

  ZZZImage<Int32> *_zzzImage;
  bool _destroyVolume;
};


/*
 * Inline methods implementations.
 */

Longvol::Longvol( ZZZImage<Int32> * volume ):_zzzImage( volume ),_destroyVolume( false )	   
{

}

Longvol::Longvol( ZZZImage<Int32> & volume ):_zzzImage( &volume ),_destroyVolume( false )	   
{

}

Longvol::Longvol( int sx, int sy, int sz, lvoxel tcolor )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<Int32>( sx, sy, sz );
  _zzzImage->binarize( tcolor );
}

Longvol::Longvol( const char *fname )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<Int32>;
  if ( *fname ) 
    _zzzImage->load( fname, false );
  else
    _zzzImage->load( "-", false );
}

Longvol::Longvol( const char *fname, int sx, int sy, int sz, lvoxel )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<Int32>( sx, sy, sz );
  if ( *fname ) {
    std::ifstream file( fname );
    _zzzImage->loadRAW( file, false );
  }
  else {
    _zzzImage->loadRAW( std::cin, false );
  }
}

Longvol::Longvol()
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<Int32>;  
}

Longvol::~Longvol()
{
  if ( _destroyVolume )
    delete _zzzImage;
}

const Longvol &
Longvol::operator=(const Longvol & v) {
  if ( _destroyVolume )
    delete _zzzImage;
  _zzzImage = new ZZZImage<Int32>( v.zzzImage() );
  _destroyVolume = true;
  return *this;
}


int
Longvol::sizeX() const { return sx(); }

int
Longvol::sizeY() const { return sy(); }

int
Longvol::sizeZ() const { return sz(); }


int
Longvol::minX() const { return 0; }

int
Longvol::minY() const { return 0; }

int
Longvol::minZ() const { return 0; }


int
Longvol::maxX() const { return sx(); }

int
Longvol::maxY() const { return sy(); }

int
Longvol::maxZ() const { return sz(); }


int
Longvol::cX() const { return sx()/2; }

int
Longvol::cY() const { return sy()/2; }

int
Longvol::cZ() const { return sz()/2; }

bool
Longvol::inBounds( int x, int y, int z ) const {
  return x >= 0 && x < sx() && y >= 0 && y < sy() && z >= 0 && z < sz();
}


inline
Longvol operator&( const Longvol & v1, const Longvol & v2 ) {
  Longvol result( v1 );
  result &= v2;
  return result;
}

inline
Longvol operator|( const Longvol & v1, const Longvol & v2 ) {
  Longvol result( v1 );
  result |= v2;
  return result;
}

inline
Longvol operator-( const Longvol & v1, const Longvol &v2 ) {
  Longvol result( v1 );
  result -= v2;
  return result;
}

#endif //  _LONGVOLZZZ_ADAPTER_

