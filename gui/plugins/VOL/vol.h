/**
 * @file   vol.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2008
 * 
 * @brief  Vol interface class (wrapper of a ZZZImage<UChar>).
 * 
 * Copyright 2008 Sebastien Fourey <Firstname.Lastname@greyc.ensicaen.fr>
 * Copyright 2002, 2003 Alexis Guillaume <aguillau@liris.univ-lyon2.fr> for
 *                       "Laboratoire LIRIS, université Lyon II, France."
 * Copyright 2002, 2003 David Coeurjolly <dcoeurjo@liris.univ-lyon2.fr> for
 *                        "Laboratoire LIRIS, université Lyon II, France."
 * 
 * This file was originally part of libvol.
 * It is a modified version intented to 
 * be used as a wrapper of the ZZZImage<Long> class
 * which present the interface of the Vol class.
 * (C) 2008 Sebastien Fourey
 * 
 */
#ifndef _VOLZZZ_ADAPTER_
#define _VOLZZZ_ADAPTER_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "zzzimage.h"
#include <fstream>


typedef unsigned char voxel;	/**<  Voxel type. */

/* 
   Not implemented :
   
   * Handling of defaultAlpha in: 
     inline Vol( const char *fname, int sx, int sy, int sz, voxel defaultAlpha = 0 );

   * Handling of getHeaderValueAsInt("Axis", &axis) in the constructor from a filename.
 */

class Vol {

 public:

  /** 
   * Default constructor.
   */
  inline Vol();

  /** 
   * Constructor with a given volume size.
   * 
   * @param sx 
   * @param sy 
   * @param sz 
   * @param tcolor 
   */
  inline Vol( int sx, int sy, int sz, voxel tcolor );

  /** 
   * Copy constructor.
   */
  Vol( const Vol & );

  /** 
   * Constructor with an associated ZZZImage.
   * 
   * @param volume 
   */
  inline Vol( ZZZImage<UChar> * volume );

  /** 
   * Constructor with an associated ZZZImage.
   * 
   * @param volume Reference to a volume.
   */
  inline Vol( ZZZImage<UChar> & volume );

  /**
  * Constructor to build a volume from a .vol file.
  * If filename is "", stdin is used.
  */
  inline Vol( const char *fname );

  /**
   * Constructor to build a volume from a .raw file
   * *without* size informations.
   */
  inline Vol( const char *fname, int sx, int sy, int sz, voxel defaultAlpha = 0 );

  /**
   * Destructor (not virtual, so don't inherit this class !)
   */
  inline ~Vol();

  /**
   * Assignement operator.
   */
  inline const Vol & operator=(const Vol &);

  /**
   * Pixel access operator.
   */
  voxel & operator()(int x, int y, int z);

  /**
   * Pixel access operator. (const version)
   */
  voxel operator()(int x, int y, int z) const;


  voxel get( int x, int y, int z ) const { return (*this)(x, y, z); }

  void  set( int x, int y, int z, voxel c ) { (*this)(x, y, z) = c; }

  /**
   * Write this volume object into a .vol file.
   * If filename is "", stdout is used.
   * @return > 0 if error.
   */
  int dumpVol( const char *filename );

  /**
   * Write this volume object into a .raw file
   *  If filanem is "", stdout is used.
   * @return > 0 if error.
   */
  int dumpRaw( const char *filename );

  /**
   * Volume union.
   */
  Vol &operator |= (const Vol &);

  /**
   * Volume AND.
   */
  Vol &operator &= (const Vol &);

  /**
   * Volume DIFFERENCE.
   */
  Vol &operator -= (const Vol &v);

  /**
   * Dummy function for compatibility with the original Vol interface. 
   * DOES NOTHING
   */
  void setVolumeCenter( int x, int y, int z );

  /**
   * Draw Axis X, Y and Z.
   */
  void	drawAxis( );
	
  /**
   * Volume invert (/!\ All colors are lost)
   */
  void invert();
  
  /**
   * Rotate image in volume.
   */
  void rotate( double rx, double ry, double rz );
  
  /**
   * Will compute the symetry of all positives points in the volume.
   */
  void symetry( int maxx, int maxy, int maxz ); 

  /**
   * Translate contents of the volume
   * New voxels are transparents
   */
  void translate( int vx, int vy, int vz );


  /**
   * X-size of the 3D volume
   */
  inline int sizeX() const;
  /**
   * Y-size of the 3D volume
   */
  inline int sizeY() const;
  /**
   * Z-size of the 3D volume
   */
  inline int sizeZ() const;

  /**
   * The least x.
   */
  inline int minX() const;
  /**
   * The least y.
   */
  inline int minY() const;
  /**
   * The least y.
   */
  inline int minZ() const;

  /**
   * The greatest X ( warning : it is not included in bounds )
   */
  inline int maxX() const;
  /**
   * The greatest Y ( warning : it is not included in bounds )
   */
  inline int maxY() const;
  /**
   * The greatest Z ( warning : it is not included in bounds )
   */
  inline int maxZ() const;
	
  /**
   * The X-coordinate of the center.
   */
  inline int cX() const;
  /**
   * The Y-coordinate of the center.
   */
  inline int cY() const;
  /**
   * The Z-coordinate of the center.
   */
  inline int cZ() const;
	
  /**
   * Check if the point is valid for this volume.
   */
  inline bool inBounds( int x, int y, int z ) const;

  /**
   * Returns alpha color.
   */
  voxel	alpha() const { return 0; };

  bool isOK() { return ! _zzzImage->isEmpty(); }
	
 private:

  //! Internal resize method
  void resize( int nsx, int nsy, int nsz );


  bool	rotatePoint( int, int, int,
		     double, double, double,
		     int &, int &, int & );

  int sx() const { return _zzzImage->width(); }
  int sy() const { return _zzzImage->height(); }
  int sz() const { return _zzzImage->depth(); }
    
  ZZZImage<UChar> & zzzImage() { return *_zzzImage; }
  const ZZZImage<UChar> & zzzImage() const { return *_zzzImage; }

  ZZZImage<UChar> *_zzzImage;
  bool _destroyVolume;
};


/*
 * Inline methods implementations.
 */

Vol::Vol( ZZZImage<UChar> * volume ):_zzzImage( volume ),_destroyVolume( false )	   
{

}

Vol::Vol( ZZZImage<UChar> & volume ):_zzzImage( &volume ),_destroyVolume( false )	   
{

}

Vol::Vol( int sx, int sy, int sz, voxel tcolor )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<UChar>( sx, sy, sz );
  _zzzImage->binarize( tcolor );
}

Vol::Vol( const char *fname )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<UChar>;
  if ( *fname ) 
    _zzzImage->load( fname, false );
  else
    _zzzImage->load( "-", false );
}

Vol::Vol( const char *fname, int sx, int sy, int sz, voxel )
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<UChar>( sx, sy, sz );
  if ( *fname ) {
    std::ifstream file( fname );
    _zzzImage->loadRAW( file, false );
  }
  else {
    _zzzImage->loadRAW( std::cin, false );
  }
}

Vol::Vol()
{
  _destroyVolume = true;
  _zzzImage = new ZZZImage<UChar>;  
}

Vol::~Vol()
{
  if ( _destroyVolume )
    delete _zzzImage;
}

const Vol &
Vol::operator=(const Vol & v) {
  if ( _destroyVolume )
    delete _zzzImage;
  _zzzImage = new ZZZImage<UChar>( v.zzzImage() );
  _destroyVolume = true;
  return *this;
}


int
Vol::sizeX() const { return sx(); }

int
Vol::sizeY() const { return sy(); }

int
Vol::sizeZ() const { return sz(); }


int
Vol::minX() const { return 0; }

int
Vol::minY() const { return 0; }

int
Vol::minZ() const { return 0; }


int
Vol::maxX() const { return sx(); }

int
Vol::maxY() const { return sy(); }

int
Vol::maxZ() const { return sz(); }


int
Vol::cX() const { return sx()/2; }

int
Vol::cY() const { return sy()/2; }

int
Vol::cZ() const { return sz()/2; }

bool
Vol::inBounds( int x, int y, int z ) const {
  return x >= 0 && x < sx() && y >= 0 && y < sy() && z >= 0 && z < sz();
}


inline
Vol operator&( const Vol & v1, const Vol & v2 ) {
  Vol result( v1 );
  result &= v2;
  return result;
}

inline
Vol operator|( const Vol & v1, const Vol & v2 ) {
  Vol result( v1 );
  result |= v2;
  return result;
}

inline
Vol operator-( const Vol & v1, const Vol &v2 ) {
  Vol result( v1 );
  result -= v2;
  return result;
}

#endif //  _VOLZZZ_ADAPTER_

