#include "vol.h"

#include "tools.h"
#include <fstream>
#include "ByteOutput.h"

#include <cassert>
#include <iostream>

namespace {
  int invmat33(double inv[3][3], double mat[3][3]);
}

Vol::Vol( const Vol & v ) {
  _destroyVolume = false;
  _zzzImage = new ZZZImage<UChar>( v.zzzImage() );
}

voxel &
Vol::operator()(int x, int y, int z) {
  // assert( state_ok );
  assert( inBounds(x, y, z) );
  return (*_zzzImage)(x,y,z);
}

voxel
Vol::operator()(int x, int y, int z) const {
  // assert( state_ok );
  assert( inBounds(x, y, z) );
  return (*_zzzImage)(x,y,z);
}

int
Vol::dumpVol( const char *fname ) {
  if ( *fname ) {
    std::ofstream file( fname );
    ByteOutputStream output( file ); 
    return ! _zzzImage->saveVOL( output );
  }
  else {
#ifdef _IS_UNIX_
    ByteOutputFD output( 1 ); 
#else
    ByteOutputStream output( std::cout ); 
#endif
    return ! _zzzImage->saveVOL( output );
  }
}

int
Vol::dumpRaw( const char *fname ) {
  if ( *fname ) {
    std::ofstream file( fname );
    ByteOutputStream output( file ); 
    return ! _zzzImage->saveRAW( output );
  }
  else {
#ifdef _IS_UNIX_
    ByteOutputFD output( 1 ); 
#else
    ByteOutputStream output( std::cout ); 
#endif
    return ! _zzzImage->saveRAW( output );
  }
}

void Vol::setVolumeCenter( int /* x */ , int /* y */, int /* z */ ) {

}

void Vol::drawAxis( ) {
  // assert( state_ok );
  // setHeaderValue( "Axis", 1 );
	
  int mins[] = {minX(), minY(), minZ()};
  int maxs[] = {maxX(), maxY(), maxZ()};	
  for (int j = mins[0]; j < maxs[0]; ++j) 
    (*this)(j, cY(), cZ()) = 0x3F;
  for (int j = mins[1]; j < maxs[1]; ++j)
    (*this)(cX(), j, cZ()) = 0x7F;
  for (int j = mins[2]; j < maxs[2]; ++j)
    (*this)(cX(), cY(), j) = 0xFF;
}

Vol &
Vol::operator&=( const Vol & v ) {
  if ( v.sx() != sx() || v.sy() != sy() || v.sz() != sz() ) {
    resize( max( sx(), v.sx() ),
	    max( sy(), v.sy() ),
	    max( sz(), v.sz() ) );
  }
  int px = abs(sx() - v.sx()) / 2;
  int py = abs(sy() - v.sy()) / 2;
  int pz = abs(sz() - v.sz()) / 2;
  for (int i = 0; i < v.sx(); ++i)
    for (int j = 0; j < v.sy(); ++j) 
      for (int k = 0; k < v.sz(); ++k) {
	if ( ! v( i, j, k ) ) {
	  (*this)( i + px, j + py, k + pz ) = 0;
	}
      }
  return *this;
}

Vol &
Vol::operator|=( const Vol & v ) {
  if ( v.sx() != sx() || v.sy() != sy() || v.sz() != sz() )
    resize( max( sx(), v.sx() ),
	    max( sy(), v.sy() ),
	    max( sz(), v.sz() ) );
  int px = abs(sx() - v.sx()) / 2;
  int py = abs(sy() - v.sy()) / 2;
  int pz = abs(sz() - v.sz()) / 2;
  for (int i = 0; i < v.sx(); ++i)
    for (int j = 0; j < v.sy(); ++j) 
      for (int k = 0; k < v.sz(); ++k) {
	if ( ! (*this)( i + px, j + py, k + pz ) && v( i, j, k ) ) {
	  (*this)( i + px, j + py, k + pz ) = v( i, j, k );
	}
      }
  return *this;
}

Vol &
Vol::operator-=( const Vol & v ) {
  if ( v.sx() != sx() || v.sy() != sy() || v.sz() != sz() )
    resize( max( sx(), v.sx() ),
	    max( sy(), v.sy() ),
	    max( sz(), v.sz() ) );
  int px = abs(sx() - v.sx()) / 2;
  int py = abs(sy() - v.sy()) / 2;
  int pz = abs(sz() - v.sz()) / 2;
  for (int i = 0; i < v.sx(); ++i)
    for (int j = 0; j < v.sy(); ++j) 
      for (int k = 0; k < v.sz(); ++k) {
	if ( v( i, j, k ) ) {
	  (*this)( i + px, j + py, k + pz ) = 0;
	}
      }
  return *this;
}

void
Vol::resize( int nsx, int nsy, int nsz ) {
  if ( _zzzImage->size() == Dimension3d( nsx, nsy, nsz ) )
    return;
  
  int px = (nsx - sx())/2;
  int py = (nsy - sy())/2;
  int pz = (nsz - sz())/2;
  ZZZImage<UChar> image( nsx, nsy, nsz );

  for (int i = 0; i < sx(); ++i) {
    for (int j = 0; j < sy(); ++j) { 
      for (int k = 0; k < sz(); ++k)
	image( i + px, j + py, k + pz ) = (*_zzzImage)( i, j, k );
    }
  }
  
  *_zzzImage = image;
}

bool Vol::rotatePoint( int i, int j, int k,
		       double rx, double ry, double rz,
		       int & inx, int & iny, int & inz) {
  
  double crx = cos(rx), srx = sin(rx);
  double cry = cos(ry), sry = sin(ry);
  double crz = cos(rz), srz = sin(rz);
  double m[3][3][3] = { { {1., 0., 0.},
			  {0., crx, -srx},
			  {0., srx, crx} },
			{ {cry, 0., sry},
			  {0., 1., 0.},
			  {-sry, 0., cry} },
			{ {crz, -srz, 0.},
			  {srz, crz, 0.},
			  {0., 0., 1.} } };
  double im[3][3][3];
	
  double x = (double)(i - sx()/2);
  double y = (double)(j - sy()/2);
  double z = (double)(k - sz()/2);

  for (int i = 0; i < 3; ++i) {
    invmat33( im[i], m[i] );
    double nx = im[i][0][0]*x + im[i][0][1]*y + im[i][0][2]*z;
    double ny = im[i][1][0]*x + im[i][1][1]*y + im[i][1][2]*z;
    double nz = im[i][2][0]*x + im[i][2][1]*y + im[i][2][2]*z;
    x = nx;
    y = ny;
    z = nz;
  }

  inx = (int)x + sx()/2;
  iny = (int)y + sy()/2;
  inz = (int)z + sz()/2;
  return (inx >= 0 && inx < sx() && iny >= 0 && iny < sy() && inz >= 0 && inz < sz() );
}

void Vol::rotate( double rx, double ry, double rz ) {
  ZZZImage<UChar> result( _zzzImage->size(), 1 );
  int nx, ny, nz;

  for (int i = 0; i < sx(); ++i) {
    for (int j = 0; j < sy(); ++j) {
      for (int k = 0; k < sz(); ++k) {
	if ( rotatePoint( i, j, k, rx, ry, rz, nx, ny, nz ) )
	  result( i, j, k ) = (*_zzzImage)( nx, ny, nz );
      }
    }
  }
  (*_zzzImage) = result;
}

void
Vol::symetry( int maxx, int maxy, int maxz ) {
  int mins[] = {-maxx, -maxy, -maxz}; 
  int maxs[] = {maxx + 1, maxy + 1, maxz + 1};
  
  for (int x = mins[0]; x < 0; ++x) {                                                               
    for (int y = 0; y < maxs[1]; ++y) {                                                           
      for (int z = 0; z < maxs[2]; ++z) {                                                       
	(*this)( x, y, z ) = (*this)( -x, y, z );                                                         
      }                                                                                         
    }                                                                                             
  }                                                                                                 

  for (int x = mins[0]; x < maxs[0]; ++x) {                                                         
    for (int y = mins[1]; y < 0; ++y) {                                                           
      for (int z = 0; z < maxs[2]; ++z) {                                                       
	(*this)( x, y, z ) = (*this)( x, -y, z );                                                         
      }                                                                                         
    }                                                                                             
  }                                                                                                 

  for (int x = mins[0]; x < maxs[0]; ++x) {                                                         
    for (int y = mins[1]; y < maxs[1]; ++y) {                                                     
      for (int z = mins[2]; z < 0; ++z) {                                                       
	(*this)( x, y, z ) = (*this)( x, y, -z );                                                         
      }                                                                                         
    }                                                                                             
  }                                                                                                 
}

namespace {
  void translateInitLoop( int *begin, int *end, int *step, int v, int m ) {
    if (v > 0) {
      *begin = m;
      *end = 0;
      *step = -1;
    } else {
      *begin = 0;
      *end = m;
      *step = 1;
    }
  }
}

void
Vol::translate( int vx, int vy, int vz ) {
  int begin[3], end[3], step[3];
  translateInitLoop( begin, end, step, vx, sx() );
  translateInitLoop( begin + 1, end + 1, step + 1, vy, sy() );
  translateInitLoop( begin + 2, end + 2, step + 2, vz, sz() );
  for (int i = begin[0]; i != end[0]; i += step[0]) 
    for (int j = begin[1]; j != end[1]; j += step[1]) 
      for (int k = begin[2]; k != end[2]; k += step[2]) {	
	int spos = (i + vx) + (j + vy)*sy() + ( k + vz )*sx()*sy();
	if ( spos >= 0 && spos < sx()*sy()*sz() ) 
	  (*this)( i, j, k ) = (*this)( i + vx, j + vy, k + vz );
	else
	  (*this)( i, j, k ) = 0;
      }
}

namespace {

  int invmat33( double inv[3][3], double mat[3][3] )
  {
    double t4, t6, t8, t10, t12, t14, t1;
  
    t4 = mat[0][0]*mat[1][1];
    t6 = mat[0][0]*mat[1][2];
    t8 = mat[0][1]*mat[1][0];
    t10 = mat[0][2]*mat[1][0];
    t12 = mat[0][1]*mat[2][0];
    t14 = mat[0][2]*mat[2][0];
    t1 = (t4*mat[2][2]-t6*mat[2][1]-t8*mat[2][2]+
	  t10*mat[2][1]+t12*mat[1][2]-t14*mat[1][1]);
  
    if(t1 == 0)
      return 0;

    inv[0][0] = (mat[1][1]*mat[2][2]-mat[1][2]*mat[2][1])/t1;
    inv[0][1] = -(mat[0][1]*mat[2][2]-mat[0][2]*mat[2][1])/t1;
    inv[0][2] = (mat[0][1]*mat[1][2]-mat[0][2]*mat[1][1])/t1;
    inv[1][0] = -(mat[1][0]*mat[2][2]-mat[1][2]*mat[2][0])/t1;
    inv[1][1] = (mat[0][0]*mat[2][2]-t14)/t1;
    inv[1][2] = -(t6-t10)/t1;
    inv[2][0] = (mat[1][0]*mat[2][1]-mat[1][1]*mat[2][0])/t1;
    inv[2][1] = -(mat[0][0]*mat[2][1]-t12)/t1;
    inv[2][2] = (t4-t8)/t1;
    return 1;
  }
}
