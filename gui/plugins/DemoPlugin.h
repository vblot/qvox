/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   DemoPlugin.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2008
 * 
 * @brief  Declaration of the demo plugin functions.
 * 
 */
#ifndef _DEMOPLUGIN_H_
#define _DEMOPLUGIN_H_

#include "zzzimage.h"

void demoPlugin( const ZZZImage<UChar> & src, ZZZImage<Int32> & dst );

#endif // _DEMOPLUGIN_H_
