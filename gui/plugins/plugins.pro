#
# Project file part dedicated to plugins.
#
# This file is included by src/gui/qvox.pro therefore
# paths should be relative to the src/gui directory.

INCLUDEPATH += ./plugins/DT ./plugins/VOL
DEPENDPATH += ./plugins/DT

#
# Add below the source files which should
# be compiled/linked with QVox
#

HEADERS += ./plugins/DT/dt.h
HEADERS += ./plugins/VOL/vol.h ./plugins/VOL/longvol.h

SOURCES += ./plugins/DT/dt.cpp
SOURCES += ./plugins/VOL/vol.cpp ./plugins/VOL/longvol.cpp

#
# If a lib needs to be linked, add it here.
#
LIBS += 
