/** -*- mode: c++ ; c-basic-offset: 3 -*-
 * @file   DemoPlugin.h
 * @author Sebastien Fourey (GREYC)
 * @date   May 2008
 *
 * @brief  Demo plugin functions.
 *
 */
#include "DemoPlugin.h"
#include "dt.h"
#include "vol.h"

/** 
 * Plugin function
 *
 * @param src The source volume.
 * @param dst The plugin result. The plugin function should take
 *            care of resizing/allocation of the destination volume.
 */
void
demoPlugin(const ZZZImage<UChar> & src, ZZZImage<Int32> &dst )
{
  SSize width, height, depth, bands;
  src.getDimension( width, height, depth, bands );
  dst.alloc( width, height, depth, bands );

  ZZZImage<UChar> im(src);

  Vol v( im );

  v.drawAxis();

  // Basic plugin sample code: dst = NOT( src )
  for ( SSize b = 0; b < bands; ++b )
    for ( SSize z = 0; z < depth; ++z )
      for ( SSize y = 0; y < height; ++y )
        for ( SSize x = 0; x < width; ++x ) {
          dst( x, y, z, b ) = ! v( x, y, z );
        }


  // This should be removed
  // It's just a test of a function from DT/dt.cpp
  // ZZZImage<UChar> im;
  // a_dummy_function( im );

}

