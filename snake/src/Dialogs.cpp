/**
 * @file   Dialogs.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:15 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <Dialogs.h>
#include <qmessagebox.h>
#include <qimage.h>
#include <qstrlist.h>

QString FileFilters[] = { 
  /* VolumeFile */
  QString( "All volume types (*.pan *.vff *.3dz *.raw *.vol);;"
	   "Pandore files (*.pan);;"
	   "VFF files (*.vff);;"
	   "3DZ files (*.3dz);;"
	   "RAW files (*.raw);;"
	   "VOL files (*.vol)" ),
  /* ViewParameters File */
  QString( "QVox 3D View parameters (*.view);;" ),
  /* ViewExportFile */
  QString( "Encapsulated Postscript (*.eps);;" 
	   "XFig figure file (*.fig);;" ),

  /* SurfaceExportFile */
  QString( "Geomview Mesh (*.off);;" 
	   "Wavefront Mesh (*.obj);;" 
	   "Surfels as Geomview Mesh (*.off);;" 
	   "POV-Ray file (*.pov);;"),
  /* RawFile */
  QString( "Raw files (*.raw);;"
	   "All files (*);;")
};

Dialogs::Dialogs( QWidget * parent )
{
  _parent = parent;
  QStrList strList = QImage::outputFormats();

  char  *str = strList.first();
  while ( str ) {
    QString ext = QString("(*.") + QString( str ).lower() + ")";
    FileFilters[ ViewExportFile ] += QString(";;") + QString( str ) + " bitmap file " + ext;
    str = strList.next();
  };
  FileFilters[ ViewExportFile ] += ")";
}

Dialogs::~Dialogs()
{
}

void
Dialogs::errorOpening( const char * fileName,
		       const QString & details )
{
  QMessageBox::critical( _parent, 
			 "Error", 
			 QString("Error: File %1 cannot be read.\n").arg( fileName ) + details );
}


QString
Dialogs::askSaveFileName( FileFilter filter, QWidget * parent ) {
  QString fileName;
  if ( ! parent ) parent = _parent;
  fileName = QFileDialog::getSaveFileName( QString::null,
					   FileFilters[ filter ], 
					   parent,
					   "save file dialog",
					   "Choose a file to save",
					   &_selectedFilter );
  if ( fileName.isNull() ) return fileName;

  char extensions[1024];
  char *pc = extensions;
  bool hasExtension = false;
  QString firstExt(".");
  strcpy( extensions, _selectedFilter.ascii() );
  
  while ( ( pc = strstr( pc, "*." ) ) ) {
    pc++;
    char ext[255];
    int i;
    for ( i = 0; i < 4 && pc[i] && QChar( pc[i] ).isLetterOrNumber(); i++ ) ext[i] = pc[i];
    ext[i] = '\0';
    if ( firstExt == QString(".")  ) firstExt = ext;
    if ( strstr( fileName.ascii(), ext ) ) hasExtension = true;
    pc++;
  }
  
  if ( ! hasExtension ) fileName += firstExt;
  if ( QFile::exists( fileName ) ) {
    QFileInfo fileInfo(fileName);
    int i = QMessageBox::warning( parent,
				  "File exists",
				  QString("A file called %1\n"
					  "modified on %2\n"
					  "already exists.\n"
					  "Do you want to overwrite it?"
					  )
				  .arg( fileInfo.fileName() )
				  .arg( fileInfo.lastModified().toString() ),
				  "&Yes",
				  "&No",
				  QString::null );
    if ( i == 0 ) 
      return fileName;
    else
      return QString::null;
  }
  return fileName;
}

QString
Dialogs::askOpenFileName( FileFilter filter, QWidget * parent ) {
  if ( ! parent ) parent = _parent;
  return  QFileDialog::getOpenFileName(QString::null,
				       FileFilters[ filter ], 
				       parent,
				       "open file dialog",
				       "Choose a file to open" );
}

bool
Dialogs::confirmDoNotSave( const char * fileName, QWidget *parent )
{
  QString filename= "";
  if ( *filename ) fileName = QString( "[%1] " ).arg( QFileInfo( filename ).fileName() ); 
  switch ( QMessageBox::question( parent,
				  "File not saved",
				  QString( "Current volume %1has not been saved.\n"
					   "Do you want to save it before continuing?" ).arg( filename ),
				  QString( "&Cancel" ),
				  QString( "&Ignore" ) ) ) {
  case 0: return false;
  case 1: return true;
  default: return false;
  }
}
