/**
 * @file   Document.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:01 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <Document.h>
#include <qcolor.h>
#include <algorithm>
#include <genesis.h>
using std::vector;
using std::map;

Document::Document()
{
  _fileName = QString::null;
  _modified = false;  
  _volume = new ZZZImage<UChar>(10,10,1,3);
  _ucVolume = new ZZZImage<UChar>(10,10,1,3);
  _valueType = Po_ValUC;
  _ssVolume = 0;
  _slVolume = 0;
  _ulVolume = 0;
  _sfVolume = 0;
  _equalization = false;
  _object = _volume;
  _valid = true;
}

void
Document::clear()
{
  dispose( _volume );
  dispose( _ucVolume );
  dispose( _ssVolume );
  dispose( _slVolume );
  dispose( _ulVolume );
  dispose( _sfVolume );
}

Document::~Document()
{
  _valid = false;
  delete _volume;
  delete _ucVolume;
  delete _ssVolume;
  delete _slVolume;
  delete _ulVolume;
  delete _sfVolume;
}

void
Document::create( Short width, Short height, Short depth,
		  Short bands,
		  ValueType valueType ) {
  valid( false );
  clear();
  
  _valueType = valueType;
  _volume = new ZZZImage<UChar>( width, height, depth, bands );
  switch( _valueType ) {
  case Po_ValUC: 
    _ucVolume = new ZZZImage<UChar>( width, height, depth, bands );
    _object = _ucVolume;
    break;
  case Po_ValSS: 
    _ssVolume = new ZZZImage<Short>( width, height, depth, bands );
    _object = _ssVolume;
    break;
  case Po_ValSL: 
    _slVolume = new ZZZImage<Long>( width, height, depth, bands );
    _object = _slVolume;
    break;
  case Po_ValUL: 
    _ulVolume = new ZZZImage<ULong>( width, height, depth, bands );
    _object = _ulVolume;
    break;
  case Po_ValSF: 
    _sfVolume = new ZZZImage<Float>( width, height, depth, bands );
    _object = _sfVolume;
    break;
  }
  _modified = false;
  _fileName = QString::null;
  valid( true );
  emit volumeChanged();
}

void
Document::histo3D( ProgressFunction progress )
{
  ZZZImage<UChar> * histo = new ZZZImage<UChar>(258,258,258,3);  
  Short depth = _volume->depth();
  Short width = _volume->width();
  Short height = _volume->height();
  UChar red, green, blue;
  if ( _volume->bands() == 3 )
    for ( Short z = 0; z < depth; z++ ) {
      if ( progress && ! ( z % 25 ) ) 
	(*progress)( 0, 0, 0 ); 
      for ( Short y = 0; y < height; y++ )
	for ( Short x = 0; x < width; x++ ) {
	  red = (*_volume)[0][z][y][x];
	  green = (*_volume)[1][z][y][x];
	  blue = (*_volume)[2][z][y][x];	
	  (*histo)[0][blue + 1l][green + 1l][red + 1l] = red;
	  (*histo)[1][blue + 1l][green + 1l][red + 1l] = green;
	  (*histo)[2][blue + 1l][green + 1l][red + 1l] = blue;	
	}
    }
  else
    for ( Short z = 0; z < depth; z++ ) {
      if ( progress && ! ( z % 25 ) ) (*progress)( 0, 0, 0 ); 
      for ( Short y = 0; y < height; y++ )
	for ( Short x = 0; x < width; x++ ) {
	  red = green = blue = (*_volume)[0][z][y][x];
	  (*histo)[0][blue + 1l][green + 1l][red + 1l] = red;
	  (*histo)[1][blue + 1l][green + 1l][red + 1l] = red;
	  (*histo)[2][blue + 1l][green + 1l][red + 1l] = red;	
	}
    }
  
  (*progress)( 0, 0, 0 ); 
  
  for ( Short x = 1;  x < 257; x++ ) {
    (*histo)[0][0][0][x] = x - 1;
    (*histo)[1][0][0][x] = 0;
    (*histo)[2][0][0][x] = 0;

    (*histo)[0][0][257][x] = x - 1;
    (*histo)[1][0][257][x] = 255;
    (*histo)[2][0][257][x] = 0;

    (*histo)[0][257][0][x] = x - 1;
    (*histo)[1][257][0][x] = 0;
    (*histo)[2][257][0][x] = 255;

    (*histo)[0][257][257][x] = x - 1;
    (*histo)[1][257][257][x] = 255;
    (*histo)[2][257][257][x] = 255;

    (*histo)[0][0][x][0] = 0;
    (*histo)[1][0][x][0] = x - 1;
    (*histo)[2][0][x][0] = 0;

    (*histo)[0][0][x][257] = 255;
    (*histo)[1][0][x][257] = x - 1;
    (*histo)[2][0][x][257] = 0;

    (*histo)[0][257][x][0] = 0;
    (*histo)[1][257][x][0] = x - 1;
    (*histo)[2][257][x][0] = 255;

    (*histo)[0][257][x][257] = 255;
    (*histo)[1][257][x][257] = x - 1;
    (*histo)[2][257][x][257] = 255;

    (*histo)[0][x][0][0] = 0;
    (*histo)[1][x][0][0] = 0;
    (*histo)[2][x][0][0] = x - 1;

    (*histo)[0][x][0][257] = 255;
    (*histo)[1][x][0][257] = 0;
    (*histo)[2][x][0][257] = x - 1;

    (*histo)[0][x][257][0] = 0;
    (*histo)[1][x][257][0] = 255;
    (*histo)[2][x][257][0] = x - 1;

    (*histo)[0][x][257][257] = 255;
    (*histo)[1][x][257][257] = 255;
    (*histo)[2][x][257][257] = x - 1;
  }

  (*progress)( 0, 0, 0 );  
  
  valid( false );
  clear();
  _volume = new ZZZImage<UChar>( *histo );
  _ucVolume = histo;
  _object = histo;
  valid( true );
  _modified = true;
  emit volumeChanged();
  (*progress)( 0, 0, 0 ); 
}

void
Document::qvoxLogo()
{
  valid( false );
  clear();
  
  _modified = false;
  _fileName = QString::null;
  _valueType = Po_ValUC;
  _volume = new ZZZImage<UChar>;
  _object = _volume;  
  _ucVolume = new ZZZImage<UChar>( *_volume );
  valid( true );
  _modified = false;
  emit volumeChanged();
}


void
Document::cast_copy()
{
  if ( !_volume ) return;
  _modified = true;  
  switch( _valueType ) {
  case Po_ValUC:
    *_volume = *_ucVolume;
    break;
  case Po_ValSS: 
    ::cast_copy( *_volume, *_ssVolume );
    break;
  case Po_ValSL: 
    ::cast_copy( *_volume, *_slVolume );
    break;
  case Po_ValUL: 
    ::cast_copy( *_volume, *_ulVolume );
    break;
  case Po_ValSF: 
    ::cast_copy( *_volume, *_sfVolume );
    break;
  }
}

void
Document::equalization( bool on )
{
  _equalization = on;
  if ( !_volume ) return;
  _modified = true;
  
  switch( _valueType ) {
  case Po_ValUC:
    *_volume = *_ucVolume;
    break;
  case Po_ValSS: 
    if ( _equalization ) 
      ::equalization( *_volume, *_ssVolume );
    else
      realignment( *_volume, *_ssVolume );
    break;
  case Po_ValSL: 
    if ( _equalization ) 
      ::equalization( *_volume, *_slVolume );
    else
      realignment( *_volume, *_slVolume );
    break;
  case Po_ValUL: 
    if ( _equalization ) 
      ::equalization( *_volume, *_ulVolume );
    else
      realignment( *_volume, *_ulVolume );
    break;
  case Po_ValSF: 
    if ( _equalization ) 
      ::equalization( *_volume, *_sfVolume );
    else
      realignment( *_volume, *_sfVolume );
    break;
  }
}


bool
Document::loadablePixmap( const char * filename )
{
  QStrList formats = QImage::inputFormats();
  QString ext = QFileInfo( filename ).extension( false ).upper().remove('.');

  if ( ext == "JPG" ) ext = "JPEG";
  
  if ( formats.contains( ext ) ) return true;
  return false;
}

bool
Document::loadPixmap( const char * filename, ProgressFunction progress )
{
  valid( false );
  _fileName = QString::null;
  clear();

  QImage image;
  if ( ! image.load( filename ) ) return false;
  
  _volume = new ZZZImage<UChar>( image.width(), image.height(), 1, 3 );
  _ucVolume = new ZZZImage<UChar>( image.width(), image.height(), 1, 3 );
  _object = _ucVolume;  

  UChar *** matrixRed = _ucVolume->data( 0 );
  UChar *** matrixGreen = _ucVolume->data( 1 );
  UChar *** matrixBlue = _ucVolume->data( 2 );
  
  for ( int x = 0 ; x < image.width() ; x++ )
    for ( int y = 0; y < image.height() ; y++ ) {
      QRgb rgb = image.pixel( x, y );
      matrixRed[0][y][x] = qRed( rgb );
      matrixGreen[0][y][x] = qGreen( rgb );
      matrixBlue[0][y][x] = qBlue( rgb );
    }
  if ( progress ) (*progress)( 0, 0, 0 );
  equalization( _equalization );
  valid( true );
  _modified = false;
  emit volumeChanged();  
  return true;
}

bool
Document::load( const char * filename, ProgressFunction progress )
{
  valid( false );
  _fileName = filename;

  if ( extension( filename, ".pan" ) )
    _valueType = pobjectFileValueType( filename );
  else 
    if ( extension( filename, ".vol" ) || extension( filename, ".3dz" ) )
      _valueType = volFileValueType( filename );
    else
      _valueType = Po_ValUC;

  if ( _valueType == Po_ValUnknown )
    return false;
  
  TRACE << "Type=" << _valueType
	    << " ValueType= "
	    << POvalueTypeName[ _valueType ] 
	    << std::endl;
  
  clear();
  _volume = new ZZZImage<UChar>;
  bool ok = false;
  switch( _valueType ) {
  case Po_ValUC: 
    _ucVolume = new ZZZImage<UChar>();
    ok = _ucVolume->load( _fileName, progress );
    _object = _ucVolume;
    break;
  case Po_ValSS: 
    _ssVolume = new ZZZImage<Short>();
    ok = _ssVolume->load( _fileName, progress );
    _object = _ssVolume;
    break;
  case Po_ValSL: 
    _slVolume = new ZZZImage<Long>();
    ok = _slVolume->load( _fileName , progress );
    _object = _slVolume;
    break;
  case Po_ValUL: 
    _ulVolume = new ZZZImage<ULong>();
    ok = _ulVolume->load( _fileName , progress );
    _object = _ulVolume;
    break;
  case Po_ValSF: 
    _sfVolume = new ZZZImage<Float>();
    ok = _sfVolume->load( _fileName , progress );
    _object = _sfVolume;
    break;
  default:
    std::cerr << "Error: Document::load(): bad value type\n";
    break;
  }

  equalization( _equalization );
  valid( true );
  emit volumeChanged();
  _modified = false;
  return ok;
}

bool
Document::loadRaw( const char * filename, ProgressFunction progress )
{
  valid( false );
  bool ok = false;
  _fileName = filename;

  TRACE << "Type=" << _valueType
	<< " ValueType= "
	<< POvalueTypeName[ _valueType ] 
	<< std::endl;
  
  switch( _valueType ) {
  case Po_ValUC: 
    ok = _ucVolume->load( _fileName, progress );
    _object = _ucVolume;
    break;
  case Po_ValSS: 
    ok = _ssVolume->load( _fileName, progress );
    _object = _ssVolume;
    break;
  case Po_ValSL: 
    ok = _slVolume->load( _fileName , progress);
    _object = _slVolume;    
    break;
  case Po_ValUL: 
    ok = _ulVolume->load( _fileName , progress);
    _object = _ulVolume;
    break;
  case Po_ValSF: 
    ok = _sfVolume->load( _fileName , progress );
    _object = _sfVolume;
    break;
  }
  equalization( _equalization );
  valid( true );
  emit volumeChanged();
  _modified = false;
  return ok;
}

bool
Document::merge( const char * filename, int axis, ProgressFunction progress )
{
  ZZZImage<UChar> *ucVolume;
  ZZZImage<Short> *ssVolume;
  ZZZImage<Long>  *slVolume;
  ZZZImage<ULong> *ulVolume;
  ZZZImage<Float> *sfVolume;  
  int valueType;
  bool ok = false;
  
  valid( false );
  char * pc;

  if ( ( pc = strstr( filename, ".pan" ) ) && pc[4] == '\0' )
    valueType = pobjectFileValueType( filename );
  else 
    if ( ( pc = strstr( filename, ".vol" ) ) && pc[4] == '\0' )
      valueType = volFileValueType( filename );
    else
      valueType = Po_ValUC;
  
  TRACE << "Type=" << valueType
	<< " ValueType= "
	<< POvalueTypeName[ valueType ] 
	<< std::endl;

  if ( valueType != _valueType ) return false;
  
  switch( valueType ) {
  case Po_ValUC: 
    ucVolume = new ZZZImage<UChar>();
    ok = ucVolume->load( filename, progress );
    if ( ok ) _ucVolume->merge( *ucVolume, axis );
    dispose( ucVolume );
    break;
  case Po_ValSS: 
    ssVolume = new ZZZImage<Short>();
    ok = ssVolume->load( filename, progress );
    if ( ok ) _ssVolume->merge( *ssVolume, axis );
    dispose( ssVolume );
    break;
  case Po_ValSL: 
    slVolume = new ZZZImage<Long>();
    ok = slVolume->load( filename, progress );
    if ( ok ) _slVolume->merge( *slVolume, axis );
    dispose( slVolume );
    break;
  case Po_ValUL: 
    ulVolume = new ZZZImage<ULong>();
    ok = ulVolume->load( filename, progress );
    if ( ok ) _ulVolume->merge( *ulVolume, axis );
    dispose( ulVolume );
    break;
  case Po_ValSF: 
    sfVolume = new ZZZImage<Float>();
    ok = sfVolume->load( filename, progress );
    if ( ok ) _sfVolume->merge( *sfVolume, axis );
    dispose( sfVolume );
    break;
  default:
    std::cerr << "Error: Document::merge(): bad value type\n";
    break;
  }
  if ( ok ) equalization( _equalization );
  valid( true );
  emit volumeChanged();
  _modified = true;
  return true;
}

void
Document::trim( int plane, int n )
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->trim( plane, n ); break;
  case Po_ValSS:  _ssVolume->trim( plane, n ); break;
  case Po_ValSL:  _slVolume->trim( plane, n ); break;
  case Po_ValUL:  _ulVolume->trim( plane, n ); break;
  case Po_ValSF:  _sfVolume->trim( plane, n ); break;
  }
  equalization( _equalization );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::fitBoundingBox( Short margin )
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->fitBoundingBox( margin ); break;
  case Po_ValSS:  _ssVolume->fitBoundingBox( margin ); break;
  case Po_ValSL:  _slVolume->fitBoundingBox( margin ); break;
  case Po_ValUL:  _ulVolume->fitBoundingBox( margin ); break;
  case Po_ValSF:  _sfVolume->fitBoundingBox( margin ); break;
  }
  equalization( _equalization );
  valid( true );
  _modified = true;
  emit volumeChanged();
}

void
Document::save()
{
  _object->save( _fileName );
  _modified = false;
}

void
Document::saveAs( const QString & filename )
{
  _fileName = filename;
  save();
}

bool
Document::setVoxelValue( Short x, Short y, Short z,
			 UChar red, UChar green, UChar blue,
			 bool refresh )
{
  UChar formerValue = (*_volume)[0][z][y][x];
  UChar gray = qGray( red, green, blue );
  if ( _volume->bands() == 1 ) {
    (*_volume)[0][z][y][x] = gray;
    switch( _valueType ) {
    case Po_ValUC: 
      (*_ucVolume)[0][z][y][x] = gray;
      break;
    case Po_ValSS: 
      (*_ssVolume)[0][z][y][x] = gray;
      break;
    case Po_ValSL: 
      (*_slVolume)[0][z][y][x] = gray;
      break;
    case Po_ValUL: 
      (*_ulVolume)[0][z][y][x] = gray;
      break;
    case Po_ValSF: 
      (*_sfVolume)[0][z][y][x] = gray;
      break;
    }    
  } else {

  }
  if ( refresh ) emit volumeChanged();
  _modified = true;
  return  ( formerValue && !gray ) || ( !formerValue && gray );
}

void
Document::raiseLevelMap( Short maxHeight, ProgressFunction progress )
{
  valid( false );
  if ( _volume->depth() != 1 ) return;
  ZZZImage<UChar> * volume = new ZZZImage<UChar>( _volume->width(),
						  _volume->height(),
						  maxHeight,
						  _volume->bands() );
  int h,s,v;
  Short width = _volume->width();
  Short height = _volume->height();
  int step = height / 5;
  if ( _volume->bands() == 1 ) {
    UChar ***matrixSrc = _volume->data( 0 );
    UChar ***matrix = volume->data( 0 );
    for ( int row = 0; row < height; row++ ) {
      if ( progress  && !( row % step ) ) (*progress)( 0, 0, 0 ); 
      for ( int col = 0; col < width; col++ ) {	
	int value = matrixSrc[ 0 ][ row ][ col ];
	value = static_cast<int>( maxHeight * ( value / 255.0 ) );
	for ( int plane = 0; plane < value; plane++ )
	  matrix[ plane ][ row ][ col ] = static_cast<UChar>( value );
      }
    }
  } else {
    UChar ***matrixSrcRed = _volume->data( 0 );
    UChar ***matrixSrcGreen = _volume->data( 1 );
    UChar ***matrixSrcBlue = _volume->data( 2 );
    UChar ***matrixRed = volume->data( 0 );
    UChar ***matrixGreen = volume->data( 1 );
    UChar ***matrixBlue = volume->data( 2 );
    QColor color;
    for ( int row = 0; row < height; row++ ) {
      if ( progress  && !( row % step ) ) (*progress)( 0, 0, 0 ); 
      for ( int col = 0; col < width; col++ ) {	
	color.setRgb( matrixSrcRed[0][row][col] ,
		      matrixSrcGreen[0][row][col] ,
		      matrixSrcBlue[0][row][col]  );
	color.getHsv( h, s, v );
	v = static_cast<int>( maxHeight * ( v / 255.0 ) );
	for ( int plane = 0; plane < v; plane++ ) {
	  matrixRed[ plane ][ row ][ col ] = matrixSrcRed[ 0 ][ row ][ col ];
	  matrixGreen[ plane ][ row ][ col ] = matrixSrcGreen[ 0 ][ row ][ col ];
	  matrixBlue[ plane ][ row ][ col ] = matrixSrcBlue[ 0 ][ row ][ col ];
	}
      }
    }
  }
  
  clear();
  _ucVolume = volume;
  _volume = new ZZZImage<UChar>( *_ucVolume );
  _object = _ucVolume;
  _modified = true;
  if ( progress ) (*progress)( 0, 0, 0 ); 
  valid( true );
  emit volumeChanged();
  if ( progress ) (*progress)( 0, 0, 0 ); 
}

void
Document::mirror( int axis )
{
  valid( false );

  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->mirror( axis ); break;
  case Po_ValSS:  _ssVolume->mirror( axis ); break;
  case Po_ValSL:  _slVolume->mirror( axis ); break;
  case Po_ValUL:  _ulVolume->mirror( axis ); break;
  case Po_ValSF:  _sfVolume->mirror( axis ); break;
  }
  equalization( _equalization );
  valid( true );
  _modified = true;
  emit volumeChanged();  
}

void
Document::invert( UChar value )
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->invert( value ); break;
  case Po_ValSS:  _ssVolume->invert( value ); break;
  case Po_ValSL:  _slVolume->invert( value ); break;
  case Po_ValUL:  _ulVolume->invert( value ); break;
  case Po_ValSF:  _sfVolume->invert( value ); break;
  }
  _modified = true;
  equalization( _equalization );
  valid( true );
  emit volumeChanged();  
}

void
Document::quantize( ULong cardinality )
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->quantize( cardinality ); break;
  case Po_ValSS:  _ssVolume->quantize( cardinality ); break;
  case Po_ValSL:  _slVolume->quantize( cardinality ); break;
  case Po_ValUL:  _ulVolume->quantize( cardinality ); break;
  case Po_ValSF:  _sfVolume->quantize( cardinality ); break;
  }
  _modified = true;
  cast_copy();
  valid( true );
  emit volumeChanged();  
}

void
Document::binarize( UChar value )
{
  valid( false );
  if ( value ) 
    switch( _valueType ) {
    case Po_ValUC:  _ucVolume->binarize( value ); break;
    case Po_ValSS:  _ssVolume->binarize( value ); break;
    case Po_ValSL:  _slVolume->binarize( value ); break;
    case Po_ValUL:  _ulVolume->binarize( value ); break;
    case Po_ValSF:  _sfVolume->binarize( value ); break;
    } 
  else 
    switch( _valueType ) {
    case Po_ValUC:  _ucVolume->clear(); break;
    case Po_ValSS:  _ssVolume->clear(); break;
    case Po_ValSL:  _slVolume->clear(); break;
    case Po_ValUL:  _ulVolume->clear(); break;
    case Po_ValSF:  _sfVolume->clear(); break;
    }
  _modified = true;    
  equalization( _equalization );
  valid( true );
  emit volumeChanged();  
}

void
Document::toGray()
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->toGray(  ); break;
  case Po_ValSS:  _ssVolume->toGray(  ); break;
  case Po_ValSL:  _slVolume->toGray(  ); break;
  case Po_ValUL:  _ulVolume->toGray(  ); break;
  case Po_ValSF:  _sfVolume->toGray(  ); break;
  }
  _modified = true;
  equalization( _equalization );
  valid( true );
  emit volumeChanged();  
}

void
Document::fill()
{
  valid( false );  
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->fillForHoleClosing(); break;
  case Po_ValSS:  _ssVolume->fillForHoleClosing(); break;
  case Po_ValSL:  _slVolume->fillForHoleClosing(); break;
  case Po_ValUL:  _ulVolume->fillForHoleClosing(); break;
  case Po_ValSF:  _sfVolume->fillForHoleClosing(); break;
  }
  _volume->fillForHoleClosing();
  _modified = true;
  valid( true );
  _modified = true;
  emit volumeChanged();  
}

void
Document::subSample( UChar minCount )
{
  valid( false );

  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->subSample( minCount ); break;
  case Po_ValSS:  _ssVolume->subSample( minCount ); break;
  case Po_ValSL:  _slVolume->subSample( minCount ); break;
  case Po_ValUL:  _ulVolume->subSample( minCount ); break;
  case Po_ValSF:  _sfVolume->subSample( minCount ); break;
  }
  _modified = true;
  equalization( _equalization );
  valid( true );
  emit volumeChanged();    
}

void
Document::scale( UChar direction, UChar factor )
{
  valid( false );
  
  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->scale( direction, factor ); break;
  case Po_ValSS:  _ssVolume->scale( direction, factor ); break;
  case Po_ValSL:  _slVolume->scale( direction, factor ); break;
  case Po_ValUL:  _ulVolume->scale( direction, factor ); break;
  case Po_ValSF:  _sfVolume->scale( direction, factor ); break;
  }
  _modified = true;
  equalization( _equalization );
  valid( true );
  emit volumeChanged();    
}

void
Document::extrude( Short depth )
{
  valid( false );

  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->extrude( depth ); break;
  case Po_ValSS:  _ssVolume->extrude( depth ); break;
  case Po_ValSL:  _slVolume->extrude( depth ); break;
  case Po_ValUL:  _ulVolume->extrude( depth ); break;
  case Po_ValSF:  _sfVolume->extrude( depth ); break;
  }
  _modified = true;
  equalization( _equalization );
  valid( true );
  emit volumeChanged();    
}

void
Document::bitPlane( UChar bit, Long band )
{
  valid( false );

  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->bitPlane( bit, band ); break;
  case Po_ValSS:  _ssVolume->bitPlane( bit, band ); break;
  case Po_ValSL:  _slVolume->bitPlane( bit, band ); break;
  case Po_ValUL:  _ulVolume->bitPlane( bit, band ); break;
  case Po_ValSF:  _sfVolume->bitPlane( bit, band ); break;
  }
  cast_copy();
  _modified = true;
  valid( true );
  emit volumeChanged();    
}

void
Document::bitPlaneGrayCode( UChar bit, Long band )
{
  valid( false );

  switch( _valueType ) {
  case Po_ValUC:  _ucVolume->bitPlaneGrayCode( bit, band ); break;
  case Po_ValSS:  _ssVolume->bitPlaneGrayCode( bit, band ); break;
  case Po_ValSL:  _slVolume->bitPlaneGrayCode( bit, band ); break;
  case Po_ValUL:  _ulVolume->bitPlaneGrayCode( bit, band ); break;
  case Po_ValSF:  _sfVolume->bitPlaneGrayCode( bit, band ); break;
  }
  cast_copy();
  _modified = true;
  valid( true );
  emit volumeChanged();    
}

void
Document::skel( bool singlePass, int adjacency, int method, int terminalTest )
{
  int iterations = singlePass;
  valid( false );

  TRACE << "SKEL pass:" << singlePass 
	<< " ADJ: " << adjacency
	<< " Method: " << method
	<< " Terminal: " << terminalTest << std::endl;
  if ( valueType() != Po_ValUC || bands() != 1 ) return;
  if ( method == 1 ) {
    skelSequential( *_ucVolume, 
		    iterations,
		    0, /* Directions sequence */
		    adjacency,
		    ArrayTerminalTest[ terminalTest ][ adjacency ],
		    false, /* binarize */
		    0 /* Trace filename */ );
  } else {
    skelParallal( *_ucVolume,
		  iterations,
		  adjacency,
		  ArrayTerminalTest[ terminalTest ][ adjacency ],
		  false,  /* binarize */
		  0 /* Trace filename */ );
  }
  cast_copy();
  _modified = true;
  valid( true );
  emit volumeChanged();    
}

void
Document::distanceMap()
{
  valid( false );
  ZZZImage<Long> *slVolume;
  
  slVolume = new ZZZImage< Long >( _volume->width(), 
				   _volume->height(),
				   _volume->depth(),
				   1 );
  distanceTransformNoBorder( *_volume, *slVolume );
  clear();

  _valueType = Po_ValSL;
  _modified = true;
  _fileName = QFileInfo( _fileName ).dirPath()
    + QString("/edt_")
    + QFileInfo( _fileName ).fileName();
  _slVolume = slVolume;
  _volume = new ZZZImage<UChar>;
  _object = _slVolume;
  
  realignmentDistanceMap( *_volume, *_slVolume );
  _modified = true;
  valid( true );
  emit volumeChanged();    
}

void
Document::sphere( Long x, Long y, Long z, Long r )
{
  valid( false );
  switch( _valueType ) {
  case Po_ValUC:
    genesisSphere<UChar>( *_ucVolume, 
			  Triple<Long>( x, y, z ), r,
			  static_cast<UChar>( 1 ), Triple<float>(0,0,0), Triple<float>(1,1,1) );
    break;
  case Po_ValSS:
    genesisSphere<Short>( *_ssVolume,
			  Triple<Long>( x, y, z ), r,
			  static_cast<Short>( 1 ), Triple<float>(0,0,0), Triple<float>(1,1,1) );
    break;
  case Po_ValSL:
    genesisSphere<Long>( *_slVolume,
			 Triple<Long>( x, y, z ), r,
			 1l, Triple<float>(0,0,0), Triple<float>(1,1,1) );
    break;
  case Po_ValUL:
    genesisSphere<ULong>( *_ulVolume,
			  Triple<Long>( x, y, z ), r,
			  static_cast<ULong>( 1 ), Triple<float>(0,0,0), Triple<float>(1,1,1) );
    break;
  case Po_ValSF:
    genesisSphere<Float>( *_sfVolume,
			  Triple<Long>( x, y, z ), r,
			  1.0f, Triple<float>(0,0,0), Triple<float>(1,1,1) );
    break;
  }
  _modified = true;
  cast_copy();
  valid( true );
  emit volumeChanged(); 
}

void 
Document::threshold( Triple<Long> min, 
		     Triple<Long> max, 
		     vector< Triple<Long> > & removedValues )
{
  Long width = _volume->width();
  Long height = _volume->height();
  Long depth = _volume->depth();
  vector< Triple<Long> >::iterator it;
  valid( false );

  if ( _volume->bands() == 1 ) {
    switch ( valueType() ) {
    case Po_ValSS:
      {
	UChar ***vMatrix = _volume->data( 0 );
	Short ***ssvMatrix = _ssVolume->data( 0 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Short value = ssvMatrix[z][y][x];
	      it = std::find( removedValues.begin(), 
			      removedValues.end(),
			      Triple<Long>( value ) );
	      if ( it != removedValues.end() || 
		   value < min.first || 
		   value > max.first )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    case Po_ValSL: 
      {
	UChar ***vMatrix = _volume->data( 0 );
	Long ***slvMatrix = _slVolume->data( 0 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Long value = slvMatrix[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<Long>( value ) );
	      if ( it != removedValues.end() 
		   || value < min.first 
		   || value > max.first )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    default:
      return;
    }
  } else {
    switch ( valueType() ) {
    case Po_ValSS: 
      {
	UChar ***vMatrix = _volume->data( 0 );
	Short ***ssvMatrix0 = _ssVolume->data( 0 );
	Short ***ssvMatrix1 = _ssVolume->data( 1 );
	Short ***ssvMatrix2 = _ssVolume->data( 2 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Short value0 = ssvMatrix0[z][y][x];
	      Short value1 = ssvMatrix1[z][y][x];
	      Short value2 = ssvMatrix2[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<Long>( value0, value1, value2 ) );
	      if ( it != removedValues.end() 
		   || value0 < min.first || value0 > max.first
		   || value1 < min.second || value1 > max.second
		   || value2 < min.third || value2 > max.third )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    case Po_ValSL: 
      {
	UChar ***vMatrix = _volume->data( 0 );
	Long ***slvMatrix0 = _slVolume->data( 0 );
	Long ***slvMatrix1 = _slVolume->data( 1 );
	Long ***slvMatrix2 = _slVolume->data( 2 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Long value0 = slvMatrix0[z][y][x];
	      Long value1 = slvMatrix1[z][y][x];
	      Long value2 = slvMatrix2[z][y][x];
	      if ( value0 < min.first || value0 > max.first
		   || value1 < min.second || value1 > max.second
		   || value2 < min.third || value2 > max.third )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    default:
      return;
    }
  }
  _modified = true;
  valid( true );
  emit volumeChanged();
}

void
Document::threshold( Triple<ULong> min, 
		     Triple<ULong> max, 
		     vector< Triple<ULong> > & removedValues )
{
  Long width = _volume->width();
  Long height = _volume->height();
  Long depth = _volume->depth();
  vector< Triple<ULong> >::iterator it;
  valid( false );
    
  if ( _volume->bands() == 1 ) {
    switch ( valueType() ) {
    case Po_ValUC:
      {
	UChar ***vMatrix = _volume->data( 0 );
	UChar ***ucvMatrix = _ucVolume->data( 0 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      UChar value = ucvMatrix[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<ULong>( value ) );
	      if ( it != removedValues.end() || 
		   value < min.first || 
		   value > max.first ) {
		vMatrix[z][y][x] = 0;
	      }
	    }
      }
      break;
    case Po_ValUL: 
      {
	UChar ***vMatrix = _volume->data( 0 );
	ULong ***ulvMatrix = _ulVolume->data( 0 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      ULong value = ulvMatrix[z][y][x];
	      it = std::find( removedValues.begin(), 
			      removedValues.end(),
			      Triple<ULong>( value ) );
	      if ( it != removedValues.end() 
		   || value < min.first 
		   || value > max.first )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    default:
      return;
    }
  } else {
    UChar ***vMatrix0 = _volume->data( 0 );
    UChar ***vMatrix1 = _volume->data( 1 );
    UChar ***vMatrix2 = _volume->data( 2 );
    switch ( valueType() ) {
    case Po_ValUC: 
      {
	UChar ***ucvMatrix0 = _ucVolume->data( 0 );
	UChar ***ucvMatrix1 = _ucVolume->data( 1 );
	UChar ***ucvMatrix2 = _ucVolume->data( 2 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      UChar value0 = ucvMatrix0[z][y][x];
	      UChar value1 = ucvMatrix1[z][y][x];
	      UChar value2 = ucvMatrix2[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<ULong>( value0, value1, value2 ) );
	      if ( it != removedValues.end() 
		   || value0 < min.first || value0 > max.first
		   || value1 < min.second || value1 > max.second
		   || value2 < min.third || value2 > max.third )
		vMatrix0[z][y][x] = vMatrix1[z][y][x] = vMatrix2[z][y][x]  = 0;
	    }
      }
      break;
    case Po_ValUL: 
      {
	ULong ***ulvMatrix0 = _ulVolume->data( 0 );
	ULong ***ulvMatrix1 = _ulVolume->data( 1 );
	ULong ***ulvMatrix2 = _ulVolume->data( 2 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      ULong value0 = ulvMatrix0[z][y][x];
	      ULong value1 = ulvMatrix1[z][y][x];
	      ULong value2 = ulvMatrix2[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<ULong>( value0, value1, value2 ) );
	      if ( it != removedValues.end() 
		   || value0 < min.first || value0 > max.first
		   || value1 < min.second || value1 > max.second
		   || value2 < min.third || value2 > max.third )
		vMatrix0[z][y][x] = vMatrix1[z][y][x] = vMatrix2[z][y][x]  = 0;
	    }
      }
      break;
    default:
      return;
    }
  }
  valid( true );
  emit volumeChanged();
}   

void
Document::threshold( Triple<Float> min, 
		     Triple<Float> max, 
		     vector< Triple<Float> > & removedValues )
{
  Long width = _volume->width();
  Long height = _volume->height();
  Long depth = _volume->depth();
  vector< Triple<Float> >::iterator it;
  valid( false );
  
  if ( _volume->bands() == 1 ) {
    switch ( valueType() ) {
    case Po_ValSF:
      {
	UChar ***vMatrix = _volume->data( 0 );
	Float ***sfvMatrix = _sfVolume->data( 0 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Float value = sfvMatrix[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<Float>( value ) );
	      if ( it != removedValues.end() || 
		   value < min.first || 
		   value > max.first )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    default:
      return;
    }
  } else {
    switch ( valueType() ) {
    case Po_ValSF: 
      {
	UChar ***vMatrix = _volume->data( 0 );
	Float ***sfvMatrix0 = _sfVolume->data( 0 );
	Float ***sfvMatrix1 = _sfVolume->data( 1 );
	Float ***sfvMatrix2 = _sfVolume->data( 2 );
	for ( Long z = 0 ; z < depth; z++ ) 
	  for ( Long y = 0 ; y < height; y++ ) 
	    for ( Long x = 0 ; x < width; x++ ) {
	      Float value0 = sfvMatrix0[z][y][x];
	      Float value1 = sfvMatrix1[z][y][x];
	      Float value2 = sfvMatrix2[z][y][x];
	      it = std::find( removedValues.begin(),
			      removedValues.end(),
			      Triple<Float>( value0, value1, value2 ) );
	      if ( it != removedValues.end() 
		   || value0 < min.first || value0 > max.first
		   || value1 < min.second || value1 > max.second
		   || value2 < min.third || value2 > max.third )
		vMatrix[z][y][x] = 0;
	    }
      }
      break;
    default:
      return;
    }
  }
  valid( true );
  emit volumeChanged();
}

template<typename T>
inline bool positive( const T & x )
{
  return x >= 0;
}

template<>
inline bool positive<UChar>( const UChar & )
{  return true; }

template<>
inline bool positive<ULong>( const ULong & )
{  return true; }


template<typename T>
void realignment( ZZZImage<UChar> & result,
		  ZZZImage<T> & source )
{
  T smin, smax;
  T *** sarray;
  UChar *** darray;
  if ( source.bands() != 3 && source.bands() != 1 ) {
    std::cerr << "Error: realignement(): bad number of bands (" 
	      << source.bands() << ")\n";
    return;
  }
  
  Long bands, depth, height, width;
  bands = source.bands();
  depth = source.depth();
  height = source.height();
  width = source.width();
  
  if ( result.bands() != bands ||
       result.depth() != depth ||
       result.width() != width ||
       result.height() != height )
    result.alloc( source.width(),
		  source.height(),
		  source.depth(),
		  source.bands() );

  
  for ( int band = 0; band < bands; band++ ) {
    sarray = source.data( band );  
    smin = sarray[0][0][0];
    smax = sarray[0][0][0];
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ ) {
	  T v = sarray[plane][row][col];
	  if ( v > smax ) smax = v;
	  if ( v < smin ) smin = v;
	}

    Float slope;
    if ( positive( smin ) /*  >= static_cast<T>( 0 ) */ ) {
      smin = static_cast<T>( 0 );
      if ( smax <= static_cast<T>( 255 ) ) 
	slope = 1.0F;
      else
	slope =  255.0F / smax;
    } else {
      if ( smax == smin )
	slope = 1.0F;
      else 
	slope = 255.0F / ( smax - smin );
    }   
    sarray = source.data( band );
    darray = result.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ )
	  darray[plane][row][col] =
	    static_cast<UChar>( slope * ( sarray[plane][row][col] - smin ) );
  }
}

void realignment( ZZZImage<UChar> & result, ZZZImage<Short> & source );
void realignment( ZZZImage<UChar> & result, ZZZImage<Long> & source );
void realignment( ZZZImage<UChar> & result, ZZZImage<Float> & source );

void realignmentDistanceMap( ZZZImage<UChar> & result,
			     ZZZImage<Long> & source )
{
  Long smin, smax;
  Long *** sarray;
  UChar *** darray;
  if ( source.bands() != 3 && source.bands() != 1 ) {
    std::cerr << "Error: realignementDistanceMap(): bad number of bands: " 
	      << source.bands() << "\n";
    return;
  }
  
  Long bands, depth, height, width;
  bands = source.bands();
  depth = source.depth();
  height = source.height();
  width = source.width();
  
  if ( result.bands() != bands ||
       result.depth() != depth ||
       result.width() != width ||
       result.height() != height )
    result.alloc( source.width(),
		  source.height(),
		  source.depth(),
		  source.bands() );
  
  for ( int band = 0; band < bands; band++ ) {
    sarray = source.data( band );  
    smin = sarray[0][0][0];
    smax = sarray[0][0][0];
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ ) {
	  Long v = sarray[plane][row][col];
	  if ( v && ( v > smax ) ) smax = v;
	  if ( v && ( v < smin ) ) smin = v;
	}
    Float slope;
    if ( smin >= static_cast<Long>( 0 ) ) {
      if ( smax <= static_cast<Long>( 255 ) ) 
	slope = 1.0F;
      else
	slope =  255.0F / smax;
    } else {
      if ( smax == smin )
	slope = 1.0F;
      else 
	slope = 255.0F / ( smax - smin );
    }   
    sarray = source.data( band );
    darray = result.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ ) {
	  if ( sarray[plane][row][col] )
	    darray[plane][row][col] = 1 + 
	      static_cast<UChar>( slope * ( sarray[plane][row][col] - smin ) );
	}
  }
}

template<typename T>
void equalization( ZZZImage<UChar> & result,
		   ZZZImage<T> & source )
{
  map<T,ULong> histo;
  map<T,Float> histoc;
  T ***sarray;
  UChar ***darray;

  if ( source.bands() != 3 && source.bands() != 1 ) {
    std::cerr << "Error: realignement(): bad number of bands: " 
	      << source.bands() << "\n";
    return;
  }
  
  Long bands, depth, height, width;
  bands = source.bands();
  depth = source.depth();
  height = source.height();
  width = source.width();
  
  if ( result.bands() != bands ||
       result.depth() != depth ||
       result.width() != width ||
       result.height() != height )
    result.alloc( source.width(),
		  source.height(),
		  source.depth(),
		  source.bands() );
  
  for ( int band = 0; band < bands; band++ ) {    
    sarray = source.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ )
	  histo[ sarray[plane][row][col] ]++;
    
    ULong sum = 0;
    typename map<T,ULong>::const_iterator h;
    typename map<T,ULong>::const_iterator end;
    h = histo.begin();
    T h0 = h->second;
    histoc[ h->first ] = 0.0F;
    h++;
    Float total = source.vectorSize() - h0;
    end = histo.end();
    for ( ; h != end; h++) {
      sum  = sum + h->second;
      histoc[ h->first ] = sum / total;
   }

    sarray = source.data( band );
    darray = result.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ )
	  darray[plane][row][col] =
	    static_cast<UChar>( histoc[ sarray[plane][row][col] ] * 255.0F );
    histo.clear();
    histoc.clear();
  }
}

void equalization( ZZZImage<UChar> & result, ZZZImage<Short> & source );
void equalization( ZZZImage<UChar> & result, ZZZImage<Long> & source );
void equalization( ZZZImage<UChar> & result, ZZZImage<Float> & source );

template<typename T>
void cast_copy( ZZZImage<UChar> & result,
		ZZZImage<T> & source )
{
  T ***sarray;
  UChar ***darray;

  if ( source.bands() != 3 && source.bands() != 1 ) {
    std::cerr << "Error: cast_copy(): bad number of bands: " 
	      << source.bands() << "\n";
    return;
  }
  
  Long bands, depth, height, width;
  bands = source.bands();
  depth = source.depth();
  height = source.height();
  width = source.width();
  
  if ( result.bands() != bands ||
       result.depth() != depth ||
       result.width() != width ||
       result.height() != height )
    result.alloc( source.width(),
		  source.height(),
		  source.depth(),
		  source.bands() );
  
  for ( int band = 0; band < bands; band++ ) {    
    sarray = source.data( band );
    darray = result.data( band );
    for ( int plane = 0; plane < depth; plane++ )
      for ( int row = 0; row < height; row++ )
	for ( int col = 0; col < width; col++ )
	  darray[plane][row][col] =
	    static_cast<UChar>( sarray[plane][row][col] );
  }
}

unsigned long
Document::rawSize()
{
  unsigned long cellSize=0;
  switch( _valueType ) {
  case Po_ValUC:  cellSize = 1; break;
  case Po_ValSS:  cellSize = 2; break;
  case Po_ValSL:  cellSize = 4; break;
  case Po_ValUL:  cellSize = 4; break;
  case Po_ValSF:  cellSize = 4; break;
  }
  return cellSize * _volume->bands() *
    _volume->depth() * _volume->height() * _volume->width();
}
