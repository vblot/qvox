/**
 * @file   View2DWidget.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:38 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <View2DWidget.h>

View2DWidget::View2DWidget( QWidget * parent, const char * name )
  :QWidget( parent, name)
{
  _volume = 0;
  _matrixRed = _matrixGreen = _matrixBlue = 0;
  _originRow = _originCol = 0;
  _originCols[0] = _originCols[1] = _originCols[2] = 0;
  _originRows[0] = _originRows[1] = _originRows[2] = 0;
  _zoom = 1;
  _x = _y = _z = 0;
  _drawingMode = false;
  _volume = 0 ;
  _maxZoom = 64;
  setBackgroundMode( Qt::NoBackground );
  _image.create( 100, 100, 32);
  _underMouseCol = _underMouseRow = -1;
  _leftButton = _rightButton = false;
  _plane = PlaneOxy;
  _depth = 0;
  _document = 0;
  _bands = 1;
  _volumeModified = false;
}

View2DWidget::~View2DWidget()
{
}

bool
View2DWidget::volumeModified()
{
  bool result = _volumeModified;
  _volumeModified = false;
  return result;
}
void
View2DWidget::setDocument( Document * document )
{
  if ( _document ) 
    disconnect( _document, SIGNAL( volumeChanged() ),
		this, SLOT( changeVolume() ) );
  _document = document;
  connect( _document, SIGNAL( volumeChanged() ),
	   this, SLOT( changeVolume() ) );
  changeVolume();
}

void
View2DWidget::changeVolume()
{
  _volume = _document->volume();
  _matrixRed = _volume->data( 0 );
  _bands = _document->volume()->bands();
  if ( _bands == 3 ) {
    _matrixGreen = _volume->data(1);
    _matrixBlue = _volume->data(2);
  }  
  setPlane( PlaneOxy, true );  
  _cols = width() / _zoom;
  _rows = height() / _zoom;
  if ( _cols > _volumeWidth ) _cols = _volumeWidth;
  if ( _rows > _volumeHeight ) _rows = _volumeHeight;
  _leftMargin = (width() - (_cols * _zoom)) >> 1;
  _topMargin = (height() - (_rows * _zoom)) >> 1;
  _maxOriginCol = _volumeWidth - _cols;
  _maxOriginRow = _volumeHeight - _rows;

  emit maxOriginRowChanged( _maxOriginRow );
  emit maxOriginColChanged( _maxOriginCol );
  emit originRowChanged( _originRow );
  emit originColChanged( _originCol );
}


void
View2DWidget::setDrawingMode()
{
  _drawingMode = true;
}

void
View2DWidget::unsetDrawingMode()
{
  _drawingMode = false;
}

void
View2DWidget::setApplication( QApplication * application )
{
  _application = application;
}

void
View2DWidget::paintEvent( QPaintEvent *)
{
  redraw();
}

void
View2DWidget::computeViewParams()
{
  _cols = width() / _zoom;
  _rows = height() / _zoom;
  if ( _cols > _volumeWidth ) _cols = _volumeWidth;
  if ( _rows > _volumeHeight ) _rows = _volumeHeight;
  _leftMargin = (width() - (_cols * _zoom)) >> 1;
  _topMargin = (height() - (_rows * _zoom)) >> 1;
  
  _maxOriginCol = _volumeWidth - _cols;
  _maxOriginRow = _volumeHeight - _rows;

  if ( _originCol > _maxOriginCol ) {
    _originCol = _maxOriginCol;
    emit originColChanged( _originCol );
  }
  if ( _originRow > _maxOriginRow ) {
    _originRow = _maxOriginRow;
    emit originRowChanged( _originRow );  
  }
  emit maxOriginRowChanged( _maxOriginRow );
  emit maxOriginColChanged( _maxOriginCol );
}

void
View2DWidget::resizeEvent( QResizeEvent * e)
{
  _image.create( e->size().width(), 
		 e->size().height(),
		 32 );
  computeViewParams();
  zoomFit();
  //redraw();
}

void
View2DWidget::zoomIn()
{
  if ( ! _document ) return;
  if ( _zoom >= _maxZoom ) return;
  _zoom *= 2;  
  computeViewParams();
  redraw();
  emit zoomChanged();
}

void
View2DWidget::zoomOut()
{
  if ( ! _document ) return;
  if ( _zoom == 1 ) return;
  _zoom /= 2;
  computeViewParams();
  redraw();
  emit zoomChanged();
}

void
View2DWidget::zoomFit()
{
  if ( ! _document ) return;

  float zoomH = width() / _volumeWidth;
  float zoomV = height() / _volumeHeight;
  
  if ( zoomH < zoomV ) 
    zoomV = zoomH;

  int z = static_cast<int>( zoomV );
  int n = 0;
  while ( z && ++n) z >>= 1;
  z = 1 << (n-1);
  if ( z != _zoom ) {
    _zoom = z;
    computeViewParams();
    redraw();
    emit zoomChanged();
  }
}

void
View2DWidget::keyPressEvent( QKeyEvent * e )
{
  switch ( e->ascii() ) {
  case '+':
    zoomIn();
    break;
  case '-':
    zoomOut();
    break;
  case '=':
    zoomFit();
    break;    
  }

  switch ( e->key() ) {
  case Qt::Key_Prior:
    setDepth( _depth + 1 );
    break;
  case Qt::Key_Next:
    setDepth( _depth - 1 );
    break;
  }

}

void
View2DWidget::drawVoxel ( int , int  ) {
  bool modified = false; 
//   modified = _document->setVoxelValue( x, y, _depth, 
// 				       _colorMap->selectedQColor().red(),
// 				       _colorMap->selectedQColor().green(),
// 					 _colorMap->selectedQColor().red(),
// 				       false );
  redraw();
  if ( modified ) _volumeModified = true;
}

void
View2DWidget::clearVoxel ( int x, int y  ) {
  bool modified = false; 
  modified = _document->setVoxelValue( x, y, _depth, 
					 0, 0, 0, 
					 false );
  redraw();
  if ( modified ) _volumeModified = true;
}

void
View2DWidget::mousePressEvent ( QMouseEvent * e  )
{
  if ( ! _document->valid() || 
       e->pos().x() < _leftMargin ||
       e->pos().y() < _topMargin ||
       e->pos().x() >= _leftMargin + _zoom*_cols ||
       e->pos().y() >= _topMargin +  _zoom*_rows ) return;
  
  int x = _originCol +  ( e->pos().x() - _leftMargin ) / _zoom;
  int y = _originRow + ( e->pos().y() - _topMargin ) / _zoom;
    
  if ( !_drawingMode
       && x == _underMouseCol
       && y == _underMouseRow ) return;

  _underMouseCol = x;
  _underMouseRow = y;
  
  if ( _drawingMode && e->button() == Qt::LeftButton ) drawVoxel( x, y );
  if ( _drawingMode && e->button() == Qt::RightButton ) clearVoxel( x, y );
  
  if ( _bands == 3 ) {
    QString message("Pointed: X,Y,Z=%1,%2,%3  Value=[%4]");
    switch ( _plane ) {
    case PlaneOxy:
      emit infoChanged( message.arg( x ).arg( y ).arg( _depth )
			.arg( _document->object()->valueString(x,y,_depth) ) );
      break;
    case PlaneOxz:
      emit infoChanged( message.arg( x ).arg( _depth ).arg( y )
			.arg( _document->object()->valueString(x,_depth,y) ) );
      break;
    case PlaneOyz:
      emit infoChanged( message.arg( _depth ).arg( x ).arg( y )
			.arg( _document->object()->valueString(_depth, x, y) ) );
      break;
    }
  } else {
    QString message("Pointed: X,Y,Z=%1,%2,%3  Value=[%4]");
    switch ( _plane ) {
    case PlaneOxy:
      emit infoChanged( message.arg( x ).arg( y ).arg( _depth )
			.arg( _document->object()->valueString(x,y,_depth) ) );
      break;
    case PlaneOxz:
      emit infoChanged( message.arg( x ).arg( _depth ).arg( y )
			.arg(  _document->object()->valueString(x,_depth,y) ) );
      break;
    case PlaneOyz:
      emit infoChanged( message.arg( _depth ).arg( x ).arg( y )
			.arg( _document->object()->valueString(_depth, x, y) ) );
      break;
    }
  }
  if ( e->button() == QMouseEvent::LeftButton ) {
    if ( ! _drawingMode ) {
      _prevX = e->pos().x();
      _prevY = e->pos().y();
    }
    _leftButton = true;
  }

  if ( e->button() == QMouseEvent::RightButton ) {
    if ( ! _drawingMode ) {
      _prevX = e->pos().x();
      _prevY = e->pos().y();
    }
    _rightButton = true;
  }

}

void
View2DWidget::mouseReleaseEvent ( QMouseEvent * e )
{
  if ( e->button() == QMouseEvent::LeftButton ) _leftButton = false;
  if ( e->button() == QMouseEvent::RightButton ) _rightButton = false;
}

void
View2DWidget::mouseMoveEvent ( QMouseEvent * e )
{
  bool update = false;
  if ( ! _document->valid() ||
       e->pos().x() < _leftMargin
       || e->pos().y() < _topMargin
       || e->pos().x() >= _leftMargin + _zoom*_cols
       || e->pos().y() >= _topMargin + _zoom*_rows ) return;

  if ( ! _leftButton && ! _rightButton ) return;  
  if ( _drawingMode ) {
    int x = _originCol +  ( e->pos().x() - _leftMargin ) / _zoom;
    int y = _originRow + ( e->pos().y() - _topMargin ) / _zoom;
    if ( x == _underMouseCol && y == _underMouseRow ) return;
    _underMouseCol = x;
    _underMouseRow = y;
    if ( _leftButton ) drawVoxel( x, y );
    if ( _rightButton ) clearVoxel( x, y );
  } else {
    /* Sliding the view */
    int dx = e->pos().x() - _prevX;
    int dy = e->pos().y() - _prevY;
    
    if (  dx > _zoom || dx < -_zoom ) {
      _prevX = e->pos().x();
      update = true;
    }
    if ( dy > _zoom || dy < -_zoom ) {
      _prevY = e->pos().y();
      update = true;
    }
    if ( update )  moveOrigin( _originCol + dx / -_zoom,
			       _originRow + dy / -_zoom );
  }
}

void
View2DWidget::redraw()
{
  if ( ! _document ) return;
  if ( !_volume || ! _document->valid() ) return;
  if ( width() < 10 || height() < 10 ) return;

  UChar * scanLine = _image.scanLine( _topMargin );
  ULong bytesPerLine = _image.scanLine(1) - _image.scanLine(0);
  UChar bytesPerPixel = 4;
  int x, y, xStart;
  QRgb *pPixel;
  QRgb pixel;
  int maxLine = _rows * _zoom ;
  _image.fill( QColor( 20, 100, 100 ).rgb() );
  int inc;
  int dec = 0;
  
  inc = _zoom;
  while ( inc >>= 1 ) dec++;  // Shift to divide by zoom (power of 2).

  UChar *** matrixRed = _volume->data(0);
  UChar *** matrixGreen = _volume->data(0);
  UChar *** matrixBlue = _volume->data(0);
  
  if ( _bands == 3 ) { // True colors mode
    matrixGreen = _volume->data(1);
    matrixBlue = _volume->data(2);
 }
  
  if ( _zoom == 1 ) {
    switch ( _plane ) {
    case PlaneOxy:
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + line;
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  * reinterpret_cast<QRgb*>( scanLine + xStart ) = 
	    QColor( matrixRed[_depth][y][x],
		    matrixGreen[_depth][y][x],
		    matrixBlue[_depth][y][x]).rgb();
	  xStart += bytesPerPixel;
	}
	scanLine += bytesPerLine;
      }
      break;
    case PlaneOxz:
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + (line / _zoom);
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  * reinterpret_cast<QRgb*>( scanLine + xStart ) = 
	    QColor( matrixRed[y][_depth][x],
		    matrixGreen[y][_depth][x],
		    matrixBlue[y][_depth][x]).rgb();
	  xStart += bytesPerPixel;
	}
	scanLine += bytesPerLine;
      }
      break;
    case PlaneOyz:
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + (line / _zoom);
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  * reinterpret_cast<QRgb*>( scanLine + xStart ) = 
	    QColor( matrixRed[y][x][_depth],
		    matrixGreen[y][x][_depth],
		    matrixBlue[y][x][_depth]).rgb();
	  xStart += bytesPerPixel;
	}
	scanLine += bytesPerLine;
      }
      break;
    }      
  } else  if ( _zoom == 2 ) {
    switch ( _plane ) {
    case PlaneOxy:
      inc = bytesPerPixel << 1;
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + ( line >> 1 );
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  pixel = QColor( matrixRed[_depth][y][x],
			  matrixGreen[_depth][y][x],
			  matrixBlue[_depth][y][x]).rgb();
	  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
	  *(pPixel++) = pixel;
	  *(pPixel++) = pixel;
	  xStart += inc;
	}
	scanLine += bytesPerLine;
      }
      break;
    case PlaneOxz:
      inc = bytesPerPixel << 1;
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + (line / _zoom);
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  pixel = QColor( matrixRed[y][_depth][x],
			  matrixGreen[y][_depth][x],
			  matrixBlue[y][_depth][x]).rgb();
	  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
	  *(pPixel++) = pixel;
	  *(pPixel++) = pixel;
	  xStart += inc;
	}
	scanLine += bytesPerLine;
      }
      break;
    case PlaneOyz:
      inc = bytesPerPixel << 1;
      for ( int line = 0 ; line < maxLine ; line++) {
	y = _originRow + (line / _zoom);
	xStart = _leftMargin * bytesPerPixel;
	for ( int col = 0; col < _cols ; col++) {
	  x = _originCol + col;
	  pixel = QColor( matrixRed[y][x][_depth],
			  matrixGreen[y][x][_depth],
			  matrixBlue[y][x][_depth]).rgb();
	  pPixel = reinterpret_cast<QRgb*>( scanLine + xStart );
	  *(pPixel++) = pixel;
	  *(pPixel++) = pixel;
	  xStart += inc;
	}
	scanLine += bytesPerLine;
      }
      break;
    }      
  } else switch ( _plane ) {  // zoom is greater than 2
  case PlaneOxy:
    inc = _zoom * bytesPerPixel;
    for ( int line = 0 ; line < maxLine ; line++) {
      y = _originRow + (line >> dec);
      pPixel = reinterpret_cast<QRgb*>( scanLine + 
					_leftMargin * bytesPerPixel );
      for ( int col = 0; col < _cols ; col++) {
	x = _originCol + col;
	pixel = QColor( matrixRed[_depth][y][x],
			matrixGreen[_depth][y][x],
			matrixBlue[_depth][y][x]).rgb();
	x = _zoom;
	while ( x--) *(pPixel++) = pixel;
      }
      scanLine += bytesPerLine;
    }
    break;
  case PlaneOxz:
    inc = _zoom * bytesPerPixel;
    for ( int line = 0 ; line < maxLine ; line++) {
      y = _originRow + (line / _zoom);
      pPixel = reinterpret_cast<QRgb*>( scanLine + 
					_leftMargin * bytesPerPixel );
      for ( int col = 0; col < _cols ; col++) {
	x = _originCol + col;
	pixel = QColor( matrixRed[y][_depth][x],
			matrixGreen[y][_depth][x],
			matrixBlue[y][_depth][x]).rgb();
	x = _zoom;
	while ( x--) *(pPixel++) = pixel;
      }
      scanLine += bytesPerLine;
    }
    break;
  case PlaneOyz:
    inc = _zoom * bytesPerPixel;
    for ( int line = 0 ; line < maxLine ; line++) {
      y = _originRow + (line / _zoom);
      pPixel = reinterpret_cast<QRgb*>( scanLine + 
					_leftMargin * bytesPerPixel );
      for ( int col = 0; col < _cols ; col++) {
	x = _originCol + col;
	pixel = QColor( matrixRed[y][x][_depth],
			matrixGreen[y][x][_depth],
			matrixBlue[y][x][_depth]).rgb();
	x = _zoom;
	while ( x--) *(pPixel++) = pixel;
      }
      scanLine += bytesPerLine;
    }
    break;
  }
  
  if ( _zoom >=16 ) {
    scanLine = _image.scanLine(_topMargin);
    for ( int r = 0 ; r <= _rows  ; r++ ) {
      memset( scanLine + bytesPerPixel * _leftMargin,
	      0xC0, 
	      _cols * _zoom * bytesPerPixel );
      scanLine += bytesPerLine * _zoom;
    }
    scanLine = _image.scanLine(_topMargin);
    for (int line = 0 ; line < maxLine ; line++) {
      UChar *pc = scanLine + _leftMargin * bytesPerPixel;
      for ( int col = 0; col <= _cols; col++) {
	*(pc++) = 0xC0;
	*(pc++) = 0xC0;
	*(pc++) = 0xC0;
	pc += _zoom * bytesPerPixel - 3;
      }
      scanLine += bytesPerLine;
    }
  }

  QPainter painter( this );
  painter.drawImage( 0, 0, _image );
}

void
View2DWidget::redrawPixel(int /*col*/, int /*row*/)
{

}


void
View2DWidget::setPlane( Plane plane, bool force )
{
  if ( !force && ( plane == _plane ) ) return;
  switch ( _plane ) {
  case PlaneOxy:  
    _z = _depth;
    _originCols[0] = _originCol;
    _originRows[0] = _originRow;
    break;
  case PlaneOxz: 
    _y = _depth;
    _originCols[1] = _originCol;
    _originRows[1] = _originRow;
    break;
  case PlaneOyz:
    _x = _depth;
    _originCols[2] = _originCol;
    _originRows[2] = _originRow;
    break;
  }
  _plane = plane;
  switch ( _plane ) {
  case PlaneOxy:
    _volumeWidth = _volume->width();
    _volumeHeight = _volume->height();
    _volumeDepth = _volume->depth();
    _depth = _z;
    _originCol = _originCols[ 0 ];
    _originRow = _originRows[ 0 ];
    break;
  case PlaneOxz:
    _volumeWidth = _volume->width();
    _volumeHeight = _volume->depth();
    _volumeDepth = _volume->height();
    _depth = _y;
    _originCol = _originCols[ 1 ];
    _originRow = _originRows[ 1 ];
    break;
  case PlaneOyz:
    _volumeWidth = _volume->height();
    _volumeHeight = _volume->depth();
    _volumeDepth = _volume->width();
    _depth = _x;
    _originCol = _originCols[ 2 ];
    _originRow = _originRows[ 2 ];
    break;
  }
  computeViewParams();
  emit maxDepthChanged( _volumeDepth );
  emit planeChanged( static_cast<int>( _plane ) );
  emit depthChanged( _depth );
  redraw();
}

void
View2DWidget::moveOrigin( int col, int row)
{
  bool changed = false;
  if ( col < 0 || row < 0 || col > _maxOriginCol ||  row > _maxOriginRow )  return;
  if ( col != _originCol ) {
    _originCol = col;
    emit originColChanged( col );
    changed = true;
  }
  if ( row != _originRow ) {
    _originRow = row;
    emit originRowChanged( row );
    changed = true;
  }
  if ( changed ) redraw();
}

void
View2DWidget::setDepth( int depth )
{
  if ( depth == _depth || depth > _volumeDepth || _depth < 0 ) return;
  _depth = depth;
  emit depthChanged( _depth );
  redraw();
}

void
View2DWidget::setOriginCol( int col )
{
  if ( col < 0 || col == _originCol || col > _maxOriginCol ) return;
  _originCol = col;
  computeViewParams();
  redraw();
  emit originColChanged( col );
}

void
View2DWidget::setOriginRow( int row )
{
  if ( row < 0 || row == _originRow || row > _maxOriginRow ) return;
  _originRow = row;
  computeViewParams();
  redraw();
  emit originRowChanged( row );
}

void
View2DWidget::copyPlane()
{

}

void
View2DWidget::cutPlane()
{

}

void
View2DWidget::pastePlane()
{

}
