/**
 * @file   ExperimentToolBar.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:22 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <qlabel.h>
#include <qdialog.h>
#include <qlayout.h>

ExperimentToolBar::ExperimentToolBar( QMainWindow * mainWindow ):
  QToolBar( QString( "Experiment" ), mainWindow, Qt::DockBottom )
{
  setHorizontallyStretchable( true );

#ifdef HAVE_QWT
  _plotDialog = new QDialog( dynamic_cast<QMainWindow*>( parent() ) );
  _plotDialog->setGeometry( 10,10, 640, 480 );
  new QVBoxLayout( _plotDialog  );
  _plotDialog->layout()->setAutoAdd( true );

  new QLabel( QString("Curvatures and Averaged curvatures"), _plotDialog );
  _plotC = new QwtPlot("Curvatures", _plotDialog , "curvatureplot" );
  _plotR = new QwtPlot("Radius", _plotDialog , "radiusplot" );

  _curve1 = _plotC->insertCurve("Curvatures");
  _curve2 = _plotC->insertCurve("Averaged curvatures");
  _curve3 = _plotR->insertCurve("Radius");
  _curve4 = _plotR->insertCurve("Averaged Radius");
  _plotC->setCurvePen( _curve2, QColor("red") );
  _plotR->setCurvePen( _curve4, QColor("red") );
#endif

  ( new QLabel( " End:", this ) )->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
  _sbRadiusStop = new QSpinBox( 1, 65535, 1, this, 0 );
  ( new QLabel( " Step:", this )  )->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
  _sbRadiusStep = new QSpinBox( 1, 255, 1, this, 0 );
  QPushButton *pbRefreshSphere 
    = new QPushButton( "Spheres", this );

  _sbRadiusStop->setValue( 50 );

//   ( new QLabel( "Plane:", this ) )->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
//   _sbPlane = new QSpinBox( 0, 255, 1, this, 0 );
//   ( new QLabel( "Band:", this ) )->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
//   _sbBand = new QSpinBox( 0, 255, 1, this, 0 );

//   connect( (new QPushButton("Extract", this) ), SIGNAL( clicked() ),
// 	   this, SLOT( extractBitPlane() ) );

//   connect( (new QPushButton("Ext. Gray", this) ), SIGNAL( clicked() ),
// 	   this, SLOT( extractBitPlaneGray() ) );

  QVBox * vbox = new QVBox( this );

#ifdef HAVE_QWT
  _cbShowGraph = new QCheckBox("Graph", vbox);
  _cbShowGraph->setChecked( true );
  connect( _cbShowGraph, SIGNAL( toggled(bool) ),
	   this, SLOT( showGraph(bool) ) );
#endif

  _cbAutoWrite = new QCheckBox( "Write movie", vbox );

  connect( (new QPushButton("Evolve", this) ), SIGNAL( clicked() ),
	   this, SLOT( evolve() ) );

  connect( (new QPushButton("Evolve2D", this) ), SIGNAL( clicked() ),
	   this, SLOT( evolve2D() ) );

  connect( (new QPushButton("Init Snake", this) ), SIGNAL( clicked() ),
	   this, SLOT( initSnake() ) );

  connect( (new QPushButton("Snake255", this) ), SIGNAL( clicked() ),
	   this, SLOT( snake() ) );


  connect( (new QPushButton("Raz", this) ), SIGNAL( clicked() ),
	   this, SLOT( resetEvolve() ) );
  
  setCloseMode( QDockWindow::Always );
  setOrientation( Qt::Horizontal );
}

void
ExperimentToolBar::setOrientation ( Orientation orientation )
{
  if ( place() == QDockWindow::OutsideDock ) 
    undock();
  QDockWindow::setOrientation( orientation );
}

void
ExperimentToolBar::undock()
{
  QDockWindow::undock();
}

void
ExperimentToolBar::resetEvolve()
{
  _borderMap2D.clear();
  // Evolver2D::image( * ( _view2DWidget->document()->ucVolume() ) );
}

void
ExperimentToolBar::evolve()
{

}


void
ExperimentToolBar::evolve2D()
{  
  Evolver2D evolver2D;
  Long radius = 1;
  Long stop = _sbRadiusStop->value();
  
  bool erased = false;
  
  for  ( ; radius < stop /*&& !erased */ ; radius++ ) {
    if ( ( radius % 25 ) == 0 ) _borderMap2D.clear();
    erased = evolver2D.evolve( *( _document->ucVolume() ),
			       _borderMap2D,
			       false, // topological 
			       10 ,  // Normals Iterations
			       2,    // Averaging
			       1   // Speed
			       );
    
    _document->cast_copy();  
  }
  
}


void
ExperimentToolBar::snake()
{  
  Evolver2D evolver2D;
  Long radius = 1, stop = 10 ;
  bool erased = false;
  
  for  ( ; radius < stop /*&& !erased */ ; radius++ ) {
    if ( ( radius % 25 ) == 0 ) _borderMap2D.clear();
    erased = evolver2D.evolveMask( Evolver2D::image(),
				   _borderMap2D,
				   false, // topological 
				   3 ,  // Normals Iterations
				   1,    // Averaging
				   1   // Speed
				   );    
    *( _document->ucVolume() ) = Evolver2D::image();
    // Mark the countour
    evolver2D.contour().markImage( *( _document->ucVolume() ), 1  );
  }
}

void
ExperimentToolBar::initSnake()
{  
  Evolver2D evolver2D;
  ZZZImage<UChar> mask;
  mask = *(_document->ucVolume());
  mask.clear();
  
  Evolver2D::mask( mask );

  Contour c;
  
  c.setImage( mask, 10, 5 );
  c.markImage( *( _document->ucVolume() ), 1  );
  _document->cast_copy();
  _document->notify();
}

