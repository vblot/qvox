TEMPLATE	= app
LANGUAGE	= C++

CONFIG	+= qt warn_on debug

LIBS	+= ../libs/libsurface.a ../libs/libzzztools.a ../libs/libtools.a

INCLUDEPATH	+= .. ./include ../tools/include ../zzztools/include ../surface/include

HEADERS	+= include/View2DWidget.h \
	include/ExperimentToolBar.h \
	include/Dialogs.h \
	include/Document.h

SOURCES	+= src/snake.cpp \
	src/View2DWidget.cpp \
	src/ExperimentToolBar.cpp \
	src/Dialogs.cpp \
	src/Document.cpp

FORMS	= ui/mainwindow.ui

IMAGES	= Pixmaps/exit.png \
	Pixmaps/filenew.png \
	Pixmaps/fileopen.png \
	Pixmaps/filesaveas.png \
	Pixmaps/filesave.png \
	Pixmaps/zoomfit.png \
	Pixmaps/zoomin.png \
	Pixmaps/zoomout.png

QMAKE_CXXFLAGS_RELEASE += -ffast-math -O3
QMAKE_CXXFLAGS_DEBUG += -ffast-math -O3

# QMAKE_CXXFLAGS_DEBUG +=  -pg
# QMAKE_LFLAGS_DEBUG +=  -pg

DEPENDPATH += ../libs
PRE_TARGETDEPS += ../libs/libsurface.a ../libs/libzzztools.a ../libs/libtools.a
DEPENDPATH += ./include
# QMAKE_LIBS += ../libs/libtools.a ../libs/libzzztools.a ../libs/libsurface.a

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
