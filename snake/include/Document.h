/**
 * @file   Document.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:32:05 2005
 * 
 * @brief  Volume document class.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#define DOCUMENT_H

#include <tools.h>
#include <qobject.h>
#include <pobject.h>
#include <zzzimage.h>
#include <distance_transform.h>
#include <skel.h>
#include <qimage.h>
#include <qfileinfo.h>
#include <Triple.h>
#include <map>
#include <vector>

class Document : public QObject {
  Q_OBJECT;

 public:
  Document();
  ~Document();

  bool modified() { return _modified; }
  void save();
  void saveAs( const QString & filename );
  int valueType() { return _valueType; }
  int bands() { return _volume->bands(); }

  bool valid() { return _valid; }

  ZZZImage<UChar> * volume() { return _volume; };
  ZZZImage<UChar> * ucVolume() { return _ucVolume; };
  ZZZImage<Short> * ssVolume() { return _ssVolume; };
  ZZZImage<Long>  * slVolume() { return _slVolume; };
  ZZZImage<ULong>  * ulVolume() { return _ulVolume; };
  ZZZImage<Float> * sfVolume() { return _sfVolume; };
  void cast_copy();
  void equalization( bool on );
  bool equalization() { return _equalization; }
  void qvoxLogo();
  PObject * object() { return _object; }
  const char * fileName() { return _fileName; }
  void notify() { valid( true ); emit volumeChanged(); }

  

  /** 
   * Change the color of a voxel in the volume.
   * 
   * 
   * @param x 
   * @param y 
   * @param z 
   * @param red 
   * @param green 
   * @param blue 
   * @param refresh 
   * 
   * @return true if the change created or removed a voxel (i.e. with non zero value). 
   */
  bool setVoxelValue( Short x, Short y, Short z,
		      UChar red, UChar green, UChar blue,
		      bool refresh = true );

  void create( Short width, Short height, Short depth,
	       Short bands,
	       ValueType valueType );

  void histo3D( ProgressFunction p = 0 );
  void fitBoundingBox( Short margin = 0 );
  void trim( int plane, int n );

  void raiseLevelMap( Short maxHeight = 255, ProgressFunction progress = 0  );

  void mirror( int axis );
  void distanceMap();
  void invert( UChar value = 255 );
  void binarize( UChar value = 255 );
  void toGray();
  void subSample( UChar minCount = 4 );
  void scale( UChar direction, UChar factor = 2 );
  void extrude( Short depth );
  void fill();
  void bitPlane( UChar bit, Long band );
  void bitPlaneGrayCode( UChar bit, Long band );
  void quantize( ULong cardinality );
  
  void sphere( Long x, Long y, Long z, Long radius );


  void skel( bool singlePass, int adjacency, int method, int terminalTest ); 
  
  void threshold( Triple<Long> min, 
		  Triple<Long> max, 
		  std::vector< Triple<Long> > & removedValues );
  void threshold( Triple<ULong> min, 
		  Triple<ULong> max, 
		  std::vector< Triple<ULong> > & removedValues );
  void threshold( Triple<Float> min, 
		  Triple<Float> max, 
		  std::vector< Triple<Float> > & removedValues );

  bool load( const char * filename, ProgressFunction p = 0 );
  bool merge( const char * filename, int axis, ProgressFunction p = 0 );
  bool loadRaw( const char * filename, ProgressFunction p = 0 );

  unsigned long rawSize();

  bool loadPixmap( const char * filename, ProgressFunction p = 0 );
  bool loadablePixmap( const char * filename );
   
 signals:
 
  void volumeChanged();
  void volumeContentChanged();
  
 private: 

  void clear();
  void valid( bool on ) { _valid = on; }
  
  ZZZImage<UChar> *_volume;
  ZZZImage<UChar> *_ucVolume;
  ZZZImage<Short> *_ssVolume;
  ZZZImage<Long>  *_slVolume;
  ZZZImage<ULong>  *_ulVolume;
  ZZZImage<Float> *_sfVolume;  
  PObject *_object;
  QString _fileName;
  bool _modified;
  bool _equalization;
  int _valueType;
  bool _valid;
};

template< typename T >
void realignment( ZZZImage<UChar> & result,
		  ZZZImage<T> & source );

template< typename T >
void equalization( ZZZImage<UChar> & result,
		   ZZZImage<T> & source );


void realignmentDistanceMap( ZZZImage<UChar> & result,
			     ZZZImage<Long> & source );

template< typename T >
void cast_copy( ZZZImage<UChar> & result,
		ZZZImage<T> & source );


#endif

