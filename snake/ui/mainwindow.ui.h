/**
 * @file   mainwindow.ui.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:29:08 2005
 * 
 * @brief  Mainwindow slots and callbacks.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 ** ui.h extension file, included from the uic-generated form implementation.
 **
 ** If you want to add, delete, or rename functions or slots, use
 ** Qt Designer to update this file, preserving your code.
 **
 ** You should not define a constructor or destructor in this file.
 ** Instead, write your code in functions called init() and destroy().  if ( _document ) 
    disconnect( _document, SIGNAL( volumeChanged() ),
		this, SLOT( volumeChanged() ) );
 ** These will automatically be called by the form's constructor and
 ** destructor.
 *****************************************************************************/
void MainWindow::destroy()
{
  dispose( _document );
}

void MainWindow::init() {

  _dialogs = new Dialogs( this );
  _experimentToolBar = new ExperimentToolBar( this );
  
  connect( zoomOutAction, SIGNAL(activated()), view2DWidget, SLOT(zoomOut()) );
  connect( zoomInAction, SIGNAL(activated()), view2DWidget, SLOT(zoomIn()) );
  connect( zoomFitAction, SIGNAL(activated()), view2DWidget, SLOT(zoomFit()) );
  
  _toolBarsMenu = new QPopupMenu( this );
  _toolBarsMenu->setCheckable( TRUE );
  connect( _toolBarsMenu, SIGNAL( aboutToShow() ),
	   this, SLOT( toolBarsMenuAboutToShow() ) );
  menuBar()->insertItem( tr( "&Toolbars" ), _toolBarsMenu, -1, 3 );

  connect( _toolBarsMenu, SIGNAL( activated(int) ),
	   this, SLOT( toolBarsMenuActivated(int) ) );

  QPopupMenu * fileMenu = new QPopupMenu( this );
  fileOpenAction->addTo( fileMenu );
  fileImportAction->addTo( fileMenu );
  fileMenu->insertSeparator();
  fileExitAction->addTo( fileMenu );
  menuBar()->insertItem( tr( "&File" ) , fileMenu, -1, 0 );
  setDocument( new Document );
  updateCaption();
}

void MainWindow::fileOpen()
{
  QString s =  _dialogs->askOpenFileName( VolumeFile );
  if ( s != QString::null )  openFile( s );
  return;
}

void MainWindow::fileExit()
{
  if ( _document->modified() && ! _dialogs->confirmDoNotSave( _document->fileName(), this ) )
    return;
  application->quit();
}

void MainWindow::setApplication( QApplication * application)
{
  MainWindow::application = application;
}


void MainWindow::setDocument( Document * document )
{
  _document = document;
  if ( _document ) 
    connect( _document, SIGNAL( volumeChanged() ),
	     this, SLOT( volumeChanged() ) );
}

Document * MainWindow::document()
{
  return _document;
}



void MainWindow::show()
{
  bool logo = false;
  QMainWindow::show();

  if ( _fileName != QString::null) {
    application->processEvents();
    if ( _document->loadablePixmap( _fileName.ascii() ) )
      _document->loadPixmap( _fileName.ascii(), 0 );
    else
      if ( ! _document->load( _fileName.ascii(), 0 ) ) {
	_dialogs->errorOpening( _fileName.ascii() );
	_document->qvoxLogo();
	logo = true;
	_fileName = QString::null;
      }
    updateCaption();
  } else  { 
    _document->qvoxLogo();
    logo = true;
    _fileName = QString::null;
  }

  view2DWidget->setDocument( _document );
  view2DWidget->zoomFit();
}

void MainWindow::setFileName( const QString & fileName )
{
  _fileName = fileName;
}


void MainWindow::toolBarsMenuAboutToShow()
{
  _toolBarsMenu->clear();
  _toolBarsMenu->setCheckable( true ); 
  _toolBarsMenu->insertItem( "Show all", 0 );
  _toolBarsMenu->insertItem( "Hide all", 1 );
  _toolBarsMenu->insertSeparator();

  QPtrList<QDockWindow> dwList = dockWindows();
  for ( unsigned int i = 0 ; i < dwList.count(); i++ ) {
    int id = _toolBarsMenu->insertItem( dwList.at( i )->caption(), 2 + i );
    if ( dwList.at( i )->isShown() ) {
      _toolBarsMenu->setItemChecked( id, true );
    }
  }
}


void MainWindow::toolBarsMenuActivated( int i )
{
  QPtrList<QDockWindow> list = dockWindows();
  int n = list.count();
  switch ( i ) {
  case 0:
    for ( int i = 0; i < n; i++ )
      list.at( i )->show();
    break;
  case 1:
    for ( int i = 0; i < n; i++ )
      list.at( i )->hide();
    break;
  }

  if ( i > 1 ) {
    bool shown = dockWindows().at( i - 2)->isShown();
    if ( shown ) 
      dockWindows().at( i - 2)->hide();
    else
      dockWindows().at( i - 2)->show();
  }
}

void MainWindow::closeEvent( QCloseEvent * e )
{
  if ( _document->modified() && ! _dialogs->confirmDoNotSave( _document->fileName(), this ) ) { 
    e->ignore();
    return;
  }
  QMainWindow::closeEvent( e );
}



void MainWindow::fileImport()
{
  if ( _document->modified() && ! _dialogs->confirmDoNotSave( _document->fileName(), this ) )
    return;

  QStrList formats = QImage::inputFormats();
  QString fileName;
  QString filter = "2D Pixmaps (";
  char * strType;
  
  strType = formats.first();
  while ( strType ) {
    filter += QString("*.") + QString(strType).lower() + " ";
    if ( QString(strType).lower() == "jpeg" ) 
      filter += QString("*.jpg ");
    strType = formats.next();
  }
  filter.truncate( filter.length() - 1 );
  filter += ")";
  
  fileName = QFileDialog::getOpenFileName( QString::null,
					   filter,
					   this,
					   "open file dialog",
					   "Choose an image file to open" );
  
  if ( fileName != QString::null ) {
    progress( "Importing file..." );
    application->processEvents();
    if ( ! _document->loadPixmap( fileName.ascii(), 0 ) )
      _dialogs->errorOpening( _fileName.ascii() );
    progress( QString::null );
    updateCaption();
  }
}







void MainWindow::volumeMirror( int id )
{
  application->processEvents();
  progress("Mirroring...");
  _document->mirror( id );
}

void MainWindow::volumeChanged()
{

}



void MainWindow::openFile( QString fileName )
{
  bool ok; 
  bool rawFile = false;
  progress( "Loading volume..." );
  application->processEvents();
  if ( rawFile ) 
    ok = _document->loadRaw( fileName.ascii(), 0 );
  else
    ok = _document->load( fileName.ascii(), 0 );

  if ( ok ) {
    updateCaption();
  } 
  else  _dialogs->errorOpening( _fileName.ascii() );
}

void MainWindow::updateCaption()
{
  QString name = "No name";
  if ( _document->fileName() != QString::null ) 
    name = QFileInfo( _document->fileName() ).fileName();
  setCaption( QString( "QVox " VERSION_STRING " [%1]")
	      .arg( name ) );
}

void MainWindow::progress( const QString & s )
{
  statusBar()->message( s );
}
