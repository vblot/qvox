/** -*- mode: c++ -*-
 * @file   ThreadedSurfacePainter.h
 * @author Sebastien Fourey (GREYC)
 * @date
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "globals.h"
#include <QPainter>
#include "ThreadedSurfacePainter.h"
#include "QThreadPool"
#include "SurfacePainterThread.h"
#include "Settings.h"

ThreadedSurfacePainter::ThreadedSurfacePainter()
{
  if ( globalSettings->maxThreads() == -1 )
    _images.resize(QThreadPool::globalInstance()->maxThreadCount());
  else
    _images.resize(globalSettings->maxThreads());
  std::vector< QImage* >::iterator it = _images.begin();
  while (it != _images.end()) {
    *it++ = new QImage(2,2,QImage::Format_ARGB32_Premultiplied);
  }
}

ThreadedSurfacePainter::~ThreadedSurfacePainter()
{
  std::vector< QImage* >::iterator it = _images.begin();
  while (it != _images.end()) {
    delete *it++;
  }
}

void ThreadedSurfacePainter::resize( const QSize & size )
{
  std::vector< QImage* >::iterator it = _images.begin();
  while (it != _images.end()) {
    *(*it++) = QImage(size,QImage::Format_ARGB32_Premultiplied);
  }
}

void ThreadedSurfacePainter::draw(AbstractSurfacePainter * painter, QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth)
{
  unsigned int threads = ( globalSettings->maxThreads() == -1 ) ? QThreadPool::globalInstance()->maxThreadCount() : globalSettings->maxThreads();
  while ( threads < _images.size() ) {
    delete _images.back();
    _images.pop_back();
  }
  while ( _images.size() < threads ) {
    _images.push_back(new QImage(image.size(),QImage::Format_ARGB32_Premultiplied));
  }
  if ( image.size() != _images.front()->size() )
    resize(image.size() );

  const unsigned long count = stop - start;
  const unsigned long interval = count / _images.size();
  for ( std::vector<QImage*>::size_type t = 0; t < _images.size() - 1; ++t ) {
    Surfel ** tStart = start + t*interval;
    Surfel ** tStop = tStart + interval;
    SurfacePainterThread * thread = new SurfacePainterThread(painter,*(_images[t]),colormap,tStart,tStop,minDepth);
    QThreadPool::globalInstance()->start(thread);
  }
  Surfel ** tStart = start + (_images.size()-1)*interval;
  SurfacePainterThread * thread = new SurfacePainterThread(painter,*(_images.back()),colormap,tStart,stop,minDepth);
  QThreadPool::globalInstance()->start(thread);
  QThreadPool::globalInstance()->waitForDone();

  QPainter p(&image);
  for ( std::vector<QImage*>::iterator it = _images.begin(); it != _images.end(); ++it ) {
    p.drawImage(0,0,*(*it));
  }
}


