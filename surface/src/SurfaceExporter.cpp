/**
 * @file   SurfaceExporter.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:00 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "SurfaceExporter.h"

#include "Surface.h"
#include "Vertex.h"
#include "tools.h"
#include "Triangle.h"
#include "Convolution.h"
#include <ColorMap.h>
#include <vector>
#include <algorithm>
#include <ctime>
#include <Board.h>

using std::vector;

namespace {
int
pSurfelZCompare(const Surfel **a, const Surfel **b)
{
  if ( (*a)->rotatedCenter.third < (*b)->rotatedCenter.third)
    return 1;
  return -1;
}
}

inline
double rounded( double x, double prec = 100000.0 )
{
  return floor( prec * x + 0.5 ) / prec;
}

SurfaceExporter::SurfaceExporter( Surface & surface, const ColorMap & colorMap )
  :_surface( surface ), _colorMap( colorMap )
{
  CONS( SurfaceExporter );
}

double
SurfaceExporter::transformX( double x )
{
  return rounded( x * _scale + _deltaX );
}

double
SurfaceExporter::transformY( double y )
{
  return rounded(  ( _top - ( y * _scale) )  + _deltaY );
}

void
SurfaceExporter::transform( double & x, double & y )
{
  x = rounded( x * _scale + _deltaX );
  y = rounded( ( _top - ( y * _scale) ) + _deltaY );
}

void
SurfaceExporter::setEPSViewPort( std::ofstream & file,
                                 double left, double top,
                                 double width, double height )
{
  _left = left;
  _top = top;
  _height = height;
  _width = width;
  double ppmm = (720/254.0);
  if ( height/width > 27.7/19.0 ) {
    _scale = 277 * ppmm / height;
  } else {
    _scale = 190 * ppmm / width;
  }
  _deltaX = 0.5 * 210 * ppmm - _scale * ( left + width / 2.0 );
  _deltaY = 0.5 * 297 * ppmm - _scale * ( top - height / 2.0 );

  file << "%%BoundingBox: " << std::setprecision( 8 )
       << transformX( left ) << " "
       << transformY( top ) << " "
       << transformX( left + width ) << " "
       << transformY( top - height ) << std::endl;

  file <<  "%% Boundary box (uncomment)\n"
        << "/drawbbox { "
        << transformX(left) << " " << transformY(top) << " "
        << transformX(left+width) << " " << transformY(top) << " "
        << transformX(left+width) << " " << transformY(top-height) << " "
        << transformX(left) << " " << transformY(top-height) << " "
        << " n m l l l cp 0.5 0.5 0.5 srgb s } bind def\n";
}

bool
SurfaceExporter::writeEPS( const char * fileName, bool /* axis  */)
{
  SurfelList & surfelList = _surface.surfelList();
  std::ofstream file( fileName );
  if ( !file )  return false;

  double x_min, y_min;
  double x_max, y_max;
  double m_x, m_y;

  surfelList.computeBBox( true, SurfelList::RotatedPosition );

  x_min = surfelList.bbMin().first;
  y_min = surfelList.bbMin().second;
  x_max = surfelList.bbMax().first;
  y_max = surfelList.bbMax().second;
  m_x = m_y = 0;
  for ( int direction = 0; direction < 6; direction++ )
    for ( int vertex = 0; vertex < 4; vertex++ ) {
      maximize( m_x, fabs( _surface._rotatedFaces[direction][vertex].first) );
      maximize( m_y, fabs( _surface._rotatedFaces[direction][vertex].second) );
    }
  file << "%!PS-Adobe-2.0\n";
  file << "%%Title: " << baseName( fileName ) << std::endl;
  file << "%%Creator: QVox (c) 2003-2063 Sebastien Fourey"
       << " - GREYC/ENSICAEN <Firstname.Lastname@greyc.ensicaen.fr>\n";

  time_t t = time( 0 );
  file << "%%CreationDate: " << ctime( &t );

  setEPSViewPort( file,
                  x_min-m_x, y_max+m_y,
                  2*m_x + x_max - x_min,
                  2*m_y + y_max - y_min );

  file <<  "%%Pages: 1\n";
  file <<  "%%Magnification: 1.0000\n";
  file <<  "%%EndComments\n\n";
  file << "/cp {closepath} bind def" << std::endl;
  file << "/ef {eofill} bind def" << std::endl;
  file << "/gr {grestore} bind def" << std::endl;
  file << "/gs {gsave} bind def" << std::endl;
  file << "/sa {save} bind def" << std::endl;
  file << "/rs {restore} bind def" << std::endl;
  file << "/l {lineto} bind def" << std::endl;
  file << "/m {moveto} bind def" << std::endl;
  file << "/rm {rmoveto} bind def" << std::endl;
  file << "/n {newpath} bind def" << std::endl;
  file << "/s {stroke} bind def" << std::endl;
  file << "/sh {show} bind def" << std::endl;
  file << "/slc {setlinecap} bind def" << std::endl;
  file << "/slj {setlinejoin} bind def" << std::endl;
  file << "/slw {setlinewidth} bind def" << std::endl;
  file << "/srgb {setrgbcolor} bind def" << std::endl;
  file << "/sg {setgray} bind def\n";
  file << "/rot {rotate} bind def" << std::endl;
  file << "/sc {scale} bind def" << std::endl;
  file << "/sd {setdash} bind def" << std::endl;
  file << "/ff {findfont} bind def" << std::endl;
  file << "/sf {setfont} bind def" << std::endl;
  file << "/scf {scalefont} bind def" << std::endl;
  file << "/sw {stringwidth} bind def" << std::endl;
  file << "/tr {translate} bind def" << std::endl;
  file <<  "/slclj {dup setlinecap setlinejoin} bind def" << std::endl;
  file <<  "/BottomLeftX { " << transformX(0.0) << "} def" << std::endl;
  file <<  "/BottomLeftY { " << transformY(0.0) << "} def" << std::endl;

  file <<  "%%begin(plot)\n";
  file << " 0.5 setlinewidth\n";
  file <<  "0 setlinecap\n0 setlinejoin\n";
  file <<  "0.0 0.0 0.0 setrgbcolor 0 slw\n";

  writeEPSColorMap( file );
  writeEPSSurfels( file );
  //  if ( axis ) writeEPSAxis( file, scale );

  file << " drawbbox\n";
  file <<  "%%end(plot)\n\n";
  file.close();
  return true;
}

void
SurfaceExporter::writeEPSColorMap( std::ofstream & file )
{
  const QColor * colors = _colorMap.qcolors();

  file << "/fblanche {n m l l l cp gs 1.0 setgray 0 slw fill gr stroke} "
          "bind def\n";
  file << std::endl << std::endl;

  if ( _surface.image().bands() == 1 ) {
    if ( _surface._edges )
      for (int i = 0; i < 256; i++ )
        file << "/f" << i << " {n m l l l  cp gs "
             << rounded( colors[i].red() / 255.0, 1000 ) << " "
             << rounded( colors[i].green() / 255.0, 1000 ) << " "
             << rounded( colors[i].blue() / 255.0, 1000 )
             << " setrgbcolor 0 slw fill gr stroke } bind def\n";
    else
      for (int i = 0; i < 256; i++ )
        file << "/f" << i << " {n m l l l  cp gs "
             << rounded( colors[i].red() / 255.0, 1000 ) << " "
             << rounded( colors[i].green() / 255.0, 1000 ) << " "
             << rounded( colors[i].blue() / 255.0, 1000 )
             << " setrgbcolor 0 slw fill gr } bind def\n";
  }

  if ( _surface._edges )
    file << "/tcface {n m l l l cp gs srgb 0 slw fill gr stroke} bind def \n";
  else
    file << "/tcface { n m l l l cp gs srgb 0 slw fill gr } bind def \n";

  file << "/cross {gs 0 slw 0 slclj 8 copy n m pop pop l pop pop cp"
       << "        stroke n pop pop m pop pop l cp stroke gr} bind def\n";
  file << std::endl << std::endl;
}

void
SurfaceExporter::writeEPSSurfels( std::ofstream & file )
{
  double x, y;
  int direction;
  Surfel *ps;
  Surfel **pps, **end;
  pps = _surface.sortedSurfelsArray();
  end = _surface.sortedSurfelsEnd();

  int color;
  int n = 0;

  vector<Surfel*> pSurfelArray;
  pSurfelArray.resize( end - pps );
  memcpy( &(pSurfelArray[0]),
      pps,
      (end - pps)*sizeof(Surfel*) );

  typedef int (*SortFunction)(const void*,const void*);
  qsort( &(pSurfelArray[0]), end - pps, sizeof(pSurfel),
      reinterpret_cast< SortFunction >( & ::pSurfelZCompare ) );

  pps = end = 0;
  if ( !pSurfelArray.empty() ) {
    pps = &(pSurfelArray.front());
    end = 1 + &(pSurfelArray.back());
  }
  if ( _surface.image().bands() == 1 )
    while ( pps != end ) {
      n++;
      ps = *pps;
      x = ps->rotatedCenter.first;
      y = ps->rotatedCenter.second;
      direction = ps->direction;
      for ( int vertex = 0; vertex < 4; ++vertex ) {
        file << transformX( x + _surface._rotatedFaces[direction][vertex].first );
        file << " ";
        file << transformY( y + _surface._rotatedFaces[direction][vertex].second );
        file << " ";
      }
      color = _surface.surfelColor( ps );
      file << " f" << color << "\n";
      ++pps;
    }
  else
    while ( pps != end ) {
      n++;
      ps = *pps;
      x = ps->rotatedCenter.first;
      y = ps->rotatedCenter.second;
      direction = ps->direction;
      Triple<float> color = _surface.getSurfelColor( ps, _colorMap );
      file << rounded( color.first, 1000 ) << " "
           << rounded( color.second, 1000 ) << " "
           << rounded( color.third, 1000 ) << " ";
      for (int vertex = 0; vertex < 4; vertex++ ) {
        file << transformX( x + _surface._rotatedFaces[direction][vertex].first );
        file << " ";
        file << transformY( y + _surface._rotatedFaces[direction][vertex].second );
        file << " ";
      }
      file << " tcface\n";
      pps++;
    }
}

void
SurfaceExporter::writeEPSAxis( std::ofstream & /* file */, double /* scale */ )
{
  return;

  // int zoom = 20;
  // int fontSize;

  // return;
  // QColor lineColor;

  // QColor backgroundColor = _colorMap.backgroundColor();

  // lineColor.setRgb( ~ ( backgroundColor.rgb() ) );
  // double red = rounded( lineColor.red()  / 255.0, 1000 );
  // double green = rounded( lineColor.green()  / 255.0, 1000 );
  // double blue = rounded( lineColor.blue()  / 255.0, 1000 );

  // file << " -297.64 -420.94 translate  " ;
  // Triple<double> normal = _surface.rotatedNormal( 0 );

  // fontSize = static_cast<int>( 12 - 6 * normal.third );

  // file << " BottomLeftX " << scale << " 2 mul add ";
  // file << " BottomLeftY " << scale << " 2 mul add  n m ";
  // file <<  ( zoom * normal.first) << " " <<  ( zoom * normal.second ) << " rlineto cp ";
  // file << red << " " << green << " " << blue << " setrgbcolor 0 slw s\n ";
  // file << "/Helvetica findfont " <<  ( 12 - 2 * normal.third ) << " scalefont setfont \n";
  // file << " BottomLeftX " << scale << " 2 mul add " << ( (zoom + 10) * normal.first) << " add ";
  // file << " BottomLeftY " << scale << " 2 mul add " << ( (zoom + 10) * normal.second ) << " add ";
  // file << " m (x) show ";

  // normal = _surface.rotatedNormal( 2 );
  // fontSize = static_cast<int>( 12 - 6 * normal.third );

  // file << " BottomLeftX " << scale << " 2 mul add ";
  // file << " BottomLeftY " << scale << " 2 mul add  n m ";
  // file <<  ( zoom * normal.first ) << " " <<  ( zoom * normal.second ) << " rlineto cp ";
  // file << red << " " << green << " " << blue << " setrgbcolor 0 slw s\n ";
  // file << "/Helvetica findfont " <<  ( 12 - 2 * normal.third ) << " scalefont setfont \n";
  // file << " BottomLeftX " << scale << " 2 mul add " << ( (zoom + 10) * normal.first ) << " add ";
  // file << " BottomLeftY " << scale << " 2 mul add " << ( (zoom + 10) * normal.second ) << " add ";
  // file << " m (y) show ";

  // normal = _surface.rotatedNormal( 4 );
  // file << " BottomLeftX " << scale << " 2 mul add ";
  // file << " BottomLeftY " << scale << " 2 mul add  n m ";
  // file <<  ( zoom * normal.first ) << " " <<  ( zoom * normal.second ) << " rlineto cp ";
  // file << red << " " << green << " " << blue << " setrgbcolor 0 slw s\n ";
  // file << "/Helvetica findfont " <<  ( 12 - 2 * normal.third ) << " scalefont setfont \n";
  // file << " BottomLeftX " << scale << " 2 mul add " << ( (zoom + 10) * normal.first ) << " add ";
  // file << " BottomLeftY " << scale << " 2 mul add " << ( (zoom + 10) * normal.second ) << " add ";
  // file << " m (z) show ";
}

bool
SurfaceExporter::writeSurfelsOFF( const char *fileName )
{
  globalProgressReceiver.pushInterval(4);

  Size nbVertices = _surface.buildSurfelVertexIndicesArray();
  const vector<Surface::SurfelVertexIndices> & surfelVertexIndices = _surface.surfelVertexIndicesArray();
  vector< Triple<double> > vertices( nbVertices );

  Surfel * array = _surface.surfelList().begin();
  Surfel * ps = array;
  Surfel * end = _surface.surfelList().end();

  globalProgressReceiver.setProgress(1);

  while ( ps != end ) {
    for ( int vertex = 0; vertex < 4; vertex++ ) {
      vertices[ surfelVertexIndices[ ps - array ].indices[ vertex ] ] =
          ps->center + _surface._referenceFaces[ ps->direction ][ vertex ];
    }
    ++ps;
  }

  globalProgressReceiver.setProgress(2);

  std::ofstream file( fileName );
  if ( !file ) return false;

  file << "OFF\n";
  file << vertices.size() << " "
       << _surface.surfelList().size() << " "
       << (_surface.surfelList().size() * 2)
       << "\n\n";

  vector< Triple<double> >::iterator it = vertices.begin();
  while ( it != vertices.end() ) {
    file << it->first << " "
         << it->second << " "
         << it->third << "\n";
    ++it;
  }

  globalProgressReceiver.setProgress(3);

  ps = array;
  while ( ps != end ) {
    file << "4 ";
    for ( int v=0; v < 4; ++v )
      file << surfelVertexIndices[ ps - array ].indices[ v ] << " ";
    Triple<float> color = _surface.getSurfelColor( ps, _colorMap );
    file << color.first << " " << color.second << " " << color.third << "\n";
    ps++;
  }

  globalProgressReceiver.setProgress(4);

  globalProgressReceiver.popInterval();

  return true;
}

bool
SurfaceExporter::writeOBJ( const char *fileName,
                           int iterations )
{
  globalProgressReceiver.pushInterval(3);

  SurfelDataArray centers;
  _surface.convolution421Centers( iterations, centers );

  Size nbVertices = _surface.buildSurfelVertexIndicesArray();
  vector<bool> visitedLoops( nbVertices );

  globalProgressReceiver.setProgress(1);

  vector<Triangle> triangles;
  HashVertices vertices( ( _surface.surfelList().size() + nbVertices ) * 2 );
  Surfel *ps = _surface.surfelList().begin();
  Surfel *end = _surface.surfelList().end();
  while ( ps != end )
    loopsToTriangles( triangles, ps++, centers,
                      _surface, _colorMap,
                      visitedLoops );


  vector<Triangle>::iterator pt = triangles.begin();
  vector<Triangle>::iterator tend = triangles.end();
  while ( pt != tend ) {
    Vertex s;
    if ( pt->valid ) {
      vertices.add( pt->vertex0 );
      vertices.add( pt->vertex1 );
      vertices.add( pt->vertex2 );
    }
    ++pt;
  }

  globalProgressReceiver.setProgress(2);

  std::ofstream file( fileName );
  if ( !file ) return false;

  file << "# Vertices\n";
  vertices.dump( file );

  file << "# Faces\n";
  pt = triangles.begin();
  while ( pt != tend ) {
    Vertex * pv;
    if ( pt->valid ) {
      pv = vertices.find( pt->vertex0 );
      file << "f " << pv->index;
      pv = vertices.find( pt->vertex1 );
      file << " "  << pv->index;
      pv = vertices.find( pt->vertex2 );
      file << " "  << pv->index << "\n";
    }
    pt++;
  }
  file.close();

  globalProgressReceiver.setProgress(3);
  globalProgressReceiver.popInterval();

  TRACE << "[" << triangles.size() << "] triangles\n";
  return true;
}

bool
SurfaceExporter::writeOFF( const char *fileName,
                           int iterations,
                           bool adaptive,
                           bool fixedColor,
                           float red, float green, float blue)
{
  globalProgressReceiver.pushInterval(4);

  SurfelDataArray centers;

  if ( adaptive )
    _surface.convolution421CentersAdaptive( iterations, centers );
  else
    _surface.convolution421Centers( iterations, centers );

  globalProgressReceiver.setProgress(1);

  Size nbVertices = _surface.buildSurfelVertexIndicesArray();
  vector<bool> visitedLoops( nbVertices );
  vector<Triangle> triangles;
  HashVertices vertices( ( _surface.surfelList().size() + nbVertices ) * 2 );
  Surfel *ps = _surface.surfelList().begin();
  Surfel *end = _surface.surfelList().end();
  while ( ps != end )
    loopsToTriangles( triangles,
                      ps++,
                      centers,
                      _surface,
                      _colorMap,
                      visitedLoops );
  visitedLoops.clear();

  globalProgressReceiver.setProgress(2);

  Triple<double> aspect = _surface.aspect();
  if ( aspect.first <= 0.0 )
    aspect = 1.0;
  vector<Triangle>::iterator pt = triangles.begin();
  vector<Triangle>::iterator tend = triangles.end();
  while ( pt != tend ) {
    if ( pt->valid ) {
      vertices.add( scale( pt->vertex0, aspect ) );
      vertices.add( scale( pt->vertex1, aspect ) );
      vertices.add( scale( pt->vertex2, aspect ) );
    }
    ++pt;
  }

  globalProgressReceiver.setProgress(3);

  std::ofstream file( fileName );
  if ( !file ) return false;

  file << "OFF\n";
  file << vertices.size() << " "
       << triangles.size() << " "
       << ( triangles.size() * 3 ) / 2
       << "\n";

  vertices.dumpOFF( file );

  TRACE << "[" << triangles.size() << "] triangles\n";
  pt = triangles.begin();
  while ( pt != tend ) {
    Vertex *pv;
    if ( pt->valid ) {
      pv = vertices.find( scale( pt->vertex0, aspect ) );
      file << "3 " << pv->index - 1;
      pv = vertices.find( scale( pt->vertex1, aspect ) );
      file << " "  << pv->index - 1;
      pv = vertices.find( scale( pt->vertex2, aspect ) );
      file << " "  << pv->index - 1;
      if ( fixedColor )
        file << " " << red << " " << green << " " << blue << std::endl;
      else
        file << " " << pt->red() << " " << pt->green() << " " << pt->blue() << std::endl;
    }
    ++pt;
  }
  file.close();

  globalProgressReceiver.setProgress(4);
  globalProgressReceiver.popInterval();
  return 1;
}

bool
SurfaceExporter::writeSmoothedEPS( int iterations,
                                   unsigned char gouraudDivisions,
                                   const char * fileName )
{
  using LibBoard::Board;
  vector<Triangle> triangles;
  SurfelDataArray centers;

  _surface.convolution421Centers( iterations, centers );

  Size nbVertices = _surface.buildSurfelVertexIndicesArray();
  vector<bool> visitedLoops( nbVertices );
  Surfel *ps = _surface.surfelList().begin();
  Surfel *end = _surface.surfelList().end();
  while ( ps != end )
    loopsToTriangles( triangles, ps++, centers,
                      _surface, _colorMap,
                      visitedLoops );

  Triple<double> aspect = _surface.aspect();
  if ( aspect.first <= 0.0 )
    aspect = 1.0;


  const Matrix rotation = _surface.rotation();
  Triple<double> normal;
  vector<Triangle> orderedTriangles;
  vector<Triangle>::iterator pt = triangles.begin();
  vector<Triangle>::iterator tend = triangles.end();
  while ( pt != tend ) {
    pt->vertex0 = rotation * scale( pt->vertex0, aspect );
    pt->vertex1 = rotation * scale( pt->vertex1, aspect );
    pt->vertex2 = rotation * scale( pt->vertex2, aspect );
    normal = wedgeProduct( pt->vertex1 - pt->vertex0, pt->vertex2 - pt->vertex0 );
    // Face culling
    if ( normal.third < 0.0 ) {
      orderedTriangles.push_back( *pt );
    }
    ++pt;
  }
  triangles.clear();
  stable_sort( orderedTriangles.begin(), orderedTriangles.end(), triangleZGreater );

  Board board;
  board.setLineWidth( 0 );
  board.setPenColor( LibBoard::Color::None );
  pt = orderedTriangles.begin();
  tend = orderedTriangles.end();
  while ( pt != tend ) {
    board.fillGouraudTriangle( pt->vertex0.first,
                               -pt->vertex0.second,
                               LibBoard::Color(true).setRGBf( pt->color0.first,
                                                              pt->color0.second,
                                                              pt->color0.third ),
                               pt->vertex1.first,
                               -pt->vertex1.second,
                               LibBoard::Color(true).setRGBf( pt->color1.first,
                                                              pt->color1.second,
                                                              pt->color1.third ),
                               pt->vertex2.first,
                               -pt->vertex2.second,
                               LibBoard::Color(true).setRGBf( pt->color2.first,
                                                              pt->color2.second,
                                                              pt->color2.third ),
                               gouraudDivisions );
    ++pt;
  }
  board.saveEPS( fileName );
  return 1;
}

void
SurfaceExporter::writeFIGColorMap( std::ofstream & file,
                                   int usedColors[] )
{
  const QColor * colors = _colorMap.qcolors();
  int nextColorIndex = 0;
  char line[1024];
  /*
    * Note that XFig allows 512 user customized colors (+ 32 predefined), and
    * that's enough for our purpose.
    */
  for ( int i = 0; i < 255; i++ )
    if ( usedColors[ i ] ) usedColors[i] = -1;

  for ( int i = 0; i < 256; i++ )
    if ( usedColors[i] ) {
      usedColors[ i ] = (nextColorIndex++) + 32;
      sprintf( line, "0 %d #%.2x%.2x%.2x\n",
               usedColors[ i ],
               colors[ i ].red(),
               colors[ i ].green(),
               colors[ i ].blue() );
      file << line ;
    }
}

bool
SurfaceExporter::draw( LibBoard::Board & board )
{
  double x, y;
  int direction;
  Surfel *ps;
  Surfel **pps, **end;
  pps = _surface.sortedSurfelsArray();
  end = _surface.sortedSurfelsEnd();

  int color;
  int n = 0;

  vector<Surfel*> pSurfelArray;
  pSurfelArray.resize( end - pps );
  memcpy( &(pSurfelArray[0]),
      pps,
      (end - pps)*sizeof(Surfel*) );

  typedef int (*SortFunction)(const void*,const void*);
  qsort( &(pSurfelArray[0]), end - pps, sizeof(pSurfel),
      reinterpret_cast< SortFunction >( & ::pSurfelZCompare ) );

  board.setLineWidth( 0.1 );
  board.setLineCap( LibBoard::Shape::RoundCap );
  board.setLineJoin( LibBoard::Shape::RoundJoin );
  vector<LibBoard::Point> polyline;
  polyline.push_back( LibBoard::Point( 0, 0 ) );
  polyline.push_back( LibBoard::Point( 0, 0 ) );
  polyline.push_back( LibBoard::Point( 0, 0 ) );
  polyline.push_back( LibBoard::Point( 0, 0 ) );
  if ( _surface.edges() )
    board.setPenColor( LibBoard::Color(0,0,255) );
  else
    board.setPenColor( LibBoard::Color::None );

  pps = end = 0;
  if ( !pSurfelArray.empty() ) {
    pps = &(pSurfelArray.front());
    end = 1 + &(pSurfelArray.back());
  }
  while ( pps != end ) {
    n++;
    ps = *pps;
    x = 10.0 * ps->rotatedCenter.first;
    y = - 10.0 * ps->rotatedCenter.second;
    direction = ps->direction;
    for ( int vertex = 0; vertex < 4; ++vertex ) {
      polyline[ vertex ].x = x + 10.0 * _surface._rotatedFaces[direction][vertex].first;
      polyline[ vertex ].y = y - 10.0 * _surface._rotatedFaces[direction][vertex].second;
    }
    color = _surface.surfelColor( ps );
    board.setFillColorRGBi( _colorMap[ color ].red(),
                            _colorMap[ color ].green(),
                            _colorMap[ color ].blue() );
    board.drawClosedPolyline( polyline );
    ++pps;
  }
  return true;
}

bool
SurfaceExporter::writeSVG( const char * fileName )
{
  LibBoard::Board board;
  draw( board );
  board.saveSVG( fileName );
  return true;
}

bool
SurfaceExporter::writeFIG( const char * fileName )
{
  LibBoard::Board board;
  draw( board );
  board.saveFIG( fileName );
  return true;
}


