/** -*- mode: c++ -*-
 * @file   SurfelListBuilder.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 *
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "SurfelListBuilder.h"
#include "Progress.h"

SurfelListBuilder::SurfelListBuilder(SurfelList & surfelList,
                                      bool fullBox,
                                      int volumeAdjacency,
                                      Double min)
  : _surfelList( surfelList ),
    _fullBox(fullBox),
    _volumeAdjacency(volumeAdjacency),
    _min(min)
{

}

//template<typename T>
//void SurfelListBuilder::buildGraph(ZZZImage<T> & image)
//{
//  // if ( !image ) return; // TODO
//  TRACE << "SurfelList::buildGraph<>() BEGIN: ";
//  TRACE << _surfelList.size() << " surfels.\n";
//  int directionA[6] = { 1, 1, 1, 1, 2, 2 };
//  int directionB[6] = { 2, 0, 0, 2, 3, 1 };

//  // (6,18) an (18,6) does not matter here.
//  int oppositeDirectionA[6][6] =
//  { { 3, 0, 3, 3, 0, 0 }, { 0, 3, 3, 3, 0, 0 },
//    { 3, 3, 3, 0, 0, 0 }, { 3, 3, 0, 3, 0, 0 },
//    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 2, 0, 0, 0 } };
//  int oppositeDirectionB[6][6] =
//  { { 0, 0, 0, 0, 1, 3 }, { 0, 2, 0, 0, 1, 3 },
//    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
//    { 0, 2, 0, 0, 1, 0 }, { 0, 2, 0, 0, 0, 3 } };

//  Face f, fs;
//  Surfel *ps = _surfelList.begin();
//  Surfel *pend = _surfelList.end();
//  Surfel *psv=0;
//  if ( _fullBox ) {
//    Face fa, fb;
//    SSize width, height, depth, bands;
//    image.getDimension( width, height, depth, bands );
//    --width;
//    --height;
//    --depth;
//    while ( ps < pend ) {
//      fs = Face( ps->voxel, ps->direction );
//      fa = fb = fs;
//      const Triple<SSize> & voxel = ps->voxel;
//      switch ( ps->direction ) {
//      case 0:
//        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
//        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
//        break;
//      case 1:
//        if ( ! voxel.second ) fa.direction = 3; else fa.y -= 1;
//        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
//        break;
//      case 2:
//        if ( ! voxel.first ) fa.direction = 1; else fa.x -= 1;
//        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
//        break;
//      case 3:
//        if ( voxel.first == width ) fa.direction = 0; else fa.x += 1;
//        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
//        break;
//      case 4:
//        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
//        if ( voxel.first == 0 ) fb.direction = 1; else fb.x -= 1;
//        break;
//      case 5:
//        if ( voxel.second == 0 ) fa.direction = 3; else fa.y -= 1;
//        if ( voxel.first == width ) fb.direction = 0; else fb.x += 1;
//        break;
//      }
//      psv = _surfelList.quickFind( fa );
//      ps->neighbors[ directionA[ ps->direction ] ] = psv;
//      psv->neighbors[ oppositeDirectionA[ ps->direction ][ psv->direction ] ] = ps;
//      psv = _surfelList.quickFind( fb );
//      ps->neighbors[ directionB[ ps->direction ] ] = psv;
//      psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ] = ps;
//      ++ps;
//    }
//  } else
//    if ( image.bands() == 1 ) {
//      if ( _volumeAdjacency == ADJ6 )
//        while ( ps < pend ) {
//          fs = Face( ps->voxel, ps->direction );
//          f = faceFindNeighbor_6_18( &image,
//                                     fs,
//                                     directionA[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionA[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
//              = ps;

//          f = faceFindNeighbor_6_18( &image,
//                                     fs,
//                                     directionB[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionB[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
//              = ps;
//          ++ps;
//        } else /* ADJ18 */
//        while ( ps < pend ) {
//          fs = Face( ps->voxel, ps->direction );
//          f = faceFindNeighbor_18_6( &image,
//                                     fs,
//                                     directionA[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionA[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
//              = ps;

//          f = faceFindNeighbor_18_6( &image,
//                                     fs,
//                                     directionB[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionB[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
//              = ps;
//          ++ps;
//        }
//    } else {
//      if ( _volumeAdjacency == ADJ6 )
//        while ( ps < pend ) {
//          fs = Face( ps->voxel, ps->direction );
//          f = faceFindNeighborColor_6_18( &image,
//                                          fs,
//                                          directionA[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionA[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
//              = ps;

//          f = faceFindNeighborColor_6_18( &image,
//                                          fs,
//                                          directionB[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionB[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
//              = ps;
//          ++ps;
//        } else
//        while ( ps < pend ) {
//          fs = Face( ps->voxel, ps->direction );
//          f = faceFindNeighborColor_18_6( &image,
//                                          fs,
//                                          directionA[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionA[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
//              = ps;

//          f = faceFindNeighborColor_18_6( &image,
//                                          fs,
//                                          directionB[ ps->direction ] );
//          psv = _surfelList.quickFind( f );
//          ps->neighbors[ directionB[ ps->direction ] ] = psv;
//          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
//              = ps;
//          ++ps;
//        }
//    }
//  TRACE << "SurfelList::buildGraph<>() END\n";
//}


template<typename T>
void SurfelListBuilder::visitAny( ZZZImage<T> & volume )
{
  TRACE << "SurfelListBuilder<>::visitAny(): BEGIN\n";
  if ( volume.isEmpty() ) {
    _surfelList.clear();
    _surfelList.buildSortedArray();
    return;
  }

  size_t inc_dy = volume.width() + 2;
  size_t inc_dz = (volume.height() + 2) * static_cast<size_t> ( volume.width() + 2 );
  SSize depth = volume.depth();
  SSize rows = volume.height();
  SSize cols = volume.width();
  T min = static_cast<T>(_min);

  int shift_dy = 1;
  int shift_dz = ( cols + 2 );
  if ( ! volume.zeroFramed() ) {
    shift_dy = 0;
    shift_dz = 0;
    inc_dy -= 2;
    inc_dz = rows * static_cast<size_t>( cols );
  }

  _surfelList.clear();
  _surfelList.bbValid(true);

  SSize x=0,y=0,z=0;

  if ( _fullBox ) {
    SSize top;
    for ( x = 0; x < cols; ++x ) {
      top = depth - 1;
      for ( y = 0; y < rows; ++y ) {
        _surfelList.add( Surfel( x, y, 0, 5, 1 ) );
        _surfelList.add( Surfel( x, y, top, 4, 1 ) );
      }
      top = rows - 1;
      for ( z = 0; z < depth; ++z ) {
        _surfelList.add( Surfel( x, 0, z, 3, 1 ) );
        _surfelList.add( Surfel( x, top, z, 2, 1 ) );
      }
    }
    top = cols - 1;
    for ( y = 0; y < rows; ++y )
      for ( z = 0; z < depth; ++z ) {
        _surfelList.add( Surfel( 0, y , z, 1, 1 ) );
        _surfelList.add( Surfel( top, y, z, 0, 1 ) );
      }
  } else if ( volume.bands() == 1 ) {
    T ***matrix = volume.data( 0 );
    T *vEEE = matrix[0][0];
    T *vMEE = vEEE - 1;
    T *vEME = vEEE - inc_dy;
    T *vEEM = vEEE - inc_dz;
    globalProgressReceiver.pushInterval( depth,"Building surfel list...");
    while ( z <= depth ) {
      if ( *vEEE >= min ) {
        if ( *vMEE < min ) _surfelList.add( Surfel( x, y, z, 1, *vEEE ) );
        if ( *vEME < min ) _surfelList.add( Surfel( x, y, z, 3, *vEEE ) );
        if ( *vEEM < min ) _surfelList.add( Surfel( x, y, z, 5, *vEEE ) );
      } else {
        if ( *vMEE >= min ) _surfelList.add( Surfel( x - 1, y, z, 0, *vMEE ) );
        if ( *vEME >= min ) _surfelList.add( Surfel( x, y - 1, z, 2, *vEME ) );
        if ( *vEEM >= min ) _surfelList.add( Surfel( x, y, z - 1, 4, *vEEM ) );
      }
      ++x; ++vEEE; ++vMEE; ++vEME; ++vEEM;
      if ( x > cols ) {
        ++y; x = 0;
        vEEE += shift_dy; vMEE += shift_dy; vEME += shift_dy; vEEM += shift_dy;
        if ( y > rows ) {
          ++z; y = 0;
          vEEE += shift_dz; vMEE += shift_dz; vEME += shift_dz; vEEM += shift_dz;
          if ( !(z%10) )
            globalProgressReceiver.setProgress(z);
        }
      }
    }
    globalProgressReceiver.popInterval();
  } else {
    T ***matrix0 = volume.data( 0 );
    T ***matrix1 = volume.data( 1 );
    T ***matrix2 = volume.data( 2 );
    T *vEEE0 = matrix0[0][0];
    T *vEEE1 = matrix1[0][0];
    T *vEEE2 = matrix2[0][0];
    T *vMEE0 = vEEE0 - 1;
    T *vEME0 = vEEE0 - inc_dy;
    T *vEEM0 = vEEE0 - inc_dz;
    T *vMEE1 = vEEE1 - 1;
    T *vEME1 = vEEE1 - inc_dy;
    T *vEEM1 = vEEE1 - inc_dz;
    T *vMEE2 = vEEE2 - 1;
    T *vEME2 = vEEE2 - inc_dy;
    T *vEEM2 = vEEE2 - inc_dz;
    globalProgressReceiver.pushInterval( depth,"Building surfel list...");
    while ( z <= depth ) {
      if ( ( *vEEE0 >= min ) || ( *vEEE1 >= min ) || ( *vEEE2 >= min ) ) {
        if ( *vMEE0 < min && *vMEE1 < min && *vMEE2 < min )
          _surfelList.add( Surfel( x, y, z, 1, 1 ) );
        if ( *vEME0 < min && *vEME1 < min && *vEME2 < min )
          _surfelList.add( Surfel( x, y, z, 3, 1 ) );
        if ( *vEEM0 < min && *vEEM1 < min && *vEEM2 < min )
          _surfelList.add( Surfel( x, y, z, 5, 1 ) );
      } else {
        if ( *vMEE0 >= min || *vMEE1 >= min || *vMEE2 >= min )
          _surfelList.add( Surfel( x - 1, y, z, 0, 1 ) );
        if ( *vEME0 >= min || *vEME1 >= min || *vEME2 >= min )
          _surfelList.add( Surfel( x, y - 1, z, 2, 1 ) );
        if ( *vEEM0 >= min || *vEEM1 >= min || *vEEM2 >= min )
          _surfelList.add( Surfel( x, y, z - 1, 4, 1 ) );
      }
      ++x;
      ++vEEE0; ++vMEE0; ++vEME0; ++vEEM0;
      ++vEEE1; ++vMEE1; ++vEME1; ++vEEM1;
      ++vEEE2; ++vMEE2; ++vEME2; ++vEEM2;
      if ( x > cols ) {
        ++y; x = 0;
        vEEE0 += shift_dy; vMEE0 += shift_dy;
        vEME0 += shift_dy; vEEM0 += shift_dy;
        vEEE1 += shift_dy; vMEE1 += shift_dy;
        vEME1 += shift_dy; vEEM1 += shift_dy;
        vEEE2 += shift_dy; vMEE2 += shift_dy;
        vEME2 += shift_dy; vEEM2 += shift_dy;
        if ( y > rows ) {
          ++z; y = 0;
          vEEE0 += shift_dz; vMEE0 += shift_dz;
          vEME0 += shift_dz; vEEM0 += shift_dz;
          vEEE1 += shift_dz; vMEE1 += shift_dz;
          vEME1 += shift_dz; vEEM1 += shift_dz;
          vEEE2 += shift_dz; vMEE2 += shift_dz;
          vEME2 += shift_dz; vEEM2 += shift_dz;
          if ( !(z%10) )
            globalProgressReceiver.setProgress(z);
        }
      }
    }
    globalProgressReceiver.popInterval();
  }

  globalProgressReceiver.showMessage("Translating...");
  _surfelList.translate( -0.5 * ( _surfelList.bbMin() + _surfelList.bbMax() ) );
  globalProgressReceiver.showMessage("Sorting surfels...");
  _surfelList.buildSortedArray();
  globalProgressReceiver.showMessage(0);
  TRACE << "SurfelListBuilder<>::visitAny(): END\n";
}

void
SurfelListBuilder::visit( ZZZImage<UChar> & volume )
{
  visitAny(volume);
}

void
SurfelListBuilder::visit( ZZZImage<Short> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<UShort> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<Int32> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<UInt32> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<Float> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<Double> & volume )
{
  visitAny(volume);
}

void SurfelListBuilder::visit( ZZZImage<Bool> &  )
{
  // visitAny(volume); // TODO
}


