/**
 * @file   Surface.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:12 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include <set>
#include <queue>
#include <vector>
#include <algorithm>
#include <limits>
#include <fstream>
#include <iostream>
#include <cassert>
#include <cmath>
#include <map>

using std::set;
using std::queue;
using std::vector;
using std::numeric_limits;
using std::endl;
using std::ofstream;
using std::ifstream;

#include <QImage>
#include <QPainter>
#include <QPolygon>
#include <QTime>
#include <QSemaphore>
#include <QThreadPool>

#include "tools.h"
#include "ColorMap.h"
#include "tools3d.h"
#include "Settings.h"

#include "Surface.h"
#include "SurfacePainter.h"
#include "EdgelList.h"
#include "Convolution.h"
#include "ConvolThread.h"
#include "SurfelListBuilder.h"
#include "SurfelGraphBuilderThread.h"
#include "zzzimage.h"

#include "TimeMeasure.h"

namespace {
   int roundInt( double x )  {
      // return static_cast<int>( floor( x+0.49 ) );
      return static_cast<int>( (x<0)?floor( x-0.49 ):ceil(x+0.49) );
   }
   int rounded( double x )  {
      return static_cast<int>( x );
   }
}

Surface::Surface()
   : _image( 0 ),
     _rotationQ( 0.0, 0.0, 0.0, 1.0 ),
     _lightRotationQ( 0.0, 0.0, 0.0, 1.0 )

{
   CONS( Surface );
   _lastColorMap = 0;
   _adjacency = ADJ26;
   _terminalTest = 0;
   _skelMethod = 0;
   _bytesPerPixel = BYTES_PER_PIXEL;

   _normals[0] = makeTriple(  1.0, 0.0, 0.0 );
   _normals[1] = makeTriple( -1.0, 0.0, 0.0 );
   _normals[2] = makeTriple( 0.0, +1.0, 0.0 );
   _normals[3] = makeTriple( 0.0, -1.0, 0.0 );
   _normals[4] = makeTriple( 0.0, 0.0, +1.0 );
   _normals[5] = makeTriple( 0.0, 0.0, -1.0 );

   _viewCenterShift = 0;

   initReferenceFaces();
   _forceAllSurfels = false;
   _zoom = 10;
   _edges = false;
   _axis = true;

   _edgeColor.setRgb( 0, 255, 0 );
   _edgeRgb = _edgeColor.rgb();

   _rotationQ.rotationMatrix( _rotation );
   _rotationsCount = 0;

   _aspect = 1.0f;
   _aspectEnabled = false;
   _markedSliceAxis = -1;
   _markedSlice = -1;
   _markedSurfel = -1;
   memset( _visibleDirections, 0, 6 * sizeof( bool ) );
   _settings = globalSettings;

   _surfelColorMethod = &Surface::surfelColorVoxelGray;
   _surfacePainter = 0;
   _cutPlaneDepth = -numeric_limits<double>::max();
   _shading = ColorFromOrientation;
   _validNormals = false;
   _valid = true;
   setShading( ColorFromMaterial );
}

Surface::~Surface()
{
}

void
Surface::clear()
{
   TRACE << "Clearing the surface.\n";
   _ssl[ 0 ][ 0 ][ 0 ].clear();
   _ssl[ 0 ][ 0 ][ 1 ].clear();
   _ssl[ 0 ][ 1 ][ 0 ].clear();
   _ssl[ 0 ][ 1 ][ 1 ].clear();
   _ssl[ 1 ][ 0 ][ 0 ].clear();
   _ssl[ 1 ][ 0 ][ 1 ].clear();
   _ssl[ 1 ][ 1 ][ 0 ].clear();
   _ssl[ 1 ][ 1 ][ 1 ].clear();
   _surfelList.clear();
   _surfelDataArray.clear();
   _valid = true;
   _image = 0;
}

void
Surface::aspect( double x, double y, double z ) {
   _aspect = makeTriple( x, y, z );
   if ( _aspect.first <= 0 )
      _aspectEnabled = false;
   else _aspectEnabled = true;
}

void
Surface::aspect( bool on ) {
   _aspectEnabled  = on;
   if ( ! _aspectEnabled )
      _aspect = -1.0;
}

void
Surface::setEdgeColor( QColor color )
{
   _edgeColor = color;
   _edgeRgb = color.rgb();
}

void
Surface::setShading( int shading )
{
   AbstractSurfacePainter::SliceType sliceType
         = (_markedSliceAxis >= 0) ? AbstractSurfacePainter::DrawSlice : AbstractSurfacePainter::NoSlice ;
   AbstractSurfacePainter::EdgesType edgesType
         = _edges ? AbstractSurfacePainter::DrawEdges : AbstractSurfacePainter::NoEdges ;

   if ( _shading != ColorFixed && shading == ColorFixed ) {
      Surfel *ps = _surfelList.begin();
      Surfel *end = _surfelList.end();
      while ( ps != end ) {
         ps->color = surfelColor( ps );
         ++ps;
      }
   }
   _shading = static_cast<Shading>( shading );
   delete _surfacePainter;
   switch ( _shading ) {
   case ColorFromOrientation:
      _surfelColorMethod = &Surface::surfelColorOrientation;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorFromDepth:
      _surfelColorMethod = &Surface::surfelColorDepth;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorFixed:
      _surfelColorMethod = &Surface::surfelColorFixed;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorFromCurvature:
      computeCurvature( _settings->normalsEstimateIterations(),
                        _settings->curvatureEstimateIterations(),
                        _settings->curvatureEstimateAveragingIterations(),
                        _settings->curvatureType(),
                        _settings->curvatureEstimateAdaptive() );
      _surfelColorMethod = &Surface::surfelColorFixed;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorWhite:
      _surfelColorMethod = &Surface::surfelColorFixed;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorFromLight:
      computeNormals( _settings->normalsEstimateIterations(), _settings->normalsEstimateAdaptive() );
      _surfelColorMethod = &Surface::surfelColorLight;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   case ColorFromLightedMaterial:
      computeNormals( _settings->normalsEstimateIterations(), _settings->normalsEstimateAdaptive() );
      if ( _image && _image->bands() != 3 ) {
         _surfelColorMethod = &Surface::surfelColorLightedVoxelGray;
         _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                          sliceType,
                                                                          edgesType );
      } else {
         _surfelColorMethod = &Surface::surfelColorLightedVoxelRGB;
         _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseTrueColor,
                                                                          sliceType,
                                                                          edgesType );
      }
      break;
   case ColorFromLightedColorMap:
      computeNormals( _settings->normalsEstimateIterations(), _settings->normalsEstimateAdaptive() );
      _surfelColorMethod = &Surface::surfelColorLightedVoxelColorMap;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseTrueColor,
                                                                       sliceType,
                                                                       edgesType );
      break;

   case ColorFromLightedLUT:
      computeNormals( _settings->normalsEstimateIterations(), _settings->normalsEstimateAdaptive() );
      _surfelColorMethod = &Surface::surfelColorLightedVoxelLUT;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseTrueColor,
                                                                       sliceType,
                                                                       edgesType );
      break;

   case ColorFromMaterial:
      if ( _image && _image->bands() != 3 ) {
         _surfelColorMethod = &Surface::surfelColorVoxelGray;
         _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                          sliceType,
                                                                          edgesType );
      } else {
         _surfelColorMethod = &Surface::surfelColorVoxelRGB;
         _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseTrueColor,
                                                                          sliceType,
                                                                          edgesType );
      }
      break;
   default:
      _surfelColorMethod = &Surface::surfelColorConst;
      _surfacePainter = AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::UseColorMap,
                                                                       sliceType,
                                                                       edgesType );
      break;
   }
   _surfacePainter->setSurface(this);
}

void
Surface::refreshShading()
{
   TRACE << "Surface::refreshShading()\n";
   switch ( _shading ) {
   case ColorFromCurvature:
      computeCurvature( _settings->normalsEstimateIterations(),
                        _settings->curvatureEstimateIterations(),
                        _settings->curvatureEstimateAveragingIterations(),
                        _settings->curvatureType(),
                        _settings->curvatureEstimateAdaptive() );
      break;
   case ColorFromLight:
   case ColorFromLightedMaterial:
   case ColorFromLightedColorMap:
   case ColorFromLightedLUT:
      invalidateNormals();
      computeNormals( _settings->normalsEstimateIterations(),
                      _settings->normalsEstimateAdaptive() );
      break;
   default:
      TRACE << "Surface::refreshShading(): Nothing to do\n";
      break;
   }
}

void
Surface::edges( bool edges ) {
   _edges = edges;
   setShading( _shading );
}

void
Surface::axis( bool on ) {
   _axis = on;
}

void
Surface::initReferenceFaces()
{
   int dir, i;
   double deltaX[4][6] = {
      {0, 0, 0.5, -0.5, -0.5, -0.5},
      {0, 0, -0.5, 0.5, 0.5, 0.5},
      {0, 0, -0.5, 0.5, 0.5, 0.5},
      {0, 0, 0.5, -0.5, -0.5, -0.5}
   };
   double deltaY[4][6] = {
      {-0.5, 0.5, 0, 0, -0.5, 0.5},
      {0.5, -0.5, 0, 0, -0.5, 0.5},
      {0.5, -0.5, 0, 0, 0.5, -0.5},
      {-0.5, 0.5, 0, 0, 0.5, -0.5}
   };
   double deltaZ[4][6] = {
      {-0.5, -0.5, -0.5, -0.5, 0, 0},
      {-0.5, -0.5, -0.5, -0.5, 0, 0},
      {0.5, 0.5, 0.5, 0.5, 0, 0},
      {0.5, 0.5, 0.5, 0.5, 0, 0}
   };

   for (dir = 0; dir < 6; dir++)
      for (i = 0; i < 4; i++) {
         _rotatedFaces[dir][i].first = deltaX[i][dir];
         _rotatedFaces[dir][i].second = deltaY[i][dir];
         _rotatedFaces[dir][i].third = deltaZ[i][dir];
         _referenceFaces[dir][i].first = deltaX[i][dir];
         _referenceFaces[dir][i].second = deltaY[i][dir];
         _referenceFaces[dir][i].third = deltaZ[i][dir];
      }

   _edgesDirections[0] = Triple<double>(0, 0, -0.5 );
   _edgesDirections[1] = Triple<double>(0, 0, -0.5 );
   _edgesDirections[2] = Triple<double>(0, 0, -0.5 );
   _edgesDirections[3] = Triple<double>(0, 0, -0.5 );
   _edgesDirections[4] = Triple<double>(0, -0.5, 0 );
   _edgesDirections[5] = Triple<double>(0, 0.5, 0 );
}

void
Surface::buildSurfelsImages()
{
   int col;
   QPolygon vertices(4);
   int x_min, x_max, y_min, y_max;
   QPainter painter;

   _maxSurfelWidth = 0;
   _maxSurfelHeight = 0;

   for (int dir = 0; dir < 6; dir++) {
      int *start = starts[dir];
      int *length = lengths[dir];
      x_min = y_min = numeric_limits<int>::max();
      x_max = y_max = numeric_limits<int>::min();

      for (int i = 0; i < 4; i++) {
         vertices[i].setX( roundInt(  _zoom * _rotatedFaces[dir][i].first ) );
         vertices[i].setY( roundInt(  _zoom  * _rotatedFaces[dir][i].second ) );
         if( vertices[i].x() > x_max ) x_max = vertices[i].x();
         if( vertices[i].y() > y_max ) y_max = vertices[i].y();
         if( vertices[i].x() < x_min ) x_min = vertices[i].x();
         if( vertices[i].y() < y_min ) y_min = vertices[i].y();
      }

      if ( (x_max - x_min) % 2 )
         for ( int i = 0; i < 4 ; i++ )
            if ( vertices[i].x() == x_max )
               vertices[i].rx() ++;

      if ( (y_max - y_min) % 2 )
         for ( int i = 0; i < 4 ; i++ )
            if ( vertices[i].y() == y_max )
               vertices[i].ry() ++;

      int pw = ( x_max - x_min ) + 1;
      int ph = ( y_max - y_min ) + 1;

      if ( pw > _maxSurfelWidth )
         _maxSurfelWidth = pw;
      if ( ph > _maxSurfelHeight )
         _maxSurfelHeight = ph;

      for ( int i = 0; i < 4 ; i++ ) {
         vertices[i].rx() += pw >> 1;
         vertices[i].ry() += ph >> 1;
      }

      QImage im( pw, ph, QImage::Format_RGB32 );
      painter.begin( & im );
      im.fill( Qt::white );
      painter.setBrush( QBrush( Qt::black ) ) ;
      if ( pw == 1 || ph == 1 )
         im.fill( Qt::black );
      else
         painter.drawPolygon( vertices );

      painter.end();
      QRgb black = QColor(Qt::black).rgb();

      dx[dir] = - (pw/2.0);
      dy[dir] = - (ph/2.0);

      int on = -1;
      int row = 0;
      for ( int y = 0; y < ph; y++) {
         if ( on < 0 )
            for ( int x = 0; x < pw && ( on < 0 ); x++) if ( im.pixel(x,y) == black ) on = y;
         if ( on >= 0 ) {
            col = -1;
            length[row] = BYTES_PER_PIXEL;
            for ( int x = 0; x < pw; x++) {
               if ( ( col < 0 )  && ( im.pixel(x,y) == black) ) {
                  col  = x;
                  start[ row ] = col << 2;
               }
               else if ( ( col >= 0 ) && (im.pixel(x,y) == black) ) {
                  length[row] += BYTES_PER_PIXEL;
               }
            }
            if ( col >= 0 ) row++;	/* A new line has been added */
            else continue;	/* The surfel is finished */
         }
      }
      height[dir] = row;
      rows[dir] = on;
   }
}

void
Surface::centersArray( SurfelDataArray & surfelDataArray )
{
   surfelDataArray.resize( _surfelList.size() );
   SurfelDataArray::iterator psd = surfelDataArray.begin();
   Surfel * ps = _surfelList.begin();
   Surfel * end = _surfelList.end();
   while ( ps < end ) {
      *psd = ps->center;
      ++ps;
      ++psd;
   }
}

void
Surface::curvaturesArray( SurfelDataArray & surfelDataArray )
{
   surfelDataArray.resize( _surfelList.size() );
   SurfelDataArray::iterator isd = surfelDataArray.begin();
   Surfel * ps = _surfelList.begin();
   Surfel * end = _surfelList.end();
   while ( ps < end ) {
      isd->first = ps->meanCurvature;      // Mean curvature
      isd->second = ps->gaussianCurvature; // Gaussian curvature
      isd->third = 0;                      // Unused
      ++ps;
      ++isd;
   }
}

void
Surface::initializeNormals( NormalsFlags normals  )
{
   Surfel *ps = _surfelList.begin();
   Surfel *end = _surfelList.end();
   if ( normals == RotatedNormals )
      while ( ps != end ) {
         ps->normal = _rotatedNormals[ ps->direction ];
         ++ps;
      }
   else
      while ( ps != end ) {
         ps->normal = _normals[ ps->direction ];
         ++ps;
      }
}

void
Surface::scalarArrayFromMarkedSurfels( SurfelDataArray & surfelDataArray,
                                       UChar color )
{
   Surfel *ps, *end;
   SurfelDataArray::iterator psd;
   surfelDataArray.resize( _surfelList.size() );
   ps = _surfelList.begin();
   end = _surfelList.end();
   psd = surfelDataArray.begin();
   while ( ps < end ) {
      // CM
      if ( ps->color == color ) {
         *psd = 1e300; // CM
         // *psd = surfelDataArray.size();
      } else {
         *psd = 0.0;
         ps->color = 0;
      }
      //     *psd = 1.0;
      ps++;
      psd++;
   }
}

void
Surface::convolutionResponse( int iterations )
{
   Surfel * ps, *end;
   SurfelDataArray sdaDest( _surfelList.size() );
   SurfelDataArray::iterator psda, psd;
   double min = numeric_limits<double>::max();
   double max = -numeric_limits<double>::max();
   SurfelData sd;

   while ( iterations-- ) {
      min = numeric_limits<double>::max();
      max = -numeric_limits<double>::max();
      ps = _surfelList.begin();
      end = _surfelList.end();
      while ( ps < end ) {
         sd = convolution421( ps, _surfelList, _surfelDataArray, sdaDest, 4, 2, 1 );
         // sd = convolutionIso( ps, _surfelList, _surfelDataArray, sdaDest, 4 );  // CM
         // sd = convolutionConst( ps, _surfelList, _surfelDataArray, sdaDest );
         if ( sd.first > max ) max = sd.first;
         if ( sd.first < min ) min = sd.first;
         ps++;
      }
      _surfelDataArray = sdaDest;
   }

   psda = psd = _surfelDataArray.begin();
   ps = _surfelList.begin();
   end = _surfelList.end();
   double range = max - min;
   while ( ps != end ) {
      // CM
      // ps->color = static_cast<UChar>( 255 * ( (psd->first - min) / range ) );
      ps->color = static_cast<UChar>( ceil( 255 * ( (psd->first - min) / range )  ) );
      ++ps;
      ++psd;
   }
}

void
Surface::convolution421Centers( int iterations,
                                SurfelDataArray & data )
{
   int threadCount= ( globalSettings->maxThreads() == -1 ) ? QThread::idealThreadCount() : globalSettings->maxThreads();
   SurfelDataArray sdaDest( _surfelList.size() );
   Surfel *ps, *end = _surfelList.end();
   centersArray( data );
   Size surfelListSize = _surfelList.size();
   Size chunkSize = surfelListSize / threadCount;
   TIME_MEASURE_START("Convolution421Centers");
   if ( _settings->multiThreading() && threadCount > 1 && chunkSize > 10000 ) {
      int t;
      Size iter = iterations;
      ConvolThread * * convolThreads = new ConvolThread* [ threadCount ];
      // QSemaphore exmut(1);
      QSemaphore semGoEven(0);
      QSemaphore semGoOdd(0);
      QSemaphore semDone(0);
      for ( t = 0; t < threadCount; ++t )
         convolThreads[ t ] = new ConvolThread( semGoEven,
                                                semGoOdd,
                                                semDone,
                                                _surfelList,
                                                data,
                                                sdaDest,
                                                chunkSize * t,
                                                ( t < threadCount - 1 ) ?
                                                   (chunkSize * (t+1)) :
                                                   surfelListSize,
                                                iterations
                                                );
      for ( t = 0; t < threadCount; ++t )
         convolThreads[ t ]->start();

      globalProgressReceiver.pushInterval(iterations,"Convolutions on surfel centers...");
      iter = iterations;
      while ( iter-- ) {
         if ( iter % 2 )
            semGoOdd.release( threadCount );  // Wakeup all threads for a
         else                                // new iteration.
            semGoEven.release( threadCount ); // Wakeup all threads for a
         // new iteration
         semDone.acquire( threadCount );     // Wait until all threads have
         // finished their part
         data = sdaDest;                     // Next iteration: result becomes the new input data
         globalProgressReceiver.setProgress(1+iterations-iter);
      }
      globalProgressReceiver.popInterval();
      for ( t = 0; t < threadCount; ++t ) {
         if ( convolThreads[ t ]->wait() )
            delete convolThreads[ t ];
         else
            std::cerr << "QThread::wait() returned false\n";
      }
      delete[] convolThreads;
   } else {
      Size iter = iterations;
      globalProgressReceiver.pushInterval(iterations,"Convolutions on surfel centers...");
      while ( iter-- ) {
         ps = _surfelList.begin();
         while ( ps != end ) {
            convolution421( ps, _surfelList, data, sdaDest );
            ++ps;
         }
         data =  sdaDest;
         globalProgressReceiver.setProgress(1+iterations-iter);
      }
      globalProgressReceiver.popInterval();
   }
   TIME_MEASURE_STOP("Convolution421Centers");
}

void
Surface::convolution421CentersAdaptive( int iterations,
                                        SurfelDataArray & data )
{
   computeCurvature( 10, 15, 1, MAXIMUM_CURVATURE, false );

   SurfelDataArray sdaDest( _surfelList.size() );
   Surfel *ps, *end = _surfelList.end();
   centersArray( data );
   globalProgressReceiver.pushInterval(iterations,"Adaptive convolution...");
   const int max = iterations;
   while ( iterations-- ) {
      ps = _surfelList.begin();
      while ( ps < end ) {
         convolution421( ps, _surfelList, data, sdaDest );
         ps++;
      }
      data =  sdaDest;
      globalProgressReceiver.setProgress(1+max-iterations);
      TRACE << "[" << iterations << " iterations remain.]\n";
   }
   globalProgressReceiver.popInterval();

   // Adaptive part according to the max curvature
   Surfel *array = _surfelList.begin();
   SurfelData sd_conv;
   SurfelData sd_ident;
   double lambda;
   double maxC;
   ps = _surfelList.begin();
   while ( ps < end ) {
      maxC = ps->meanCurvature - sqrt( dsquare(ps->meanCurvature) - ps->gaussianCurvature );
      lambda =  atan( 50 * abs( maxC ) ) * 2.0 / M_PI;
      sd_conv = sdaDest[ ps - array ];
      sd_ident = ps->center;
      data[ ps - array ] = (1.0-lambda) * sd_conv + lambda * sd_ident;
      ps++;
   }
}

void
Surface::convolution421Centers( int iterationsA,
                                SurfelDataArray & dataA,
                                int iterationsB,
                                SurfelDataArray & dataB )
{
   if ( iterationsA > iterationsB ) {
      centersArray( dataA );
      centersArray( dataB );
      ERROR << "convolution421Centers(): Bad iteration numbers (" << iterationsA << "," << iterationsB << ")\n";
      return;
   }
   SurfelDataArray sdaDest( _surfelList.size() );
   Surfel *ps=0, *end=_surfelList.end();
   centersArray( dataA );

   globalProgressReceiver.pushInterval(2,"Convolution on surfel centers...");

   globalProgressReceiver.pushInterval(iterationsA);
   const int maxA = iterationsA;
   while ( iterationsA-- ) {
      ps = _surfelList.begin();
      while ( ps != end ) {
        convolution421( ps, _surfelList, dataA, sdaDest );
        ps++;
      }
      dataA =  sdaDest;
      globalProgressReceiver.setProgress(1+maxA-iterationsA);
   }
   globalProgressReceiver.popInterval();
   globalProgressReceiver.setProgress(1);
   dataB = dataA;
   iterationsB -= iterationsA;
   globalProgressReceiver.pushInterval(iterationsB);
   const int maxB = iterationsB;
   while ( iterationsB-- ) {
      ps = _surfelList.begin();
      while ( ps != end ) {
         convolution421( ps, _surfelList, dataB, sdaDest );
         ps++;
      }
      dataB =  sdaDest;
      globalProgressReceiver.setProgress(1+maxB-iterationsB);
   }
   globalProgressReceiver.popInterval();
   globalProgressReceiver.setProgress(2);
   globalProgressReceiver.popInterval();
}

void
Surface::convolution421CentersAdaptive( int iterationsA,
                                        SurfelDataArray & dataA,
                                        int iterationsB,
                                        SurfelDataArray & dataB )
{
   if ( iterationsA > iterationsB ) {
      centersArray( dataA );
      centersArray( dataB );
      ERROR << "convolution421CentersAdaptive(): Bad iteration numbers"
            << " ("  << iterationsA << "," << iterationsB << ")\n";
      return;
   }

   globalProgressReceiver.pushInterval(4,"Adaptive convolution on surfel centers...");

   computeCurvature( 10, 15, 1, MAXIMUM_CURVATURE, false );

   globalProgressReceiver.setProgress(1);

   SurfelDataArray sdaDest( _surfelList.size() );
   Surfel *ps=0, *end=_surfelList.end();
   centersArray( dataA );
   globalProgressReceiver.pushInterval(iterationsA);
   const int maxA = iterationsA;
   while ( iterationsA-- ) {
      ps = _surfelList.begin();
      while ( ps != end ) {
         convolution421( ps, _surfelList, dataA, sdaDest );
         ps++;
      }
      dataA = sdaDest;
      globalProgressReceiver.setProgress(1+maxA-iterationsA);
   }
   globalProgressReceiver.popInterval();

   globalProgressReceiver.setProgress(2);

   dataB = dataA;
   iterationsB -= iterationsA;
   globalProgressReceiver.pushInterval(iterationsA);
   const int maxB = iterationsB;
   while ( iterationsB-- ) {
      ps = _surfelList.begin();
      while ( ps != end ) {
         convolution421( ps, _surfelList, dataB, sdaDest );
         ps++;
      }
      dataB = sdaDest;
      globalProgressReceiver.setProgress(1+maxB-iterationsB);
   }
   globalProgressReceiver.popInterval();

   globalProgressReceiver.setProgress(3);

   // Adaptive part according to the max curvature.
   Surfel *array = _surfelList.begin();
   SurfelData sd_conv;
   SurfelData sd_ident;
   double lambda;
   double maxC;
   ps = _surfelList.begin();
   while ( ps < end ) {
      maxC = ps->meanCurvature - sqrt( dsquare(ps->meanCurvature) - ps->gaussianCurvature );
      lambda =  atan( 50 * abs( maxC ) ) * 2.0 / M_PI;
      sd_conv = sdaDest[ ps - array ];
      sd_ident = ps->center;
      dataB[ ps - array ] = (1-lambda) * sd_conv + lambda * sd_ident;
      ps++;
   }

   globalProgressReceiver.setProgress(4);

   globalProgressReceiver.popInterval();
}

void
Surface::normalsArray( SurfelDataArray & sda )
{
   Surfel *ps = _surfelList.begin();
   Surfel *begin = _surfelList.begin();
   Surfel *end = _surfelList.end();
   sda.resize( _surfelList.size() );
   while ( ps != end ) {
      sda[ ps - begin ] = ps->normal;
      ++ps;
   }
}

void
Surface::computeNormals( int iterations, bool adaptive )
{
   if ( _validNormals ) return;
   if ( adaptive )
      convolution421CentersAdaptive( iterations, _surfelDataArray );
   else
      convolution421Centers( iterations, _surfelDataArray );

   // Compute partial derivatives and normal vectors.
   Surfel *ps = _surfelList.begin();
   Surfel *begin = _surfelList.begin();
   Surfel *end = _surfelList.end();
   Triple<double> dxdu, dxdv;
   while ( ps != end ) {
      dxdu = _surfelDataArray[ ps->neighbors[0] - begin ] - _surfelDataArray[ ps - begin ];
      dxdv = _surfelDataArray[ ps->neighbors[1] - begin ] - _surfelDataArray[ ps - begin ];
      ps->normal = wedgeProduct( dxdu, dxdv );
      ps->normal /= norm( ps->normal );
      ++ps;
   }
   _validNormals = true;
   //std::cout << (std::clock() - t) / static_cast<float>( CLOCKS_PER_SEC ) << " secs\n";
}

namespace {
   inline Surfel * next( Surfel * ps, int i )
   {
      return ps->neighbors[ i ];
   }
   inline Surfel * nextnext( Surfel * ps, int i )
   {
      int edge = ( edgeFromTable[ ps->direction ][ i ][ ps->neighbors[ i ]->direction ] + 2 ) % 4 ;
      return ps->neighbors[ i ]->neighbors[ edge ];
   }
}

void
Surface::computeCurvature( int normalEstimateIterations,
                           int curvatureEstimateIterations,
                           int averagingIterations,
                           int curvatureType,
                           bool adaptive )
{
   computeCurvature121Scheme( normalEstimateIterations,
                              // computeCurvatureCenteredScheme( normalEstimateIterations,
                              curvatureEstimateIterations,
                              averagingIterations,
                              curvatureType,
                              adaptive );
}

void
Surface::computeCurvatureCenteredScheme( int normalEstimateIterations,
                                         int curvatureEstimateIterations,
                                         int averagingIterations,
                                         int curvatureType,
                                         bool adaptive )
{
   _surfelDataArray.clear();
   SurfelDataArray sda1,sda2;

   if ( adaptive )
      convolution421CentersAdaptive( normalEstimateIterations,
                                     sda1,
                                     curvatureEstimateIterations, /* normalEstimateIterations */
                                     sda2 );
   else
      convolution421Centers( normalEstimateIterations,
                             sda1,
                             curvatureEstimateIterations, /* normalEstimateIterations */
                             sda2 );

   Surfel *ps = _surfelList.begin();
   Surfel *begin = _surfelList.begin();
   Surfel *end = _surfelList.end();
   Triple<double> dxdu, dxdv, nu;
   Triple<double> d2xdu2, d2xdv2, d2xduv;
   Triple<double> tmp1, tmp2;
   int edge;
   double m = numeric_limits<double>::max();
   while ( ps != end ) {
      dxdu = 0.5 * ( sda1[ next(ps,0) - begin ] - sda1[ next(ps,2) - begin ] );
      dxdv = 0.5 * ( sda1[ next(ps,1) - begin ] - sda1[ next(ps,3) - begin ] );
      nu = wedgeProduct( dxdu, dxdv );
      nu /= norm( nu );
      ps->normal = nu;

      // tmp1 = dxdu at ps->neighbor[0]
      tmp1 = 0.5 * ( sda2[ nextnext(ps,0) - begin ] - sda2[ ps - begin ] );
      // tmp2 = dxdu at ps->neighbor[2]
      tmp2 = 0.5 * ( sda2[ ps - begin ] - sda2[ nextnext(ps,2) - begin ] );
      d2xdu2 = 0.5 * ( tmp1 - tmp2 );

      // tmp = dxdv at ps->neighbor[1]
      tmp1 = 0.5 * ( sda2[ nextnext(ps,1) - begin ] - sda2[ ps - begin ] );
      // tmp = dxdv at ps->neighbor[3]
      tmp2 = 0.5 * ( sda2[ ps - begin ] - sda2[ nextnext(ps,3) - begin ] );
      d2xdv2 = 0.5 * ( tmp1 - tmp2 );

      // tmp1 = dxdu at ps->neighbor[1]
      edge = ( edgeFromTable[ ps->direction ][ 1 ][ next(ps,1)->direction ] + 1 ) % 4;
      tmp1 = 0.5 * ( sda1[ next(next(ps,1),edge) - begin ] - sda1[ next(next(ps,1),(edge+2)%4) - begin ] );
      // tmp2 = dxdu at ps->neighbor[3]
      edge = ( edgeFromTable[ ps->direction ][ 3 ][ next(ps,3)->direction ] + 3 ) % 4;
      tmp2 = 0.5 * ( sda1[ next(next(ps,3),edge) - begin ] - sda1[ next(next(ps,3),(edge+2)%4) - begin ] );
      //tmp2 = 0.5 * ( _surfelDataArray[ next(ps,0) - begin ] - _surfelDataArray[ next(ps,2) - begin ] );
      d2xduv = 0.5 * ( tmp1 - tmp2 );
      //d2xduv = tmp1 - tmp2;

      double E = dxdu * dxdu;
      double F = dxdu * dxdv;
      double G = dxdv * dxdv;
      double e = (-nu) * d2xdu2;
      double f = (-nu) * d2xduv;
      double g = (-nu) * d2xdv2;

      double EG_F2 = E*G - dsquare(F);
      double eg_f2 = e*g - dsquare(f);
      double eG_2fF_gE = e*G - 2*f*F + g*E;

      minimize( m, fabs( EG_F2 ) );

      if ( fabs( EG_F2 ) > 0.1 ) {
         ps->gaussianCurvature = eg_f2 / EG_F2;
         ps->meanCurvature = eG_2fF_gE / ( 2 * EG_F2 );
      } else {
         ps->gaussianCurvature = 0;
         ps->meanCurvature = 0;
      }
      ++ps;
   }

   curvaturesArray( sda1 );

   // Averagings
   sda2.clear();
   sda2.resize( _surfelList.size() );
   globalProgressReceiver.pushInterval(averagingIterations,"Averaging");
   const int max = averagingIterations;
   while ( averagingIterations-- ) {
      Surfel *ps = _surfelList.begin();
      Surfel *end = _surfelList.end();
      while ( ps != end ) {
         convolution421( ps, _surfelList, sda1, sda2, 4, 2, 1 );
         ++ps;
      }
      sda1 = sda2;
      globalProgressReceiver.setProgress(1+max-averagingIterations);
   }
   globalProgressReceiver.popInterval();

   // Fill surfels curvature fields
   ps = _surfelList.begin();
   std::vector< SurfelData >::iterator isd = sda1.begin();
   while ( ps != end ) {
      ps->meanCurvature = isd->first;
      ps->gaussianCurvature = isd->second;
      ++ps;
      ++isd;
   }

   colorizeCurvature( _surfelList, curvatureType );
}

void
Surface::computeCurvature121Scheme( int normalEstimateIterations,
                                    int curvatureEstimateIterations,
                                    int averagingIterations,
                                    int curvatureType,
                                    bool adaptive )
{
   TRACE << "121 SCHEME\n";
   _surfelDataArray.clear();
   SurfelDataArray sda1,sda2;

   if ( adaptive )
      convolution421CentersAdaptive( normalEstimateIterations,
                                     sda1,
                                     curvatureEstimateIterations,
                                     sda2 );
   else
      convolution421Centers( normalEstimateIterations,
                             sda1,
                             curvatureEstimateIterations,
                             sda2 );

   Surfel *ps = _surfelList.begin();
   Surfel *begin = _surfelList.begin();
   Surfel *end = _surfelList.end();
   Triple<double> dxdu, dxdv, nu;
   Triple<double> d2xdu2, d2xdv2, d2xduv;
   Triple<double> tmp1, tmp2;
   int edge;
   double m = numeric_limits<double>::max();
   while ( ps != end ) {
      dxdu = sda1[ next(ps,0) - begin ] - sda1[ ps - begin ];
      dxdv = sda1[ next(ps,1) - begin ] - sda1[ ps - begin ];
      nu = wedgeProduct( dxdu, dxdv );
      nu /= norm( nu );
      ps->normal = nu;

      d2xdu2 = sda2[ next(ps,0) - begin ] + sda2[ next(ps,2)-begin ] - 2.0 * sda2[ ps-begin ];
      d2xdv2 = sda2[ next(ps,1) - begin ] + sda2[ next(ps,3)-begin ] - 2.0 * sda2[ ps-begin ];

      // dxdu at ps->neighbor[1]
      edge = ( edgeFromTable[ ps->direction ][ 1 ][ next(ps,1)->direction ] + 1 ) % 4;
      tmp1 =  sda1[ next(next(ps,1),edge) - begin ] - sda1[ next(ps,1) - begin ];
      // d2xduv
      d2xduv = tmp1 - dxdu;

      double E = dxdu * dxdu;
      double F = dxdu * dxdv;
      double G = dxdv * dxdv;
      double e = (-nu) * d2xdu2;
      double f = (-nu) * d2xduv;
      double g = (-nu) * d2xdv2;

      double EG_F2 = E*G - dsquare(F);
      double eg_f2 = e*g - dsquare(f);
      double eG_2fF_gE = e*G - 2*f*F + g*E;

      minimize( m, fabs( EG_F2 ) );

      if ( fabs( EG_F2 ) > 0.1 ) {
         ps->gaussianCurvature = eg_f2 / EG_F2;
         ps->meanCurvature = eG_2fF_gE / ( 2 * EG_F2 );
      } else {
         ps->gaussianCurvature = 0;
         ps->meanCurvature = 0;
      }
      ++ps;
   }

   curvaturesArray( sda1 );

   // Averagings
   sda2.clear();
   sda2.resize( _surfelList.size() );
   globalProgressReceiver.pushInterval(averagingIterations,"Averaging");
   const int max = averagingIterations;
   while ( averagingIterations-- ) {
      Surfel *ps = _surfelList.begin();
      Surfel *end = _surfelList.end();
      while ( ps != end ) {
         convolution421( ps, _surfelList, sda1, sda2, 4, 2, 1 );
         ++ps;
      }
      sda1 = sda2;
      globalProgressReceiver.setProgress(1+max-averagingIterations);
   }
   globalProgressReceiver.popInterval();

   // Fill surfels curvature fields
   ps = _surfelList.begin();
   std::vector< SurfelData >::iterator isd = sda1.begin();
   while ( ps != end ) {
      ps->meanCurvature = isd->first;
      ps->gaussianCurvature = isd->second;
      ++ps;
      ++isd;
   }

   colorizeCurvature( _surfelList, curvatureType );
}


void
Surface::convolution421NormalsAndArea(int iterations,
                                      SurfelDataArray & data )
{
   SurfelDataArray sdaDest( _surfelList.size() );
   SurfelData sd;
   double totalArea = 0.0;
   double cosine, scalar, normA, normB;

   computeNormals( iterations, _settings->normalsEstimateAdaptive() );
   totalArea = 0.0;
   Surfel *ps = _surfelList.begin();
   Surfel *end  = _surfelList.end();
   while ( ps < end ) {
      ps->color = static_cast<UChar>( (250 * fabs( ps->normal.third )) + 1 );
      scalar = ps->normal * Triple<double>( _normals[ ps->direction ] );
      normA = norm( ps->normal );
      normB = norm( _normals[ ps->direction ] );
      cosine = scalar / (normA * normB);
      totalArea += cosine;
      ++ps;
   }
   normalsArray( data );
}

void
Surface::convolutionNormalsAndAngleSphere( int iterations,
                                           SurfelDataArray & data )
{
   Triple<double> trueNormal;
   double errorSum;
   double squaredErrorSum;
   double normA,normB,cosine,scalar;
   int n = iterations;
   Size cardinal;

   _surfelList.rotate( _rotation );

   computeNormals( iterations, _settings->normalsEstimateAdaptive() );

   Surfel *ps = _surfelList.begin();
   Surfel *end = _surfelList.end();
   errorSum = squaredErrorSum = 0.0;
   while ( ps < end ) {
      ps->color = static_cast<UChar>( 120 - 100 * ps->normal.third );
      trueNormal = ps->center;
      scalar = ps->normal * trueNormal;
      normA = norm( ps->normal );
      normB = norm( trueNormal );
      cosine = scalar / (normA * normB);
      errorSum += acos( cosine );
      squaredErrorSum += acos( cosine ) * acos( cosine );
      ++ps;
   }
   cardinal = _surfelList.size();

   TRACE << (n - iterations) << " "
         << (180 / M_PI) * (errorSum / cardinal) << " "
         <<  (180/M_PI)* sqrt((squaredErrorSum/cardinal) -
                              ((errorSum / cardinal)
                               * (errorSum / cardinal)))
          << endl;

   normalsArray( data );
}

Surfel*
Surface::getCloserSurfel( int x, int y,
                          int width, int height,
                          int * vertex, int * edge )
{
   Surfel *result = 0;
   Triple<float> mousePoint( ( x - ( width >> 1 ) ) / zoom(),
                             ( y - ( height >> 1 ) ) / zoom(),
                             0 );
   Triple<float> surfelCenter;
   Triple<float> u,v,p;
   double alpha1 = 0.0, alpha2 = 0.0;

   Surfel **begin = sortedSurfelsArray();
   Surfel **pps = sortedSurfelsEnd();
   do {
      pps--;
      surfelCenter = (*pps)->rotatedCenter;
      surfelCenter.third = 0;
      p = mousePoint - surfelCenter;
      if ( norm( p ) <= 3.0 ) {
         u = 0.5 * (_rotatedFaces[(*pps)->direction][1] - _rotatedFaces[(*pps)->direction][0]);
         u.third = 0;
         v = 0.5 * (_rotatedFaces[(*pps)->direction][2] - _rotatedFaces[(*pps)->direction][1]);
         v.third = 0.0;
         double invDet = 1.0 / ( u.first * v.second - u.second * v.first);
         alpha1 = invDet * ( p.first * v.second - p.second * v.first );
         alpha2 = invDet * ( -p.first * u.second + p.second * u.first );
         if ( ( fabs( alpha1 ) <= 1 ) && ( fabs( alpha2 ) <= 1 ) )
            result = *pps;
      }
   } while ( ( pps != begin ) && !result );

   if ( result && vertex ) {
      *vertex = 0;
      if ( alpha1 >= 0 ) {
         if ( alpha2 >= 0 )
            *vertex = 2;
         else
            *vertex = 1;
      } else {
         if ( alpha2 >= 0 )
            *vertex = 3;
         else
            *vertex = 0;
      }
   }

   if ( result && edge ) {
      *edge = 0;
      if ( alpha1 >= 0 ) {
         if ( fabs( alpha1 ) > fabs( alpha2 ) )
            *edge = 1;
         else if ( alpha2 >= 0 ) *edge = 2;
         else *edge = 0;
      } else {
         if ( fabs( alpha1 ) > fabs( alpha2 ) )
            *edge = 3;
         else if ( alpha2 >= 0 ) *edge = 2;
         else *edge = 0;
      }
   }

   return result;
}


void
Surface::markSurfel( int offset )
{
   _markedSurfel = offset;
}

void
Surface::markSurfelList( vector<Surfel*> & v )
{
   vector<Surfel*>::iterator i, end;
   end = v.end();
   i = v.begin();
   while ( i != end ) {
      (*i)->color = 255;
      i++;
   }
}

void
Surface::markSliceCurve( Surfel * ps )
{
   Surfel * ps2 = ps;
   ps->color = 254;
   do {
      switch ( ps2->direction ) {
      case 4:
      case 3:
      case 5:
         ps2 = ps2->neighbors[2];
         break;
      case 2:
         ps2 = ps2->neighbors[0];
         break;
      default:
         return;
      }
      ps2->color = 254;
   } while ( ps2 != ps );
}

void
Surface::setImage( AbstractImage * image,
                   bool fullBox,
                   ViewParamsFlags viewParams,
                   int volumeAdjacency )
{
   TRACE << "Surface::setImage() BEGIN" << std::endl;

   _valid = false;

   // Set a basic shading
   setShading(ColorFromOrientation);
   _image = image;
   if ( !_image ) {
      _surfelList.clear();
      clearSortedSurfelLists();
      _surfelDataArray.clear();
      _validNormals = true;
      emit changed();
      return;
   }

   // Build the surfel list

   SurfelListBuilder builder(_surfelList,fullBox,volumeAdjacency, 1.0 );
   image->accept(builder);

   // Build the surfel graph

   TRACE << "Building surfel graph (" << _surfelList.size() << " surfels)..." << std::flush;

   emit info("Bulding surfel graph...");

   if ( globalSettings->multiThreading() ) {
      TIME_MEASURE_START("GraphBuilder_MT");
      const unsigned int threadCount = ( globalSettings->maxThreads() == -1 ) ? QThreadPool::globalInstance()->maxThreadCount() : globalSettings->maxThreads();
      const unsigned long surfelsCount = _surfelList.end() - _surfelList.begin();
      const unsigned long interval = surfelsCount / threadCount;
      for ( unsigned long t = 0; t < threadCount - 1; ++t ) {
         Surfel * tStart = _surfelList.begin() + t*interval;
         Surfel * tStop = tStart + interval;
         SurfelGraphBuilderThread * thread = new SurfelGraphBuilderThread(image,
                                                                          new SurfelGraphBuilder(_surfelList,
                                                                                                 tStart,
                                                                                                 tStop,
                                                                                                 fullBox,
                                                                                                 volumeAdjacency));
         QThreadPool::globalInstance()->start(thread);
      }
      Surfel * tStart = _surfelList.begin() + (threadCount-1)*interval;
      SurfelGraphBuilderThread * thread = new SurfelGraphBuilderThread(image,
                                                                       new SurfelGraphBuilder(_surfelList,
                                                                                              tStart,
                                                                                              _surfelList.end(),
                                                                                              fullBox,
                                                                                              volumeAdjacency));
      QThreadPool::globalInstance()->start(thread);
      QThreadPool::globalInstance()->waitForDone();
      TRACE << "done.\n";
      TIME_MEASURE_STOP("GraphBuilder_MT");
   } else {
      TIME_MEASURE_START("GraphBuilder");
      SurfelGraphBuilder * graphBuilder = new SurfelGraphBuilder(_surfelList,
                                                                 _surfelList.begin(),
                                                                 _surfelList.end(),
                                                                 fullBox,
                                                                 volumeAdjacency );
      SurfelGraphBuilderThread graphBuilderThread(image,graphBuilder);
      graphBuilderThread.run();
      TRACE << "done.\n";
      TIME_MEASURE_STOP("GraphBuilder");
   }

   emit info("Computing bounding box...");
   _surfelList.computeBBox( true, SurfelList::OriginalPosition );
   emit info("Computing pre-sorted surfel arrays...");
   buildSSL();
   rotateSurfels( RotateVisibleSurfels );
   if ( viewParams == ResetViewParams )
      resetPosition();
   resetLight();

   // CM
   if ( globalSettings->computeVextexIndices() ) {
      TIME_MEASURE_START("Indices");
      emit info("Computing vertex/edge indices...");
      buildSurfelVertexIndicesArray();
      buildSurfelEdgeIndicesArray();
      TIME_MEASURE_STOP("Indices");
   } else {
      _surfelVertexIndicesArray.clear();
      _surfelEdgeIndicesArray.clear();
   }
   // Invalidate normals
   _validNormals = false;

   TRACE << "Surface::setImage() END" << std::endl;
   _valid = true;
   emit changed();
}

QString
Surface::surfelInfo( Surfel * ps ) {
   QString r;
   SurfelDataArray::iterator p;
   Size position = ps - _surfelList.begin();
   r += QString("# %1\n").arg( position );

   r += QString("Normal: %1 %2 %3\n")
         .arg( ps->normal.first ).arg( ps->normal.second ).arg( ps->normal.third );

   if ( position < _surfelDataArray.size() ) {
      p = _surfelDataArray.begin() + position;
      r += QString("Data: %1 %2 %3  |%4|\n")
            .arg( p->first, 0, 'f', 20 )
            .arg( p->second, 0, 'f', 20 )
            .arg( p->third, 0, 'f', 20 )
            .arg( norm( *p ) );
   }

   double maxi = ps->meanCurvature + sqrt( dsquare(ps->meanCurvature)
                                           - ps->gaussianCurvature );
   // double x = dsquare(ps->meanCurvature) - ps->gaussianCurvature;

   r += QString("Curvature:\n"
                "  Mean=%3 (1/Mean=%4) Gauss=%5\n"
                "  Maxi=%6")
         .arg( ps->meanCurvature )
         .arg( 1 / ps->meanCurvature )
         .arg( ps->gaussianCurvature )
         .arg( maxi );
   return r;
}

void
Surface::resetPosition()
{
   _rotationQ.set( 0.0, 0.0, 0.0, 1.0 );
   _rotationQ.rotationMatrix( _rotation );
   rotateSurfels( RotateVisibleSurfels );
}

void
Surface::rotateSurfels( RotationFlag flags )
{
   Triple<double> point;
   int i, f;

   /* Rotation of the normals */
   for ( i = 0; i < 6; i++ ) {
      _rotatedNormals[ i ] = _rotation * _normals[ i ];
      _rotatedEdgesDirections[ i ]  = _rotation * _edgesDirections[i];
   }

   /* Compute visibles directions */
   for ( i = 0; i < 6; i++ )
      _visibleDirections[i] = ( _rotatedNormals[i].third < 0 );

   /* Rotate the surfel centers */
   switch ( flags ) {
   case RotateAllSurfels:
      _surfelList.rotate( _rotation );
      break;
   case RotateVisibleSurfels:
      _ssl[ _visibleDirections[0] ][ _visibleDirections[2] ][ _visibleDirections[4] ].rotate( _rotation, _aspect );
      break;
   case RotateNoSurfel:
      break;
   }


   /* Rotation of the faces */
   if ( _aspectEnabled )
      for ( f = 0; f < 6; f++ )
         for ( i = 0; i < 4; i++ ) {
            point.first = _aspect.first * _referenceFaces[f][i].first;
            point.second = _aspect.second * _referenceFaces[f][i].second;
            point.third = _aspect.third * _referenceFaces[f][i].third;
            _rotatedFaces[f][i] = _rotation * point;
         }
   else
      for ( f = 0; f < 6; f++ )
         for ( i = 0; i < 4; i++ )
            _rotatedFaces[f][i] = _rotation * _referenceFaces[f][i];

   buildSurfelsImages();
}


void
Surface::rotate( double theta_x, double theta_y, double theta_z,
                 RotationFlag flags )
{
   _rotationQ *= Quaternion::rotation( Triple<double>(1.0, 0.0, 0.0 ), theta_x );
   _rotationQ *= Quaternion::rotation( Triple<double>(0.0, 1.0, 0.0 ), theta_y );
   _rotationQ *= Quaternion::rotation( Triple<double>(0.0, 0.0, 1.0 ), theta_z );
   if ( ++_rotationsCount > 90 ) {
      _rotationQ.normalize();
      _rotationsCount = 0;
   }
   _rotationQ.rotationMatrix( _rotation );
   rotateSurfels( flags );
}

void
Surface::rotate( const Quaternion & q,
                 RotationFlag flags )
{
   _rotationQ *= q;
   if ( ++_rotationsCount > 90 ) {
      _rotationQ.normalize();
      _rotationsCount = 0;
   }
   _rotationQ.rotationMatrix( _rotation );
   rotateSurfels( flags );
}

void
Surface::setRotation(const Quaternion & q, RotationFlag flags )
{
   _rotationQ = q;
   _rotationQ.rotationMatrix( _rotation );
   rotateSurfels( flags );
}

void
Surface::rotateLight( const Quaternion & q )
{
   _lightRotationQ *= q;
   if ( ++_rotationsCount > 90 ) {
      _lightRotationQ.normalize();
      _rotationsCount = 0;
   }
   _lightRotationQ.rotationMatrix( _lightRotation );
   _lightPosition = _lightRotation * _originalLightPosition;
}

void
Surface::rotateLight( double theta_x, double theta_y, double theta_z )
{
   _lightRotationQ *= Quaternion::rotation( Triple<double>(1.0, 0.0, 0.0 ), theta_x );
   _lightRotationQ *= Quaternion::rotation( Triple<double>(0.0, 1.0, 0.0 ), theta_y );
   _lightRotationQ *= Quaternion::rotation( Triple<double>(0.0, 0.0, 1.0 ), theta_z );
   _lightRotationQ.rotationMatrix( _lightRotation );
   _lightPosition = _lightRotation * _originalLightPosition;
}

void
Surface::pushLight() {
   _originalLightPosition *= 0.9;
   rotateLight( 0, 0, 0 );
}

void
Surface::pullLight() {
   _originalLightPosition *= 1.1;
   rotateLight( 0, 0, 0 );
}

void
Surface::resetLight()
{
   _surfelList.computeBBox( false, SurfelList::RotatedPosition );
   Triple<double> range = _surfelList.bbMax() - _surfelList.bbMin();
   double depth = std::max( range.first, std::max( range.second, range.third ) );
   _originalLightPosition.set( 0, 0, -5 * depth );
   _lightPosition = _originalLightPosition;
   _lightRotation.identity();
}

void
Surface::translate( Triple<double> delta )
{
   _surfelList.translate( delta );
}

void
Surface::viewCenterShift( int dx, int dy )
{
   _viewCenterShift.first = dx;
   _viewCenterShift.second = dy;
}

void
Surface::viewCenterReset()
{
   _viewCenterShift = 0;
}

void
Surface::buildSSL()
{
   Matrix old_rot;
   TRACE << "buildSSL(): BEGIN\n";
   TIME_MEASURE_START("buildSSL");

   old_rot = _rotation;
   Size l = 0, surfelsPerLayer = 1;

   l = 8 * std::max( _image->width(),
                     std::max ( _image->height(),
                                _image->depth() ) );
   while ( surfelsPerLayer < l )
      surfelsPerLayer <<= 1;

   globalProgressReceiver.pushInterval(5,"Building sorted surfels lists...");

   resetPosition();
   rotate( -0.2f, -0.2f, 0.0f, RotateAllSurfels );	/* 011001 */
   _surfelList.computeBBox( true, SurfelList::RotatedPosition );
   _ssl[0][1][0].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            false );
   _ssl[1][0][1].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            true );
   globalProgressReceiver.setProgress(1);

   resetPosition( );
   rotate( -0.7f, 0.7f, 0.0f, RotateAllSurfels );	/* 101001 */
   _surfelList.computeBBox( true, SurfelList::RotatedPosition );
   _ssl[1][1][0].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            false );
   _ssl[0][0][1].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            true );
   globalProgressReceiver.setProgress(2);

   resetPosition( );
   rotate( 0.7f, -0.7f, 0.0f, RotateAllSurfels );	/* 010101 */
   _surfelList.computeBBox( true, SurfelList::RotatedPosition );
   _ssl[0][0][0].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            false );
   _ssl[1][1][1].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            true );
   globalProgressReceiver.setProgress(3);

   resetPosition( );
   rotate( 0.7f, 0.7f, 0.0f, RotateAllSurfels );	/* 100101 */
   _surfelList.computeBBox( true, SurfelList::RotatedPosition );
   _ssl[1][0][0].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            false );
   _ssl[0][1][1].buildFromSurfelListLinear( _surfelList,
                                            _visibleDirections,
                                            surfelsPerLayer,
                                            true );
   globalProgressReceiver.setProgress(4);

   _rotation = old_rot;
   rotateSurfels();

   globalProgressReceiver.setProgress(5);

   globalProgressReceiver.popInterval();

   TRACE << "buildSSL(): END\n";
   TIME_MEASURE_STOP("buildSSL");
}

void
Surface::zoomFit( int width, int height )
{
   _surfelList.bbValid( false );
   _surfelList.computeBBox( true, SurfelList::RotatedPosition );
   if ( _surfelList.bbMax().first - _surfelList.bbMin().first < 0.5 )
      zoom( 10 ) ;
   else {
      double z = std::min( ( width - 2 ) /
                           abs( _surfelList.bbMax().first - _surfelList.bbMin().first ),
                           ( height - 2 ) /
                           abs( _surfelList.bbMax().second - _surfelList.bbMin().second ) );
      zoom( std::min( z, HALF_ZOOM / 1.45 ) );
   }
}

void
Surface::zoom( double x )
{
   _zoom = x;
   if ( _zoom > 1 )
      _zoom = floor( _zoom * 4 ) / 4;
   else
      _zoom = floor( _zoom * 100 ) / 100;
   buildSurfelsImages();
}

void
Surface::zoomIn( )
{
   if ( _zoom < 1.0 )
      zoom( std::min( _zoom * 2.0, HALF_ZOOM / 1.45 ) );
   else
      zoom( std::min( _zoom * 1.5, HALF_ZOOM / 1.45 ) );
}

void
Surface::zoomOut( )
{
   zoom( std::max( _zoom / 1.5, 0.01 ) );
}

void
Surface::markSlice( int axis, int slice )
{
   _markedSliceAxis = axis;
   _markedSlice = slice;
}

void
Surface::activateSliceMark( bool on )
{
   if ( on ) {
      _markedSliceAxis = 0;
      _markedSlice = 0;
   } else {
      _markedSliceAxis = -1;
      _markedSlice = -1;
   }
   setShading( _shading );
}

void
Surface::setSurfelColorsFromVoxels()
{
   Surfel *ps = _surfelList.begin();
   Surfel *end = _surfelList.end();
   while ( ps < end ) {
      _image->getGrayLevel(ps->voxel,ps->color);
      ps++;
   }
}

void
Surface::setSurfelColorsFromOrientation()
{
   Surfel *ps = _surfelList.begin();
   Surfel *end = _surfelList.end();
   UChar facesColors[ 6 ];
   for ( int dir = 0; dir < 6; dir++ )
      facesColors[ dir ] =
            (COLORMAP_SIZE - 1) -
            static_cast<UChar>(fabs(_rotatedNormals[dir].third) * (COLORMAP_SIZE - 2));

   while ( ps != end ) {
      ps->color = facesColors[ps->direction];
      ps++;
   }
}

bool
Surface::simpleVoxel()
{
   Config cnf = _image->config( _clickedVoxel[0], _clickedVoxel[1], _clickedVoxel[2] );
   return SkelSimpleTests[_adjacency] (cnf);
}

bool
Surface::saveViewParams( const char *filename )
{
   char name[1024];
   if ( strstr( filename, ".qvp" ) )
      strcpy( name, filename );
   else sprintf( name, "%s.qvp", filename );
   ofstream file( name );
   if ( !file ) {
      ERROR << "File [" << filename << " ] could not be created.";
      return false;
   }
   file << "ROTATION QUATERNION\n";
   file << _rotationQ << "\n";
   file << "ZOOM\n" << _zoom << endl;
   file.close();
   return true;
}

bool
Surface::loadViewParams( const char *filename )
{
   char line[1024];
   ifstream file( filename );
   if ( !file ) {
      ERROR << "File [" << filename << "] could not be open\n";
      return false;
   }
   file.getline( line, 1024 );
   TRACE << "HEADER:[" << line << "]\n";
   if ( ! strstr( line, "ROTATION QUATERNION" ) ) {
      ERROR << "File not recognized [" << filename << "]\n";
      return false;
   }
   file >> _rotationQ;
   do {
      file.getline( line, 1024 );
   } while ( !strstr( line, "ZOOM" ) );
   file >> _zoom;
   TRACE << "zoom=" << _zoom << endl;
   file.close();
   rotate( 0, 0, 0, RotateAllSurfels );
   return true;
}

void
Surface::saveSurfelsColors(  char *filename )
{
   Surfel *ps;
   Surfel *end;
   ofstream file( filename );

   if ( !file ) {
      ERROR << "File [" << filename << "] could not be created\n";
   }

   file << "SURFELS\n\n";
   ps = _surfelList.begin();
   end = _surfelList.end();

   while (ps < end) {
      file << "F "
           << ps->voxel << " "
           << ps->direction << " "
           << ps->color << endl;
      ps++;
   }
   file << "." << endl;
   file.close();
}

void
Surface::loadSurfelsColors( char *filename )
{
   ifstream file( filename );
   char message[255];
   Surfel *ps;
   int direction, color;
   Triple<SSize> voxel;

   if ( !file ) {
      ERROR << "File [" << filename << "%s] could not be open.\n";
      return;
   }

   file >> message;
   if ( !strstr( message, "SURFELS") ) {
      ERROR << " File [" << filename << "%s] is not in a proper format\n";
      file.close();
      return;
   }

   do {
      file >> message;
      if ( !strcmp( message, "F" ) ) {
         file >> voxel.first;
         file >> voxel.second;
         file >> voxel.third;
         file >> direction;
         file >> color;
         ps = _surfelList.quickFind( voxel, direction );
         if ( ps ) ps->color = color;
      }
   } while ( file && strcmp( message, "." ) );
   file.close();
}

QRgb
Surface::surfelColor( Surfel * ps )
{
   return (this->*_surfelColorMethod)( ps );
}

QRgb
Surface::surfelColorOrientation( Surfel * ps ) const
{
   return static_cast<QRgb>( _colors[ ps->direction ] );
}

QRgb
Surface::surfelColorDepth( Surfel * ps ) const
{
   return static_cast<UChar>( 1 +
                              ( ( ps->rotatedCenter.third
                                  + 0.5 * _surfelList.maxDepthRange() )
                                / _surfelList.maxDepthRange() )
                              * (COLORMAP_SIZE - 2) );
}

QRgb Surface::surfelColorFixed( Surfel * ps ) const
{
   return ps->color;
}

QRgb Surface::surfelColorLight( Surfel * ps ) const
{
   // Diffuse
   const Triple<double> n = _rotation * ps->normal;
   Triple<double> light = _lightPosition - ps->rotatedCenter;
   light /= norm( light );
   const double cos_theta = light * n;

   // Specular
   Triple<double> eye = Triple<double>( 0, 0, -10000 ) - ps->rotatedCenter;
   eye /= norm( eye );
   Triple<double> r( ( 2.0 * ( n * eye ) ) * n - eye );
   r /= norm( r );
   const double cos_alpha = light * r;
   const double is  = cos_alpha > 0 ? (0.3*exp(30*log(cos_alpha))) : 0;
   ps->color = (cos_theta>=0) ? static_cast<UChar>(255*(0.7*cos_theta+is)) : 1;
   return ps->color;
}

QRgb Surface::surfelColorVoxelGray( Surfel * ps ) const
{
   UChar value;
   _image->getGrayLevel(ps->voxel,value);
   return value;
}

QRgb Surface::surfelColorVoxelRGB( Surfel * ps ) const
{
   UChar r,g,b;
   _image->getColor(ps->voxel,r,g,b);
   return qRgb(r,g,b);
}

QRgb
Surface::surfelColorLightedVoxelGray( Surfel * ps ) const
{
   // Diffuse
   UChar grayLevel;
   _image->getGrayLevel(ps->voxel,grayLevel);
   const Triple<double> estimatedNormal = _rotation * ps->normal;
   Triple<double> light = _lightPosition - ps->rotatedCenter;
   light /= norm( light );
   const double cos_theta = light * estimatedNormal;

   // Specular
   Triple<double> eye = Triple<double>( 0, 0, -10000 ) - ps->rotatedCenter;
   eye /= norm( eye );
   Triple<double> r( ( 2.0 * ( estimatedNormal * eye ) ) * estimatedNormal - eye );
   r /= norm( r );
   const double cos_alpha = light * r;
   const double is = (cos_alpha>0) ? (0.3*exp(30*log(cos_alpha))) : 0;
   ps->color = (cos_theta>0) ? static_cast<UChar>(grayLevel*(0.7*cos_theta+is)) : 1;
   return ps->color;
}

QRgb
Surface::surfelColorLightedVoxelColorMap( Surfel * ps ) const
{
   if ( ! _lastColorMap ) return 0;
   // Diffuse
   const Triple<double> estimatedNormal = _rotation * ps->normal;
   Triple<double> light = _lightPosition - ps->rotatedCenter;
   light /= norm( light );
   const double cos_theta = light * estimatedNormal;

   // Specular
   Triple<double> eye = Triple<double>( 0, 0, -10000 ) - ps->rotatedCenter;
   eye /= norm( eye );
   Triple<double> r( ( 2.0 * ( estimatedNormal * eye ) ) * estimatedNormal - eye );
   r /= norm( r );
   const double cos_alpha = light * r;
   const double is = (cos_alpha>0) ? (0.3*exp(30*log(cos_alpha))) : 0;

   UChar value;
   _image->getGrayLevel(ps->voxel,value);
   QColor color( _lastColorMap->qcolor( value ) );

   int h,s,v;
   color.getHsv( &h, &s, &v );
   v = (cos_theta>0) ? static_cast<int>(v*(0.7*cos_theta+is)) : 0;
   if ( v < 0 ) v = 0;
   if ( v > 255 ) v = 255;
   color.setHsv( h, s, v );
   return color.rgb();
}

QRgb
Surface::surfelColorLightedVoxelLUT( Surfel * ps ) const
{
   // Diffuse
   const Triple<double> estimatedNormal = _rotation * ps->normal;
   Triple<double> light = _lightPosition - ps->rotatedCenter;
   light /= norm( light );
   const double cos_theta = light * estimatedNormal;

   // Specular
   Triple<double> eye = Triple<double>( 0, 0, -10000 ) - ps->rotatedCenter;
   eye /= norm( eye );
   Triple<double> r( ( 2.0 * ( estimatedNormal * eye ) ) * estimatedNormal - eye );
   r /= norm( r );
   const double cos_alpha = light * r;
   const double is = (cos_alpha>0) ? (0.3*exp(30*log(cos_alpha))) : 0;

   Triple<UChar> c = _image->getColorFromLUT(ps->voxel,_lut);
   QColor color(c.first,c.second,c.third);

   int h,s,v;
   color.getHsv( &h, &s, &v );
   v = (cos_theta>0) ? static_cast<int>(v*(0.7*cos_theta+is)) : 0;
   if ( v < 0 ) v = 0;
   if ( v > 255 ) v = 255;
   color.setHsv( h, s, v );
   return color.rgb();
}


QRgb Surface::surfelColorLightedVoxelRGB( Surfel * ps ) const
{
   // Diffuse
   const Triple<double> estimatedNormal = _rotation * ps->normal;
   Triple<double> light = _lightPosition - ps->rotatedCenter;
   light /= norm( light );
   const double cos_theta = light * estimatedNormal;

   // Specular
   Triple<double> eye = Triple<double>( 0, 0, -10000 ) - ps->rotatedCenter;
   eye /= norm( eye );
   Triple<double> r( ( 2.0 * ( estimatedNormal * eye ) ) * estimatedNormal - eye );
   r /= norm( r );
   const double cos_alpha = light * r;
   const double is = (cos_alpha>0) ? (0.3*exp(30*log(cos_alpha))) : 0;

   UChar red,green,blue;
   _image->getColor(ps->voxel,red,green,blue);
   QColor color(red,green,blue);

   int h,s,v;
   color.getHsv( &h, &s, &v );
   v = (cos_theta>0) ? static_cast<int>(v*(0.7*cos_theta+is)) : 0;
   if ( v < 0 ) v = 0;
   if ( v > 255 ) v = 255;
   color.setHsv( h, s, v );
   return color.rgb();
}

QRgb
Surface::surfelColorConst( Surfel * ) const
{
   return 128;
}

Triple<float>
Surface::getSurfelColor( Surfel *s, const ColorMap & colormap ) const
{
  Triple<float> color;
  int c = getSurfelColor( s );
  bool useColormap = true;
  switch ( _shading ) {
  case Surface::ColorFromOrientation:
  case Surface::ColorFromDepth:
  case Surface::ColorFixed:
  case Surface::ColorFromCurvature:
  case Surface::ColorWhite:
  case Surface::ColorFromLight:
    useColormap = false;
    break;
  case Surface::ColorFromLightedMaterial:
    if ( _image->bands() == 3 ) {
      useColormap = false;
    } else {
      useColormap = true;
    }
    break;
  case Surface::ColorFromLightedColorMap:
  case Surface::ColorFromLightedLUT:
  case Surface::ColorFromMaterial:
    if ( _image->bands() != 3 ) {
      useColormap = true;
    } else {
      useColormap = false;
    }
    break;
  default:
    useColormap = false;
    break;
  }
  if ( useColormap ) {
    color.first = colormap.qcolor( c ).red() / 255.0F;
    color.second = colormap.qcolor( c ).green() / 255.0F;
    color.third = colormap.qcolor( c ).blue() / 255.0F;
  } else {
    color.first = qRed(c) / 255.0F;
    color.second = qGreen(c) / 255.0F;
    color.third = qBlue(c) / 255.0F;
  }
  return color;
}



void
Surface::drawSurface( QImage & image, ColorMap & colormap )
{
   _lastColorMap = & colormap;
   if ( image.height() < 2 )
      return;

   /* Fill background */
   UChar * prevScanLine = image.scanLine(0);
   UChar * scanLine = image.scanLine(1);
   const UChar * bgLine = colormap.backgroundLine();
   const int bytesPerLine = image.scanLine(1) - image.scanLine(0);
   const int viewHeight = image.height();
   memcpy( prevScanLine, bgLine, bytesPerLine );
   for ( int line = 1; line != viewHeight;
         ++line, prevScanLine = scanLine, scanLine += bytesPerLine  )
      memcpy( scanLine, prevScanLine, bytesPerLine );

   if ( !_image || !_valid  ) return;

   /* For color from orientation */
   _colors[0] = static_cast<UChar>( fabs( _rotatedNormals[0].third ) * 255);
   _colors[1] = static_cast<UChar>( fabs( _rotatedNormals[1].third ) * 255);
   _colors[2] = static_cast<UChar>( fabs( _rotatedNormals[2].third ) * 255);
   _colors[3] = static_cast<UChar>( fabs( _rotatedNormals[3].third ) * 255);
   _colors[4] = static_cast<UChar>( fabs( _rotatedNormals[4].third ) * 255);
   _colors[5] = static_cast<UChar>( fabs( _rotatedNormals[5].third ) * 255);
   _zMin = _surfelList.bbMin().third;
   _zMax = _surfelList.bbMax().third;
   _zWidth = _zMax - _zMin;


   /* Draw surface */
   if (globalSettings->multiThreading()) {
      TIME_MEASURE_START("DRAW_MT");
      _threadedSurfacePainter.draw( _surfacePainter, image, colormap, visibleSurfelsBegin(), visibleSurfelsEnd(), _cutPlaneDepth );
      TIME_MEASURE_STOP("DRAW_MT");
   } else {
      TIME_MEASURE_START("DRAW");
      _surfacePainter->draw( image, colormap, visibleSurfelsBegin(), visibleSurfelsEnd(), _cutPlaneDepth );
      TIME_MEASURE_STOP("DRAW");
   }
}

void
Surface::drawFineSurface( QImage & image, ColorMap & colormap )
{
   if ( _settings->transparency() )
      drawFine( image, colormap, _cutPlaneDepth, _settings->opacity() );
   else
      drawFine( image, colormap, _cutPlaneDepth );
}

void
Surface::drawBoundingBox( QImage & image, ColorMap & colormap )
{
   Triple<double> bbMax = _surfelList.bbMinOriginal();
   Triple<double> bbMin = _surfelList.bbMaxOriginal();
   Triple<double> vertices[8] = {
      Triple<double>( bbMin),                                       // 0
      Triple<double>( bbMax.first, bbMin.second, bbMin.third ),     // 1
      Triple<double>( bbMax.first, bbMax.second, bbMin.third ),     // 2
      Triple<double>( bbMin.first, bbMax.second, bbMin.third ),     // 3
      Triple<double>( bbMin.first, bbMin.second, bbMax.third ),     // 4
      Triple<double>( bbMax.first, bbMin.second, bbMax.third ),     // 5
      Triple<double>( bbMax),                                       // 6
      Triple<double>( bbMin.first, bbMax.second, bbMax.third )      // 7
   };
   const int facesVertices[6][4] = {
      { 3, 0, 4, 7 },
      { 1, 2, 6, 5 },
      { 0, 1, 5, 4 },
      { 2, 3, 7, 6 },
      { 3, 2, 1, 0 },
      { 4, 5, 6, 7 }
   };

   for ( int i=0; i < 8; ++i ) {
      vertices[i] *= _zoom;
      vertices[i] = _rotation * vertices[i];
   }

   QPainter painter( &image );
   image.fill( colormap.backgroundColor().rgb() );
   QPolygon polygon(4);
   painter.setPen( QColor("red") );
   painter.setBrush( QBrush( QColor(200,200,200) ) );

   int center_x = image.width() / 2;
   int center_y = image.height() / 2;
   for ( int direction=0; direction < 6; ++direction ) {
      if ( !_visibleDirections[direction] ) continue;
      for ( int i = 0; i < 4; i++) {
         const Triple<double> & point = vertices[ facesVertices[direction][i] ];
         polygon[i].setX( rounded( center_x + point.first ) );
         polygon[i].setY( rounded( center_y +  point.second ) );
      }
      painter.drawPolygon( polygon );
   }
}

void
Surface::highlightSurfel( QImage & image, ColorMap & colormap, Surfel * ps )
{
   if ( image.height() < 2 ) return;
   int i, j, center_x, center_y;
   UChar color = surfelColor( ps );
   UChar **pixels = colormap.pixels();
   UChar *pixelLine = pixels[ color ];
   int *plength;
   int *pstart;
   int row;
   unsigned char *line_beg;
   int viewWidth = image.width();
   int viewHeight = image.height();
   int xLimitMax = viewWidth - _maxSurfelWidth;
   int yLimitMax = viewHeight - _maxSurfelHeight;
   int bytesPerLine = image.scanLine(1) - image.scanLine(0);
   center_x = ( viewWidth / 2 );
   center_y = ( viewHeight / 2);
   int direction = ps->direction;

   j = roundInt( center_x + dx[direction] + _zoom * ps->rotatedCenter.first );
   i = roundInt( center_y + dy[direction] - _zoom * ps->rotatedCenter.second );
   if ( j >= 0 && i >= 0 &&
        j < xLimitMax && i < yLimitMax  ) {
      plength = lengths[direction];
      pstart = starts[direction];
      line_beg =  image.scanLine( i ) + ( j << 2 );
      row = height[direction];
      while ( row--) {
         memcpy( line_beg + (*pstart),
                 pixelLine,
                 *plength );
         QRgb* ppixel = reinterpret_cast<QRgb*>( line_beg + (*pstart) );
         int n = (  (*plength) >> 2 );
         while ( n-- ) { *ppixel = ~(*ppixel); ppixel++; }
         *reinterpret_cast<QRgb*>( line_beg + (*pstart) ) = ~ _edgeRgb;
         *reinterpret_cast<QRgb*>( line_beg + (*pstart) + *plength )
               = ~ _edgeRgb;
         line_beg += bytesPerLine;
         plength++;
         pstart++;
      }
   }
}

void
Surface::drawSurfel( QImage & image, ColorMap & colormap,
                     Surfel *surfel,
                     int vertex, int edge )
{
   int center_x = ( image.width() / 2 );
   int center_y = ( image.height() / 2);
   const QColor *cmapColors = colormap.qcolors();
   QPolygon polygon(4);
   QPainter painter( &image );
   painter.setPen( _edgeColor );
   int direction = surfel->direction;
   int color = 255 - (this->*_surfelColorMethod)( surfel );
   for ( int i = 0; i < 4; i++ ) {
      polygon[i].setX( rounded( center_x
                                + _zoom * surfel->rotatedCenter.first
                                + _zoom * _rotatedFaces[direction][i].first ) );
      polygon[i].setY( rounded( center_y
                                + ( _zoom * surfel->rotatedCenter.second  +
                                    _zoom * _rotatedFaces[direction][i].second) ) );
   }
   painter.setBrush( QBrush( cmapColors[ color ] ) );
   painter.drawPolygon( polygon );

   painter.setPen(  cmapColors[ (this->*_surfelColorMethod)( surfel ) ] );
   polygon.clear();
   polygon.push_back( QPoint( rounded( center_x
                                       + _zoom * surfel->rotatedCenter.first ),
                              rounded( center_y + ( _zoom * surfel->rotatedCenter.second ) ) ) );
   polygon.push_back( QPoint( rounded( center_x
                                       + _zoom * surfel->rotatedCenter.first
                                       + _zoom * _rotatedFaces[direction][vertex].first ),
                              rounded( center_y + ( _zoom * surfel->rotatedCenter.second  +
                                                    _zoom * _rotatedFaces[direction][vertex].second) ) ) );
   painter.drawPolyline( polygon );

   polygon.clear();
   polygon.push_back( QPoint( rounded( center_x
                                       + _zoom * surfel->rotatedCenter.first
                                       + _zoom * _rotatedFaces[direction][edge].first ),
                              rounded( center_y + ( _zoom * surfel->rotatedCenter.second  +
                                                    _zoom * _rotatedFaces[direction][edge].second) ) ) );
   polygon.push_back( QPoint( rounded( center_x
                                       + _zoom * surfel->rotatedCenter.first
                                       + _zoom * _rotatedFaces[direction][(edge+1)%4].first ),
                      rounded( center_y + ( _zoom * surfel->rotatedCenter.second  +
                                            _zoom * _rotatedFaces[direction][(edge+1)%4].second) ) ) );
   painter.drawPolyline( polygon );
}

void
Surface::drawFine( QImage & image, ColorMap & colormap, double minDepth )
{
   Surfel *ps;
   int direction;
   int i, center_x, center_y;
   int color;
   int viewWidth = image.width();
   int viewHeight = image.height();
   int bytesPerLine = image.scanLine(1) - image.scanLine(0);
   const UChar * bgLine = colormap.backgroundLine();
   UChar * prevScanLine = image.scanLine(0);
   UChar * scanLine = image.scanLine(1);
   center_x = ( viewWidth / 2 );
   center_y = ( viewHeight / 2);
   Surfel **pps = visibleSurfelsBegin();
   Surfel **end = visibleSurfelsEnd();
   const QColor *cmapColors = colormap.qcolors();
   QPainter painter( &image );

   _zMin = _surfelList.bbMin().third;
   _zMax = _surfelList.bbMax().third;
   _zWidth = _zMax - _zMin;
   _lastColorMap = & colormap;

   memcpy( prevScanLine, bgLine, bytesPerLine );
   for ( int line = 1; line != viewHeight;
         ++line, prevScanLine = scanLine, scanLine += bytesPerLine  )
      memcpy( scanLine, prevScanLine, bytesPerLine );

   /* For color from orientation */
   _colors[0] = static_cast<UChar>( fabs( _rotatedNormals[0].third ) * 255);
   _colors[1] = static_cast<UChar>( fabs( _rotatedNormals[1].third ) * 255);
   _colors[2] = static_cast<UChar>( fabs( _rotatedNormals[2].third ) * 255);
   _colors[3] = static_cast<UChar>( fabs( _rotatedNormals[3].third ) * 255);
   _colors[4] = static_cast<UChar>( fabs( _rotatedNormals[4].third ) * 255);
   _colors[5] = static_cast<UChar>( fabs( _rotatedNormals[5].third ) * 255);

   QPolygon polygon(4);

   if ( _edges ) {
      painter.setPen( _edgeColor );
      while ( pps != end ) {
         ps = *pps;
         if ( _zoom * ps->rotatedCenter.third < minDepth ) {
            ++pps;
            continue;
         }
         direction = ps->direction;
         color = (this->*_surfelColorMethod)( ps );
         for (i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom * _rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         painter.setBrush( QBrush( cmapColors[ color ] ) );
         painter.drawPolygon( polygon );
         pps++;
      }
   } else {
      while ( pps != end ) {
         ps = *pps;
         if ( _zoom * ps->rotatedCenter.third < minDepth ) {
            ++pps;
            continue;
         }
         direction = ps->direction;
         color = (this->*_surfelColorMethod)( ps );
         for (i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom *_rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         painter.setBrush( QBrush( cmapColors[ color ] ) );
         painter.setPen( cmapColors[ color ] );
         painter.drawPolygon( polygon );
         pps++;
      }
   }

   if ( _markedSurfel > 0 ) {
      ps = _surfelList.begin() + _markedSurfel;
      if ( ps < _surfelList.end() && _visibleDirections[ps->direction] ) {
         painter.setBrush( QBrush( QColor( 255, 0, 0 ) ) );
         painter.setPen( QColor( 255, 0, 0 ) );
         direction = ps->direction;
         for ( i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom *_rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         painter.drawPolygon( polygon );

         polygon[0].setX( 0 );
         polygon[0].setY( 0 );
         polygon[1].setX( rounded( center_x + _zoom * ps->rotatedCenter.first ) );
         polygon[1].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second ) ) );
         painter.drawLine( polygon[0], polygon[1] );
      }
   }
}

void
Surface::drawFine( QImage & image, ColorMap & colormap, double minDepth, int alpha )
{
   Surfel *ps;
   int direction;
   int i, center_x, center_y;
   int color;
   int viewWidth = image.width();
   int viewHeight = image.height();
   int bytesPerLine = image.scanLine(1) - image.scanLine(0);
   const UChar * bgLine = colormap.backgroundLine();
   UChar * prevScanLine = image.scanLine(0);
   UChar * scanLine = image.scanLine(1);
   center_x = ( viewWidth / 2 );
   center_y = ( viewHeight / 2);
   Surfel **pps = visibleSurfelsBegin();
   Surfel **end = visibleSurfelsEnd();
   const QColor *cmapColors = colormap.qcolors();
   QPainter painter( &image );

   _zMin = _surfelList.bbMin().third;
   _zMax = _surfelList.bbMax().third;
   _zWidth = _zMax - _zMin;
   _lastColorMap = & colormap;

   memcpy( prevScanLine, bgLine, bytesPerLine );
   for ( int line = 1; line != viewHeight;
         ++line, prevScanLine = scanLine, scanLine += bytesPerLine  )
      memcpy( scanLine, prevScanLine, bytesPerLine );

   /* For color from orientation */
   _colors[0] = static_cast<UChar>( fabs( _rotatedNormals[0].third ) * 255);
   _colors[1] = static_cast<UChar>( fabs( _rotatedNormals[1].third ) * 255);
   _colors[2] = static_cast<UChar>( fabs( _rotatedNormals[2].third ) * 255);
   _colors[3] = static_cast<UChar>( fabs( _rotatedNormals[3].third ) * 255);
   _colors[4] = static_cast<UChar>( fabs( _rotatedNormals[4].third ) * 255);
   _colors[5] = static_cast<UChar>( fabs( _rotatedNormals[5].third ) * 255);

   QPolygon polygon(4);
   QColor qcolor;
   if ( _edges ) {
      painter.setPen( _edgeColor );
      while ( pps != end ) {
         ps = *pps;
         if ( _zoom * ps->rotatedCenter.third < minDepth ) {
            ++pps;
            continue;
         }
         direction = ps->direction;
         color = (this->*_surfelColorMethod)( ps );
         for (i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom * _rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         qcolor = cmapColors[ color ];
         qcolor.setAlpha( alpha );
         painter.setBrush( QBrush( qcolor ) );
         painter.drawPolygon( polygon );
         pps++;
      }
   } else {
      while ( pps != end ) {
         ps = *pps;
         if ( _zoom * ps->rotatedCenter.third < minDepth ) {
            ++pps;
            continue;
         }
         direction = ps->direction;
         color = (this->*_surfelColorMethod)( ps );
         for (i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom *_rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         qcolor = cmapColors[ color ];
         qcolor.setAlpha( alpha );
         painter.setBrush( QBrush( qcolor ) );
         painter.setPen( qcolor );
         painter.drawPolygon( polygon );
         pps++;
      }
   }

   if ( _markedSurfel > 0 ) {
      ps = _surfelList.begin() + _markedSurfel;
      if ( ps < _surfelList.end() && _visibleDirections[ps->direction] ) {
         painter.setBrush( QBrush( QColor( 255, 0, 0 ) ) );
         painter.setPen( QColor( 255, 0, 0 ) );
         direction = ps->direction;
         for ( i = 0; i < 4; i++) {
            polygon[i].setX( rounded( center_x
                                      + _zoom * ps->rotatedCenter.first
                                      + _zoom *_rotatedFaces[direction][i].first ) );
            polygon[i].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second  +
                                                   _zoom * _rotatedFaces[direction][i].second) ) );
         }
         painter.drawPolygon( polygon );

         polygon[0].setX( 0 );
         polygon[0].setY( 0 );
         polygon[1].setX( rounded( center_x + _zoom * ps->rotatedCenter.first ) );
         polygon[1].setY( rounded( center_y + ( _zoom * ps->rotatedCenter.second ) ) );
         painter.drawLine( polygon[0], polygon[1] );
      }
   }
}

void
Surface::paintSurfel(Surfel * surfel, SSize radius, UChar color )
{
   std::vector<Surfel*> v;
   std::vector<Surfel*>::iterator it,end;

   queue<Surfel*> q;
   set<Surfel*> visited;
   Surfel *ps;

   q.push( surfel );
   visited.insert( surfel );
   q.push( 0 );

   while ( q.size() && radius ) {
      ps = q.front();
      q.pop();
      if ( ps ) {
         ps->color = color;
         /* Enqueue unvisited neighbors of ps */
         v = vNeighborhood( ps );
         it = v.begin();
         while ( it != v.end() ) {
            if ( ! visited.count( *it ) ) {
               visited.insert( *it );
               q.push( *it );
            }
            ++it;
         }
      } else {
         q.push( 0 );
         --radius;
      }
   }
}

void
Surface::drawEdgelList( EdgelList & edgelList,
                        QImage & image,
                        ColorMap & /* colormap */,
                        const Matrix & rotation,
                        const Triple<double> & delta ) {
   Edgel *array = edgelList.begin();
   Edgel *edgel = array;
   Edgel *stop = array;
   Edgel *end = edgelList.end();

   char * visited;
   visited = new char[ end - array ];
   memset( visited, 0, end - array );

   int center_x, center_y;
   int viewWidth = image.width();
   int viewHeight = image.height();
   center_x = ( viewWidth / 2 );
   center_y = ( viewHeight / 2);
   QPainter painter( &image );

   double first_dx[4] = { 0, 0, 0.5, -0.5 };
   double first_dy[4] = { -0.5, 0.5, 0, 0 };
   double second_dx[4] = { 0, 0, -0.5, 0.5 };
   double second_dy[4] = { 0.5, -0.5, 0, 0 };

   double r11 = rotation.a11, r12 = rotation.a12, r13 = rotation.a13;
   double r21 = rotation.a21, r22 = rotation.a22, r23 = rotation.a23;
   double x, y, z;
   double xn, yn;
   double ax = _aspect.first;
   double ay = _aspect.second;
   double az = _aspect.third;

   painter.setPen( QColor( 255, 0, 0 ) );

   int first_x = 0, first_y = 0;
   int second_x = 0, second_y = 0;

   int starting_point = 0;
   int end_point = end - array;

   while ( starting_point < end_point ) {
      // Find next unvisited edgel
      while ( ( starting_point < end_point )
              && visited[ starting_point ] )
         ++starting_point;
      if ( starting_point >= end_point ) continue;
      edgel = stop = array + starting_point;
      visited[ starting_point ] = 1;
      do  {
         edgel = edgel->next;
         visited[ edgel - array ] = 1;

         x = edgel->x + delta.first + first_dx[ edgel->direction ];
         y = edgel->y + delta.second + first_dy[ edgel->direction ];
         z = -0.5 + delta.third;
         if ( ax > 0  ) {
            x *= ax;  y *= ay; z *= az;
         }
         xn = x * r11 + y * r12 + z * r13;
         yn = x * r21 + y * r22 + z * r23;
         first_x = static_cast<int>( center_x + _zoom * xn );
         first_y = static_cast<int>( center_y - _zoom * yn );


         x = edgel->x + delta.first + second_dx[ edgel->direction ];
         y = edgel->y + delta.second + second_dy[ edgel->direction ];
         z = -0.5 + delta.third;
         if ( ax > 0  ) {
            x *= ax;  y *= ay; z *= az;
         }
         xn = x * r11 + y * r12 + z * r13;
         yn = x * r21 + y * r22 + z * r23;
         second_x = static_cast<int>( center_x + _zoom * xn );
         second_y = static_cast<int>( center_y - _zoom * yn );

         painter.drawLine( first_x, first_y, second_x, second_y );
      } while ( edgel != stop );
   }

   disposeArray( visited );
}

Size
Surface::buildSurfelVertexIndicesArray( )
{
   const Size nil = ~ static_cast<Size>(0);
   Size nbVertices = 0;
   _surfelVertexIndicesArray.resize( _surfelList.size() );
   memset( &_surfelVertexIndicesArray.front(),
           static_cast<int>( nil ),
           sizeof( SurfelVertexIndices ) * _surfelVertexIndicesArray.size() );

   Surfel *begin = _surfelList.begin();
   Surfel *ps = begin;
   Surfel *end = _surfelList.end();
   SurfelVertexIndices *svi;
   int edge;
   while ( ps != end ) {
      svi = & _surfelVertexIndicesArray[ ps - begin ];
      for ( int vertex = 0; vertex < 4; ++vertex ) {
         if ( svi->indices[ vertex ] == nil ) {
            Size index =  nbVertices++;
            Surfel *surfel = ps;
            int newEdge;
            Surfel *newSurfel;
            edge = vertex;
            do {
               _surfelVertexIndicesArray[ surfel - begin ].indices[ edge ] = index;
               newEdge = (edge+3)%4;
               newSurfel = surfel->neighbors[ newEdge ];
               edge = edgeFrom( surfel->direction,
                                newEdge,
                                newSurfel->direction );
               surfel = newSurfel;
            } while ( surfel != ps );
         }
      }
      ++ps;
   }
   return nbVertices;
}

Size
Surface::buildSurfelEdgeIndicesArray( )
{
   const Size nil = ~ static_cast<Size>(0);
   Size nbEdges = 0;
   _surfelEdgeIndicesArray.resize( _surfelList.size() );
   memset( &_surfelEdgeIndicesArray.front(),
           static_cast<int>( nil ),
           sizeof( SurfelEdgeIndices ) * _surfelEdgeIndicesArray.size() );

   Surfel *begin = _surfelList.begin();
   Surfel *ps = begin;
   Surfel *end = _surfelList.end();
   SurfelEdgeIndices *sei;
   while ( ps != end ) {
      sei = & _surfelEdgeIndicesArray[ ps - begin ];
      for ( int e = 0; e < 4; ++e ) {
         if ( sei->indices[ e ] == nil ) {
            Size index =  nbEdges++;
            Surfel *newSurfel = ps->neighbors[ e ];
            int newEdge = edgeFrom( ps->direction,
                                    e,
                                    newSurfel->direction );
            _surfelEdgeIndicesArray[ newSurfel - begin ].indices[ newEdge ] = index;
            _surfelEdgeIndicesArray[ ps - begin ].indices[ e ] = index;
         }
      }
      ++ps;
   }
   return nbEdges;
}

void Surface::setCutPlane(bool on)
{
   if ( on ) {
      double width = abs(_surfelList.bbMax().third - _surfelList.bbMin().third);
      _cutPlaneShift = width * 0.05;
      _cutPlaneDepth = (_surfelList.bbMin().third + _surfelList.bbMax().third)/2;
   } else {
      _cutPlaneDepth = -numeric_limits<double>::max();
   }
}

void Surface::moveCutPlaneForward()
{
   _cutPlaneDepth += _cutPlaneShift;
}

void Surface::moveCutPlaneBackward()
{
   _cutPlaneDepth -= _cutPlaneShift;
}


void Surface::loadLUT( const char * filename )
{
   if ( !strcmp(filename,"/dev/null") ) {
      _lut.clear();
      return;
   }
   ifstream file(filename);
   int r=0,g=0,b=0;
   unsigned int count = 0;
   file >> count;
   char c;
   do file.get(c); while (c!='\n');
   do file.get(c); while (c!='\n');
   if ( count )
      _lut.clear();
   while ( count-- ) {
      file >> r >> g >> b;
      _lut.push_back(Triple<UChar>(r,g,b));
   }
}

void Surface::clearLUT()
{
   _lut.clear();
}

void Surface::invalidateNormals()
{
   _validNormals = false;
}
