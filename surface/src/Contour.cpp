/**
 * @file   Contour.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:12 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Contour.h"
#include <fstream>
#include <cassert>

Contour::Contour()
{
   CONS( Contour );
   _normals[0] = makeTriple( 1.0f, 0.0f, 0.0f );
   _normals[1] = makeTriple( -1.0f, 0.0f, 0.0f );
   _normals[2] = makeTriple( 0.0f, 1.0f, 0.0f );
   _normals[3] = makeTriple( 0.0f, -1.0f, 0.0f );
}

Contour::~Contour()
{
}

void
Contour::centersArray( EdgelDataArray & edgelDataArray )
{
   edgelDataArray.resize( _edgelList.end() - _edgelList.array() );
   EdgelDataArray::iterator edi = edgelDataArray.begin();
   Edgel * ps = _edgelList.array();
   Edgel * end = _edgelList.end();
   while ( ps < end ) {
      edi->first = ps->x;
      edi->second = ps->y;
      ++ps;
      ++edi;
   }
}

void
Contour::curvatureArray( EdgelDataArray & edgelDataArray )
{
   edgelDataArray.resize( _edgelList.end() - _edgelList.array() );
   EdgelDataArray::iterator edi = edgelDataArray.begin();
   Edgel * ps = _edgelList.array();
   Edgel * end = _edgelList.end();
   while ( ps < end ) {
      edi->first= ps->curvature;
      edi->second = 0;
      ++ps;
      ++edi;
   }
}

void
Contour::normalsArray( EdgelDataArray & edgelDataArray )
{
   Edgel *ps = _edgelList.array();
   Edgel *end = _edgelList.end();
   edgelDataArray.resize( _edgelList.end() - _edgelList.array() );
   EdgelDataArray::iterator edi = edgelDataArray.begin();
   while ( ps < end ) {
      edi->first = _normals[ ps->direction ].first;
      edi->second = _normals[ ps->direction ].second;
      ++ps;
      ++edi;
   }
}

Pair<double>
convolution21( Edgel *edgel,
               EdgelList & edgelList,
               EdgelDataArray & edaSrc,
               EdgelDataArray & edaDest )
{
   Edgel * slArray = edgelList.array();
   Pair<double> ed;
   ed += edaSrc[ edgel - slArray ];
   ed += edaSrc[ edgel->next - slArray ];
   ed += edaSrc[ edgel->previous - slArray ];
   ed /= 3.0;
   edaDest[ edgel - slArray ] = ed;
   return ed;
}

void
Contour::convolution421Centers( int iterations, EdgelDataArray & data )
{
   EdgelDataArray edaDest( _edgelList.end() - _edgelList.array() );
   Edgel *ps, *end = _edgelList.end();
   centersArray( data );
   if ( iterations > 0 )
      while ( iterations-- ) {
         ps = _edgelList.array();
         while ( ps < end ) {
            convolution21( ps, _edgelList, data, edaDest );
            ps++;
         }
         data =  edaDest;
      }
}

void
Contour::convolutionNormalsOnce( bool stop )
{
   static bool first = true;
   if ( stop ) {
      first = true;
      return;
   }
   if ( first )  normalsArray( _edgelDataArray );
   first = false;

   EdgelDataArray edaDest;

   edaDest.resize( _edgelList.end() - _edgelList.array() );

   Edgel * array = _edgelList.array();
   Edgel * pe = array;
   Edgel * end = _edgelList.end();
   Size i = 0;
   Pair<double> ed;
   while ( pe < end ) {
      ed = _edgelDataArray[ i ] * 2; //  * 2;
      ed += _edgelDataArray[ pe->next - array ];
      ed += _edgelDataArray[ pe->previous - array ];
      ed /= 4.0;
      edaDest[ i ] = ed;
      pe->color = static_cast<UChar>( 125 - 120 * ed.second );
      ++pe; ++i;
   }
   _edgelDataArray = edaDest;
}

void
Contour::convolutionNormals( int iterations )
{
   if ( iterations > 0 )
      while ( iterations-- ) convolutionNormalsOnce( false );
   else
      normalsArray( _edgelDataArray );
   convolutionNormalsOnce( true );
}

void
Contour::convolutionDifferential( EdgelDataArray & result )
{
   Edgel *array = _edgelList.array();
   Edgel *pe = array;
   Edgel * end = _edgelList.end();
   Pair<double> ed;
   Size i = 0;
   double dx=0,dy=0;
   result.resize( _edgelList.end() - _edgelList.array() );

   while ( pe < end ) {
      ed = _edgelDataArray[ pe->next - array ];
      ed -= _edgelDataArray[ pe->previous - array ];
      dx = pe->next->x - pe->previous->x;
      dy = pe->next->y - pe->previous->y;
      assert( sqrt( dx*dx + dy*dy ) > 0.1 );
      ed /= sqrt( dx*dx + dy*dy );
      result[ i ] = ed;;
      ++pe; ++i;
   }
}

void
Contour::markImage( ZZZImage<UChar> & image, UChar value )
{
   Edgel * pe = _edgelList.array();
   Edgel * end = _edgelList.end();
   while ( pe < end ) {
      image( pe->xp, pe->yp ) = value;
      ++pe;
   }
}

Pair<double>
Contour::setImage( AbstractImage & image,
                   int normalsIterations,
                   int curvatureAveragings,
                   std::vector<double> * curvatures,
                   std::vector<double> * averagedCurvatures,
                   bool localUpdate )
{

   if ( localUpdate )
      _edgelList.updateFromImage( image );
   else
      _edgelList.buildFromImage( image );

   convolutionNormals( normalsIterations );

   EdgelDataArray deltaN;
   deltaN.resize( _edgelList.count() );
   convolutionDifferential( deltaN );
   computeCurvature( deltaN );
   if ( curvatures ) _edgelList.curvatures( *curvatures );
   if ( curvatureAveragings )
      while ( curvatureAveragings-- )  convolutionCurvaturesAveragingOnce();
   if ( averagedCurvatures ) _edgelList.curvatures( *averagedCurvatures );
   colorizeCurvature( _edgelList );
   return _curvatureRange;
}

void
Contour::computeCurvature( EdgelDataArray & deltaN )
{
   Pair<double> delta, tangent;
   Edgel *array = _edgelList.array();
   Edgel *edgel = array, *end = _edgelList.end();
   UInt i = 0;

   _curvatureRange.first = std::numeric_limits<double>::max();
   _curvatureRange.second = -std::numeric_limits<double>::max();
   while ( edgel != end ) {
      delta = deltaN[ i ];
      if ( abs( delta ) > 0.01 ) {
         tangent.first = edgel->next->x - edgel->previous->x;
         tangent.second = edgel->next->y - edgel->previous->y;
         tangent /= abs( tangent );
         edgel->curvature = delta * tangent;
         if ( edgel->curvature >= 0 )
            edgel->curvature =  abs( delta );
         else
            edgel->curvature = - abs( delta );
      } else {
         edgel->curvature = 0;
      }
      if ( edgel->curvature  < _curvatureRange.first )
         _curvatureRange.first = edgel->curvature;
      if ( edgel->curvature  > _curvatureRange.second )
         _curvatureRange.second = edgel->curvature;
      ++edgel;
      ++i;
   }
}

void
Contour::convolutionCurvaturesAveragingOnce( )
{
   std::vector< double > result;
   result.resize( _edgelList.count() );
   std::vector< double >::iterator i = result.begin();

   Edgel * array = _edgelList.array();
   Edgel * pe = array;
   Edgel * end = _edgelList.end();
   while ( pe < end ) {
      *i  = ( ( 1 * pe->curvature ) + pe->next->curvature + pe->previous->curvature ) / 3;
      ++pe; ++i;
   }

   _curvatureRange.first = std::numeric_limits<double>::max();
   _curvatureRange.second = - std::numeric_limits<double>::max();
   i = result.begin();
   pe = array;
   while ( pe < end ) {
      if ( *i  < _curvatureRange.first ) _curvatureRange.first = *i;
      if ( *i  > _curvatureRange.second ) _curvatureRange.second = *i;
      (pe++)->curvature = *(i++);
   }
}

void
Contour::colorizeCurvature( EdgelList & edgelList )
{
   double maxAbs = std::max<double>( fabs( _curvatureRange.first ),
                                     fabs( _curvatureRange.second ) );
   Edgel *edgel = edgelList.array();
   Edgel *end = edgelList.end();
   while ( edgel != end ) {
      edgel->color = static_cast<UChar>( 128 + 127 * edgel->curvature / maxAbs );
      ++edgel;
   }
}
