/** -*- mode: c++ -*-
 * @file   SurfelGraphBuilder.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 *
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "SurfelGraphBuilder.h"

SurfelGraphBuilder::SurfelGraphBuilder( SurfelList & surfelList,
                                        Surfel * start,
                                        Surfel * stop,
                                        bool fullBox,
                                        int volumeAdjacency )
  : _surfelList( surfelList ),
    _start( start ),
    _stop( stop ),
    _fullBox( fullBox ),
    _volumeAdjacency(volumeAdjacency)
{

}

SurfelGraphBuilder::~SurfelGraphBuilder()
{
}

template<typename T>
void SurfelGraphBuilder::buildGraph(ZZZImage<T> & image)
{
  // if ( !image ) return; // TODO
  int directionA[6] = { 1, 1, 1, 1, 2, 2 };
  int directionB[6] = { 2, 0, 0, 2, 3, 1 };

  // (6,18) an (18,6) does not matter here.
  int oppositeDirectionA[6][6] =
  { { 3, 0, 3, 3, 0, 0 }, { 0, 3, 3, 3, 0, 0 },
    { 3, 3, 3, 0, 0, 0 }, { 3, 3, 0, 3, 0, 0 },
    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 2, 0, 0, 0 } };
  int oppositeDirectionB[6][6] =
  { { 0, 0, 0, 0, 1, 3 }, { 0, 2, 0, 0, 1, 3 },
    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
    { 0, 2, 0, 0, 1, 0 }, { 0, 2, 0, 0, 0, 3 } };

  Face f, fs;
  Surfel *ps = _start;
  Surfel *psv=0;
  if ( _fullBox ) {
    Face fa, fb;
    SSize width, height, depth, bands;
    image.getDimension( width, height, depth, bands );
    --width;
    --height;
    --depth;
    while ( ps < _stop ) {
      fs = Face( ps->voxel, ps->direction );
      fa = fb = fs;
      const Triple<SSize> & voxel = ps->voxel;
      switch ( ps->direction ) {
      case 0:
        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
        break;
      case 1:
        if ( ! voxel.second ) fa.direction = 3; else fa.y -= 1;
        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
        break;
      case 2:
        if ( ! voxel.first ) fa.direction = 1; else fa.x -= 1;
        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
        break;
      case 3:
        if ( voxel.first == width ) fa.direction = 0; else fa.x += 1;
        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
        break;
      case 4:
        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
        if ( voxel.first == 0 ) fb.direction = 1; else fb.x -= 1;
        break;
      case 5:
        if ( voxel.second == 0 ) fa.direction = 3; else fa.y -= 1;
        if ( voxel.first == width ) fb.direction = 0; else fb.x += 1;
        break;
      }
      psv = _surfelList.quickFind( fa );
      ps->neighbors[ directionA[ ps->direction ] ] = psv;
      psv->neighbors[ oppositeDirectionA[ ps->direction ][ psv->direction ] ] = ps;
      psv = _surfelList.quickFind( fb );
      ps->neighbors[ directionB[ ps->direction ] ] = psv;
      psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ] = ps;
      ++ps;
    }
  } else
    if ( image.bands() == 1 ) {
      if ( _volumeAdjacency == ADJ6 )
        while ( ps < _stop ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighbor_6_18( &image,
                                     fs,
                                     directionA[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighbor_6_18( &image,
                                     fs,
                                     directionB[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        } else /* ADJ18 */
        while ( ps < _stop ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighbor_18_6( &image,
                                     fs,
                                     directionA[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighbor_18_6( &image,
                                     fs,
                                     directionB[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        }
    } else {
      if ( _volumeAdjacency == ADJ6 )
        while ( ps < _stop ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighborColor_6_18( &image,
                                          fs,
                                          directionA[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighborColor_6_18( &image,
                                          fs,
                                          directionB[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        } else
        while ( ps < _stop ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighborColor_18_6( &image,
                                          fs,
                                          directionA[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighborColor_18_6( &image,
                                          fs,
                                          directionB[ ps->direction ] );
          psv = _surfelList.quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        }
    }
}


template<typename T>
void SurfelGraphBuilder::visitAny( ZZZImage<T> & volume )
{
  if ( volume.isEmpty() ) {
    return;
  }
  buildGraph(volume);
}

void
SurfelGraphBuilder::visit( ZZZImage<UChar> & volume )
{
  visitAny(volume);
}

void
SurfelGraphBuilder::visit( ZZZImage<Short> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<UShort> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<Int32> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<UInt32> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<Float> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<Double> & volume )
{
  visitAny(volume);
}

void SurfelGraphBuilder::visit( ZZZImage<Bool> &  )
{
  // visitAny(volume); // TODO
}

