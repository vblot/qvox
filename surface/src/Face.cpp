/**
 * @file   Face.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Nov 12 20:48:53 2005
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Face.h"
#include "Surfel.h"
#include <zzzimage.h>
#include <zzzimage_bool.h>

namespace {
   template<typename T>
   inline bool inside( T ***m0, T ***m1, T ***m2,
                       int z, int y, int x ) {
      return m0[z][y][x] || m1[z][y][x]  || m2[z][y][x];
   }
}

/**
 * Retourne la face voisine d'une face par une arete donnee.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
template<typename T>
Face
faceFindNeighbor_6_18( ZZZImage<T> * image, Face f, UChar edge)
{
   T ***matrix = image->data( 0 );
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;

   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( !matrix[z - 1][y][x] )
            return Face( x, y, z, 5 );
         if ( matrix[z - 1][y][x + 1] )
            return Face( x + 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 0 );
      case 1:
         if ( !matrix[z][y + 1][x] )
            return Face( x, y, z, 2 );
         if ( matrix[z][y + 1][x + 1] )
            return Face( x + 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 0 );
      case 2:
         if ( !matrix[z + 1][y][x] )
            return Face( x, y, z, 4 );
         if ( matrix[z + 1][y][x + 1] )
            return Face( x + 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 0 );
         break;
      case 3:
         if ( !matrix[z][y - 1][x] )
            return Face( x, y, z, 3 );
         if ( matrix[z][y - 1][x + 1] )
            return Face( x + 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 0 );
      }
      break;
   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( !matrix[z - 1][y][x] )
            return Face( x, y, z, 5 );
         if ( matrix[z - 1][y][x - 1] )
            return Face( x - 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 1 );
      case 1:
         if ( !matrix[z][y - 1][x] )
            return Face( x, y, z, 3 );
         if ( matrix[z][y - 1][x - 1] )
            return Face( x - 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 1 );
      case 2:
         if ( !matrix[z + 1][y][x] )
            return Face( x, y, z, 4 );
         if ( matrix[z + 1][y][x - 1] )
            return Face( x - 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 1 );
      case 3:
         if ( !matrix[z][y + 1][x] )
            return Face( x, y, z, 2 );
         if ( matrix[z][y + 1][x - 1] )
            return Face( x - 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 1 );
      }
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( !matrix[z - 1][y][x] )
            return Face( x, y, z, 5 );
         if ( matrix[z - 1][y + 1][x] )
            return Face( x, y + 1, z - 1, 4 );
         return Face( x, y, z - 1, 2 );
      case 1:
         if ( !matrix[z][y][x - 1] )
            return Face( x, y, z, 1 );
         if ( matrix[z][y + 1][x - 1] )
            return Face( x - 1, y + 1, z, 0 );
         return Face( x - 1, y, z, 2 );
      case 2:
         if ( !matrix[z + 1][y][x] )
            return Face( x, y, z, 4 );
         if ( matrix[z + 1][y + 1][x] )
            return Face( x, y + 1, z + 1, 5 );
         return Face( x, y, z + 1, 2 );
      case 3:
         if ( !matrix[z][y][x + 1] )
            return Face( x, y, z, 0 );
         if ( matrix[z][y + 1][x + 1] )
            return Face( x + 1, y + 1, z, 1 );
         return Face( x + 1, y, z, 2 );
      }
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( !matrix[z - 1][y][x] )
            return Face( x, y, z, 5 );
         if ( matrix[z - 1][y - 1][x] )
            return Face( x, y - 1, z - 1, 4 );
         return Face( x, y, z - 1, 3 );
      case 1:
         if ( !matrix[z][y][x + 1] )
            return Face( x, y, z, 0 );
         if ( matrix[z][y - 1][x + 1] )
            return Face( x + 1, y - 1, z, 1 );
         return Face( x + 1, y, z, 3 );
      case 2:
         if ( !matrix[z + 1][y][x] )
            return Face( x, y, z, 4 );
         if ( matrix[z + 1][y - 1][x] )
            return Face( x, y - 1, z + 1, 5 );
         return Face( x, y, z + 1, 3 );
      case 3:
         if ( !matrix[z][y][x - 1] )
            return Face( x, y, z, 1 );
         if ( matrix[z][y - 1][x - 1] )
            return Face( x - 1, y - 1, z, 0 );
         return Face( x - 1, y, z, 3 );
      }
   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( !matrix[z][y - 1][x] )
            return Face( x, y, z, 3 );
         if ( matrix[z + 1][y - 1][x] )
            return Face( x, y - 1, z + 1, 2 );
         return Face( x, y - 1, z, 4 );
      case 1:
         if ( !matrix[z][y][x + 1] )
            return Face( x, y, z, 0 );
         if ( matrix[z + 1][y][x + 1] )
            return Face( x + 1, y, z + 1, 1 );
         return Face( x + 1, y, z, 4 );
      case 2:
         if ( !matrix[z][y + 1][x] )
            return Face( x, y, z, 2 );
         if ( matrix[z + 1][y + 1][x] )
            return Face( x, y + 1, z + 1, 3 );
         return Face( x, y + 1, z, 4 );
      case 3:
         if ( !matrix[z][y][x - 1] )
            return Face( x, y, z, 1 );
         if ( matrix[z + 1][y][x - 1] )
            return Face( x - 1, y, z + 1, 0 );
         return Face( x - 1, y, z, 4 );
      }

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( !matrix[z][y + 1][x] )
            return Face( x, y, z, 2 );
         if ( matrix[z - 1][y + 1][x] )
            return Face( x, y + 1, z - 1, 3 );
         return Face( x, y + 1, z, 5 );
      case 1:
         if ( !matrix[z][y][x + 1] )
            return Face( x, y, z, 0 );
         if ( matrix[z - 1][y][x + 1] )
            return Face( x + 1, y, z - 1, 1 );
         return Face( x + 1, y, z, 5 );
      case 2:
         if ( !matrix[z][y - 1][x] )
            return Face( x, y, z, 3 );
         if ( matrix[z - 1][y - 1][x] )
            return Face( x, y - 1, z - 1, 2 );
         return Face( x, y - 1, z, 5 );
      case 3:
         if ( !matrix[z][y][x - 1] )
            return Face( x, y, z, 1 );
         if ( matrix[z - 1][y][x - 1] )
            return Face( x - 1, y, z - 1, 0 );
         return Face( x - 1, y, z, 5 );
      }
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}


/**
 * Retourne la face voisine d'une face par une arete donnee.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
Face
faceFindNeighbor_6_18( ZZZImage<Bool> * image, Face f, UChar edge)
{
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;

   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( image->bit(x + 1,y,z - 1) )
            return Face( x + 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 0 );
      case 1:
         if ( !image->bit(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( image->bit(x + 1,y + 1,z) )
            return Face( x + 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 0 );
      case 2:
         if ( !image->bit(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( image->bit(x + 1,y,z + 1) )
            return Face( x + 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 0 );
         break;
      case 3:
         if ( !image->bit(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( image->bit(x + 1,y - 1,z) )
            return Face( x + 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 0 );
      }
      break;
   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( image->bit(x - 1,y,z - 1) )
            return Face( x - 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 1 );
      case 1:
         if ( !image->bit(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( image->bit(x - 1,y - 1,z) )
            return Face( x - 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 1 );
      case 2:
         if ( !image->bit(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( image->bit(x - 1,y,z + 1) )
            return Face( x - 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 1 );
      case 3:
         if ( !image->bit(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( image->bit(x - 1,y + 1,z) )
            return Face( x - 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 1 );
      }
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( image->bit(x,y + 1,z - 1) )
            return Face( x, y + 1, z - 1, 4 );
         return Face( x, y, z - 1, 2 );
      case 1:
         if ( !image->bit(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( image->bit(x - 1,y + 1,z) )
            return Face( x - 1, y + 1, z, 0 );
         return Face( x - 1, y, z, 2 );
      case 2:
         if ( !image->bit(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( image->bit(x,y + 1,z + 1) )
            return Face( x, y + 1, z + 1, 5 );
         return Face( x, y, z + 1, 2 );
      case 3:
         if ( !image->bit(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( image->bit(x + 1,y + 1,z) )
            return Face( x + 1, y + 1, z, 1 );
         return Face( x + 1, y, z, 2 );
      }
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( image->bit(x,y - 1,z - 1) )
            return Face( x, y - 1, z - 1, 4 );
         return Face( x, y, z - 1, 3 );
      case 1:
         if ( !image->bit(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( image->bit(x + 1,y - 1,z) )
            return Face( x + 1, y - 1, z, 1 );
         return Face( x + 1, y, z, 3 );
      case 2:
         if ( !image->bit(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( image->bit(x,y - 1,z + 1) )
            return Face( x, y - 1, z + 1, 5 );
         return Face( x, y, z + 1, 3 );
      case 3:
         if ( !image->bit(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( image->bit(x - 1,y - 1,z) )
            return Face( x - 1, y - 1, z, 0 );
         return Face( x - 1, y, z, 3 );
      }
   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( image->bit(x,y - 1,z + 1) )
            return Face( x, y - 1, z + 1, 2 );
         return Face( x, y - 1, z, 4 );
      case 1:
         if ( !image->bit(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( image->bit(x + 1,y,z + 1) )
            return Face( x + 1, y, z + 1, 1 );
         return Face( x + 1, y, z, 4 );
      case 2:
         if ( !image->bit(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( image->bit(x,y + 1,z + 1) )
            return Face( x, y + 1, z + 1, 3 );
         return Face( x, y + 1, z, 4 );
      case 3:
         if ( !image->bit(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( image->bit(x - 1,y,z + 1) )
            return Face( x - 1, y, z + 1, 0 );
         return Face( x - 1, y, z, 4 );
      }

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( !image->bit(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( image->bit(x,y + 1,z - 1) )
            return Face( x, y + 1, z - 1, 3 );
         return Face( x, y + 1, z, 5 );
      case 1:
         if ( !image->bit(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( image->bit(x + 1,y,z - 1) )
            return Face( x + 1, y, z - 1, 1 );
         return Face( x + 1, y, z, 5 );
      case 2:
         if ( !image->bit(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( image->bit(x,y - 1,z - 1) )
            return Face( x, y - 1, z - 1, 2 );
         return Face( x, y - 1, z, 5 );
      case 3:
         if ( !image->bit(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( image->bit(x - 1,y,z - 1) )
            return Face( x - 1, y, z - 1, 0 );
         return Face( x - 1, y, z, 5 );
      }
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}

/**
 * Retourne la face voisine d'une face par une arete donnee
 * pour un volume en (18,6) adjacence.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
template<typename T>
Face
faceFindNeighbor_18_6( ZZZImage<T> * image, Face f, UChar edge)
{
   T ***matrix = image->data( 0 );
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;

   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( matrix[z-1][y][x+1] ) return Face( x+1, y, z-1, 4 );
         if ( matrix[z-1][y][x] ) return Face( x, y, z-1, 0 );
         return Face( x, y, z, 5 );
      case 1:
         if ( matrix[z][y+1][x+1] ) return Face( x+1, y+1, z, 3 );
         if ( matrix[z][y+1][x] ) return Face( x, y+1, z, 0 );
         return Face( x, y, z, 2 );
      case 2:
         if ( matrix[z+1][y][x+1] ) return Face( x+1, y, z+1, 5 );
         if ( matrix[z+1][y][x] ) return Face( x, y, z+1, 0 );
         return Face( x, y, z, 4 );
         break;
      case 3:
         if ( matrix[z][y-1][x+1] ) return Face( x+1, y-1, z, 2 );
         if ( matrix[z][y-1][x] ) return Face( x, y-1, z, 0 );
         return Face( x, y, z, 3 );
      } // OK
      break;

   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( matrix[z-1][y][x-1] ) return Face( x-1, y, z-1, 4 );
         if ( matrix[z-1][y][x] ) return Face( x, y, z-1, 1 );
         return Face( x, y, z, 5 );
      case 1:
         if ( matrix[z][y-1][x-1] ) return Face( x-1, y-1, z, 2 );
         if ( matrix[z][y-1][x] ) return Face( x, y - 1, z, 1 );
         return Face( x, y, z, 3 );
      case 2:
         if ( matrix[z+1][y][x-1] ) return Face( x-1, y, z+1, 5 );
         if ( matrix[z+1][y][x] ) return Face( x, y, z+1, 1 );
         return Face( x, y, z, 4 );
      case 3:
         if ( matrix[z][y+1][x-1] ) return Face( x-1, y+1, z, 3 );
         if ( matrix[z][y+1][x] ) return Face( x, y+1, z, 1 );
         return Face( x, y, z, 2 );
      } // OK
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( matrix[z-1][y+1][x] ) return Face( x, y+1, z-1, 4 );
         if ( matrix[z-1][y][x] ) return Face( x, y, z-1, 2 );
         return Face( x, y, z, 5 );
      case 1:
         if ( matrix[z][y+1][x-1] ) return Face( x-1, y+1, z, 0 );
         if ( matrix[z][y][x-1] ) return Face( x-1, y, z, 2 );
         return Face( x, y, z, 1 );
      case 2:
         if ( matrix[z+1][y+1][x] ) return Face( x, y+1, z+1, 5 );
         if ( matrix[z+1][y][x] ) return Face( x, y, z+1, 2 );
         return Face( x, y, z, 4 );
      case 3:
         if ( matrix[z][y+1][x+1] ) return Face( x+1, y+1, z, 1 );
         if ( matrix[z][y][x+1] ) return Face( x+1, y, z, 2 );
         return Face( x, y, z, 0 );
      } // OK
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( matrix[z-1][y-1][x] ) return Face( x, y-1, z-1, 4 );
         if ( matrix[z-1][y][x] ) return Face( x, y, z-1, 3 );
         return Face( x, y, z, 5 );
      case 1:
         if ( matrix[z][y-1][x+1] ) return Face( x+1, y-1, z, 1 );
         if ( matrix[z][y][x+1] ) return Face( x+1, y, z, 3 );
         return Face( x, y, z, 0 );
      case 2:
         if ( matrix[z+1][y-1][x] ) return Face( x, y-1, z+1, 5 );
         if ( matrix[z+1][y][x] ) return Face( x, y, z+1, 3 );
         return Face( x, y, z, 4 );
      case 3:
         if ( matrix[z][y-1][x-1] ) return Face( x-1, y-1, z, 0 );
         if ( matrix[z][y][x-1] ) return Face( x-1, y, z, 3 );
         return Face( x, y, z, 1 );
      } // OK

   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( matrix[z+1][y-1][x] ) return Face( x, y-1, z+1, 2 );
         if ( matrix[z][y-1][x] ) return Face( x, y-1, z, 4 );
         return Face( x, y, z, 3 );
      case 1:
         if ( matrix[z+1][y][x+1] ) return Face( x+1, y, z+1, 1 );
         if ( matrix[z][y][x+1] ) return Face( x+1, y, z, 4 );
         return Face( x, y, z, 0 );
      case 2:
         if ( matrix[z+1][y+1][x] ) return Face( x, y+1, z+1, 3 );
         if ( matrix[z][y+1][x] ) return Face( x, y+1, z, 4 );
         return Face( x, y, z, 2 );
      case 3:
         if ( matrix[z+1][y][x-1] ) return Face( x-1, y, z+1, 0 );
         if ( matrix[z][y][x-1] ) return Face( x-1, y, z, 4 );
         return Face( x, y, z, 1 );
      } // OK

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( matrix[z-1][y+1][x] ) return Face( x, y+1, z-1, 3 );
         if ( matrix[z][y+1][x] ) return Face( x, y+1, z, 5 );
         return Face( x, y, z, 2 );
      case 1:
         if ( matrix[z-1][y][x+1] ) return Face( x+1, y, z-1, 1 );
         if ( matrix[z][y][x+1] ) return Face( x+1, y, z, 5 );
         return Face( x, y, z, 0 );
      case 2:
         if ( matrix[z-1][y-1][x] ) return Face( x, y-1, z-1, 2 );
         if ( matrix[z][y-1][x] ) return Face( x, y-1, z, 5 );
         return Face( x, y, z, 3 );
      case 3:
         if ( matrix[z-1][y][x-1] ) return Face( x-1, y, z-1, 0 );
         if ( matrix[z][y][x-1] ) return Face( x-1, y, z, 5 );
         return Face( x, y, z, 1 );
      } // OK
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}

/**
 * Retourne la face voisine d'une face par une arete donnee
 * pour un volume en (18,6) adjacence.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
template<typename T>
Face
faceFindNeighbor_18_6( ZZZImage<Bool> * image, Face f, UChar edge)
{
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;

   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x+1,y,z-1) ) return Face( x+1, y, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 0 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x+1,y+1,z) ) return Face( x+1, y+1, z, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 0 );
         return Face( x, y, z, 2 );
      case 2:
         if ( image->bit(x+1,y,z+1) ) return Face( x+1, y, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 0 );
         return Face( x, y, z, 4 );
         break;
      case 3:
         if ( image->bit(x+1,y-1,z) ) return Face( x+1, y-1, z, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 0 );
         return Face( x, y, z, 3 );
      } // OK
      break;

   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x-1,y,z-1) ) return Face( x-1, y, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 1 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x-1,y-1,z) ) return Face( x-1, y-1, z, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y - 1, z, 1 );
         return Face( x, y, z, 3 );
      case 2:
         if ( image->bit(x-1,y,z+1) ) return Face( x-1, y, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 1 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x-1,y+1,z) ) return Face( x-1, y+1, z, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 1 );
         return Face( x, y, z, 2 );
      } // OK
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y+1,z-1) ) return Face( x, y+1, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 2 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x-1,y+1,z) ) return Face( x-1, y+1, z, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 2 );
         return Face( x, y, z, 1 );
      case 2:
         if ( image->bit(x,y+1,z+1) ) return Face( x, y+1, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 2 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x+1,y+1,z) ) return Face( x+1, y+1, z, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 2 );
         return Face( x, y, z, 0 );
      } // OK
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y-1,z-1) ) return Face( x, y-1, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 3 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x+1,y-1,z) ) return Face( x+1, y-1, z, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 3 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y-1,z+1) ) return Face( x, y-1, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 3 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x-1,y-1,z) ) return Face( x-1, y-1, z, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 3 );
         return Face( x, y, z, 1 );
      } // OK

   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y-1,z+1) ) return Face( x, y-1, z+1, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 4 );
         return Face( x, y, z, 3 );
      case 1:
         if ( image->bit(x+1,y,z+1) ) return Face( x+1, y, z+1, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 4 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y+1,z+1) ) return Face( x, y+1, z+1, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 4 );
         return Face( x, y, z, 2 );
      case 3:
         if ( image->bit(x-1,y,z+1) ) return Face( x-1, y, z+1, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 4 );
         return Face( x, y, z, 1 );
      } // OK

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y+1,z-1) ) return Face( x, y+1, z-1, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 5 );
         return Face( x, y, z, 2 );
      case 1:
         if ( image->bit(x+1,y,z-1) ) return Face( x+1, y, z-1, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 5 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y-1,z-1) ) return Face( x, y-1, z-1, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 5 );
         return Face( x, y, z, 3 );
      case 3:
         if ( image->bit(x-1,y,z-1) ) return Face( x-1, y, z-1, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 5 );
         return Face( x, y, z, 1 );
      } // OK
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}


/**
 * Retourne la face voisine d'une face par une arete donnee
 * pour un volume en (18,6) adjacence.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
Face
faceFindNeighbor_18_6( ZZZImage<Bool> * image, Face f, UChar edge)
{
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;

   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x+1,y,z-1) ) return Face( x+1, y, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 0 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x+1,y+1,z) ) return Face( x+1, y+1, z, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 0 );
         return Face( x, y, z, 2 );
      case 2:
         if ( image->bit(x+1,y,z+1) ) return Face( x+1, y, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 0 );
         return Face( x, y, z, 4 );
         break;
      case 3:
         if ( image->bit(x+1,y-1,z) ) return Face( x+1, y-1, z, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 0 );
         return Face( x, y, z, 3 );
      } // OK
      break;

   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x-1,y,z-1) ) return Face( x-1, y, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 1 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x-1,y-1,z) ) return Face( x-1, y-1, z, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y - 1, z, 1 );
         return Face( x, y, z, 3 );
      case 2:
         if ( image->bit(x-1,y,z+1) ) return Face( x-1, y, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 1 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x-1,y+1,z) ) return Face( x-1, y+1, z, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 1 );
         return Face( x, y, z, 2 );
      } // OK
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y+1,z-1) ) return Face( x, y+1, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 2 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x-1,y+1,z) ) return Face( x-1, y+1, z, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 2 );
         return Face( x, y, z, 1 );
      case 2:
         if ( image->bit(x,y+1,z+1) ) return Face( x, y+1, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 2 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x+1,y+1,z) ) return Face( x+1, y+1, z, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 2 );
         return Face( x, y, z, 0 );
      } // OK
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y-1,z-1) ) return Face( x, y-1, z-1, 4 );
         if ( image->bit(x,y,z-1) ) return Face( x, y, z-1, 3 );
         return Face( x, y, z, 5 );
      case 1:
         if ( image->bit(x+1,y-1,z) ) return Face( x+1, y-1, z, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 3 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y-1,z+1) ) return Face( x, y-1, z+1, 5 );
         if ( image->bit(x,y,z+1) ) return Face( x, y, z+1, 3 );
         return Face( x, y, z, 4 );
      case 3:
         if ( image->bit(x-1,y-1,z) ) return Face( x-1, y-1, z, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 3 );
         return Face( x, y, z, 1 );
      } // OK

   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y-1,z+1) ) return Face( x, y-1, z+1, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 4 );
         return Face( x, y, z, 3 );
      case 1:
         if ( image->bit(x+1,y,z+1) ) return Face( x+1, y, z+1, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 4 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y+1,z+1) ) return Face( x, y+1, z+1, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 4 );
         return Face( x, y, z, 2 );
      case 3:
         if ( image->bit(x-1,y,z+1) ) return Face( x-1, y, z+1, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 4 );
         return Face( x, y, z, 1 );
      } // OK

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( image->bit(x,y+1,z-1) ) return Face( x, y+1, z-1, 3 );
         if ( image->bit(x,y+1,z) ) return Face( x, y+1, z, 5 );
         return Face( x, y, z, 2 );
      case 1:
         if ( image->bit(x+1,y,z-1) ) return Face( x+1, y, z-1, 1 );
         if ( image->bit(x+1,y,z) ) return Face( x+1, y, z, 5 );
         return Face( x, y, z, 0 );
      case 2:
         if ( image->bit(x,y-1,z-1) ) return Face( x, y-1, z-1, 2 );
         if ( image->bit(x,y-1,z) ) return Face( x, y-1, z, 5 );
         return Face( x, y, z, 3 );
      case 3:
         if ( image->bit(x-1,y,z-1) ) return Face( x-1, y, z-1, 0 );
         if ( image->bit(x-1,y,z) ) return Face( x-1, y, z, 5 );
         return Face( x, y, z, 1 );
      } // OK
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}

/**
 * Retourne la face voisine d'une face par une arete donnee.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
template<typename T>
Face
faceFindNeighborColor_6_18( ZZZImage<T> * image, Face f, UChar edge)
{
  T ***matrix0 = image->data( 0 );
  T ***matrix1 = image->data( 1 );
  T ***matrix2 = image->data( 2 );
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;
   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z - 1, y, x) )
            return Face( x, y, z, 5 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y, x + 1) )
            return Face( x + 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 0 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y + 1, x) )
            return Face( x, y, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z, y + 1, x + 1) )
            return Face( x + 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 0 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z + 1, y, x) )
            return Face( x, y, z, 4 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y, x + 1) )
            return Face( x + 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 0 );
         break;
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y - 1, x) )
            return Face( x, y, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z, y - 1, x + 1) )
            return Face( x + 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 0 );
      }
   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z - 1, y, x) )
            return Face( x, y, z, 5 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y, x - 1) )
            return Face( x - 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 1 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y - 1, x) )
            return Face( x, y, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z, y - 1, x - 1) )
            return Face( x - 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 1 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z + 1, y, x) )
            return Face( x, y, z, 4 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y, x - 1) )
            return Face( x - 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 1 );
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y + 1, x) )
            return Face( x, y, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z, y + 1, x - 1) )
            return Face( x - 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 1 );
      }
      break;
   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z - 1, y, x) )
            return Face( x, y, z, 5 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y + 1, x) )
            return Face( x, y + 1, z - 1, 4 );
         return Face( x, y, z - 1, 2 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x - 1) )
            return Face( x, y, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z, y + 1, x - 1) )
            return Face( x - 1, y + 1, z, 0 );
         return Face( x - 1, y, z, 2 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z + 1, y, x) )
            return Face( x, y, z, 4 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y + 1, x) )
            return Face( x, y + 1, z + 1, 5 );
         return Face( x, y, z + 1, 2 );
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x + 1) )
            return Face( x, y, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z, y + 1, x + 1) )
            return Face( x + 1, y + 1, z, 1 );
         return Face( x + 1, y, z, 2 );
      }
      break;
   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z - 1, y, x) )
            return Face( x, y, z, 5 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y - 1, x) )
            return Face( x, y - 1, z - 1, 4 );
         return Face( x, y, z - 1, 3 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x + 1) )
            return Face( x, y, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z, y - 1, x + 1) )
            return Face( x + 1, y - 1, z, 1 );
         return Face( x + 1, y, z, 3 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z + 1, y, x) )
            return Face( x, y, z, 4 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y - 1, x) )
            return Face( x, y - 1, z + 1, 5 );
         return Face( x, y, z + 1, 3 );
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x - 1) )
            return Face( x, y, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z, y - 1, x - 1) )
            return Face( x - 1, y - 1, z, 0 );
         return Face( x - 1, y, z, 3 );
      }
   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z, y - 1, x) )
            return Face( x, y, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y - 1, x) )
            return Face( x, y - 1, z + 1, 2 );
         return Face( x, y - 1, z, 4 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x + 1) )
            return Face( x, y, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y, x + 1) )
            return Face( x + 1, y, z + 1, 1 );
         return Face( x + 1, y, z, 4 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z, y + 1, x) )
            return Face( x, y, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y + 1, x) )
            return Face( x, y + 1, z + 1, 3 );
         return Face( x, y + 1, z, 4 );
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x - 1) )
            return Face( x, y, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z + 1, y, x - 1) )
            return Face( x - 1, y, z + 1, 0 );
         return Face( x - 1, y, z, 4 );
      }
   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( !inside(matrix0,matrix1,matrix2,z, y + 1, x) )
            return Face( x, y, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y + 1, x) )
            return Face( x, y + 1, z - 1, 3 );
         return Face( x, y + 1, z, 5 );
      case 1:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x + 1) )
            return Face( x, y, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y, x + 1) )
            return Face( x + 1, y, z - 1, 1 );
         return Face( x + 1, y, z, 5 );
      case 2:
         if ( !inside(matrix0,matrix1,matrix2,z, y - 1, x) )
            return Face( x, y, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y - 1, x) )
            return Face( x, y - 1, z - 1, 2 );
         return Face( x, y - 1, z, 5 );
      case 3:
         if ( !inside(matrix0,matrix1,matrix2,z, y, x - 1) )
            return Face( x, y, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z - 1, y, x - 1) )
            return Face( x - 1, y, z - 1, 0 );
         return Face( x - 1, y, z, 5 );
      }
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}


/**
 * Retourne la face voisine d'une face par une arete donnee.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
Face
faceFindNeighborColor_6_18( ZZZImage<Bool> * image, Face f, UChar edge)
{
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;
   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( ! image->zero(x + 1,y,z - 1) )
            return Face( x + 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 0 );
      case 1:
         if ( image->zero(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( ! image->zero(x + 1,y + 1,z) )
            return Face( x + 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 0 );
      case 2:
         if ( image->zero(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( ! image->zero(x + 1,y,z + 1) )
            return Face( x + 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 0 );
         break;
      case 3:
         if ( image->zero(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( ! image->zero(x + 1,y - 1,z) )
            return Face( x + 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 0 );
      }
   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( ! image->zero(x - 1,y,z - 1) )
            return Face( x - 1, y, z - 1, 4 );
         return Face( x, y, z - 1, 1 );
      case 1:
         if ( image->zero(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( ! image->zero(x - 1,y - 1,z) )
            return Face( x - 1, y - 1, z, 2 );
         return Face( x, y - 1, z, 1 );
      case 2:
         if ( image->zero(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( ! image->zero(x - 1,y,z + 1) )
            return Face( x - 1, y, z + 1, 5 );
         return Face( x, y, z + 1, 1 );
      case 3:
         if ( image->zero(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( ! image->zero(x - 1,y + 1,z) )
            return Face( x - 1, y + 1, z, 3 );
         return Face( x, y + 1, z, 1 );
      }
      break;
   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( ! image->zero(x,y + 1,z - 1) )
            return Face( x, y + 1, z - 1, 4 );
         return Face( x, y, z - 1, 2 );
      case 1:
         if ( image->zero(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( ! image->zero(x - 1,y + 1,z) )
            return Face( x - 1, y + 1, z, 0 );
         return Face( x - 1, y, z, 2 );
      case 2:
         if ( image->zero(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( ! image->zero(x,y + 1,z + 1) )
            return Face( x, y + 1, z + 1, 5 );
         return Face( x, y, z + 1, 2 );
      case 3:
         if ( image->zero(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( ! image->zero(x + 1,y + 1,z) )
            return Face( x + 1, y + 1, z, 1 );
         return Face( x + 1, y, z, 2 );
      }
      break;
   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y,z - 1) )
            return Face( x, y, z, 5 );
         if ( ! image->zero(x,y - 1,z - 1) )
            return Face( x, y - 1, z - 1, 4 );
         return Face( x, y, z - 1, 3 );
      case 1:
         if ( image->zero(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( ! image->zero(x + 1,y - 1,z) )
            return Face( x + 1, y - 1, z, 1 );
         return Face( x + 1, y, z, 3 );
      case 2:
         if ( image->zero(x,y,z + 1) )
            return Face( x, y, z, 4 );
         if ( ! image->zero(x,y - 1,z + 1) )
            return Face( x, y - 1, z + 1, 5 );
         return Face( x, y, z + 1, 3 );
      case 3:
         if ( image->zero(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( ! image->zero(x - 1,y - 1,z) )
            return Face( x - 1, y - 1, z, 0 );
         return Face( x - 1, y, z, 3 );
      }
   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( ! image->zero(x,y - 1,z + 1) )
            return Face( x, y - 1, z + 1, 2 );
         return Face( x, y - 1, z, 4 );
      case 1:
         if ( image->zero(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( ! image->zero(x + 1,y,z + 1) )
            return Face( x + 1, y, z + 1, 1 );
         return Face( x + 1, y, z, 4 );
      case 2:
         if ( image->zero(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( ! image->zero(x,y + 1,z + 1) )
            return Face( x, y + 1, z + 1, 3 );
         return Face( x, y + 1, z, 4 );
      case 3:
         if ( image->zero(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( ! image->zero(x - 1,y,z + 1) )
            return Face( x - 1, y, z + 1, 0 );
         return Face( x - 1, y, z, 4 );
      }
   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( image->zero(x,y + 1,z) )
            return Face( x, y, z, 2 );
         if ( ! image->zero(x,y + 1,z - 1) )
            return Face( x, y + 1, z - 1, 3 );
         return Face( x, y + 1, z, 5 );
      case 1:
         if ( image->zero(x + 1,y,z) )
            return Face( x, y, z, 0 );
         if ( ! image->zero(x + 1,y,z - 1) )
            return Face( x + 1, y, z - 1, 1 );
         return Face( x + 1, y, z, 5 );
      case 2:
         if ( image->zero(x,y - 1,z) )
            return Face( x, y, z, 3 );
         if ( ! image->zero(x,y - 1,z - 1) )
            return Face( x, y - 1, z - 1, 2 );
         return Face( x, y - 1, z, 5 );
      case 3:
         if ( image->zero(x - 1,y,z) )
            return Face( x, y, z, 1 );
         if ( ! image->zero(x - 1,y,z - 1) )
            return Face( x - 1, y, z - 1, 0 );
         return Face( x - 1, y, z, 5 );
      }
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}


/**
 * Retourne la face voisine d'une face par une arete donnee
 * pour un volume en (18,6) adjacence.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
template<typename T>
Face
faceFindNeighborColor_18_6( ZZZImage<T> * image, Face f, UChar edge)
{
   T ***matrix0 = image->data( 0 );
   T ***matrix1 = image->data( 1 );
   T ***matrix2 = image->data( 2 );
   const int x = f.x;
   const int y = f.y;
   const int z = f.z;
   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x+1) ) return Face( x+1, y, z-1, 4 );
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x) ) return Face( x, y, z-1, 0 );
         return Face( x, y, z, 5 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x+1) ) return Face( x+1, y+1, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x) ) return Face( x, y+1, z, 0 );
         return Face( x, y, z, 2 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x+1) ) return Face( x+1, y, z+1, 5 );
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x) ) return Face( x, y, z+1, 0 );
         return Face( x, y, z, 4 );
         break;
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x+1) ) return Face( x+1, y-1, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x) ) return Face( x, y-1, z, 0 );
         return Face( x, y, z, 3 );
      } // OK
      break;

   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x-1) ) return Face( x-1, y, z-1, 4 );
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x) ) return Face( x, y, z-1, 1 );
         return Face( x, y, z, 5 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x-1) ) return Face( x-1, y-1, z, 2 );
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x) ) return Face( x, y - 1, z, 1 );
         return Face( x, y, z, 3 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x-1) ) return Face( x-1, y, z+1, 5 );
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x) ) return Face( x, y, z+1, 1 );
         return Face( x, y, z, 4 );
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x-1) ) return Face( x-1, y+1, z, 3 );
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x) ) return Face( x, y+1, z, 1 );
         return Face( x, y, z, 2 );
      } // OK
      break;

   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z-1,y+1,x) ) return Face( x, y+1, z-1, 4 );
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x) ) return Face( x, y, z-1, 2 );
         return Face( x, y, z, 5 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x-1) ) return Face( x-1, y+1, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x-1) ) return Face( x-1, y, z, 2 );
         return Face( x, y, z, 1 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z+1,y+1,x) ) return Face( x, y+1, z+1, 5 );
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x) ) return Face( x, y, z+1, 2 );
         return Face( x, y, z, 4 );
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x+1) ) return Face( x+1, y+1, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x+1) ) return Face( x+1, y, z, 2 );
         return Face( x, y, z, 0 );
      } // OK
      break;

   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z-1,y-1,x) ) return Face( x, y-1, z-1, 4 );
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x) ) return Face( x, y, z-1, 3 );
         return Face( x, y, z, 5 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x+1) ) return Face( x+1, y-1, z, 1 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x+1) ) return Face( x+1, y, z, 3 );
         return Face( x, y, z, 0 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z+1,y-1,x) ) return Face( x, y-1, z+1, 5 );
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x) ) return Face( x, y, z+1, 3 );
         return Face( x, y, z, 4 );
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x-1) ) return Face( x-1, y-1, z, 0 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x-1) ) return Face( x-1, y, z, 3 );
         return Face( x, y, z, 1 );
      } // OK

   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z+1,y-1,x) ) return Face( x, y-1, z+1, 2 );
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x) ) return Face( x, y-1, z, 4 );
         return Face( x, y, z, 3 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x+1) ) return Face( x+1, y, z+1, 1 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x+1) ) return Face( x+1, y, z, 4 );
         return Face( x, y, z, 0 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z+1,y+1,x) ) return Face( x, y+1, z+1, 3 );
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x) ) return Face( x, y+1, z, 4 );
         return Face( x, y, z, 2 );
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z+1,y,x-1) ) return Face( x-1, y, z+1, 0 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x-1) ) return Face( x-1, y, z, 4 );
         return Face( x, y, z, 1 );
      } // OK

   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( inside(matrix0,matrix1,matrix2,z-1,y+1,x) ) return Face( x, y+1, z-1, 3 );
         if ( inside(matrix0,matrix1,matrix2,z,y+1,x) ) return Face( x, y+1, z, 5 );
         return Face( x, y, z, 2 );
      case 1:
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x+1) ) return Face( x+1, y, z-1, 1 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x+1) ) return Face( x+1, y, z, 5 );
         return Face( x, y, z, 0 );
      case 2:
         if ( inside(matrix0,matrix1,matrix2,z-1,y-1,x) ) return Face( x, y-1, z-1, 2 );
         if ( inside(matrix0,matrix1,matrix2,z,y-1,x) ) return Face( x, y-1, z, 5 );
         return Face( x, y, z, 3 );
      case 3:
         if ( inside(matrix0,matrix1,matrix2,z-1,y,x-1) ) return Face( x-1, y, z-1, 0 );
         if ( inside(matrix0,matrix1,matrix2,z,y,x-1) ) return Face( x-1, y, z, 5 );
         return Face( x, y, z, 1 );
      } // OK
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}



/**
 * Retourne la face voisine d'une face par une arete donnee.
 *
 * @param s	Une face.
 * @param arete	Le numero d'une arete de la face (0..3).
 */
Face
faceFindNeighborColor_18_6( ZZZImage<Bool> * image, Face f, UChar edge)
{
   int x, y, z;
   x = f.x;
   y = f.y;
   z = f.z;
   switch ( f.direction ) {
   case 0:			/* Direction X+ */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x+1 , y, z-1) )
            return Face( x+1, y, z-1, 4 );
         if ( ! image->zero( x , y, z-1) )
            return Face( x, y, z-1, 0 );
         return Face( x, y, z, 5 );
      case 1:
         if ( ! image->zero( x+1 , y+1, z) )
            return Face( x+1, y+1, z, 3 );
         if ( ! image->zero( x , y+1, z) )
            return Face( x, y+1, z, 0 );
         return Face( x, y, z, 2 );
      case 2:
         if ( ! image->zero( x+1 , y, z+1) )
            return Face( x+1, y, z+1, 5 );
         if ( ! image->zero( x , y, z+1) )
            return Face( x, y, z+1, 0 );
         return Face( x, y, z, 4 );
         break;
      case 3:
         if ( ! image->zero( x+1 , y-1, z) )
            return Face( x+1, y-1, z, 2 );
         if ( ! image->zero( x , y-1, z) )
            return Face( x, y-1, z, 0 );
         return Face( x, y, z, 3 );
      }
      break;
   case 1:			/* Direction X- */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x-1 , y, z-1) )
            return Face( x-1, y, z-1, 4 );
         if ( ! image->zero( x , y, z-1) )
            return Face( x, y, z-1, 1 );
         return Face( x, y, z, 5 );
      case 1:
         if ( ! image->zero( x-1 , y-1, z) )
            return Face( x-1, y-1, z, 2 );
         if ( ! image->zero( x , y-1, z) )
            return Face( x, y - 1, z, 1 );
         return Face( x, y, z, 3 );
      case 2:
         if ( ! image->zero( x-1 , y, z+1) )
            return Face( x-1, y, z+1, 5 );
         if ( ! image->zero( x , y, z+1) )
            return Face( x, y, z+1, 1 );
         return Face( x, y, z, 4 );
      case 3:
         if ( ! image->zero( x-1 , y+1, z) )
            return Face( x-1, y+1, z, 3 );
         if ( ! image->zero( x , y+1, z) )
            return Face( x, y+1, z, 1 );
         return Face( x, y, z, 2 );
      }
      break;
   case 2:			/* Direction Y+ */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x , y+1, z-1) )
            return Face( x, y+1, z-1, 4 );
         if ( ! image->zero( x , y, z-1) )
            return Face( x, y, z-1, 2 );
         return Face( x, y, z, 5 );
      case 1:
         if ( ! image->zero( x-1 , y+1, z) )
            return Face( x-1, y+1, z, 0 );
         if ( ! image->zero( x-1 , y, z) )
            return Face( x-1, y, z, 2 );
         return Face( x, y, z, 1 );
      case 2:
         if ( ! image->zero( x , y+1, z+1) )
            return Face( x, y+1, z+1, 5 );
         if ( ! image->zero( x , y, z+1) )
            return Face( x, y, z+1, 2 );
         return Face( x, y, z, 4 );
      case 3:
         if ( ! image->zero( x+1 , y+1, z) )
            return Face( x+1, y+1, z, 1 );
         if ( ! image->zero( x+1 , y, z) )
            return Face( x+1, y, z, 2 );
         return Face( x, y, z, 0 );
      }
      break;
   case 3:			/* Direction Y- */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x , y-1, z-1) )
            return Face( x, y-1, z-1, 4 );
         if ( ! image->zero( x , y, z-1) )
            return Face( x, y, z-1, 3 );
         return Face( x, y, z, 5 );
      case 1:
         if ( ! image->zero( x+1 , y-1, z) )
            return Face( x+1, y-1, z, 1 );
         if ( ! image->zero( x+1 , y, z) )
            return Face( x+1, y, z, 3 );
         return Face( x, y, z, 0 );
      case 2:
         if ( ! image->zero( x , y-1, z+1) )
            return Face( x, y-1, z+1, 5 );
         if ( ! image->zero( x , y, z+1) )
            return Face( x, y, z+1, 3 );
         return Face( x, y, z, 4 );
      case 3:
         if ( ! image->zero( x-1 , y-1, z) )
            return Face( x-1, y-1, z, 0 );
         if ( ! image->zero( x-1 , y, z) )
            return Face( x-1, y, z, 3 );
         return Face( x, y, z, 1 );
      }
   case 4:			/* Direction Z+ */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x , y-1, z+1) )
            return Face( x, y-1, z+1, 2 );
         if ( ! image->zero( x , y-1, z) )
            return Face( x, y-1, z, 4 );
         return Face( x, y, z, 3 );
      case 1:
         if ( ! image->zero( x+1 , y, z+1) )
            return Face( x+1, y, z+1, 1 );
         if ( ! image->zero( x+1 , y, z) )
            return Face( x+1, y, z, 4 );
         return Face( x, y, z, 0 );
      case 2:
         if ( ! image->zero( x , y+1, z+1) )
            return Face( x, y+1, z+1, 3 );
         if ( ! image->zero( x , y+1, z) )
            return Face( x, y+1, z, 4 );
         return Face( x, y, z, 2 );
      case 3:
         if ( ! image->zero( x-1 , y, z+1) )
            return Face( x-1, y, z+1, 0 );
         if ( ! image->zero( x-1 , y, z) )
            return Face( x-1, y, z, 4 );
         return Face( x, y, z, 1 );
      }
   case 5:			/* Direction Z- */
      switch ( edge ) {
      case 0:
         if ( ! image->zero( x , y+1, z-1) )
            return Face( x, y+1, z-1, 3 );
         if ( ! image->zero( x , y+1, z) )
            return Face( x, y+1, z, 5 );
         return Face( x, y, z, 2 );
      case 1:
         if ( ! image->zero( x+1 , y, z-1) )
            return Face( x+1, y, z-1, 1 );
         if ( ! image->zero( x+1 , y, z) )
            return Face( x+1, y, z, 5 );
         return Face( x, y, z, 0 );
      case 2:
         if ( ! image->zero( x , y-1, z-1) )
            return Face( x, y-1, z-1, 2 );
         if ( ! image->zero( x , y-1, z) )
            return Face( x, y-1, z, 5 );
         return Face( x, y, z, 3 );
      case 3:
         if ( ! image->zero( x-1 , y, z-1) )
            return Face( x-1, y, z-1, 0 );
         if ( ! image->zero( x-1 , y, z) )
            return Face( x-1, y, z, 5 );
         return Face( x, y, z, 1 );
      }
   }
   ERROR << "faceFindNeighbor(): face not found!\n";
   return Face(0, 0, 0, 0);
}


/**
 * Retourne le numero d'une arete d'une face pour sa face voisine par
 * cette arete.
 *
 * @param direction	Une orientation de face.
 * @param arete		Le numero de l'arete.
 */
int
faceAreteOpposee(int direction, int arete)
{
   switch ( direction ) {
   case 0:
      switch ( arete ) {
      case 0:
         return 3;
      case 1:
         return 3;
      case 2:
         return 1;
      case 3:
         return 1;
      }
   case 1:
      switch ( arete ) {
      case 0:
         return 1;
      case 1:
         return 3;
      case 2:
         return 3;
      case 3:
         return 1;
      }
   case 2:
      switch ( arete ) {
      case 0:
         return 0;
      case 1:
         return 3;
      case 2:
         return 2;
      case 3:
         return 1;
      }
   case 3:
      switch ( arete ) {
      case 0:
         return 2;
      case 1:
         return 3;
      case 2:
         return 0;
      case 3:
         return 1;
      }
   case 4:
      switch ( arete ) {
      case 0:
         return 2;
      case 1:
         return 2;
      case 2:
         return 2;
      case 3:
         return 2;
      }
   case 5:
      switch (arete) {
      case 0:
         return 0;
      case 1:
         return 0;
      case 2:
         return 0;
      case 3:
         return 0;
      }
   }
   fprintf(stderr, "faceAreteOpposee(): Bad argument.\n");
   return 0;
}

std::ostream &
operator<<( std::ostream & out, const Face & face )
{
   out << "Face(" << face.x << "," << face.y << "," << face.z
       << ")d(" << face.direction << ")";
   return out;
}

/**
                                         * Retourne une face correspondant a un surfel.
                                         *
                                         * @param ps	A surfel.
                                         */
Face
surfelToFace( Surfel *ps )
{
   return Face( ps->voxel.first, ps->voxel.second, ps->voxel.third, ps->direction );
}

/*
                                         * Template function instanciation
                                         */

#define INSTANCIATE( TYPE ) \
   template Face faceFindNeighbor_6_18( ZZZImage< TYPE > * image, Face f, UChar edge); \
   template Face faceFindNeighbor_18_6( ZZZImage< TYPE > * image, Face f, UChar edge); \
   template Face faceFindNeighborColor_6_18( ZZZImage< TYPE > * image, Face f, UChar edge); \
   template Face faceFindNeighborColor_18_6( ZZZImage< TYPE > * image, Face f, UChar edge);

INSTANCIATE( UChar )
INSTANCIATE( Short )
INSTANCIATE( UShort )
INSTANCIATE( Int32 )
INSTANCIATE( UInt32 )
INSTANCIATE( Float )
INSTANCIATE( Double )
