/**
 * @file   SurfacePainter.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:12 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "SurfacePainter.h"
#include "SurfelList.h"
#include "Surface.h"
#include "ColorMap.h"
#include "globals.h"
#include <QImage>
#include <QColor>
#include <assert.h>

namespace {
int roundInt( double x )  {
  // return static_cast<int>( floor( x+0.49 ) );
  return static_cast<int>( (x<0)?floor( x-0.49 ):ceil(x+0.49) );
}
int rounded( double x )  {
  return static_cast<int>( x );
}
}

AbstractSurfacePainter::~AbstractSurfacePainter()
{
}

AbstractSurfacePainter *
AbstractSurfacePainter::surfacePainterFactory( AbstractSurfacePainter::ColorType color,
                                               AbstractSurfacePainter::SliceType slice,
                                               AbstractSurfacePainter::EdgesType edges )
{
  if ( color == UseColorMap ) {
    if ( slice == DrawSlice ) {
      if ( edges == DrawEdges ) return new  ColorMapSliceEdges_Painter;
      if ( edges == NoEdges ) return new  ColorMapSlice_Painter;
    }
    if ( slice == NoSlice ) {
      if ( edges == DrawEdges ) return new  ColorMapEdges_Painter;
      if ( edges == NoEdges ) return new  ColorMap_Painter;
    }
  }
  if ( color == UseTrueColor ) {
    if ( slice == DrawSlice ) {
      if ( edges == DrawEdges ) return new  TrueColorSliceEdges_Painter;
      if ( edges == NoEdges ) return new  TrueColorSlice_Painter;
    }
    if ( slice == NoSlice ) {
      if ( edges == DrawEdges ) return new  TrueColorEdges_Painter;
      if ( edges == NoEdges ) return new  TrueColor_Painter;
    }
  }
  assert(false);
  return 0;
}

void
AbstractSurfacePainter::setSurface( Surface * surface )
{
  _surface = surface;
}

void
ColorMapSliceEdges_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  UChar **pixels = colormap.pixels();
  UChar *pixelLine;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;
  UChar *hLine = colormap.singleLine();


  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const int markedSlice = _surface->_markedSlice;
  const int markedSliceAxis = _surface->_markedSliceAxis;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;
  const QRgb edgeRgb = _surface->_edgeRgb;

  unsigned char * puc;
  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    pixelLine = pixels[ (_surface->*colorMethod)( ps ) ];
    switch ( markedSliceAxis ) {
    case 0:
      if ( markedSlice == ps->voxel.first )
        pixelLine = hLine;
      break;
    case 1:
      if ( markedSlice == ps->voxel.second )
        pixelLine = hLine;
      break;
    case 2:
      if ( markedSlice == ps->voxel.third )
        pixelLine = hLine;
      break;
    default:
      ERROR << "Surface::draw(): axis should be -1, 0, 1 or 2."
            << " Nothing else!\n";
      exit( -1 );
      break;
    }
    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      line_beg =  image.scanLine( i ) + ( j << 2 );
      row = _surface->height[direction];
      while ( row--) {
        puc = line_beg + (*pstart);
        memcpy( puc,
                pixelLine,
                (*plength) + BYTES_PER_PIXEL );
        *reinterpret_cast<QRgb*>( puc ) = edgeRgb;
        *reinterpret_cast<QRgb*>( puc  + *plength ) = edgeRgb;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
ColorMapSlice_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  UChar **pixels = colormap.pixels();
  UChar *pixelLine;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;
  UChar *hLine = colormap.singleLine();


  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const int markedSlice = _surface->_markedSlice;
  const int markedSliceAxis = _surface->_markedSliceAxis;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;

  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    pixelLine = pixels[ (_surface->*colorMethod)( ps ) ];

    switch ( markedSliceAxis ) {
    case 0: if ( markedSlice == ps->voxel.first ) pixelLine = hLine; break;
    case 1: if ( markedSlice == ps->voxel.second ) pixelLine = hLine; break;
    case 2: if ( markedSlice == ps->voxel.third ) pixelLine = hLine; break;
    default: ERROR <<"Surface::draw(): axis should be -1, 0, 1 or 2. Nothing else.\n"; exit( -1 ); break;
    }

    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      line_beg =  image.scanLine( i ) + ( j << 2 );
      row = _surface->height[direction];
      while ( row--) {
        memcpy( line_beg + (*pstart),
                pixelLine,
                (*plength) + BYTES_PER_PIXEL );
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
ColorMapEdges_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  UChar **pixels = colormap.pixels();
  UChar *pixelLine;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;
  const QRgb edgeRgb = _surface->_edgeRgb;

  while ( start  < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    pixelLine = pixels[ (_surface->*colorMethod)( ps ) ];
    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      while ( row--) {
        memcpy( line_beg + (*pstart),
                pixelLine,
                (*plength) + BYTES_PER_PIXEL );
        *reinterpret_cast<QRgb*>( line_beg + (*pstart) ) = edgeRgb;
        *reinterpret_cast<QRgb*>( line_beg + (*pstart)  + *plength ) = edgeRgb;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
ColorMap_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  UChar **pixels = colormap.pixels();
  UChar *pixelLine;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;

  while ( start  < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    pixelLine = pixels[ (_surface->*colorMethod)( ps ) ];
    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      while ( row--) {
        memcpy( line_beg + (*pstart),
                pixelLine,
                (*plength) + BYTES_PER_PIXEL );
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
TrueColorSliceEdges_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;
  QRgb singleLineRgb = colormap.singleLineColor().rgb();

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const int markedSlice = _surface->_markedSlice;
  const int markedSliceAxis = _surface->_markedSliceAxis;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;
  const QRgb edgeRgb = _surface->_edgeRgb;

  unsigned int count;
  QRgb * ppixel;
  QRgb pixel;
  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    j = roundInt( center_x + dx[direction] + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y + dy[direction] + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      pixel = (_surface->*colorMethod)( ps );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      switch ( markedSliceAxis ) {
      case 0: if ( markedSlice == ps->voxel.first ) pixel = singleLineRgb; break;
      case 1: if ( markedSlice == ps->voxel.second ) pixel = singleLineRgb; break;
      case 2: if ( markedSlice == ps->voxel.third ) pixel = singleLineRgb; break;
      default:
        ERROR <<"Surface::draw(): Axis should be -1, 0, 1 or 2. Nothing else.\n";
        exit( -1 );
        break;
      }
      while ( row--) {
        count = ( ( *plength  ) >> 2 ) + 1 ;
        ppixel = reinterpret_cast<QRgb*>( line_beg + (*pstart) );
        while ( count-- ) *(ppixel++) = pixel;
        *reinterpret_cast<QRgb*>( line_beg + (*pstart) ) = edgeRgb;
        *reinterpret_cast<QRgb*>( line_beg + (*pstart)  + *plength ) = edgeRgb;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
TrueColorSlice_Painter::draw( QImage & image, ColorMap & colormap, Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;
  QRgb singleLineRgb = colormap.singleLineColor().rgb();

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const int markedSlice = _surface->_markedSlice;
  const int markedSliceAxis = _surface->_markedSliceAxis;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;

  unsigned int count;
  QRgb * ppixel;
  QRgb pixel;
  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    j = roundInt( center_x + dx[direction] + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y + dy[direction] + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      pixel = (_surface->*colorMethod)( ps );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      switch ( markedSliceAxis ) {
      case 0: if ( markedSlice == ps->voxel.first ) pixel = singleLineRgb; break;
      case 1: if ( markedSlice == ps->voxel.second ) pixel = singleLineRgb; break;
      case 2: if ( markedSlice == ps->voxel.third ) pixel = singleLineRgb; break;
      default:
        ERROR <<"Surface::draw(): Axis should be -1, 0, 1 or 2. Nothing else.\n";
        exit( -1 );
        break;
      }
      while ( row--) {
        count = ( ( *plength  ) >> 2 ) + 1 ;
        ppixel = reinterpret_cast<QRgb*>( line_beg + (*pstart) );
        while ( count-- ) *(ppixel++) = pixel;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

void
TrueColorEdges_Painter::draw( QImage & image, ColorMap & , Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;
  const QRgb edgeRgb = _surface->_edgeRgb;

  unsigned int count;
  QRgb * ppixel;
  QRgb pixel;
  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      pixel = (_surface->*colorMethod)( ps );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      while ( row--) {
        count = ( ( *plength  ) >> 2 ) + 1 ;
        ppixel = reinterpret_cast<QRgb*>( line_beg + (*pstart) );
        while ( count-- ) *(ppixel++) = pixel;
        *reinterpret_cast<QRgb*>( line_beg + (*pstart) ) = edgeRgb;
        *reinterpret_cast<QRgb*>( line_beg + (*pstart)  + *plength ) = edgeRgb;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}


void
TrueColor_Painter::draw( QImage & image, ColorMap & , Surfel ** start, Surfel ** stop, double minDepth ) {
  Surfel *ps;
  int direction;
  int i, j, center_x, center_y;
  int *plength;
  int *pstart;
  int row;
  unsigned char *line_beg;
  int viewWidth = image.width();
  int viewHeight = image.height();
  int xLimitMax = viewWidth - _surface->_maxSurfelWidth;
  int yLimitMax = viewHeight - _surface->_maxSurfelHeight;
  int bytesPerLine = image.scanLine(1) - image.scanLine(0);
  center_x = ( viewWidth / 2 ) + _surface->_viewCenterShift.first;
  center_y = ( viewHeight / 2) + _surface->_viewCenterShift.second;

  SurfelColorMethod colorMethod = _surface->_surfelColorMethod;
  const double zoom = _surface->_zoom;
  const double * dx = _surface->dx;
  const double * dy = _surface->dy;

  unsigned int count;
  QRgb * ppixel;
  QRgb pixel;
  while ( start < stop ) {
    ps = *start;
    if ( zoom * ps->rotatedCenter.third < minDepth ) {
      ++start;
      continue;
    }
    direction = ps->direction;
    j = roundInt( center_x
                  + dx[direction]
                  + zoom * ps->rotatedCenter.first );
    i = roundInt( center_y
                  + dy[direction]
                  + zoom * ps->rotatedCenter.second );
    if ( j >= 0 && i >= 0 && j < xLimitMax && i < yLimitMax  ) {
      line_beg =  image.scanLine( i ) + ( j << 2 );
      pixel = (_surface->*colorMethod)( ps );
      plength = _surface->lengths[direction];
      pstart = _surface->starts[direction];
      row = _surface->height[direction];
      while ( row--) {
        count = ( ( *plength  ) >> 2 ) + 1 ;
        ppixel = reinterpret_cast<QRgb*>( line_beg + (*pstart) );
        while ( count-- ) *(ppixel++) = pixel;
        line_beg += bytesPerLine;
        ++plength;
        ++pstart;
      }
    }
    ++start;
  }
}

