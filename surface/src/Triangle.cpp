/**
 * @file   Triangle.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:58 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "SurfelList.h"
#include "Surface.h"
#include "ColorMap.h"
#include "Convolution.h"

using std::vector;

bool triangleZLess( const Triangle & a, const Triangle & b )
{
  double za = (a.vertex0.third + a.vertex1.third + a.vertex2.third)/3.0;
  double zb = (b.vertex0.third + b.vertex1.third + b.vertex2.third)/3.0;
  return za < zb;
}

bool triangleZGreater( const Triangle & a, const Triangle & b )
{
  double za = (a.vertex0.third + a.vertex1.third + a.vertex2.third)/3.0;
  double zb = (b.vertex0.third + b.vertex1.third + b.vertex2.third)/3.0;
  return za > zb;
}

std::ostream &
dumpPOV( const vector<Triangle> & triangles, std::ostream & out, bool colors )
{
  vector<Triangle>::const_iterator pt = triangles.begin();
  vector<Triangle>::const_iterator end = triangles.end();

  if (colors)
    while (pt < end ) {
      if ( pt->valid ) {
        out << " triangle { <"
            << pt->vertex0.first << ", "
            << - pt->vertex0.second<< ", "
            << pt->vertex0.third << ">, "
            << pt->vertex1.first << ", "
            << - pt->vertex1.second<< ", "
            << pt->vertex1.third << ">, "
            << pt->vertex2.first << ", "
            << - pt->vertex2.second<< ", "
            << pt->vertex2.third << "> "
            << "pigment { color red " << pt->red()
            << " green " << pt->green()
            << " blue " << pt->blue() << " } }\n";
      }
      ++pt;
    }
  else
    while ( pt < end ) {
      if ( pt->valid ) {
        out << " triangle { <"
            << pt->vertex0.first << ", "
            << - pt->vertex0.second<< ", "
            << pt->vertex0.third << ">, "
            << pt->vertex1.first << ", "
            << - pt->vertex1.second<< ", "
            << pt->vertex1.third << ">, "
            << pt->vertex2.first << ", "
            << - pt->vertex2.second<< ", "
            << pt->vertex2.third << "> "
            << "pigment { color red 1.0 green 0.0 blue 1.0 } }\n";
      }
      ++pt;
    }
  return out;
}

bool
Triangle::operator==( const Triangle & other ) const
{

#define S_EQUAL(S1,S2) ((fabs(S1.first - S2.first) < 0.00001) && \
  (fabs(S1.second - S2.second) < 0.00001) && \
  (fabs(S1.third - S2.third) < 0.00001))

  return
      (S_EQUAL(vertex0,other.vertex0) &&
       S_EQUAL(vertex1,other.vertex1) &&
       S_EQUAL(vertex2,other.vertex2)) ||
      (S_EQUAL(vertex0,other.vertex1) &&
       S_EQUAL(vertex1,other.vertex2) &&
       S_EQUAL(vertex2,other.vertex0)) ||
      (S_EQUAL(vertex0,other.vertex2) &&
       S_EQUAL(vertex1,other.vertex0) &&
       S_EQUAL(vertex2,other.vertex1));
}

void
addTriangle( vector<Triangle> & triangles,
             SurfelData & s1, SurfelData & s2, SurfelData & s3,
             const Triple<float> & color1,
             const Triple<float> & color2,
             const Triple<float> & color3 )
{
  Triangle t;
  t.valid = true;
  t.vertex0 = s1;
  t.vertex1 = s2;
  t.vertex2 = s3;
  t.color0 = color1;
  t.color1 = color2;
  t.color2 = color3;
  triangles.push_back( t );
}

/** 
 * Computes the color of a triangle as the mean of the 3 vertices colors.
 *
 */
void getTriangleColor( const ColorMap & colormap,
                       const Surface & surface,
                       Surfel *s1, Surfel *s2, Surfel *s3,
                       float & red, float & green, float & blue )
{
  int n = 0;
  red = green = blue = 0.0;  
  Triple<float> color;
  
  if ( s1 ) {
    color = surface.getSurfelColor( s1, colormap );
    ++n;
  }
  if ( s2 ) {
    color = surface.getSurfelColor( s2, colormap );
    ++n;
  }
  if ( s3 ) {
    color = surface.getSurfelColor( s3, colormap );
    ++n;
  }
  color /=  (float) n;
  red = color.first;
  green =  color.second;
  blue = color.third;
  return;
}

void
loopToTriangles( vector<Triangle> & triangles,
                 SurfelDataArray::iterator *loop,
                 int n,
                 Surfel *surfels[],
                 const Surface & surface,
                 const ColorMap & colormap )
{
  SurfelData centerData;
  centerData = 0.0f;
  if (n == 3) {
    addTriangle( triangles,
                 *( loop[0] ), *(loop[1]), *(loop[2]),
		 surface.getSurfelColor( surfels[0], colormap ),
		 surface.getSurfelColor( surfels[1], colormap ),
		 surface.getSurfelColor( surfels[2], colormap ) );
    return;
  }

  if (n == 4) {
    addTriangle( triangles,
                 *(loop[0]), *(loop[1]), *(loop[2]),
		 surface.getSurfelColor( surfels[0], colormap ),
		 surface.getSurfelColor( surfels[1], colormap ),
		 surface.getSurfelColor( surfels[2], colormap ) );
    addTriangle( triangles,
                 *(loop[2]), *(loop[3]), *(loop[0]),
		 surface.getSurfelColor( surfels[2], colormap ),
		 surface.getSurfelColor( surfels[3], colormap ),
		 surface.getSurfelColor( surfels[0], colormap ) );
    return;
  }

  Triple<float> centerColor;
  for ( int i = 0; i < n; i++) {
    centerData += *(loop[i]);
    centerColor += surface.getSurfelColor( surfels[i], colormap );
  }
  centerData /= static_cast<double>( n );
  centerColor /= static_cast<float>( n );

  for ( int i = 0; i < n - 1; i++) {
    addTriangle( triangles,
                 centerData, *(loop[i]), *(loop[i+1]),
        centerColor,
        surface.getSurfelColor( surfels[i], colormap ),
        surface.getSurfelColor( surfels[i+1], colormap ) );
  }
  addTriangle( triangles,
               centerData, *(loop[n-1]), *(loop[0]),
	       centerColor,
	       surface.getSurfelColor( surfels[n-1], colormap ),
	       surface.getSurfelColor( surfels[0], colormap ) );
}

void
loopsToTriangles( std::vector<Triangle> & triangles,
                  Surfel * surfel,
                  SurfelDataArray & surfelCenters,
                  const Surface & surface,
                  const ColorMap & colormap,
                  vector<bool> & visitedLoops )
{
  VNeighborhood v = computeVNeighborhood( surfel );
  int i,n;
  SurfelDataArray::iterator  loop[7];
  Surfel * surfels[7];
  SurfelDataArray::iterator centersBegin = surfelCenters.begin();
  const Surfel * slArray = surface.surfelList().begin();

  const vector<Surface::SurfelVertexIndices> & vertexIndices = surface.surfelVertexIndicesArray();

  Size surfelIndex = surfel - surface.surfelList().begin();


  for ( int l = 0; l < 4; ++l ) {
    if ( ! visitedLoops[ vertexIndices[ surfelIndex ].indices[l] ] ) {
      visitedLoops[ vertexIndices[ surfelIndex ].indices[l] ]  = true;
      n = 0;
      loop[n] = ( centersBegin + ( v.center - slArray ) );
      surfels[n] = v.center;
      ++n;
      loop[n] = ( centersBegin + ( v.eNeighbors[ (l+3)%4 ] - slArray ) );
      surfels[n] = v.eNeighbors[ (l+3)%4 ];
      ++n;
      for (i = 0; i < v.loopSize[l] ; i++ ) {
        loop[n] = ( centersBegin + ( v.loops[l][i] - slArray ) );
        surfels[n] = v.loops[l][i];
        ++n;
      }
      loop[n] = ( centersBegin + ( v.eNeighbors[l] - slArray ) );
      surfels[n] = v.eNeighbors[l];
      ++n;
      loopToTriangles( triangles, loop, n, surfels, surface, colormap );
    }
  }
}



