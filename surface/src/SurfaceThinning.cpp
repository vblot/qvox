/**
 * @file   SurfaceThinning.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:29 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "SurfaceThinning.h"
#include "SurfelList.h"
#include "Convolution.h"

#include <vector>
using namespace std;

const char SimplePixelConfig8[256] = { 
  0,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,1,
  0,1,0,1,0,1,0,0,0,0,0,1,0,1,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,
  1,0,1,0,1,0,0,0,0,0,1,0,1,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,
  0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,1,1,0,1,0,1,
  0,1,0,0,0,0,0,1,0,1,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,0,1,0,
  1,0,0,0,0,0,1,0,1,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1,0 };
  
const char SimplePixelConfig4[256] = { 
  0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,1,0,1,
  0,1,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,1,1,0,1,0,
  0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,
  0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,1,0,1,0,1,1,0,1,1,
  0,1,0,1,1,0,1,1,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,1,0,1,0,1,1,0,1,1,0,1,
  0,1,1,0,1,1,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,
  0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,
  0,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,0
};  

bool
simpleSurfelE( Surfel * surfel, int & eNeighbors, VNeighborhood & nb )
{
  nb = computeVNeighborhood( surfel );
  int i;
  unsigned int config = 0;

  eNeighbors = 0;
  if ( nb.eNeighbors[0]->color & InsideMask )
    { config |= 1;  ++eNeighbors; }
  if ( nb.eNeighbors[1]->color & InsideMask )
    { config |= 4;  ++eNeighbors; }
  if ( nb.eNeighbors[2]->color & InsideMask )
    { config |= 16; ++eNeighbors; }
  if ( nb.eNeighbors[3]->color & InsideMask )
    { config |= 64; ++eNeighbors; }

  i = 0;
  while ( ( i < nb.loopSize[1] )  && ( nb.loops[1][ i ]->color & InsideMask ) )
    ++i;
  if ( i == nb.loopSize[1] ) config |= 2;
  i = 0;
  while ( ( i < nb.loopSize[2] )  && ( nb.loops[2][ i ]->color & InsideMask ) )
    ++i;
  if ( i == nb.loopSize[2] ) config |= 8;
  i = 0;
  while ( ( i < nb.loopSize[3] )  && ( nb.loops[3][ i ]->color & InsideMask ) )
    ++i;
  if ( i == nb.loopSize[3] ) config |= 32;
  i = 0;
  while ( ( i < nb.loopSize[0] )  && ( nb.loops[0][ i ]->color & InsideMask ) )
    ++i;
  if ( i == nb.loopSize[0] ) config |= 128;

  return SimplePixelConfig4[ config ] != 0;
}

bool
simpleSurfelV( Surfel *surfel, int & vNeighbors, VNeighborhood & nb )
{
  nb = computeVNeighborhood( surfel );
  int i;
  unsigned int config = 0;
  vNeighbors = 0;
  bool e0Inside = false;
  bool e1Inside = false;
  bool e2Inside = false;
  bool e3Inside = false;
  if ( nb.eNeighbors[0]->color & InsideMask ) { 
    config |= 1;
    e0Inside = true;
    ++vNeighbors;
  }
  if ( nb.eNeighbors[1]->color & InsideMask ) { 
    config |= 4;
    e1Inside = true;
    ++vNeighbors;
  }
  if ( nb.eNeighbors[2]->color & InsideMask ) { 
    config |= 16;
    e2Inside = true;
    ++vNeighbors;
  }
  if ( nb.eNeighbors[3]->color & InsideMask ) { 
    config |= 64;
    e3Inside = true;
    ++vNeighbors;
  }
  i = 0;
  if ( nb.loopSize[1] )
    while ( i < nb.loopSize[1] ) {
      if ( nb.loops[1][ i ]->color & InsideMask ) {
	++vNeighbors;
	config |= 2;
      }
      ++i; 
    }
  else if ( e0Inside || e1Inside ) config |= 2;
  i = 0;
  if ( nb.loopSize[2] )
    while ( i < nb.loopSize[2] ) {
      if ( nb.loops[2][ i ]->color & InsideMask ) {
	++vNeighbors;
	config |= 8;
      }
      ++i; 
    }
  else if ( e1Inside || e2Inside ) config |= 8;
  i = 0;
  if ( nb.loopSize[3] )
    while ( i < nb.loopSize[3] ) {
      if ( nb.loops[3][ i ]->color & InsideMask ) {
	++vNeighbors;
	config |= 32;
      }
      ++i; 
    }
  else if ( e2Inside || e3Inside ) config |= 32;
  i = 0;
  if ( nb.loopSize[0] )
    while ( i < nb.loopSize[0] ) {
      if ( nb.loops[0][ i ]->color & InsideMask ) {
	++vNeighbors;
	config |= 128;
      }
      ++i; 
    }
  else if ( e3Inside || e0Inside ) config |= 128;
  return SimplePixelConfig8[ config ] != 0;
}

bool
borderSurfelE( Surfel * surfel )
{
  if ( ! (  ( surfel->neighbors[0]->color & InsideMask ) &&
	    ( surfel->neighbors[1]->color & InsideMask ) &&
	    ( surfel->neighbors[2]->color & InsideMask ) &&
	    ( surfel->neighbors[3]->color & InsideMask ) ) )
    return true; 
  VNeighborhood nb = computeVNeighborhood( surfel );
  int i = 0;
  while ( ( i < nb.loopSize[1] ) && ( nb.loops[1][i]->color & InsideMask ) )
    ++i;
  if ( i != nb.loopSize[1] ) return true;
  i = 0;
  while ( ( i < nb.loopSize[2] ) && ( nb.loops[2][i]->color & InsideMask ) )
    ++i;
  if ( i != nb.loopSize[2] ) return true;
  i = 0;
  while ( ( i < nb.loopSize[3] ) && ( nb.loops[3][i]->color & InsideMask ) )
    ++i;
  if ( i != nb.loopSize[3] ) return true;
  i = 0;
  while ( ( i < nb.loopSize[0] ) && ( nb.loops[0][i]->color & InsideMask ) )
    ++i;
  if ( i != nb.loopSize[0] ) return true;

  return false;  
}

bool eExtremity( Surfel * surfel )
{
  int count = 0;
  Surfel *neighbor = 0;
  int neighborEdge;
  if ( surfel->neighbors[0]->color & InsideMask ) {
    ++count; 
    neighbor = surfel->neighbors[0];
    neighborEdge = 0;
  }
  if ( surfel->neighbors[1]->color & InsideMask ) {
    ++count; 
    neighbor = surfel->neighbors[1];
    neighborEdge = 1;
  }
  if ( surfel->neighbors[2]->color & InsideMask ) {
    ++count; 
    neighbor = surfel->neighbors[2];
    neighborEdge = 2;
 }
  if ( surfel->neighbors[3]->color & InsideMask ) {
    ++count;
    neighbor = surfel->neighbors[3];
    neighborEdge = 3;
  }
  if ( count != 1 ) return false;
  int edge = edgeFromTable[ surfel->direction ][ neighborEdge ][ neighbor->direction ];
  
  return !( ( neighbor->neighbors[ (edge+1)%4 ]->color & InsideMask )
	    || ( neighbor->neighbors[ (edge+3)%4 ]->color & InsideMask ) );
}


void
surfaceThinningV( SurfelList & surfelList, 
		  int iterations,
		  bool keepExtremities )
{
  vector<Surfel*> border1, border2;
  vector<Surfel*> *currentBorder = &border1;
  vector<Surfel*> *nextBorder = &border2;
  
  /*
   * Build the border list.
   */
  Surfel * surfel = surfelList.begin();
  Surfel * end = surfelList.end();
  while ( surfel != end ) {
    if ( surfel->color ) surfel->color = InsideMask;
    ++surfel;
  }
  surfel = surfelList.begin();
  while ( surfel != end ) {
    if ( surfel->color && borderSurfelV( surfel ) ) {
      currentBorder->push_back( surfel );
      surfel->color = BorderMask;
    }
    ++surfel;
  }
  
  VNeighborhood nb;
  int neighbors;
  vector<Surfel*>::iterator itSurfel, itEnd;  
  while ( iterations-- ) {
    itSurfel = currentBorder->begin();
    itEnd = currentBorder->end();
    while ( itSurfel != itEnd ) {
      surfel = *itSurfel;
      if ( simpleSurfelV( surfel, neighbors, nb )
	   && ( !keepExtremities || neighbors != 1 ) ) {
	surfel->color = 0;
	if (  surfel->neighbors[0]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[0] );
	  surfel->neighbors[0]->color |= BorderMask;
	}
	if ( surfel->neighbors[1]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[1] );
	  surfel->neighbors[1]->color |= BorderMask;
	}
	if ( surfel->neighbors[2]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[2] );
	  surfel->neighbors[2]->color |= BorderMask;
	}
	if ( surfel->neighbors[3]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[3] );
	  surfel->neighbors[3]->color |= BorderMask;
	}
      } else nextBorder->push_back( surfel );
      ++itSurfel;
    }
    swap( currentBorder, nextBorder );
    nextBorder->clear();
  }
}


void
surfaceThinningE( SurfelList & surfelList,
		  int iterations,
		  bool keepExtremities )
{
  vector<Surfel*> border1, border2;
  vector<Surfel*> *currentBorder = &border1;
  vector<Surfel*> *nextBorder = &border2;
  
  /*
   * Build the border list.
   */
  Surfel * surfel = surfelList.begin();
  Surfel * end = surfelList.end();
  while ( surfel != end ) {
    if ( surfel->color ) surfel->color = InsideMask;
    ++surfel;
  }
  surfel = surfelList.begin();
  while ( surfel != end ) {
    if ( surfel->color && borderSurfelE( surfel ) ) {
      currentBorder->push_back( surfel );
      surfel->color = BorderMask;
    }
    ++surfel;
  }
  
  VNeighborhood nb;
  int neighbors;
  int i;
  Surfel * s;
  vector<Surfel*>::iterator itSurfel, itEnd;  
  while ( iterations-- ) {
    itSurfel = currentBorder->begin();
    itEnd = currentBorder->end();
    while ( itSurfel != itEnd ) {
      surfel = *itSurfel;
      if ( simpleSurfelE( surfel, neighbors, nb )
	   && ( !keepExtremities || ! eExtremity( surfel ) /* neighbors != 1 */ ) ) {
	surfel->color = 0;       
	if ( surfel->neighbors[0]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[0] );
	  surfel->neighbors[0]->color |= BorderMask;
	}
	if ( surfel->neighbors[1]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[1] );
	  surfel->neighbors[1]->color |= BorderMask;
	}
	if ( surfel->neighbors[2]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[2] );
	  surfel->neighbors[2]->color |= BorderMask;
	}
	if ( surfel->neighbors[3]->color == InsideMask ) {
	  nextBorder->push_back( surfel->neighbors[3] );
	  surfel->neighbors[3]->color |= BorderMask;
	}
	i = 0;
	while ( i < nb.loopSize[1] ) {
	  s = nb.loops[1][i];
	  if ( s->color == InsideMask ) {
	    nextBorder->push_back( s );
	    s->color |= BorderMask;
	  }
	  ++i;
	}
	i = 0;
	while ( i < nb.loopSize[2] ) {
	  s = nb.loops[2][i];
	  if ( s->color == InsideMask ) {
	    nextBorder->push_back( s );
	    s->color |= BorderMask;
	  }
	  ++i;
	}
	i = 0;
	while ( i < nb.loopSize[3] ) {
	  s = nb.loops[3][i];
	  if ( s->color == InsideMask ) {
	    nextBorder->push_back( s );
	    s->color |= BorderMask;
	  }
	  ++i;
	}
	i = 0;
	while ( i < nb.loopSize[0] ) {
	  s = nb.loops[0][i];
	  if ( s->color == InsideMask ) {
	    nextBorder->push_back( s );
	    s->color |= BorderMask;
	  }
	  ++i;
	}
      } else nextBorder->push_back( surfel );
      ++itSurfel;
    }
    swap( currentBorder, nextBorder );
    nextBorder->clear();
  }
}

Surfel *
singleNeighborE( Surfel * surfel )
{
  int count = 0;
  int neighborEdge = 0;
  if ( surfel->neighbors[0]->color & InsideMask ) {
    ++count; 
    neighborEdge = 0;
  }
  if ( surfel->neighbors[1]->color & InsideMask ) {
    ++count; 
    neighborEdge = 1;
  }
  if ( surfel->neighbors[2]->color & InsideMask ) {
    ++count; 
    neighborEdge = 2;
  }
  if ( surfel->neighbors[3]->color & InsideMask ) {
    ++count; 
    neighborEdge = 3;
  }
  if ( count != 1 ) return surfel;
  return surfel->neighbors[ neighborEdge ];
}

Surfel *
singleNeighborV( Surfel * surfel )
{
  VNeighborhood nb = computeVNeighborhood( surfel );
  int i;
  int vNeighbors = 0;
  Surfel ** loop = 0;
  int position = 0;

  for ( int e = 0; e < 4; ++e ) 
    if ( nb.eNeighbors[e]->color & InsideMask ) {
      ++vNeighbors; 
      position = e;
    }
  
  if ( vNeighbors > 1 )
    return surfel;
  
  for ( int l = 0; l < 4; ++l ) 
    for ( i = 0;  i < nb.loopSize[l]; ++i )
      if ( nb.loops[l][i]->color & InsideMask ) {
	++vNeighbors;
	loop = nb.loops[l];
	position = i;
      }
  
  if ( vNeighbors != 1 )
    return surfel;
  if ( loop )
    return loop[ position ];
  return surfel->neighbors[ position ];
}

Surfel *
secondENeighborIfUnique( Surfel *surfel, Surfel *comeFrom )
{
  int count = 0;
  int neighborEdge = 0;
  Surfel * ps;
  for ( int e = 0; e < 4; ++e ) {
    ps = surfel->neighbors[e];
    if ( ps != comeFrom && ps->color & InsideMask ) {
      ++count;
      neighborEdge = e;
    }
  }
  if ( count != 1 ) return surfel;
  return surfel->neighbors[ neighborEdge ];
}

Surfel *
secondVNeighborIfUnique( Surfel *surfel, Surfel *comeFrom )
{
  VNeighborhood nb = computeVNeighborhood( surfel );
  int i;
  int vNeighbors = 0;
  Surfel **loop = 0;
  int position = 0;
  
  for ( int e = 0; e < 4; ++e )
    if ( ( nb.eNeighbors[e]->color & InsideMask ) && 
	 ( nb.eNeighbors[e] != comeFrom ) ) {
      ++vNeighbors;
      position = e; 
    }

  if ( vNeighbors > 1 ) return surfel;
  
  for ( int l = 0; l < 4; ++l )
    for ( i = 0;  i < nb.loopSize[l]; ++i ) { 
      if ( ( nb.loops[l][ i ]->color & InsideMask ) && 
	   ( nb.loops[l][ i ] != comeFrom ) ) {
	++vNeighbors;
	loop = nb.loops[l];
	position = i;
      }
    }

  if ( vNeighbors != 1 )
    return surfel;
  if ( loop )
    return loop[ position ];
  return surfel->neighbors[ position ];
}

void
pruneEbranches( SurfelList & surfelList,
		int maxLength ) { 
  Surfel *surfel = surfelList.begin();
  Surfel *end = surfelList.end();
  Surfel *ps, *from, *next;
  int count;
  VNeighborhood vnb;
  int neighbors;

  while ( surfel != end ) {
    ps = singleNeighborE( surfel );
    if ( ps != surfel ) { 
      count = 0;
      from = surfel;
      while ( ( next = secondENeighborIfUnique( ps, from ) ) != ps ) {
	++count;
	from = ps;
	ps = next;
      }
      if ( count < maxLength )  { 
	ps = singleNeighborE( surfel );	
	from = surfel;
	while ( ( next = secondENeighborIfUnique( ps, from ) ) != ps ) {
	  ++count;
	  from->color = 0;
	  from = ps;
	  ps = next;
	}
 	if ( simpleSurfelE( ps, neighbors, vnb ) )
	  ps->color = 0;
      }
    }
    ++surfel;
  }
  
}

void
pruneVbranches( SurfelList & surfelList,
		int maxLength )
{ 
  Surfel *surfel = surfelList.begin();
  Surfel *end = surfelList.end();
  Surfel *ps, *from, *next;
  int count;
  VNeighborhood vnb;
  int neighbors;

  while ( surfel != end ) {
    ps = singleNeighborV( surfel );
    if ( ps != surfel ) { 
      count = 0;
      from = surfel;
      while ( ( next = secondVNeighborIfUnique( ps, from ) ) != ps ) {
	++count;
	from = ps;
	ps = next;
      }
      if ( count && count < maxLength )  { 
	ps = singleNeighborE( surfel );
	from = surfel;
	while ( ( next = secondVNeighborIfUnique( ps, from ) ) != ps ) {
	  ++count;
	  from->color = 0;
	  from = ps;
	  ps = next;
	}
 	if ( simpleSurfelV( ps, neighbors, vnb ) )
	  ps->color = 0;
      }
    }
    ++surfel;
  }
}
