/**
 * @file   Evolver.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:39:12 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "Evolver.h"
#include "Convolution.h"
#include "Surface.h"
#include "SurfelList.h"
#include "SurfelData.h"

namespace {
inline double rounded( double x ) {
  return floor( x + 0.5 );
}
}

Evolver::Evolver( Surface *surface, ZZZImage<UChar> *volume )
{
  CONS( Evolver );
  _surface = surface;
  _volume = volume;
}

Evolver::~Evolver()
{

}

void Evolver::evolve( int curvatureIterations, int averagingIterations, Z3toR3 & borderMap )
{
  ZZZImage<UChar> src;
  SSize width, height, depth, bands;
  _volume->getDimension( width, height, depth, bands );

  _surface->computeCurvature( curvatureIterations,
                              curvatureIterations,
                              averagingIterations,
                              MEAN_CURVATURE,
                              false );

  SurfelDataArray normalsArray = _surface->surfelDataArray();

  SurfelDataArray::iterator normals =  normalsArray.begin();
  SurfelDataArray::iterator normal = normals;
  SurfelDataArray::iterator normalsEnd = normalsArray.end();
  while ( normal != normalsEnd )
    normalize( *(normal++) );
  
  src = *_volume;
  UChar *** dstMatrix = _volume->data( 0 );

  Surfel *ps = _surface->surfelList().begin();
  Surfel *end = _surface->surfelList().end();
  UInt index = 0;

  //double kappa = -0.5 / std::max( fabs( _surface->minMeanCurvature() ),
  //		       fabs( _surface->maxMeanCurvature() ) );
  double kappa = -0.5;
  normal = normals;
  while ( ps < end ) {
    double mean = ps->meanCurvature;
    Triple<double> realPosition;
    // if ( fabs(mean) < 0.2 )  mean = 0.0;

    if ( borderMap.count( ps->voxel ) )
      realPosition = borderMap[ ps->voxel ];
    else
      realPosition = ( borderMap[ ps->voxel ] =
          Triple<double>( ps->voxel ) );
    
    Triple<double> voxelCenter( realPosition );
    Triple<double> sd = (*normal)  * kappa *  ( mean /* * kappa */ );
    //if ( mean < 0 ) sd = (*normal)  * ( -0.5 * mean );
    voxelCenter += sd;
    //double norm = mean * kappa;

    SSize x = static_cast<SSize>( ::rounded( voxelCenter.first ) );
    SSize y = static_cast<SSize>( ::rounded( voxelCenter.second ) );
    SSize z = static_cast<SSize>( ::rounded( voxelCenter.third ) );

    if (  ( ( x != ps->voxel.first )
            || ( y != ps->voxel.second )
            || ( z != ps->voxel.third ) )
          && ( x - ps->voxel.first > -2 ) && ( x - ps->voxel.first < 2 )
          && ( y - ps->voxel.second > -2 ) && ( y - ps->voxel.second < 2 )
          && ( z - ps->voxel.third > -2 ) && ( z - ps->voxel.third < 2 )
          ) {
      if ( dstMatrix[ z ][ y ][ x ] ) {
        if ( cnfSimple6( _volume->config( ps->voxel ) ) ) {
          dstMatrix[ ps->voxel.third ][ ps->voxel.second ][ ps->voxel.first ] = 0;
          borderMap[ Triple<SSize>( x, y, z ) ] = voxelCenter;
        }
      } else {
        if ( ( z < depth ) && ( y < height ) && ( x < width ) ) {
          if ( cnfSimple6( MASK_CENTER | _volume->config( x, y, z ) ) /* cnf & MASK_6_NEIGHBORS */  ) {
            dstMatrix[ z ][ y ][ x ] = 1;
            borderMap[ Triple<SSize>( x, y, z ) ] = voxelCenter;
          }
        }
      }
    } else
      borderMap[ Triple<SSize>( x, y, z ) ] = voxelCenter;
    ps++; index++; normal++;
  }
  
}
