/**
 * @file   EdgelList.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:29 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "EdgelList.h"
#include "EdgelListBuilder.h"
#include <zzzimage.h>

using std::ostream;

void
EdgelList::clear()
{
  _end = _array;
}

EdgelList::EdgelList(Size size )
{
  CONS( EdgelList );
  _sortedArray = 0;
  if ( !size ) {
   _array = _current = _limit = _end = 0;
  } else {
    _array = new Edgel[ size ];
    _current = _end = _array;
    _limit = _array + size;
  }
}

EdgelList::~EdgelList()
{
  delete[] _array;
  delete[] _sortedArray;
}

ostream &
operator<<( ostream & out, const Edgel & edgel )
{
  out << "Edgel( " << edgel.direction << " 0("
      << edgel.x << "," << edgel.y << ");"
      << " V(" << edgel.xp << "," << edgel.yp << ")) ";
   return out;
}

bool
Edgel::operator>( const Edgel & other ) const
{
   if ( xp > other.xp ) return true;
   if ( xp == other.xp && yp > other.yp ) return true;
   return false;
}

int
edgelCompare( Edgel **a, Edgel **b)
{
   const Edgel *psa = *a;
   const Edgel *psb = *b;
   if ( psa->xp > psb->xp ) return 1;
   if ( psa->xp == psb->xp ) {
     if ( psa->yp > psb->yp ) return 1;
     if ( psa->yp == psb->yp && psa->direction > psb->direction ) return 1;
     if ( psa->yp == psb->yp && psa->direction == psb->direction ) return 0;
   }
   return -1;
}

void
Edgel::set(double x, double y, Direction direction, UChar color)
{
   Edgel::x = x; Edgel::y = y;
   Edgel::direction = direction;
   Edgel::color = color;
   curvature = 0.0;
}

void
Edgel::setPixel(SSize x, SSize y,
                Direction direction, UChar color)
{
   Edgel::x = xp = x;
   Edgel::y = yp = y;
   switch ( direction ) {
   case XPlusDirection: Edgel::x += 0.5; break;
   case XMinusDirection: Edgel::x -= 0.5; break;
   case YPlusDirection: Edgel::y += 0.5; break;
   case YMinusDirection: Edgel::y -= 0.5; break;
   }
   Edgel::direction = direction;
   Edgel::color = color;
}


void
EdgelList::resize(Size size )
{
  disposeArray( _array );
  if ( size ) {
    _array = new Edgel[ size ];
    _current = _end = _array;
  } else _array = _current = _end = 0;
  _limit = _array + size;
}

void
EdgelList::addPixel( SSize x, SSize y,
                     Edgel::Direction direction,
                     UChar color)
{
  Size formerSize = _end - _array;
  if ( _end == _limit ) {
    Edgel *newList;
    newList = new Edgel[ 2 * formerSize ];
    memcpy( newList, _array, formerSize * sizeof( Edgel ) );
    disposeArray( _array );
    _array = _current = newList;
    _limit = _array + 2 * formerSize;
    _end = _array + formerSize;
  }
  _end->setPixel( x, y, direction, color );
  ++_end;
}

void
EdgelList::buildSortedArray()
{
  Edgel *ps = _array;
  Edgel **ppe = 0;
  disposeArray( _sortedArray );
  if ( ! _array ) return;
  _sortedArray = new Edgel*[ _end - _array ];
  ppe = _sortedArray;
  while ( ps < _end ) *(ppe++) = ps++;
  qsort( _sortedArray, _end - _array, sizeof(Edgel*),
   ( int (*)(const void*, const void*)) & edgelCompare);
}

Edgel*
EdgelList::find( SSize x, SSize y, Edgel::Direction direction )
{
   Edgel *pe = _array;
   while ( pe < _end ) {
     if ( pe->xp == x &&
    pe->yp == y &&
    pe->direction == direction )
       return pe;
     ++pe;
   }
   return 0;
}

void
EdgelList::color( UChar c )
{
   Edgel *pe = _array;
   while ( pe < _end ) {
     pe->color = c;
     ++pe;
   }
}

Edgel*
EdgelList::quickFind( SSize x, SSize y, Edgel::Direction direction )
{
  Edgel s;
  Edgel *ps = &s;
  Edgel **left = _sortedArray;
  Edgel **right = _sortedArray + ( _end - _array ) - 1;
  Edgel **middle = 0;
  s.xp = x; s.yp = y; s.direction = direction;

  while ( left <= right ) {
    middle =  left + ( right - left ) / 2;
    if ( ( s.xp == (*middle)->xp ) &&
   ( s.yp == (*middle)->yp ) &&
   ( s.direction == (*middle)->direction ) )
      return *middle;
    if ( edgelCompare( middle, &ps ) == -1 )
      left = middle + 1;
    else
      right = middle - 1;
  }
  ERROR << "EdgelList::quickFind(USort,USort,USort,Direction): Dichotomy error\n";
  ERROR << "          " << x << " " << y << " " << direction << "\n";
  ERROR << "  Middle: " << (*middle)->xp << "," << (*middle)->yp << " / " << (*middle)->direction << std::endl;

  left = middle - 10;;
  right = middle + 10;
  while ( left < right ) {
    TRACE << (*left)->xp << "," << (*left)->yp << "/"
    << (*left)->direction << " " << std::endl;
    ++left;
  }

  exit(0);
  return 0;
}

Edgel*
EdgelList::quickFind( Edgel & edgel )
{
   Edgel *ps = &edgel;
   Edgel **left = _sortedArray;
   Edgel **right = _sortedArray + ( _end - _array ) - 1;
   Edgel **middle;

   while ( left <= right ) {
      middle =  left + ( right - left ) / 2;
      if ( ( edgel.xp == (*middle)->xp ) &&
     ( edgel.yp == (*middle)->yp ) &&
     ( edgel.direction == (*middle)->direction ) )
   return *middle;
      if ( edgelCompare( middle, &ps ) == -1 )
   left = middle + 1;
      else
   right = middle - 1;
   }
   ERROR << "EdgelList::quickFindFace(Edgel&): Dichotomy error\n";
   exit(0);
   return 0;
}

void
EdgelList::buildFromImage( AbstractImage & image,
                           UChar min )
{
   EdgelListBuilder builder(*this, min, false );
   image.accept(builder);
}

void
EdgelList::updateFromImage( AbstractImage & image,
                            UChar min )
{
   EdgelListBuilder builder(*this, min, true );
   image.accept(builder);
}


ostream &
operator<<( ostream & out, const EdgelList & edgelList )
{
  Edgel * ps = edgelList._array;
  while ( ps != edgelList._end ) {
    out << *ps << std::endl;
    ++ps;
  }
  return out;
}

void
EdgelList::dumpCurvatures( std::ostream & out )
{
  Edgel * ps = _array->next;
  Int i = 0;
  while ( ps != _array ) {
    out << ( i++ ) << "\t" << ps->curvature << "\n";
    ps = ps->next;
  }
}

void
EdgelList::curvatures( std::vector<double> & v )
{
  v.clear();
  bool * visited;
  visited = new bool [ count() ];
  memset( visited, 0, count() * sizeof( bool ) );
  bool added = true;
  while ( added ) {
    Edgel *start = _array;
    Int i = 0;
    while ( visited[ i ] && start < _end ) { ++i; ++start; }
    if ( start != _end ) {
      Edgel * ps = start->next;
      while ( ps != start ) {
  v.push_back( ps->curvature );
  if ( visited[ ps - _array ] ) exit( 0 );
  visited[ ps - _array ] = true;
  ps = ps->next;
      }
      v.push_back( ps->curvature );
      visited[ ps - _array ] = true;
    }
    else added = false;
  }
  disposeArray( visited );
}
