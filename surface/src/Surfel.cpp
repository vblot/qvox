/**
 * @file   Surfel.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:29 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "SurfelList.h"
#include "EdgelList.h"
#include <Triple.h>
#include <Array.h>

using std::ostream;
using std::vector;

const int edgeFromTable[6][4][6+2] =
  { { { 2, -1, -1, -1, 3, 1, 255, 255 },
      { 3, -1, 3, 3, -1, -1, 255, 255 },
      { 0, -1, -1, -1, 1, 3, 255, 255 },
      { 1, -1, 1, 1, -1, -1, 255, 255 } },
    { { -1, 2, -1, -1, 1, 3, 255, 255 },
      { -1, 3, 3, 3, -1, -1, 255, 255 },
      { -1, 0, -1, -1, 3, 1, 255, 255 },
      { -1, 1, 1, 1, -1, -1, 255, 255 } },
    { { -1, -1, 2, -1, 0, 0, 255, 255 },
      { 3, 3, 3, -1, -1, -1, 255, 255 },
      { -1, -1, 0, -1, 2, 2, 255, 255 },
      { 1, 1, 1, -1, -1, -1, 255, 255 } },
    { { -1, -1, -1, 2, 2, 2, 255, 255 },
      { 3, 3, -1, 3, -1, -1, 255, 255 },
      { -1, -1, -1, 0, 0, 0, 255, 255 },
      { 1, 1, -1, 1, -1, -1, 255, 255 } },
    { { -1, -1, 0, 2, 2, -1, 255, 255 },
      { 2, 0, -1, -1, 3, -1, 255, 255 },
      { -1, -1, 2, 0, 0, -1, 255, 255 },
      { 0, 2, -1, -1, 1, -1, 255, 255 } },
    { { -1, -1, 0, 2, -1, 2, 255, 255 },
      { 0, 2, -1, -1, -1, 3, 255, 255 },
      { -1, -1, 2, 0, -1, 0, 255, 255 },
      { 2, 0, -1, -1, -1, 1, 255, 255 } } };

int
pSurfelZCompare(const Surfel **a, const Surfel **b)
{
   if ( (*a)->rotatedCenter.third < (*b)->rotatedCenter.third)
     return 1;
   return -1;
}

int
pSurfelZCompareReverse(const Surfel **a, const Surfel **b)
{
  if ( (*a)->rotatedCenter.third > (*b)->rotatedCenter.third ) return 1;
  return -1;
}

ostream &
operator<<( ostream & out, const Surfel & surfel)
{
  out << "Surfel[ C:" << surfel.center
      << " N: " << surfel.rotatedCenter
      << " V: " << surfel.voxel << " ]";
  return out;
}

bool
Surfel::operator>( const Surfel & other ) const
{
   if ( voxel.first > other.voxel.first ) return true;
   if ( voxel.first == other.voxel.first ) {
      if ( voxel.second > other.voxel.second ) return true;
      if ( voxel.second == other.voxel.second ) {
	 if ( voxel.third > other.voxel.third ) return true;
	 if ( voxel.third == other.voxel.third
	      && direction > other.direction ) return true;
      }
   }
   return false;
}

int
surfelCompare( Surfel **a, Surfel **b)
{
  const Surfel *psa = *a;
  const Surfel *psb = *b;
  if ( psa->voxel.first > psb->voxel.first ) return 1;
  if ( psa->voxel.first == psb->voxel.first ) {
    if ( psa->voxel.second > psb->voxel.second ) return 1;
    if ( psa->voxel.second == psb->voxel.second ) {
      if ( psa->voxel.third > psb->voxel.third ) return 1;
      if ( psa->voxel.third == psb->voxel.third ) {
        if ( psa->direction > psb->direction ) return 1;
        else if ( psa->direction == psb->direction ) return 0;
      }
    }
  }
  return -1;
}

void
Surfel::set( double x, double y, double z, int direction, UChar color )
{
   center.set( x, y , z );
   rotatedCenter.set( x, y, z );
   Surfel::direction = direction;
   Surfel::color  = color;
   meanCurvature = gaussianCurvature = 0.0;
}

void
Surfel::setVoxel(Short x, Short y, Short z,
		 int direction, UChar color)
{
  voxel.set( x, y, z );
  center.set( x, y, z );
  switch ( direction ) {
  case 0: center.first += 0.5; break;
  case 1: center.first -= 0.5; break;
  case 2: center.second += 0.5; break;
  case 3: center.second -= 0.5; break;
  case 4: center.third += 0.5; break;
  case 5: center.third -= 0.5; break;
  }
  rotatedCenter = center;
  Surfel::direction = direction;
  Surfel::color  = color;
}


