/**
 * @file   Convolution.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:44:28 2006
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Convolution.h"
#include "tools.h"
#include "Triple.h"
#include "ColorMap.h"
#include "zzzimage.h"
#include <map>

namespace {
  /**
   * Retourne n-1 modulo 4
   * @param n Un entier compris entre 0 et 3.
   * @return n-1 mod 4
   */
  inline int
  minusOneMod4( int n )
  {
    return (n+3)%4;
  }
}

VNeighborhood::VNeighborhood( Surfel * surfel )
{
   memset( this, 0, sizeof(VNeighborhood) );
   if ( !surfel )
     return;
   
   center = surfel;
   eNeighbors = surfel->neighbors;

   int edge;
   Surfel *newSurfel, *prevSurfel, *stop;

   newSurfel = surfel->neighbors[0];
   stop = surfel->neighbors[1];
   edge = edgeFrom( surfel->direction, 0, newSurfel->direction );
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4( edge ) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4( edge ),
		      newSurfel->direction );
     if ( newSurfel != stop ) {
       loops[1][ loopSize[1] ] = newSurfel;
       loopSize[1]++;
     }   
   } while ( newSurfel != stop );

   newSurfel = surfel->neighbors[1];
   stop = surfel->neighbors[2];
   edge = edgeFrom( surfel->direction, 1, newSurfel->direction );
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       loops[2][ loopSize[2] ] = newSurfel;
       loopSize[2]++;
     }      
   } while ( newSurfel != stop );
   
   newSurfel = surfel->neighbors[2];
   stop =  surfel->neighbors[3];
   edge = edgeFrom( surfel->direction, 2, newSurfel->direction);   
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       loops[3][ loopSize[3] ] = newSurfel;
       loopSize[3]++;
     }      
   } while ( newSurfel != stop );
   
   newSurfel = surfel->neighbors[3];
   stop =  surfel->neighbors[0];
   edge = edgeFrom( surfel->direction, 3, newSurfel->direction);
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       loops[0][ loopSize[0] ] = newSurfel;
       loopSize[0]++;
     }      
   } while ( newSurfel != stop );
}

/**
 * Retourne le surfel correspondant � l'application
 * du masque 4-2-1 sur le v-voisinage d'une face. 
 * @param f Une face.
 * @return Le surfel correspondant a l'application du 
 *         masque de convolution.
 */
SurfelData
convolution421( Surfel *surfel,
		const SurfelList & surfelList,
		const SurfelDataArray & sdaSrc,
		SurfelDataArray & sdaDest,
		double cCentral /* = 4.0F */ ,
		double cEdge /* = 2.0F */ , 
		double cVertex /* = 1.0F */ )
{
   const Surfel * slArray = surfelList.begin();
   SurfelDataArray::const_iterator  srcArray = sdaSrc.begin();
   double contributionEvoisin0;
   double contributionEvoisin1;
   double contributionEvoisin2;
   double contributionEvoisin3;
   VNeighborhood v( surfel );
   int i;
   SurfelData sd;
   sd = 0.0;

   contributionEvoisin0 = cEdge;
   contributionEvoisin1 = cEdge;
   contributionEvoisin2 = cEdge;
   contributionEvoisin3 = cEdge;

   if ( ! v.loopSize[0] ) {
      contributionEvoisin0 += 0.5 * cVertex;
      contributionEvoisin3 += 0.5 * cVertex;
   }
   
   if ( ! v.loopSize[1] ) {
      contributionEvoisin0 += 0.5 * cVertex;
      contributionEvoisin1 += 0.5 * cVertex;
   }
      
   if ( ! v.loopSize[2] ) {
      contributionEvoisin1 += 0.5 * cVertex;
      contributionEvoisin2 += 0.5 * cVertex;
   }
   
   if ( ! v.loopSize[3] ) {
      contributionEvoisin2 += 0.5 * cVertex;
      contributionEvoisin3 += 0.5 * cVertex;
   }
   
   sd += *( srcArray + (v.center - slArray )) * cCentral;
   sd += *( srcArray + (v.eNeighbors[0] - slArray )) * contributionEvoisin0;
   sd += *( srcArray + (v.eNeighbors[1] - slArray )) * contributionEvoisin1;
   sd += *( srcArray + (v.eNeighbors[2] - slArray )) * contributionEvoisin2;
   sd += *( srcArray + (v.eNeighbors[3] - slArray )) * contributionEvoisin3;
   
   double w;
   for ( int loop = 0; loop < 4; ++loop ) {
     w = cVertex / v.loopSize[ loop ];
     for (i = 0; i < v.loopSize[loop]; i++)
       sd +=  *(srcArray + ( v.loops[loop][i] - slArray )) * w;
   }   
   sd /= ( cCentral + 4.0 * cEdge + 4.0 * cVertex );
   sdaDest[  surfel - slArray ] = sd;
   return sd;
}

/**
 * Retourne le surfel correspondant � l'application
 * du masque isotrope sur le v-voisinage d'une face. 
 * @param f Une face.
 * @return Le surfel correspondant a l'application du 
 *         masque de convolution.
 */
SurfelData
convolutionIso( Surfel *surfel,
		SurfelList & surfelList,
		SurfelDataArray & sdaSrc,
		SurfelDataArray & sdaDest,
		double weight /* = 4.0F */ )
{
   Surfel * slArray = surfelList.begin();
   SurfelDataArray::iterator  srcArray = sdaSrc.begin();
   double contributionS;
   double contributionEvoisin0;
   double contributionEvoisin1;
   double contributionEvoisin2;
   double contributionEvoisin3;
   double contributionLoop0 = 0;
   double contributionLoop1 = 0;
   double contributionLoop2 = 0;
   double contributionLoop3 = 0;
   VNeighborhood v( surfel );
   
   int i;
   SurfelData sd;
   sd = 0.0;
   
  
   contributionLoop0 = weight / ( v.loopSize[0] + 3 ); 
   contributionLoop1 = weight / ( v.loopSize[1] + 3 ); 
   contributionLoop2 = weight / ( v.loopSize[2] + 3 ); 
   contributionLoop3 = weight / ( v.loopSize[3] + 3 ); 

   contributionEvoisin0 = contributionLoop0 + contributionLoop1;
   contributionEvoisin1 = contributionLoop1 + contributionLoop2;
   contributionEvoisin2 = contributionLoop2 + contributionLoop3;
   contributionEvoisin3 = contributionLoop3 + contributionLoop0;

   contributionS = contributionEvoisin0 + contributionEvoisin2;

   sd += *( srcArray + (v.center - slArray )) * contributionS;
   sd += *( srcArray + (v.eNeighbors[0] - slArray )) * contributionEvoisin0;
   sd += *( srcArray + (v.eNeighbors[1] - slArray )) * contributionEvoisin1;
   sd += *( srcArray + (v.eNeighbors[2] - slArray )) * contributionEvoisin2;
   sd += *( srcArray + (v.eNeighbors[3] - slArray )) * contributionEvoisin3;
   
   int size;
   size = v.loopSize[ 0 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[0][i] - slArray )) * contributionLoop0;
   size = v.loopSize[ 1 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[1][i] - slArray )) * contributionLoop1;
   size = v.loopSize[ 2 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[2][i] - slArray )) * contributionLoop2;
   size = v.loopSize[ 3 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[3][i] - slArray )) * contributionLoop3;
   
   sd /= 4 * weight;
   sdaDest[ v.center - slArray ] = sd;
   return sd;
}


/**
 * Retourne le surfel correspondant � l'application
 * du masque constant sur le v-voisinage d'une face. 
 * @param f Une face.
 * @return Le surfel correspondant a l'application du 
 *         masque de convolution.
 */
SurfelData
convolutionConst( Surfel *surfel,
		  SurfelList & surfelList,
		  SurfelDataArray & sdaSrc,
		  SurfelDataArray & sdaDest )
{
   Surfel * slArray = surfelList.begin();
   SurfelDataArray::iterator  srcArray = sdaSrc.begin();
   VNeighborhood v( surfel );
   int i;
   SurfelData sd;
   sd = 0.0;

   double scale = 5 + v.loopSize[0] + v.loopSize[1]
     + v.loopSize[2] + v.loopSize[3];
   
   sd += *( srcArray + (v.center - slArray ));
   sd += *( srcArray + (v.eNeighbors[0] - slArray ));
   sd += *( srcArray + (v.eNeighbors[1] - slArray ));
   sd += *( srcArray + (v.eNeighbors[2] - slArray ));
   sd += *( srcArray + (v.eNeighbors[3] - slArray ));
   
   int size;
   size = v.loopSize[ 0 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[0][i] - slArray ));
   size = v.loopSize[ 1 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[1][i] - slArray ));
   size = v.loopSize[ 2 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[2][i] - slArray ));
   size = v.loopSize[ 3 ];
   for (i = 0; i < size; i++)
     sd +=  *(srcArray + ( v.loops[3][i] - slArray ));
   
   sd /= scale;
   sdaDest[ v.center - slArray ] = sd;
   return sd;
}

inline bool significant( double d ) {
  return ( fabs( d ) > 1e-6 );
}

/**
 * Retourne le surfel correspondant � l'application
 * du masque -1 -2 0 2 1 differentiel dans deux directions.
 * @param f Une face.
 * @return Le surfel correspondant a l'application du 
 *         masque de convolution.
 */
void
convolutionDifferential( Surfel *surfel,
			 SurfelList & surfelList,
			 SurfelDataArray & destA,
			 SurfelDataArray & destB,
			 SurfelData & sda,
			 SurfelData & sdb )
{
   double diffNorm, normLeft, normMiddle, normRight;
   Surfel *slArray = surfelList.begin();
   
   // eeNeighbor( Surfel * surfel, int edge, int vertex );
   Surfel *after,  *before;
   Triple<double> diff[ 3 ];
   
   memset( diff, 0, 3 * sizeof( Triple<double> ) );   

   // Middle Direction A
   diff[ 1 ] = surfel->neighbors[ 1 ]->normal - surfel->neighbors[ 3 ]->normal;
   diff[ 1 ] /= distance( surfel->neighbors[ 1 ]->center, surfel->neighbors[ 3 ]->center ) ;
   normMiddle = norm( diff[ 1 ] );
   
   
   // Left Direction A
   after = eeNeighbor( surfel, 1, 2 );
   if ( !after ) after = surfel->neighbors[ 1 ];
   before = eeNeighbor( surfel, 3, 3 );
   if ( !before ) before = surfel->neighbors[ 3 ];
   diff[ 0 ] = after->normal - before->normal;
   diff[ 0 ] /= distance( after->center, before->center ) ;
   normLeft = norm( diff[ 0 ] );
   
   // Right Direction A
   after = eeNeighbor( surfel, 1, 1 );
   if ( !after ) after = surfel->neighbors[ 1 ];
   before = eeNeighbor( surfel, 3, 0 );
   if ( !before ) before = surfel->neighbors[ 3 ];
   diff[ 2 ] = after->normal - before->normal;
   diff[ 2 ] /= distance( after->center,  before->center ) ;
   normRight = norm( diff[ 2 ] );
   
   diffNorm = ( ( 2 * normMiddle ) + normLeft + normRight ) / 4.0f;
   if ( significant( diffNorm ) && significant( normMiddle ) ) 
     diff[ 1 ] *= ( diffNorm / normMiddle );
   destA[ surfel - slArray ] = sda = diff[ 1 ];

   memset( diff, 0, 3 * sizeof( SurfelData ) );
   // Middle Direction B
   diff[ 1 ] = surfel->neighbors[ 2 ]->normal - surfel->neighbors[ 0 ]->normal;
   diff[ 1 ] /= distance( surfel->neighbors[ 2 ]->center,  surfel->neighbors[ 0 ]->center ) ;
   normMiddle = norm( diff[ 1 ] );

   // Left Direction B
   after = eeNeighbor( surfel, 2, 3 );
   if ( !after ) after = surfel->neighbors[ 2 ];
   before = eeNeighbor( surfel, 0, 0 );
   if ( !before ) before = surfel->neighbors[ 0 ];
   diff[ 0 ] = after->normal - before->normal;
   diff[ 0 ] /= distance( after->center,  before->center ) ;
   normLeft = norm( diff[ 0 ] );
      
   // Right Direction B
   after = eeNeighbor( surfel, 2, 2 );
   if ( !after ) after = surfel->neighbors[ 2 ];
   before = eeNeighbor( surfel, 0, 1 );
   if ( !before ) before = surfel->neighbors[ 0 ];
   diff[ 2 ] = after->normal - before->normal;
   diff[ 2 ] /= distance( after->center, before->center ) ;
   normRight = norm( diff[ 2 ] );
   
   diffNorm = ( ( 2 * normMiddle ) + normLeft + normRight ) / 4.0f;
   if ( significant( diffNorm ) && significant( normMiddle ) ) 
     diff[ 1 ] *= ( diffNorm / normMiddle );
   destB[  surfel - slArray ] = sdb = diff[ 1 ];
}

void
computeCurvature( Surfel *ps, const Triple<double>[] /* normals */ )
{
  Triple<double> estimatedNormal;
  Triple<double> normal;
  
  Triple<double> dNdu = ( ps->neighbors[0]->normal - ps->normal );
  Triple<double> dNdv = ( ps->neighbors[1]->normal - ps->normal );
  Triple<double> dSdu = ( ps->neighbors[0]->center - ps->center );
  Triple<double> dSdv = ( ps->neighbors[1]->center - ps->center );
  
  double eg_f2 = (dNdu*dSdu)*(dNdv*dSdv) - dsquare(dNdu*dSdv);
  double EG_F2 = (dSdu*dSdu)*(dSdv*dSdv) - dsquare(dSdu*dSdv);
  double eG_2fF_gE = (dNdu*dSdu)*(dSdv*dSdv) - 2*(dNdu*dSdv)*(dSdu*dSdv) + (dNdv*dSdv)*(dSdu*dSdu); 

  ps->gaussianCurvature = eg_f2 / EG_F2;
  ps->meanCurvature = eG_2fF_gE / ( 2 * EG_F2 );  
}

void
colorizeCurvature( SurfelList & surfelList,
		   const SurfelDataArray & curvatures,
		   int curvatureType )
{
  SurfelDataArray::const_iterator curvaturesArray = curvatures.begin();
  Surfel *slArray = surfelList.begin();
  Surfel *surfel = slArray, *end = surfelList.end();

  double maxAbsGaussian = 0.0;
  double maxAbsMean = 0.0;
  double maxGaussian = -std::numeric_limits<double>::max();
  double maxMean = -std::numeric_limits<double>::max();
  double minGaussian = std::numeric_limits<double>::max();
  double minMean = std::numeric_limits<double>::max();

  while ( surfel != end ) {
    size_t delta = surfel - slArray;
    double mean = curvaturesArray[delta].first;
    double gaussian = curvaturesArray[delta].second;
    if ( fabs( gaussian ) > maxAbsGaussian ) maxAbsGaussian = fabs( gaussian );
    if ( gaussian > maxGaussian ) maxGaussian = gaussian;
    if ( gaussian < minGaussian ) minGaussian = gaussian;
    if ( fabs( mean ) > maxAbsMean ) maxAbsMean = fabs( mean );
    if ( mean  > maxMean ) maxMean = mean;
    if ( mean  < minMean ) minMean = mean;
    surfel++;
  }
  
  surfel = slArray;
  if ( curvatureType == MEAN_CURVATURE )  /* Mean Curvature */
    while ( surfel != end ) {
      size_t delta = surfel - slArray;
      double mean = curvaturesArray[ delta ].first;
      double x;
      if ( mean > 0 ) { 
	x = mean / maxMean;
	x = log( 1.0 + 127.0 * x ) / log( 128.0 );
      } else {
	x = mean / minMean;
	x = - log( 1.0 + 127.0 * x ) / log( 128.0 );	
      }
      surfel->color = static_cast<UChar>( 128 + 127 * x );
      surfel++;
    } else  /* Gaussian Curvature */
      while ( surfel != end ) {
	size_t delta = surfel - slArray;
	double gaussian = curvaturesArray[ delta ].second;
	double x;
	if ( gaussian > 0 ) { 
	  x = gaussian / maxGaussian;
	  x = log( 1.0 + 127.0 * x ) / log( 128.0 );
	} else {
	  x = gaussian / minGaussian;
	  x = - log( 1.0 + 127.0 * x ) / log( 128.0 );	
	}
	surfel->color = static_cast<UChar>( 128 + 127 * x );
	surfel++;
    }
}

void
colorizeCurvature( SurfelList & surfelList,
		   int curvatureType )
{
  Surfel *slArray = surfelList.begin();
  Surfel *end = surfelList.end();
  Surfel *surfel;
  
  double maxAbsGaussian = 0.0;
  double maxAbsMean = 0.0;
  double maxAbsMaxi = 0.0;
  double maxGaussian = -std::numeric_limits<double>::max();
  double minGaussian = std::numeric_limits<double>::max();
  double maxMean = -std::numeric_limits<double>::max();
  double minMean = std::numeric_limits<double>::max();

  surfel = slArray;
  while ( surfel != end ) {
    double mean = surfel->meanCurvature;
    double gaussian = surfel->gaussianCurvature;
    double maxi = mean + sqrt( dsquare(mean) - gaussian );
    maximize( maxAbsGaussian, fabs( gaussian ) );
    maximize( maxAbsMean, fabs( mean ) );
    maximize( maxAbsMaxi, fabs( maxi ) );
    maximize( maxGaussian, gaussian );
    minimize( minGaussian, gaussian );
    minimize( minMean, mean );
    maximize( maxMean, mean );
    ++surfel;
  }
  
  //  SHOW( maxAbsMean );
  //  SHOW( maxAbsGaussian );
  //  surfel = slArray;
  //  while ( surfel != end ) { 
  //    if ( fabs( surfel->gaussianCurvature ) == maxAbsGaussian )
  //      SHOW( surfel - slArray );
  //    ++surfel;
  //  }

  surfel = slArray;
  
  if ( curvatureType == MEAN_CURVATURE )
    while ( surfel != end ) {
      double mean = surfel->meanCurvature;
      double x;
      x = fabs( mean / maxAbsMean );
      // x = sign( mean ) * log( 1.0 + 127.0 * x ) / log( 128.0 );
      // surfel->color = static_cast<UChar>( 128 + 127 * x );
      surfel->color = static_cast<UChar>( 128 + 127 * sign( mean ) * x );
      ++surfel;
    } 

  if ( curvatureType == GAUSSIAN_CURVATURE )
    while ( surfel != end ) {
      double gaussian = surfel->gaussianCurvature;
      double x;
      x = fabs( gaussian / maxAbsGaussian );
      // x = sign( gaussian) * log( 1.0 + 127.0 * x ) / log( 128.0 );
      // surfel->color = static_cast<UChar>( 128 + 127 * x );
      surfel->color = static_cast<UChar>( 128 + 127 * sign( gaussian ) * x );
      ++surfel;
    }

  if ( curvatureType == MAXIMUM_CURVATURE )
    while ( surfel != end ) {
      double maxi = surfel->meanCurvature + sqrt( dsquare(surfel->meanCurvature) 
						  - surfel->gaussianCurvature );
      double x;
      x = fabs( maxi / maxAbsMaxi );
      // x = sign( maxi ) * log( 1.0 + 127.0 * x ) / log( 128.0 );
      // surfel->color = static_cast<UChar>( 128 + 127 * x );
      surfel->color = static_cast<UChar>( 128 + 127 * sign( maxi ) * x );
      ++surfel;
    }
}

SurfelData
convolutionNormals421( Surfel *surfel )
{
  double contributionEvoisin0;
  double contributionEvoisin1;
  double contributionEvoisin2;
  double contributionEvoisin3;
  double contributionLoop0 = 0;
  double contributionLoop1 = 0;
  double contributionLoop2 = 0;
  double contributionLoop3 = 0;
  VNeighborhood v( surfel );
  int i;
  Triple<double> result;

  result = 0.0f;
  contributionEvoisin0 = 2.0;
  contributionEvoisin1 = 2.0;
  contributionEvoisin2 = 2.0;
  contributionEvoisin3 = 2.0;

  if (v.loopSize[0] == 0) {
    contributionEvoisin0 += 0.5;
    contributionEvoisin3 += 0.5;
  }

  if (v.loopSize[1] == 0) {
    contributionEvoisin0 += 0.5;
    contributionEvoisin1 += 0.5;
  }

  if (v.loopSize[2] == 0) {
    contributionEvoisin1 += 0.5;
    contributionEvoisin2 += 0.5;
  }

  if (v.loopSize[3] == 0) {
    contributionEvoisin2 += 0.5;
    contributionEvoisin3 += 0.5;
  }

  if (v.loopSize[0])
    contributionLoop0 = 1.0 / v.loopSize[0];
  if (v.loopSize[1])
    contributionLoop1 = 1.0 / v.loopSize[1];
  if (v.loopSize[2])
    contributionLoop2 = 1.0 / v.loopSize[2];
  if (v.loopSize[3])
    contributionLoop3 = 1.0 / v.loopSize[3];

  result += v.center->normal * 4.0;
  result += v.eNeighbors[0]->normal * contributionEvoisin0;
  result += v.eNeighbors[1]->normal * contributionEvoisin1;
  result += v.eNeighbors[2]->normal * contributionEvoisin2;
  result += v.eNeighbors[3]->normal * contributionEvoisin3;

  for (i = 0; i < v.loopSize[0]; i++)
    result +=  v.loops[0][i]->normal  * contributionLoop0;
  for (i = 0; i < v.loopSize[1]; i++)
    result +=  v.loops[1][i]->normal * contributionLoop1;
  for (i = 0; i < v.loopSize[2]; i++)
    result +=  v.loops[2][i]->normal * contributionLoop2;
  for (i = 0; i < v.loopSize[3]; i++)
    result +=  v.loops[3][i]->normal * contributionLoop3;
  
  return result / 16.0;
}

Surfel *
eeNeighbor( Surfel * surfel, int edge, int vertex )
{
   Surfel *newSurfel;
   switch ( edge ) {
   case 0:
     newSurfel = surfel->neighbors[ 0 ];
     edge = edgeFrom( surfel->direction, 0, newSurfel->direction );
     if ( vertex == 0 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 1 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 3 ] ) return 0;
     } else if ( vertex == 1 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 3 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 1 ] ) return 0;
     } else {
       ERROR << "eeNeighbor(): should never happen.\n";
       return 0;
     }
     return newSurfel;
     break;
   case 1:
     newSurfel = surfel->neighbors[ 1 ];
     edge = edgeFrom( surfel->direction, 1, newSurfel->direction );
     if ( vertex == 1 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 1 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 0 ] ) return 0;
     } else if ( vertex == 2 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 3 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 2 ] ) return 0;
     } else {
       ERROR << "eeNeighbor(): should never happen.\n";
       return 0;
     }
     return newSurfel;
     break;
   case 2:
     newSurfel = surfel->neighbors[ 2 ];
     edge = edgeFrom( surfel->direction, 2, newSurfel->direction );
     if ( vertex == 2 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 1 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 1 ] ) return 0;
     } else if ( vertex == 3 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 3 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 3 ] ) return 0;
     } else {
       ERROR << "eeNeighbor(): should never happen.\n";
       return 0;
     }
     return newSurfel;
     break;
   case 3:
     newSurfel = surfel->neighbors[ 3 ];
     edge = edgeFrom( surfel->direction, 3, newSurfel->direction );
     if ( vertex == 3 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 1 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 2 ] ) return 0;
     } else if ( vertex == 0 ) {
       newSurfel = newSurfel->neighbors[ ( edge + 3 ) % 4 ];
       if ( newSurfel == surfel->neighbors[ 0 ] ) return 0;
     } else {
       ERROR << "eeNeighbor(): should never happen.\n";
       return 0;
     }
     return newSurfel;
     break;
   default:
     ERROR << "eeNeighbor(): should never happen.\n";
     return 0;
     break;
   }
}

VNeighborhood
computeVNeighborhood( Surfel *surfel )
{
   VNeighborhood result;
   int edge;
   Surfel *newSurfel, *prevSurfel, *stop;
   
   memset( &result, 0, sizeof(VNeighborhood) );

   result.center = surfel;
   result.eNeighbors = surfel->neighbors;

   newSurfel = surfel->neighbors[0];
   stop = surfel->neighbors[1];
   edge = edgeFrom( surfel->direction, 0, newSurfel->direction );
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4( edge ) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4( edge ),
		      newSurfel->direction );
     if ( newSurfel != stop ) {
       result.loops[1][ result.loopSize[1] ] = newSurfel;
       result.loopSize[1]++;
     }   
   } while ( newSurfel != stop );

   newSurfel = surfel->neighbors[1];
   stop = surfel->neighbors[2];
   edge = edgeFrom( surfel->direction, 1, newSurfel->direction );
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       result.loops[2][ result.loopSize[2] ] = newSurfel;
       result.loopSize[2]++;
     }      
   } while ( newSurfel != stop );
   
   newSurfel = surfel->neighbors[2];
   stop =  surfel->neighbors[3];
   edge = edgeFrom( surfel->direction, 2, newSurfel->direction);   
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       result.loops[3][ result.loopSize[3] ] = newSurfel;
       result.loopSize[3]++;
     }      
   } while ( newSurfel != stop );
   
   newSurfel = surfel->neighbors[3];
   stop =  surfel->neighbors[0];
   edge = edgeFrom( surfel->direction, 3, newSurfel->direction);
   do {
     prevSurfel = newSurfel;
     newSurfel = prevSurfel->neighbors[ minusOneMod4(edge) ];
     edge = edgeFrom( prevSurfel->direction,
		      minusOneMod4(edge),
		      newSurfel->direction);
     if ( newSurfel != stop ) {
       result.loops[0][ result.loopSize[0] ] = newSurfel;
       result.loopSize[0]++;
     }      
   } while ( newSurfel != stop );
   
   return result;
}

std::vector<Surfel*>
vNeighborhood( Surfel * surfel )
{
  std::vector<Surfel*> result;
  int edge;
  Surfel *newSurfel, *prevSurfel, *stop;
  
  for ( int l = 0; l < 4; ++l ) {
    result.push_back( surfel->neighbors[l] );
    newSurfel = surfel->neighbors[l];
    stop = surfel->neighbors[ (l+1)%4 ];
    edge = edgeFrom( surfel->direction, l,
		     surfel->neighbors[l]->direction );
    do {
      prevSurfel = newSurfel;
      newSurfel = prevSurfel->neighbors[ minusOneMod4( edge ) ];
      edge = edgeFrom( prevSurfel->direction,
		       minusOneMod4( edge ),
		       newSurfel->direction );
      if ( newSurfel != stop )
	result.push_back( newSurfel );
    } while ( newSurfel != stop );
  }

  return result;
}
