/**
 * @file   Vertex.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:37:47 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "Vertex.h"

#include <cstring>
#include <cstdlib>

HashVertices::HashVertices( Size size ) {
  Size c = (size * 3) / 2;
  if ( size ) {
    _count = 0;
    _array = new Vertex[ c ];
    _limit = _array + c;
    memset( _array, 0, c * sizeof( Vertex ) );
  } else _limit = _array = 0;
}

HashVertices::~HashVertices() { 
  disposeArray( _array );
}

Size
HashVertices::key( const Triple<double> & v )
{
  Size c = _limit - _array;
  Size result = double2Size( v.first )
      * double2Size( v.second )
      * double2Size( v.third );
  result %= c;
  return result;
}

Vertex *
HashVertices::add( Triple<double> position )
{
  Vertex *pv;
  Vertex v;
  v.position = position;
  v.valid = true;
  pv = _array + key( position );
  while ( pv->valid ) {
    if ( *pv == v ) return pv;
    pv++;
    if ( pv == _limit ) pv = _array;
  }

  *pv = v;
  _count++;
  pv->index = _count;

  if ( _count == static_cast<Size>( _limit - _array ) ) {
    ERROR << "HashVertices::add(): array full.";
    exit(-1);
  }
  return pv;
}

Vertex *
HashVertices::find(Vertex v)
{
  Vertex *pv = _array + key( v.position );
  while ( pv->valid ) {
    if ( *pv == v ) return pv;
    pv++;
    if ( pv == _limit) pv = _array;
  }
  return 0;
}

Vertex *
HashVertices::find( Triple<double> position )
{
  Vertex *pv = _array + key( position );
  Vertex v;
  v.position = position;
  while ( pv->valid ) {
    if ( *pv == v ) return pv;
    pv++;
    if ( pv == _limit) pv = _array;
  }
  return 0;
}

int
vertexCompare( const Vertex *v1, const Vertex *v2) 
{
  if ( v1->valid && !(v2->valid) ) return 1;
  if (!(v1->valid) && v2->valid) return -1;
  if (!(v1->valid) && !(v2->valid)) return 0;
  return v1->index - v2->index;
}

std::ostream &
HashVertices::dump( std::ostream & out )
{
  Vertex *array;
  Vertex *limit;

  Size size = _limit - _array;
  typedef int (*test)(const void *, const void *);

  array = new Vertex[ size ];
  limit = array + size;
  memcpy( array, _array, sizeof(Vertex) * size );

  qsort( array,
         size,
         sizeof(Vertex),
         reinterpret_cast<test>( & vertexCompare ));

  Vertex *pv = array;
  while ( pv < limit ) {
    if (pv->valid)
      out << "v " << pv->position.first
          << " " << pv->position.second
          << " " << pv->position.third << "\n";
    pv++;
  }
  disposeArray( array );
  return out;
}

std::ostream &
HashVertices::dumpOFF( std::ostream & out )
{
  Vertex *array;
  Vertex *limit;
  Size size = _limit - _array;
  typedef int (*test)(const void *, const void *);

  array = new Vertex[ size ];
  limit = array + size;
  memcpy( array, _array, sizeof(Vertex) * size );

  qsort( array,
         size,
         sizeof(Vertex),
         reinterpret_cast<test>( & vertexCompare ));

  Vertex *pv = array;
  while ( pv < limit ) {
    if (pv->valid)
      out << pv->position.first
          << " " << pv->position.second
          << " " << pv->position.third << "\n";
    pv++;
  }
  disposeArray( array );
  return out;
}


double
HashVertices::double3Decimals( double x )
{
  int one, two, three;
  int f;
  f = static_cast<int>( floor(x) );
  one = static_cast<int>( floor( (x - f) * 10 ) );
  f = static_cast<int>( floor( x * 10) );
  two = static_cast<int>( floor( ((x * 10) - f) * 10 ) );
  f = static_cast<int>( floor( x * 100) );
  three = static_cast<int>( floor( (( x * 100) - f) * 10) );
  f = static_cast<int>( floor(x) );
  return f + one/10.0 + two/100.0 + three/1000.0;
}

Size
HashVertices::double2Size(double x)
{
  int one, two, three;
  int f;
  x = fabs( x );
  f = static_cast<int>( floor( x ) );
  one = static_cast<int>( floor( (x - f) * 10 ) );
  f = static_cast<int>( floor( x * 10 ) );
  two = static_cast<int>( floor( (( x * 10 ) - f) * 10 ) );
  f = static_cast<int>( floor( x * 100) );
  three = static_cast<int>( floor( (( x * 100) - f) * 10) );
  f = static_cast<int>( floor(x) );
  return (int) f + one * 10 + two * 100 + three * 1000;
}


