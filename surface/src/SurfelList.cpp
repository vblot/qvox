/**
 * @file   SurfelList.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Mar 27 13:38:29 2006
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#include "SurfelList.h"
#include "EdgelList.h"
#include <Triple.h>
#include <Array.h>
#include "TimeMeasure.h"
#include "Settings.h"
#include "SurfelListRotateThread.h"
#include <QThreadPool>
using std::ostream;
using std::vector;

SurfelList::SurfelList(Size size )
{
  CONS( SurfelList );
  _bbMin = std::numeric_limits<double>::max();
  _bbMax = -std::numeric_limits<double>::max();
  _centerTranslation = 0;
  _bbValid = false;
  _sortedArray = 0;
  _array.resize( size );
}

SurfelList::~SurfelList()
{
  delete[] _sortedArray;
}

void
SurfelList::free()
{
  _array.clear();
  disposeArray( _sortedArray );
}

void
SurfelList::clear()
{
  _array.clear();
  _bbMin = std::numeric_limits<double>::max();
  _bbMax = -std::numeric_limits<double>::max();
  _bbValid = false;
  _centerTranslation = 0;
}

void
SurfelList::computeBBox( bool force, SurfelList::Position position )
{
  if ( !force && _bbValid )
    return;
  Surfel *ps = begin();
  Surfel * pend = end();
  switch ( position ) {
  case RotatedPosition:
    _bbMin = std::numeric_limits<double>::max();
    _bbMax = -std::numeric_limits<double>::max();
    while ( ps < pend ) {
      lower( _bbMin, ps->rotatedCenter );
      raise( _bbMax, ps->rotatedCenter );
      ++ps;
    }
    _bbValid = true;
    break;
  case OriginalPosition:
    _bbMinOriginal = std::numeric_limits<double>::max();
    _bbMaxOriginal = -std::numeric_limits<double>::max();
    while ( ps < pend ) {
      lower( _bbMinOriginal, ps->center );
      raise( _bbMaxOriginal, ps->center );
      ++ps;
    }
    break;
  }
}

void
SurfelList::resize( Size size )
{
  if ( size )
    _array.resize( size );
  else
    _array.clear();
  _bbMin = std::numeric_limits<double>::max();
  _bbMax = -std::numeric_limits<double>::max();
  _centerTranslation = 0;
  _bbValid = false;
}

void
SurfelList::translate( Triple<double> delta )
{
  TRACE << "SurfelList::translate(): BEGIN\n";
  Surfel *ps = begin();
  Surfel *pend = end();
  while (ps < pend) {
    ps->rotatedCenter += delta;
    ps->center += delta;
    ++ps;
  }
  _bbMin += delta;
  _bbMax += delta;
  _centerTranslation += delta;
  TRACE << "SurfelList::translate(): END\n";
}

void
SurfelList::reCenter()
{
  translate( - _centerTranslation );
}

void
SurfelList::colorize( EdgelList & edgelList )
{
  Edgel * pe = edgelList.array();
  Edgel * end = edgelList.end();
  Surfel * ps;
  while ( pe < end ) {
    ps = quickFind( Triple<SSize>( pe->xp, pe->yp, 0 ), pe->direction );
    ps->color = pe->color;
    ++pe;
  }
}

void
SurfelList::buildSortedArray()
{
  TRACE << "buildSortedArray(): BEGIN\n";
  TIME_MEASURE_START("buildSortedArray()");
  Surfel *ps = begin();
  Surfel *pend = end();
  Surfel **pps = 0;
  disposeArray( _sortedArray );
  if ( ! _array.size() ) return;
  _sortedArray = new Surfel* [ _array.size() ];
  pps = _sortedArray;
  while ( ps < pend )
    *(pps++) = ps++;
  qsort( _sortedArray, _array.size(), sizeof(Surfel*),
         ( int (*)(const void*, const void*)) & surfelCompare);
  TIME_MEASURE_STOP("buildSortedArray()");
  TRACE << "buildSortedArray(): END\n";
}

Surfel*
SurfelList::find( Triple<SSize> voxel, int direction )
{
  Surfel *ps = begin();
  Surfel *pend = end();
  while ( ps < pend ) {
    if ( ps->voxel == voxel && ps->direction == direction)
      return ps;
    ++ps;
  }
  return 0;
}

Surfel*
SurfelList::quickFind( const Face & face )
{
  Surfel s;
  Surfel *ps = &s;
  s.voxel.set( face.x, face.y, face.z );
  s.direction = face.direction;

  Surfel **left = _sortedArray;
  Surfel **right = _sortedArray + _array.size() - 1;
  Surfel **middle;
  while ( left <= right ) {
    middle =  left + ( right - left ) / 2;
    switch ( surfelCompare( middle, &ps ) ) {
    case 0: return *middle; break;
    case -1: left = middle + 1; break;
    case 1: right = middle - 1; break;
    default:
      throw "Strange things happened with surfelCompare()!";
      break;
    }
  }
  ERROR << "SurfelList::quickFind(SSize,SSize,SSize,int): "
        <<" Dichotomy error: "
       << s.voxel << s.direction << "\n";
  throw "Strange things happened!";
  return 0;
}

Surfel*
SurfelList::quickFind(Triple<SSize> voxel, int direction )
{
  Surfel s;
  Surfel *ps = &s;
  Surfel **left = _sortedArray;
  Surfel **right = _sortedArray + _array.size() - 1;
  Surfel **middle;
  s.voxel = voxel; //s.xv = x; s.yv = y; s.zv = z;
  s.direction = direction;
  while ( left <= right ) {
    middle =  left + ( right - left ) / 2;
    switch ( surfelCompare( middle, &ps ) ) {
    case 0: return *middle; break;
    case -1: left = middle + 1; break;
    case 1: right = middle - 1; break;
    default:
      throw "Strange things happened with surfelCompare()!";
      break;
    }
  }
  ERROR << "SurfelList::quickFind(SSize,SSize,SSize,int): "
        << "Dichotomy error: "
        << voxel << direction << "\n";
  throw "Strange things happened!";
  return 0;
}

Surfel*
SurfelList::quickFind( Surfel & surfel )
{
  Surfel *ps = &surfel;
  Surfel **left = _sortedArray;
  Surfel **right = _sortedArray + _array.size() - 1;
  Surfel **middle;

  while ( left <= right ) {
    middle =  left + ( right - left ) / 2;
    switch ( surfelCompare( middle, &ps ) ) {
    case 0: return *middle; break;
    case -1: left = middle + 1; break;
    case 1: right = middle - 1; break;
    default:
      throw "Strange things happened with surfelCompare()!";
      break;
    }
  }
  ERROR << "SurfelList::quickFindFace(Surfel&): "
        << "Dichotomy error: "
        << surfel.voxel << surfel.direction << "\n";
  throw "Strange things happened!";
  return 0;
}

void
SurfelList::setColors( UChar color)
{
  Surfel *ps = begin();
  Surfel *pend = end();
  while ( ps < pend )
    (ps++)->color = color;
}

void
SurfelList::setTransparency( UChar color )
{
  Surfel *ps = begin();
  Surfel *pend = end();
  while ( ps < pend ) {
    if ( ps->color == color ) ps->color = 0;
    ++ps;
  }
}

void
SurfelList::unsetTransparency( UChar color )
{
  Surfel *ps = begin();
  Surfel *pend = end();
  while ( ps < pend ) {
    if ( ! ps->color ) ps->color = color;
    ++ps;
  }
}

void
SurfelList::rotate( const Matrix & rotation )
{
  Surfel *ps = begin();
  Surfel *pend = end();
  while ( ps != pend ) {
    ps->rotatedCenter =  rotation * ps->center;
    ++ps;
  }
  _bbValid = false;
}


void
SurfelList::buildGraph( ZZZImage<UChar> * image,
                        bool fullBox,
                        int volumeAdjacency )
{
  if ( !image ) return;
  TRACE << "SurfelList::buildGraph() BEGIN:";
  TRACE << size() << " surfels.\n";
  int directionA[6] = { 1, 1, 1, 1, 2, 2 };
  int directionB[6] = { 2, 0, 0, 2, 3, 1 };

  // (6,18) an (18,6) does not matter here.
  int oppositeDirectionA[6][6] =
  { { 3, 0, 3, 3, 0, 0 }, { 0, 3, 3, 3, 0, 0 },
    { 3, 3, 3, 0, 0, 0 }, { 3, 3, 0, 3, 0, 0 },
    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 2, 0, 0, 0 } };
  int oppositeDirectionB[6][6] =
  { { 0, 0, 0, 0, 1, 3 }, { 0, 2, 0, 0, 1, 3 },
    { 0, 0, 2, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
    { 0, 2, 0, 0, 1, 0 }, { 0, 2, 0, 0, 0, 3 } };

  Face f, fs;
  Surfel *ps = begin();
  Surfel *pend = end();
  Surfel *psv=0;
  if ( fullBox ) {
    Face fa, fb;
    SSize width, height, depth, bands;
    image->getDimension( width, height, depth, bands );
    --width;
    --height;
    --depth;
    while ( ps < pend ) {
      fs = Face( ps->voxel, ps->direction );
      fa = fb = fs;
      const Triple<SSize> & voxel = ps->voxel;
      switch ( ps->direction ) {
      case 0:
        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
        break;
      case 1:
        if ( ! voxel.second ) fa.direction = 3; else fa.y -= 1;
        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
        break;
      case 2:
        if ( ! voxel.first ) fa.direction = 1; else fa.x -= 1;
        if ( ! voxel.third ) fb.direction = 5; else fb.z -= 1;
        break;
      case 3:
        if ( voxel.first == width ) fa.direction = 0; else fa.x += 1;
        if ( voxel.third == depth ) fb.direction = 4; else fb.z += 1;
        break;
      case 4:
        if ( voxel.second == height ) fa.direction = 2; else fa.y += 1;
        if ( voxel.first == 0 ) fb.direction = 1; else fb.x -= 1;
        break;
      case 5:
        if ( voxel.second == 0 ) fa.direction = 3; else fa.y -= 1;
        if ( voxel.first == width ) fb.direction = 0; else fb.x += 1;
        break;
      }
      psv = quickFind( fa );
      ps->neighbors[ directionA[ ps->direction ] ] = psv;
      psv->neighbors[ oppositeDirectionA[ ps->direction ][ psv->direction ] ] = ps;
      psv = quickFind( fb );
      ps->neighbors[ directionB[ ps->direction ] ] = psv;
      psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ] = ps;
      ++ps;
    }
  } else
    if ( image->bands() == 1 ) {
      if ( volumeAdjacency == ADJ6 )
        while ( ps < pend ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighbor_6_18( image,
                                     fs,
                                     directionA[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighbor_6_18( image,
                                     fs,
                                     directionB[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        } else /* ADJ18 */
        while ( ps < pend ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighbor_18_6( image,
                                     fs,
                                     directionA[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighbor_18_6( image,
                                     fs,
                                     directionB[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        }
    } else {
      if ( volumeAdjacency == ADJ6 )
        while ( ps < pend ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighborColor_6_18( image,
                                          fs,
                                          directionA[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighborColor_6_18( image,
                                          fs,
                                          directionB[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        } else
        while ( ps < pend ) {
          fs = Face( ps->voxel, ps->direction );
          f = faceFindNeighborColor_18_6( image,
                                          fs,
                                          directionA[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionA[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionA[ps->direction][psv->direction] ]
              = ps;

          f = faceFindNeighborColor_18_6( image,
                                          fs,
                                          directionB[ ps->direction ] );
          psv = quickFind( f );
          ps->neighbors[ directionB[ ps->direction ] ] = psv;
          psv->neighbors[ oppositeDirectionB[ps->direction][psv->direction] ]
              = ps;
          ++ps;
        }
    }
  TRACE << "SurfelList::buildGraph() END\n";
}

/**
 * Retourne un surfel correspondant a une face.
 * @param sl Une liste de surfels.
 * @param f Une face.
 * @return Le surfel correspondant a la face.
 */
Surfel
SurfelList::faceToSurfel( Face f )
{
  Surfel result;
  double shifts[6][3] = { {  1,  0,  0 },
                          { -1,  0,  0 },
                          {  0,  1,  0 },
                          {  0, -1,  0 },
                          {  0,  0,  1 },
                          {  0,  0, -1 } };
  result.direction = f.direction;
  result.center = Triple<double>( shifts[ f.direction ] ) * 0.5;
  result.center += _centerTranslation + Triple<double>( f.x, f.y, f.z );
  return result;
}

void
SurfelList::fillVolumeFromSurfelColor( ZZZImage<UChar> & volume,
                                       UChar /* color */ )
{
  Surfel *ps = begin();
  Surfel *pend = end();
  UChar ***matrix = volume.data( 0 );
  while ( ps != pend ) {
    if ( ps->voxel.third == 0 && ps->direction == 5  )
      matrix[ 0 ][ ps->voxel.second ][ ps->voxel.first ]  = 1;
    ++ps;
  }
}

ostream &
operator<<( ostream & out, SurfelList & surfelList)
{
  Surfel *ps = surfelList.begin();
  Surfel *pend = surfelList.end();
  if ( ps ) out << *ps++;
  while ( ps != pend )
    out << "," << *(ps++);
  return out;
}

/*
 * Definition of the SortedSurfelList methods.
 */

SortedSurfelList::SortedSurfelList(Size size )
{
  if ( !size ) {
    _array = _end = _limit = 0;
  } else {
    _array = new Surfel*[ size ];
    _end = _array;
    _limit = _array + size;
  }
}

SortedSurfelList::~SortedSurfelList()
{
  delete[] _array ;
}

void
SortedSurfelList::resize( Size size )
{
  disposeArray( _array );
  if ( size ) {
    _array = new Surfel*[ size ];
    _end = _array;
  } else _array = _end = 0;
  _limit = _array + size;
}

void
SortedSurfelList::buildFromSurfelList( SurfelList & sl, bool visible[6], bool reversed )
{
  Surfel *ps, *end;
  resize( sl.size() / 2 );
  ps = sl.begin();
  end = sl.end();
  while ( ps < end ) {
    if ( visible[ ps->direction ] )  *(_end++) = ps;
    ++ps;
  }
  sort( reversed );
}

void
SortedSurfelList::buildFromSurfelListLinear( SurfelList & sl, bool visible[6], Size surfelsPerLayer, bool reversed )
{
  static vector< vector<Surfel*> > _layers(0);
  static Size _layersCount = 0;
  if ( ! sl.size() ) {
    resize( 0 );
    return;
  }
  bool shouldClean = ! (_layers.empty() );
  double zMin = sl.bbMin().third;
  double zMax = sl.bbMax().third;
  double deltaZ = zMax - zMin;
  _layersCount = static_cast<Size>( ceilf( sqrt(3.0) * deltaZ ) )  ;

  if ( _layers.empty() ) {
    _layers.resize( _layersCount );
    for ( Size layer = 0; layer < _layersCount; ++layer )
      _layers[layer].reserve( surfelsPerLayer );
  } else {
    for ( Size layer = 0; layer < _layersCount; ++layer ) {
      _layers[layer].clear();
      _layers[layer].reserve( surfelsPerLayer );
    }
  }

  Size layer;
  Surfel * ps = sl.begin();
  Surfel * pend = sl.end();
  if ( reversed )
    while ( ps < pend ) {
      if ( ! visible[ ps->direction ] ) {
        layer = static_cast<Size>( ( ( ps->rotatedCenter.third - zMin )
                                     / deltaZ ) * _layersCount );
        if ( layer == _layersCount ) --layer;
        _layers[ layer ].push_back( ps );
      }
      ++ps;
    }
  else
    while ( ps < pend ) {
      if ( visible[ ps->direction ] ) {
        layer = static_cast<Size>( ( ( zMax - ps->rotatedCenter.third )
                                     / deltaZ ) * _layersCount );
        if ( layer == _layersCount ) --layer;
        _layers[ layer ].push_back( ps );
      }
      ++ps;
    }

  resize( sl.size() / 2 );
  Size s;
  for ( layer = 0; layer < _layersCount; ++layer ) {
    s = _layers[ layer ].size();
    memcpy( _end, &(*_layers[ layer ].begin()),
            sizeof(Surfel*) * s );
    _end += s;
  }

  if ( shouldClean ) {
    _layers.clear();
  }
}

void
SortedSurfelList::inversedCopy( SortedSurfelList & other )
{
  resize( other._limit - other._array );
  Surfel ** pps = other._limit - 1;
  while ( _end != _limit ) {
    *(_end++) = *(pps--);
  }
}

void
SortedSurfelList::sort( bool reversed )
{
  typedef int (*compare)( const void*, const void*);
  if ( reversed )
    qsort( _array, size(), sizeof(pSurfel),
           reinterpret_cast<compare>( &pSurfelZCompareReverse ) );
  else
    qsort( _array, size(), sizeof(pSurfel),
           reinterpret_cast<compare>( &pSurfelZCompare ) );
}

void
SortedSurfelList::rotate(const Matrix & rotation, const Triple<double> & aspect )
{
  Surfel **pps = _array;
  Surfel *ps;

  int threadCount= ( globalSettings->maxThreads() == -1 ) ? QThreadPool::globalInstance()->maxThreadCount() : globalSettings->maxThreads();
  if ( globalSettings->multiThreading() ) {
    TIME_MEASURE_START("ROTATE_MT");
    const unsigned long count = _end - _array;
    const unsigned long interval = count / threadCount;
    for ( int t = 0; t < threadCount - 1; ++t ) {
      Surfel ** tStart = _array + t*interval;
      Surfel ** tStop = tStart + interval;
      SurfelListRotateThead * thread = new SurfelListRotateThead(tStart,tStop,rotation,aspect);
      QThreadPool::globalInstance()->start(thread);
    }
    Surfel ** tStart = _array + (threadCount-1)*interval;
    SurfelListRotateThead * thread = new SurfelListRotateThead(tStart,_end,rotation,aspect);
    QThreadPool::globalInstance()->start(thread);
    QThreadPool::globalInstance()->waitForDone();
    TIME_MEASURE_STOP("ROTATE_MT");
  } else {
    TIME_MEASURE_START("ROTATE");
    if ( aspect.first < 0  ) {
      while ( pps < _end ) {
        ps = *pps;
        ps->rotatedCenter =  rotation * ps->center;
        ++pps;
      }
    } else {
      const double ax = aspect.first;
      const double ay = aspect.second;
      const double az = aspect.third;
      Triple<double> point;
      while ( pps < _end ) {
        ps = *pps;
        point.first = ax * ps->center.first;
        point.second = ay * ps->center.second;
        point.third = az * ps->center.third;
        ps->rotatedCenter = rotation * point;
        ++pps;
      }
    }
    TIME_MEASURE_STOP("ROTATE");
  }
}
