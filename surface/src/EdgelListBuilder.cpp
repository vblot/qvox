/** -*- mode: c++ -*-
 * @file   EgdelListBuilder.cpp
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 *
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "EdgelListBuilder.h"

EdgelListBuilder::EdgelListBuilder( EdgelList & edgelList,
                                    Double min,
                                    bool update )
   : _edgelList( edgelList ),
     _min(min),
     _update( update )
{

}

template<typename T>
void EdgelListBuilder::next( T ***matrix, Edgel * pe, SSize & x, SSize & y, Edgel::Direction & direction )
{
   x = pe->xp;
   y = pe->yp;
   direction = pe->direction;
   switch( direction ) {
   case Edgel::XPlusDirection:
      if ( matrix[0][ y + 1 ][ x + 1 ] ) {
         ++y; ++x; direction = Edgel::YMinusDirection;
      } else if ( matrix[ 0 ][ y + 1 ][ x ] ) ++y;
      else direction = Edgel::YPlusDirection;
      break;
   case Edgel::XMinusDirection:
      if ( matrix[0][ y - 1 ][ x - 1 ] ) {
         --y; --x; direction = Edgel::YPlusDirection;
      } else if ( matrix[ 0 ][ y - 1 ][ x ] ) --y;
      else direction = Edgel::YMinusDirection;
      break;
   case Edgel::YPlusDirection:
      if ( matrix[0][ y + 1 ][ x - 1 ] ) {
         --x; ++y; direction = Edgel::XPlusDirection;
      } else if ( matrix[ 0 ][ y ][ x - 1 ] ) --x;
      else direction = Edgel::XMinusDirection;
      break;
   case Edgel::YMinusDirection:
      if ( matrix[0][ y - 1 ][ x + 1 ] ) {
         ++x; --y; direction = Edgel::XMinusDirection;
      } else if ( matrix[ 0 ][ y ][ x + 1 ] ) ++x;
      else direction = Edgel::XPlusDirection;
      break;
   default:
      ERROR << " EdgelList: next(" << direction << ") should never happen!\n";
      exit( -1 );
      break;
   }
}

template<typename T>
void EdgelListBuilder::next(T ***matrix0, T ***matrix1, T ***matrix2,
                             Edgel * pe,
                             SSize &x, SSize &y, Edgel::Direction & direction )
{
   x = pe->xp;
   y = pe->yp;
   direction = pe->direction;

   switch( direction ) {
   case Edgel::XPlusDirection:
      if ( matrix0[0][ y + 1 ][ x + 1 ]
           || matrix1[0][ y + 1 ][ x + 1 ]
           || matrix2[0][ y + 1 ][ x + 1 ] ) {
         ++y; ++x; direction = Edgel::YMinusDirection;
      } else if ( matrix0[ 0 ][ y + 1 ][ x ]
                  || matrix1[ 0 ][ y + 1 ][ x ]
                  || matrix2[ 0 ][ y + 1 ][ x ] )  ++y;
      else direction = Edgel::YPlusDirection;
      break;
   case Edgel::XMinusDirection:
      if ( matrix0[0][ y - 1 ][ x - 1 ]
           || matrix1[0][ y - 1 ][ x - 1 ]
           || matrix2[0][ y - 1 ][ x - 1 ] ) {
         --y; --x; direction = Edgel::YPlusDirection;
      } else if ( matrix0[ 0 ][ y - 1 ][ x ]
                  || matrix1[ 0 ][ y - 1 ][ x ]
                  || matrix2[ 0 ][ y - 1 ][ x ] ) --y;
      else direction = Edgel::YMinusDirection;
      break;
   case Edgel::YPlusDirection:
      if ( matrix0[0][ y + 1 ][ x - 1 ]
           || matrix1[0][ y + 1 ][ x - 1 ]
           || matrix2[0][ y + 1 ][ x - 1 ] ) {
         --x; ++y; direction = Edgel::XPlusDirection;
      } else if ( matrix0[ 0 ][ y ][ x - 1 ]
                  || matrix1[ 0 ][ y ][ x - 1 ]
                  || matrix2[ 0 ][ y ][ x - 1 ] ) --x;
      else direction = Edgel::XMinusDirection;
      break;
   case Edgel::YMinusDirection:
      if ( matrix0[0][ y - 1 ][ x + 1 ]
           || matrix1[0][ y - 1 ][ x + 1 ]
           || matrix2[0][ y - 1 ][ x + 1 ] ) {
         ++x; --y; direction = Edgel::XMinusDirection;
      } else if ( matrix0[ 0 ][ y ][ x + 1 ]
                  || matrix1[ 0 ][ y ][ x + 1 ]
                  || matrix2[ 0 ][ y ][ x + 1 ] ) ++x;
      else direction = Edgel::XPlusDirection;
      break;
   default:
      ERROR << "EdgleList next(" << direction << ") should never happen!\n";
      exit( -1 );
      break;
   }
}


template<typename T>
void
EdgelListBuilder::buildGraph( ZZZImage<T> & image )
{
   Edgel * ps = _edgelList.array();
   Edgel * end = _edgelList.end();
   Edgel *psv=0;
   SSize x,y;
   Edgel::Direction  direction;
   if ( image.bands() == 1 ) {
      T ***matrix = image.data( 0 );
      while ( ps < end ) {
         next( matrix, ps, x, y, direction );
         psv = _edgelList.quickFind( x, y, direction );
         ps->next = psv;
         psv->previous = ps;
         ++ps;
      }
   } else {
      T ***matrix0 = image.data( 0 );
      T ***matrix1 = image.data( 1 );
      T ***matrix2 = image.data( 2 );
      while ( ps < end ) {
         next( matrix0, matrix1, matrix2, ps, x, y, direction );
         psv = _edgelList.quickFind( x, y, direction );
         ps->next = psv;
         psv->previous = ps;
         ++ps;
      }
   }
}

template<typename T>
void EdgelListBuilder::buildFrom( ZZZImage<T> & volume )
{

   SSize x = 0, y = 0;
   unsigned long size = 0;
   size_t inc_dy = volume.width() + 2;
   SSize width = volume.width();
   SSize height = volume.height();

   int shift_dy = 1;
   if ( ! volume.zeroFramed() ) {
      shift_dy = 0;
      inc_dy -= 2;
   }
   _edgelList.clear();
   size = std::max(  width, height ) * 4;
   _edgelList.resize( size );
   size = 0;
   if ( volume.bands() == 1 ) {
      T ***matrix = volume.data( 0 );
      T *vEE = matrix[0][0];
      T *vME = vEE - 1;
      T *vEM = vEE - inc_dy;
      y = x = 0;
      while ( y <= height ) {
         if ( ( *vEE >= _min ) ) {
            if ( *vME < _min ) { ++size; _edgelList.addPixel( x, y, Edgel::XMinusDirection, *vEE ); }
            if ( *vEM < _min ) { ++size; _edgelList.addPixel( x, y, Edgel::YMinusDirection, *vEE ); }
         } else {
            if ( *vME >= _min ) { ++size; _edgelList.addPixel( x - 1, y, Edgel::XPlusDirection, *vME ); }
            if ( *vEM >= _min ) { ++size; _edgelList.addPixel( x, y - 1, Edgel::YPlusDirection, *vEM ); }
         }
         ++x; ++vEE; ++vME; ++vEM;
         if ( x > width ) {
            ++y; x = 0;
            vEE += shift_dy; vME += shift_dy; vEM += shift_dy;
         }
      }
   } else {
      T ***matrix0 = volume.data( 0 );
      T ***matrix1 = volume.data( 1 );
      T ***matrix2 = volume.data( 2 );
      T *vEE0 = matrix0[0][0];
      T *vEE1 = matrix1[0][0];
      T *vEE2 = matrix2[0][0];
      T *vME0 = vEE0 - 1;
      T *vEM0 = vEE0 - inc_dy;
      T *vME1 = vEE1 - 1;
      T *vEM1 = vEE1 - inc_dy;
      T *vME2 = vEE2 - 1;
      T *vEM2 = vEE2 - inc_dy;
      y = x = 0;
      while ( y <= height ) {
         if ( ( *vEE0 >= _min ) || ( *vEE1 >= _min ) || ( *vEE2 >= _min ) ) {
            if ( *vME0 < _min && *vME1 < _min && *vME2 < _min ) {
               ++size;
               _edgelList.addPixel( x, y, Edgel::XMinusDirection, 1 );
            }
            if ( *vEM0 < _min && *vEM1 < _min && *vEM2 < _min ) {
               ++size;
               _edgelList.addPixel( x, y, Edgel::Edgel::YMinusDirection, 1 );
            }
         } else {
            if ( *vME0 >= _min || *vME1 >= _min || *vME2 >= _min ) {
               ++size;
               _edgelList.addPixel( x - 1, y, Edgel::Edgel::XPlusDirection, 1 );
            }
            if ( *vEM0 >= _min || *vEM1 >= _min || *vEM2 >= _min ) {
               ++size;
               _edgelList.addPixel( x, y - 1, Edgel::Edgel::YPlusDirection, 1 );
            }
         }
         ++x;
         ++vEE0; ++vME0; ++vEM0;
         ++vEE1; ++vME1; ++vEM1;
         ++vEE2; ++vME2; ++vEM2;
         if ( x > width ) {
            ++y; x = 0;
            vEE0 += shift_dy; vME0 += shift_dy; vEM0 += shift_dy;
            vEE1 += shift_dy; vME1 += shift_dy; vEM1 += shift_dy;
            vEE2 += shift_dy; vME2 += shift_dy; vEM2 += shift_dy;
         }
      }
   }
   _edgelList.buildSortedArray();
   buildGraph( volume );
}


template<typename T>
void EdgelListBuilder::updateFrom( ZZZImage<T> & volume )
{
   static ZZZImage<UChar> visited;
   size_t inc_dy = volume.width() + 2;
   if ( ! volume.zeroFramed() ) {
      inc_dy -= 2;
   }
   unsigned long size = _edgelList.end() - _edgelList.array();
   Edgel * prevArray;
   prevArray = new Edgel[ size ];
   memcpy( prevArray, _edgelList.array(), size * sizeof( Edgel ) );
   Edgel * edgel = prevArray;
   Edgel * end = prevArray + size;
   const int delta_x[4] = { +1, -1, 0, 0 };
   const int delta_y[4] = {  0,  0, +1, -1 };
   const int oblique_x[4] = { 0, 0, -1, +1 };
   const int oblique_y[4] = { +1, -1, 0, 0 };
   _edgelList.clear();
   size = 0;

   // This will zero the volume
   visited.alloc( volume.width(), volume.height(), 1, 1, ImageProperties::ZeroFramedArrayLayout );

   T *pixel = 0;
   UChar *pv = 0;

   if ( volume.bands() == 1 ) {
      T ***matrix = volume.data( 0 );
      SSize x = 0, y = 0;
      while ( edgel < end ) {
         /* Look around the inside pixel */
         x = edgel->xp;
         y = edgel->yp;
         pixel = matrix[0][ y ] + x;
         pv = & visited( x, y );
         if ( (*pixel) >= _min ) { // Pixel belongs
            if ( ( *(pixel + 1) < _min )  && ! ( (*pv) & Edgel::Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            if ( ( *( pixel - 1 ) < _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            if ( ( *( pixel + inc_dy )  < _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
            if ( ( *( pixel - inc_dy ) < _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
         } else { // Pixel si outside
            pv = &visited( x + 1, y );
            if ( ( *(pixel + 1) >= _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x + 1, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            pv = &visited( x - 1, y );
            if ( ( *( pixel - 1 ) >= _min )  && ! ( (*pv) & Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x - 1, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            pv = &visited( x, y + 1 );
            if ( ( *( pixel + inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y + 1, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
            pv = &visited( x, y - 1 );
            if ( ( *( pixel - inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y - 1 , Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
         }

         /* Look around the outside pixel */
         x += delta_x[ edgel->direction ];
         y += delta_y[ edgel->direction ];
         pixel = volume.data(0)[0][ y ] + x;
         pv = & visited( x, y );
         if ( (*pixel) >= _min ) { // Pixel belongs
            if ( ( *(pixel + 1) < _min )  && ! ( (*pv) & Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            if ( ( *( pixel - 1 ) < _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            if ( ( *( pixel + inc_dy )  < _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
            if ( ( *( pixel - inc_dy ) < _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
         } else { // Pixel si outside
            pv = &visited( x + 1, y );
            if ( ( *(pixel + 1) >= _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x + 1, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            pv = &visited( x - 1, y );
            if ( ( *( pixel - 1 ) >= _min )  && ! ( (*pv) & Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x - 1, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            pv = &visited( x, y + 1 );
            if ( ( *( pixel + inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y + 1, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
            pv = &visited( x, y - 1 );
            if ( ( *( pixel - inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y - 1 , Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
         }

         /* Look around the diagonal pixel */
         x += oblique_x[ edgel->direction ];
         y += oblique_y[ edgel->direction ];
         pixel = volume.data(0)[0][ y ] + x;
         if ( (*pixel) >= _min ) { // Pixel belongs
            pv = & visited( x, y );
            if ( ( *(pixel + 1) < _min )  && ! ( (*pv) & Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            if ( ( *( pixel - 1 ) < _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            if ( ( *( pixel + inc_dy )  < _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
            if ( ( *( pixel - inc_dy ) < _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
         } else { // Pixel is outside
            pv = &visited( x + 1, y );
            if ( ( *(pixel + 1) >= _min )  && ! ( (*pv) & Edgel::XMinusMask ) ) {
               _edgelList.addPixel( x + 1, y, Edgel::XMinusDirection, 1 );
               (*pv) |= Edgel::XMinusMask;
            }
            pv = &visited( x - 1, y );
            if ( ( *( pixel - 1 ) >= _min )  && ! ( (*pv) & Edgel::XPlusMask ) ) {
               _edgelList.addPixel( x - 1, y, Edgel::XPlusDirection, 1 );
               (*pv) |= Edgel::XPlusMask;
            }
            pv = &visited( x, y + 1 );
            if ( ( *( pixel + inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YMinusMask ) ) {
               _edgelList.addPixel( x, y + 1, Edgel::YMinusDirection, 1 );
               (*pv) |= Edgel::YMinusMask;
            }
            pv = &visited( x, y - 1 );
            if ( ( *( pixel - inc_dy ) >= _min )  && ! ( (*pv) & Edgel::YPlusMask ) ) {
               _edgelList.addPixel( x, y - 1 , Edgel::YPlusDirection, 1 );
               (*pv) |= Edgel::YPlusMask;
            }
         }
         ++edgel;
      }
   }
   disposeArray( prevArray );
   _edgelList.buildSortedArray();
   buildGraph( volume );
}

template<typename T>
void EdgelListBuilder::visitAny( ZZZImage<T> & volume )
{
   if ( _update )
      updateFrom(volume);
   else
      buildFrom(volume);
}

void
EdgelListBuilder::visit( ZZZImage<UChar> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<Short> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<UShort> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<Int32> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<UInt32> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<Float> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<Double> & volume )
{
   visitAny(volume);
}

void
EdgelListBuilder::visit( ZZZImage<Bool> &  )
{
   // visitAny(volume); // TODO
}


