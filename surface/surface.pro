LANGUAGE = C++
CONFIG	= qt staticlib
include("../global_options.qprefs")
TEMPLATE = lib

QT += widgets

INCLUDEPATH	+= .. ./include \
                      ../tools/include \
                      ../zzztools/include \
                      ../surface/include \
                      ../board/include \
                      ../gui/include

HEADERS	+=  ../globals.h \
            ./include/Surfel.h \
            ./include/SurfelList.h \
            ./include/EdgelList.h \
            ./include/Surface.h \
            ./include/SurfacePainter.h \
            ./include/SurfaceThinning.h \
            ./include/SurfelData.h \
            ./include/Vertex.h \
            ./include/Triangle.h \
            ./include/Convolution.h \
            ./include/ConvolThread.h \
            ./include/Face.h \
            ./include/SurfaceExporter.h \
            ./include/Evolver.h \
            ./include/Contour.h \
    include/SurfelListBuilder.h \
    include/EdgelListBuilder.h \
    include/ThreadedSurfacePainter.h \
    include/SurfacePainterThread.h \
    include/SurfelListRotateThread.h \
    include/SurfelGraphBuilder.h \
    include/SurfelGraphBuilderThread.h

SOURCES	+= src/Surfel.cpp \
           src/SurfelList.cpp \
           src/EdgelList.cpp \
           src/Vertex.cpp \
           src/Triangle.cpp \
           src/Surface.cpp \
           src/SurfacePainter.cpp \
           src/SurfaceThinning.cpp \
           src/Convolution.cpp \
           src/ConvolThread.cpp \
           src/Face.cpp \
           src/SurfaceExporter.cpp \
           src/Evolver.cpp \
           src/Contour.cpp \
    src/SurfelListBuilder.cpp \
    src/EdgelListBuilder.cpp \
    src/ThreadedSurfacePainter.cpp \
    src/SurfacePainterThread.cpp \
    src/SurfelListRotateThread.cpp \
    src/SurfelGraphBuilder.cpp \
    src/SurfelGraphBuilderThread.cpp

TARGET = ../libs/surface

release { DEFINES += _RELEASE_ }

OBJECTS_DIR = objs

#unix {
  UI_DIR = .ui
  MOC_DIR = .moc
#
# "-pedantic" is too much for Qt with linux-g++ ...
# ... but "-Wno-long-long" makes it fine! 
#
  QMAKE_CXXFLAGS_RELEASE += -fPIC -ffast-math -O3 -Wall -W -Wno-long-long -pedantic
  QMAKE_CXXFLAGS_DEBUG += -fPIC -ffast-math -Wall -W -Wextra -Wno-long-long -pedantic
  DEFINES += _IS_UNIX_
#}

# win32-g++ {
#   QMAKE_CXXFLAGS_RELEASE += -ffast-math -W -Wall -pedantic -Wno-long-long
#   QMAKE_CXXFLAGS_DEBUG += -W -Wall -pedantic -Wno-long-long
# }
