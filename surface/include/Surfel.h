/** -*- mode: c++ -*-
 * @file   Surfel.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 * 
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SURFEL_H_
#define _SURFEL_H_

#include <strings.h>
#include <limits>

#include <globals.h>
#include <zzzimage.h>
#include <dimension.h>
#include <Matrix.h>
#include <tools.h>
#include <iostream>
#include <vector>

#include <Triple.h>

#include "Face.h"

#define XP 0
#define XM 1
#define YP 2
#define YM 3
#define ZP 4
#define ZM 5

struct Surfel {
  Triple<SSize> voxel;
  Triple<double> center;
  Triple<double> rotatedCenter;
  Surfel * neighbors[4];
  int direction;
  UChar color;
  double gaussianCurvature;
  double meanCurvature;
  Triple<double> normal;

  inline Surfel();
  inline Surfel( Short x, Short y, Short z, int direction, UChar color );
  
  /** 
   * 
   * @param other 
   * 
   * @return 
   */
  bool operator==(const Surfel & other) const
  {
    return ( voxel == other.voxel) && (direction == other.direction);      
  }
  
  /** 
   * 
   * @param x 
   * @param y 
   * @param z 
   * @param direction 
   * @param color 
   */
  void set(double x, double y, double z, int direction, UChar color = 1);

  /** 
   * 
   * @param x 
   * @param y 
   * @param z 
   * @param direction 
   * @param color 
   */
  void setVoxel(Short x, Short y, Short z, int direction,
		UChar color = 1);


  /** 
   * 
   * @param v 
   */
  void getRotatedCenter( Triple<double> & v ) const;

  /** 
   * 
   * @param out 
   * @param surfel 
   * 
   * @return 
   */
  friend std::ostream & operator<<( std::ostream & out, const Surfel & surfel);

  /** 
   * 
   * @param other 
   * 
   * @return 
   */
  bool operator>( const Surfel & other ) const;
};

typedef Surfel *pSurfel;

std::ostream & operator<<( std::ostream & out, const Surfel & surfel);

int surfelCompare( Surfel **a, Surfel **b );

int pSurfelZCompare( const Surfel **a, const Surfel **b );

int pSurfelZCompareReverse( const Surfel **a, const Surfel **b );

extern const int edgeFromTable[6][4][6+2];

/**
 * Retourne le numero de l'arete par laquelle le surfel
 * de direction direction2 est adjacent a celui de direction
 * direction1 via son arete. 
 * @param direction1 L'orientation du premier surfel.
 * @param arete L'arete d'e-adjacence du premier surfel. 
 * @param direction2 L'orientation du second surfel.
 * @return L'ar�te par laquelle le second surfel est e-adjacent au
 * premier via son arete arete.
 */
inline int
edgeFrom( int direction1, int arete, int direction2 )
{  
  return edgeFromTable[direction1][arete][direction2];
}


/*
 * Surfel inline methods definitions
 */
Surfel::Surfel() {
  memset( this, 0, sizeof(Surfel) );
}

Surfel::Surfel( Short x, Short y, Short z, 
		int direction,
		UChar color ):
  voxel( x, y, z ),center( x, y, z ),direction( direction ),
     color( color )
{
  switch ( direction ) {
  case 0: center.first += 0.5; break;
  case 1: center.first -= 0.5; break;
  case 2: center.second += 0.5; break;
  case 3: center.second -= 0.5; break;
  case 4: center.third += 0.5; break;
  case 5: center.third -= 0.5; break;
  }
  rotatedCenter = center;
}

#endif
