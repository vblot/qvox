/** -*- mode: c++ -*-
 * @file   Triangle.h
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Nov 12 16:49:05 2005
 * 
 * @brief  
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include <iostream>
#include <vector>

#include <globals.h>
#include <Triple.h>
#include <zzzimage.h>

#include "SurfelData.h"
#include "Surface.h"

struct Triangle { 
  Triple<double> vertex0;
  Triple<double> vertex1;
  Triple<double> vertex2;
  Triple<float> color0;
  Triple<float> color1;
  Triple<float> color2;
  inline float red() const { return ( color0.first + color1.first + color2.first ) / 3.0F; }
  inline float green() const { return ( color0.second + color1.second + color2.second ) / 3.0F; }
  inline float blue() const { return ( color0.third + color1.third + color2.third ) / 3.0F; }
  bool valid;
  bool operator==( const Triangle & other ) const;
};

bool triangleZLess( const Triangle & a, const Triangle & b );
bool triangleZGreater( const Triangle & a, const Triangle & b );

std::ostream & dumpTrianglesPOV( std::vector<Triangle> & triangles, std::ostream & out, bool colors );

struct Surfel;
class SurfelList;
class ColorMap;

void addTriangle( std::vector<Triangle> & triangles,
		  SurfelData & s1, SurfelData & s2, SurfelData & s3,
		  const Triple<float> & color1,
		  const Triple<float> & color2,
		  const Triple<float> & color3 );


/** 
 * Adds to a vector of triangle the set of triangles associated
 * to the loops incident to a surfel. Only loops whose vertex indices
 * have not already been visited are added.
 * 
 * @param h 
 * @param surfel 
 * @param surfelCenters 
 * @param surface 
 * @param colormap 
 * @param visitedLoops Vector of bool which indicate whether or not
 *                     a loop has been added already. Vertex indices
 *                     must be used.
 */
void loopsToTriangles( std::vector<Triangle> & h,
		       Surfel *surfel,
		       SurfelDataArray & surfelCenters,
		       const Surface & surface,
		       const ColorMap & colormap,
		       std::vector<bool> & visitedLoops );

void loopToTriangles( std::vector<Triangle> & h, 
		      SurfelData *loop[], 
		      int n,
		      Surfel *surfels[],
		      const Surface & surface,
		      const ColorMap & colormap );

void getTriangleColor( const ColorMap & colormap,
		       const Surface & surface,
		       Surfel *s1, Surfel *s2, Surfel *s3,
		       float & red, float & green, float & blue );




#endif
