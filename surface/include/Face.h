/** -*- mode: c++ -*-
 * @file   Face.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:31:08 2005
 * 
 * @brief  Face structure.
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _FACE_H_
#define _FACE_H_

#include <globals.h>

template<typename T> class ZZZImage;

#include <Triple.h>

struct Surfel;

struct Face {
  Short x,y,z;
  int direction;
  Face():x(0),y(0),z(0),direction(0) { }
  Face( Short x, Short y, Short z, int direction )
    :x(x),y(y),z(z),direction(direction) { }
  Face( const Triple<Short> & voxel, int direction )
    :x(voxel.first),y(voxel.second),z(voxel.third),direction(direction) { }  
};

template<typename T> Face faceFindNeighbor_6_18( ZZZImage<T> * image, Face f, UChar edge);
template<typename T> Face faceFindNeighbor_18_6( ZZZImage<T> * image, Face f, UChar edge);
template<typename T> Face faceFindNeighborColor_6_18( ZZZImage<T> * image, Face f, UChar edge);
template<typename T> Face faceFindNeighborColor_18_6( ZZZImage<T> * image, Face f, UChar edge);

Face faceFindNeighbor_6_18( ZZZImage<Bool> * image, Face f, UChar edge);
Face faceFindNeighbor_18_6( ZZZImage<Bool> * image, Face f, UChar edge);
Face faceFindNeighborColor_6_18( ZZZImage<Bool> * image, Face f, UChar edge);
Face faceFindNeighborColor_18_6( ZZZImage<Bool> * image, Face f, UChar edge);

int faceAreteOpposee(int direction, int arete);

std::ostream & operator<<( std::ostream & out, const Face & face );

Face surfelToFace( Surfel *ps );


#endif
