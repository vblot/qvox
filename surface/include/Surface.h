/** -*- mode: c++ -*-
 * @file   Surface.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Nov 14 14:58:53 2005
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SURFACE_H_
#define _SURFACE_H_

#include <vector>
#include <limits>
#include <ctime>

#include <QString>
#include <QColor>
#include <Matrix.h>
#include "Triple.h"
#include "Pair.h"

#include <Progress.h>
#include "SurfelList.h"
#include "SurfelData.h"
#include "Quaternion.h"
#include "ThreadedSurfacePainter.h"

class AbstractSurfacePainter;
class SurfaceExporter;
class EdgelList;
class ColorMap;
class QImage;
class Settings;

#define COLORMAP_SIZE 255
#define BYTES_PER_PIXEL 4

class Surface;

class AbstractImage;

typedef QRgb (Surface::*SurfelColorMethod)( Surfel * ) const;
typedef Triple<double> t_facet[4];

class Surface : public QObject {

  Q_OBJECT

public:

  enum NormalsFlags { OriginalNormals, RotatedNormals };
  enum RotationFlag { RotateVisibleSurfels, RotateAllSurfels, RotateNoSurfel };
  enum Shading {
    ColorFromOrientation = 0,
    ColorFromDepth,
    ColorFromCurvature,
    ColorFromMaterial,
    ColorWhite,
    ColorFixed,
    ColorFromLight,
    ColorFromLightedMaterial,
    ColorFromLightedColorMap,
    ColorFromLightedLUT,
    ColorNotSet
  };
  enum ViewParamsFlags { KeepViewParams, ResetViewParams };

  friend class ColorMapSliceEdges_Painter;
  friend class ColorMapSlice_Painter;
  friend class ColorMapEdges_Painter;
  friend class ColorMap_Painter;
  friend class TrueColorSliceEdges_Painter;
  friend class TrueColorSlice_Painter;
  friend class TrueColorEdges_Painter;
  friend class TrueColor_Painter;

  /**
   *  The SurfelVertexIndices struct.
   */
  struct SurfelVertexIndices {
    Size indices[4];
  };

  /**
   *  The SurfelEdgeIndices struct.
   */
  struct SurfelEdgeIndices {
     Size indices[4];
  };

public slots:

  void setShading( int );
  void refreshShading();
  void clear();

signals:

  void shadingChanged( int );
  void changed();
  void info(const QString &);
  void clearInfo();

public:

  Surface();
  ~Surface();
  void markSurfel( int offset );
  void markSurfelList( std::vector<Surfel*> & v );
  void markSliceCurve( Surfel * ps );
  Surfel * getCloserSurfel( int x, int y,
                            int width, int height,
                            int * vertex = 0, int * edge = 0 );
  void initReferenceFaces();
  void initializeNormals( NormalsFlags type );
  void centersArray( SurfelDataArray & );
  void scalarArrayFromMarkedSurfels( SurfelDataArray & sda , UChar color );
  void curvaturesArray( SurfelDataArray & surfelDataArray );
  SurfelDataArray & surfelDataArray() { return _surfelDataArray; }
  SurfelList & surfelList() { return _surfelList; }
  const SurfelList & surfelList() const { return _surfelList; }
  void setImage(AbstractImage *,
                bool fullBox,
                ViewParamsFlags viewParams = KeepViewParams,
                int volumeAdjacency = ADJ18);

  AbstractImage & image() { return *_image; }
  const AbstractImage & image() const { return *_image; }

  Size buildSurfelVertexIndicesArray();
  Size buildSurfelEdgeIndicesArray();
  inline std::vector<SurfelVertexIndices> & surfelVertexIndicesArray();
  inline const std::vector<SurfelVertexIndices> & surfelVertexIndicesArray() const;
  inline std::vector<SurfelEdgeIndices> & surfelEdgeIndicesArray();
  inline const std::vector<SurfelEdgeIndices> & surfelEdgeIndicesArray() const;

  inline void settings( Settings * settings );

  void normalsArray( SurfelDataArray & sda );

  void convolution421Centers(int iterations, SurfelDataArray & data );
  void convolution421CentersAdaptive(int iterations, SurfelDataArray & data );
  void convolution421Centers( int iterationsA, SurfelDataArray & dataA,
                              int iterationsB, SurfelDataArray & dataB );
  void convolution421CentersAdaptive( int iterationsA, SurfelDataArray & dataA,
                                      int iterationsB, SurfelDataArray & dataB );
  void computeNormals( int iterations, bool adaptive );
  void computeCurvature( int normalEstimateIterations,
                         int curvatureEstimateIterations,
                         int averagingIterations,
                         int curvatureType,
                         bool adaptive );
  void computeCurvatureCenteredScheme( int normalEstimateIterations,
                                       int curvatureEstimateIterations,
                                       int averagingIterations,
                                       int curvatureType,
                                       bool adaptive );

  void computeCurvature121Scheme( int normalEstimateIterations,
                                  int curvatureEstimateIterations,
                                  int averagingIterations,
                                  int curvatureType,
                                  bool adaptive );

  void convolution421NormalsAndArea( int iterations,
                                     SurfelDataArray & data );
  void convolutionNormalsAndAngleSphere( int iterations,
                                         SurfelDataArray & data );

  void convolutionResponse( int iterations );

  void buildSSL();

  inline const Matrix & rotation();
  inline const Quaternion & rotationQuaternion();
  void rotateSurfels( RotationFlag flags = RotateVisibleSurfels );
  void rotate( double theta_x, double theta_y, double theta_z,
               RotationFlag flags = RotateVisibleSurfels );
  void rotate( const Quaternion & q,
               RotationFlag flags = RotateVisibleSurfels );
  void setRotation( const Quaternion & q,
                    RotationFlag flags = RotateVisibleSurfels );

  void viewCenterShift( int dx, int dy );
  void viewCenterReset();
  void translate( Triple<double> delta );

  void rotateLight( double theta_x, double theta_y, double theta_z );
  void rotateLight( const Quaternion & );
  void resetLight();
  void pullLight();
  void pushLight();

  void resetPosition();
  inline double zoom();
  void zoom( double factor );
  void zoomIn( );
  void zoomOut( );
  void zoomFit( int width, int height );

  inline int adjacency();
  inline void adjacency( int adjacency );

  bool simpleVoxel();

  inline int terminalTest();
  inline void terminalTest( int n );
  inline int skelMethod();
  inline void skelMethod( int n );

  bool saveViewParams( const char *filename );
  bool loadViewParams( const char *filename );

  void saveSurfelsColors( char *filename );
  void loadSurfelsColors( char *filename );

  inline Shading shading() const;

  inline bool edges();
  void edges( bool edges );
  void axis( bool on );
  inline bool axis();

  inline const Triple<double> & rotatedNormal( int i );

  QString surfelInfo( Surfel * ps );

  void paintSurfel( Surfel * ps, SSize radius, UChar color );

  inline Surfel ** visibleSurfelsBegin();
  inline Surfel ** visibleSurfelsEnd();

  inline Surfel ** sortedSurfelsArray();
  inline Surfel ** sortedSurfelsEnd();

  inline bool * visibleDirections();
  void buildSurfelsImages();
  QRgb surfelColor( Surfel * ps );


  void highlightSurfel( QImage & image, ColorMap & colormap, Surfel * ps );

  void drawSurface( QImage & image, ColorMap & colormap );

  void markSlice( int axis, int slice );
  void activateSliceMark( bool on );

  void drawEdgelList( EdgelList &  edgelList,
                      QImage & image, ColorMap & colormap,
                      const Matrix & rotation,
                      const Triple<double> & delta );

  QRgb surfelColorOrientation( Surfel * ps ) const;
  QRgb surfelColorDepth( Surfel * ps ) const;
  QRgb surfelColorFixed( Surfel * ps ) const;
  QRgb surfelColorLight( Surfel * ps ) const;
  QRgb surfelColorVoxelGray( Surfel * ps ) const;
  QRgb surfelColorVoxelRGB( Surfel * ps ) const;
  QRgb surfelColorLightedVoxelGray( Surfel * ps ) const;
  QRgb surfelColorLightedVoxelRGB( Surfel * ps ) const;
  QRgb surfelColorLightedVoxelColorMap( Surfel * ps ) const;
  QRgb surfelColorLightedVoxelLUT( Surfel * ps ) const;
  QRgb surfelColorConst( Surfel * ps ) const;

  void setSurfelColorsFromVoxels();
  void setSurfelColorsFromOrientation();

  void drawBoundingBox( QImage & image, ColorMap & colormap );
  void drawFine( QImage & image, ColorMap & colormap, double minDepth );
  void drawFine( QImage & image, ColorMap & colormap, double minDepth, int alpha );
  void drawSurfel( QImage & image, ColorMap & colormap,
                   Surfel * surfel,
                   int vertex,
                   int edge );
  void drawFineSurface(QImage & image, ColorMap & colormap );

  void setEdgeColor( QColor );
  const Triple<double> & aspect() const { return _aspect; }
  void aspect( bool on );
  void aspect( double x, double y, double z );

  inline int getSurfelColor( Surfel * ps ) const;

  Triple<float> getSurfelColor( Surfel *, const ColorMap &  ) const;

  void setCutPlane(bool);
  void moveCutPlaneForward();
  void moveCutPlaneBackward();

  void loadLUT( const char * filename );
  void clearLUT();
  inline const std::vector< Triple<UChar> > & lut();

  friend class SurfaceExporter;

  void invalidateNormals();
  inline bool validNormals();
  inline void clearSortedSurfelLists();



private:

  AbstractSurfacePainter * _surfacePainter;
  AbstractImage * _image;

  double _cutPlaneDepth;
  double _cutPlaneShift;

  SurfelColorMethod _surfelColorMethod;
  Shading _shading;
  QRgb _edgeRgb;
  QColor _edgeColor;

  bool _valid;
  SurfelList _surfelList;
  SortedSurfelList _ssl[ 2 ][ 2 ][ 2 ];
  SurfelDataArray _surfelDataArray;
  std::vector<SurfelVertexIndices> _surfelVertexIndicesArray;
  std::vector<SurfelEdgeIndices> _surfelEdgeIndicesArray;

  int _adjacency;
  bool _forceAllSurfels;
  Triple<double> _normals[6];
  Triple<double> _rotatedNormals[6];
  Pair<int> _viewCenterShift;
  Matrix _rotation;
  Quaternion _rotationQ;
  UInt _rotationsCount;
  Matrix _lightRotation;
  Quaternion _lightRotationQ;
  bool _edges;
  bool _axis;
  t_facet _rotatedFaces[6];
  t_facet _referenceFaces[6];

  Triple<double> _edgesDirections[6];
  Triple<double> _rotatedEdgesDirections[6];

  bool _visibleDirections[6];
  SSize _clickedVoxel[3];
  double _zoom;
  Triple<double> _lightPosition;
  Triple<double> _originalLightPosition;

  Triple<double> _aspect;
  bool _aspectEnabled;

  UChar _facesColors[6];

  Settings * _settings;

  int _terminalTest;
  int _skelMethod;

  int starts[6][MAX_ZOOM];
  int lengths[6][MAX_ZOOM];
  int height[6], rows[6];
  double dx[6], dy[6];
  int _maxSurfelWidth, _maxSurfelHeight;

  int _markedSurfel;
  int _markedSliceAxis;
  int _markedSlice;

  int _bytesPerPixel;

  /* Used by surfelColorMethod's */
  UChar _colors[6];
  double _zWidth, _zMin, _zMax;

  ColorMap * _lastColorMap;
  std::vector< Triple<UChar> > _lut;
  ThreadedSurfacePainter _threadedSurfacePainter;

  bool _validNormals;
};

/*
 * Definitions of inline methods
 */

double Surface::zoom()
{
   return _zoom;
}

int Surface::adjacency() {
   return _adjacency;
}

void Surface::adjacency( int adjacency )
{
   _adjacency = adjacency;
}

int Surface::terminalTest() { return _terminalTest; }
void Surface::terminalTest( int n ) { _terminalTest = n; }
int Surface::skelMethod() { return _skelMethod; }
void Surface::skelMethod( int n ) { _skelMethod = n; }

Surface::Shading Surface::shading() const { return _shading; }

bool Surface::edges() { return _edges; }
bool Surface::axis() { return _axis; }

std::vector<Surface::SurfelVertexIndices> & Surface::surfelVertexIndicesArray()
{
  return _surfelVertexIndicesArray;
}

const std::vector<Surface::SurfelVertexIndices> & Surface::surfelVertexIndicesArray() const
{
  return _surfelVertexIndicesArray;
}

std::vector<Surface::SurfelEdgeIndices> & Surface::surfelEdgeIndicesArray()
{
  return _surfelEdgeIndicesArray;
}

const std::vector<Surface::SurfelEdgeIndices> & Surface::surfelEdgeIndicesArray() const
{
  return _surfelEdgeIndicesArray;
}

const Matrix & Surface::rotation()
{
   return _rotation;
}

const Quaternion & Surface::rotationQuaternion()
{
   return _rotationQ;
}

Surfel ** Surface::sortedSurfelsArray()
{
   return _ssl[_visibleDirections[0]][_visibleDirections[2]][_visibleDirections[4]].begin();
}

Surfel ** Surface::sortedSurfelsEnd()
{
   return _ssl[_visibleDirections[0]][_visibleDirections[2]][_visibleDirections[4]].end();
}

bool * Surface::visibleDirections()
{
   return _visibleDirections;
}

int
Surface::getSurfelColor( Surfel * ps ) const
{
  return (this->*_surfelColorMethod)( ps );
}

const Triple<double> & Surface::rotatedNormal( int i ) {
  return _rotatedNormals[i];
}

inline void
Surface::settings( Settings * settings )
{
  _settings = settings;
}

Surfel **
Surface::visibleSurfelsBegin()
{
  return _ssl[ _visibleDirections[0] ][ _visibleDirections[2] ][ _visibleDirections[4] ].begin();
}

Surfel **
Surface::visibleSurfelsEnd()
{
  return _ssl[ _visibleDirections[0] ][ _visibleDirections[2] ][ _visibleDirections[4] ].end();
}

const std::vector< Triple<UChar> > & Surface::lut()
{
  return _lut;
}

bool Surface::validNormals()
{
  return _validNormals;
}

void Surface::clearSortedSurfelLists()
{
   _ssl[0][0][0].clear();
   _ssl[0][0][1].clear();
   _ssl[0][1][0].clear();
   _ssl[0][1][1].clear();
   _ssl[1][0][0].clear();
   _ssl[1][0][1].clear();
   _ssl[1][1][0].clear();
   _ssl[1][1][1].clear();
}


#endif
