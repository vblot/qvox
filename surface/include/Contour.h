/** -*- mode: c++ -*-
 * @file   Contour.h
 * @author Sebastien Fourey (GREYC)
 * @date   Mon Nov 14 14:58:53 2005
 * 
 * @brief  
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _CONTOUR_H_
#define _CONTOUR_H_

#include <limits>
#include <vector>

#include <globals.h>
#include <zzzimage.h>
#include <tools.h>
#include <Pair.h>
#include <config.h>

#include "EdgelList.h"

typedef std::vector< Pair<double> > EdgelDataArray;

class Contour {

 public:
   Contour();
   ~Contour();
   void normalsArray( EdgelDataArray & );
   void centersArray( EdgelDataArray & );
   void curvatureArray( EdgelDataArray & );
   EdgelDataArray & edgelDataArray() { return _edgelDataArray; }

   EdgelList & edgelList() { return _edgelList; }


   /** 
    * 
    * @param image 
    * @param normalIterations 
    * @param curvatureAveragings 
    * @param curvatures 
    * @param averagedCurvature 
    * 
    * @return The curvature range (min/max).
    */
   Pair<double> setImage( AbstractImage & image,
                          int normalIterations,
                          int curvatureAveragings,
                          std::vector<double> * curvatures = 0,
                          std::vector<double> * averagedCurvature = 0,
                          bool localUpdates = false );
  
   

   void markImage( ZZZImage<UChar> & image, UChar value = 64 );
   void convolutionNormals( int iterations );
   void convolutionNormalsOnce( bool stop );
   void convolutionDifferential( EdgelDataArray & );
   void computeCurvature( EdgelDataArray & );
   void convolutionCurvaturesAveragingOnce();
   void convolution421Centers(int iterations, EdgelDataArray & data );
   void convolution421Normals( int iterations, EdgelDataArray & data );
   
   void colorizeCurvature( EdgelList & edgelList );   

 private:
   EdgelList _edgelList;
   EdgelDataArray _edgelDataArray;
   Triple<double> _normals[4];
   Pair<double> _curvatureRange;
};




#endif
