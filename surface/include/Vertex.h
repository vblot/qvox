/** -*- mode: c++ -*-
 * @file   Vertex.h
 * @author Sebastien Fourey (GREYC)
 * @date   Sat Nov 12 15:59:41 2005
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <iostream>

#include <globals.h>
#include <tools.h>
#include <Triple.h>

struct Vertex { 
  Triple<double> position;
  Size index;
  bool valid;
  Vertex() {
    position = 0.0f;
    index = 0;
    valid = false;
  }
  bool operator==( const Vertex & other ) {
    return ( fabs( position.first - other.position.first ) < 0.00001 &&
             fabs( position.second - other.position.second ) < 0.00001 &&
             fabs( position.third - other.position.third ) < 0.00001 );
  }
};

class HashVertices { 
public:
  HashVertices( Size size );
  ~HashVertices();

  Size size() { return _count; }
  Vertex * add( Triple<double> position );
  Vertex * find( Vertex v );
  Vertex * find( Triple<double> position );

  std::ostream & dump( std::ostream & out);
  std::ostream & dumpOFF( std::ostream & out);

protected:
  Size double2Size( double x);
  double double3Decimals( double x );
  Size key( const Triple<double> & v );

private:
  Vertex * _array;
  Vertex * _limit;
  Size _count;
};


#endif



