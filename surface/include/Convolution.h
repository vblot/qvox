/** -*- mode: c++ -*-
 * @file   Convolution.h
 * @author Sebastien Fourey (GREYC)
 * @date   Wed Nov 16 14:37:12 2005
 * 
 * @brief  
 * 
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _CONVOLUTION_H_
#define _CONVOLUTION_H_

#include <zzzimage.h>
#include <Triangle.h>
#include <Triple.h>
#include <vector>

#include "SurfelList.h"
#include "SurfelData.h"

class ColorMap;

#define MEAN_CURVATURE 0
#define GAUSSIAN_CURVATURE 1
#define MAXIMUM_CURVATURE 2

struct VNeighborhood {  
  Surfel *center;
  Surfel **eNeighbors;
  Surfel * loops[4][4];		/**< Surfels of the loops that are not e-neigbors */
  int loopSize[4];

  VNeighborhood( Surfel * surfel = 0 );
};

VNeighborhood computeVNeighborhood( Surfel *surfel );

std::vector<Surfel*> vNeighborhood( Surfel * );

/** 
 * Return the e-neighbor of the e-neighbor according to a given edge and
 * which belong to the given loop (according to the vertex).
 * 
 * @param surfel A surfel.
 * @param edge The edge shared with the e-neighbor.
 * @param vertex The vertex of the loop of interest.
 * 
 * @return The e-neighbor of the e-beighbor that belongs to the loop.
 */
Surfel * eeNeighbor( Surfel * surfel, int edge, int vertex );

SurfelData convolution421( Surfel *surfel,
			   const SurfelList & surfelList,
			   const SurfelDataArray & sdaSrc,
			   SurfelDataArray & sdaDest,
			   double cCentral = 4.0,
			   double cEdge = 2.0, 
			   double cVertex = 1.0 );

SurfelData convolutionIso( Surfel *surfel,
			   SurfelList & surfelList,
			   SurfelDataArray & sdaSrc,
			   SurfelDataArray & sdaDest,
			   double weight = 4.0 );

SurfelData convolutionConst( Surfel *surfel,
			     SurfelList & surfelList,
			     SurfelDataArray & sdaSrc,
			     SurfelDataArray & sdaDest );

SurfelData convolutionNormals421( Surfel *surfel );

void convolutionDifferential(Surfel *surfel,
			     SurfelList & surfelList,
			     SurfelDataArray & destA,
			     SurfelDataArray & destB,
			     SurfelData & sda,
			     SurfelData & sdb);


void computeCurvature( Surfel *ps,
		       const Triple<double> normals[] );

void colorizeCurvature( SurfelList & surfelList,
			const SurfelDataArray & curvatures,
			int curvatureType );

void colorizeCurvature( SurfelList & surfelList,
			int curvatureType );

#endif



