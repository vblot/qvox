/** -*- mode: c++ -*-
 * @file   EdgelList.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 *
 * @brief  Edgel List and Ordered Edgel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _EDGELLIST_H_
#define _EDGELLIST_H_

#include <limits>
#include <iostream>
#include <vector>

#include <globals.h>
#include <tools.h>

#include "zzzimage.h"


struct Edgel {

   enum DirectionMask { XPlusMask = 1,
                        XMinusMask = 2,
                        YPlusMask = 4,
                        YMinusMask = 8 };

   enum Direction { XPlusDirection = 0,
                    XMinusDirection = 1,
                    YPlusDirection = 2,
                    YMinusDirection = 3 };

   double x, y;
   SSize xp, yp;
   Edgel *next, *previous;
   Direction direction;
   double curvature;
   UChar color;

   bool operator==( const Edgel & other ) const
   {
      return (xp == other.xp) && (yp == other.yp) && (direction == other.direction);
   }

   void set( double x, double y, Direction direction, UChar color = 1 );

   void setPixel( SSize x, SSize y, Direction direction, UChar color = 1 );

   bool operator>( const Edgel & other ) const;
};

std::ostream & operator<<( std::ostream & out, const Edgel & edgel );

int edgelCompare( Edgel **a, Edgel **b );

class EdgelList {

public:
   Edgel * array() { return _array; }
   Edgel * begin() { return _array; }
   Edgel * end() { return _end; }
   Size count() { return _end - _array; }

   EdgelList( Size size = 0 );
   ~EdgelList();

   void buildFromImage( AbstractImage & image,
                        UChar min = 1 );

   void updateFromImage( AbstractImage & image,
                         UChar min = 1 );

   void buildSortedArray();

   void resize( Size size );
   void clear();
   void addPixel( SSize x, SSize y, Edgel::Direction direction, UChar color);

   Edgel* find( SSize x, SSize y, Edgel::Direction direction );

   Edgel* quickFind( SSize x, SSize y, Edgel::Direction direction );

   Edgel* quickFind( Edgel & edgel );

   friend std::ostream & operator<<( std::ostream & out, const EdgelList & edgelList );

   void dumpCurvatures( std::ostream & out );

   void curvatures( std::vector<double> & v );

   void color( UChar );

private:
   Edgel *_array;
   Edgel *_current;
   Edgel *_end;
   Edgel *_limit;
   Edgel **_sortedArray;
};

std::ostream & operator<<( std::ostream & out, const EdgelList & edgelList );

#endif
