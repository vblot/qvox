/** -*- mode: c++ -*-
 * @file   SurfaceThinng.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 * 
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SURFACETHINNING_H_
#define _SURFACETHINNING_H_

#include "SurfelList.h"

const int InsideMask = 1;
const int BorderMask = 3;

struct VNeighborhood;

/** 
 * Checks if a surfel is e-simple.
 * 
 * @param surfel The surfel to be checked.
 * @param eNeighbors (out parameter) The number of e-neighbors which are ones.
 * @param nb (out parameter) The surfel's v-neighborhoord.
 * 
 * @return 
 */
bool simpleSurfelE( Surfel *surfel, int & eNeighbors, VNeighborhood & nb );

/** 
 * Checks if a surfel is v-simple.
 * 
 * @param surfel The surfel to be checked.
 * @param vNeighbors (out parameter) The number of v-neighbors which are ones.
 * @param nb (out parameter) The surfel's v-neighborhoord.
 * 
 * @return True if the surfel is v-simple.
 */
bool simpleSurfelV( Surfel *surfel, int & vNeighbors, VNeighborhood & nb );

/** 
 * Checks if a surfel belongs to the border of an e-connected set.
 * (i.e., if it has at least one 0-surfel in its v-neighborhood.
 * 
 * @param surfel A surfel
 * 
 * @return true if the surfel belongs to the (e,v)-border
 */
bool borderSurfelE( Surfel * surfel );

/** 
 * Checks if a surfel belongs to the border of a v-connected set.
 * (i.e., if it has at least one 0-surfel in its e-neighborhood.
 * 
 * @param surfel A surfel.
 * 
 * @return true if the surfel belongs to the (v,e)-border.
 */
inline bool borderSurfelV( Surfel * surfel );


/** 
 * Check if a surfel is an e-extremity.
 * Warning: An e-extremity is here defined as a 1-surfel s which 
 * has a single e-neighbors s' such that the two e-neighbors
 * of s' in the v-neighborhood of s are 0-surfel. This
 * is more restrictive than "A surfel that has a single 1 e-neighbor.
 * 
 * @param surfel The surfel to be checked.
 * 
 * @return true if the surfel is an e-extremity (in the special 
 *         sense mentioned above).
 */
bool borderSurfelE( Surfel * surfel );


/** 
 * Applies an homotopic thinning with (v,e)-adjecency. 
 * 
 * @param surfelList 
 * @param iterations 
 */
void surfaceThinningV( SurfelList & surfelList,
		       int iterations,
		       bool keepExtremities );

/** 
 * Applies an homotopic thinning with (e,v)-adjecency. 
 * 
 * @param surfelList 
 * @param iterations 
 */
void surfaceThinningE( SurfelList & surfelList,
		       int iterations,
		       bool keepExtremities );


/** 
 * Removes e-branches which are longer then a given length.
 * 
 * @param surfelList The list of surfels.
 * @param minLength The minimal length of branches to be removed.
 */
void pruneEbranches( SurfelList & surfelList, int minLength );


/** 
 * Removes v-branches which are longer then a given length.
 * 
 * @param surfelList The list of surfels.
 * @param minLength The minimal length of branches to be removed.
 */
void pruneVbranches( SurfelList & surfelList, int maxLength );

/*
 * Definition of inline functions.
 */

bool
borderSurfelV( Surfel * surfel )
{
  return  ! ( ( surfel->neighbors[0]->color & InsideMask ) && 
	      ( surfel->neighbors[1]->color & InsideMask ) && 
	      ( surfel->neighbors[2]->color & InsideMask ) && 
	      ( surfel->neighbors[3]->color & InsideMask ) ) ;
}





#endif /* ifdef SURFELSIMPLICITY_H */
