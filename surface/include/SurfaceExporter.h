/** -*- mode: c++ -*-
 * @file   SurfaceExporter.h
 * @author Sebastien Fourey (GREYC)
 * @date   Thu Feb  9 11:13:09 2006
 *
 * @brief
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 *
 * https://foureys.users.greyc.fr
 *
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SURFACEEXPORTER_H_
#define _SURFACEEXPORTER_H_

#include "globals.h"
#include <fstream>
#include <Progress.h>

class Surface;
class ColorMap;
namespace LibBoard { class Board; }


class SurfaceExporter {

public:
  SurfaceExporter( Surface & surface, const ColorMap & colorMap );
  
  bool writeEPS( const char * fileName, bool axis );
  bool writeFIG( const char *fileName );
  bool writeSVG( const char *fileName );
  bool writeSmoothedEPS( int iterations, unsigned char gouraudDivisions, const char * fileName );
  bool writeOFF( const char *fileName,
                 int iterations,
                 bool adaptive,
                 bool fixedColor,
                 float red, float green, float blue );
  bool writeOBJ( const char *fileName,
                 int iterations);
  bool writeSurfelsOFF( const char *fileName);

protected:

  bool draw( LibBoard::Board & board );

  void writeEPSColorMap( std::ofstream & file );
  void writeEPSSurfels( std::ofstream & file );
  void writeEPSAxis( std::ofstream & file, double scale );
  void setEPSViewPort( std::ofstream & file,
                       double left, double top,
                       double width, double height );

  void writeFIGColorMap( std::ofstream & file,
                         int usedColors[] );


protected:
  double transformX( double x );
  double transformY( double y );
  void transform( double & x, double & y );
  double _scale, _deltaX, _deltaY;
  double _top, _left, _width, _height;
private:
  Surface & _surface;
  const ColorMap & _colorMap;
};

#endif
