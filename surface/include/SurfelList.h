/** -*- mode: c++ -*-
 * @file   SurfelList.h
 * @author Sebastien Fourey (GREYC)
 * @date   Tue Dec 20 17:22:55 2005
 *
 * @brief  Surfel List and Ordered Surfel List classes.
 *
 * \@copyright
 * Copyright or � or Copr. Sebastien Fourey
 * 
 * https://foureys.users.greyc.fr
 * 
 * This source code is part of the QVox project, a computer program whose
 * purpose is to allow the visualization and edition of volumetric images. 
 * 
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
#ifndef _SURFELLIST_H_
#define _SURFELLIST_H_

#include <strings.h>
#include <limits>

#include <globals.h>
#include <zzzimage.h>
#include <dimension.h>
#include <Matrix.h>
#include <tools.h>
#include <iostream>
#include <vector>

#include <Triple.h>

#include <QTime>
#include <vector>

#include "Face.h"
#include "Surfel.h"

#define XP 0
#define XM 1
#define YP 2
#define YM 3
#define ZP 4
#define ZM 5

class EdgelList;

class SurfelList {

public:


  enum Position { RotatedPosition, OriginalPosition };

  SurfelList( Size size = 0 );
  ~SurfelList();

  inline Surfel * begin();
  inline Surfel * end();
  inline const Surfel * begin() const;
  inline const Surfel * end() const;
  inline size_t size() const;
  inline size_t capacity() const;

  inline const Triple<double> & bbMin() const;
  inline const Triple<double> & bbMax() const;
  inline const Triple<double> & bbMinOriginal() const;
  inline const Triple<double> & bbMaxOriginal() const;
  inline const Triple<double> & centerTranslation() const;

  void buildGraph( ZZZImage<UChar> * image,
                   bool fullBox,
                   int volumeAdjacency );

  /**
   *
   * @param size
   */
  void resize(Size size);

  /**
   * Empty the list.
   *
   */
  void clear();

  /**
   * Free the memory used by the list.
   *
   */
  void free();

  /**
   *
   */
  void buildSortedArray();

  void colorize( EdgelList & edgelList );

  /**
   *
   * @param x
   * @param y
   * @param z
   * @param direction
   * @param color
   */
  inline void add( const Surfel & surfel );


  /**
   *
   * @param dx
   * @param dy
   * @param dz
   */
  void translate( Triple<double> delta );


  /**
   * Reset the center of the set of surfels
   * by translating back.
   *
   */
  void reCenter();


  /**
   *
   * @param rotation
   * @param visible
   */
  void rotate( const Matrix & rotation );

  /**
   *
   * @param f
   *
   * @return
   */
  Surfel* find( Triple<SSize> voxel , int direction );

  /**
   *
   * @param f
   *
   * @return
   */
  Surfel* quickFind( Triple<SSize> voxel, int direction );


  /**
   *
   * @param f
   *
   * @return
   */
  Surfel* quickFind( const Face & face );

  /**
   *
   * @param surfel
   *
   * @return
   */
  Surfel * quickFind( Surfel & surfel );

  /**
   *
   * @param f
   *
   * @return
   */
  Surfel faceToSurfel( Face f );

  /**
   * Fill a volume mask with 1's where a surfel
   * with given color is in the list.
   *
   * @param volume
   * @param color
   */
  void fillVolumeFromSurfelColor( ZZZImage<UChar> & volume,
                                  UChar color );

  /**
   *
   * @param color
   */
  void setColors(UChar color);

  /**
   *
   * @param color
   */
  void setTransparency( UChar color );

  /**
   *
   * @param color
   */
  void unsetTransparency( UChar color );

  friend std::ostream & operator<<( std::ostream & out, SurfelList & surfelList);

  bool bbValid() { return _bbValid; }
  void bbValid( bool b ) { _bbValid = b; }

  inline double maxDepthRange() const;

  void computeBBox( bool force, Position position );

  void rotatedBBox( double & xMin, double & xMax,
                    double & yMin, double & yMax );


private:
  std::vector<Surfel> _array;
  Surfel **_sortedArray;
  Triple<double> _bbMin;
  Triple<double> _bbMax;
  Triple<double> _bbMinOriginal;
  Triple<double> _bbMaxOriginal;
  Triple<double> _centerTranslation;
  bool _bbValid;
};

std::ostream & operator<<( std::ostream & out, SurfelList & surfelList);

class SortedSurfelList {
  Surfel **_array;
  Surfel **_end;
  Surfel **_limit;

public:
  SortedSurfelList( Size size = 0 );
  ~SortedSurfelList();

  Surfel ** begin() { return _array; }
  Surfel ** end() { return _end; }
  Size size() { return _end - _array; }
  Size capacity() { return _limit - _array; }

  void resize( Size size );
  void clear() { _end = _array; }

  void buildFromSurfelList( SurfelList & sl, bool visible[6], bool reversed = false );
  void buildFromSurfelListLinear( SurfelList & sl,
                                  bool visible[6],
  Size surfelsPerLayer,
  bool reversed
  );
  void inversedCopy( SortedSurfelList & sl );
  void sort( bool reversed = false );
  void rotateOLD( Matrix rotation, Triple<double> & bbMin, Triple<double> & bbMax );
  void rotate( const Matrix & rotation, const Triple<double> & aspect );
};

/*
 * SurfelList inline methods definitions
 */

Surfel *
SurfelList::begin() {
  if ( ! _array.size() ) return 0;
  return reinterpret_cast<Surfel*>( & _array.front() );
}

Surfel *
SurfelList::end() {
  if ( ! _array.size() ) return 0;
  return reinterpret_cast<Surfel*>( ( &_array.back() ) + 1 );
}

const Surfel *
SurfelList::begin() const {
  if ( ! _array.size() ) return 0;
  return reinterpret_cast<const Surfel*>( & _array.front() );
}

const Surfel *
SurfelList::end() const {
  if ( ! _array.size() ) return 0;
  return reinterpret_cast<const Surfel*>( (& _array.back() ) + 1 );
}

size_t
SurfelList::size() const {
  return _array.size();
}

size_t
SurfelList::capacity() const {
  return _array.capacity();
}

const Triple<double> &
SurfelList::bbMin() const {
  return _bbMin;
}

const Triple<double> &
SurfelList::bbMax() const {
  return _bbMax;
}

const Triple<double> &
SurfelList::bbMinOriginal() const {
  return _bbMinOriginal;
}

const Triple<double> &
SurfelList::bbMaxOriginal() const {
  return _bbMaxOriginal;
}

double
SurfelList::maxDepthRange() const {
  return abs( _bbMax - _bbMin );
}

const Triple<double> &
SurfelList::centerTranslation() const {
  return _centerTranslation;
}

void
SurfelList::add( const Surfel & surfel )
{
  _array.push_back( surfel );
  lower( _bbMin, surfel.center );
  raise( _bbMax, surfel.center );
}

#endif
